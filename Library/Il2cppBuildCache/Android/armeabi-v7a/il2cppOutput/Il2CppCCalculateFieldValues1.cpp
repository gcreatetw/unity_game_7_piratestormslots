﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>
#include <limits>



// System.Action`1<System.Boolean>
struct Action_1_tCE2D770918A65CAD277C08C4E8C05385EA267E83;
// System.Action`1<CSBankCoinPanel>
struct Action_1_t0B84DFBE5FC87EC8D8B398C1BACFE174F3A5D542;
// System.Action`1<CSCard>
struct Action_1_t920BC933029F70EEB3BF5271315DE37429A513B5;
// System.Action`1<UnityEngine.Color>
struct Action_1_tC747810051096FEC47140D886015F5428DEDDAE2;
// System.Action`1<GoogleMobileAds.Api.InitializationStatus>
struct Action_1_t73B690804B71062EA366B9EF731317DA6FE13907;
// System.Action`1<System.Object>
struct Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC;
// System.Action`1<System.Single>
struct Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9;
// System.Action`1<Sound>
struct Action_1_t86AC5575DD5B9AEA9C3D10160063071E7C6850C1;
// System.Action`1<UnityEngine.Vector2>
struct Action_1_t0C1F417511439CBAA03909A69138FAF0BD19FA30;
// System.Action`1<UnityEngine.Vector3>
struct Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B;
// System.Action`2<System.Boolean,CSZLGambleContent>
struct Action_2_t6263456D797EA3E0910B4DCAB4AA76BB2A78EB45;
// System.Action`2<CSTimer,System.String>
struct Action_2_tCF09AEE84B095AE2F1E794A351B70E78CB615A6B;
// System.Action`2<UnityEngine.Color,System.Object>
struct Action_2_t05923E9D06D867E1AC6A90E2B1B2FE28CA9F910A;
// System.Action`2<UnityEngine.Purchasing.Product,CSIAPProduct>
struct Action_2_t72FC6CF8D9D151A7D4DF016104A186B035F2FC31;
// System.Action`2<System.Single,System.Boolean>
struct Action_2_tBF5D9EB992F91A3DA045CAF342A5DE18B805F91D;
// System.Action`2<System.Single,System.Object>
struct Action_2_tC5EA1BA82797894656DD89A0F3E7B791EC8BD050;
// System.Action`2<System.Single,System.Single>
struct Action_2_tBA82D2430D3596594262E1ACAD640A8EEB5DB2F2;
// System.Action`2<UnityEngine.Vector3,System.Object>
struct Action_2_tCA8676CFBCEAA1EE28321875815FD1767B25E4CB;
// System.Action`3<System.Boolean,CSCard,CSCard>
struct Action_3_t14CBE6B9894E7C8B946B2CDA30D69657CCF537C8;
// CSListNavigation`1<CSPayline>
struct CSListNavigation_1_t4AC590C238DA470F8CF25714CBC444E615E3C5C1;
// System.Comparison`1<System.Int32>
struct Comparison_1_t3430355DCDD0AF453788265DC88E9288D19C4E9C;
// System.Collections.Generic.Dictionary`2<CSSymbolType,CSSymbolData>
struct Dictionary_2_t0B8BEAF41857685DF2F0FB4B87D806425B9E3188;
// System.Collections.Generic.Dictionary`2<System.Int32,StarData>
struct Dictionary_2_tA165F361FD3982DB0143EB119AB88636E6991F01;
// System.Collections.Generic.Dictionary`2<System.String,CSTimer>
struct Dictionary_2_t9F4C1D688345D026BBBA09BE046EAF8BF6564015;
// System.Collections.Generic.Dictionary`2<System.String,CSTimerData>
struct Dictionary_2_tC13AD4CF9968EB1A9E950D624D16833A37BC9E5C;
// System.Collections.Generic.List`1<CSCardValue>
struct List_1_t4CDCC84CA5994FB6E22235E4D8E37718C18D58B6;
// System.Collections.Generic.List`1<CSCell>
struct List_1_t2FB4CCBD066E089694C632BDEB208275C984E004;
// System.Collections.Generic.List`1<CSIAPProduct>
struct List_1_t164CAD3B766922142F98E0633949D448FA07B5BE;
// System.Collections.Generic.List`1<CSSymbol>
struct List_1_t2A81543C57C79F7AA8F30E3FB31798B8BED5D3E9;
// System.Collections.Generic.List`1<CSSymbolPercent>
struct List_1_tD670094CFCCB25276B57801E2F067BFF969BE09D;
// System.Collections.Generic.List`1<CSTimer>
struct List_1_tC45461C839422BCF03CB1E007A1F16EE75DFE211;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA;
// System.Collections.Generic.List`1<UnityEngine.UI.Toggle>
struct List_1_tECEEA56321275CFF8DECB929786CE364F743B07D;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t11E0F272F18CD83EDF205B4A43689B005D10B7BF;
// System.Action`1<LTEvent>[]
struct Action_1U5BU5D_t930658157BBD9409C35E85E5A34B23FC4EEF0772;
// UnityEngine.Vector3[][]
struct Vector3U5BU5DU5BU5D_tAD34024FBDE3F1F720ECB62B24B914A748C60BCB;
// System.Boolean[]
struct BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C;
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B;
// CSCardData[]
struct CSCardDataU5BU5D_tCBC644233E59D6BA967534AAD4A52E1EBC8464A1;
// CSLine[]
struct CSLineU5BU5D_t858A6F64F7D40673B867E58C0664C80F3F154FB2;
// CSReel[]
struct CSReelU5BU5D_tC3ABB496276FFD59F38137BCBDB1F126B84C1A27;
// CSSymbol[]
struct CSSymbolU5BU5D_tF220DF3FE5F1E80568DA2518BF6970BE5AB3E4EA;
// CSSymbolData[]
struct CSSymbolDataU5BU5D_tFCB90478E34016E4E24E2CAC2B35B9759D7C174D;
// CSSymbolPercent[]
struct CSSymbolPercentU5BU5D_tFBD1ACC0D6BF2C5F9D97B45EEC1FA6E3D395FF8D;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// LTBezier[]
struct LTBezierU5BU5D_t4DDB3352316B2B476AEEE5AB4F760513856FC153;
// LTDescr[]
struct LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F;
// LTRect[]
struct LTRectU5BU5D_t764F00C0F4846E8D85477368EC19E34DF5D0DCBA;
// LTSeq[]
struct LTSeqU5BU5D_t5C740B5E327381CE2E6FD41B011EA9A72355FD51;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.Rect[]
struct RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE;
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
// UnityEngine.UI.Slider[]
struct SliderU5BU5D_t819F98171147BE3B24976AC62AB8B1900BDEE01A;
// Sound[]
struct SoundU5BU5D_tA5C580C0BFABFBC56CF4F26FC1765B4F5194E2DF;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// TMPro.TMP_MeshInfo[]
struct TMP_MeshInfoU5BU5D_t6C0A65D18C54B6FA681B2EB0676B83116FD03119;
// UnityEngine.Transform[]
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// CS2DBoolArray/L2D[]
struct L2DU5BU5D_tDA306A5D580822CBB99C992A56CE5A22280374EA;
// CSCardList2D/List2D[]
struct List2DU5BU5D_t19A5AD69DAAFF117D4A6DBF4629C88A7B1CCFFAE;
// TMPro.Examples.VertexJitter/VertexAnim[]
struct VertexAnimU5BU5D_t069E9FCD87DE8FF65BE288FDB2834711D933AAFF;
// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6;
// UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// UnityEngine.AsyncOperation
struct AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86;
// UnityEngine.AudioClip
struct AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE;
// UnityEngine.AudioSource
struct AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B;
// TMPro.Examples.Benchmark01
struct Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934;
// TMPro.Examples.Benchmark01_UGUI
struct Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B;
// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D;
// CS2DBoolArray
struct CS2DBoolArray_t4A5EA41B80365341FAB8F7B222392F23F5083434;
// CSAlert
struct CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A;
// CSAlertRewardAnim
struct CSAlertRewardAnim_t13B8641467B4284296D29B346C9760D59E21E9E0;
// CSAnimationData
struct CSAnimationData_t39AC7CF1C6BE740E5450F413840F4B8E08309645;
// CSBankCoinPanel
struct CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029;
// CSBottomPanel
struct CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38;
// CSCSGamble
struct CSCSGamble_tC4C1846CB1F82B4350A1D3E73034D7581A80B93F;
// CSCSWinAlert
struct CSCSWinAlert_tE1DC42587F7A4AF4EE8AA718FAB6269314AB6B79;
// CSCard
struct CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2;
// CSCardData
struct CSCardData_tD1BD9207FB0542F11285FAA8DAC8A02F88C6C9B7;
// CSCardList2D
struct CSCardList2D_tB265A74FEF9F4D8A544A88653A96C54F1F5B66F5;
// CSCell
struct CSCell_tEC2CF7F1268B4BE5C1155F2B9476449C3A14383D;
// CSExperiencePanel
struct CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F;
// CSFreeGamePanel
struct CSFreeGamePanel_tC3C7EE40E9C939B3B63EFCE22621ED5792C3D7E6;
// CSGameData
struct CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04;
// CSGameStore
struct CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D;
// CSGlowAnimation
struct CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186;
// CSIAPProduct
struct CSIAPProduct_tF735AB0F8AA68F1481FA63E8AF5EB9AF02F7EAA2;
// CSInfo
struct CSInfo_tD22B0C9AD79CB449BCBC64512307F5EFAE046AA1;
// CSLFBGAlertBoard
struct CSLFBGAlertBoard_t6EF319BFC3C51BF1443743AFC7030B7566305D45;
// CSLFProgressBar
struct CSLFProgressBar_t2236B83E6C32E322F6C3707BEEAD2FC5EE9F2035;
// CSLevelUpAlert
struct CSLevelUpAlert_tEF4E42E838D5DB762ABB678F2A29B38E6FD491CB;
// CSLine
struct CSLine_t3E62A34AE974F1DA0D692280F027A34103D5214C;
// CSReel
struct CSReel_t8454490940D72C04785CF2BF71C41073113E1890;
// CSReelAutoSpin
struct CSReelAutoSpin_tE3C4581E7195FF30D21E5E5EFDE1BA9344C385E4;
// CSReelRandom
struct CSReelRandom_t16B21B9455802008C805A265B104CD5F2D02423B;
// CSReels
struct CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47;
// CSReelsAnimation
struct CSReelsAnimation_t04EC01AF43D2016CB036755318F8873C79F9714D;
// CSRule
struct CSRule_t57A6472E5A907AC0E251BCF987970B41AD1F0561;
// CSSettingsPanel
struct CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842;
// CSSettingsPanelSmall
struct CSSettingsPanelSmall_t436B210ADD3C25F979D822BD214FBDE286D44CA0;
// CSSliderGroup
struct CSSliderGroup_tDC34B5CDF947DBA827407A2B6D39F5E7ADC28F4C;
// CSSymbolData
struct CSSymbolData_t189F10C509C4A90E39640E27AA4367BCC44D5816;
// CSTimer
struct CSTimer_tA0601B382CE03F0C9C9A31E54C5EEDAD89CD7668;
// CSToggleGroupHelper
struct CSToggleGroupHelper_t0B4982EA19DDA807327BDCF285651D4FB8C4551A;
// CSTopPanel
struct CSTopPanel_t94C2B84ED826C2C503575317319F623658C0C4D8;
// CSZLGamble
struct CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C;
// CSZLGambleContent
struct CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasGroup
struct CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F;
// UnityEngine.Purchasing.ConfigurationBuilder
struct ConfigurationBuilder_t8C2A33B91D14FD46DA1EF4EF45CA14143260969A;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// EnvMapAnimator
struct EnvMapAnimator_t7C30A094A3786710A896AD9A6FA46B13EFEB72FD;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// UnityEngine.Font
struct Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9;
// UnityEngine.GUIStyle
struct GUIStyle_t29C59470ACD0A35C81EB0615653FD38C455A4726;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// HorizontalScrollSnap
struct HorizontalScrollSnap_tE6B632D4D5CDD31DFC9CF915D070FD422F1597BA;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_tFD47F15B6FCCD46BE6E88D2112B30EC88061B8DC;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t7F8439B516FC2268F81CE246954C55F8DC6E40F8;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// GoogleMobileAds.Api.InterstitialAd
struct InterstitialAd_t9ED58965C70F2CF9E66F92EAE19149A1A237E8C9;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9;
// LTBezierPath
struct LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48;
// LTDescr
struct LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F;
// LTDescrOptional
struct LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2;
// LTRect
struct LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD;
// LTSpline
struct LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701;
// LeanTester
struct LeanTester_t537C3FE41ED244252241E5B02F6B1BA6DA437297;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.ParticleSystem
struct ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC;
// UnityEngine.UI.RawImage
struct RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C;
// GoogleMobileAds.Api.RewardBasedVideoAd
struct RewardBasedVideoAd_tB89863A4548A04A4DCF7E1E8408F5648E6BC84CC;
// UnityEngine.UI.ScrollRect
struct ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA;
// UnityEngine.UI.Scrollbar
struct Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28;
// TMPro.Examples.ShaderPropAnimator
struct ShaderPropAnimator_t44FB2882ECA0D2C780444EB96B9550F5D245CFB8;
// TMPro.Examples.SkewTextExample
struct SkewTextExample_tA9F165D1BE969ED73FB86CA6C641386C22F1EE07;
// UnityEngine.UI.Slider
struct Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF;
// System.String
struct String_t;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2;
// TMPro.TMP_InputField
struct TMP_InputField_tD50B4F3E6822EAC2720FAED56B86E98183F61D59;
// TMPro.TMP_Text
struct TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262;
// TMPro.TMP_TextEventHandler
struct TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547;
// TMPro.Examples.TeleType
struct TeleType_t92B5ED4FE818F912A562F085CE035F6B42834934;
// DentedPixel.LTExamples.TestingUnitTests
struct TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// TMPro.Examples.TextConsoleSimulator
struct TextConsoleSimulator_t82904A7668D46B20E576B30E4CE367C0B66C5289;
// TMPro.TextContainer
struct TextContainer_t397B1340CD69150F936048DB53D857135408D2A1;
// UnityEngine.TextMesh
struct TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273;
// TMPro.TextMeshPro
struct TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4;
// TMPro.Examples.TextMeshProFloatingText
struct TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1;
// UnityEngine.Texture
struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE;
// TimerDelegate
struct TimerDelegate_t34BCC9B9964D8BA67E6A87849C74455F86AFA1DB;
// UnityEngine.UI.Toggle
struct Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E;
// UnityEngine.UI.ToggleGroup
struct ToggleGroup_t12E1DFDEB3FFD979A20299EE42A94388AC619C95;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// TMPro.Examples.VertexColorCycler
struct VertexColorCycler_t37E80E87D8EAD0D7757CDA45BD5D3AA82FD28374;
// TMPro.Examples.VertexJitter
struct VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A;
// TMPro.Examples.VertexShakeA
struct VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A;
// TMPro.Examples.VertexShakeB
struct VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9;
// TMPro.Examples.VertexZoom
struct VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// TMPro.Examples.WarpTextExample
struct WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96;
// LTDescr/ActionMethodDelegate
struct ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05;
// LTDescr/EaseTypeDelegate
struct EaseTypeDelegate_t0285AB55B28F5B5A6688E966B51FBA837B2E84AF;
// ScrollSnapBase/SelectionChangeEndEvent
struct SelectionChangeEndEvent_t61026CA54A4A81045E6FA5F1D8C79E556527C29E;
// ScrollSnapBase/SelectionChangeStartEvent
struct SelectionChangeStartEvent_tE0687452817ACBB2DEECBBF8D2E996D0137F7FE0;
// ScrollSnapBase/SelectionPageChangedEvent
struct SelectionPageChangedEvent_tB33544091EB61DE45C9FF3A71E4DA084BDAA6DBC;
// TMPro.TMP_TextEventHandler/CharacterSelectionEvent
struct CharacterSelectionEvent_t3F24E19931D51E4616A11C4D690BC0B3D738D64F;
// TMPro.TMP_TextEventHandler/LineSelectionEvent
struct LineSelectionEvent_tBD0FAC01BF9DE63FE60817F0B760E24E951C4712;
// TMPro.TMP_TextEventHandler/LinkSelectionEvent
struct LinkSelectionEvent_tDDEFCE74540F682E52A3445C536D3F29A91F98AF;
// TMPro.TMP_TextEventHandler/SpriteSelectionEvent
struct SpriteSelectionEvent_tAF7524A6DF6D4849E206726E3AC085BCB91B4618;
// TMPro.TMP_TextEventHandler/WordSelectionEvent
struct WordSelectionEvent_t3FE09C7D7E726DC634003A28E2B3CB78B42AE6F8;
// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0
struct U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F;
// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0
struct U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F;
// TMPro.Examples.VertexZoom/<>c__DisplayClass10_0
struct U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// CS2DBoolArray
struct CS2DBoolArray_t4A5EA41B80365341FAB8F7B222392F23F5083434  : public RuntimeObject
{
public:
	// CS2DBoolArray/L2D[] CS2DBoolArray::l0
	L2DU5BU5D_tDA306A5D580822CBB99C992A56CE5A22280374EA* ___l0_0;

public:
	inline static int32_t get_offset_of_l0_0() { return static_cast<int32_t>(offsetof(CS2DBoolArray_t4A5EA41B80365341FAB8F7B222392F23F5083434, ___l0_0)); }
	inline L2DU5BU5D_tDA306A5D580822CBB99C992A56CE5A22280374EA* get_l0_0() const { return ___l0_0; }
	inline L2DU5BU5D_tDA306A5D580822CBB99C992A56CE5A22280374EA** get_address_of_l0_0() { return &___l0_0; }
	inline void set_l0_0(L2DU5BU5D_tDA306A5D580822CBB99C992A56CE5A22280374EA* value)
	{
		___l0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___l0_0), (void*)value);
	}
};


// CSAnimationData
struct CSAnimationData_t39AC7CF1C6BE740E5450F413840F4B8E08309645  : public RuntimeObject
{
public:
	// UnityEngine.Sprite[] CSAnimationData::frames
	SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* ___frames_0;
	// System.Single CSAnimationData::duration
	float ___duration_1;
	// System.Single CSAnimationData::delay
	float ___delay_2;

public:
	inline static int32_t get_offset_of_frames_0() { return static_cast<int32_t>(offsetof(CSAnimationData_t39AC7CF1C6BE740E5450F413840F4B8E08309645, ___frames_0)); }
	inline SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* get_frames_0() const { return ___frames_0; }
	inline SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77** get_address_of_frames_0() { return &___frames_0; }
	inline void set_frames_0(SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* value)
	{
		___frames_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___frames_0), (void*)value);
	}

	inline static int32_t get_offset_of_duration_1() { return static_cast<int32_t>(offsetof(CSAnimationData_t39AC7CF1C6BE740E5450F413840F4B8E08309645, ___duration_1)); }
	inline float get_duration_1() const { return ___duration_1; }
	inline float* get_address_of_duration_1() { return &___duration_1; }
	inline void set_duration_1(float value)
	{
		___duration_1 = value;
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(CSAnimationData_t39AC7CF1C6BE740E5450F413840F4B8E08309645, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}
};


// CSCardList2D
struct CSCardList2D_tB265A74FEF9F4D8A544A88653A96C54F1F5B66F5  : public RuntimeObject
{
public:
	// CSCardList2D/List2D[] CSCardList2D::l0
	List2DU5BU5D_t19A5AD69DAAFF117D4A6DBF4629C88A7B1CCFFAE* ___l0_0;

public:
	inline static int32_t get_offset_of_l0_0() { return static_cast<int32_t>(offsetof(CSCardList2D_tB265A74FEF9F4D8A544A88653A96C54F1F5B66F5, ___l0_0)); }
	inline List2DU5BU5D_t19A5AD69DAAFF117D4A6DBF4629C88A7B1CCFFAE* get_l0_0() const { return ___l0_0; }
	inline List2DU5BU5D_t19A5AD69DAAFF117D4A6DBF4629C88A7B1CCFFAE** get_address_of_l0_0() { return &___l0_0; }
	inline void set_l0_0(List2DU5BU5D_t19A5AD69DAAFF117D4A6DBF4629C88A7B1CCFFAE* value)
	{
		___l0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___l0_0), (void*)value);
	}
};


// CSCell
struct CSCell_tEC2CF7F1268B4BE5C1155F2B9476449C3A14383D  : public RuntimeObject
{
public:
	// System.Int32 CSCell::column
	int32_t ___column_0;
	// System.Int32 CSCell::row
	int32_t ___row_1;

public:
	inline static int32_t get_offset_of_column_0() { return static_cast<int32_t>(offsetof(CSCell_tEC2CF7F1268B4BE5C1155F2B9476449C3A14383D, ___column_0)); }
	inline int32_t get_column_0() const { return ___column_0; }
	inline int32_t* get_address_of_column_0() { return &___column_0; }
	inline void set_column_0(int32_t value)
	{
		___column_0 = value;
	}

	inline static int32_t get_offset_of_row_1() { return static_cast<int32_t>(offsetof(CSCell_tEC2CF7F1268B4BE5C1155F2B9476449C3A14383D, ___row_1)); }
	inline int32_t get_row_1() const { return ___row_1; }
	inline int32_t* get_address_of_row_1() { return &___row_1; }
	inline void set_row_1(int32_t value)
	{
		___row_1 = value;
	}
};


// CSReelRandom
struct CSReelRandom_t16B21B9455802008C805A265B104CD5F2D02423B  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<CSSymbolPercent> CSReelRandom::symbols
	List_1_tD670094CFCCB25276B57801E2F067BFF969BE09D * ___symbols_0;
	// System.Single CSReelRandom::_totalChance
	float ____totalChance_1;

public:
	inline static int32_t get_offset_of_symbols_0() { return static_cast<int32_t>(offsetof(CSReelRandom_t16B21B9455802008C805A265B104CD5F2D02423B, ___symbols_0)); }
	inline List_1_tD670094CFCCB25276B57801E2F067BFF969BE09D * get_symbols_0() const { return ___symbols_0; }
	inline List_1_tD670094CFCCB25276B57801E2F067BFF969BE09D ** get_address_of_symbols_0() { return &___symbols_0; }
	inline void set_symbols_0(List_1_tD670094CFCCB25276B57801E2F067BFF969BE09D * value)
	{
		___symbols_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___symbols_0), (void*)value);
	}

	inline static int32_t get_offset_of__totalChance_1() { return static_cast<int32_t>(offsetof(CSReelRandom_t16B21B9455802008C805A265B104CD5F2D02423B, ____totalChance_1)); }
	inline float get__totalChance_1() const { return ____totalChance_1; }
	inline float* get_address_of__totalChance_1() { return &____totalChance_1; }
	inline void set__totalChance_1(float value)
	{
		____totalChance_1 = value;
	}
};


// CSRule
struct CSRule_t57A6472E5A907AC0E251BCF987970B41AD1F0561  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Single> CSRule::reward
	List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * ___reward_0;
	// System.Boolean CSRule::substitutesForWild
	bool ___substitutesForWild_1;

public:
	inline static int32_t get_offset_of_reward_0() { return static_cast<int32_t>(offsetof(CSRule_t57A6472E5A907AC0E251BCF987970B41AD1F0561, ___reward_0)); }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * get_reward_0() const { return ___reward_0; }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA ** get_address_of_reward_0() { return &___reward_0; }
	inline void set_reward_0(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * value)
	{
		___reward_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reward_0), (void*)value);
	}

	inline static int32_t get_offset_of_substitutesForWild_1() { return static_cast<int32_t>(offsetof(CSRule_t57A6472E5A907AC0E251BCF987970B41AD1F0561, ___substitutesForWild_1)); }
	inline bool get_substitutesForWild_1() const { return ___substitutesForWild_1; }
	inline bool* get_address_of_substitutesForWild_1() { return &___substitutesForWild_1; }
	inline void set_substitutesForWild_1(bool value)
	{
		___substitutesForWild_1 = value;
	}
};


// CSTime
struct CSTime_t36D0CAEE19AE1D72C0043917C9A62A2BA4BA5DA8  : public RuntimeObject
{
public:

public:
};


// CSUtilities
struct CSUtilities_tD1C88ACE4AB98642C64583DC0994B1341397001E  : public RuntimeObject
{
public:

public:
};

struct CSUtilities_tD1C88ACE4AB98642C64583DC0994B1341397001E_StaticFields
{
public:
	// System.Single CSUtilities::min
	float ___min_0;
	// System.Int32 CSUtilities::idx
	int32_t ___idx_1;
	// System.String[] CSUtilities::tokens
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___tokens_2;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(CSUtilities_tD1C88ACE4AB98642C64583DC0994B1341397001E_StaticFields, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_idx_1() { return static_cast<int32_t>(offsetof(CSUtilities_tD1C88ACE4AB98642C64583DC0994B1341397001E_StaticFields, ___idx_1)); }
	inline int32_t get_idx_1() const { return ___idx_1; }
	inline int32_t* get_address_of_idx_1() { return &___idx_1; }
	inline void set_idx_1(int32_t value)
	{
		___idx_1 = value;
	}

	inline static int32_t get_offset_of_tokens_2() { return static_cast<int32_t>(offsetof(CSUtilities_tD1C88ACE4AB98642C64583DC0994B1341397001E_StaticFields, ___tokens_2)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_tokens_2() const { return ___tokens_2; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_tokens_2() { return &___tokens_2; }
	inline void set_tokens_2(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___tokens_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tokens_2), (void*)value);
	}
};


// LTBezierPath
struct LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] LTBezierPath::pts
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___pts_0;
	// System.Single LTBezierPath::length
	float ___length_1;
	// System.Boolean LTBezierPath::orientToPath
	bool ___orientToPath_2;
	// System.Boolean LTBezierPath::orientToPath2d
	bool ___orientToPath2d_3;
	// LTBezier[] LTBezierPath::beziers
	LTBezierU5BU5D_t4DDB3352316B2B476AEEE5AB4F760513856FC153* ___beziers_4;
	// System.Single[] LTBezierPath::lengthRatio
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___lengthRatio_5;
	// System.Int32 LTBezierPath::currentBezier
	int32_t ___currentBezier_6;
	// System.Int32 LTBezierPath::previousBezier
	int32_t ___previousBezier_7;

public:
	inline static int32_t get_offset_of_pts_0() { return static_cast<int32_t>(offsetof(LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48, ___pts_0)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_pts_0() const { return ___pts_0; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_pts_0() { return &___pts_0; }
	inline void set_pts_0(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___pts_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pts_0), (void*)value);
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48, ___length_1)); }
	inline float get_length_1() const { return ___length_1; }
	inline float* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(float value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_orientToPath_2() { return static_cast<int32_t>(offsetof(LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48, ___orientToPath_2)); }
	inline bool get_orientToPath_2() const { return ___orientToPath_2; }
	inline bool* get_address_of_orientToPath_2() { return &___orientToPath_2; }
	inline void set_orientToPath_2(bool value)
	{
		___orientToPath_2 = value;
	}

	inline static int32_t get_offset_of_orientToPath2d_3() { return static_cast<int32_t>(offsetof(LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48, ___orientToPath2d_3)); }
	inline bool get_orientToPath2d_3() const { return ___orientToPath2d_3; }
	inline bool* get_address_of_orientToPath2d_3() { return &___orientToPath2d_3; }
	inline void set_orientToPath2d_3(bool value)
	{
		___orientToPath2d_3 = value;
	}

	inline static int32_t get_offset_of_beziers_4() { return static_cast<int32_t>(offsetof(LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48, ___beziers_4)); }
	inline LTBezierU5BU5D_t4DDB3352316B2B476AEEE5AB4F760513856FC153* get_beziers_4() const { return ___beziers_4; }
	inline LTBezierU5BU5D_t4DDB3352316B2B476AEEE5AB4F760513856FC153** get_address_of_beziers_4() { return &___beziers_4; }
	inline void set_beziers_4(LTBezierU5BU5D_t4DDB3352316B2B476AEEE5AB4F760513856FC153* value)
	{
		___beziers_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___beziers_4), (void*)value);
	}

	inline static int32_t get_offset_of_lengthRatio_5() { return static_cast<int32_t>(offsetof(LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48, ___lengthRatio_5)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_lengthRatio_5() const { return ___lengthRatio_5; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_lengthRatio_5() { return &___lengthRatio_5; }
	inline void set_lengthRatio_5(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___lengthRatio_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lengthRatio_5), (void*)value);
	}

	inline static int32_t get_offset_of_currentBezier_6() { return static_cast<int32_t>(offsetof(LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48, ___currentBezier_6)); }
	inline int32_t get_currentBezier_6() const { return ___currentBezier_6; }
	inline int32_t* get_address_of_currentBezier_6() { return &___currentBezier_6; }
	inline void set_currentBezier_6(int32_t value)
	{
		___currentBezier_6 = value;
	}

	inline static int32_t get_offset_of_previousBezier_7() { return static_cast<int32_t>(offsetof(LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48, ___previousBezier_7)); }
	inline int32_t get_previousBezier_7() const { return ___previousBezier_7; }
	inline int32_t* get_address_of_previousBezier_7() { return &___previousBezier_7; }
	inline void set_previousBezier_7(int32_t value)
	{
		___previousBezier_7 = value;
	}
};


// LTEvent
struct LTEvent_tFD435CD71D73BB5C8DBB11E20B4E3EF3ECC428EA  : public RuntimeObject
{
public:
	// System.Int32 LTEvent::id
	int32_t ___id_0;
	// System.Object LTEvent::data
	RuntimeObject * ___data_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(LTEvent_tFD435CD71D73BB5C8DBB11E20B4E3EF3ECC428EA, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(LTEvent_tFD435CD71D73BB5C8DBB11E20B4E3EF3ECC428EA, ___data_1)); }
	inline RuntimeObject * get_data_1() const { return ___data_1; }
	inline RuntimeObject ** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(RuntimeObject * value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_1), (void*)value);
	}
};


// LTSeq
struct LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222  : public RuntimeObject
{
public:
	// LTSeq LTSeq::previous
	LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222 * ___previous_0;
	// LTSeq LTSeq::current
	LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222 * ___current_1;
	// LTDescr LTSeq::tween
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___tween_2;
	// System.Single LTSeq::totalDelay
	float ___totalDelay_3;
	// System.Single LTSeq::timeScale
	float ___timeScale_4;
	// System.Int32 LTSeq::debugIter
	int32_t ___debugIter_5;
	// System.UInt32 LTSeq::counter
	uint32_t ___counter_6;
	// System.Boolean LTSeq::toggle
	bool ___toggle_7;
	// System.UInt32 LTSeq::_id
	uint32_t ____id_8;

public:
	inline static int32_t get_offset_of_previous_0() { return static_cast<int32_t>(offsetof(LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222, ___previous_0)); }
	inline LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222 * get_previous_0() const { return ___previous_0; }
	inline LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222 ** get_address_of_previous_0() { return &___previous_0; }
	inline void set_previous_0(LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222 * value)
	{
		___previous_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___previous_0), (void*)value);
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222, ___current_1)); }
	inline LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222 * get_current_1() const { return ___current_1; }
	inline LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222 ** get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222 * value)
	{
		___current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_1), (void*)value);
	}

	inline static int32_t get_offset_of_tween_2() { return static_cast<int32_t>(offsetof(LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222, ___tween_2)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_tween_2() const { return ___tween_2; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_tween_2() { return &___tween_2; }
	inline void set_tween_2(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___tween_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tween_2), (void*)value);
	}

	inline static int32_t get_offset_of_totalDelay_3() { return static_cast<int32_t>(offsetof(LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222, ___totalDelay_3)); }
	inline float get_totalDelay_3() const { return ___totalDelay_3; }
	inline float* get_address_of_totalDelay_3() { return &___totalDelay_3; }
	inline void set_totalDelay_3(float value)
	{
		___totalDelay_3 = value;
	}

	inline static int32_t get_offset_of_timeScale_4() { return static_cast<int32_t>(offsetof(LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222, ___timeScale_4)); }
	inline float get_timeScale_4() const { return ___timeScale_4; }
	inline float* get_address_of_timeScale_4() { return &___timeScale_4; }
	inline void set_timeScale_4(float value)
	{
		___timeScale_4 = value;
	}

	inline static int32_t get_offset_of_debugIter_5() { return static_cast<int32_t>(offsetof(LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222, ___debugIter_5)); }
	inline int32_t get_debugIter_5() const { return ___debugIter_5; }
	inline int32_t* get_address_of_debugIter_5() { return &___debugIter_5; }
	inline void set_debugIter_5(int32_t value)
	{
		___debugIter_5 = value;
	}

	inline static int32_t get_offset_of_counter_6() { return static_cast<int32_t>(offsetof(LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222, ___counter_6)); }
	inline uint32_t get_counter_6() const { return ___counter_6; }
	inline uint32_t* get_address_of_counter_6() { return &___counter_6; }
	inline void set_counter_6(uint32_t value)
	{
		___counter_6 = value;
	}

	inline static int32_t get_offset_of_toggle_7() { return static_cast<int32_t>(offsetof(LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222, ___toggle_7)); }
	inline bool get_toggle_7() const { return ___toggle_7; }
	inline bool* get_address_of_toggle_7() { return &___toggle_7; }
	inline void set_toggle_7(bool value)
	{
		___toggle_7 = value;
	}

	inline static int32_t get_offset_of__id_8() { return static_cast<int32_t>(offsetof(LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222, ____id_8)); }
	inline uint32_t get__id_8() const { return ____id_8; }
	inline uint32_t* get_address_of__id_8() { return &____id_8; }
	inline void set__id_8(uint32_t value)
	{
		____id_8 = value;
	}
};


// LTSpline
struct LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701  : public RuntimeObject
{
public:
	// System.Single LTSpline::distance
	float ___distance_2;
	// System.Boolean LTSpline::constantSpeed
	bool ___constantSpeed_3;
	// UnityEngine.Vector3[] LTSpline::pts
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___pts_4;
	// UnityEngine.Vector3[] LTSpline::ptsAdj
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___ptsAdj_5;
	// System.Int32 LTSpline::ptsAdjLength
	int32_t ___ptsAdjLength_6;
	// System.Boolean LTSpline::orientToPath
	bool ___orientToPath_7;
	// System.Boolean LTSpline::orientToPath2d
	bool ___orientToPath2d_8;
	// System.Int32 LTSpline::numSections
	int32_t ___numSections_9;
	// System.Int32 LTSpline::currPt
	int32_t ___currPt_10;

public:
	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_constantSpeed_3() { return static_cast<int32_t>(offsetof(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701, ___constantSpeed_3)); }
	inline bool get_constantSpeed_3() const { return ___constantSpeed_3; }
	inline bool* get_address_of_constantSpeed_3() { return &___constantSpeed_3; }
	inline void set_constantSpeed_3(bool value)
	{
		___constantSpeed_3 = value;
	}

	inline static int32_t get_offset_of_pts_4() { return static_cast<int32_t>(offsetof(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701, ___pts_4)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_pts_4() const { return ___pts_4; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_pts_4() { return &___pts_4; }
	inline void set_pts_4(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___pts_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pts_4), (void*)value);
	}

	inline static int32_t get_offset_of_ptsAdj_5() { return static_cast<int32_t>(offsetof(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701, ___ptsAdj_5)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_ptsAdj_5() const { return ___ptsAdj_5; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_ptsAdj_5() { return &___ptsAdj_5; }
	inline void set_ptsAdj_5(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___ptsAdj_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ptsAdj_5), (void*)value);
	}

	inline static int32_t get_offset_of_ptsAdjLength_6() { return static_cast<int32_t>(offsetof(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701, ___ptsAdjLength_6)); }
	inline int32_t get_ptsAdjLength_6() const { return ___ptsAdjLength_6; }
	inline int32_t* get_address_of_ptsAdjLength_6() { return &___ptsAdjLength_6; }
	inline void set_ptsAdjLength_6(int32_t value)
	{
		___ptsAdjLength_6 = value;
	}

	inline static int32_t get_offset_of_orientToPath_7() { return static_cast<int32_t>(offsetof(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701, ___orientToPath_7)); }
	inline bool get_orientToPath_7() const { return ___orientToPath_7; }
	inline bool* get_address_of_orientToPath_7() { return &___orientToPath_7; }
	inline void set_orientToPath_7(bool value)
	{
		___orientToPath_7 = value;
	}

	inline static int32_t get_offset_of_orientToPath2d_8() { return static_cast<int32_t>(offsetof(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701, ___orientToPath2d_8)); }
	inline bool get_orientToPath2d_8() const { return ___orientToPath2d_8; }
	inline bool* get_address_of_orientToPath2d_8() { return &___orientToPath2d_8; }
	inline void set_orientToPath2d_8(bool value)
	{
		___orientToPath2d_8 = value;
	}

	inline static int32_t get_offset_of_numSections_9() { return static_cast<int32_t>(offsetof(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701, ___numSections_9)); }
	inline int32_t get_numSections_9() const { return ___numSections_9; }
	inline int32_t* get_address_of_numSections_9() { return &___numSections_9; }
	inline void set_numSections_9(int32_t value)
	{
		___numSections_9 = value;
	}

	inline static int32_t get_offset_of_currPt_10() { return static_cast<int32_t>(offsetof(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701, ___currPt_10)); }
	inline int32_t get_currPt_10() const { return ___currPt_10; }
	inline int32_t* get_address_of_currPt_10() { return &___currPt_10; }
	inline void set_currPt_10(int32_t value)
	{
		___currPt_10 = value;
	}
};

struct LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701_StaticFields
{
public:
	// System.Int32 LTSpline::DISTANCE_COUNT
	int32_t ___DISTANCE_COUNT_0;
	// System.Int32 LTSpline::SUBLINE_COUNT
	int32_t ___SUBLINE_COUNT_1;

public:
	inline static int32_t get_offset_of_DISTANCE_COUNT_0() { return static_cast<int32_t>(offsetof(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701_StaticFields, ___DISTANCE_COUNT_0)); }
	inline int32_t get_DISTANCE_COUNT_0() const { return ___DISTANCE_COUNT_0; }
	inline int32_t* get_address_of_DISTANCE_COUNT_0() { return &___DISTANCE_COUNT_0; }
	inline void set_DISTANCE_COUNT_0(int32_t value)
	{
		___DISTANCE_COUNT_0 = value;
	}

	inline static int32_t get_offset_of_SUBLINE_COUNT_1() { return static_cast<int32_t>(offsetof(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701_StaticFields, ___SUBLINE_COUNT_1)); }
	inline int32_t get_SUBLINE_COUNT_1() const { return ___SUBLINE_COUNT_1; }
	inline int32_t* get_address_of_SUBLINE_COUNT_1() { return &___SUBLINE_COUNT_1; }
	inline void set_SUBLINE_COUNT_1(int32_t value)
	{
		___SUBLINE_COUNT_1 = value;
	}
};


// LTUtility
struct LTUtility_tD1A4A8590D2B9A3EB56F29821CACD7B41905503B  : public RuntimeObject
{
public:

public:
};


// DentedPixel.LeanDummy
struct LeanDummy_tB8BEBC768133908A0553CDD8C0E763705722BC5E  : public RuntimeObject
{
public:

public:
};


// LeanSmooth
struct LeanSmooth_t4E1B1AAA58DAE2327DB5E72D257C5E325E252A09  : public RuntimeObject
{
public:

public:
};


// LeanTest
struct LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F  : public RuntimeObject
{
public:

public:
};

struct LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields
{
public:
	// System.Int32 LeanTest::expected
	int32_t ___expected_0;
	// System.Int32 LeanTest::tests
	int32_t ___tests_1;
	// System.Int32 LeanTest::passes
	int32_t ___passes_2;
	// System.Single LeanTest::timeout
	float ___timeout_3;
	// System.Boolean LeanTest::timeoutStarted
	bool ___timeoutStarted_4;
	// System.Boolean LeanTest::testsFinished
	bool ___testsFinished_5;

public:
	inline static int32_t get_offset_of_expected_0() { return static_cast<int32_t>(offsetof(LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields, ___expected_0)); }
	inline int32_t get_expected_0() const { return ___expected_0; }
	inline int32_t* get_address_of_expected_0() { return &___expected_0; }
	inline void set_expected_0(int32_t value)
	{
		___expected_0 = value;
	}

	inline static int32_t get_offset_of_tests_1() { return static_cast<int32_t>(offsetof(LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields, ___tests_1)); }
	inline int32_t get_tests_1() const { return ___tests_1; }
	inline int32_t* get_address_of_tests_1() { return &___tests_1; }
	inline void set_tests_1(int32_t value)
	{
		___tests_1 = value;
	}

	inline static int32_t get_offset_of_passes_2() { return static_cast<int32_t>(offsetof(LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields, ___passes_2)); }
	inline int32_t get_passes_2() const { return ___passes_2; }
	inline int32_t* get_address_of_passes_2() { return &___passes_2; }
	inline void set_passes_2(int32_t value)
	{
		___passes_2 = value;
	}

	inline static int32_t get_offset_of_timeout_3() { return static_cast<int32_t>(offsetof(LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields, ___timeout_3)); }
	inline float get_timeout_3() const { return ___timeout_3; }
	inline float* get_address_of_timeout_3() { return &___timeout_3; }
	inline void set_timeout_3(float value)
	{
		___timeout_3 = value;
	}

	inline static int32_t get_offset_of_timeoutStarted_4() { return static_cast<int32_t>(offsetof(LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields, ___timeoutStarted_4)); }
	inline bool get_timeoutStarted_4() const { return ___timeoutStarted_4; }
	inline bool* get_address_of_timeoutStarted_4() { return &___timeoutStarted_4; }
	inline void set_timeoutStarted_4(bool value)
	{
		___timeoutStarted_4 = value;
	}

	inline static int32_t get_offset_of_testsFinished_5() { return static_cast<int32_t>(offsetof(LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields, ___testsFinished_5)); }
	inline bool get_testsFinished_5() const { return ___testsFinished_5; }
	inline bool* get_address_of_testsFinished_5() { return &___testsFinished_5; }
	inline void set_testsFinished_5(bool value)
	{
		___testsFinished_5 = value;
	}
};


// LeanTweenExt
struct LeanTweenExt_t115D1289C8F5FA55D2B2CEA08AC2EA4312DFD414  : public RuntimeObject
{
public:

public:
};


// Sound
struct Sound_tF983595F9C621A86B56E05F9778810369E90A0FE  : public RuntimeObject
{
public:
	// UnityEngine.AudioClip Sound::clip
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___clip_0;
	// System.Boolean Sound::loop
	bool ___loop_1;
	// UnityEngine.AudioSource Sound::source
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___source_2;
	// System.Single Sound::volume
	float ___volume_3;

public:
	inline static int32_t get_offset_of_clip_0() { return static_cast<int32_t>(offsetof(Sound_tF983595F9C621A86B56E05F9778810369E90A0FE, ___clip_0)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get_clip_0() const { return ___clip_0; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of_clip_0() { return &___clip_0; }
	inline void set_clip_0(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		___clip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___clip_0), (void*)value);
	}

	inline static int32_t get_offset_of_loop_1() { return static_cast<int32_t>(offsetof(Sound_tF983595F9C621A86B56E05F9778810369E90A0FE, ___loop_1)); }
	inline bool get_loop_1() const { return ___loop_1; }
	inline bool* get_address_of_loop_1() { return &___loop_1; }
	inline void set_loop_1(bool value)
	{
		___loop_1 = value;
	}

	inline static int32_t get_offset_of_source_2() { return static_cast<int32_t>(offsetof(Sound_tF983595F9C621A86B56E05F9778810369E90A0FE, ___source_2)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_source_2() const { return ___source_2; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_source_2() { return &___source_2; }
	inline void set_source_2(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___source_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_2), (void*)value);
	}

	inline static int32_t get_offset_of_volume_3() { return static_cast<int32_t>(offsetof(Sound_tF983595F9C621A86B56E05F9778810369E90A0FE, ___volume_3)); }
	inline float get_volume_3() const { return ___volume_3; }
	inline float* get_address_of_volume_3() { return &___volume_3; }
	inline void set_volume_3(float value)
	{
		___volume_3 = value;
	}
};


// UnityEngine.Events.UnityEventBase
struct UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_Calls_0)); }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// TMPro.Examples.Benchmark01/<Start>d__10
struct U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.Benchmark01/<Start>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.Benchmark01/<Start>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.Examples.Benchmark01 TMPro.Examples.Benchmark01/<Start>d__10::<>4__this
	Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934 * ___U3CU3E4__this_2;
	// System.Int32 TMPro.Examples.Benchmark01/<Start>d__10::<i>5__2
	int32_t ___U3CiU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640, ___U3CU3E4__this_2)); }
	inline Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640, ___U3CiU3E5__2_3)); }
	inline int32_t get_U3CiU3E5__2_3() const { return ___U3CiU3E5__2_3; }
	inline int32_t* get_address_of_U3CiU3E5__2_3() { return &___U3CiU3E5__2_3; }
	inline void set_U3CiU3E5__2_3(int32_t value)
	{
		___U3CiU3E5__2_3 = value;
	}
};


// TMPro.Examples.Benchmark01_UGUI/<Start>d__10
struct U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI/<Start>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.Examples.Benchmark01_UGUI TMPro.Examples.Benchmark01_UGUI/<Start>d__10::<>4__this
	Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B * ___U3CU3E4__this_2;
	// System.Int32 TMPro.Examples.Benchmark01_UGUI/<Start>d__10::<i>5__2
	int32_t ___U3CiU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3, ___U3CU3E4__this_2)); }
	inline Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3, ___U3CiU3E5__2_3)); }
	inline int32_t get_U3CiU3E5__2_3() const { return ___U3CiU3E5__2_3; }
	inline int32_t* get_address_of_U3CiU3E5__2_3() { return &___U3CiU3E5__2_3; }
	inline void set_U3CiU3E5__2_3(int32_t value)
	{
		___U3CiU3E5__2_3 = value;
	}
};


// CSAdMobManager/<>c
struct U3CU3Ec_t9179B94870D58F4201DEB8D7A718495A50CC068D  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t9179B94870D58F4201DEB8D7A718495A50CC068D_StaticFields
{
public:
	// CSAdMobManager/<>c CSAdMobManager/<>c::<>9
	U3CU3Ec_t9179B94870D58F4201DEB8D7A718495A50CC068D * ___U3CU3E9_0;
	// System.Action`1<GoogleMobileAds.Api.InitializationStatus> CSAdMobManager/<>c::<>9__15_0
	Action_1_t73B690804B71062EA366B9EF731317DA6FE13907 * ___U3CU3E9__15_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9179B94870D58F4201DEB8D7A718495A50CC068D_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t9179B94870D58F4201DEB8D7A718495A50CC068D * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t9179B94870D58F4201DEB8D7A718495A50CC068D ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t9179B94870D58F4201DEB8D7A718495A50CC068D * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__15_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9179B94870D58F4201DEB8D7A718495A50CC068D_StaticFields, ___U3CU3E9__15_0_1)); }
	inline Action_1_t73B690804B71062EA366B9EF731317DA6FE13907 * get_U3CU3E9__15_0_1() const { return ___U3CU3E9__15_0_1; }
	inline Action_1_t73B690804B71062EA366B9EF731317DA6FE13907 ** get_address_of_U3CU3E9__15_0_1() { return &___U3CU3E9__15_0_1; }
	inline void set_U3CU3E9__15_0_1(Action_1_t73B690804B71062EA366B9EF731317DA6FE13907 * value)
	{
		___U3CU3E9__15_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__15_0_1), (void*)value);
	}
};


// CSAlert/<>c__DisplayClass19_0
struct U3CU3Ec__DisplayClass19_0_t8F539BCA90B9E5930ECA2AC7790897B05CD50FC7  : public RuntimeObject
{
public:
	// CSAlert CSAlert/<>c__DisplayClass19_0::<>4__this
	CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A * ___U3CU3E4__this_0;
	// System.Action CSAlert/<>c__DisplayClass19_0::callback
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___callback_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_t8F539BCA90B9E5930ECA2AC7790897B05CD50FC7, ___U3CU3E4__this_0)); }
	inline CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_t8F539BCA90B9E5930ECA2AC7790897B05CD50FC7, ___callback_1)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_callback_1() const { return ___callback_1; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callback_1), (void*)value);
	}
};


// CSAlert/<>c__DisplayClass21_0
struct U3CU3Ec__DisplayClass21_0_tC1A4F6BBD31A2498ECF79C6D507777B1C8612889  : public RuntimeObject
{
public:
	// CSAlert CSAlert/<>c__DisplayClass21_0::<>4__this
	CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A * ___U3CU3E4__this_0;
	// System.Action CSAlert/<>c__DisplayClass21_0::callback
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___callback_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tC1A4F6BBD31A2498ECF79C6D507777B1C8612889, ___U3CU3E4__this_0)); }
	inline CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tC1A4F6BBD31A2498ECF79C6D507777B1C8612889, ___callback_1)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_callback_1() const { return ___callback_1; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callback_1), (void*)value);
	}
};


// CSBankCoinPanel/<>c__DisplayClass18_0
struct U3CU3Ec__DisplayClass18_0_t880A5064739A67D17371896B595087DF248DCBB7  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text CSBankCoinPanel/<>c__DisplayClass18_0::label
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___label_0;
	// CSBankCoinPanel CSBankCoinPanel/<>c__DisplayClass18_0::<>4__this
	CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_label_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t880A5064739A67D17371896B595087DF248DCBB7, ___label_0)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_label_0() const { return ___label_0; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_label_0() { return &___label_0; }
	inline void set_label_0(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___label_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___label_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t880A5064739A67D17371896B595087DF248DCBB7, ___U3CU3E4__this_1)); }
	inline CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}
};


// CSButtonTap/<>c
struct U3CU3Ec_t6072D81B91FC0BA7572310D528126455DE9C73D8  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t6072D81B91FC0BA7572310D528126455DE9C73D8_StaticFields
{
public:
	// CSButtonTap/<>c CSButtonTap/<>c::<>9
	U3CU3Ec_t6072D81B91FC0BA7572310D528126455DE9C73D8 * ___U3CU3E9_0;
	// UnityEngine.Events.UnityAction CSButtonTap/<>c::<>9__0_0
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___U3CU3E9__0_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6072D81B91FC0BA7572310D528126455DE9C73D8_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t6072D81B91FC0BA7572310D528126455DE9C73D8 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t6072D81B91FC0BA7572310D528126455DE9C73D8 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t6072D81B91FC0BA7572310D528126455DE9C73D8 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6072D81B91FC0BA7572310D528126455DE9C73D8_StaticFields, ___U3CU3E9__0_0_1)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__0_0_1), (void*)value);
	}
};


// CSCSLoading/<>c
struct U3CU3Ec_tE219C1852668FC6947326874826AFCD61DFDD86B  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tE219C1852668FC6947326874826AFCD61DFDD86B_StaticFields
{
public:
	// CSCSLoading/<>c CSCSLoading/<>c::<>9
	U3CU3Ec_tE219C1852668FC6947326874826AFCD61DFDD86B * ___U3CU3E9_0;
	// System.Action`2<System.Single,System.Boolean> CSCSLoading/<>c::<>9__4_1
	Action_2_tBF5D9EB992F91A3DA045CAF342A5DE18B805F91D * ___U3CU3E9__4_1_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE219C1852668FC6947326874826AFCD61DFDD86B_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tE219C1852668FC6947326874826AFCD61DFDD86B * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tE219C1852668FC6947326874826AFCD61DFDD86B ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tE219C1852668FC6947326874826AFCD61DFDD86B * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE219C1852668FC6947326874826AFCD61DFDD86B_StaticFields, ___U3CU3E9__4_1_1)); }
	inline Action_2_tBF5D9EB992F91A3DA045CAF342A5DE18B805F91D * get_U3CU3E9__4_1_1() const { return ___U3CU3E9__4_1_1; }
	inline Action_2_tBF5D9EB992F91A3DA045CAF342A5DE18B805F91D ** get_address_of_U3CU3E9__4_1_1() { return &___U3CU3E9__4_1_1; }
	inline void set_U3CU3E9__4_1_1(Action_2_tBF5D9EB992F91A3DA045CAF342A5DE18B805F91D * value)
	{
		___U3CU3E9__4_1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__4_1_1), (void*)value);
	}
};


// CSExperiencePanel/<>c__DisplayClass21_0
struct U3CU3Ec__DisplayClass21_0_tFE6AAC70CEF4F120217A108769FDB473CF53C25E  : public RuntimeObject
{
public:
	// System.Single CSExperiencePanel/<>c__DisplayClass21_0::v
	float ___v_0;
	// System.Single CSExperiencePanel/<>c__DisplayClass21_0::prev
	float ___prev_1;
	// System.Single CSExperiencePanel/<>c__DisplayClass21_0::m
	float ___m_2;
	// System.Int32 CSExperiencePanel/<>c__DisplayClass21_0::l
	int32_t ___l_3;
	// CSExperiencePanel CSExperiencePanel/<>c__DisplayClass21_0::<>4__this
	CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F * ___U3CU3E4__this_4;
	// System.Int32 CSExperiencePanel/<>c__DisplayClass21_0::curLevel
	int32_t ___curLevel_5;

public:
	inline static int32_t get_offset_of_v_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tFE6AAC70CEF4F120217A108769FDB473CF53C25E, ___v_0)); }
	inline float get_v_0() const { return ___v_0; }
	inline float* get_address_of_v_0() { return &___v_0; }
	inline void set_v_0(float value)
	{
		___v_0 = value;
	}

	inline static int32_t get_offset_of_prev_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tFE6AAC70CEF4F120217A108769FDB473CF53C25E, ___prev_1)); }
	inline float get_prev_1() const { return ___prev_1; }
	inline float* get_address_of_prev_1() { return &___prev_1; }
	inline void set_prev_1(float value)
	{
		___prev_1 = value;
	}

	inline static int32_t get_offset_of_m_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tFE6AAC70CEF4F120217A108769FDB473CF53C25E, ___m_2)); }
	inline float get_m_2() const { return ___m_2; }
	inline float* get_address_of_m_2() { return &___m_2; }
	inline void set_m_2(float value)
	{
		___m_2 = value;
	}

	inline static int32_t get_offset_of_l_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tFE6AAC70CEF4F120217A108769FDB473CF53C25E, ___l_3)); }
	inline int32_t get_l_3() const { return ___l_3; }
	inline int32_t* get_address_of_l_3() { return &___l_3; }
	inline void set_l_3(int32_t value)
	{
		___l_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tFE6AAC70CEF4F120217A108769FDB473CF53C25E, ___U3CU3E4__this_4)); }
	inline CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_4), (void*)value);
	}

	inline static int32_t get_offset_of_curLevel_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tFE6AAC70CEF4F120217A108769FDB473CF53C25E, ___curLevel_5)); }
	inline int32_t get_curLevel_5() const { return ___curLevel_5; }
	inline int32_t* get_address_of_curLevel_5() { return &___curLevel_5; }
	inline void set_curLevel_5(int32_t value)
	{
		___curLevel_5 = value;
	}
};


// CSIAPManager/<>c
struct U3CU3Ec_t33621A848112F597C3E83557E9C2E45798792FD9  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t33621A848112F597C3E83557E9C2E45798792FD9_StaticFields
{
public:
	// CSIAPManager/<>c CSIAPManager/<>c::<>9
	U3CU3Ec_t33621A848112F597C3E83557E9C2E45798792FD9 * ___U3CU3E9_0;
	// System.Action`1<System.Boolean> CSIAPManager/<>c::<>9__15_0
	Action_1_tCE2D770918A65CAD277C08C4E8C05385EA267E83 * ___U3CU3E9__15_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t33621A848112F597C3E83557E9C2E45798792FD9_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t33621A848112F597C3E83557E9C2E45798792FD9 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t33621A848112F597C3E83557E9C2E45798792FD9 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t33621A848112F597C3E83557E9C2E45798792FD9 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__15_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t33621A848112F597C3E83557E9C2E45798792FD9_StaticFields, ___U3CU3E9__15_0_1)); }
	inline Action_1_tCE2D770918A65CAD277C08C4E8C05385EA267E83 * get_U3CU3E9__15_0_1() const { return ___U3CU3E9__15_0_1; }
	inline Action_1_tCE2D770918A65CAD277C08C4E8C05385EA267E83 ** get_address_of_U3CU3E9__15_0_1() { return &___U3CU3E9__15_0_1; }
	inline void set_U3CU3E9__15_0_1(Action_1_tCE2D770918A65CAD277C08C4E8C05385EA267E83 * value)
	{
		___U3CU3E9__15_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__15_0_1), (void*)value);
	}
};


// CSIAPManager/<>c__DisplayClass8_0
struct U3CU3Ec__DisplayClass8_0_t4BC79262157EE77EC1C80AD29B85A9CA0EBBB225  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.ConfigurationBuilder CSIAPManager/<>c__DisplayClass8_0::builder
	ConfigurationBuilder_t8C2A33B91D14FD46DA1EF4EF45CA14143260969A * ___builder_0;

public:
	inline static int32_t get_offset_of_builder_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t4BC79262157EE77EC1C80AD29B85A9CA0EBBB225, ___builder_0)); }
	inline ConfigurationBuilder_t8C2A33B91D14FD46DA1EF4EF45CA14143260969A * get_builder_0() const { return ___builder_0; }
	inline ConfigurationBuilder_t8C2A33B91D14FD46DA1EF4EF45CA14143260969A ** get_address_of_builder_0() { return &___builder_0; }
	inline void set_builder_0(ConfigurationBuilder_t8C2A33B91D14FD46DA1EF4EF45CA14143260969A * value)
	{
		___builder_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___builder_0), (void*)value);
	}
};


// CSReel/<>c__DisplayClass16_0
struct U3CU3Ec__DisplayClass16_0_t36ED014080145BD25341EBC04BC5C89BD97234E4  : public RuntimeObject
{
public:
	// System.Action CSReel/<>c__DisplayClass16_0::callback
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_t36ED014080145BD25341EBC04BC5C89BD97234E4, ___callback_0)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_callback_0() const { return ___callback_0; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callback_0), (void*)value);
	}
};


// CSReelsAnimation/<AnimatePaylines>d__7
struct U3CAnimatePaylinesU3Ed__7_t6E550BE059940F53440A4AB80C861629B3C68FE6  : public RuntimeObject
{
public:
	// System.Int32 CSReelsAnimation/<AnimatePaylines>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object CSReelsAnimation/<AnimatePaylines>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// CSReelsAnimation CSReelsAnimation/<AnimatePaylines>d__7::<>4__this
	CSReelsAnimation_t04EC01AF43D2016CB036755318F8873C79F9714D * ___U3CU3E4__this_2;
	// CSListNavigation`1<CSPayline> CSReelsAnimation/<AnimatePaylines>d__7::paylines
	CSListNavigation_1_t4AC590C238DA470F8CF25714CBC444E615E3C5C1 * ___paylines_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAnimatePaylinesU3Ed__7_t6E550BE059940F53440A4AB80C861629B3C68FE6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAnimatePaylinesU3Ed__7_t6E550BE059940F53440A4AB80C861629B3C68FE6, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CAnimatePaylinesU3Ed__7_t6E550BE059940F53440A4AB80C861629B3C68FE6, ___U3CU3E4__this_2)); }
	inline CSReelsAnimation_t04EC01AF43D2016CB036755318F8873C79F9714D * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CSReelsAnimation_t04EC01AF43D2016CB036755318F8873C79F9714D ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CSReelsAnimation_t04EC01AF43D2016CB036755318F8873C79F9714D * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_paylines_3() { return static_cast<int32_t>(offsetof(U3CAnimatePaylinesU3Ed__7_t6E550BE059940F53440A4AB80C861629B3C68FE6, ___paylines_3)); }
	inline CSListNavigation_1_t4AC590C238DA470F8CF25714CBC444E615E3C5C1 * get_paylines_3() const { return ___paylines_3; }
	inline CSListNavigation_1_t4AC590C238DA470F8CF25714CBC444E615E3C5C1 ** get_address_of_paylines_3() { return &___paylines_3; }
	inline void set_paylines_3(CSListNavigation_1_t4AC590C238DA470F8CF25714CBC444E615E3C5C1 * value)
	{
		___paylines_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___paylines_3), (void*)value);
	}
};


// CSReelsAnimation/<AnimatePaylinesFreeGame>d__8
struct U3CAnimatePaylinesFreeGameU3Ed__8_t54D4BB7E0BA22FAA724545794C17ECAED600F58C  : public RuntimeObject
{
public:
	// System.Int32 CSReelsAnimation/<AnimatePaylinesFreeGame>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object CSReelsAnimation/<AnimatePaylinesFreeGame>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// CSReelsAnimation CSReelsAnimation/<AnimatePaylinesFreeGame>d__8::<>4__this
	CSReelsAnimation_t04EC01AF43D2016CB036755318F8873C79F9714D * ___U3CU3E4__this_2;
	// CSListNavigation`1<CSPayline> CSReelsAnimation/<AnimatePaylinesFreeGame>d__8::paylines
	CSListNavigation_1_t4AC590C238DA470F8CF25714CBC444E615E3C5C1 * ___paylines_3;
	// System.Action CSReelsAnimation/<AnimatePaylinesFreeGame>d__8::callback
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___callback_4;
	// System.Single CSReelsAnimation/<AnimatePaylinesFreeGame>d__8::<multiplier>5__2
	float ___U3CmultiplierU3E5__2_5;
	// System.Int32 CSReelsAnimation/<AnimatePaylinesFreeGame>d__8::<i>5__3
	int32_t ___U3CiU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAnimatePaylinesFreeGameU3Ed__8_t54D4BB7E0BA22FAA724545794C17ECAED600F58C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAnimatePaylinesFreeGameU3Ed__8_t54D4BB7E0BA22FAA724545794C17ECAED600F58C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CAnimatePaylinesFreeGameU3Ed__8_t54D4BB7E0BA22FAA724545794C17ECAED600F58C, ___U3CU3E4__this_2)); }
	inline CSReelsAnimation_t04EC01AF43D2016CB036755318F8873C79F9714D * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CSReelsAnimation_t04EC01AF43D2016CB036755318F8873C79F9714D ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CSReelsAnimation_t04EC01AF43D2016CB036755318F8873C79F9714D * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_paylines_3() { return static_cast<int32_t>(offsetof(U3CAnimatePaylinesFreeGameU3Ed__8_t54D4BB7E0BA22FAA724545794C17ECAED600F58C, ___paylines_3)); }
	inline CSListNavigation_1_t4AC590C238DA470F8CF25714CBC444E615E3C5C1 * get_paylines_3() const { return ___paylines_3; }
	inline CSListNavigation_1_t4AC590C238DA470F8CF25714CBC444E615E3C5C1 ** get_address_of_paylines_3() { return &___paylines_3; }
	inline void set_paylines_3(CSListNavigation_1_t4AC590C238DA470F8CF25714CBC444E615E3C5C1 * value)
	{
		___paylines_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___paylines_3), (void*)value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CAnimatePaylinesFreeGameU3Ed__8_t54D4BB7E0BA22FAA724545794C17ECAED600F58C, ___callback_4)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_callback_4() const { return ___callback_4; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callback_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CmultiplierU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CAnimatePaylinesFreeGameU3Ed__8_t54D4BB7E0BA22FAA724545794C17ECAED600F58C, ___U3CmultiplierU3E5__2_5)); }
	inline float get_U3CmultiplierU3E5__2_5() const { return ___U3CmultiplierU3E5__2_5; }
	inline float* get_address_of_U3CmultiplierU3E5__2_5() { return &___U3CmultiplierU3E5__2_5; }
	inline void set_U3CmultiplierU3E5__2_5(float value)
	{
		___U3CmultiplierU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CAnimatePaylinesFreeGameU3Ed__8_t54D4BB7E0BA22FAA724545794C17ECAED600F58C, ___U3CiU3E5__3_6)); }
	inline int32_t get_U3CiU3E5__3_6() const { return ___U3CiU3E5__3_6; }
	inline int32_t* get_address_of_U3CiU3E5__3_6() { return &___U3CiU3E5__3_6; }
	inline void set_U3CiU3E5__3_6(int32_t value)
	{
		___U3CiU3E5__3_6 = value;
	}
};


// CSSliderGroup/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_t9FF349AD8747383405A279B38F890A20822BE7C3  : public RuntimeObject
{
public:
	// UnityEngine.UI.Slider CSSliderGroup/<>c__DisplayClass2_0::item
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * ___item_0;
	// CSSliderGroup CSSliderGroup/<>c__DisplayClass2_0::<>4__this
	CSSliderGroup_tDC34B5CDF947DBA827407A2B6D39F5E7ADC28F4C * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t9FF349AD8747383405A279B38F890A20822BE7C3, ___item_0)); }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * get_item_0() const { return ___item_0; }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___item_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t9FF349AD8747383405A279B38F890A20822BE7C3, ___U3CU3E4__this_1)); }
	inline CSSliderGroup_tDC34B5CDF947DBA827407A2B6D39F5E7ADC28F4C * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline CSSliderGroup_tDC34B5CDF947DBA827407A2B6D39F5E7ADC28F4C ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(CSSliderGroup_tDC34B5CDF947DBA827407A2B6D39F5E7ADC28F4C * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}
};


// CSSliderGroup/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_t6630702C4953351F76E68CD3FBBFECD306EBD1E6  : public RuntimeObject
{
public:
	// UnityEngine.UI.Slider CSSliderGroup/<>c__DisplayClass3_0::item
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * ___item_0;
	// CSSliderGroup CSSliderGroup/<>c__DisplayClass3_0::<>4__this
	CSSliderGroup_tDC34B5CDF947DBA827407A2B6D39F5E7ADC28F4C * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t6630702C4953351F76E68CD3FBBFECD306EBD1E6, ___item_0)); }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * get_item_0() const { return ___item_0; }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___item_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t6630702C4953351F76E68CD3FBBFECD306EBD1E6, ___U3CU3E4__this_1)); }
	inline CSSliderGroup_tDC34B5CDF947DBA827407A2B6D39F5E7ADC28F4C * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline CSSliderGroup_tDC34B5CDF947DBA827407A2B6D39F5E7ADC28F4C ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(CSSliderGroup_tDC34B5CDF947DBA827407A2B6D39F5E7ADC28F4C * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}
};


// CSSoundManager/<>c
struct U3CU3Ec_tBDA41C01FAABEE4BF31833F05FAC1849607F7751  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tBDA41C01FAABEE4BF31833F05FAC1849607F7751_StaticFields
{
public:
	// CSSoundManager/<>c CSSoundManager/<>c::<>9
	U3CU3Ec_tBDA41C01FAABEE4BF31833F05FAC1849607F7751 * ___U3CU3E9_0;
	// System.Action`1<Sound> CSSoundManager/<>c::<>9__17_0
	Action_1_t86AC5575DD5B9AEA9C3D10160063071E7C6850C1 * ___U3CU3E9__17_0_1;
	// System.Action`1<Sound> CSSoundManager/<>c::<>9__18_0
	Action_1_t86AC5575DD5B9AEA9C3D10160063071E7C6850C1 * ___U3CU3E9__18_0_2;
	// System.Action`1<Sound> CSSoundManager/<>c::<>9__19_0
	Action_1_t86AC5575DD5B9AEA9C3D10160063071E7C6850C1 * ___U3CU3E9__19_0_3;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tBDA41C01FAABEE4BF31833F05FAC1849607F7751_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tBDA41C01FAABEE4BF31833F05FAC1849607F7751 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tBDA41C01FAABEE4BF31833F05FAC1849607F7751 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tBDA41C01FAABEE4BF31833F05FAC1849607F7751 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__17_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tBDA41C01FAABEE4BF31833F05FAC1849607F7751_StaticFields, ___U3CU3E9__17_0_1)); }
	inline Action_1_t86AC5575DD5B9AEA9C3D10160063071E7C6850C1 * get_U3CU3E9__17_0_1() const { return ___U3CU3E9__17_0_1; }
	inline Action_1_t86AC5575DD5B9AEA9C3D10160063071E7C6850C1 ** get_address_of_U3CU3E9__17_0_1() { return &___U3CU3E9__17_0_1; }
	inline void set_U3CU3E9__17_0_1(Action_1_t86AC5575DD5B9AEA9C3D10160063071E7C6850C1 * value)
	{
		___U3CU3E9__17_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__17_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__18_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tBDA41C01FAABEE4BF31833F05FAC1849607F7751_StaticFields, ___U3CU3E9__18_0_2)); }
	inline Action_1_t86AC5575DD5B9AEA9C3D10160063071E7C6850C1 * get_U3CU3E9__18_0_2() const { return ___U3CU3E9__18_0_2; }
	inline Action_1_t86AC5575DD5B9AEA9C3D10160063071E7C6850C1 ** get_address_of_U3CU3E9__18_0_2() { return &___U3CU3E9__18_0_2; }
	inline void set_U3CU3E9__18_0_2(Action_1_t86AC5575DD5B9AEA9C3D10160063071E7C6850C1 * value)
	{
		___U3CU3E9__18_0_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__18_0_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__19_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tBDA41C01FAABEE4BF31833F05FAC1849607F7751_StaticFields, ___U3CU3E9__19_0_3)); }
	inline Action_1_t86AC5575DD5B9AEA9C3D10160063071E7C6850C1 * get_U3CU3E9__19_0_3() const { return ___U3CU3E9__19_0_3; }
	inline Action_1_t86AC5575DD5B9AEA9C3D10160063071E7C6850C1 ** get_address_of_U3CU3E9__19_0_3() { return &___U3CU3E9__19_0_3; }
	inline void set_U3CU3E9__19_0_3(Action_1_t86AC5575DD5B9AEA9C3D10160063071E7C6850C1 * value)
	{
		___U3CU3E9__19_0_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__19_0_3), (void*)value);
	}
};


// CSSoundManager/<>c__DisplayClass20_0
struct U3CU3Ec__DisplayClass20_0_t4C31C11F3EDCDD19943B5AD0555C102F58BAB0AF  : public RuntimeObject
{
public:
	// System.String CSSoundManager/<>c__DisplayClass20_0::clipName
	String_t* ___clipName_0;

public:
	inline static int32_t get_offset_of_clipName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_t4C31C11F3EDCDD19943B5AD0555C102F58BAB0AF, ___clipName_0)); }
	inline String_t* get_clipName_0() const { return ___clipName_0; }
	inline String_t** get_address_of_clipName_0() { return &___clipName_0; }
	inline void set_clipName_0(String_t* value)
	{
		___clipName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___clipName_0), (void*)value);
	}
};


// CSSoundManager/<>c__DisplayClass21_0
struct U3CU3Ec__DisplayClass21_0_t7FED1675E28C0A8A3F352F1EC92CE5D067447617  : public RuntimeObject
{
public:
	// System.Boolean CSSoundManager/<>c__DisplayClass21_0::value
	bool ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_t7FED1675E28C0A8A3F352F1EC92CE5D067447617, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}
};


// CSToggleGroupHelper/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_t6D6DFCE53002EDD377AB45444A01C94C56B4D51D  : public RuntimeObject
{
public:
	// UnityEngine.UI.Toggle CSToggleGroupHelper/<>c__DisplayClass3_0::tt
	Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * ___tt_0;
	// CSToggleGroupHelper CSToggleGroupHelper/<>c__DisplayClass3_0::<>4__this
	CSToggleGroupHelper_t0B4982EA19DDA807327BDCF285651D4FB8C4551A * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_tt_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t6D6DFCE53002EDD377AB45444A01C94C56B4D51D, ___tt_0)); }
	inline Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * get_tt_0() const { return ___tt_0; }
	inline Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E ** get_address_of_tt_0() { return &___tt_0; }
	inline void set_tt_0(Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * value)
	{
		___tt_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tt_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t6D6DFCE53002EDD377AB45444A01C94C56B4D51D, ___U3CU3E4__this_1)); }
	inline CSToggleGroupHelper_t0B4982EA19DDA807327BDCF285651D4FB8C4551A * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline CSToggleGroupHelper_t0B4982EA19DDA807327BDCF285651D4FB8C4551A ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(CSToggleGroupHelper_t0B4982EA19DDA807327BDCF285651D4FB8C4551A * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}
};


// CSToggleTap/<>c
struct U3CU3Ec_tAA9E5DE4FCDBFC55E762BEDDF4D98A3769F4F393  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tAA9E5DE4FCDBFC55E762BEDDF4D98A3769F4F393_StaticFields
{
public:
	// CSToggleTap/<>c CSToggleTap/<>c::<>9
	U3CU3Ec_tAA9E5DE4FCDBFC55E762BEDDF4D98A3769F4F393 * ___U3CU3E9_0;
	// UnityEngine.Events.UnityAction`1<System.Boolean> CSToggleTap/<>c::<>9__0_0
	UnityAction_1_t11E0F272F18CD83EDF205B4A43689B005D10B7BF * ___U3CU3E9__0_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tAA9E5DE4FCDBFC55E762BEDDF4D98A3769F4F393_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tAA9E5DE4FCDBFC55E762BEDDF4D98A3769F4F393 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tAA9E5DE4FCDBFC55E762BEDDF4D98A3769F4F393 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tAA9E5DE4FCDBFC55E762BEDDF4D98A3769F4F393 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tAA9E5DE4FCDBFC55E762BEDDF4D98A3769F4F393_StaticFields, ___U3CU3E9__0_0_1)); }
	inline UnityAction_1_t11E0F272F18CD83EDF205B4A43689B005D10B7BF * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline UnityAction_1_t11E0F272F18CD83EDF205B4A43689B005D10B7BF ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(UnityAction_1_t11E0F272F18CD83EDF205B4A43689B005D10B7BF * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__0_0_1), (void*)value);
	}
};


// CSUtilities/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_t2D1253B73E681503FD2FA9E03F5BCADF2379AF0C  : public RuntimeObject
{
public:
	// System.Boolean CSUtilities/<>c__DisplayClass5_0::reverse
	bool ___reverse_0;
	// System.Int32 CSUtilities/<>c__DisplayClass5_0::count
	int32_t ___count_1;
	// System.Int32 CSUtilities/<>c__DisplayClass5_0::prev
	int32_t ___prev_2;
	// UnityEngine.UI.Image CSUtilities/<>c__DisplayClass5_0::image
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___image_3;
	// CSAnimationData CSUtilities/<>c__DisplayClass5_0::animationData
	CSAnimationData_t39AC7CF1C6BE740E5450F413840F4B8E08309645 * ___animationData_4;

public:
	inline static int32_t get_offset_of_reverse_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t2D1253B73E681503FD2FA9E03F5BCADF2379AF0C, ___reverse_0)); }
	inline bool get_reverse_0() const { return ___reverse_0; }
	inline bool* get_address_of_reverse_0() { return &___reverse_0; }
	inline void set_reverse_0(bool value)
	{
		___reverse_0 = value;
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t2D1253B73E681503FD2FA9E03F5BCADF2379AF0C, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}

	inline static int32_t get_offset_of_prev_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t2D1253B73E681503FD2FA9E03F5BCADF2379AF0C, ___prev_2)); }
	inline int32_t get_prev_2() const { return ___prev_2; }
	inline int32_t* get_address_of_prev_2() { return &___prev_2; }
	inline void set_prev_2(int32_t value)
	{
		___prev_2 = value;
	}

	inline static int32_t get_offset_of_image_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t2D1253B73E681503FD2FA9E03F5BCADF2379AF0C, ___image_3)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_image_3() const { return ___image_3; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_image_3() { return &___image_3; }
	inline void set_image_3(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___image_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___image_3), (void*)value);
	}

	inline static int32_t get_offset_of_animationData_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t2D1253B73E681503FD2FA9E03F5BCADF2379AF0C, ___animationData_4)); }
	inline CSAnimationData_t39AC7CF1C6BE740E5450F413840F4B8E08309645 * get_animationData_4() const { return ___animationData_4; }
	inline CSAnimationData_t39AC7CF1C6BE740E5450F413840F4B8E08309645 ** get_address_of_animationData_4() { return &___animationData_4; }
	inline void set_animationData_4(CSAnimationData_t39AC7CF1C6BE740E5450F413840F4B8E08309645 * value)
	{
		___animationData_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animationData_4), (void*)value);
	}
};


// CSUtilities/<>c__DisplayClass8_0
struct U3CU3Ec__DisplayClass8_0_t8F8118C81CFC6E82AABAF5BE633AD6184F43C5AD  : public RuntimeObject
{
public:
	// TMPro.TextMeshProUGUI CSUtilities/<>c__DisplayClass8_0::label
	TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * ___label_0;
	// System.String CSUtilities/<>c__DisplayClass8_0::coin
	String_t* ___coin_1;
	// System.Boolean CSUtilities/<>c__DisplayClass8_0::format
	bool ___format_2;

public:
	inline static int32_t get_offset_of_label_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t8F8118C81CFC6E82AABAF5BE633AD6184F43C5AD, ___label_0)); }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * get_label_0() const { return ___label_0; }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 ** get_address_of_label_0() { return &___label_0; }
	inline void set_label_0(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * value)
	{
		___label_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___label_0), (void*)value);
	}

	inline static int32_t get_offset_of_coin_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t8F8118C81CFC6E82AABAF5BE633AD6184F43C5AD, ___coin_1)); }
	inline String_t* get_coin_1() const { return ___coin_1; }
	inline String_t** get_address_of_coin_1() { return &___coin_1; }
	inline void set_coin_1(String_t* value)
	{
		___coin_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___coin_1), (void*)value);
	}

	inline static int32_t get_offset_of_format_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t8F8118C81CFC6E82AABAF5BE633AD6184F43C5AD, ___format_2)); }
	inline bool get_format_2() const { return ___format_2; }
	inline bool* get_address_of_format_2() { return &___format_2; }
	inline void set_format_2(bool value)
	{
		___format_2 = value;
	}
};


// CSZLGamble/<>c__DisplayClass26_0
struct U3CU3Ec__DisplayClass26_0_tAAC530862CD320954956F11901125E5BC043CAAF  : public RuntimeObject
{
public:
	// System.Action CSZLGamble/<>c__DisplayClass26_0::callback
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___callback_0;
	// CSZLGamble CSZLGamble/<>c__DisplayClass26_0::<>4__this
	CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass26_0_tAAC530862CD320954956F11901125E5BC043CAAF, ___callback_0)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_callback_0() const { return ___callback_0; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callback_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass26_0_tAAC530862CD320954956F11901125E5BC043CAAF, ___U3CU3E4__this_1)); }
	inline CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}
};


// CSZLGamble/<>c__DisplayClass27_0
struct U3CU3Ec__DisplayClass27_0_t9F015BCA3E7B26527AC505BC00E81696F6F91E93  : public RuntimeObject
{
public:
	// System.Action CSZLGamble/<>c__DisplayClass27_0::callback
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass27_0_t9F015BCA3E7B26527AC505BC00E81696F6F91E93, ___callback_0)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_callback_0() const { return ___callback_0; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callback_0), (void*)value);
	}
};


// CSZLGambleContent/<>c
struct U3CU3Ec_t01DD19CD922EF68E93C6DB03BC98BAB04EE884B3  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t01DD19CD922EF68E93C6DB03BC98BAB04EE884B3_StaticFields
{
public:
	// CSZLGambleContent/<>c CSZLGambleContent/<>c::<>9
	U3CU3Ec_t01DD19CD922EF68E93C6DB03BC98BAB04EE884B3 * ___U3CU3E9_0;
	// System.Action CSZLGambleContent/<>c::<>9__17_0
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__17_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t01DD19CD922EF68E93C6DB03BC98BAB04EE884B3_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t01DD19CD922EF68E93C6DB03BC98BAB04EE884B3 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t01DD19CD922EF68E93C6DB03BC98BAB04EE884B3 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t01DD19CD922EF68E93C6DB03BC98BAB04EE884B3 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__17_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t01DD19CD922EF68E93C6DB03BC98BAB04EE884B3_StaticFields, ___U3CU3E9__17_0_1)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__17_0_1() const { return ___U3CU3E9__17_0_1; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__17_0_1() { return &___U3CU3E9__17_0_1; }
	inline void set_U3CU3E9__17_0_1(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__17_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__17_0_1), (void*)value);
	}
};


// CSZLGambleContent/<>c__DisplayClass24_0
struct U3CU3Ec__DisplayClass24_0_tCFDCFCD45FF9A7DE832D774051A4A4F7A94CBFA6  : public RuntimeObject
{
public:
	// CSZLGambleContent CSZLGambleContent/<>c__DisplayClass24_0::<>4__this
	CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D * ___U3CU3E4__this_0;
	// CSCard CSZLGambleContent/<>c__DisplayClass24_0::first
	CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2 * ___first_1;
	// CSCard CSZLGambleContent/<>c__DisplayClass24_0::sender
	CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2 * ___sender_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tCFDCFCD45FF9A7DE832D774051A4A4F7A94CBFA6, ___U3CU3E4__this_0)); }
	inline CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_first_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tCFDCFCD45FF9A7DE832D774051A4A4F7A94CBFA6, ___first_1)); }
	inline CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2 * get_first_1() const { return ___first_1; }
	inline CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2 ** get_address_of_first_1() { return &___first_1; }
	inline void set_first_1(CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2 * value)
	{
		___first_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___first_1), (void*)value);
	}

	inline static int32_t get_offset_of_sender_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tCFDCFCD45FF9A7DE832D774051A4A4F7A94CBFA6, ___sender_2)); }
	inline CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2 * get_sender_2() const { return ___sender_2; }
	inline CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2 ** get_address_of_sender_2() { return &___sender_2; }
	inline void set_sender_2(CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2 * value)
	{
		___sender_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sender_2), (void*)value);
	}
};


// CSZLGambleContent/<>c__DisplayClass25_0
struct U3CU3Ec__DisplayClass25_0_t1471EDB5201095CD33CA027507C8E9E78167B12E  : public RuntimeObject
{
public:
	// CSZLGambleContent CSZLGambleContent/<>c__DisplayClass25_0::<>4__this
	CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D * ___U3CU3E4__this_0;
	// System.Boolean CSZLGambleContent/<>c__DisplayClass25_0::win
	bool ___win_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass25_0_t1471EDB5201095CD33CA027507C8E9E78167B12E, ___U3CU3E4__this_0)); }
	inline CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_win_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass25_0_t1471EDB5201095CD33CA027507C8E9E78167B12E, ___win_1)); }
	inline bool get_win_1() const { return ___win_1; }
	inline bool* get_address_of_win_1() { return &___win_1; }
	inline void set_win_1(bool value)
	{
		___win_1 = value;
	}
};


// LTDescr/<>c
struct U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58_StaticFields
{
public:
	// LTDescr/<>c LTDescr/<>c::<>9
	U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58 * ___U3CU3E9_0;
	// LTDescr/ActionMethodDelegate LTDescr/<>c::<>9__113_0
	ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * ___U3CU3E9__113_0_1;
	// LTDescr/ActionMethodDelegate LTDescr/<>c::<>9__114_0
	ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * ___U3CU3E9__114_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__113_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58_StaticFields, ___U3CU3E9__113_0_1)); }
	inline ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * get_U3CU3E9__113_0_1() const { return ___U3CU3E9__113_0_1; }
	inline ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 ** get_address_of_U3CU3E9__113_0_1() { return &___U3CU3E9__113_0_1; }
	inline void set_U3CU3E9__113_0_1(ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * value)
	{
		___U3CU3E9__113_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__113_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__114_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58_StaticFields, ___U3CU3E9__114_0_2)); }
	inline ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * get_U3CU3E9__114_0_2() const { return ___U3CU3E9__114_0_2; }
	inline ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 ** get_address_of_U3CU3E9__114_0_2() { return &___U3CU3E9__114_0_2; }
	inline void set_U3CU3E9__114_0_2(ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * value)
	{
		___U3CU3E9__114_0_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__114_0_2), (void*)value);
	}
};


// LeanTester/<timeoutCheck>d__2
struct U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6  : public RuntimeObject
{
public:
	// System.Int32 LeanTester/<timeoutCheck>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LeanTester/<timeoutCheck>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// LeanTester LeanTester/<timeoutCheck>d__2::<>4__this
	LeanTester_t537C3FE41ED244252241E5B02F6B1BA6DA437297 * ___U3CU3E4__this_2;
	// System.Single LeanTester/<timeoutCheck>d__2::<pauseEndTime>5__2
	float ___U3CpauseEndTimeU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6, ___U3CU3E4__this_2)); }
	inline LeanTester_t537C3FE41ED244252241E5B02F6B1BA6DA437297 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LeanTester_t537C3FE41ED244252241E5B02F6B1BA6DA437297 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LeanTester_t537C3FE41ED244252241E5B02F6B1BA6DA437297 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpauseEndTimeU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6, ___U3CpauseEndTimeU3E5__2_3)); }
	inline float get_U3CpauseEndTimeU3E5__2_3() const { return ___U3CpauseEndTimeU3E5__2_3; }
	inline float* get_address_of_U3CpauseEndTimeU3E5__2_3() { return &___U3CpauseEndTimeU3E5__2_3; }
	inline void set_U3CpauseEndTimeU3E5__2_3(float value)
	{
		___U3CpauseEndTimeU3E5__2_3 = value;
	}
};


// LeanTween/<>c__DisplayClass193_0
struct U3CU3Ec__DisplayClass193_0_tAD697F408BB575C622FD5E018C1F75AB28CBAF48  : public RuntimeObject
{
public:
	// LTDescr LeanTween/<>c__DisplayClass193_0::d
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___d_0;
	// System.Single LeanTween/<>c__DisplayClass193_0::smoothTime
	float ___smoothTime_1;
	// System.Single LeanTween/<>c__DisplayClass193_0::maxSpeed
	float ___maxSpeed_2;

public:
	inline static int32_t get_offset_of_d_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass193_0_tAD697F408BB575C622FD5E018C1F75AB28CBAF48, ___d_0)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_d_0() const { return ___d_0; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_d_0() { return &___d_0; }
	inline void set_d_0(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___d_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___d_0), (void*)value);
	}

	inline static int32_t get_offset_of_smoothTime_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass193_0_tAD697F408BB575C622FD5E018C1F75AB28CBAF48, ___smoothTime_1)); }
	inline float get_smoothTime_1() const { return ___smoothTime_1; }
	inline float* get_address_of_smoothTime_1() { return &___smoothTime_1; }
	inline void set_smoothTime_1(float value)
	{
		___smoothTime_1 = value;
	}

	inline static int32_t get_offset_of_maxSpeed_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass193_0_tAD697F408BB575C622FD5E018C1F75AB28CBAF48, ___maxSpeed_2)); }
	inline float get_maxSpeed_2() const { return ___maxSpeed_2; }
	inline float* get_address_of_maxSpeed_2() { return &___maxSpeed_2; }
	inline void set_maxSpeed_2(float value)
	{
		___maxSpeed_2 = value;
	}
};


// LeanTween/<>c__DisplayClass194_0
struct U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814  : public RuntimeObject
{
public:
	// LTDescr LeanTween/<>c__DisplayClass194_0::d
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___d_0;
	// System.Single LeanTween/<>c__DisplayClass194_0::smoothTime
	float ___smoothTime_1;
	// System.Single LeanTween/<>c__DisplayClass194_0::maxSpeed
	float ___maxSpeed_2;
	// System.Single LeanTween/<>c__DisplayClass194_0::friction
	float ___friction_3;
	// System.Single LeanTween/<>c__DisplayClass194_0::accelRate
	float ___accelRate_4;

public:
	inline static int32_t get_offset_of_d_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814, ___d_0)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_d_0() const { return ___d_0; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_d_0() { return &___d_0; }
	inline void set_d_0(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___d_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___d_0), (void*)value);
	}

	inline static int32_t get_offset_of_smoothTime_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814, ___smoothTime_1)); }
	inline float get_smoothTime_1() const { return ___smoothTime_1; }
	inline float* get_address_of_smoothTime_1() { return &___smoothTime_1; }
	inline void set_smoothTime_1(float value)
	{
		___smoothTime_1 = value;
	}

	inline static int32_t get_offset_of_maxSpeed_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814, ___maxSpeed_2)); }
	inline float get_maxSpeed_2() const { return ___maxSpeed_2; }
	inline float* get_address_of_maxSpeed_2() { return &___maxSpeed_2; }
	inline void set_maxSpeed_2(float value)
	{
		___maxSpeed_2 = value;
	}

	inline static int32_t get_offset_of_friction_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814, ___friction_3)); }
	inline float get_friction_3() const { return ___friction_3; }
	inline float* get_address_of_friction_3() { return &___friction_3; }
	inline void set_friction_3(float value)
	{
		___friction_3 = value;
	}

	inline static int32_t get_offset_of_accelRate_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814, ___accelRate_4)); }
	inline float get_accelRate_4() const { return ___accelRate_4; }
	inline float* get_address_of_accelRate_4() { return &___accelRate_4; }
	inline void set_accelRate_4(float value)
	{
		___accelRate_4 = value;
	}
};


// LeanTween/<>c__DisplayClass195_0
struct U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4  : public RuntimeObject
{
public:
	// LTDescr LeanTween/<>c__DisplayClass195_0::d
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___d_0;
	// System.Single LeanTween/<>c__DisplayClass195_0::smoothTime
	float ___smoothTime_1;
	// System.Single LeanTween/<>c__DisplayClass195_0::maxSpeed
	float ___maxSpeed_2;
	// System.Single LeanTween/<>c__DisplayClass195_0::friction
	float ___friction_3;
	// System.Single LeanTween/<>c__DisplayClass195_0::accelRate
	float ___accelRate_4;
	// System.Single LeanTween/<>c__DisplayClass195_0::hitDamping
	float ___hitDamping_5;

public:
	inline static int32_t get_offset_of_d_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4, ___d_0)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_d_0() const { return ___d_0; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_d_0() { return &___d_0; }
	inline void set_d_0(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___d_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___d_0), (void*)value);
	}

	inline static int32_t get_offset_of_smoothTime_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4, ___smoothTime_1)); }
	inline float get_smoothTime_1() const { return ___smoothTime_1; }
	inline float* get_address_of_smoothTime_1() { return &___smoothTime_1; }
	inline void set_smoothTime_1(float value)
	{
		___smoothTime_1 = value;
	}

	inline static int32_t get_offset_of_maxSpeed_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4, ___maxSpeed_2)); }
	inline float get_maxSpeed_2() const { return ___maxSpeed_2; }
	inline float* get_address_of_maxSpeed_2() { return &___maxSpeed_2; }
	inline void set_maxSpeed_2(float value)
	{
		___maxSpeed_2 = value;
	}

	inline static int32_t get_offset_of_friction_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4, ___friction_3)); }
	inline float get_friction_3() const { return ___friction_3; }
	inline float* get_address_of_friction_3() { return &___friction_3; }
	inline void set_friction_3(float value)
	{
		___friction_3 = value;
	}

	inline static int32_t get_offset_of_accelRate_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4, ___accelRate_4)); }
	inline float get_accelRate_4() const { return ___accelRate_4; }
	inline float* get_address_of_accelRate_4() { return &___accelRate_4; }
	inline void set_accelRate_4(float value)
	{
		___accelRate_4 = value;
	}

	inline static int32_t get_offset_of_hitDamping_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4, ___hitDamping_5)); }
	inline float get_hitDamping_5() const { return ___hitDamping_5; }
	inline float* get_address_of_hitDamping_5() { return &___hitDamping_5; }
	inline void set_hitDamping_5(float value)
	{
		___hitDamping_5 = value;
	}
};


// LeanTween/<>c__DisplayClass196_0
struct U3CU3Ec__DisplayClass196_0_t4FA263E9CE532093F4513F5750D54960760E3F78  : public RuntimeObject
{
public:
	// LTDescr LeanTween/<>c__DisplayClass196_0::d
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___d_0;
	// System.Single LeanTween/<>c__DisplayClass196_0::moveSpeed
	float ___moveSpeed_1;

public:
	inline static int32_t get_offset_of_d_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass196_0_t4FA263E9CE532093F4513F5750D54960760E3F78, ___d_0)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_d_0() const { return ___d_0; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_d_0() { return &___d_0; }
	inline void set_d_0(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___d_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___d_0), (void*)value);
	}

	inline static int32_t get_offset_of_moveSpeed_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass196_0_t4FA263E9CE532093F4513F5750D54960760E3F78, ___moveSpeed_1)); }
	inline float get_moveSpeed_1() const { return ___moveSpeed_1; }
	inline float* get_address_of_moveSpeed_1() { return &___moveSpeed_1; }
	inline void set_moveSpeed_1(float value)
	{
		___moveSpeed_1 = value;
	}
};


// TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6
struct U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.Examples.ShaderPropAnimator TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::<>4__this
	ShaderPropAnimator_t44FB2882ECA0D2C780444EB96B9550F5D245CFB8 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D, ___U3CU3E4__this_2)); }
	inline ShaderPropAnimator_t44FB2882ECA0D2C780444EB96B9550F5D245CFB8 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ShaderPropAnimator_t44FB2882ECA0D2C780444EB96B9550F5D245CFB8 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ShaderPropAnimator_t44FB2882ECA0D2C780444EB96B9550F5D245CFB8 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// TMPro.Examples.SkewTextExample/<WarpText>d__7
struct U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.SkewTextExample/<WarpText>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.Examples.SkewTextExample TMPro.Examples.SkewTextExample/<WarpText>d__7::<>4__this
	SkewTextExample_tA9F165D1BE969ED73FB86CA6C641386C22F1EE07 * ___U3CU3E4__this_2;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>d__7::<old_CurveScale>5__2
	float ___U3Cold_CurveScaleU3E5__2_3;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>d__7::<old_ShearValue>5__3
	float ___U3Cold_ShearValueU3E5__3_4;
	// UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample/<WarpText>d__7::<old_curve>5__4
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___U3Cold_curveU3E5__4_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B, ___U3CU3E4__this_2)); }
	inline SkewTextExample_tA9F165D1BE969ED73FB86CA6C641386C22F1EE07 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SkewTextExample_tA9F165D1BE969ED73FB86CA6C641386C22F1EE07 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SkewTextExample_tA9F165D1BE969ED73FB86CA6C641386C22F1EE07 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3Cold_CurveScaleU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B, ___U3Cold_CurveScaleU3E5__2_3)); }
	inline float get_U3Cold_CurveScaleU3E5__2_3() const { return ___U3Cold_CurveScaleU3E5__2_3; }
	inline float* get_address_of_U3Cold_CurveScaleU3E5__2_3() { return &___U3Cold_CurveScaleU3E5__2_3; }
	inline void set_U3Cold_CurveScaleU3E5__2_3(float value)
	{
		___U3Cold_CurveScaleU3E5__2_3 = value;
	}

	inline static int32_t get_offset_of_U3Cold_ShearValueU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B, ___U3Cold_ShearValueU3E5__3_4)); }
	inline float get_U3Cold_ShearValueU3E5__3_4() const { return ___U3Cold_ShearValueU3E5__3_4; }
	inline float* get_address_of_U3Cold_ShearValueU3E5__3_4() { return &___U3Cold_ShearValueU3E5__3_4; }
	inline void set_U3Cold_ShearValueU3E5__3_4(float value)
	{
		___U3Cold_ShearValueU3E5__3_4 = value;
	}

	inline static int32_t get_offset_of_U3Cold_curveU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B, ___U3Cold_curveU3E5__4_5)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_U3Cold_curveU3E5__4_5() const { return ___U3Cold_curveU3E5__4_5; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_U3Cold_curveU3E5__4_5() { return &___U3Cold_curveU3E5__4_5; }
	inline void set_U3Cold_curveU3E5__4_5(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___U3Cold_curveU3E5__4_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3Cold_curveU3E5__4_5), (void*)value);
	}
};


// TMPro.Examples.TeleType/<Start>d__4
struct U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.TeleType/<Start>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.TeleType/<Start>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.Examples.TeleType TMPro.Examples.TeleType/<Start>d__4::<>4__this
	TeleType_t92B5ED4FE818F912A562F085CE035F6B42834934 * ___U3CU3E4__this_2;
	// System.Int32 TMPro.Examples.TeleType/<Start>d__4::<totalVisibleCharacters>5__2
	int32_t ___U3CtotalVisibleCharactersU3E5__2_3;
	// System.Int32 TMPro.Examples.TeleType/<Start>d__4::<counter>5__3
	int32_t ___U3CcounterU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E, ___U3CU3E4__this_2)); }
	inline TeleType_t92B5ED4FE818F912A562F085CE035F6B42834934 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TeleType_t92B5ED4FE818F912A562F085CE035F6B42834934 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TeleType_t92B5ED4FE818F912A562F085CE035F6B42834934 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E, ___U3CtotalVisibleCharactersU3E5__2_3)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E5__2_3() const { return ___U3CtotalVisibleCharactersU3E5__2_3; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E5__2_3() { return &___U3CtotalVisibleCharactersU3E5__2_3; }
	inline void set_U3CtotalVisibleCharactersU3E5__2_3(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E5__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CcounterU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E, ___U3CcounterU3E5__3_4)); }
	inline int32_t get_U3CcounterU3E5__3_4() const { return ___U3CcounterU3E5__3_4; }
	inline int32_t* get_address_of_U3CcounterU3E5__3_4() { return &___U3CcounterU3E5__3_4; }
	inline void set_U3CcounterU3E5__3_4(int32_t value)
	{
		___U3CcounterU3E5__3_4 = value;
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<>c
struct U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields
{
public:
	// DentedPixel.LTExamples.TestingUnitTests/<>c DentedPixel.LTExamples.TestingUnitTests/<>c::<>9
	U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104 * ___U3CU3E9_0;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c::<>9__22_3
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__22_3_1;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c::<>9__22_22
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__22_22_2;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c::<>9__22_7
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__22_7_3;
	// System.Action`1<System.Single> DentedPixel.LTExamples.TestingUnitTests/<>c::<>9__22_12
	Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * ___U3CU3E9__22_12_4;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c::<>9__22_18
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__22_18_5;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c::<>9__26_0
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__26_0_6;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_3_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields, ___U3CU3E9__22_3_1)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__22_3_1() const { return ___U3CU3E9__22_3_1; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__22_3_1() { return &___U3CU3E9__22_3_1; }
	inline void set_U3CU3E9__22_3_1(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__22_3_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__22_3_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_22_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields, ___U3CU3E9__22_22_2)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__22_22_2() const { return ___U3CU3E9__22_22_2; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__22_22_2() { return &___U3CU3E9__22_22_2; }
	inline void set_U3CU3E9__22_22_2(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__22_22_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__22_22_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_7_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields, ___U3CU3E9__22_7_3)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__22_7_3() const { return ___U3CU3E9__22_7_3; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__22_7_3() { return &___U3CU3E9__22_7_3; }
	inline void set_U3CU3E9__22_7_3(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__22_7_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__22_7_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_12_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields, ___U3CU3E9__22_12_4)); }
	inline Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * get_U3CU3E9__22_12_4() const { return ___U3CU3E9__22_12_4; }
	inline Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 ** get_address_of_U3CU3E9__22_12_4() { return &___U3CU3E9__22_12_4; }
	inline void set_U3CU3E9__22_12_4(Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * value)
	{
		___U3CU3E9__22_12_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__22_12_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_18_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields, ___U3CU3E9__22_18_5)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__22_18_5() const { return ___U3CU3E9__22_18_5; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__22_18_5() { return &___U3CU3E9__22_18_5; }
	inline void set_U3CU3E9__22_18_5(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__22_18_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__22_18_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__26_0_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields, ___U3CU3E9__26_0_6)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__26_0_6() const { return ___U3CU3E9__26_0_6; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__26_0_6() { return &___U3CU3E9__26_0_6; }
	inline void set_U3CU3E9__26_0_6(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__26_0_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__26_0_6), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_1
struct U3CU3Ec__DisplayClass22_1_t041360FB309BAE73F44DAC9B4689082AC2FF5973  : public RuntimeObject
{
public:
	// System.Single DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_1::beforeX
	float ___beforeX_0;
	// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_beforeX_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_1_t041360FB309BAE73F44DAC9B4689082AC2FF5973, ___beforeX_0)); }
	inline float get_beforeX_0() const { return ___beforeX_0; }
	inline float* get_address_of_beforeX_0() { return &___beforeX_0; }
	inline void set_beforeX_0(float value)
	{
		___beforeX_0 = value;
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_1_t041360FB309BAE73F44DAC9B4689082AC2FF5973, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CSU24U3CU3E8__locals1_1), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_2
struct U3CU3Ec__DisplayClass22_2_tF5B9A247AE90B21641EE6BA256550BF6E5CBC1E8  : public RuntimeObject
{
public:
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_2::totalTweenTypeLength
	int32_t ___totalTweenTypeLength_0;
	// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_2::CS$<>8__locals2
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F * ___CSU24U3CU3E8__locals2_1;
	// System.Action`1<System.Object> DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_2::<>9__24
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ___U3CU3E9__24_2;

public:
	inline static int32_t get_offset_of_totalTweenTypeLength_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_2_tF5B9A247AE90B21641EE6BA256550BF6E5CBC1E8, ___totalTweenTypeLength_0)); }
	inline int32_t get_totalTweenTypeLength_0() const { return ___totalTweenTypeLength_0; }
	inline int32_t* get_address_of_totalTweenTypeLength_0() { return &___totalTweenTypeLength_0; }
	inline void set_totalTweenTypeLength_0(int32_t value)
	{
		___totalTweenTypeLength_0 = value;
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals2_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_2_tF5B9A247AE90B21641EE6BA256550BF6E5CBC1E8, ___CSU24U3CU3E8__locals2_1)); }
	inline U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F * get_CSU24U3CU3E8__locals2_1() const { return ___CSU24U3CU3E8__locals2_1; }
	inline U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F ** get_address_of_CSU24U3CU3E8__locals2_1() { return &___CSU24U3CU3E8__locals2_1; }
	inline void set_CSU24U3CU3E8__locals2_1(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F * value)
	{
		___CSU24U3CU3E8__locals2_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CSU24U3CU3E8__locals2_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_2_tF5B9A247AE90B21641EE6BA256550BF6E5CBC1E8, ___U3CU3E9__24_2)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get_U3CU3E9__24_2() const { return ___U3CU3E9__24_2; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of_U3CU3E9__24_2() { return &___U3CU3E9__24_2; }
	inline void set_U3CU3E9__24_2(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		___U3CU3E9__24_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__24_2), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25
struct U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523  : public RuntimeObject
{
public:
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DentedPixel.LTExamples.TestingUnitTests DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<>4__this
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * ___U3CU3E4__this_2;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<cubeCount>5__2
	int32_t ___U3CcubeCountU3E5__2_3;
	// System.Int32[] DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<tweensA>5__3
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___U3CtweensAU3E5__3_4;
	// UnityEngine.GameObject[] DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<aGOs>5__4
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___U3CaGOsU3E5__4_5;
	// System.Int32[] DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<tweensB>5__5
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___U3CtweensBU3E5__5_6;
	// UnityEngine.GameObject[] DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::<bGOs>5__6
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___U3CbGOsU3E5__6_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CU3E4__this_2)); }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcubeCountU3E5__2_3() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CcubeCountU3E5__2_3)); }
	inline int32_t get_U3CcubeCountU3E5__2_3() const { return ___U3CcubeCountU3E5__2_3; }
	inline int32_t* get_address_of_U3CcubeCountU3E5__2_3() { return &___U3CcubeCountU3E5__2_3; }
	inline void set_U3CcubeCountU3E5__2_3(int32_t value)
	{
		___U3CcubeCountU3E5__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CtweensAU3E5__3_4() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CtweensAU3E5__3_4)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_U3CtweensAU3E5__3_4() const { return ___U3CtweensAU3E5__3_4; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_U3CtweensAU3E5__3_4() { return &___U3CtweensAU3E5__3_4; }
	inline void set_U3CtweensAU3E5__3_4(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___U3CtweensAU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtweensAU3E5__3_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CaGOsU3E5__4_5() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CaGOsU3E5__4_5)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_U3CaGOsU3E5__4_5() const { return ___U3CaGOsU3E5__4_5; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_U3CaGOsU3E5__4_5() { return &___U3CaGOsU3E5__4_5; }
	inline void set_U3CaGOsU3E5__4_5(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___U3CaGOsU3E5__4_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CaGOsU3E5__4_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtweensBU3E5__5_6() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CtweensBU3E5__5_6)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_U3CtweensBU3E5__5_6() const { return ___U3CtweensBU3E5__5_6; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_U3CtweensBU3E5__5_6() { return &___U3CtweensBU3E5__5_6; }
	inline void set_U3CtweensBU3E5__5_6(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___U3CtweensBU3E5__5_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtweensBU3E5__5_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CbGOsU3E5__6_7() { return static_cast<int32_t>(offsetof(U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523, ___U3CbGOsU3E5__6_7)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_U3CbGOsU3E5__6_7() const { return ___U3CbGOsU3E5__6_7; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_U3CbGOsU3E5__6_7() { return &___U3CbGOsU3E5__6_7; }
	inline void set_U3CbGOsU3E5__6_7(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___U3CbGOsU3E5__6_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CbGOsU3E5__6_7), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26
struct U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB  : public RuntimeObject
{
public:
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DentedPixel.LTExamples.TestingUnitTests DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::<>4__this
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB, ___U3CU3E4__this_2)); }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24
struct U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65  : public RuntimeObject
{
public:
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DentedPixel.LTExamples.TestingUnitTests DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::<>4__this
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * ___U3CU3E4__this_2;
	// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0 DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::<>8__1
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F * ___U3CU3E8__1_3;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::<descriptionMatchCount>5__2
	int32_t ___U3CdescriptionMatchCountU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65, ___U3CU3E4__this_2)); }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_3() { return static_cast<int32_t>(offsetof(U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65, ___U3CU3E8__1_3)); }
	inline U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F * get_U3CU3E8__1_3() const { return ___U3CU3E8__1_3; }
	inline U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F ** get_address_of_U3CU3E8__1_3() { return &___U3CU3E8__1_3; }
	inline void set_U3CU3E8__1_3(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F * value)
	{
		___U3CU3E8__1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E8__1_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdescriptionMatchCountU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65, ___U3CdescriptionMatchCountU3E5__2_4)); }
	inline int32_t get_U3CdescriptionMatchCountU3E5__2_4() const { return ___U3CdescriptionMatchCountU3E5__2_4; }
	inline int32_t* get_address_of_U3CdescriptionMatchCountU3E5__2_4() { return &___U3CdescriptionMatchCountU3E5__2_4; }
	inline void set_U3CdescriptionMatchCountU3E5__2_4(int32_t value)
	{
		___U3CdescriptionMatchCountU3E5__2_4 = value;
	}
};


// TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7
struct U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::textComponent
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___textComponent_2;
	// TMPro.Examples.TextConsoleSimulator TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::<>4__this
	TextConsoleSimulator_t82904A7668D46B20E576B30E4CE367C0B66C5289 * ___U3CU3E4__this_3;
	// TMPro.TMP_TextInfo TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::<textInfo>5__2
	TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * ___U3CtextInfoU3E5__2_4;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::<totalVisibleCharacters>5__3
	int32_t ___U3CtotalVisibleCharactersU3E5__3_5;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::<visibleCount>5__4
	int32_t ___U3CvisibleCountU3E5__4_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_textComponent_2() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E, ___textComponent_2)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_textComponent_2() const { return ___textComponent_2; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_textComponent_2() { return &___textComponent_2; }
	inline void set_textComponent_2(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___textComponent_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textComponent_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E, ___U3CU3E4__this_3)); }
	inline TextConsoleSimulator_t82904A7668D46B20E576B30E4CE367C0B66C5289 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline TextConsoleSimulator_t82904A7668D46B20E576B30E4CE367C0B66C5289 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(TextConsoleSimulator_t82904A7668D46B20E576B30E4CE367C0B66C5289 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E, ___U3CtextInfoU3E5__2_4)); }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * get_U3CtextInfoU3E5__2_4() const { return ___U3CtextInfoU3E5__2_4; }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 ** get_address_of_U3CtextInfoU3E5__2_4() { return &___U3CtextInfoU3E5__2_4; }
	inline void set_U3CtextInfoU3E5__2_4(TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * value)
	{
		___U3CtextInfoU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtextInfoU3E5__2_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E, ___U3CtotalVisibleCharactersU3E5__3_5)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E5__3_5() const { return ___U3CtotalVisibleCharactersU3E5__3_5; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E5__3_5() { return &___U3CtotalVisibleCharactersU3E5__3_5; }
	inline void set_U3CtotalVisibleCharactersU3E5__3_5(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E5__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E, ___U3CvisibleCountU3E5__4_6)); }
	inline int32_t get_U3CvisibleCountU3E5__4_6() const { return ___U3CvisibleCountU3E5__4_6; }
	inline int32_t* get_address_of_U3CvisibleCountU3E5__4_6() { return &___U3CvisibleCountU3E5__4_6; }
	inline void set_U3CvisibleCountU3E5__4_6(int32_t value)
	{
		___U3CvisibleCountU3E5__4_6 = value;
	}
};


// TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8
struct U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::textComponent
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___textComponent_2;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::<totalWordCount>5__2
	int32_t ___U3CtotalWordCountU3E5__2_3;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::<totalVisibleCharacters>5__3
	int32_t ___U3CtotalVisibleCharactersU3E5__3_4;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::<counter>5__4
	int32_t ___U3CcounterU3E5__4_5;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::<visibleCount>5__5
	int32_t ___U3CvisibleCountU3E5__5_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_textComponent_2() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408, ___textComponent_2)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_textComponent_2() const { return ___textComponent_2; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_textComponent_2() { return &___textComponent_2; }
	inline void set_textComponent_2(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___textComponent_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textComponent_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtotalWordCountU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408, ___U3CtotalWordCountU3E5__2_3)); }
	inline int32_t get_U3CtotalWordCountU3E5__2_3() const { return ___U3CtotalWordCountU3E5__2_3; }
	inline int32_t* get_address_of_U3CtotalWordCountU3E5__2_3() { return &___U3CtotalWordCountU3E5__2_3; }
	inline void set_U3CtotalWordCountU3E5__2_3(int32_t value)
	{
		___U3CtotalWordCountU3E5__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408, ___U3CtotalVisibleCharactersU3E5__3_4)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E5__3_4() const { return ___U3CtotalVisibleCharactersU3E5__3_4; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E5__3_4() { return &___U3CtotalVisibleCharactersU3E5__3_4; }
	inline void set_U3CtotalVisibleCharactersU3E5__3_4(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E5__3_4 = value;
	}

	inline static int32_t get_offset_of_U3CcounterU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408, ___U3CcounterU3E5__4_5)); }
	inline int32_t get_U3CcounterU3E5__4_5() const { return ___U3CcounterU3E5__4_5; }
	inline int32_t* get_address_of_U3CcounterU3E5__4_5() { return &___U3CcounterU3E5__4_5; }
	inline void set_U3CcounterU3E5__4_5(int32_t value)
	{
		___U3CcounterU3E5__4_5 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E5__5_6() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408, ___U3CvisibleCountU3E5__5_6)); }
	inline int32_t get_U3CvisibleCountU3E5__5_6() const { return ___U3CvisibleCountU3E5__5_6; }
	inline int32_t* get_address_of_U3CvisibleCountU3E5__5_6() { return &___U3CvisibleCountU3E5__5_6; }
	inline void set_U3CvisibleCountU3E5__5_6(int32_t value)
	{
		___U3CvisibleCountU3E5__5_6 = value;
	}
};


// TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3
struct U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.Examples.VertexColorCycler TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::<>4__this
	VertexColorCycler_t37E80E87D8EAD0D7757CDA45BD5D3AA82FD28374 * ___U3CU3E4__this_2;
	// TMPro.TMP_TextInfo TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::<textInfo>5__2
	TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * ___U3CtextInfoU3E5__2_3;
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::<currentCharacter>5__3
	int32_t ___U3CcurrentCharacterU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF, ___U3CU3E4__this_2)); }
	inline VertexColorCycler_t37E80E87D8EAD0D7757CDA45BD5D3AA82FD28374 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VertexColorCycler_t37E80E87D8EAD0D7757CDA45BD5D3AA82FD28374 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VertexColorCycler_t37E80E87D8EAD0D7757CDA45BD5D3AA82FD28374 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF, ___U3CtextInfoU3E5__2_3)); }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * get_U3CtextInfoU3E5__2_3() const { return ___U3CtextInfoU3E5__2_3; }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 ** get_address_of_U3CtextInfoU3E5__2_3() { return &___U3CtextInfoU3E5__2_3; }
	inline void set_U3CtextInfoU3E5__2_3(TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * value)
	{
		___U3CtextInfoU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtextInfoU3E5__2_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcurrentCharacterU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF, ___U3CcurrentCharacterU3E5__3_4)); }
	inline int32_t get_U3CcurrentCharacterU3E5__3_4() const { return ___U3CcurrentCharacterU3E5__3_4; }
	inline int32_t* get_address_of_U3CcurrentCharacterU3E5__3_4() { return &___U3CcurrentCharacterU3E5__3_4; }
	inline void set_U3CcurrentCharacterU3E5__3_4(int32_t value)
	{
		___U3CcurrentCharacterU3E5__3_4 = value;
	}
};


// TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11
struct U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.Examples.VertexJitter TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::<>4__this
	VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A * ___U3CU3E4__this_2;
	// TMPro.TMP_TextInfo TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::<textInfo>5__2
	TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * ___U3CtextInfoU3E5__2_3;
	// System.Int32 TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::<loopCount>5__3
	int32_t ___U3CloopCountU3E5__3_4;
	// TMPro.Examples.VertexJitter/VertexAnim[] TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::<vertexAnim>5__4
	VertexAnimU5BU5D_t069E9FCD87DE8FF65BE288FDB2834711D933AAFF* ___U3CvertexAnimU3E5__4_5;
	// TMPro.TMP_MeshInfo[] TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::<cachedMeshInfo>5__5
	TMP_MeshInfoU5BU5D_t6C0A65D18C54B6FA681B2EB0676B83116FD03119* ___U3CcachedMeshInfoU3E5__5_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741, ___U3CU3E4__this_2)); }
	inline VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741, ___U3CtextInfoU3E5__2_3)); }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * get_U3CtextInfoU3E5__2_3() const { return ___U3CtextInfoU3E5__2_3; }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 ** get_address_of_U3CtextInfoU3E5__2_3() { return &___U3CtextInfoU3E5__2_3; }
	inline void set_U3CtextInfoU3E5__2_3(TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * value)
	{
		___U3CtextInfoU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtextInfoU3E5__2_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CloopCountU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741, ___U3CloopCountU3E5__3_4)); }
	inline int32_t get_U3CloopCountU3E5__3_4() const { return ___U3CloopCountU3E5__3_4; }
	inline int32_t* get_address_of_U3CloopCountU3E5__3_4() { return &___U3CloopCountU3E5__3_4; }
	inline void set_U3CloopCountU3E5__3_4(int32_t value)
	{
		___U3CloopCountU3E5__3_4 = value;
	}

	inline static int32_t get_offset_of_U3CvertexAnimU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741, ___U3CvertexAnimU3E5__4_5)); }
	inline VertexAnimU5BU5D_t069E9FCD87DE8FF65BE288FDB2834711D933AAFF* get_U3CvertexAnimU3E5__4_5() const { return ___U3CvertexAnimU3E5__4_5; }
	inline VertexAnimU5BU5D_t069E9FCD87DE8FF65BE288FDB2834711D933AAFF** get_address_of_U3CvertexAnimU3E5__4_5() { return &___U3CvertexAnimU3E5__4_5; }
	inline void set_U3CvertexAnimU3E5__4_5(VertexAnimU5BU5D_t069E9FCD87DE8FF65BE288FDB2834711D933AAFF* value)
	{
		___U3CvertexAnimU3E5__4_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CvertexAnimU3E5__4_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcachedMeshInfoU3E5__5_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741, ___U3CcachedMeshInfoU3E5__5_6)); }
	inline TMP_MeshInfoU5BU5D_t6C0A65D18C54B6FA681B2EB0676B83116FD03119* get_U3CcachedMeshInfoU3E5__5_6() const { return ___U3CcachedMeshInfoU3E5__5_6; }
	inline TMP_MeshInfoU5BU5D_t6C0A65D18C54B6FA681B2EB0676B83116FD03119** get_address_of_U3CcachedMeshInfoU3E5__5_6() { return &___U3CcachedMeshInfoU3E5__5_6; }
	inline void set_U3CcachedMeshInfoU3E5__5_6(TMP_MeshInfoU5BU5D_t6C0A65D18C54B6FA681B2EB0676B83116FD03119* value)
	{
		___U3CcachedMeshInfoU3E5__5_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CcachedMeshInfoU3E5__5_6), (void*)value);
	}
};


// TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11
struct U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.Examples.VertexShakeA TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::<>4__this
	VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A * ___U3CU3E4__this_2;
	// TMPro.TMP_TextInfo TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::<textInfo>5__2
	TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * ___U3CtextInfoU3E5__2_3;
	// UnityEngine.Vector3[][] TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::<copyOfVertices>5__3
	Vector3U5BU5DU5BU5D_tAD34024FBDE3F1F720ECB62B24B914A748C60BCB* ___U3CcopyOfVerticesU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155, ___U3CU3E4__this_2)); }
	inline VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155, ___U3CtextInfoU3E5__2_3)); }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * get_U3CtextInfoU3E5__2_3() const { return ___U3CtextInfoU3E5__2_3; }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 ** get_address_of_U3CtextInfoU3E5__2_3() { return &___U3CtextInfoU3E5__2_3; }
	inline void set_U3CtextInfoU3E5__2_3(TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * value)
	{
		___U3CtextInfoU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtextInfoU3E5__2_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcopyOfVerticesU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155, ___U3CcopyOfVerticesU3E5__3_4)); }
	inline Vector3U5BU5DU5BU5D_tAD34024FBDE3F1F720ECB62B24B914A748C60BCB* get_U3CcopyOfVerticesU3E5__3_4() const { return ___U3CcopyOfVerticesU3E5__3_4; }
	inline Vector3U5BU5DU5BU5D_tAD34024FBDE3F1F720ECB62B24B914A748C60BCB** get_address_of_U3CcopyOfVerticesU3E5__3_4() { return &___U3CcopyOfVerticesU3E5__3_4; }
	inline void set_U3CcopyOfVerticesU3E5__3_4(Vector3U5BU5DU5BU5D_tAD34024FBDE3F1F720ECB62B24B914A748C60BCB* value)
	{
		___U3CcopyOfVerticesU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CcopyOfVerticesU3E5__3_4), (void*)value);
	}
};


// TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10
struct U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.Examples.VertexShakeB TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::<>4__this
	VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9 * ___U3CU3E4__this_2;
	// TMPro.TMP_TextInfo TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::<textInfo>5__2
	TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * ___U3CtextInfoU3E5__2_3;
	// UnityEngine.Vector3[][] TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::<copyOfVertices>5__3
	Vector3U5BU5DU5BU5D_tAD34024FBDE3F1F720ECB62B24B914A748C60BCB* ___U3CcopyOfVerticesU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6, ___U3CU3E4__this_2)); }
	inline VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6, ___U3CtextInfoU3E5__2_3)); }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * get_U3CtextInfoU3E5__2_3() const { return ___U3CtextInfoU3E5__2_3; }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 ** get_address_of_U3CtextInfoU3E5__2_3() { return &___U3CtextInfoU3E5__2_3; }
	inline void set_U3CtextInfoU3E5__2_3(TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * value)
	{
		___U3CtextInfoU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtextInfoU3E5__2_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcopyOfVerticesU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6, ___U3CcopyOfVerticesU3E5__3_4)); }
	inline Vector3U5BU5DU5BU5D_tAD34024FBDE3F1F720ECB62B24B914A748C60BCB* get_U3CcopyOfVerticesU3E5__3_4() const { return ___U3CcopyOfVerticesU3E5__3_4; }
	inline Vector3U5BU5DU5BU5D_tAD34024FBDE3F1F720ECB62B24B914A748C60BCB** get_address_of_U3CcopyOfVerticesU3E5__3_4() { return &___U3CcopyOfVerticesU3E5__3_4; }
	inline void set_U3CcopyOfVerticesU3E5__3_4(Vector3U5BU5DU5BU5D_tAD34024FBDE3F1F720ECB62B24B914A748C60BCB* value)
	{
		___U3CcopyOfVerticesU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CcopyOfVerticesU3E5__3_4), (void*)value);
	}
};


// TMPro.Examples.VertexZoom/<>c__DisplayClass10_0
struct U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Single> TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::modifiedCharScale
	List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * ___modifiedCharScale_0;
	// System.Comparison`1<System.Int32> TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<>9__0
	Comparison_1_t3430355DCDD0AF453788265DC88E9288D19C4E9C * ___U3CU3E9__0_1;

public:
	inline static int32_t get_offset_of_modifiedCharScale_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6, ___modifiedCharScale_0)); }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * get_modifiedCharScale_0() const { return ___modifiedCharScale_0; }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA ** get_address_of_modifiedCharScale_0() { return &___modifiedCharScale_0; }
	inline void set_modifiedCharScale_0(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * value)
	{
		___modifiedCharScale_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___modifiedCharScale_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6, ___U3CU3E9__0_1)); }
	inline Comparison_1_t3430355DCDD0AF453788265DC88E9288D19C4E9C * get_U3CU3E9__0_1() const { return ___U3CU3E9__0_1; }
	inline Comparison_1_t3430355DCDD0AF453788265DC88E9288D19C4E9C ** get_address_of_U3CU3E9__0_1() { return &___U3CU3E9__0_1; }
	inline void set_U3CU3E9__0_1(Comparison_1_t3430355DCDD0AF453788265DC88E9288D19C4E9C * value)
	{
		___U3CU3E9__0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__0_1), (void*)value);
	}
};


// TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10
struct U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.Examples.VertexZoom TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::<>4__this
	VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163 * ___U3CU3E4__this_2;
	// TMPro.Examples.VertexZoom/<>c__DisplayClass10_0 TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::<>8__1
	U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6 * ___U3CU3E8__1_3;
	// TMPro.TMP_TextInfo TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::<textInfo>5__2
	TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * ___U3CtextInfoU3E5__2_4;
	// TMPro.TMP_MeshInfo[] TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::<cachedMeshInfoVertexData>5__3
	TMP_MeshInfoU5BU5D_t6C0A65D18C54B6FA681B2EB0676B83116FD03119* ___U3CcachedMeshInfoVertexDataU3E5__3_5;
	// System.Collections.Generic.List`1<System.Int32> TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::<scaleSortingOrder>5__4
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___U3CscaleSortingOrderU3E5__4_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40, ___U3CU3E4__this_2)); }
	inline VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40, ___U3CU3E8__1_3)); }
	inline U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6 * get_U3CU3E8__1_3() const { return ___U3CU3E8__1_3; }
	inline U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6 ** get_address_of_U3CU3E8__1_3() { return &___U3CU3E8__1_3; }
	inline void set_U3CU3E8__1_3(U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6 * value)
	{
		___U3CU3E8__1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E8__1_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40, ___U3CtextInfoU3E5__2_4)); }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * get_U3CtextInfoU3E5__2_4() const { return ___U3CtextInfoU3E5__2_4; }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 ** get_address_of_U3CtextInfoU3E5__2_4() { return &___U3CtextInfoU3E5__2_4; }
	inline void set_U3CtextInfoU3E5__2_4(TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * value)
	{
		___U3CtextInfoU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtextInfoU3E5__2_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcachedMeshInfoVertexDataU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40, ___U3CcachedMeshInfoVertexDataU3E5__3_5)); }
	inline TMP_MeshInfoU5BU5D_t6C0A65D18C54B6FA681B2EB0676B83116FD03119* get_U3CcachedMeshInfoVertexDataU3E5__3_5() const { return ___U3CcachedMeshInfoVertexDataU3E5__3_5; }
	inline TMP_MeshInfoU5BU5D_t6C0A65D18C54B6FA681B2EB0676B83116FD03119** get_address_of_U3CcachedMeshInfoVertexDataU3E5__3_5() { return &___U3CcachedMeshInfoVertexDataU3E5__3_5; }
	inline void set_U3CcachedMeshInfoVertexDataU3E5__3_5(TMP_MeshInfoU5BU5D_t6C0A65D18C54B6FA681B2EB0676B83116FD03119* value)
	{
		___U3CcachedMeshInfoVertexDataU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CcachedMeshInfoVertexDataU3E5__3_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CscaleSortingOrderU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40, ___U3CscaleSortingOrderU3E5__4_6)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_U3CscaleSortingOrderU3E5__4_6() const { return ___U3CscaleSortingOrderU3E5__4_6; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_U3CscaleSortingOrderU3E5__4_6() { return &___U3CscaleSortingOrderU3E5__4_6; }
	inline void set_U3CscaleSortingOrderU3E5__4_6(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___U3CscaleSortingOrderU3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CscaleSortingOrderU3E5__4_6), (void*)value);
	}
};


// TMPro.Examples.WarpTextExample/<WarpText>d__8
struct U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.WarpTextExample/<WarpText>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.Examples.WarpTextExample TMPro.Examples.WarpTextExample/<WarpText>d__8::<>4__this
	WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96 * ___U3CU3E4__this_2;
	// System.Single TMPro.Examples.WarpTextExample/<WarpText>d__8::<old_CurveScale>5__2
	float ___U3Cold_CurveScaleU3E5__2_3;
	// UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample/<WarpText>d__8::<old_curve>5__3
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___U3Cold_curveU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76, ___U3CU3E4__this_2)); }
	inline WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3Cold_CurveScaleU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76, ___U3Cold_CurveScaleU3E5__2_3)); }
	inline float get_U3Cold_CurveScaleU3E5__2_3() const { return ___U3Cold_CurveScaleU3E5__2_3; }
	inline float* get_address_of_U3Cold_CurveScaleU3E5__2_3() { return &___U3Cold_CurveScaleU3E5__2_3; }
	inline void set_U3Cold_CurveScaleU3E5__2_3(float value)
	{
		___U3Cold_CurveScaleU3E5__2_3 = value;
	}

	inline static int32_t get_offset_of_U3Cold_curveU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76, ___U3Cold_curveU3E5__3_4)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_U3Cold_curveU3E5__3_4() const { return ___U3Cold_curveU3E5__3_4; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_U3Cold_curveU3E5__3_4() { return &___U3Cold_curveU3E5__3_4; }
	inline void set_U3Cold_curveU3E5__3_4(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___U3Cold_curveU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3Cold_curveU3E5__3_4), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<System.Int32>
struct UnityEvent_1_tB235B5DAD099AC425DC059D10DEB8B97A35E2BBF  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_tB235B5DAD099AC425DC059D10DEB8B97A35E2BBF, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`2<System.Char,System.Int32>
struct UnityEvent_2_t5304A7BB9DB39BB3BA59CAF408F8EA7341658E2C  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_2_t5304A7BB9DB39BB3BA59CAF408F8EA7341658E2C, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`3<System.String,System.Int32,System.Int32>
struct UnityEvent_3_tB2C1BFEE5A56978DECD9BA6756512E2CC49CB9FE  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_3_tB2C1BFEE5A56978DECD9BA6756512E2CC49CB9FE, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`3<System.String,System.String,System.Int32>
struct UnityEvent_3_t99EBCF5B18C77267336F36583A2B69BE0535E6E9  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_3_t99EBCF5B18C77267336F36583A2B69BE0535E6E9, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// CSCSBonusGameSettings
struct CSCSBonusGameSettings_t6311F6E406F6804E8E84F28F2155273091F40076 
{
public:
	// System.Int32 CSCSBonusGameSettings::freeSpins
	int32_t ___freeSpins_0;
	// System.Single CSCSBonusGameSettings::coins
	float ___coins_1;
	// System.Int32 CSCSBonusGameSettings::multiplier
	int32_t ___multiplier_2;

public:
	inline static int32_t get_offset_of_freeSpins_0() { return static_cast<int32_t>(offsetof(CSCSBonusGameSettings_t6311F6E406F6804E8E84F28F2155273091F40076, ___freeSpins_0)); }
	inline int32_t get_freeSpins_0() const { return ___freeSpins_0; }
	inline int32_t* get_address_of_freeSpins_0() { return &___freeSpins_0; }
	inline void set_freeSpins_0(int32_t value)
	{
		___freeSpins_0 = value;
	}

	inline static int32_t get_offset_of_coins_1() { return static_cast<int32_t>(offsetof(CSCSBonusGameSettings_t6311F6E406F6804E8E84F28F2155273091F40076, ___coins_1)); }
	inline float get_coins_1() const { return ___coins_1; }
	inline float* get_address_of_coins_1() { return &___coins_1; }
	inline void set_coins_1(float value)
	{
		___coins_1 = value;
	}

	inline static int32_t get_offset_of_multiplier_2() { return static_cast<int32_t>(offsetof(CSCSBonusGameSettings_t6311F6E406F6804E8E84F28F2155273091F40076, ___multiplier_2)); }
	inline int32_t get_multiplier_2() const { return ___multiplier_2; }
	inline int32_t* get_address_of_multiplier_2() { return &___multiplier_2; }
	inline void set_multiplier_2(int32_t value)
	{
		___multiplier_2 = value;
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.Color32
struct Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};


// System.DateTime
struct DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth365_29), (void*)value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth366_30), (void*)value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___MinValue_31)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___MaxValue_32)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___MaxValue_32 = value;
	}
};


// System.Double
struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.LayerMask
struct LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};


// UnityEngine.Matrix4x4
struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___identityMatrix_17 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Rect
struct Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_HighlightedSprite_0)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_PressedSprite_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_SelectedSprite_2)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_DisabledSprite_3)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_pinvoke
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_com
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};

// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// CS2DBoolArray/L2D
struct L2D_t890C859307906191E6AE39D17A20495B84F62703 
{
public:
	// System.Boolean[] CS2DBoolArray/L2D::l1
	BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* ___l1_0;

public:
	inline static int32_t get_offset_of_l1_0() { return static_cast<int32_t>(offsetof(L2D_t890C859307906191E6AE39D17A20495B84F62703, ___l1_0)); }
	inline BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* get_l1_0() const { return ___l1_0; }
	inline BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C** get_address_of_l1_0() { return &___l1_0; }
	inline void set_l1_0(BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* value)
	{
		___l1_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___l1_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of CS2DBoolArray/L2D
struct L2D_t890C859307906191E6AE39D17A20495B84F62703_marshaled_pinvoke
{
	int32_t* ___l1_0;
};
// Native definition for COM marshalling of CS2DBoolArray/L2D
struct L2D_t890C859307906191E6AE39D17A20495B84F62703_marshaled_com
{
	int32_t* ___l1_0;
};

// CSCardList2D/List2D
struct List2D_tF34A588855096C9705F18D8CAAF87AA174FA71BD 
{
public:
	// CSCardData[] CSCardList2D/List2D::l1
	CSCardDataU5BU5D_tCBC644233E59D6BA967534AAD4A52E1EBC8464A1* ___l1_0;

public:
	inline static int32_t get_offset_of_l1_0() { return static_cast<int32_t>(offsetof(List2D_tF34A588855096C9705F18D8CAAF87AA174FA71BD, ___l1_0)); }
	inline CSCardDataU5BU5D_tCBC644233E59D6BA967534AAD4A52E1EBC8464A1* get_l1_0() const { return ___l1_0; }
	inline CSCardDataU5BU5D_tCBC644233E59D6BA967534AAD4A52E1EBC8464A1** get_address_of_l1_0() { return &___l1_0; }
	inline void set_l1_0(CSCardDataU5BU5D_tCBC644233E59D6BA967534AAD4A52E1EBC8464A1* value)
	{
		___l1_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___l1_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of CSCardList2D/List2D
struct List2D_tF34A588855096C9705F18D8CAAF87AA174FA71BD_marshaled_pinvoke
{
	CSCardDataU5BU5D_tCBC644233E59D6BA967534AAD4A52E1EBC8464A1* ___l1_0;
};
// Native definition for COM marshalling of CSCardList2D/List2D
struct List2D_tF34A588855096C9705F18D8CAAF87AA174FA71BD_marshaled_com
{
	CSCardDataU5BU5D_tCBC644233E59D6BA967534AAD4A52E1EBC8464A1* ___l1_0;
};

// TMPro.Examples.VertexJitter/VertexAnim
struct VertexAnim_t3C1CE8C591FCD7E249D77127BED0B743EAD556F4 
{
public:
	// System.Single TMPro.Examples.VertexJitter/VertexAnim::angleRange
	float ___angleRange_0;
	// System.Single TMPro.Examples.VertexJitter/VertexAnim::angle
	float ___angle_1;
	// System.Single TMPro.Examples.VertexJitter/VertexAnim::speed
	float ___speed_2;

public:
	inline static int32_t get_offset_of_angleRange_0() { return static_cast<int32_t>(offsetof(VertexAnim_t3C1CE8C591FCD7E249D77127BED0B743EAD556F4, ___angleRange_0)); }
	inline float get_angleRange_0() const { return ___angleRange_0; }
	inline float* get_address_of_angleRange_0() { return &___angleRange_0; }
	inline void set_angleRange_0(float value)
	{
		___angleRange_0 = value;
	}

	inline static int32_t get_offset_of_angle_1() { return static_cast<int32_t>(offsetof(VertexAnim_t3C1CE8C591FCD7E249D77127BED0B743EAD556F4, ___angle_1)); }
	inline float get_angle_1() const { return ___angle_1; }
	inline float* get_address_of_angle_1() { return &___angle_1; }
	inline void set_angle_1(float value)
	{
		___angle_1 = value;
	}

	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(VertexAnim_t3C1CE8C591FCD7E249D77127BED0B743EAD556F4, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}
};


// CSEnumFlagAttribute
struct CSEnumFlagAttribute_tC301B9CC7595F4CC930BD5F23373B22D42D7559F  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String CSEnumFlagAttribute::enumName
	String_t* ___enumName_0;

public:
	inline static int32_t get_offset_of_enumName_0() { return static_cast<int32_t>(offsetof(CSEnumFlagAttribute_tC301B9CC7595F4CC930BD5F23373B22D42D7559F, ___enumName_0)); }
	inline String_t* get_enumName_0() const { return ___enumName_0; }
	inline String_t** get_address_of_enumName_0() { return &___enumName_0; }
	inline void set_enumName_0(String_t* value)
	{
		___enumName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumName_0), (void*)value);
	}
};


// CSGameData
struct CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04  : public RuntimeObject
{
public:
	// System.Boolean CSGameData::sound
	bool ___sound_0;
	// System.Boolean CSGameData::music
	bool ___music_1;
	// System.Single CSGameData::highscore
	float ___highscore_2;
	// System.UInt32 CSGameData::playcount
	uint32_t ___playcount_3;
	// System.Boolean CSGameData::ads
	bool ___ads_4;
	// System.Single CSGameData::coins
	float ___coins_5;
	// System.DateTime CSGameData::installDate
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___installDate_6;
	// System.Collections.Generic.Dictionary`2<System.String,CSTimerData> CSGameData::timers
	Dictionary_2_tC13AD4CF9968EB1A9E950D624D16833A37BC9E5C * ___timers_7;
	// System.Single CSGameData::xp
	float ___xp_8;
	// System.Int32 CSGameData::level
	int32_t ___level_9;
	// System.Int32 CSGameData::installDays
	int32_t ___installDays_10;
	// System.String CSGameData::selectedTheme
	String_t* ___selectedTheme_11;
	// System.Int32 CSGameData::classicSevenBetStep
	int32_t ___classicSevenBetStep_12;
	// System.Single CSGameData::volume
	float ___volume_13;

public:
	inline static int32_t get_offset_of_sound_0() { return static_cast<int32_t>(offsetof(CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04, ___sound_0)); }
	inline bool get_sound_0() const { return ___sound_0; }
	inline bool* get_address_of_sound_0() { return &___sound_0; }
	inline void set_sound_0(bool value)
	{
		___sound_0 = value;
	}

	inline static int32_t get_offset_of_music_1() { return static_cast<int32_t>(offsetof(CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04, ___music_1)); }
	inline bool get_music_1() const { return ___music_1; }
	inline bool* get_address_of_music_1() { return &___music_1; }
	inline void set_music_1(bool value)
	{
		___music_1 = value;
	}

	inline static int32_t get_offset_of_highscore_2() { return static_cast<int32_t>(offsetof(CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04, ___highscore_2)); }
	inline float get_highscore_2() const { return ___highscore_2; }
	inline float* get_address_of_highscore_2() { return &___highscore_2; }
	inline void set_highscore_2(float value)
	{
		___highscore_2 = value;
	}

	inline static int32_t get_offset_of_playcount_3() { return static_cast<int32_t>(offsetof(CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04, ___playcount_3)); }
	inline uint32_t get_playcount_3() const { return ___playcount_3; }
	inline uint32_t* get_address_of_playcount_3() { return &___playcount_3; }
	inline void set_playcount_3(uint32_t value)
	{
		___playcount_3 = value;
	}

	inline static int32_t get_offset_of_ads_4() { return static_cast<int32_t>(offsetof(CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04, ___ads_4)); }
	inline bool get_ads_4() const { return ___ads_4; }
	inline bool* get_address_of_ads_4() { return &___ads_4; }
	inline void set_ads_4(bool value)
	{
		___ads_4 = value;
	}

	inline static int32_t get_offset_of_coins_5() { return static_cast<int32_t>(offsetof(CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04, ___coins_5)); }
	inline float get_coins_5() const { return ___coins_5; }
	inline float* get_address_of_coins_5() { return &___coins_5; }
	inline void set_coins_5(float value)
	{
		___coins_5 = value;
	}

	inline static int32_t get_offset_of_installDate_6() { return static_cast<int32_t>(offsetof(CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04, ___installDate_6)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_installDate_6() const { return ___installDate_6; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_installDate_6() { return &___installDate_6; }
	inline void set_installDate_6(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___installDate_6 = value;
	}

	inline static int32_t get_offset_of_timers_7() { return static_cast<int32_t>(offsetof(CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04, ___timers_7)); }
	inline Dictionary_2_tC13AD4CF9968EB1A9E950D624D16833A37BC9E5C * get_timers_7() const { return ___timers_7; }
	inline Dictionary_2_tC13AD4CF9968EB1A9E950D624D16833A37BC9E5C ** get_address_of_timers_7() { return &___timers_7; }
	inline void set_timers_7(Dictionary_2_tC13AD4CF9968EB1A9E950D624D16833A37BC9E5C * value)
	{
		___timers_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___timers_7), (void*)value);
	}

	inline static int32_t get_offset_of_xp_8() { return static_cast<int32_t>(offsetof(CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04, ___xp_8)); }
	inline float get_xp_8() const { return ___xp_8; }
	inline float* get_address_of_xp_8() { return &___xp_8; }
	inline void set_xp_8(float value)
	{
		___xp_8 = value;
	}

	inline static int32_t get_offset_of_level_9() { return static_cast<int32_t>(offsetof(CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04, ___level_9)); }
	inline int32_t get_level_9() const { return ___level_9; }
	inline int32_t* get_address_of_level_9() { return &___level_9; }
	inline void set_level_9(int32_t value)
	{
		___level_9 = value;
	}

	inline static int32_t get_offset_of_installDays_10() { return static_cast<int32_t>(offsetof(CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04, ___installDays_10)); }
	inline int32_t get_installDays_10() const { return ___installDays_10; }
	inline int32_t* get_address_of_installDays_10() { return &___installDays_10; }
	inline void set_installDays_10(int32_t value)
	{
		___installDays_10 = value;
	}

	inline static int32_t get_offset_of_selectedTheme_11() { return static_cast<int32_t>(offsetof(CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04, ___selectedTheme_11)); }
	inline String_t* get_selectedTheme_11() const { return ___selectedTheme_11; }
	inline String_t** get_address_of_selectedTheme_11() { return &___selectedTheme_11; }
	inline void set_selectedTheme_11(String_t* value)
	{
		___selectedTheme_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selectedTheme_11), (void*)value);
	}

	inline static int32_t get_offset_of_classicSevenBetStep_12() { return static_cast<int32_t>(offsetof(CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04, ___classicSevenBetStep_12)); }
	inline int32_t get_classicSevenBetStep_12() const { return ___classicSevenBetStep_12; }
	inline int32_t* get_address_of_classicSevenBetStep_12() { return &___classicSevenBetStep_12; }
	inline void set_classicSevenBetStep_12(int32_t value)
	{
		___classicSevenBetStep_12 = value;
	}

	inline static int32_t get_offset_of_volume_13() { return static_cast<int32_t>(offsetof(CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04, ___volume_13)); }
	inline float get_volume_13() const { return ___volume_13; }
	inline float* get_address_of_volume_13() { return &___volume_13; }
	inline void set_volume_13(float value)
	{
		___volume_13 = value;
	}
};


// CSLoadingState
struct CSLoadingState_t34C48CC058E619064852C5A2B460F068EF95B644 
{
public:
	// System.Int32 CSLoadingState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CSLoadingState_t34C48CC058E619064852C5A2B460F068EF95B644, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// CSRank
struct CSRank_tAD01A3314E9998618FECABE21AE478ADA68673BC 
{
public:
	// System.Int32 CSRank::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CSRank_tAD01A3314E9998618FECABE21AE478ADA68673BC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// CSSuit
struct CSSuit_tAA75CE75D9A835173D3678A1607BF238058BCC02 
{
public:
	// System.Int32 CSSuit::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CSSuit_tAA75CE75D9A835173D3678A1607BF238058BCC02, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// CSSymbolType
struct CSSymbolType_tA08EC9FA0B5E0C098DA0501759A595E02935EC08 
{
public:
	// System.Int32 CSSymbolType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CSSymbolType_tA08EC9FA0B5E0C098DA0501759A595E02935EC08, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// CSTimeProperty
struct CSTimeProperty_t880ACDE81B4949E6C83531AA6236E52D759F74D1 
{
public:
	// System.Int32 CSTimeProperty::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CSTimeProperty_t880ACDE81B4949E6C83531AA6236E52D759F74D1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// LTBezier
struct LTBezier_tD081588F24017B409080ADD7EC92332888511C41  : public RuntimeObject
{
public:
	// System.Single LTBezier::length
	float ___length_0;
	// UnityEngine.Vector3 LTBezier::a
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a_1;
	// UnityEngine.Vector3 LTBezier::aa
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___aa_2;
	// UnityEngine.Vector3 LTBezier::bb
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___bb_3;
	// UnityEngine.Vector3 LTBezier::cc
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___cc_4;
	// System.Single LTBezier::len
	float ___len_5;
	// System.Single[] LTBezier::arcLengths
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___arcLengths_6;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(LTBezier_tD081588F24017B409080ADD7EC92332888511C41, ___length_0)); }
	inline float get_length_0() const { return ___length_0; }
	inline float* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(float value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_a_1() { return static_cast<int32_t>(offsetof(LTBezier_tD081588F24017B409080ADD7EC92332888511C41, ___a_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_a_1() const { return ___a_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_a_1() { return &___a_1; }
	inline void set_a_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___a_1 = value;
	}

	inline static int32_t get_offset_of_aa_2() { return static_cast<int32_t>(offsetof(LTBezier_tD081588F24017B409080ADD7EC92332888511C41, ___aa_2)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_aa_2() const { return ___aa_2; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_aa_2() { return &___aa_2; }
	inline void set_aa_2(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___aa_2 = value;
	}

	inline static int32_t get_offset_of_bb_3() { return static_cast<int32_t>(offsetof(LTBezier_tD081588F24017B409080ADD7EC92332888511C41, ___bb_3)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_bb_3() const { return ___bb_3; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_bb_3() { return &___bb_3; }
	inline void set_bb_3(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___bb_3 = value;
	}

	inline static int32_t get_offset_of_cc_4() { return static_cast<int32_t>(offsetof(LTBezier_tD081588F24017B409080ADD7EC92332888511C41, ___cc_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_cc_4() const { return ___cc_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_cc_4() { return &___cc_4; }
	inline void set_cc_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___cc_4 = value;
	}

	inline static int32_t get_offset_of_len_5() { return static_cast<int32_t>(offsetof(LTBezier_tD081588F24017B409080ADD7EC92332888511C41, ___len_5)); }
	inline float get_len_5() const { return ___len_5; }
	inline float* get_address_of_len_5() { return &___len_5; }
	inline void set_len_5(float value)
	{
		___len_5 = value;
	}

	inline static int32_t get_offset_of_arcLengths_6() { return static_cast<int32_t>(offsetof(LTBezier_tD081588F24017B409080ADD7EC92332888511C41, ___arcLengths_6)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_arcLengths_6() const { return ___arcLengths_6; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_arcLengths_6() { return &___arcLengths_6; }
	inline void set_arcLengths_6(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___arcLengths_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arcLengths_6), (void*)value);
	}
};


// LTDescrOptional
struct LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2  : public RuntimeObject
{
public:
	// UnityEngine.Transform LTDescrOptional::<toTrans>k__BackingField
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CtoTransU3Ek__BackingField_0;
	// UnityEngine.Vector3 LTDescrOptional::<point>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CpointU3Ek__BackingField_1;
	// UnityEngine.Vector3 LTDescrOptional::<axis>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CaxisU3Ek__BackingField_2;
	// System.Single LTDescrOptional::<lastVal>k__BackingField
	float ___U3ClastValU3Ek__BackingField_3;
	// UnityEngine.Quaternion LTDescrOptional::<origRotation>k__BackingField
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___U3CorigRotationU3Ek__BackingField_4;
	// LTBezierPath LTDescrOptional::<path>k__BackingField
	LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48 * ___U3CpathU3Ek__BackingField_5;
	// LTSpline LTDescrOptional::<spline>k__BackingField
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * ___U3CsplineU3Ek__BackingField_6;
	// UnityEngine.AnimationCurve LTDescrOptional::animationCurve
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___animationCurve_7;
	// System.Int32 LTDescrOptional::initFrameCount
	int32_t ___initFrameCount_8;
	// UnityEngine.Color LTDescrOptional::color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color_9;
	// LTRect LTDescrOptional::<ltRect>k__BackingField
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD * ___U3CltRectU3Ek__BackingField_10;
	// System.Action`1<System.Single> LTDescrOptional::<onUpdateFloat>k__BackingField
	Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * ___U3ConUpdateFloatU3Ek__BackingField_11;
	// System.Action`2<System.Single,System.Single> LTDescrOptional::<onUpdateFloatRatio>k__BackingField
	Action_2_tBA82D2430D3596594262E1ACAD640A8EEB5DB2F2 * ___U3ConUpdateFloatRatioU3Ek__BackingField_12;
	// System.Action`2<System.Single,System.Object> LTDescrOptional::<onUpdateFloatObject>k__BackingField
	Action_2_tC5EA1BA82797894656DD89A0F3E7B791EC8BD050 * ___U3ConUpdateFloatObjectU3Ek__BackingField_13;
	// System.Action`1<UnityEngine.Vector2> LTDescrOptional::<onUpdateVector2>k__BackingField
	Action_1_t0C1F417511439CBAA03909A69138FAF0BD19FA30 * ___U3ConUpdateVector2U3Ek__BackingField_14;
	// System.Action`1<UnityEngine.Vector3> LTDescrOptional::<onUpdateVector3>k__BackingField
	Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B * ___U3ConUpdateVector3U3Ek__BackingField_15;
	// System.Action`2<UnityEngine.Vector3,System.Object> LTDescrOptional::<onUpdateVector3Object>k__BackingField
	Action_2_tCA8676CFBCEAA1EE28321875815FD1767B25E4CB * ___U3ConUpdateVector3ObjectU3Ek__BackingField_16;
	// System.Action`1<UnityEngine.Color> LTDescrOptional::<onUpdateColor>k__BackingField
	Action_1_tC747810051096FEC47140D886015F5428DEDDAE2 * ___U3ConUpdateColorU3Ek__BackingField_17;
	// System.Action`2<UnityEngine.Color,System.Object> LTDescrOptional::<onUpdateColorObject>k__BackingField
	Action_2_t05923E9D06D867E1AC6A90E2B1B2FE28CA9F910A * ___U3ConUpdateColorObjectU3Ek__BackingField_18;
	// System.Action LTDescrOptional::<onComplete>k__BackingField
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3ConCompleteU3Ek__BackingField_19;
	// System.Action`1<System.Object> LTDescrOptional::<onCompleteObject>k__BackingField
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ___U3ConCompleteObjectU3Ek__BackingField_20;
	// System.Object LTDescrOptional::<onCompleteParam>k__BackingField
	RuntimeObject * ___U3ConCompleteParamU3Ek__BackingField_21;
	// System.Object LTDescrOptional::<onUpdateParam>k__BackingField
	RuntimeObject * ___U3ConUpdateParamU3Ek__BackingField_22;
	// System.Action LTDescrOptional::<onStart>k__BackingField
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3ConStartU3Ek__BackingField_23;

public:
	inline static int32_t get_offset_of_U3CtoTransU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3CtoTransU3Ek__BackingField_0)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CtoTransU3Ek__BackingField_0() const { return ___U3CtoTransU3Ek__BackingField_0; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CtoTransU3Ek__BackingField_0() { return &___U3CtoTransU3Ek__BackingField_0; }
	inline void set_U3CtoTransU3Ek__BackingField_0(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CtoTransU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtoTransU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CpointU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3CpointU3Ek__BackingField_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CpointU3Ek__BackingField_1() const { return ___U3CpointU3Ek__BackingField_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CpointU3Ek__BackingField_1() { return &___U3CpointU3Ek__BackingField_1; }
	inline void set_U3CpointU3Ek__BackingField_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CpointU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CaxisU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3CaxisU3Ek__BackingField_2)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CaxisU3Ek__BackingField_2() const { return ___U3CaxisU3Ek__BackingField_2; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CaxisU3Ek__BackingField_2() { return &___U3CaxisU3Ek__BackingField_2; }
	inline void set_U3CaxisU3Ek__BackingField_2(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CaxisU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3ClastValU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ClastValU3Ek__BackingField_3)); }
	inline float get_U3ClastValU3Ek__BackingField_3() const { return ___U3ClastValU3Ek__BackingField_3; }
	inline float* get_address_of_U3ClastValU3Ek__BackingField_3() { return &___U3ClastValU3Ek__BackingField_3; }
	inline void set_U3ClastValU3Ek__BackingField_3(float value)
	{
		___U3ClastValU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CorigRotationU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3CorigRotationU3Ek__BackingField_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_U3CorigRotationU3Ek__BackingField_4() const { return ___U3CorigRotationU3Ek__BackingField_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_U3CorigRotationU3Ek__BackingField_4() { return &___U3CorigRotationU3Ek__BackingField_4; }
	inline void set_U3CorigRotationU3Ek__BackingField_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___U3CorigRotationU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CpathU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3CpathU3Ek__BackingField_5)); }
	inline LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48 * get_U3CpathU3Ek__BackingField_5() const { return ___U3CpathU3Ek__BackingField_5; }
	inline LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48 ** get_address_of_U3CpathU3Ek__BackingField_5() { return &___U3CpathU3Ek__BackingField_5; }
	inline void set_U3CpathU3Ek__BackingField_5(LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48 * value)
	{
		___U3CpathU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CpathU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CsplineU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3CsplineU3Ek__BackingField_6)); }
	inline LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * get_U3CsplineU3Ek__BackingField_6() const { return ___U3CsplineU3Ek__BackingField_6; }
	inline LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 ** get_address_of_U3CsplineU3Ek__BackingField_6() { return &___U3CsplineU3Ek__BackingField_6; }
	inline void set_U3CsplineU3Ek__BackingField_6(LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701 * value)
	{
		___U3CsplineU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsplineU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_animationCurve_7() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___animationCurve_7)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_animationCurve_7() const { return ___animationCurve_7; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_animationCurve_7() { return &___animationCurve_7; }
	inline void set_animationCurve_7(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___animationCurve_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animationCurve_7), (void*)value);
	}

	inline static int32_t get_offset_of_initFrameCount_8() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___initFrameCount_8)); }
	inline int32_t get_initFrameCount_8() const { return ___initFrameCount_8; }
	inline int32_t* get_address_of_initFrameCount_8() { return &___initFrameCount_8; }
	inline void set_initFrameCount_8(int32_t value)
	{
		___initFrameCount_8 = value;
	}

	inline static int32_t get_offset_of_color_9() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___color_9)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_color_9() const { return ___color_9; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_color_9() { return &___color_9; }
	inline void set_color_9(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___color_9 = value;
	}

	inline static int32_t get_offset_of_U3CltRectU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3CltRectU3Ek__BackingField_10)); }
	inline LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD * get_U3CltRectU3Ek__BackingField_10() const { return ___U3CltRectU3Ek__BackingField_10; }
	inline LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD ** get_address_of_U3CltRectU3Ek__BackingField_10() { return &___U3CltRectU3Ek__BackingField_10; }
	inline void set_U3CltRectU3Ek__BackingField_10(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD * value)
	{
		___U3CltRectU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CltRectU3Ek__BackingField_10), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConUpdateFloatU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConUpdateFloatU3Ek__BackingField_11)); }
	inline Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * get_U3ConUpdateFloatU3Ek__BackingField_11() const { return ___U3ConUpdateFloatU3Ek__BackingField_11; }
	inline Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 ** get_address_of_U3ConUpdateFloatU3Ek__BackingField_11() { return &___U3ConUpdateFloatU3Ek__BackingField_11; }
	inline void set_U3ConUpdateFloatU3Ek__BackingField_11(Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * value)
	{
		___U3ConUpdateFloatU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConUpdateFloatU3Ek__BackingField_11), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConUpdateFloatRatioU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConUpdateFloatRatioU3Ek__BackingField_12)); }
	inline Action_2_tBA82D2430D3596594262E1ACAD640A8EEB5DB2F2 * get_U3ConUpdateFloatRatioU3Ek__BackingField_12() const { return ___U3ConUpdateFloatRatioU3Ek__BackingField_12; }
	inline Action_2_tBA82D2430D3596594262E1ACAD640A8EEB5DB2F2 ** get_address_of_U3ConUpdateFloatRatioU3Ek__BackingField_12() { return &___U3ConUpdateFloatRatioU3Ek__BackingField_12; }
	inline void set_U3ConUpdateFloatRatioU3Ek__BackingField_12(Action_2_tBA82D2430D3596594262E1ACAD640A8EEB5DB2F2 * value)
	{
		___U3ConUpdateFloatRatioU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConUpdateFloatRatioU3Ek__BackingField_12), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConUpdateFloatObjectU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConUpdateFloatObjectU3Ek__BackingField_13)); }
	inline Action_2_tC5EA1BA82797894656DD89A0F3E7B791EC8BD050 * get_U3ConUpdateFloatObjectU3Ek__BackingField_13() const { return ___U3ConUpdateFloatObjectU3Ek__BackingField_13; }
	inline Action_2_tC5EA1BA82797894656DD89A0F3E7B791EC8BD050 ** get_address_of_U3ConUpdateFloatObjectU3Ek__BackingField_13() { return &___U3ConUpdateFloatObjectU3Ek__BackingField_13; }
	inline void set_U3ConUpdateFloatObjectU3Ek__BackingField_13(Action_2_tC5EA1BA82797894656DD89A0F3E7B791EC8BD050 * value)
	{
		___U3ConUpdateFloatObjectU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConUpdateFloatObjectU3Ek__BackingField_13), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConUpdateVector2U3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConUpdateVector2U3Ek__BackingField_14)); }
	inline Action_1_t0C1F417511439CBAA03909A69138FAF0BD19FA30 * get_U3ConUpdateVector2U3Ek__BackingField_14() const { return ___U3ConUpdateVector2U3Ek__BackingField_14; }
	inline Action_1_t0C1F417511439CBAA03909A69138FAF0BD19FA30 ** get_address_of_U3ConUpdateVector2U3Ek__BackingField_14() { return &___U3ConUpdateVector2U3Ek__BackingField_14; }
	inline void set_U3ConUpdateVector2U3Ek__BackingField_14(Action_1_t0C1F417511439CBAA03909A69138FAF0BD19FA30 * value)
	{
		___U3ConUpdateVector2U3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConUpdateVector2U3Ek__BackingField_14), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConUpdateVector3U3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConUpdateVector3U3Ek__BackingField_15)); }
	inline Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B * get_U3ConUpdateVector3U3Ek__BackingField_15() const { return ___U3ConUpdateVector3U3Ek__BackingField_15; }
	inline Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B ** get_address_of_U3ConUpdateVector3U3Ek__BackingField_15() { return &___U3ConUpdateVector3U3Ek__BackingField_15; }
	inline void set_U3ConUpdateVector3U3Ek__BackingField_15(Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B * value)
	{
		___U3ConUpdateVector3U3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConUpdateVector3U3Ek__BackingField_15), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConUpdateVector3ObjectU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConUpdateVector3ObjectU3Ek__BackingField_16)); }
	inline Action_2_tCA8676CFBCEAA1EE28321875815FD1767B25E4CB * get_U3ConUpdateVector3ObjectU3Ek__BackingField_16() const { return ___U3ConUpdateVector3ObjectU3Ek__BackingField_16; }
	inline Action_2_tCA8676CFBCEAA1EE28321875815FD1767B25E4CB ** get_address_of_U3ConUpdateVector3ObjectU3Ek__BackingField_16() { return &___U3ConUpdateVector3ObjectU3Ek__BackingField_16; }
	inline void set_U3ConUpdateVector3ObjectU3Ek__BackingField_16(Action_2_tCA8676CFBCEAA1EE28321875815FD1767B25E4CB * value)
	{
		___U3ConUpdateVector3ObjectU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConUpdateVector3ObjectU3Ek__BackingField_16), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConUpdateColorU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConUpdateColorU3Ek__BackingField_17)); }
	inline Action_1_tC747810051096FEC47140D886015F5428DEDDAE2 * get_U3ConUpdateColorU3Ek__BackingField_17() const { return ___U3ConUpdateColorU3Ek__BackingField_17; }
	inline Action_1_tC747810051096FEC47140D886015F5428DEDDAE2 ** get_address_of_U3ConUpdateColorU3Ek__BackingField_17() { return &___U3ConUpdateColorU3Ek__BackingField_17; }
	inline void set_U3ConUpdateColorU3Ek__BackingField_17(Action_1_tC747810051096FEC47140D886015F5428DEDDAE2 * value)
	{
		___U3ConUpdateColorU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConUpdateColorU3Ek__BackingField_17), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConUpdateColorObjectU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConUpdateColorObjectU3Ek__BackingField_18)); }
	inline Action_2_t05923E9D06D867E1AC6A90E2B1B2FE28CA9F910A * get_U3ConUpdateColorObjectU3Ek__BackingField_18() const { return ___U3ConUpdateColorObjectU3Ek__BackingField_18; }
	inline Action_2_t05923E9D06D867E1AC6A90E2B1B2FE28CA9F910A ** get_address_of_U3ConUpdateColorObjectU3Ek__BackingField_18() { return &___U3ConUpdateColorObjectU3Ek__BackingField_18; }
	inline void set_U3ConUpdateColorObjectU3Ek__BackingField_18(Action_2_t05923E9D06D867E1AC6A90E2B1B2FE28CA9F910A * value)
	{
		___U3ConUpdateColorObjectU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConUpdateColorObjectU3Ek__BackingField_18), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConCompleteU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConCompleteU3Ek__BackingField_19)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3ConCompleteU3Ek__BackingField_19() const { return ___U3ConCompleteU3Ek__BackingField_19; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3ConCompleteU3Ek__BackingField_19() { return &___U3ConCompleteU3Ek__BackingField_19; }
	inline void set_U3ConCompleteU3Ek__BackingField_19(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3ConCompleteU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConCompleteU3Ek__BackingField_19), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConCompleteObjectU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConCompleteObjectU3Ek__BackingField_20)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get_U3ConCompleteObjectU3Ek__BackingField_20() const { return ___U3ConCompleteObjectU3Ek__BackingField_20; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of_U3ConCompleteObjectU3Ek__BackingField_20() { return &___U3ConCompleteObjectU3Ek__BackingField_20; }
	inline void set_U3ConCompleteObjectU3Ek__BackingField_20(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		___U3ConCompleteObjectU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConCompleteObjectU3Ek__BackingField_20), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConCompleteParamU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConCompleteParamU3Ek__BackingField_21)); }
	inline RuntimeObject * get_U3ConCompleteParamU3Ek__BackingField_21() const { return ___U3ConCompleteParamU3Ek__BackingField_21; }
	inline RuntimeObject ** get_address_of_U3ConCompleteParamU3Ek__BackingField_21() { return &___U3ConCompleteParamU3Ek__BackingField_21; }
	inline void set_U3ConCompleteParamU3Ek__BackingField_21(RuntimeObject * value)
	{
		___U3ConCompleteParamU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConCompleteParamU3Ek__BackingField_21), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConUpdateParamU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConUpdateParamU3Ek__BackingField_22)); }
	inline RuntimeObject * get_U3ConUpdateParamU3Ek__BackingField_22() const { return ___U3ConUpdateParamU3Ek__BackingField_22; }
	inline RuntimeObject ** get_address_of_U3ConUpdateParamU3Ek__BackingField_22() { return &___U3ConUpdateParamU3Ek__BackingField_22; }
	inline void set_U3ConUpdateParamU3Ek__BackingField_22(RuntimeObject * value)
	{
		___U3ConUpdateParamU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConUpdateParamU3Ek__BackingField_22), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConStartU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2, ___U3ConStartU3Ek__BackingField_23)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3ConStartU3Ek__BackingField_23() const { return ___U3ConStartU3Ek__BackingField_23; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3ConStartU3Ek__BackingField_23() { return &___U3ConStartU3Ek__BackingField_23; }
	inline void set_U3ConStartU3Ek__BackingField_23(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3ConStartU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ConStartU3Ek__BackingField_23), (void*)value);
	}
};


// LTGUI
struct LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816  : public RuntimeObject
{
public:

public:
};

struct LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields
{
public:
	// System.Int32 LTGUI::RECT_LEVELS
	int32_t ___RECT_LEVELS_0;
	// System.Int32 LTGUI::RECTS_PER_LEVEL
	int32_t ___RECTS_PER_LEVEL_1;
	// System.Int32 LTGUI::BUTTONS_MAX
	int32_t ___BUTTONS_MAX_2;
	// LTRect[] LTGUI::levels
	LTRectU5BU5D_t764F00C0F4846E8D85477368EC19E34DF5D0DCBA* ___levels_3;
	// System.Int32[] LTGUI::levelDepths
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___levelDepths_4;
	// UnityEngine.Rect[] LTGUI::buttons
	RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* ___buttons_5;
	// System.Int32[] LTGUI::buttonLevels
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buttonLevels_6;
	// System.Int32[] LTGUI::buttonLastFrame
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buttonLastFrame_7;
	// LTRect LTGUI::r
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD * ___r_8;
	// UnityEngine.Color LTGUI::color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color_9;
	// System.Boolean LTGUI::isGUIEnabled
	bool ___isGUIEnabled_10;
	// System.Int32 LTGUI::global_counter
	int32_t ___global_counter_11;

public:
	inline static int32_t get_offset_of_RECT_LEVELS_0() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___RECT_LEVELS_0)); }
	inline int32_t get_RECT_LEVELS_0() const { return ___RECT_LEVELS_0; }
	inline int32_t* get_address_of_RECT_LEVELS_0() { return &___RECT_LEVELS_0; }
	inline void set_RECT_LEVELS_0(int32_t value)
	{
		___RECT_LEVELS_0 = value;
	}

	inline static int32_t get_offset_of_RECTS_PER_LEVEL_1() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___RECTS_PER_LEVEL_1)); }
	inline int32_t get_RECTS_PER_LEVEL_1() const { return ___RECTS_PER_LEVEL_1; }
	inline int32_t* get_address_of_RECTS_PER_LEVEL_1() { return &___RECTS_PER_LEVEL_1; }
	inline void set_RECTS_PER_LEVEL_1(int32_t value)
	{
		___RECTS_PER_LEVEL_1 = value;
	}

	inline static int32_t get_offset_of_BUTTONS_MAX_2() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___BUTTONS_MAX_2)); }
	inline int32_t get_BUTTONS_MAX_2() const { return ___BUTTONS_MAX_2; }
	inline int32_t* get_address_of_BUTTONS_MAX_2() { return &___BUTTONS_MAX_2; }
	inline void set_BUTTONS_MAX_2(int32_t value)
	{
		___BUTTONS_MAX_2 = value;
	}

	inline static int32_t get_offset_of_levels_3() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___levels_3)); }
	inline LTRectU5BU5D_t764F00C0F4846E8D85477368EC19E34DF5D0DCBA* get_levels_3() const { return ___levels_3; }
	inline LTRectU5BU5D_t764F00C0F4846E8D85477368EC19E34DF5D0DCBA** get_address_of_levels_3() { return &___levels_3; }
	inline void set_levels_3(LTRectU5BU5D_t764F00C0F4846E8D85477368EC19E34DF5D0DCBA* value)
	{
		___levels_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___levels_3), (void*)value);
	}

	inline static int32_t get_offset_of_levelDepths_4() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___levelDepths_4)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_levelDepths_4() const { return ___levelDepths_4; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_levelDepths_4() { return &___levelDepths_4; }
	inline void set_levelDepths_4(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___levelDepths_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___levelDepths_4), (void*)value);
	}

	inline static int32_t get_offset_of_buttons_5() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___buttons_5)); }
	inline RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* get_buttons_5() const { return ___buttons_5; }
	inline RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE** get_address_of_buttons_5() { return &___buttons_5; }
	inline void set_buttons_5(RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* value)
	{
		___buttons_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buttons_5), (void*)value);
	}

	inline static int32_t get_offset_of_buttonLevels_6() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___buttonLevels_6)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buttonLevels_6() const { return ___buttonLevels_6; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buttonLevels_6() { return &___buttonLevels_6; }
	inline void set_buttonLevels_6(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buttonLevels_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buttonLevels_6), (void*)value);
	}

	inline static int32_t get_offset_of_buttonLastFrame_7() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___buttonLastFrame_7)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buttonLastFrame_7() const { return ___buttonLastFrame_7; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buttonLastFrame_7() { return &___buttonLastFrame_7; }
	inline void set_buttonLastFrame_7(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buttonLastFrame_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buttonLastFrame_7), (void*)value);
	}

	inline static int32_t get_offset_of_r_8() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___r_8)); }
	inline LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD * get_r_8() const { return ___r_8; }
	inline LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD ** get_address_of_r_8() { return &___r_8; }
	inline void set_r_8(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD * value)
	{
		___r_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___r_8), (void*)value);
	}

	inline static int32_t get_offset_of_color_9() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___color_9)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_color_9() const { return ___color_9; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_color_9() { return &___color_9; }
	inline void set_color_9(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___color_9 = value;
	}

	inline static int32_t get_offset_of_isGUIEnabled_10() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___isGUIEnabled_10)); }
	inline bool get_isGUIEnabled_10() const { return ___isGUIEnabled_10; }
	inline bool* get_address_of_isGUIEnabled_10() { return &___isGUIEnabled_10; }
	inline void set_isGUIEnabled_10(bool value)
	{
		___isGUIEnabled_10 = value;
	}

	inline static int32_t get_offset_of_global_counter_11() { return static_cast<int32_t>(offsetof(LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields, ___global_counter_11)); }
	inline int32_t get_global_counter_11() const { return ___global_counter_11; }
	inline int32_t* get_address_of_global_counter_11() { return &___global_counter_11; }
	inline void set_global_counter_11(int32_t value)
	{
		___global_counter_11 = value;
	}
};


// LeanProp
struct LeanProp_tEF273BF6EB0960483D03A097A40313D700DE3538 
{
public:
	// System.Int32 LeanProp::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LeanProp_tEF273BF6EB0960483D03A097A40313D700DE3538, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// LeanTweenType
struct LeanTweenType_tAE51C34373F1326AC0BB9DB0F7EF1883603D55A9 
{
public:
	// System.Int32 LeanTweenType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LeanTweenType_tAE51C34373F1326AC0BB9DB0F7EF1883603D55A9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Purchasing.ProductType
struct ProductType_tBF332314E0B8C2094184DDA4751FDB3518A79D5A 
{
public:
	// System.Int32 UnityEngine.Purchasing.ProductType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ProductType_tBF332314E0B8C2094184DDA4751FDB3518A79D5A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// StarData
struct StarData_t2FA473959759AC85532D9EC701C3715E28D2219E  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 StarData::position
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position_0;
	// UnityEngine.Vector3 StarData::rotation
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rotation_1;
	// UnityEngine.Color StarData::color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color_2;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(StarData_t2FA473959759AC85532D9EC701C3715E28D2219E, ___position_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_position_0() const { return ___position_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(StarData_t2FA473959759AC85532D9EC701C3715E28D2219E, ___rotation_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rotation_1() const { return ___rotation_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rotation_1 = value;
	}

	inline static int32_t get_offset_of_color_2() { return static_cast<int32_t>(offsetof(StarData_t2FA473959759AC85532D9EC701C3715E28D2219E, ___color_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_color_2() const { return ___color_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_color_2() { return &___color_2; }
	inline void set_color_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___color_2 = value;
	}
};


// System.TimeSpan
struct TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_22;

public:
	inline static int32_t get_offset_of__ticks_22() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203, ____ticks_22)); }
	inline int64_t get__ticks_22() const { return ____ticks_22; }
	inline int64_t* get_address_of__ticks_22() { return &____ticks_22; }
	inline void set__ticks_22(int64_t value)
	{
		____ticks_22 = value;
	}
};

struct TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___Zero_19;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___MaxValue_20;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___MinValue_21;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_24;

public:
	inline static int32_t get_offset_of_Zero_19() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___Zero_19)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_Zero_19() const { return ___Zero_19; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_Zero_19() { return &___Zero_19; }
	inline void set_Zero_19(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___Zero_19 = value;
	}

	inline static int32_t get_offset_of_MaxValue_20() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___MaxValue_20)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_MaxValue_20() const { return ___MaxValue_20; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_MaxValue_20() { return &___MaxValue_20; }
	inline void set_MaxValue_20(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_MinValue_21() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ___MinValue_21)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_MinValue_21() const { return ___MinValue_21; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_MinValue_21() { return &___MinValue_21; }
	inline void set_MinValue_21(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___MinValue_21 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_23() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ____legacyConfigChecked_23)); }
	inline bool get__legacyConfigChecked_23() const { return ____legacyConfigChecked_23; }
	inline bool* get_address_of__legacyConfigChecked_23() { return &____legacyConfigChecked_23; }
	inline void set__legacyConfigChecked_23(bool value)
	{
		____legacyConfigChecked_23 = value;
	}

	inline static int32_t get_offset_of__legacyMode_24() { return static_cast<int32_t>(offsetof(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203_StaticFields, ____legacyMode_24)); }
	inline bool get__legacyMode_24() const { return ____legacyMode_24; }
	inline bool* get_address_of__legacyMode_24() { return &____legacyMode_24; }
	inline void set__legacyMode_24(bool value)
	{
		____legacyMode_24 = value;
	}
};


// TweenAction
struct TweenAction_tB7B9473B02F7CC04ED4083C3934285274119AE0B 
{
public:
	// System.Int32 TweenAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TweenAction_tB7B9473B02F7CC04ED4083C3934285274119AE0B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// CSGameStore/<>c__DisplayClass25_0
struct U3CU3Ec__DisplayClass25_0_t9017258DBD08C6FFAA044EA2B01242385746C17F  : public RuntimeObject
{
public:
	// UnityEngine.Color CSGameStore/<>c__DisplayClass25_0::color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color_0;
	// UnityEngine.UI.Image CSGameStore/<>c__DisplayClass25_0::image
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___image_1;

public:
	inline static int32_t get_offset_of_color_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass25_0_t9017258DBD08C6FFAA044EA2B01242385746C17F, ___color_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_color_0() const { return ___color_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_color_0() { return &___color_0; }
	inline void set_color_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___color_0 = value;
	}

	inline static int32_t get_offset_of_image_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass25_0_t9017258DBD08C6FFAA044EA2B01242385746C17F, ___image_1)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_image_1() const { return ___image_1; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_image_1() { return &___image_1; }
	inline void set_image_1(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___image_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___image_1), (void*)value);
	}
};


// CSInfo/<>c__DisplayClass17_0
struct U3CU3Ec__DisplayClass17_0_tEB2C08B0350F5EF9B0D4987C5D7758335B4E8EA4  : public RuntimeObject
{
public:
	// UnityEngine.Color CSInfo/<>c__DisplayClass17_0::color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color_0;
	// UnityEngine.UI.Image CSInfo/<>c__DisplayClass17_0::image
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___image_1;

public:
	inline static int32_t get_offset_of_color_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_tEB2C08B0350F5EF9B0D4987C5D7758335B4E8EA4, ___color_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_color_0() const { return ___color_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_color_0() { return &___color_0; }
	inline void set_color_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___color_0 = value;
	}

	inline static int32_t get_offset_of_image_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_tEB2C08B0350F5EF9B0D4987C5D7758335B4E8EA4, ___image_1)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_image_1() const { return ___image_1; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_image_1() { return &___image_1; }
	inline void set_image_1(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___image_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___image_1), (void*)value);
	}
};


// CSReel/<>c__DisplayClass17_0
struct U3CU3Ec__DisplayClass17_0_t32F44E19E309C7E6C4B68A468C4CA94053003A60  : public RuntimeObject
{
public:
	// System.Single CSReel/<>c__DisplayClass17_0::h
	float ___h_0;
	// CSReel CSReel/<>c__DisplayClass17_0::<>4__this
	CSReel_t8454490940D72C04785CF2BF71C41073113E1890 * ___U3CU3E4__this_1;
	// System.Single CSReel/<>c__DisplayClass17_0::prev
	float ___prev_2;
	// UnityEngine.Vector3 CSReel/<>c__DisplayClass17_0::min
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___min_3;
	// UnityEngine.Vector3 CSReel/<>c__DisplayClass17_0::max
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___max_4;
	// System.Int32 CSReel/<>c__DisplayClass17_0::lastRoll
	int32_t ___lastRoll_5;
	// System.Int32 CSReel/<>c__DisplayClass17_0::count
	int32_t ___count_6;

public:
	inline static int32_t get_offset_of_h_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t32F44E19E309C7E6C4B68A468C4CA94053003A60, ___h_0)); }
	inline float get_h_0() const { return ___h_0; }
	inline float* get_address_of_h_0() { return &___h_0; }
	inline void set_h_0(float value)
	{
		___h_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t32F44E19E309C7E6C4B68A468C4CA94053003A60, ___U3CU3E4__this_1)); }
	inline CSReel_t8454490940D72C04785CF2BF71C41073113E1890 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline CSReel_t8454490940D72C04785CF2BF71C41073113E1890 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(CSReel_t8454490940D72C04785CF2BF71C41073113E1890 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}

	inline static int32_t get_offset_of_prev_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t32F44E19E309C7E6C4B68A468C4CA94053003A60, ___prev_2)); }
	inline float get_prev_2() const { return ___prev_2; }
	inline float* get_address_of_prev_2() { return &___prev_2; }
	inline void set_prev_2(float value)
	{
		___prev_2 = value;
	}

	inline static int32_t get_offset_of_min_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t32F44E19E309C7E6C4B68A468C4CA94053003A60, ___min_3)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_min_3() const { return ___min_3; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_min_3() { return &___min_3; }
	inline void set_min_3(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___min_3 = value;
	}

	inline static int32_t get_offset_of_max_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t32F44E19E309C7E6C4B68A468C4CA94053003A60, ___max_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_max_4() const { return ___max_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_max_4() { return &___max_4; }
	inline void set_max_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___max_4 = value;
	}

	inline static int32_t get_offset_of_lastRoll_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t32F44E19E309C7E6C4B68A468C4CA94053003A60, ___lastRoll_5)); }
	inline int32_t get_lastRoll_5() const { return ___lastRoll_5; }
	inline int32_t* get_address_of_lastRoll_5() { return &___lastRoll_5; }
	inline void set_lastRoll_5(int32_t value)
	{
		___lastRoll_5 = value;
	}

	inline static int32_t get_offset_of_count_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t32F44E19E309C7E6C4B68A468C4CA94053003A60, ___count_6)); }
	inline int32_t get_count_6() const { return ___count_6; }
	inline int32_t* get_address_of_count_6() { return &___count_6; }
	inline void set_count_6(int32_t value)
	{
		___count_6 = value;
	}
};


// CSSettingsPanel/<>c__DisplayClass21_0
struct U3CU3Ec__DisplayClass21_0_t1469F90E7A3CCE2352886D7ADB52106B3D49C40D  : public RuntimeObject
{
public:
	// UnityEngine.Color CSSettingsPanel/<>c__DisplayClass21_0::color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color_0;
	// UnityEngine.UI.Image CSSettingsPanel/<>c__DisplayClass21_0::image
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___image_1;

public:
	inline static int32_t get_offset_of_color_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_t1469F90E7A3CCE2352886D7ADB52106B3D49C40D, ___color_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_color_0() const { return ___color_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_color_0() { return &___color_0; }
	inline void set_color_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___color_0 = value;
	}

	inline static int32_t get_offset_of_image_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_t1469F90E7A3CCE2352886D7ADB52106B3D49C40D, ___image_1)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_image_1() const { return ___image_1; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_image_1() { return &___image_1; }
	inline void set_image_1(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___image_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___image_1), (void*)value);
	}
};


// TMPro.Examples.CameraController/CameraModes
struct CameraModes_t86C030D336478B1715780ECE7AB12A629AD0ACE0 
{
public:
	// System.Int32 TMPro.Examples.CameraController/CameraModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraModes_t86C030D336478B1715780ECE7AB12A629AD0ACE0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// EnvMapAnimator/<Start>d__4
struct U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266  : public RuntimeObject
{
public:
	// System.Int32 EnvMapAnimator/<Start>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object EnvMapAnimator/<Start>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// EnvMapAnimator EnvMapAnimator/<Start>d__4::<>4__this
	EnvMapAnimator_t7C30A094A3786710A896AD9A6FA46B13EFEB72FD * ___U3CU3E4__this_2;
	// UnityEngine.Matrix4x4 EnvMapAnimator/<Start>d__4::<matrix>5__2
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___U3CmatrixU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266, ___U3CU3E4__this_2)); }
	inline EnvMapAnimator_t7C30A094A3786710A896AD9A6FA46B13EFEB72FD * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline EnvMapAnimator_t7C30A094A3786710A896AD9A6FA46B13EFEB72FD ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(EnvMapAnimator_t7C30A094A3786710A896AD9A6FA46B13EFEB72FD * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CmatrixU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266, ___U3CmatrixU3E5__2_3)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_U3CmatrixU3E5__2_3() const { return ___U3CmatrixU3E5__2_3; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_U3CmatrixU3E5__2_3() { return &___U3CmatrixU3E5__2_3; }
	inline void set_U3CmatrixU3E5__2_3(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___U3CmatrixU3E5__2_3 = value;
	}
};


// LTGUI/Element_Type
struct Element_Type_tCB9236487D710B365968D3F055C4CCCA555A6BFE 
{
public:
	// System.Int32 LTGUI/Element_Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Element_Type_tCB9236487D710B365968D3F055C4CCCA555A6BFE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TMPro.Examples.ObjectSpin/MotionType
struct MotionType_t7C60A9BB5280294C6FBC1DB5E747EFF94A342BF5 
{
public:
	// System.Int32 TMPro.Examples.ObjectSpin/MotionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MotionType_t7C60A9BB5280294C6FBC1DB5E747EFF94A342BF5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// ScrollSnapBase/SelectionChangeEndEvent
struct SelectionChangeEndEvent_t61026CA54A4A81045E6FA5F1D8C79E556527C29E  : public UnityEvent_1_tB235B5DAD099AC425DC059D10DEB8B97A35E2BBF
{
public:

public:
};


// ScrollSnapBase/SelectionChangeStartEvent
struct SelectionChangeStartEvent_tE0687452817ACBB2DEECBBF8D2E996D0137F7FE0  : public UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4
{
public:

public:
};


// ScrollSnapBase/SelectionPageChangedEvent
struct SelectionPageChangedEvent_tB33544091EB61DE45C9FF3A71E4DA084BDAA6DBC  : public UnityEvent_1_tB235B5DAD099AC425DC059D10DEB8B97A35E2BBF
{
public:

public:
};


// TMPro.Examples.TMP_ExampleScript_01/objectType
struct objectType_t5159C717EF79DEA9C8F7CF5F830EE5D4C4560036 
{
public:
	// System.Int32 TMPro.Examples.TMP_ExampleScript_01/objectType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(objectType_t5159C717EF79DEA9C8F7CF5F830EE5D4C4560036, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions
struct FpsCounterAnchorPositions_tD7297D86C9462F5BC670EC9B05049FA736903F03 
{
public:
	// System.Int32 TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FpsCounterAnchorPositions_tD7297D86C9462F5BC670EC9B05049FA736903F03, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TMPro.TMP_TextEventHandler/CharacterSelectionEvent
struct CharacterSelectionEvent_t3F24E19931D51E4616A11C4D690BC0B3D738D64F  : public UnityEvent_2_t5304A7BB9DB39BB3BA59CAF408F8EA7341658E2C
{
public:

public:
};


// TMPro.TMP_TextEventHandler/LineSelectionEvent
struct LineSelectionEvent_tBD0FAC01BF9DE63FE60817F0B760E24E951C4712  : public UnityEvent_3_tB2C1BFEE5A56978DECD9BA6756512E2CC49CB9FE
{
public:

public:
};


// TMPro.TMP_TextEventHandler/LinkSelectionEvent
struct LinkSelectionEvent_tDDEFCE74540F682E52A3445C536D3F29A91F98AF  : public UnityEvent_3_t99EBCF5B18C77267336F36583A2B69BE0535E6E9
{
public:

public:
};


// TMPro.TMP_TextEventHandler/SpriteSelectionEvent
struct SpriteSelectionEvent_tAF7524A6DF6D4849E206726E3AC085BCB91B4618  : public UnityEvent_2_t5304A7BB9DB39BB3BA59CAF408F8EA7341658E2C
{
public:

public:
};


// TMPro.TMP_TextEventHandler/WordSelectionEvent
struct WordSelectionEvent_t3FE09C7D7E726DC634003A28E2B3CB78B42AE6F8  : public UnityEvent_3_tB2C1BFEE5A56978DECD9BA6756512E2CC49CB9FE
{
public:

public:
};


// TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions
struct FpsCounterAnchorPositions_t1E46DD5A81E44E1D3F7140DD75ABBBCB9B52DDCC 
{
public:
	// System.Int32 TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FpsCounterAnchorPositions_t1E46DD5A81E44E1D3F7140DD75ABBBCB9B52DDCC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions
struct FpsCounterAnchorPositions_t8925F6C50F79F9AFA64E5BF6270769F3A3634927 
{
public:
	// System.Int32 TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FpsCounterAnchorPositions_t8925F6C50F79F9AFA64E5BF6270769F3A3634927, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0
struct U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F  : public RuntimeObject
{
public:
	// DentedPixel.LTExamples.TestingUnitTests DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<>4__this
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * ___U3CU3E4__this_0;
	// UnityEngine.GameObject[] DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubes
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___cubes_1;
	// System.Int32[] DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::tweenIds
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___tweenIds_2;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::onCompleteCount
	int32_t ___onCompleteCount_3;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubeToTrans
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeToTrans_4;
	// UnityEngine.Vector3 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubeDestEnd
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___cubeDestEnd_5;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubeSpline
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeSpline_6;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::jumpTimeId
	int32_t ___jumpTimeId_7;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::jumpCube
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___jumpCube_8;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::zeroCube
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___zeroCube_9;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubeScale
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeScale_10;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubeRotate
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeRotate_11;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubeRotateA
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeRotateA_12;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::cubeRotateB
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeRotateB_13;
	// System.Single DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::onStartTime
	float ___onStartTime_14;
	// UnityEngine.Vector3 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::beforePos
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___beforePos_15;
	// UnityEngine.Vector3 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::beforePos2
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___beforePos2_16;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::totalEasingCheck
	int32_t ___totalEasingCheck_17;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::totalEasingCheckSuccess
	int32_t ___totalEasingCheckSuccess_18;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::value2UpdateCalled
	bool ___value2UpdateCalled_19;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<>9__21
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__21_20;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___U3CU3E4__this_0)); }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_cubes_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___cubes_1)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_cubes_1() const { return ___cubes_1; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_cubes_1() { return &___cubes_1; }
	inline void set_cubes_1(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___cubes_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubes_1), (void*)value);
	}

	inline static int32_t get_offset_of_tweenIds_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___tweenIds_2)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_tweenIds_2() const { return ___tweenIds_2; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_tweenIds_2() { return &___tweenIds_2; }
	inline void set_tweenIds_2(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___tweenIds_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tweenIds_2), (void*)value);
	}

	inline static int32_t get_offset_of_onCompleteCount_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___onCompleteCount_3)); }
	inline int32_t get_onCompleteCount_3() const { return ___onCompleteCount_3; }
	inline int32_t* get_address_of_onCompleteCount_3() { return &___onCompleteCount_3; }
	inline void set_onCompleteCount_3(int32_t value)
	{
		___onCompleteCount_3 = value;
	}

	inline static int32_t get_offset_of_cubeToTrans_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___cubeToTrans_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeToTrans_4() const { return ___cubeToTrans_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeToTrans_4() { return &___cubeToTrans_4; }
	inline void set_cubeToTrans_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeToTrans_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeToTrans_4), (void*)value);
	}

	inline static int32_t get_offset_of_cubeDestEnd_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___cubeDestEnd_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_cubeDestEnd_5() const { return ___cubeDestEnd_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_cubeDestEnd_5() { return &___cubeDestEnd_5; }
	inline void set_cubeDestEnd_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___cubeDestEnd_5 = value;
	}

	inline static int32_t get_offset_of_cubeSpline_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___cubeSpline_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeSpline_6() const { return ___cubeSpline_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeSpline_6() { return &___cubeSpline_6; }
	inline void set_cubeSpline_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeSpline_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeSpline_6), (void*)value);
	}

	inline static int32_t get_offset_of_jumpTimeId_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___jumpTimeId_7)); }
	inline int32_t get_jumpTimeId_7() const { return ___jumpTimeId_7; }
	inline int32_t* get_address_of_jumpTimeId_7() { return &___jumpTimeId_7; }
	inline void set_jumpTimeId_7(int32_t value)
	{
		___jumpTimeId_7 = value;
	}

	inline static int32_t get_offset_of_jumpCube_8() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___jumpCube_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_jumpCube_8() const { return ___jumpCube_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_jumpCube_8() { return &___jumpCube_8; }
	inline void set_jumpCube_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___jumpCube_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___jumpCube_8), (void*)value);
	}

	inline static int32_t get_offset_of_zeroCube_9() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___zeroCube_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_zeroCube_9() const { return ___zeroCube_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_zeroCube_9() { return &___zeroCube_9; }
	inline void set_zeroCube_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___zeroCube_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___zeroCube_9), (void*)value);
	}

	inline static int32_t get_offset_of_cubeScale_10() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___cubeScale_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeScale_10() const { return ___cubeScale_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeScale_10() { return &___cubeScale_10; }
	inline void set_cubeScale_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeScale_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeScale_10), (void*)value);
	}

	inline static int32_t get_offset_of_cubeRotate_11() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___cubeRotate_11)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeRotate_11() const { return ___cubeRotate_11; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeRotate_11() { return &___cubeRotate_11; }
	inline void set_cubeRotate_11(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeRotate_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeRotate_11), (void*)value);
	}

	inline static int32_t get_offset_of_cubeRotateA_12() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___cubeRotateA_12)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeRotateA_12() const { return ___cubeRotateA_12; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeRotateA_12() { return &___cubeRotateA_12; }
	inline void set_cubeRotateA_12(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeRotateA_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeRotateA_12), (void*)value);
	}

	inline static int32_t get_offset_of_cubeRotateB_13() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___cubeRotateB_13)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeRotateB_13() const { return ___cubeRotateB_13; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeRotateB_13() { return &___cubeRotateB_13; }
	inline void set_cubeRotateB_13(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeRotateB_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeRotateB_13), (void*)value);
	}

	inline static int32_t get_offset_of_onStartTime_14() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___onStartTime_14)); }
	inline float get_onStartTime_14() const { return ___onStartTime_14; }
	inline float* get_address_of_onStartTime_14() { return &___onStartTime_14; }
	inline void set_onStartTime_14(float value)
	{
		___onStartTime_14 = value;
	}

	inline static int32_t get_offset_of_beforePos_15() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___beforePos_15)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_beforePos_15() const { return ___beforePos_15; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_beforePos_15() { return &___beforePos_15; }
	inline void set_beforePos_15(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___beforePos_15 = value;
	}

	inline static int32_t get_offset_of_beforePos2_16() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___beforePos2_16)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_beforePos2_16() const { return ___beforePos2_16; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_beforePos2_16() { return &___beforePos2_16; }
	inline void set_beforePos2_16(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___beforePos2_16 = value;
	}

	inline static int32_t get_offset_of_totalEasingCheck_17() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___totalEasingCheck_17)); }
	inline int32_t get_totalEasingCheck_17() const { return ___totalEasingCheck_17; }
	inline int32_t* get_address_of_totalEasingCheck_17() { return &___totalEasingCheck_17; }
	inline void set_totalEasingCheck_17(int32_t value)
	{
		___totalEasingCheck_17 = value;
	}

	inline static int32_t get_offset_of_totalEasingCheckSuccess_18() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___totalEasingCheckSuccess_18)); }
	inline int32_t get_totalEasingCheckSuccess_18() const { return ___totalEasingCheckSuccess_18; }
	inline int32_t* get_address_of_totalEasingCheckSuccess_18() { return &___totalEasingCheckSuccess_18; }
	inline void set_totalEasingCheckSuccess_18(int32_t value)
	{
		___totalEasingCheckSuccess_18 = value;
	}

	inline static int32_t get_offset_of_value2UpdateCalled_19() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___value2UpdateCalled_19)); }
	inline bool get_value2UpdateCalled_19() const { return ___value2UpdateCalled_19; }
	inline bool* get_address_of_value2UpdateCalled_19() { return &___value2UpdateCalled_19; }
	inline void set_value2UpdateCalled_19(bool value)
	{
		___value2UpdateCalled_19 = value;
	}

	inline static int32_t get_offset_of_U3CU3E9__21_20() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F, ___U3CU3E9__21_20)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__21_20() const { return ___U3CU3E9__21_20; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__21_20() { return &___U3CU3E9__21_20; }
	inline void set_U3CU3E9__21_20(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__21_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__21_20), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0
struct U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F  : public RuntimeObject
{
public:
	// DentedPixel.LTExamples.TestingUnitTests DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<>4__this
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * ___U3CU3E4__this_0;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::pauseCount
	int32_t ___pauseCount_1;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::cubeRound
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeRound_2;
	// UnityEngine.Vector3 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::onStartPos
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___onStartPos_3;
	// UnityEngine.Vector3 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::onStartPosSpline
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___onStartPosSpline_4;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::cubeSpline
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeSpline_5;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::cubeSeq
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeSeq_6;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::cubeBounds
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeBounds_7;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::didPassBounds
	bool ___didPassBounds_8;
	// UnityEngine.Vector3 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::failPoint
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___failPoint_9;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::setOnStartNum
	int32_t ___setOnStartNum_10;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::setPosOnUpdate
	bool ___setPosOnUpdate_11;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::setPosNum
	int32_t ___setPosNum_12;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::hasGroupTweensCheckStarted
	bool ___hasGroupTweensCheckStarted_13;
	// System.Single DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::previousXlt4
	float ___previousXlt4_14;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::onUpdateWasCalled
	bool ___onUpdateWasCalled_15;
	// System.Single DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::start
	float ___start_16;
	// System.Single DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::expectedTime
	float ___expectedTime_17;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::didGetCorrectOnUpdate
	bool ___didGetCorrectOnUpdate_18;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<>9__13
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__13_19;
	// System.Action`1<UnityEngine.Vector3> DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<>9__14
	Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B * ___U3CU3E9__14_20;
	// System.Action DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<>9__16
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___U3CU3E9__16_21;
	// System.Action`1<System.Object> DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<>9__15
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ___U3CU3E9__15_22;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___U3CU3E4__this_0)); }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_pauseCount_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___pauseCount_1)); }
	inline int32_t get_pauseCount_1() const { return ___pauseCount_1; }
	inline int32_t* get_address_of_pauseCount_1() { return &___pauseCount_1; }
	inline void set_pauseCount_1(int32_t value)
	{
		___pauseCount_1 = value;
	}

	inline static int32_t get_offset_of_cubeRound_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___cubeRound_2)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeRound_2() const { return ___cubeRound_2; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeRound_2() { return &___cubeRound_2; }
	inline void set_cubeRound_2(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeRound_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeRound_2), (void*)value);
	}

	inline static int32_t get_offset_of_onStartPos_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___onStartPos_3)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_onStartPos_3() const { return ___onStartPos_3; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_onStartPos_3() { return &___onStartPos_3; }
	inline void set_onStartPos_3(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___onStartPos_3 = value;
	}

	inline static int32_t get_offset_of_onStartPosSpline_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___onStartPosSpline_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_onStartPosSpline_4() const { return ___onStartPosSpline_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_onStartPosSpline_4() { return &___onStartPosSpline_4; }
	inline void set_onStartPosSpline_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___onStartPosSpline_4 = value;
	}

	inline static int32_t get_offset_of_cubeSpline_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___cubeSpline_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeSpline_5() const { return ___cubeSpline_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeSpline_5() { return &___cubeSpline_5; }
	inline void set_cubeSpline_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeSpline_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeSpline_5), (void*)value);
	}

	inline static int32_t get_offset_of_cubeSeq_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___cubeSeq_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeSeq_6() const { return ___cubeSeq_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeSeq_6() { return &___cubeSeq_6; }
	inline void set_cubeSeq_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeSeq_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeSeq_6), (void*)value);
	}

	inline static int32_t get_offset_of_cubeBounds_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___cubeBounds_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeBounds_7() const { return ___cubeBounds_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeBounds_7() { return &___cubeBounds_7; }
	inline void set_cubeBounds_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeBounds_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeBounds_7), (void*)value);
	}

	inline static int32_t get_offset_of_didPassBounds_8() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___didPassBounds_8)); }
	inline bool get_didPassBounds_8() const { return ___didPassBounds_8; }
	inline bool* get_address_of_didPassBounds_8() { return &___didPassBounds_8; }
	inline void set_didPassBounds_8(bool value)
	{
		___didPassBounds_8 = value;
	}

	inline static int32_t get_offset_of_failPoint_9() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___failPoint_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_failPoint_9() const { return ___failPoint_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_failPoint_9() { return &___failPoint_9; }
	inline void set_failPoint_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___failPoint_9 = value;
	}

	inline static int32_t get_offset_of_setOnStartNum_10() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___setOnStartNum_10)); }
	inline int32_t get_setOnStartNum_10() const { return ___setOnStartNum_10; }
	inline int32_t* get_address_of_setOnStartNum_10() { return &___setOnStartNum_10; }
	inline void set_setOnStartNum_10(int32_t value)
	{
		___setOnStartNum_10 = value;
	}

	inline static int32_t get_offset_of_setPosOnUpdate_11() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___setPosOnUpdate_11)); }
	inline bool get_setPosOnUpdate_11() const { return ___setPosOnUpdate_11; }
	inline bool* get_address_of_setPosOnUpdate_11() { return &___setPosOnUpdate_11; }
	inline void set_setPosOnUpdate_11(bool value)
	{
		___setPosOnUpdate_11 = value;
	}

	inline static int32_t get_offset_of_setPosNum_12() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___setPosNum_12)); }
	inline int32_t get_setPosNum_12() const { return ___setPosNum_12; }
	inline int32_t* get_address_of_setPosNum_12() { return &___setPosNum_12; }
	inline void set_setPosNum_12(int32_t value)
	{
		___setPosNum_12 = value;
	}

	inline static int32_t get_offset_of_hasGroupTweensCheckStarted_13() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___hasGroupTweensCheckStarted_13)); }
	inline bool get_hasGroupTweensCheckStarted_13() const { return ___hasGroupTweensCheckStarted_13; }
	inline bool* get_address_of_hasGroupTweensCheckStarted_13() { return &___hasGroupTweensCheckStarted_13; }
	inline void set_hasGroupTweensCheckStarted_13(bool value)
	{
		___hasGroupTweensCheckStarted_13 = value;
	}

	inline static int32_t get_offset_of_previousXlt4_14() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___previousXlt4_14)); }
	inline float get_previousXlt4_14() const { return ___previousXlt4_14; }
	inline float* get_address_of_previousXlt4_14() { return &___previousXlt4_14; }
	inline void set_previousXlt4_14(float value)
	{
		___previousXlt4_14 = value;
	}

	inline static int32_t get_offset_of_onUpdateWasCalled_15() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___onUpdateWasCalled_15)); }
	inline bool get_onUpdateWasCalled_15() const { return ___onUpdateWasCalled_15; }
	inline bool* get_address_of_onUpdateWasCalled_15() { return &___onUpdateWasCalled_15; }
	inline void set_onUpdateWasCalled_15(bool value)
	{
		___onUpdateWasCalled_15 = value;
	}

	inline static int32_t get_offset_of_start_16() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___start_16)); }
	inline float get_start_16() const { return ___start_16; }
	inline float* get_address_of_start_16() { return &___start_16; }
	inline void set_start_16(float value)
	{
		___start_16 = value;
	}

	inline static int32_t get_offset_of_expectedTime_17() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___expectedTime_17)); }
	inline float get_expectedTime_17() const { return ___expectedTime_17; }
	inline float* get_address_of_expectedTime_17() { return &___expectedTime_17; }
	inline void set_expectedTime_17(float value)
	{
		___expectedTime_17 = value;
	}

	inline static int32_t get_offset_of_didGetCorrectOnUpdate_18() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___didGetCorrectOnUpdate_18)); }
	inline bool get_didGetCorrectOnUpdate_18() const { return ___didGetCorrectOnUpdate_18; }
	inline bool* get_address_of_didGetCorrectOnUpdate_18() { return &___didGetCorrectOnUpdate_18; }
	inline void set_didGetCorrectOnUpdate_18(bool value)
	{
		___didGetCorrectOnUpdate_18 = value;
	}

	inline static int32_t get_offset_of_U3CU3E9__13_19() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___U3CU3E9__13_19)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__13_19() const { return ___U3CU3E9__13_19; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__13_19() { return &___U3CU3E9__13_19; }
	inline void set_U3CU3E9__13_19(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__13_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__13_19), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_20() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___U3CU3E9__14_20)); }
	inline Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B * get_U3CU3E9__14_20() const { return ___U3CU3E9__14_20; }
	inline Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B ** get_address_of_U3CU3E9__14_20() { return &___U3CU3E9__14_20; }
	inline void set_U3CU3E9__14_20(Action_1_tC2B4AB26EC30C6FC4AD8C9172FE509B3B4E1C26B * value)
	{
		___U3CU3E9__14_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__14_20), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_21() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___U3CU3E9__16_21)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_U3CU3E9__16_21() const { return ___U3CU3E9__16_21; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_U3CU3E9__16_21() { return &___U3CU3E9__16_21; }
	inline void set_U3CU3E9__16_21(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___U3CU3E9__16_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__16_21), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__15_22() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F, ___U3CU3E9__15_22)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get_U3CU3E9__15_22() const { return ___U3CU3E9__15_22; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of_U3CU3E9__15_22() { return &___U3CU3E9__15_22; }
	inline void set_U3CU3E9__15_22(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		___U3CU3E9__15_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__15_22), (void*)value);
	}
};


// TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13
struct U3CDisplayTextMeshFloatingTextU3Ed__13_t4B6A65678514F8D3CD0E6F44F8AB9DDA1228C55C  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::<>4__this
	TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200 * ___U3CU3E4__this_2;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::<CountDuration>5__2
	float ___U3CCountDurationU3E5__2_3;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::<starting_Count>5__3
	float ___U3Cstarting_CountU3E5__3_4;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::<current_Count>5__4
	float ___U3Ccurrent_CountU3E5__4_5;
	// UnityEngine.Vector3 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::<start_pos>5__5
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3Cstart_posU3E5__5_6;
	// UnityEngine.Color32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::<start_color>5__6
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___U3Cstart_colorU3E5__6_7;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::<alpha>5__7
	float ___U3CalphaU3E5__7_8;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::<fadeDuration>5__8
	float ___U3CfadeDurationU3E5__8_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ed__13_t4B6A65678514F8D3CD0E6F44F8AB9DDA1228C55C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ed__13_t4B6A65678514F8D3CD0E6F44F8AB9DDA1228C55C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ed__13_t4B6A65678514F8D3CD0E6F44F8AB9DDA1228C55C, ___U3CU3E4__this_2)); }
	inline TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCountDurationU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ed__13_t4B6A65678514F8D3CD0E6F44F8AB9DDA1228C55C, ___U3CCountDurationU3E5__2_3)); }
	inline float get_U3CCountDurationU3E5__2_3() const { return ___U3CCountDurationU3E5__2_3; }
	inline float* get_address_of_U3CCountDurationU3E5__2_3() { return &___U3CCountDurationU3E5__2_3; }
	inline void set_U3CCountDurationU3E5__2_3(float value)
	{
		___U3CCountDurationU3E5__2_3 = value;
	}

	inline static int32_t get_offset_of_U3Cstarting_CountU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ed__13_t4B6A65678514F8D3CD0E6F44F8AB9DDA1228C55C, ___U3Cstarting_CountU3E5__3_4)); }
	inline float get_U3Cstarting_CountU3E5__3_4() const { return ___U3Cstarting_CountU3E5__3_4; }
	inline float* get_address_of_U3Cstarting_CountU3E5__3_4() { return &___U3Cstarting_CountU3E5__3_4; }
	inline void set_U3Cstarting_CountU3E5__3_4(float value)
	{
		___U3Cstarting_CountU3E5__3_4 = value;
	}

	inline static int32_t get_offset_of_U3Ccurrent_CountU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ed__13_t4B6A65678514F8D3CD0E6F44F8AB9DDA1228C55C, ___U3Ccurrent_CountU3E5__4_5)); }
	inline float get_U3Ccurrent_CountU3E5__4_5() const { return ___U3Ccurrent_CountU3E5__4_5; }
	inline float* get_address_of_U3Ccurrent_CountU3E5__4_5() { return &___U3Ccurrent_CountU3E5__4_5; }
	inline void set_U3Ccurrent_CountU3E5__4_5(float value)
	{
		___U3Ccurrent_CountU3E5__4_5 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_posU3E5__5_6() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ed__13_t4B6A65678514F8D3CD0E6F44F8AB9DDA1228C55C, ___U3Cstart_posU3E5__5_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3Cstart_posU3E5__5_6() const { return ___U3Cstart_posU3E5__5_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3Cstart_posU3E5__5_6() { return &___U3Cstart_posU3E5__5_6; }
	inline void set_U3Cstart_posU3E5__5_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3Cstart_posU3E5__5_6 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_colorU3E5__6_7() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ed__13_t4B6A65678514F8D3CD0E6F44F8AB9DDA1228C55C, ___U3Cstart_colorU3E5__6_7)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_U3Cstart_colorU3E5__6_7() const { return ___U3Cstart_colorU3E5__6_7; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_U3Cstart_colorU3E5__6_7() { return &___U3Cstart_colorU3E5__6_7; }
	inline void set_U3Cstart_colorU3E5__6_7(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___U3Cstart_colorU3E5__6_7 = value;
	}

	inline static int32_t get_offset_of_U3CalphaU3E5__7_8() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ed__13_t4B6A65678514F8D3CD0E6F44F8AB9DDA1228C55C, ___U3CalphaU3E5__7_8)); }
	inline float get_U3CalphaU3E5__7_8() const { return ___U3CalphaU3E5__7_8; }
	inline float* get_address_of_U3CalphaU3E5__7_8() { return &___U3CalphaU3E5__7_8; }
	inline void set_U3CalphaU3E5__7_8(float value)
	{
		___U3CalphaU3E5__7_8 = value;
	}

	inline static int32_t get_offset_of_U3CfadeDurationU3E5__8_9() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ed__13_t4B6A65678514F8D3CD0E6F44F8AB9DDA1228C55C, ___U3CfadeDurationU3E5__8_9)); }
	inline float get_U3CfadeDurationU3E5__8_9() const { return ___U3CfadeDurationU3E5__8_9; }
	inline float* get_address_of_U3CfadeDurationU3E5__8_9() { return &___U3CfadeDurationU3E5__8_9; }
	inline void set_U3CfadeDurationU3E5__8_9(float value)
	{
		___U3CfadeDurationU3E5__8_9 = value;
	}
};


// TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12
struct U3CDisplayTextMeshProFloatingTextU3Ed__12_t0F0EBDC5A3A2FF615C3205C8019AB8A81EDEF6FC  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::<>4__this
	TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200 * ___U3CU3E4__this_2;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::<CountDuration>5__2
	float ___U3CCountDurationU3E5__2_3;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::<starting_Count>5__3
	float ___U3Cstarting_CountU3E5__3_4;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::<current_Count>5__4
	float ___U3Ccurrent_CountU3E5__4_5;
	// UnityEngine.Vector3 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::<start_pos>5__5
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3Cstart_posU3E5__5_6;
	// UnityEngine.Color32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::<start_color>5__6
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___U3Cstart_colorU3E5__6_7;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::<alpha>5__7
	float ___U3CalphaU3E5__7_8;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::<fadeDuration>5__8
	float ___U3CfadeDurationU3E5__8_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ed__12_t0F0EBDC5A3A2FF615C3205C8019AB8A81EDEF6FC, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ed__12_t0F0EBDC5A3A2FF615C3205C8019AB8A81EDEF6FC, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ed__12_t0F0EBDC5A3A2FF615C3205C8019AB8A81EDEF6FC, ___U3CU3E4__this_2)); }
	inline TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCountDurationU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ed__12_t0F0EBDC5A3A2FF615C3205C8019AB8A81EDEF6FC, ___U3CCountDurationU3E5__2_3)); }
	inline float get_U3CCountDurationU3E5__2_3() const { return ___U3CCountDurationU3E5__2_3; }
	inline float* get_address_of_U3CCountDurationU3E5__2_3() { return &___U3CCountDurationU3E5__2_3; }
	inline void set_U3CCountDurationU3E5__2_3(float value)
	{
		___U3CCountDurationU3E5__2_3 = value;
	}

	inline static int32_t get_offset_of_U3Cstarting_CountU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ed__12_t0F0EBDC5A3A2FF615C3205C8019AB8A81EDEF6FC, ___U3Cstarting_CountU3E5__3_4)); }
	inline float get_U3Cstarting_CountU3E5__3_4() const { return ___U3Cstarting_CountU3E5__3_4; }
	inline float* get_address_of_U3Cstarting_CountU3E5__3_4() { return &___U3Cstarting_CountU3E5__3_4; }
	inline void set_U3Cstarting_CountU3E5__3_4(float value)
	{
		___U3Cstarting_CountU3E5__3_4 = value;
	}

	inline static int32_t get_offset_of_U3Ccurrent_CountU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ed__12_t0F0EBDC5A3A2FF615C3205C8019AB8A81EDEF6FC, ___U3Ccurrent_CountU3E5__4_5)); }
	inline float get_U3Ccurrent_CountU3E5__4_5() const { return ___U3Ccurrent_CountU3E5__4_5; }
	inline float* get_address_of_U3Ccurrent_CountU3E5__4_5() { return &___U3Ccurrent_CountU3E5__4_5; }
	inline void set_U3Ccurrent_CountU3E5__4_5(float value)
	{
		___U3Ccurrent_CountU3E5__4_5 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_posU3E5__5_6() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ed__12_t0F0EBDC5A3A2FF615C3205C8019AB8A81EDEF6FC, ___U3Cstart_posU3E5__5_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3Cstart_posU3E5__5_6() const { return ___U3Cstart_posU3E5__5_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3Cstart_posU3E5__5_6() { return &___U3Cstart_posU3E5__5_6; }
	inline void set_U3Cstart_posU3E5__5_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3Cstart_posU3E5__5_6 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_colorU3E5__6_7() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ed__12_t0F0EBDC5A3A2FF615C3205C8019AB8A81EDEF6FC, ___U3Cstart_colorU3E5__6_7)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_U3Cstart_colorU3E5__6_7() const { return ___U3Cstart_colorU3E5__6_7; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_U3Cstart_colorU3E5__6_7() { return &___U3Cstart_colorU3E5__6_7; }
	inline void set_U3Cstart_colorU3E5__6_7(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___U3Cstart_colorU3E5__6_7 = value;
	}

	inline static int32_t get_offset_of_U3CalphaU3E5__7_8() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ed__12_t0F0EBDC5A3A2FF615C3205C8019AB8A81EDEF6FC, ___U3CalphaU3E5__7_8)); }
	inline float get_U3CalphaU3E5__7_8() const { return ___U3CalphaU3E5__7_8; }
	inline float* get_address_of_U3CalphaU3E5__7_8() { return &___U3CalphaU3E5__7_8; }
	inline void set_U3CalphaU3E5__7_8(float value)
	{
		___U3CalphaU3E5__7_8 = value;
	}

	inline static int32_t get_offset_of_U3CfadeDurationU3E5__8_9() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ed__12_t0F0EBDC5A3A2FF615C3205C8019AB8A81EDEF6FC, ___U3CfadeDurationU3E5__8_9)); }
	inline float get_U3CfadeDurationU3E5__8_9() const { return ___U3CfadeDurationU3E5__8_9; }
	inline float* get_address_of_U3CfadeDurationU3E5__8_9() { return &___U3CfadeDurationU3E5__8_9; }
	inline void set_U3CfadeDurationU3E5__8_9(float value)
	{
		___U3CfadeDurationU3E5__8_9 = value;
	}
};


// CSCardValue
struct CSCardValue_tEDFD24331AA54328DA51DA03E0123741F8729D38 
{
public:
	// CSSuit CSCardValue::suit
	int32_t ___suit_0;
	// CSRank CSCardValue::rank
	int32_t ___rank_1;

public:
	inline static int32_t get_offset_of_suit_0() { return static_cast<int32_t>(offsetof(CSCardValue_tEDFD24331AA54328DA51DA03E0123741F8729D38, ___suit_0)); }
	inline int32_t get_suit_0() const { return ___suit_0; }
	inline int32_t* get_address_of_suit_0() { return &___suit_0; }
	inline void set_suit_0(int32_t value)
	{
		___suit_0 = value;
	}

	inline static int32_t get_offset_of_rank_1() { return static_cast<int32_t>(offsetof(CSCardValue_tEDFD24331AA54328DA51DA03E0123741F8729D38, ___rank_1)); }
	inline int32_t get_rank_1() const { return ___rank_1; }
	inline int32_t* get_address_of_rank_1() { return &___rank_1; }
	inline void set_rank_1(int32_t value)
	{
		___rank_1 = value;
	}
};


// CSPayline
struct CSPayline_t3562100776857062BF81F66EEEBBF159B5772FCF  : public RuntimeObject
{
public:
	// CSLine CSPayline::line
	CSLine_t3E62A34AE974F1DA0D692280F027A34103D5214C * ___line_0;
	// CSSymbolType CSPayline::type
	int32_t ___type_1;
	// System.Collections.Generic.List`1<CSSymbol> CSPayline::symbols
	List_1_t2A81543C57C79F7AA8F30E3FB31798B8BED5D3E9 * ___symbols_2;
	// System.Single CSPayline::win
	float ___win_3;

public:
	inline static int32_t get_offset_of_line_0() { return static_cast<int32_t>(offsetof(CSPayline_t3562100776857062BF81F66EEEBBF159B5772FCF, ___line_0)); }
	inline CSLine_t3E62A34AE974F1DA0D692280F027A34103D5214C * get_line_0() const { return ___line_0; }
	inline CSLine_t3E62A34AE974F1DA0D692280F027A34103D5214C ** get_address_of_line_0() { return &___line_0; }
	inline void set_line_0(CSLine_t3E62A34AE974F1DA0D692280F027A34103D5214C * value)
	{
		___line_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___line_0), (void*)value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(CSPayline_t3562100776857062BF81F66EEEBBF159B5772FCF, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_symbols_2() { return static_cast<int32_t>(offsetof(CSPayline_t3562100776857062BF81F66EEEBBF159B5772FCF, ___symbols_2)); }
	inline List_1_t2A81543C57C79F7AA8F30E3FB31798B8BED5D3E9 * get_symbols_2() const { return ___symbols_2; }
	inline List_1_t2A81543C57C79F7AA8F30E3FB31798B8BED5D3E9 ** get_address_of_symbols_2() { return &___symbols_2; }
	inline void set_symbols_2(List_1_t2A81543C57C79F7AA8F30E3FB31798B8BED5D3E9 * value)
	{
		___symbols_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___symbols_2), (void*)value);
	}

	inline static int32_t get_offset_of_win_3() { return static_cast<int32_t>(offsetof(CSPayline_t3562100776857062BF81F66EEEBBF159B5772FCF, ___win_3)); }
	inline float get_win_3() const { return ___win_3; }
	inline float* get_address_of_win_3() { return &___win_3; }
	inline void set_win_3(float value)
	{
		___win_3 = value;
	}
};


// CSSymbolPercent
struct CSSymbolPercent_tB65FE49C56B898EB176325F3AA98A5C1A5BF340E 
{
public:
	// System.Single CSSymbolPercent::percent
	float ___percent_0;
	// CSSymbolType CSSymbolPercent::type
	int32_t ___type_1;

public:
	inline static int32_t get_offset_of_percent_0() { return static_cast<int32_t>(offsetof(CSSymbolPercent_tB65FE49C56B898EB176325F3AA98A5C1A5BF340E, ___percent_0)); }
	inline float get_percent_0() const { return ___percent_0; }
	inline float* get_address_of_percent_0() { return &___percent_0; }
	inline void set_percent_0(float value)
	{
		___percent_0 = value;
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(CSSymbolPercent_tB65FE49C56B898EB176325F3AA98A5C1A5BF340E, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}
};


// CSTimer
struct CSTimer_tA0601B382CE03F0C9C9A31E54C5EEDAD89CD7668  : public RuntimeObject
{
public:
	// TimerDelegate CSTimer::timerTickEvent
	TimerDelegate_t34BCC9B9964D8BA67E6A87849C74455F86AFA1DB * ___timerTickEvent_0;
	// TimerDelegate CSTimer::timerStopEvent
	TimerDelegate_t34BCC9B9964D8BA67E6A87849C74455F86AFA1DB * ___timerStopEvent_1;
	// System.DateTime CSTimer::startDate
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___startDate_2;
	// System.DateTime CSTimer::endDate
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___endDate_3;
	// System.TimeSpan CSTimer::time
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___time_4;
	// System.String CSTimer::key
	String_t* ___key_5;
	// System.Single CSTimer::percent
	float ___percent_6;
	// System.Double CSTimer::_totalSec
	double ____totalSec_7;
	// System.Boolean CSTimer::_running
	bool ____running_8;

public:
	inline static int32_t get_offset_of_timerTickEvent_0() { return static_cast<int32_t>(offsetof(CSTimer_tA0601B382CE03F0C9C9A31E54C5EEDAD89CD7668, ___timerTickEvent_0)); }
	inline TimerDelegate_t34BCC9B9964D8BA67E6A87849C74455F86AFA1DB * get_timerTickEvent_0() const { return ___timerTickEvent_0; }
	inline TimerDelegate_t34BCC9B9964D8BA67E6A87849C74455F86AFA1DB ** get_address_of_timerTickEvent_0() { return &___timerTickEvent_0; }
	inline void set_timerTickEvent_0(TimerDelegate_t34BCC9B9964D8BA67E6A87849C74455F86AFA1DB * value)
	{
		___timerTickEvent_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___timerTickEvent_0), (void*)value);
	}

	inline static int32_t get_offset_of_timerStopEvent_1() { return static_cast<int32_t>(offsetof(CSTimer_tA0601B382CE03F0C9C9A31E54C5EEDAD89CD7668, ___timerStopEvent_1)); }
	inline TimerDelegate_t34BCC9B9964D8BA67E6A87849C74455F86AFA1DB * get_timerStopEvent_1() const { return ___timerStopEvent_1; }
	inline TimerDelegate_t34BCC9B9964D8BA67E6A87849C74455F86AFA1DB ** get_address_of_timerStopEvent_1() { return &___timerStopEvent_1; }
	inline void set_timerStopEvent_1(TimerDelegate_t34BCC9B9964D8BA67E6A87849C74455F86AFA1DB * value)
	{
		___timerStopEvent_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___timerStopEvent_1), (void*)value);
	}

	inline static int32_t get_offset_of_startDate_2() { return static_cast<int32_t>(offsetof(CSTimer_tA0601B382CE03F0C9C9A31E54C5EEDAD89CD7668, ___startDate_2)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_startDate_2() const { return ___startDate_2; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_startDate_2() { return &___startDate_2; }
	inline void set_startDate_2(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___startDate_2 = value;
	}

	inline static int32_t get_offset_of_endDate_3() { return static_cast<int32_t>(offsetof(CSTimer_tA0601B382CE03F0C9C9A31E54C5EEDAD89CD7668, ___endDate_3)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_endDate_3() const { return ___endDate_3; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_endDate_3() { return &___endDate_3; }
	inline void set_endDate_3(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___endDate_3 = value;
	}

	inline static int32_t get_offset_of_time_4() { return static_cast<int32_t>(offsetof(CSTimer_tA0601B382CE03F0C9C9A31E54C5EEDAD89CD7668, ___time_4)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_time_4() const { return ___time_4; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_time_4() { return &___time_4; }
	inline void set_time_4(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___time_4 = value;
	}

	inline static int32_t get_offset_of_key_5() { return static_cast<int32_t>(offsetof(CSTimer_tA0601B382CE03F0C9C9A31E54C5EEDAD89CD7668, ___key_5)); }
	inline String_t* get_key_5() const { return ___key_5; }
	inline String_t** get_address_of_key_5() { return &___key_5; }
	inline void set_key_5(String_t* value)
	{
		___key_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___key_5), (void*)value);
	}

	inline static int32_t get_offset_of_percent_6() { return static_cast<int32_t>(offsetof(CSTimer_tA0601B382CE03F0C9C9A31E54C5EEDAD89CD7668, ___percent_6)); }
	inline float get_percent_6() const { return ___percent_6; }
	inline float* get_address_of_percent_6() { return &___percent_6; }
	inline void set_percent_6(float value)
	{
		___percent_6 = value;
	}

	inline static int32_t get_offset_of__totalSec_7() { return static_cast<int32_t>(offsetof(CSTimer_tA0601B382CE03F0C9C9A31E54C5EEDAD89CD7668, ____totalSec_7)); }
	inline double get__totalSec_7() const { return ____totalSec_7; }
	inline double* get_address_of__totalSec_7() { return &____totalSec_7; }
	inline void set__totalSec_7(double value)
	{
		____totalSec_7 = value;
	}

	inline static int32_t get_offset_of__running_8() { return static_cast<int32_t>(offsetof(CSTimer_tA0601B382CE03F0C9C9A31E54C5EEDAD89CD7668, ____running_8)); }
	inline bool get__running_8() const { return ____running_8; }
	inline bool* get_address_of__running_8() { return &____running_8; }
	inline void set__running_8(bool value)
	{
		____running_8 = value;
	}
};


// CSTimerData
struct CSTimerData_t4D5EF8636ACBC695F6934CEC7B653CD513CD1321  : public RuntimeObject
{
public:
	// System.DateTime CSTimerData::startDate
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___startDate_0;
	// System.TimeSpan CSTimerData::time
	TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  ___time_1;
	// System.String CSTimerData::key
	String_t* ___key_2;

public:
	inline static int32_t get_offset_of_startDate_0() { return static_cast<int32_t>(offsetof(CSTimerData_t4D5EF8636ACBC695F6934CEC7B653CD513CD1321, ___startDate_0)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_startDate_0() const { return ___startDate_0; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_startDate_0() { return &___startDate_0; }
	inline void set_startDate_0(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___startDate_0 = value;
	}

	inline static int32_t get_offset_of_time_1() { return static_cast<int32_t>(offsetof(CSTimerData_t4D5EF8636ACBC695F6934CEC7B653CD513CD1321, ___time_1)); }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  get_time_1() const { return ___time_1; }
	inline TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203 * get_address_of_time_1() { return &___time_1; }
	inline void set_time_1(TimeSpan_t4F6A0E13E703B65365CFCAB58E05EE0AF3EE6203  value)
	{
		___time_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(CSTimerData_t4D5EF8636ACBC695F6934CEC7B653CD513CD1321, ___key_2)); }
	inline String_t* get_key_2() const { return ___key_2; }
	inline String_t** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(String_t* value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___key_2), (void*)value);
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// LTDescr
struct LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F  : public RuntimeObject
{
public:
	// System.Boolean LTDescr::toggle
	bool ___toggle_0;
	// System.Boolean LTDescr::useEstimatedTime
	bool ___useEstimatedTime_1;
	// System.Boolean LTDescr::useFrames
	bool ___useFrames_2;
	// System.Boolean LTDescr::useManualTime
	bool ___useManualTime_3;
	// System.Boolean LTDescr::usesNormalDt
	bool ___usesNormalDt_4;
	// System.Boolean LTDescr::hasInitiliazed
	bool ___hasInitiliazed_5;
	// System.Boolean LTDescr::hasExtraOnCompletes
	bool ___hasExtraOnCompletes_6;
	// System.Boolean LTDescr::hasPhysics
	bool ___hasPhysics_7;
	// System.Boolean LTDescr::onCompleteOnRepeat
	bool ___onCompleteOnRepeat_8;
	// System.Boolean LTDescr::onCompleteOnStart
	bool ___onCompleteOnStart_9;
	// System.Boolean LTDescr::useRecursion
	bool ___useRecursion_10;
	// System.Single LTDescr::ratioPassed
	float ___ratioPassed_11;
	// System.Single LTDescr::passed
	float ___passed_12;
	// System.Single LTDescr::delay
	float ___delay_13;
	// System.Single LTDescr::time
	float ___time_14;
	// System.Single LTDescr::speed
	float ___speed_15;
	// System.Single LTDescr::lastVal
	float ___lastVal_16;
	// System.UInt32 LTDescr::_id
	uint32_t ____id_17;
	// System.Int32 LTDescr::loopCount
	int32_t ___loopCount_18;
	// System.UInt32 LTDescr::counter
	uint32_t ___counter_19;
	// System.Single LTDescr::direction
	float ___direction_20;
	// System.Single LTDescr::directionLast
	float ___directionLast_21;
	// System.Single LTDescr::overshoot
	float ___overshoot_22;
	// System.Single LTDescr::period
	float ___period_23;
	// System.Single LTDescr::scale
	float ___scale_24;
	// System.Boolean LTDescr::destroyOnComplete
	bool ___destroyOnComplete_25;
	// UnityEngine.Transform LTDescr::trans
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___trans_26;
	// UnityEngine.Vector3 LTDescr::fromInternal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___fromInternal_27;
	// UnityEngine.Vector3 LTDescr::toInternal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___toInternal_28;
	// UnityEngine.Vector3 LTDescr::diff
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___diff_29;
	// UnityEngine.Vector3 LTDescr::diffDiv2
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___diffDiv2_30;
	// TweenAction LTDescr::type
	int32_t ___type_31;
	// LeanTweenType LTDescr::easeType
	int32_t ___easeType_32;
	// LeanTweenType LTDescr::loopType
	int32_t ___loopType_33;
	// System.Boolean LTDescr::hasUpdateCallback
	bool ___hasUpdateCallback_34;
	// LTDescr/EaseTypeDelegate LTDescr::easeMethod
	EaseTypeDelegate_t0285AB55B28F5B5A6688E966B51FBA837B2E84AF * ___easeMethod_35;
	// LTDescr/ActionMethodDelegate LTDescr::<easeInternal>k__BackingField
	ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * ___U3CeaseInternalU3Ek__BackingField_36;
	// LTDescr/ActionMethodDelegate LTDescr::<initInternal>k__BackingField
	ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * ___U3CinitInternalU3Ek__BackingField_37;
	// UnityEngine.SpriteRenderer LTDescr::spriteRen
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___spriteRen_38;
	// UnityEngine.RectTransform LTDescr::rectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___rectTransform_39;
	// UnityEngine.UI.Text LTDescr::uiText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___uiText_40;
	// UnityEngine.UI.Image LTDescr::uiImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___uiImage_41;
	// UnityEngine.UI.RawImage LTDescr::rawImage
	RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * ___rawImage_42;
	// UnityEngine.Sprite[] LTDescr::sprites
	SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* ___sprites_43;
	// LTDescrOptional LTDescr::_optional
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2 * ____optional_44;

public:
	inline static int32_t get_offset_of_toggle_0() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___toggle_0)); }
	inline bool get_toggle_0() const { return ___toggle_0; }
	inline bool* get_address_of_toggle_0() { return &___toggle_0; }
	inline void set_toggle_0(bool value)
	{
		___toggle_0 = value;
	}

	inline static int32_t get_offset_of_useEstimatedTime_1() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___useEstimatedTime_1)); }
	inline bool get_useEstimatedTime_1() const { return ___useEstimatedTime_1; }
	inline bool* get_address_of_useEstimatedTime_1() { return &___useEstimatedTime_1; }
	inline void set_useEstimatedTime_1(bool value)
	{
		___useEstimatedTime_1 = value;
	}

	inline static int32_t get_offset_of_useFrames_2() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___useFrames_2)); }
	inline bool get_useFrames_2() const { return ___useFrames_2; }
	inline bool* get_address_of_useFrames_2() { return &___useFrames_2; }
	inline void set_useFrames_2(bool value)
	{
		___useFrames_2 = value;
	}

	inline static int32_t get_offset_of_useManualTime_3() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___useManualTime_3)); }
	inline bool get_useManualTime_3() const { return ___useManualTime_3; }
	inline bool* get_address_of_useManualTime_3() { return &___useManualTime_3; }
	inline void set_useManualTime_3(bool value)
	{
		___useManualTime_3 = value;
	}

	inline static int32_t get_offset_of_usesNormalDt_4() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___usesNormalDt_4)); }
	inline bool get_usesNormalDt_4() const { return ___usesNormalDt_4; }
	inline bool* get_address_of_usesNormalDt_4() { return &___usesNormalDt_4; }
	inline void set_usesNormalDt_4(bool value)
	{
		___usesNormalDt_4 = value;
	}

	inline static int32_t get_offset_of_hasInitiliazed_5() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___hasInitiliazed_5)); }
	inline bool get_hasInitiliazed_5() const { return ___hasInitiliazed_5; }
	inline bool* get_address_of_hasInitiliazed_5() { return &___hasInitiliazed_5; }
	inline void set_hasInitiliazed_5(bool value)
	{
		___hasInitiliazed_5 = value;
	}

	inline static int32_t get_offset_of_hasExtraOnCompletes_6() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___hasExtraOnCompletes_6)); }
	inline bool get_hasExtraOnCompletes_6() const { return ___hasExtraOnCompletes_6; }
	inline bool* get_address_of_hasExtraOnCompletes_6() { return &___hasExtraOnCompletes_6; }
	inline void set_hasExtraOnCompletes_6(bool value)
	{
		___hasExtraOnCompletes_6 = value;
	}

	inline static int32_t get_offset_of_hasPhysics_7() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___hasPhysics_7)); }
	inline bool get_hasPhysics_7() const { return ___hasPhysics_7; }
	inline bool* get_address_of_hasPhysics_7() { return &___hasPhysics_7; }
	inline void set_hasPhysics_7(bool value)
	{
		___hasPhysics_7 = value;
	}

	inline static int32_t get_offset_of_onCompleteOnRepeat_8() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___onCompleteOnRepeat_8)); }
	inline bool get_onCompleteOnRepeat_8() const { return ___onCompleteOnRepeat_8; }
	inline bool* get_address_of_onCompleteOnRepeat_8() { return &___onCompleteOnRepeat_8; }
	inline void set_onCompleteOnRepeat_8(bool value)
	{
		___onCompleteOnRepeat_8 = value;
	}

	inline static int32_t get_offset_of_onCompleteOnStart_9() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___onCompleteOnStart_9)); }
	inline bool get_onCompleteOnStart_9() const { return ___onCompleteOnStart_9; }
	inline bool* get_address_of_onCompleteOnStart_9() { return &___onCompleteOnStart_9; }
	inline void set_onCompleteOnStart_9(bool value)
	{
		___onCompleteOnStart_9 = value;
	}

	inline static int32_t get_offset_of_useRecursion_10() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___useRecursion_10)); }
	inline bool get_useRecursion_10() const { return ___useRecursion_10; }
	inline bool* get_address_of_useRecursion_10() { return &___useRecursion_10; }
	inline void set_useRecursion_10(bool value)
	{
		___useRecursion_10 = value;
	}

	inline static int32_t get_offset_of_ratioPassed_11() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___ratioPassed_11)); }
	inline float get_ratioPassed_11() const { return ___ratioPassed_11; }
	inline float* get_address_of_ratioPassed_11() { return &___ratioPassed_11; }
	inline void set_ratioPassed_11(float value)
	{
		___ratioPassed_11 = value;
	}

	inline static int32_t get_offset_of_passed_12() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___passed_12)); }
	inline float get_passed_12() const { return ___passed_12; }
	inline float* get_address_of_passed_12() { return &___passed_12; }
	inline void set_passed_12(float value)
	{
		___passed_12 = value;
	}

	inline static int32_t get_offset_of_delay_13() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___delay_13)); }
	inline float get_delay_13() const { return ___delay_13; }
	inline float* get_address_of_delay_13() { return &___delay_13; }
	inline void set_delay_13(float value)
	{
		___delay_13 = value;
	}

	inline static int32_t get_offset_of_time_14() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___time_14)); }
	inline float get_time_14() const { return ___time_14; }
	inline float* get_address_of_time_14() { return &___time_14; }
	inline void set_time_14(float value)
	{
		___time_14 = value;
	}

	inline static int32_t get_offset_of_speed_15() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___speed_15)); }
	inline float get_speed_15() const { return ___speed_15; }
	inline float* get_address_of_speed_15() { return &___speed_15; }
	inline void set_speed_15(float value)
	{
		___speed_15 = value;
	}

	inline static int32_t get_offset_of_lastVal_16() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___lastVal_16)); }
	inline float get_lastVal_16() const { return ___lastVal_16; }
	inline float* get_address_of_lastVal_16() { return &___lastVal_16; }
	inline void set_lastVal_16(float value)
	{
		___lastVal_16 = value;
	}

	inline static int32_t get_offset_of__id_17() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ____id_17)); }
	inline uint32_t get__id_17() const { return ____id_17; }
	inline uint32_t* get_address_of__id_17() { return &____id_17; }
	inline void set__id_17(uint32_t value)
	{
		____id_17 = value;
	}

	inline static int32_t get_offset_of_loopCount_18() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___loopCount_18)); }
	inline int32_t get_loopCount_18() const { return ___loopCount_18; }
	inline int32_t* get_address_of_loopCount_18() { return &___loopCount_18; }
	inline void set_loopCount_18(int32_t value)
	{
		___loopCount_18 = value;
	}

	inline static int32_t get_offset_of_counter_19() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___counter_19)); }
	inline uint32_t get_counter_19() const { return ___counter_19; }
	inline uint32_t* get_address_of_counter_19() { return &___counter_19; }
	inline void set_counter_19(uint32_t value)
	{
		___counter_19 = value;
	}

	inline static int32_t get_offset_of_direction_20() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___direction_20)); }
	inline float get_direction_20() const { return ___direction_20; }
	inline float* get_address_of_direction_20() { return &___direction_20; }
	inline void set_direction_20(float value)
	{
		___direction_20 = value;
	}

	inline static int32_t get_offset_of_directionLast_21() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___directionLast_21)); }
	inline float get_directionLast_21() const { return ___directionLast_21; }
	inline float* get_address_of_directionLast_21() { return &___directionLast_21; }
	inline void set_directionLast_21(float value)
	{
		___directionLast_21 = value;
	}

	inline static int32_t get_offset_of_overshoot_22() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___overshoot_22)); }
	inline float get_overshoot_22() const { return ___overshoot_22; }
	inline float* get_address_of_overshoot_22() { return &___overshoot_22; }
	inline void set_overshoot_22(float value)
	{
		___overshoot_22 = value;
	}

	inline static int32_t get_offset_of_period_23() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___period_23)); }
	inline float get_period_23() const { return ___period_23; }
	inline float* get_address_of_period_23() { return &___period_23; }
	inline void set_period_23(float value)
	{
		___period_23 = value;
	}

	inline static int32_t get_offset_of_scale_24() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___scale_24)); }
	inline float get_scale_24() const { return ___scale_24; }
	inline float* get_address_of_scale_24() { return &___scale_24; }
	inline void set_scale_24(float value)
	{
		___scale_24 = value;
	}

	inline static int32_t get_offset_of_destroyOnComplete_25() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___destroyOnComplete_25)); }
	inline bool get_destroyOnComplete_25() const { return ___destroyOnComplete_25; }
	inline bool* get_address_of_destroyOnComplete_25() { return &___destroyOnComplete_25; }
	inline void set_destroyOnComplete_25(bool value)
	{
		___destroyOnComplete_25 = value;
	}

	inline static int32_t get_offset_of_trans_26() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___trans_26)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_trans_26() const { return ___trans_26; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_trans_26() { return &___trans_26; }
	inline void set_trans_26(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___trans_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trans_26), (void*)value);
	}

	inline static int32_t get_offset_of_fromInternal_27() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___fromInternal_27)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_fromInternal_27() const { return ___fromInternal_27; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_fromInternal_27() { return &___fromInternal_27; }
	inline void set_fromInternal_27(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___fromInternal_27 = value;
	}

	inline static int32_t get_offset_of_toInternal_28() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___toInternal_28)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_toInternal_28() const { return ___toInternal_28; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_toInternal_28() { return &___toInternal_28; }
	inline void set_toInternal_28(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___toInternal_28 = value;
	}

	inline static int32_t get_offset_of_diff_29() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___diff_29)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_diff_29() const { return ___diff_29; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_diff_29() { return &___diff_29; }
	inline void set_diff_29(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___diff_29 = value;
	}

	inline static int32_t get_offset_of_diffDiv2_30() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___diffDiv2_30)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_diffDiv2_30() const { return ___diffDiv2_30; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_diffDiv2_30() { return &___diffDiv2_30; }
	inline void set_diffDiv2_30(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___diffDiv2_30 = value;
	}

	inline static int32_t get_offset_of_type_31() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___type_31)); }
	inline int32_t get_type_31() const { return ___type_31; }
	inline int32_t* get_address_of_type_31() { return &___type_31; }
	inline void set_type_31(int32_t value)
	{
		___type_31 = value;
	}

	inline static int32_t get_offset_of_easeType_32() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___easeType_32)); }
	inline int32_t get_easeType_32() const { return ___easeType_32; }
	inline int32_t* get_address_of_easeType_32() { return &___easeType_32; }
	inline void set_easeType_32(int32_t value)
	{
		___easeType_32 = value;
	}

	inline static int32_t get_offset_of_loopType_33() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___loopType_33)); }
	inline int32_t get_loopType_33() const { return ___loopType_33; }
	inline int32_t* get_address_of_loopType_33() { return &___loopType_33; }
	inline void set_loopType_33(int32_t value)
	{
		___loopType_33 = value;
	}

	inline static int32_t get_offset_of_hasUpdateCallback_34() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___hasUpdateCallback_34)); }
	inline bool get_hasUpdateCallback_34() const { return ___hasUpdateCallback_34; }
	inline bool* get_address_of_hasUpdateCallback_34() { return &___hasUpdateCallback_34; }
	inline void set_hasUpdateCallback_34(bool value)
	{
		___hasUpdateCallback_34 = value;
	}

	inline static int32_t get_offset_of_easeMethod_35() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___easeMethod_35)); }
	inline EaseTypeDelegate_t0285AB55B28F5B5A6688E966B51FBA837B2E84AF * get_easeMethod_35() const { return ___easeMethod_35; }
	inline EaseTypeDelegate_t0285AB55B28F5B5A6688E966B51FBA837B2E84AF ** get_address_of_easeMethod_35() { return &___easeMethod_35; }
	inline void set_easeMethod_35(EaseTypeDelegate_t0285AB55B28F5B5A6688E966B51FBA837B2E84AF * value)
	{
		___easeMethod_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___easeMethod_35), (void*)value);
	}

	inline static int32_t get_offset_of_U3CeaseInternalU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___U3CeaseInternalU3Ek__BackingField_36)); }
	inline ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * get_U3CeaseInternalU3Ek__BackingField_36() const { return ___U3CeaseInternalU3Ek__BackingField_36; }
	inline ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 ** get_address_of_U3CeaseInternalU3Ek__BackingField_36() { return &___U3CeaseInternalU3Ek__BackingField_36; }
	inline void set_U3CeaseInternalU3Ek__BackingField_36(ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * value)
	{
		___U3CeaseInternalU3Ek__BackingField_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CeaseInternalU3Ek__BackingField_36), (void*)value);
	}

	inline static int32_t get_offset_of_U3CinitInternalU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___U3CinitInternalU3Ek__BackingField_37)); }
	inline ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * get_U3CinitInternalU3Ek__BackingField_37() const { return ___U3CinitInternalU3Ek__BackingField_37; }
	inline ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 ** get_address_of_U3CinitInternalU3Ek__BackingField_37() { return &___U3CinitInternalU3Ek__BackingField_37; }
	inline void set_U3CinitInternalU3Ek__BackingField_37(ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05 * value)
	{
		___U3CinitInternalU3Ek__BackingField_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CinitInternalU3Ek__BackingField_37), (void*)value);
	}

	inline static int32_t get_offset_of_spriteRen_38() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___spriteRen_38)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_spriteRen_38() const { return ___spriteRen_38; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_spriteRen_38() { return &___spriteRen_38; }
	inline void set_spriteRen_38(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___spriteRen_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spriteRen_38), (void*)value);
	}

	inline static int32_t get_offset_of_rectTransform_39() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___rectTransform_39)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_rectTransform_39() const { return ___rectTransform_39; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_rectTransform_39() { return &___rectTransform_39; }
	inline void set_rectTransform_39(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___rectTransform_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rectTransform_39), (void*)value);
	}

	inline static int32_t get_offset_of_uiText_40() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___uiText_40)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_uiText_40() const { return ___uiText_40; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_uiText_40() { return &___uiText_40; }
	inline void set_uiText_40(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___uiText_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___uiText_40), (void*)value);
	}

	inline static int32_t get_offset_of_uiImage_41() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___uiImage_41)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_uiImage_41() const { return ___uiImage_41; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_uiImage_41() { return &___uiImage_41; }
	inline void set_uiImage_41(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___uiImage_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___uiImage_41), (void*)value);
	}

	inline static int32_t get_offset_of_rawImage_42() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___rawImage_42)); }
	inline RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * get_rawImage_42() const { return ___rawImage_42; }
	inline RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A ** get_address_of_rawImage_42() { return &___rawImage_42; }
	inline void set_rawImage_42(RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * value)
	{
		___rawImage_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rawImage_42), (void*)value);
	}

	inline static int32_t get_offset_of_sprites_43() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ___sprites_43)); }
	inline SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* get_sprites_43() const { return ___sprites_43; }
	inline SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77** get_address_of_sprites_43() { return &___sprites_43; }
	inline void set_sprites_43(SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* value)
	{
		___sprites_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sprites_43), (void*)value);
	}

	inline static int32_t get_offset_of__optional_44() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F, ____optional_44)); }
	inline LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2 * get__optional_44() const { return ____optional_44; }
	inline LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2 ** get_address_of__optional_44() { return &____optional_44; }
	inline void set__optional_44(LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2 * value)
	{
		____optional_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____optional_44), (void*)value);
	}
};

struct LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_StaticFields
{
public:
	// System.Single LTDescr::val
	float ___val_45;
	// System.Single LTDescr::dt
	float ___dt_46;
	// UnityEngine.Vector3 LTDescr::newVect
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___newVect_47;

public:
	inline static int32_t get_offset_of_val_45() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_StaticFields, ___val_45)); }
	inline float get_val_45() const { return ___val_45; }
	inline float* get_address_of_val_45() { return &___val_45; }
	inline void set_val_45(float value)
	{
		___val_45 = value;
	}

	inline static int32_t get_offset_of_dt_46() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_StaticFields, ___dt_46)); }
	inline float get_dt_46() const { return ___dt_46; }
	inline float* get_address_of_dt_46() { return &___dt_46; }
	inline void set_dt_46(float value)
	{
		___dt_46 = value;
	}

	inline static int32_t get_offset_of_newVect_47() { return static_cast<int32_t>(offsetof(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_StaticFields, ___newVect_47)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_newVect_47() const { return ___newVect_47; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_newVect_47() { return &___newVect_47; }
	inline void set_newVect_47(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___newVect_47 = value;
	}
};


// LTRect
struct LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD  : public RuntimeObject
{
public:
	// UnityEngine.Rect LTRect::_rect
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ____rect_0;
	// System.Single LTRect::alpha
	float ___alpha_1;
	// System.Single LTRect::rotation
	float ___rotation_2;
	// UnityEngine.Vector2 LTRect::pivot
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___pivot_3;
	// UnityEngine.Vector2 LTRect::margin
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___margin_4;
	// UnityEngine.Rect LTRect::relativeRect
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___relativeRect_5;
	// System.Boolean LTRect::rotateEnabled
	bool ___rotateEnabled_6;
	// System.Boolean LTRect::rotateFinished
	bool ___rotateFinished_7;
	// System.Boolean LTRect::alphaEnabled
	bool ___alphaEnabled_8;
	// System.String LTRect::labelStr
	String_t* ___labelStr_9;
	// LTGUI/Element_Type LTRect::type
	int32_t ___type_10;
	// UnityEngine.GUIStyle LTRect::style
	GUIStyle_t29C59470ACD0A35C81EB0615653FD38C455A4726 * ___style_11;
	// System.Boolean LTRect::useColor
	bool ___useColor_12;
	// UnityEngine.Color LTRect::color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color_13;
	// System.Boolean LTRect::fontScaleToFit
	bool ___fontScaleToFit_14;
	// System.Boolean LTRect::useSimpleScale
	bool ___useSimpleScale_15;
	// System.Boolean LTRect::sizeByHeight
	bool ___sizeByHeight_16;
	// UnityEngine.Texture LTRect::texture
	Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___texture_17;
	// System.Int32 LTRect::_id
	int32_t ____id_18;
	// System.Int32 LTRect::counter
	int32_t ___counter_19;

public:
	inline static int32_t get_offset_of__rect_0() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ____rect_0)); }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  get__rect_0() const { return ____rect_0; }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * get_address_of__rect_0() { return &____rect_0; }
	inline void set__rect_0(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  value)
	{
		____rect_0 = value;
	}

	inline static int32_t get_offset_of_alpha_1() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___alpha_1)); }
	inline float get_alpha_1() const { return ___alpha_1; }
	inline float* get_address_of_alpha_1() { return &___alpha_1; }
	inline void set_alpha_1(float value)
	{
		___alpha_1 = value;
	}

	inline static int32_t get_offset_of_rotation_2() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___rotation_2)); }
	inline float get_rotation_2() const { return ___rotation_2; }
	inline float* get_address_of_rotation_2() { return &___rotation_2; }
	inline void set_rotation_2(float value)
	{
		___rotation_2 = value;
	}

	inline static int32_t get_offset_of_pivot_3() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___pivot_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_pivot_3() const { return ___pivot_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_pivot_3() { return &___pivot_3; }
	inline void set_pivot_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___pivot_3 = value;
	}

	inline static int32_t get_offset_of_margin_4() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___margin_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_margin_4() const { return ___margin_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_margin_4() { return &___margin_4; }
	inline void set_margin_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___margin_4 = value;
	}

	inline static int32_t get_offset_of_relativeRect_5() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___relativeRect_5)); }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  get_relativeRect_5() const { return ___relativeRect_5; }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * get_address_of_relativeRect_5() { return &___relativeRect_5; }
	inline void set_relativeRect_5(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  value)
	{
		___relativeRect_5 = value;
	}

	inline static int32_t get_offset_of_rotateEnabled_6() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___rotateEnabled_6)); }
	inline bool get_rotateEnabled_6() const { return ___rotateEnabled_6; }
	inline bool* get_address_of_rotateEnabled_6() { return &___rotateEnabled_6; }
	inline void set_rotateEnabled_6(bool value)
	{
		___rotateEnabled_6 = value;
	}

	inline static int32_t get_offset_of_rotateFinished_7() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___rotateFinished_7)); }
	inline bool get_rotateFinished_7() const { return ___rotateFinished_7; }
	inline bool* get_address_of_rotateFinished_7() { return &___rotateFinished_7; }
	inline void set_rotateFinished_7(bool value)
	{
		___rotateFinished_7 = value;
	}

	inline static int32_t get_offset_of_alphaEnabled_8() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___alphaEnabled_8)); }
	inline bool get_alphaEnabled_8() const { return ___alphaEnabled_8; }
	inline bool* get_address_of_alphaEnabled_8() { return &___alphaEnabled_8; }
	inline void set_alphaEnabled_8(bool value)
	{
		___alphaEnabled_8 = value;
	}

	inline static int32_t get_offset_of_labelStr_9() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___labelStr_9)); }
	inline String_t* get_labelStr_9() const { return ___labelStr_9; }
	inline String_t** get_address_of_labelStr_9() { return &___labelStr_9; }
	inline void set_labelStr_9(String_t* value)
	{
		___labelStr_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___labelStr_9), (void*)value);
	}

	inline static int32_t get_offset_of_type_10() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___type_10)); }
	inline int32_t get_type_10() const { return ___type_10; }
	inline int32_t* get_address_of_type_10() { return &___type_10; }
	inline void set_type_10(int32_t value)
	{
		___type_10 = value;
	}

	inline static int32_t get_offset_of_style_11() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___style_11)); }
	inline GUIStyle_t29C59470ACD0A35C81EB0615653FD38C455A4726 * get_style_11() const { return ___style_11; }
	inline GUIStyle_t29C59470ACD0A35C81EB0615653FD38C455A4726 ** get_address_of_style_11() { return &___style_11; }
	inline void set_style_11(GUIStyle_t29C59470ACD0A35C81EB0615653FD38C455A4726 * value)
	{
		___style_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___style_11), (void*)value);
	}

	inline static int32_t get_offset_of_useColor_12() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___useColor_12)); }
	inline bool get_useColor_12() const { return ___useColor_12; }
	inline bool* get_address_of_useColor_12() { return &___useColor_12; }
	inline void set_useColor_12(bool value)
	{
		___useColor_12 = value;
	}

	inline static int32_t get_offset_of_color_13() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___color_13)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_color_13() const { return ___color_13; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_color_13() { return &___color_13; }
	inline void set_color_13(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___color_13 = value;
	}

	inline static int32_t get_offset_of_fontScaleToFit_14() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___fontScaleToFit_14)); }
	inline bool get_fontScaleToFit_14() const { return ___fontScaleToFit_14; }
	inline bool* get_address_of_fontScaleToFit_14() { return &___fontScaleToFit_14; }
	inline void set_fontScaleToFit_14(bool value)
	{
		___fontScaleToFit_14 = value;
	}

	inline static int32_t get_offset_of_useSimpleScale_15() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___useSimpleScale_15)); }
	inline bool get_useSimpleScale_15() const { return ___useSimpleScale_15; }
	inline bool* get_address_of_useSimpleScale_15() { return &___useSimpleScale_15; }
	inline void set_useSimpleScale_15(bool value)
	{
		___useSimpleScale_15 = value;
	}

	inline static int32_t get_offset_of_sizeByHeight_16() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___sizeByHeight_16)); }
	inline bool get_sizeByHeight_16() const { return ___sizeByHeight_16; }
	inline bool* get_address_of_sizeByHeight_16() { return &___sizeByHeight_16; }
	inline void set_sizeByHeight_16(bool value)
	{
		___sizeByHeight_16 = value;
	}

	inline static int32_t get_offset_of_texture_17() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___texture_17)); }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * get_texture_17() const { return ___texture_17; }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE ** get_address_of_texture_17() { return &___texture_17; }
	inline void set_texture_17(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * value)
	{
		___texture_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___texture_17), (void*)value);
	}

	inline static int32_t get_offset_of__id_18() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ____id_18)); }
	inline int32_t get__id_18() const { return ____id_18; }
	inline int32_t* get_address_of__id_18() { return &____id_18; }
	inline void set__id_18(int32_t value)
	{
		____id_18 = value;
	}

	inline static int32_t get_offset_of_counter_19() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD, ___counter_19)); }
	inline int32_t get_counter_19() const { return ___counter_19; }
	inline int32_t* get_address_of_counter_19() { return &___counter_19; }
	inline void set_counter_19(int32_t value)
	{
		___counter_19 = value;
	}
};

struct LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD_StaticFields
{
public:
	// System.Boolean LTRect::colorTouched
	bool ___colorTouched_20;

public:
	inline static int32_t get_offset_of_colorTouched_20() { return static_cast<int32_t>(offsetof(LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD_StaticFields, ___colorTouched_20)); }
	inline bool get_colorTouched_20() const { return ___colorTouched_20; }
	inline bool* get_address_of_colorTouched_20() { return &___colorTouched_20; }
	inline void set_colorTouched_20(bool value)
	{
		___colorTouched_20 = value;
	}
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// CSCardData
struct CSCardData_tD1BD9207FB0542F11285FAA8DAC8A02F88C6C9B7  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// UnityEngine.Sprite CSCardData::sprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___sprite_4;
	// CSCardValue CSCardData::value
	CSCardValue_tEDFD24331AA54328DA51DA03E0123741F8729D38  ___value_5;

public:
	inline static int32_t get_offset_of_sprite_4() { return static_cast<int32_t>(offsetof(CSCardData_tD1BD9207FB0542F11285FAA8DAC8A02F88C6C9B7, ___sprite_4)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_sprite_4() const { return ___sprite_4; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_sprite_4() { return &___sprite_4; }
	inline void set_sprite_4(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___sprite_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sprite_4), (void*)value);
	}

	inline static int32_t get_offset_of_value_5() { return static_cast<int32_t>(offsetof(CSCardData_tD1BD9207FB0542F11285FAA8DAC8A02F88C6C9B7, ___value_5)); }
	inline CSCardValue_tEDFD24331AA54328DA51DA03E0123741F8729D38  get_value_5() const { return ___value_5; }
	inline CSCardValue_tEDFD24331AA54328DA51DA03E0123741F8729D38 * get_address_of_value_5() { return &___value_5; }
	inline void set_value_5(CSCardValue_tEDFD24331AA54328DA51DA03E0123741F8729D38  value)
	{
		___value_5 = value;
	}
};


// CSIAPProduct
struct CSIAPProduct_tF735AB0F8AA68F1481FA63E8AF5EB9AF02F7EAA2  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.String CSIAPProduct::productId
	String_t* ___productId_4;
	// UnityEngine.Purchasing.ProductType CSIAPProduct::type
	int32_t ___type_5;
	// System.Single CSIAPProduct::coins
	float ___coins_6;

public:
	inline static int32_t get_offset_of_productId_4() { return static_cast<int32_t>(offsetof(CSIAPProduct_tF735AB0F8AA68F1481FA63E8AF5EB9AF02F7EAA2, ___productId_4)); }
	inline String_t* get_productId_4() const { return ___productId_4; }
	inline String_t** get_address_of_productId_4() { return &___productId_4; }
	inline void set_productId_4(String_t* value)
	{
		___productId_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___productId_4), (void*)value);
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(CSIAPProduct_tF735AB0F8AA68F1481FA63E8AF5EB9AF02F7EAA2, ___type_5)); }
	inline int32_t get_type_5() const { return ___type_5; }
	inline int32_t* get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(int32_t value)
	{
		___type_5 = value;
	}

	inline static int32_t get_offset_of_coins_6() { return static_cast<int32_t>(offsetof(CSIAPProduct_tF735AB0F8AA68F1481FA63E8AF5EB9AF02F7EAA2, ___coins_6)); }
	inline float get_coins_6() const { return ___coins_6; }
	inline float* get_address_of_coins_6() { return &___coins_6; }
	inline void set_coins_6(float value)
	{
		___coins_6 = value;
	}
};


// CSLine
struct CSLine_t3E62A34AE974F1DA0D692280F027A34103D5214C  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.Int32 CSLine::number
	int32_t ___number_4;
	// UnityEngine.Sprite CSLine::sprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___sprite_5;
	// System.Collections.Generic.List`1<CSCell> CSLine::line
	List_1_t2FB4CCBD066E089694C632BDEB208275C984E004 * ___line_6;

public:
	inline static int32_t get_offset_of_number_4() { return static_cast<int32_t>(offsetof(CSLine_t3E62A34AE974F1DA0D692280F027A34103D5214C, ___number_4)); }
	inline int32_t get_number_4() const { return ___number_4; }
	inline int32_t* get_address_of_number_4() { return &___number_4; }
	inline void set_number_4(int32_t value)
	{
		___number_4 = value;
	}

	inline static int32_t get_offset_of_sprite_5() { return static_cast<int32_t>(offsetof(CSLine_t3E62A34AE974F1DA0D692280F027A34103D5214C, ___sprite_5)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_sprite_5() const { return ___sprite_5; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_sprite_5() { return &___sprite_5; }
	inline void set_sprite_5(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___sprite_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sprite_5), (void*)value);
	}

	inline static int32_t get_offset_of_line_6() { return static_cast<int32_t>(offsetof(CSLine_t3E62A34AE974F1DA0D692280F027A34103D5214C, ___line_6)); }
	inline List_1_t2FB4CCBD066E089694C632BDEB208275C984E004 * get_line_6() const { return ___line_6; }
	inline List_1_t2FB4CCBD066E089694C632BDEB208275C984E004 ** get_address_of_line_6() { return &___line_6; }
	inline void set_line_6(List_1_t2FB4CCBD066E089694C632BDEB208275C984E004 * value)
	{
		___line_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___line_6), (void*)value);
	}
};


// CSScatterPlayLine
struct CSScatterPlayLine_t18E8F2CA43C352FAAF3B96A656764BE385FCE0F0  : public CSPayline_t3562100776857062BF81F66EEEBBF159B5772FCF
{
public:
	// System.Boolean CSScatterPlayLine::runned
	bool ___runned_4;

public:
	inline static int32_t get_offset_of_runned_4() { return static_cast<int32_t>(offsetof(CSScatterPlayLine_t18E8F2CA43C352FAAF3B96A656764BE385FCE0F0, ___runned_4)); }
	inline bool get_runned_4() const { return ___runned_4; }
	inline bool* get_address_of_runned_4() { return &___runned_4; }
	inline void set_runned_4(bool value)
	{
		___runned_4 = value;
	}
};


// CSSymbolData
struct CSSymbolData_t189F10C509C4A90E39640E27AA4367BCC44D5816  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// CSSymbolType CSSymbolData::type
	int32_t ___type_4;
	// UnityEngine.GameObject CSSymbolData::particle
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___particle_5;
	// CSAnimationData CSSymbolData::animationData
	CSAnimationData_t39AC7CF1C6BE740E5450F413840F4B8E08309645 * ___animationData_6;
	// UnityEngine.Sprite CSSymbolData::sprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___sprite_7;
	// CSRule CSSymbolData::rule
	CSRule_t57A6472E5A907AC0E251BCF987970B41AD1F0561 * ___rule_8;

public:
	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(CSSymbolData_t189F10C509C4A90E39640E27AA4367BCC44D5816, ___type_4)); }
	inline int32_t get_type_4() const { return ___type_4; }
	inline int32_t* get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(int32_t value)
	{
		___type_4 = value;
	}

	inline static int32_t get_offset_of_particle_5() { return static_cast<int32_t>(offsetof(CSSymbolData_t189F10C509C4A90E39640E27AA4367BCC44D5816, ___particle_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_particle_5() const { return ___particle_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_particle_5() { return &___particle_5; }
	inline void set_particle_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___particle_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___particle_5), (void*)value);
	}

	inline static int32_t get_offset_of_animationData_6() { return static_cast<int32_t>(offsetof(CSSymbolData_t189F10C509C4A90E39640E27AA4367BCC44D5816, ___animationData_6)); }
	inline CSAnimationData_t39AC7CF1C6BE740E5450F413840F4B8E08309645 * get_animationData_6() const { return ___animationData_6; }
	inline CSAnimationData_t39AC7CF1C6BE740E5450F413840F4B8E08309645 ** get_address_of_animationData_6() { return &___animationData_6; }
	inline void set_animationData_6(CSAnimationData_t39AC7CF1C6BE740E5450F413840F4B8E08309645 * value)
	{
		___animationData_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animationData_6), (void*)value);
	}

	inline static int32_t get_offset_of_sprite_7() { return static_cast<int32_t>(offsetof(CSSymbolData_t189F10C509C4A90E39640E27AA4367BCC44D5816, ___sprite_7)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_sprite_7() const { return ___sprite_7; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_sprite_7() { return &___sprite_7; }
	inline void set_sprite_7(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___sprite_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sprite_7), (void*)value);
	}

	inline static int32_t get_offset_of_rule_8() { return static_cast<int32_t>(offsetof(CSSymbolData_t189F10C509C4A90E39640E27AA4367BCC44D5816, ___rule_8)); }
	inline CSRule_t57A6472E5A907AC0E251BCF987970B41AD1F0561 * get_rule_8() const { return ___rule_8; }
	inline CSRule_t57A6472E5A907AC0E251BCF987970B41AD1F0561 ** get_address_of_rule_8() { return &___rule_8; }
	inline void set_rule_8(CSRule_t57A6472E5A907AC0E251BCF987970B41AD1F0561 * value)
	{
		___rule_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rule_8), (void*)value);
	}
};


// TMPro.TMP_InputValidator
struct TMP_InputValidator_t5DE1CB404972CB5D787521EF3B382C27D5DB456D  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:

public:
};


// TimerDelegate
struct TimerDelegate_t34BCC9B9964D8BA67E6A87849C74455F86AFA1DB  : public MulticastDelegate_t
{
public:

public:
};


// LTDescr/ActionMethodDelegate
struct ActionMethodDelegate_t35A1DC5D365ACE4EFD8348E5E4DF89A1E9129C05  : public MulticastDelegate_t
{
public:

public:
};


// LTDescr/EaseTypeDelegate
struct EaseTypeDelegate_t0285AB55B28F5B5A6688E966B51FBA837B2E84AF  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// TMPro.TMP_DigitValidator
struct TMP_DigitValidator_t93649C48020A2E3764BB415BD7E4F742A3D5A878  : public TMP_InputValidator_t5DE1CB404972CB5D787521EF3B382C27D5DB456D
{
public:

public:
};


// TMPro.TMP_PhoneNumberValidator
struct TMP_PhoneNumberValidator_tF4A8EC8378D1DD16E227BEBC1C5B0AFC8BB018E3  : public TMP_InputValidator_t5DE1CB404972CB5D787521EF3B382C27D5DB456D
{
public:

public:
};


// TMPro.Examples.Benchmark01
struct Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 TMPro.Examples.Benchmark01::BenchmarkType
	int32_t ___BenchmarkType_4;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01::TMProFont
	TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * ___TMProFont_5;
	// UnityEngine.Font TMPro.Examples.Benchmark01::TextMeshFont
	Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 * ___TextMeshFont_6;
	// TMPro.TextMeshPro TMPro.Examples.Benchmark01::m_textMeshPro
	TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * ___m_textMeshPro_7;
	// TMPro.TextContainer TMPro.Examples.Benchmark01::m_textContainer
	TextContainer_t397B1340CD69150F936048DB53D857135408D2A1 * ___m_textContainer_8;
	// UnityEngine.TextMesh TMPro.Examples.Benchmark01::m_textMesh
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * ___m_textMesh_9;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material01
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_material01_12;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material02
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_material02_13;

public:
	inline static int32_t get_offset_of_BenchmarkType_4() { return static_cast<int32_t>(offsetof(Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934, ___BenchmarkType_4)); }
	inline int32_t get_BenchmarkType_4() const { return ___BenchmarkType_4; }
	inline int32_t* get_address_of_BenchmarkType_4() { return &___BenchmarkType_4; }
	inline void set_BenchmarkType_4(int32_t value)
	{
		___BenchmarkType_4 = value;
	}

	inline static int32_t get_offset_of_TMProFont_5() { return static_cast<int32_t>(offsetof(Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934, ___TMProFont_5)); }
	inline TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * get_TMProFont_5() const { return ___TMProFont_5; }
	inline TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 ** get_address_of_TMProFont_5() { return &___TMProFont_5; }
	inline void set_TMProFont_5(TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * value)
	{
		___TMProFont_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TMProFont_5), (void*)value);
	}

	inline static int32_t get_offset_of_TextMeshFont_6() { return static_cast<int32_t>(offsetof(Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934, ___TextMeshFont_6)); }
	inline Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 * get_TextMeshFont_6() const { return ___TextMeshFont_6; }
	inline Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 ** get_address_of_TextMeshFont_6() { return &___TextMeshFont_6; }
	inline void set_TextMeshFont_6(Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 * value)
	{
		___TextMeshFont_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TextMeshFont_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_7() { return static_cast<int32_t>(offsetof(Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934, ___m_textMeshPro_7)); }
	inline TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * get_m_textMeshPro_7() const { return ___m_textMeshPro_7; }
	inline TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 ** get_address_of_m_textMeshPro_7() { return &___m_textMeshPro_7; }
	inline void set_m_textMeshPro_7(TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * value)
	{
		___m_textMeshPro_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_textMeshPro_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_textContainer_8() { return static_cast<int32_t>(offsetof(Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934, ___m_textContainer_8)); }
	inline TextContainer_t397B1340CD69150F936048DB53D857135408D2A1 * get_m_textContainer_8() const { return ___m_textContainer_8; }
	inline TextContainer_t397B1340CD69150F936048DB53D857135408D2A1 ** get_address_of_m_textContainer_8() { return &___m_textContainer_8; }
	inline void set_m_textContainer_8(TextContainer_t397B1340CD69150F936048DB53D857135408D2A1 * value)
	{
		___m_textContainer_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_textContainer_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_textMesh_9() { return static_cast<int32_t>(offsetof(Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934, ___m_textMesh_9)); }
	inline TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * get_m_textMesh_9() const { return ___m_textMesh_9; }
	inline TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 ** get_address_of_m_textMesh_9() { return &___m_textMesh_9; }
	inline void set_m_textMesh_9(TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * value)
	{
		___m_textMesh_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_textMesh_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_material01_12() { return static_cast<int32_t>(offsetof(Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934, ___m_material01_12)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_material01_12() const { return ___m_material01_12; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_material01_12() { return &___m_material01_12; }
	inline void set_m_material01_12(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_material01_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_material01_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_material02_13() { return static_cast<int32_t>(offsetof(Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934, ___m_material02_13)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_material02_13() const { return ___m_material02_13; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_material02_13() { return &___m_material02_13; }
	inline void set_m_material02_13(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_material02_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_material02_13), (void*)value);
	}
};


// TMPro.Examples.Benchmark01_UGUI
struct Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI::BenchmarkType
	int32_t ___BenchmarkType_4;
	// UnityEngine.Canvas TMPro.Examples.Benchmark01_UGUI::canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___canvas_5;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01_UGUI::TMProFont
	TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * ___TMProFont_6;
	// UnityEngine.Font TMPro.Examples.Benchmark01_UGUI::TextMeshFont
	Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 * ___TextMeshFont_7;
	// TMPro.TextMeshProUGUI TMPro.Examples.Benchmark01_UGUI::m_textMeshPro
	TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * ___m_textMeshPro_8;
	// UnityEngine.UI.Text TMPro.Examples.Benchmark01_UGUI::m_textMesh
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___m_textMesh_9;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material01
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_material01_12;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material02
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_material02_13;

public:
	inline static int32_t get_offset_of_BenchmarkType_4() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B, ___BenchmarkType_4)); }
	inline int32_t get_BenchmarkType_4() const { return ___BenchmarkType_4; }
	inline int32_t* get_address_of_BenchmarkType_4() { return &___BenchmarkType_4; }
	inline void set_BenchmarkType_4(int32_t value)
	{
		___BenchmarkType_4 = value;
	}

	inline static int32_t get_offset_of_canvas_5() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B, ___canvas_5)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_canvas_5() const { return ___canvas_5; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_canvas_5() { return &___canvas_5; }
	inline void set_canvas_5(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___canvas_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvas_5), (void*)value);
	}

	inline static int32_t get_offset_of_TMProFont_6() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B, ___TMProFont_6)); }
	inline TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * get_TMProFont_6() const { return ___TMProFont_6; }
	inline TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 ** get_address_of_TMProFont_6() { return &___TMProFont_6; }
	inline void set_TMProFont_6(TMP_FontAsset_tDD8F58129CF4A9094C82DD209531E9E71F9837B2 * value)
	{
		___TMProFont_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TMProFont_6), (void*)value);
	}

	inline static int32_t get_offset_of_TextMeshFont_7() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B, ___TextMeshFont_7)); }
	inline Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 * get_TextMeshFont_7() const { return ___TextMeshFont_7; }
	inline Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 ** get_address_of_TextMeshFont_7() { return &___TextMeshFont_7; }
	inline void set_TextMeshFont_7(Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 * value)
	{
		___TextMeshFont_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TextMeshFont_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_8() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B, ___m_textMeshPro_8)); }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * get_m_textMeshPro_8() const { return ___m_textMeshPro_8; }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 ** get_address_of_m_textMeshPro_8() { return &___m_textMeshPro_8; }
	inline void set_m_textMeshPro_8(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * value)
	{
		___m_textMeshPro_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_textMeshPro_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_textMesh_9() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B, ___m_textMesh_9)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_m_textMesh_9() const { return ___m_textMesh_9; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_m_textMesh_9() { return &___m_textMesh_9; }
	inline void set_m_textMesh_9(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___m_textMesh_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_textMesh_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_material01_12() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B, ___m_material01_12)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_material01_12() const { return ___m_material01_12; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_material01_12() { return &___m_material01_12; }
	inline void set_m_material01_12(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_material01_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_material01_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_material02_13() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B, ___m_material02_13)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_material02_13() const { return ___m_material02_13; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_material02_13() { return &___m_material02_13; }
	inline void set_m_material02_13(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_material02_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_material02_13), (void*)value);
	}
};


// TMPro.Examples.Benchmark02
struct Benchmark02_tA0FA2BFB7AB5F7A8501D8DDFE0AF341C329663A1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 TMPro.Examples.Benchmark02::SpawnType
	int32_t ___SpawnType_4;
	// System.Int32 TMPro.Examples.Benchmark02::NumberOfNPC
	int32_t ___NumberOfNPC_5;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.Benchmark02::floatingText_Script
	TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200 * ___floatingText_Script_6;

public:
	inline static int32_t get_offset_of_SpawnType_4() { return static_cast<int32_t>(offsetof(Benchmark02_tA0FA2BFB7AB5F7A8501D8DDFE0AF341C329663A1, ___SpawnType_4)); }
	inline int32_t get_SpawnType_4() const { return ___SpawnType_4; }
	inline int32_t* get_address_of_SpawnType_4() { return &___SpawnType_4; }
	inline void set_SpawnType_4(int32_t value)
	{
		___SpawnType_4 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_5() { return static_cast<int32_t>(offsetof(Benchmark02_tA0FA2BFB7AB5F7A8501D8DDFE0AF341C329663A1, ___NumberOfNPC_5)); }
	inline int32_t get_NumberOfNPC_5() const { return ___NumberOfNPC_5; }
	inline int32_t* get_address_of_NumberOfNPC_5() { return &___NumberOfNPC_5; }
	inline void set_NumberOfNPC_5(int32_t value)
	{
		___NumberOfNPC_5 = value;
	}

	inline static int32_t get_offset_of_floatingText_Script_6() { return static_cast<int32_t>(offsetof(Benchmark02_tA0FA2BFB7AB5F7A8501D8DDFE0AF341C329663A1, ___floatingText_Script_6)); }
	inline TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200 * get_floatingText_Script_6() const { return ___floatingText_Script_6; }
	inline TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200 ** get_address_of_floatingText_Script_6() { return &___floatingText_Script_6; }
	inline void set_floatingText_Script_6(TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200 * value)
	{
		___floatingText_Script_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___floatingText_Script_6), (void*)value);
	}
};


// TMPro.Examples.Benchmark03
struct Benchmark03_t8AC945A66AB915DDE86610B70E9FD1E1ED8FF3EA  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 TMPro.Examples.Benchmark03::SpawnType
	int32_t ___SpawnType_4;
	// System.Int32 TMPro.Examples.Benchmark03::NumberOfNPC
	int32_t ___NumberOfNPC_5;
	// UnityEngine.Font TMPro.Examples.Benchmark03::TheFont
	Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 * ___TheFont_6;

public:
	inline static int32_t get_offset_of_SpawnType_4() { return static_cast<int32_t>(offsetof(Benchmark03_t8AC945A66AB915DDE86610B70E9FD1E1ED8FF3EA, ___SpawnType_4)); }
	inline int32_t get_SpawnType_4() const { return ___SpawnType_4; }
	inline int32_t* get_address_of_SpawnType_4() { return &___SpawnType_4; }
	inline void set_SpawnType_4(int32_t value)
	{
		___SpawnType_4 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_5() { return static_cast<int32_t>(offsetof(Benchmark03_t8AC945A66AB915DDE86610B70E9FD1E1ED8FF3EA, ___NumberOfNPC_5)); }
	inline int32_t get_NumberOfNPC_5() const { return ___NumberOfNPC_5; }
	inline int32_t* get_address_of_NumberOfNPC_5() { return &___NumberOfNPC_5; }
	inline void set_NumberOfNPC_5(int32_t value)
	{
		___NumberOfNPC_5 = value;
	}

	inline static int32_t get_offset_of_TheFont_6() { return static_cast<int32_t>(offsetof(Benchmark03_t8AC945A66AB915DDE86610B70E9FD1E1ED8FF3EA, ___TheFont_6)); }
	inline Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 * get_TheFont_6() const { return ___TheFont_6; }
	inline Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 ** get_address_of_TheFont_6() { return &___TheFont_6; }
	inline void set_TheFont_6(Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 * value)
	{
		___TheFont_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TheFont_6), (void*)value);
	}
};


// TMPro.Examples.Benchmark04
struct Benchmark04_t62DC44C021DD8E8057039D65F66E816CFE713D68  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 TMPro.Examples.Benchmark04::SpawnType
	int32_t ___SpawnType_4;
	// System.Int32 TMPro.Examples.Benchmark04::MinPointSize
	int32_t ___MinPointSize_5;
	// System.Int32 TMPro.Examples.Benchmark04::MaxPointSize
	int32_t ___MaxPointSize_6;
	// System.Int32 TMPro.Examples.Benchmark04::Steps
	int32_t ___Steps_7;
	// UnityEngine.Transform TMPro.Examples.Benchmark04::m_Transform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_Transform_8;

public:
	inline static int32_t get_offset_of_SpawnType_4() { return static_cast<int32_t>(offsetof(Benchmark04_t62DC44C021DD8E8057039D65F66E816CFE713D68, ___SpawnType_4)); }
	inline int32_t get_SpawnType_4() const { return ___SpawnType_4; }
	inline int32_t* get_address_of_SpawnType_4() { return &___SpawnType_4; }
	inline void set_SpawnType_4(int32_t value)
	{
		___SpawnType_4 = value;
	}

	inline static int32_t get_offset_of_MinPointSize_5() { return static_cast<int32_t>(offsetof(Benchmark04_t62DC44C021DD8E8057039D65F66E816CFE713D68, ___MinPointSize_5)); }
	inline int32_t get_MinPointSize_5() const { return ___MinPointSize_5; }
	inline int32_t* get_address_of_MinPointSize_5() { return &___MinPointSize_5; }
	inline void set_MinPointSize_5(int32_t value)
	{
		___MinPointSize_5 = value;
	}

	inline static int32_t get_offset_of_MaxPointSize_6() { return static_cast<int32_t>(offsetof(Benchmark04_t62DC44C021DD8E8057039D65F66E816CFE713D68, ___MaxPointSize_6)); }
	inline int32_t get_MaxPointSize_6() const { return ___MaxPointSize_6; }
	inline int32_t* get_address_of_MaxPointSize_6() { return &___MaxPointSize_6; }
	inline void set_MaxPointSize_6(int32_t value)
	{
		___MaxPointSize_6 = value;
	}

	inline static int32_t get_offset_of_Steps_7() { return static_cast<int32_t>(offsetof(Benchmark04_t62DC44C021DD8E8057039D65F66E816CFE713D68, ___Steps_7)); }
	inline int32_t get_Steps_7() const { return ___Steps_7; }
	inline int32_t* get_address_of_Steps_7() { return &___Steps_7; }
	inline void set_Steps_7(int32_t value)
	{
		___Steps_7 = value;
	}

	inline static int32_t get_offset_of_m_Transform_8() { return static_cast<int32_t>(offsetof(Benchmark04_t62DC44C021DD8E8057039D65F66E816CFE713D68, ___m_Transform_8)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_Transform_8() const { return ___m_Transform_8; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_Transform_8() { return &___m_Transform_8; }
	inline void set_m_Transform_8(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_Transform_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Transform_8), (void*)value);
	}
};


// CSAdMobManager
struct CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String CSAdMobManager::adMobAppIdiOS
	String_t* ___adMobAppIdiOS_4;
	// System.String CSAdMobManager::adMobAppIdAndroid
	String_t* ___adMobAppIdAndroid_5;
	// System.String CSAdMobManager::adMobAppInterstitialiOS
	String_t* ___adMobAppInterstitialiOS_6;
	// System.String CSAdMobManager::adMobAppInterstitialAndroid
	String_t* ___adMobAppInterstitialAndroid_7;
	// System.String CSAdMobManager::adMobAppRewardiOS
	String_t* ___adMobAppRewardiOS_8;
	// System.String CSAdMobManager::adMobAppRewardAndroid
	String_t* ___adMobAppRewardAndroid_9;
	// GoogleMobileAds.Api.InterstitialAd CSAdMobManager::_interstitial
	InterstitialAd_t9ED58965C70F2CF9E66F92EAE19149A1A237E8C9 * ____interstitial_11;
	// System.Boolean CSAdMobManager::_live
	bool ____live_12;
	// GoogleMobileAds.Api.RewardBasedVideoAd CSAdMobManager::_rewardBasedVideo
	RewardBasedVideoAd_tB89863A4548A04A4DCF7E1E8408F5648E6BC84CC * ____rewardBasedVideo_13;
	// System.Action CSAdMobManager::_RewardVideoCallback
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ____RewardVideoCallback_14;
	// System.Action CSAdMobManager::_InterstitialCallback
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ____InterstitialCallback_15;

public:
	inline static int32_t get_offset_of_adMobAppIdiOS_4() { return static_cast<int32_t>(offsetof(CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8, ___adMobAppIdiOS_4)); }
	inline String_t* get_adMobAppIdiOS_4() const { return ___adMobAppIdiOS_4; }
	inline String_t** get_address_of_adMobAppIdiOS_4() { return &___adMobAppIdiOS_4; }
	inline void set_adMobAppIdiOS_4(String_t* value)
	{
		___adMobAppIdiOS_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___adMobAppIdiOS_4), (void*)value);
	}

	inline static int32_t get_offset_of_adMobAppIdAndroid_5() { return static_cast<int32_t>(offsetof(CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8, ___adMobAppIdAndroid_5)); }
	inline String_t* get_adMobAppIdAndroid_5() const { return ___adMobAppIdAndroid_5; }
	inline String_t** get_address_of_adMobAppIdAndroid_5() { return &___adMobAppIdAndroid_5; }
	inline void set_adMobAppIdAndroid_5(String_t* value)
	{
		___adMobAppIdAndroid_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___adMobAppIdAndroid_5), (void*)value);
	}

	inline static int32_t get_offset_of_adMobAppInterstitialiOS_6() { return static_cast<int32_t>(offsetof(CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8, ___adMobAppInterstitialiOS_6)); }
	inline String_t* get_adMobAppInterstitialiOS_6() const { return ___adMobAppInterstitialiOS_6; }
	inline String_t** get_address_of_adMobAppInterstitialiOS_6() { return &___adMobAppInterstitialiOS_6; }
	inline void set_adMobAppInterstitialiOS_6(String_t* value)
	{
		___adMobAppInterstitialiOS_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___adMobAppInterstitialiOS_6), (void*)value);
	}

	inline static int32_t get_offset_of_adMobAppInterstitialAndroid_7() { return static_cast<int32_t>(offsetof(CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8, ___adMobAppInterstitialAndroid_7)); }
	inline String_t* get_adMobAppInterstitialAndroid_7() const { return ___adMobAppInterstitialAndroid_7; }
	inline String_t** get_address_of_adMobAppInterstitialAndroid_7() { return &___adMobAppInterstitialAndroid_7; }
	inline void set_adMobAppInterstitialAndroid_7(String_t* value)
	{
		___adMobAppInterstitialAndroid_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___adMobAppInterstitialAndroid_7), (void*)value);
	}

	inline static int32_t get_offset_of_adMobAppRewardiOS_8() { return static_cast<int32_t>(offsetof(CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8, ___adMobAppRewardiOS_8)); }
	inline String_t* get_adMobAppRewardiOS_8() const { return ___adMobAppRewardiOS_8; }
	inline String_t** get_address_of_adMobAppRewardiOS_8() { return &___adMobAppRewardiOS_8; }
	inline void set_adMobAppRewardiOS_8(String_t* value)
	{
		___adMobAppRewardiOS_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___adMobAppRewardiOS_8), (void*)value);
	}

	inline static int32_t get_offset_of_adMobAppRewardAndroid_9() { return static_cast<int32_t>(offsetof(CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8, ___adMobAppRewardAndroid_9)); }
	inline String_t* get_adMobAppRewardAndroid_9() const { return ___adMobAppRewardAndroid_9; }
	inline String_t** get_address_of_adMobAppRewardAndroid_9() { return &___adMobAppRewardAndroid_9; }
	inline void set_adMobAppRewardAndroid_9(String_t* value)
	{
		___adMobAppRewardAndroid_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___adMobAppRewardAndroid_9), (void*)value);
	}

	inline static int32_t get_offset_of__interstitial_11() { return static_cast<int32_t>(offsetof(CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8, ____interstitial_11)); }
	inline InterstitialAd_t9ED58965C70F2CF9E66F92EAE19149A1A237E8C9 * get__interstitial_11() const { return ____interstitial_11; }
	inline InterstitialAd_t9ED58965C70F2CF9E66F92EAE19149A1A237E8C9 ** get_address_of__interstitial_11() { return &____interstitial_11; }
	inline void set__interstitial_11(InterstitialAd_t9ED58965C70F2CF9E66F92EAE19149A1A237E8C9 * value)
	{
		____interstitial_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____interstitial_11), (void*)value);
	}

	inline static int32_t get_offset_of__live_12() { return static_cast<int32_t>(offsetof(CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8, ____live_12)); }
	inline bool get__live_12() const { return ____live_12; }
	inline bool* get_address_of__live_12() { return &____live_12; }
	inline void set__live_12(bool value)
	{
		____live_12 = value;
	}

	inline static int32_t get_offset_of__rewardBasedVideo_13() { return static_cast<int32_t>(offsetof(CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8, ____rewardBasedVideo_13)); }
	inline RewardBasedVideoAd_tB89863A4548A04A4DCF7E1E8408F5648E6BC84CC * get__rewardBasedVideo_13() const { return ____rewardBasedVideo_13; }
	inline RewardBasedVideoAd_tB89863A4548A04A4DCF7E1E8408F5648E6BC84CC ** get_address_of__rewardBasedVideo_13() { return &____rewardBasedVideo_13; }
	inline void set__rewardBasedVideo_13(RewardBasedVideoAd_tB89863A4548A04A4DCF7E1E8408F5648E6BC84CC * value)
	{
		____rewardBasedVideo_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rewardBasedVideo_13), (void*)value);
	}

	inline static int32_t get_offset_of__RewardVideoCallback_14() { return static_cast<int32_t>(offsetof(CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8, ____RewardVideoCallback_14)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get__RewardVideoCallback_14() const { return ____RewardVideoCallback_14; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of__RewardVideoCallback_14() { return &____RewardVideoCallback_14; }
	inline void set__RewardVideoCallback_14(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		____RewardVideoCallback_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____RewardVideoCallback_14), (void*)value);
	}

	inline static int32_t get_offset_of__InterstitialCallback_15() { return static_cast<int32_t>(offsetof(CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8, ____InterstitialCallback_15)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get__InterstitialCallback_15() const { return ____InterstitialCallback_15; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of__InterstitialCallback_15() { return &____InterstitialCallback_15; }
	inline void set__InterstitialCallback_15(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		____InterstitialCallback_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____InterstitialCallback_15), (void*)value);
	}
};

struct CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8_StaticFields
{
public:
	// CSAdMobManager CSAdMobManager::instance
	CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8 * ___instance_10;

public:
	inline static int32_t get_offset_of_instance_10() { return static_cast<int32_t>(offsetof(CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8_StaticFields, ___instance_10)); }
	inline CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8 * get_instance_10() const { return ___instance_10; }
	inline CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8 ** get_address_of_instance_10() { return &___instance_10; }
	inline void set_instance_10(CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8 * value)
	{
		___instance_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_10), (void*)value);
	}
};


// CSAlert
struct CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.ParticleSystem CSAlert::fireworks
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___fireworks_4;
	// UnityEngine.RectTransform CSAlert::ribbon
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___ribbon_5;
	// UnityEngine.Transform CSAlert::title
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___title_6;
	// UnityEngine.Transform CSAlert::board
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___board_7;
	// UnityEngine.CanvasGroup CSAlert::canvas
	CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * ___canvas_8;
	// UnityEngine.CanvasGroup CSAlert::innerCanvas
	CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * ___innerCanvas_9;
	// UnityEngine.GameObject CSAlert::_title
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____title_10;
	// UnityEngine.GameObject CSAlert::_board
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____board_11;
	// System.Single CSAlert::_ribbonWidth
	float ____ribbonWidth_12;
	// UnityEngine.UI.Image CSAlert::_image
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ____image_13;
	// UnityEngine.GameObject CSAlert::_particle
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____particle_14;
	// CSGlowAnimation CSAlert::_glowScript
	CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186 * ____glowScript_15;
	// UnityEngine.Vector3 CSAlert::_titleShowPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____titleShowPosition_16;
	// UnityEngine.Vector3 CSAlert::_boardShowPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____boardShowPosition_17;
	// System.Boolean CSAlert::_active
	bool ____active_18;

public:
	inline static int32_t get_offset_of_fireworks_4() { return static_cast<int32_t>(offsetof(CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A, ___fireworks_4)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_fireworks_4() const { return ___fireworks_4; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_fireworks_4() { return &___fireworks_4; }
	inline void set_fireworks_4(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___fireworks_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fireworks_4), (void*)value);
	}

	inline static int32_t get_offset_of_ribbon_5() { return static_cast<int32_t>(offsetof(CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A, ___ribbon_5)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_ribbon_5() const { return ___ribbon_5; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_ribbon_5() { return &___ribbon_5; }
	inline void set_ribbon_5(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___ribbon_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ribbon_5), (void*)value);
	}

	inline static int32_t get_offset_of_title_6() { return static_cast<int32_t>(offsetof(CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A, ___title_6)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_title_6() const { return ___title_6; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_title_6() { return &___title_6; }
	inline void set_title_6(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___title_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___title_6), (void*)value);
	}

	inline static int32_t get_offset_of_board_7() { return static_cast<int32_t>(offsetof(CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A, ___board_7)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_board_7() const { return ___board_7; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_board_7() { return &___board_7; }
	inline void set_board_7(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___board_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___board_7), (void*)value);
	}

	inline static int32_t get_offset_of_canvas_8() { return static_cast<int32_t>(offsetof(CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A, ___canvas_8)); }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * get_canvas_8() const { return ___canvas_8; }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F ** get_address_of_canvas_8() { return &___canvas_8; }
	inline void set_canvas_8(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * value)
	{
		___canvas_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvas_8), (void*)value);
	}

	inline static int32_t get_offset_of_innerCanvas_9() { return static_cast<int32_t>(offsetof(CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A, ___innerCanvas_9)); }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * get_innerCanvas_9() const { return ___innerCanvas_9; }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F ** get_address_of_innerCanvas_9() { return &___innerCanvas_9; }
	inline void set_innerCanvas_9(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * value)
	{
		___innerCanvas_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___innerCanvas_9), (void*)value);
	}

	inline static int32_t get_offset_of__title_10() { return static_cast<int32_t>(offsetof(CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A, ____title_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__title_10() const { return ____title_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__title_10() { return &____title_10; }
	inline void set__title_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____title_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____title_10), (void*)value);
	}

	inline static int32_t get_offset_of__board_11() { return static_cast<int32_t>(offsetof(CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A, ____board_11)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__board_11() const { return ____board_11; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__board_11() { return &____board_11; }
	inline void set__board_11(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____board_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____board_11), (void*)value);
	}

	inline static int32_t get_offset_of__ribbonWidth_12() { return static_cast<int32_t>(offsetof(CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A, ____ribbonWidth_12)); }
	inline float get__ribbonWidth_12() const { return ____ribbonWidth_12; }
	inline float* get_address_of__ribbonWidth_12() { return &____ribbonWidth_12; }
	inline void set__ribbonWidth_12(float value)
	{
		____ribbonWidth_12 = value;
	}

	inline static int32_t get_offset_of__image_13() { return static_cast<int32_t>(offsetof(CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A, ____image_13)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get__image_13() const { return ____image_13; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of__image_13() { return &____image_13; }
	inline void set__image_13(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		____image_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____image_13), (void*)value);
	}

	inline static int32_t get_offset_of__particle_14() { return static_cast<int32_t>(offsetof(CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A, ____particle_14)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__particle_14() const { return ____particle_14; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__particle_14() { return &____particle_14; }
	inline void set__particle_14(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____particle_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____particle_14), (void*)value);
	}

	inline static int32_t get_offset_of__glowScript_15() { return static_cast<int32_t>(offsetof(CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A, ____glowScript_15)); }
	inline CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186 * get__glowScript_15() const { return ____glowScript_15; }
	inline CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186 ** get_address_of__glowScript_15() { return &____glowScript_15; }
	inline void set__glowScript_15(CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186 * value)
	{
		____glowScript_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____glowScript_15), (void*)value);
	}

	inline static int32_t get_offset_of__titleShowPosition_16() { return static_cast<int32_t>(offsetof(CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A, ____titleShowPosition_16)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__titleShowPosition_16() const { return ____titleShowPosition_16; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__titleShowPosition_16() { return &____titleShowPosition_16; }
	inline void set__titleShowPosition_16(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____titleShowPosition_16 = value;
	}

	inline static int32_t get_offset_of__boardShowPosition_17() { return static_cast<int32_t>(offsetof(CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A, ____boardShowPosition_17)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__boardShowPosition_17() const { return ____boardShowPosition_17; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__boardShowPosition_17() { return &____boardShowPosition_17; }
	inline void set__boardShowPosition_17(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____boardShowPosition_17 = value;
	}

	inline static int32_t get_offset_of__active_18() { return static_cast<int32_t>(offsetof(CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A, ____active_18)); }
	inline bool get__active_18() const { return ____active_18; }
	inline bool* get_address_of__active_18() { return &____active_18; }
	inline void set__active_18(bool value)
	{
		____active_18 = value;
	}
};


// CSBankCoinPanel
struct CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Action`1<CSBankCoinPanel> CSBankCoinPanel::bankValueChanged
	Action_1_t0B84DFBE5FC87EC8D8B398C1BACFE174F3A5D542 * ___bankValueChanged_4;
	// UnityEngine.UI.Text CSBankCoinPanel::text
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___text_5;
	// UnityEngine.GameObject CSBankCoinPanel::particle
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___particle_6;
	// UnityEngine.RectTransform CSBankCoinPanel::coinIcon
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___coinIcon_7;
	// System.Boolean CSBankCoinPanel::formatText
	bool ___formatText_8;
	// System.Single CSBankCoinPanel::_bank
	float ____bank_9;

public:
	inline static int32_t get_offset_of_bankValueChanged_4() { return static_cast<int32_t>(offsetof(CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029, ___bankValueChanged_4)); }
	inline Action_1_t0B84DFBE5FC87EC8D8B398C1BACFE174F3A5D542 * get_bankValueChanged_4() const { return ___bankValueChanged_4; }
	inline Action_1_t0B84DFBE5FC87EC8D8B398C1BACFE174F3A5D542 ** get_address_of_bankValueChanged_4() { return &___bankValueChanged_4; }
	inline void set_bankValueChanged_4(Action_1_t0B84DFBE5FC87EC8D8B398C1BACFE174F3A5D542 * value)
	{
		___bankValueChanged_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bankValueChanged_4), (void*)value);
	}

	inline static int32_t get_offset_of_text_5() { return static_cast<int32_t>(offsetof(CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029, ___text_5)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_text_5() const { return ___text_5; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_text_5() { return &___text_5; }
	inline void set_text_5(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___text_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___text_5), (void*)value);
	}

	inline static int32_t get_offset_of_particle_6() { return static_cast<int32_t>(offsetof(CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029, ___particle_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_particle_6() const { return ___particle_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_particle_6() { return &___particle_6; }
	inline void set_particle_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___particle_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___particle_6), (void*)value);
	}

	inline static int32_t get_offset_of_coinIcon_7() { return static_cast<int32_t>(offsetof(CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029, ___coinIcon_7)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_coinIcon_7() const { return ___coinIcon_7; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_coinIcon_7() { return &___coinIcon_7; }
	inline void set_coinIcon_7(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___coinIcon_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___coinIcon_7), (void*)value);
	}

	inline static int32_t get_offset_of_formatText_8() { return static_cast<int32_t>(offsetof(CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029, ___formatText_8)); }
	inline bool get_formatText_8() const { return ___formatText_8; }
	inline bool* get_address_of_formatText_8() { return &___formatText_8; }
	inline void set_formatText_8(bool value)
	{
		___formatText_8 = value;
	}

	inline static int32_t get_offset_of__bank_9() { return static_cast<int32_t>(offsetof(CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029, ____bank_9)); }
	inline float get__bank_9() const { return ____bank_9; }
	inline float* get_address_of__bank_9() { return &____bank_9; }
	inline void set__bank_9(float value)
	{
		____bank_9 = value;
	}
};


// CSBottomPanel
struct CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// CSBankCoinPanel CSBottomPanel::coins
	CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 * ___coins_4;
	// UnityEngine.UI.Button CSBottomPanel::spinButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___spinButton_5;
	// UnityEngine.UI.Text CSBottomPanel::betLabel
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___betLabel_6;
	// System.Int32 CSBottomPanel::_totalBet
	int32_t ____totalBet_7;
	// UnityEngine.UI.Text CSBottomPanel::winLabel
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___winLabel_8;
	// System.Single CSBottomPanel::_win
	float ____win_9;
	// System.Int32 CSBottomPanel::minBet
	int32_t ___minBet_10;
	// System.Int32 CSBottomPanel::maxStep
	int32_t ___maxStep_11;
	// CSExperiencePanel CSBottomPanel::xp
	CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F * ___xp_12;
	// System.Int32 CSBottomPanel::_step
	int32_t ____step_13;
	// System.Boolean CSBottomPanel::_enableSpin
	bool ____enableSpin_14;
	// UnityEngine.UI.Button[] CSBottomPanel::_buttons
	ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B* ____buttons_15;
	// System.Boolean CSBottomPanel::_enable
	bool ____enable_16;

public:
	inline static int32_t get_offset_of_coins_4() { return static_cast<int32_t>(offsetof(CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38, ___coins_4)); }
	inline CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 * get_coins_4() const { return ___coins_4; }
	inline CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 ** get_address_of_coins_4() { return &___coins_4; }
	inline void set_coins_4(CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 * value)
	{
		___coins_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___coins_4), (void*)value);
	}

	inline static int32_t get_offset_of_spinButton_5() { return static_cast<int32_t>(offsetof(CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38, ___spinButton_5)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_spinButton_5() const { return ___spinButton_5; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_spinButton_5() { return &___spinButton_5; }
	inline void set_spinButton_5(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___spinButton_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spinButton_5), (void*)value);
	}

	inline static int32_t get_offset_of_betLabel_6() { return static_cast<int32_t>(offsetof(CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38, ___betLabel_6)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_betLabel_6() const { return ___betLabel_6; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_betLabel_6() { return &___betLabel_6; }
	inline void set_betLabel_6(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___betLabel_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___betLabel_6), (void*)value);
	}

	inline static int32_t get_offset_of__totalBet_7() { return static_cast<int32_t>(offsetof(CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38, ____totalBet_7)); }
	inline int32_t get__totalBet_7() const { return ____totalBet_7; }
	inline int32_t* get_address_of__totalBet_7() { return &____totalBet_7; }
	inline void set__totalBet_7(int32_t value)
	{
		____totalBet_7 = value;
	}

	inline static int32_t get_offset_of_winLabel_8() { return static_cast<int32_t>(offsetof(CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38, ___winLabel_8)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_winLabel_8() const { return ___winLabel_8; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_winLabel_8() { return &___winLabel_8; }
	inline void set_winLabel_8(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___winLabel_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___winLabel_8), (void*)value);
	}

	inline static int32_t get_offset_of__win_9() { return static_cast<int32_t>(offsetof(CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38, ____win_9)); }
	inline float get__win_9() const { return ____win_9; }
	inline float* get_address_of__win_9() { return &____win_9; }
	inline void set__win_9(float value)
	{
		____win_9 = value;
	}

	inline static int32_t get_offset_of_minBet_10() { return static_cast<int32_t>(offsetof(CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38, ___minBet_10)); }
	inline int32_t get_minBet_10() const { return ___minBet_10; }
	inline int32_t* get_address_of_minBet_10() { return &___minBet_10; }
	inline void set_minBet_10(int32_t value)
	{
		___minBet_10 = value;
	}

	inline static int32_t get_offset_of_maxStep_11() { return static_cast<int32_t>(offsetof(CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38, ___maxStep_11)); }
	inline int32_t get_maxStep_11() const { return ___maxStep_11; }
	inline int32_t* get_address_of_maxStep_11() { return &___maxStep_11; }
	inline void set_maxStep_11(int32_t value)
	{
		___maxStep_11 = value;
	}

	inline static int32_t get_offset_of_xp_12() { return static_cast<int32_t>(offsetof(CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38, ___xp_12)); }
	inline CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F * get_xp_12() const { return ___xp_12; }
	inline CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F ** get_address_of_xp_12() { return &___xp_12; }
	inline void set_xp_12(CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F * value)
	{
		___xp_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___xp_12), (void*)value);
	}

	inline static int32_t get_offset_of__step_13() { return static_cast<int32_t>(offsetof(CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38, ____step_13)); }
	inline int32_t get__step_13() const { return ____step_13; }
	inline int32_t* get_address_of__step_13() { return &____step_13; }
	inline void set__step_13(int32_t value)
	{
		____step_13 = value;
	}

	inline static int32_t get_offset_of__enableSpin_14() { return static_cast<int32_t>(offsetof(CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38, ____enableSpin_14)); }
	inline bool get__enableSpin_14() const { return ____enableSpin_14; }
	inline bool* get_address_of__enableSpin_14() { return &____enableSpin_14; }
	inline void set__enableSpin_14(bool value)
	{
		____enableSpin_14 = value;
	}

	inline static int32_t get_offset_of__buttons_15() { return static_cast<int32_t>(offsetof(CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38, ____buttons_15)); }
	inline ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B* get__buttons_15() const { return ____buttons_15; }
	inline ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B** get_address_of__buttons_15() { return &____buttons_15; }
	inline void set__buttons_15(ButtonU5BU5D_t2D8C7329F91F78C37FD1A3807DCD4366F7D7EC7B* value)
	{
		____buttons_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____buttons_15), (void*)value);
	}

	inline static int32_t get_offset_of__enable_16() { return static_cast<int32_t>(offsetof(CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38, ____enable_16)); }
	inline bool get__enable_16() const { return ____enable_16; }
	inline bool* get_address_of__enable_16() { return &____enable_16; }
	inline void set__enable_16(bool value)
	{
		____enable_16 = value;
	}
};


// CSButtonTap
struct CSButtonTap_t9C5F9EEF2D29E6AA91FB12987E291832E61CA7FA  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// CSCSBlock
struct CSCSBlock_t46BF4D64718D4116A97602551C8D8A7EE394E14A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject CSCSBlock::blick
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___blick_4;
	// UnityEngine.RectTransform CSCSBlock::_rectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ____rectTransform_5;
	// System.Boolean CSCSBlock::_animate
	bool ____animate_6;

public:
	inline static int32_t get_offset_of_blick_4() { return static_cast<int32_t>(offsetof(CSCSBlock_t46BF4D64718D4116A97602551C8D8A7EE394E14A, ___blick_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_blick_4() const { return ___blick_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_blick_4() { return &___blick_4; }
	inline void set_blick_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___blick_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___blick_4), (void*)value);
	}

	inline static int32_t get_offset_of__rectTransform_5() { return static_cast<int32_t>(offsetof(CSCSBlock_t46BF4D64718D4116A97602551C8D8A7EE394E14A, ____rectTransform_5)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get__rectTransform_5() const { return ____rectTransform_5; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of__rectTransform_5() { return &____rectTransform_5; }
	inline void set__rectTransform_5(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		____rectTransform_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rectTransform_5), (void*)value);
	}

	inline static int32_t get_offset_of__animate_6() { return static_cast<int32_t>(offsetof(CSCSBlock_t46BF4D64718D4116A97602551C8D8A7EE394E14A, ____animate_6)); }
	inline bool get__animate_6() const { return ____animate_6; }
	inline bool* get_address_of__animate_6() { return &____animate_6; }
	inline void set__animate_6(bool value)
	{
		____animate_6 = value;
	}
};


// CSCSFreeGameInterface
struct CSCSFreeGameInterface_t77251F89137DEC1449785ADDF44AEDE47985BEC8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Sprite CSCSFreeGameInterface::freeGameSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___freeGameSprite_4;
	// CSReels CSCSFreeGameInterface::reels
	CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47 * ___reels_5;
	// UnityEngine.Sprite CSCSFreeGameInterface::_gamePlaySprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ____gamePlaySprite_6;
	// UnityEngine.UI.Image CSCSFreeGameInterface::_image
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ____image_7;

public:
	inline static int32_t get_offset_of_freeGameSprite_4() { return static_cast<int32_t>(offsetof(CSCSFreeGameInterface_t77251F89137DEC1449785ADDF44AEDE47985BEC8, ___freeGameSprite_4)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_freeGameSprite_4() const { return ___freeGameSprite_4; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_freeGameSprite_4() { return &___freeGameSprite_4; }
	inline void set_freeGameSprite_4(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___freeGameSprite_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___freeGameSprite_4), (void*)value);
	}

	inline static int32_t get_offset_of_reels_5() { return static_cast<int32_t>(offsetof(CSCSFreeGameInterface_t77251F89137DEC1449785ADDF44AEDE47985BEC8, ___reels_5)); }
	inline CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47 * get_reels_5() const { return ___reels_5; }
	inline CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47 ** get_address_of_reels_5() { return &___reels_5; }
	inline void set_reels_5(CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47 * value)
	{
		___reels_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reels_5), (void*)value);
	}

	inline static int32_t get_offset_of__gamePlaySprite_6() { return static_cast<int32_t>(offsetof(CSCSFreeGameInterface_t77251F89137DEC1449785ADDF44AEDE47985BEC8, ____gamePlaySprite_6)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get__gamePlaySprite_6() const { return ____gamePlaySprite_6; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of__gamePlaySprite_6() { return &____gamePlaySprite_6; }
	inline void set__gamePlaySprite_6(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		____gamePlaySprite_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____gamePlaySprite_6), (void*)value);
	}

	inline static int32_t get_offset_of__image_7() { return static_cast<int32_t>(offsetof(CSCSFreeGameInterface_t77251F89137DEC1449785ADDF44AEDE47985BEC8, ____image_7)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get__image_7() const { return ____image_7; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of__image_7() { return &____image_7; }
	inline void set__image_7(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		____image_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____image_7), (void*)value);
	}
};


// CSCSLoading
struct CSCSLoading_tA5936CA8227F956F2FBA41BEDC75DE00A57BAB2A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// CSLFProgressBar CSCSLoading::progressBar
	CSLFProgressBar_t2236B83E6C32E322F6C3707BEEAD2FC5EE9F2035 * ___progressBar_4;

public:
	inline static int32_t get_offset_of_progressBar_4() { return static_cast<int32_t>(offsetof(CSCSLoading_tA5936CA8227F956F2FBA41BEDC75DE00A57BAB2A, ___progressBar_4)); }
	inline CSLFProgressBar_t2236B83E6C32E322F6C3707BEEAD2FC5EE9F2035 * get_progressBar_4() const { return ___progressBar_4; }
	inline CSLFProgressBar_t2236B83E6C32E322F6C3707BEEAD2FC5EE9F2035 ** get_address_of_progressBar_4() { return &___progressBar_4; }
	inline void set_progressBar_4(CSLFProgressBar_t2236B83E6C32E322F6C3707BEEAD2FC5EE9F2035 * value)
	{
		___progressBar_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___progressBar_4), (void*)value);
	}
};

struct CSCSLoading_tA5936CA8227F956F2FBA41BEDC75DE00A57BAB2A_StaticFields
{
public:
	// CSLoadingState CSCSLoading::state
	int32_t ___state_5;

public:
	inline static int32_t get_offset_of_state_5() { return static_cast<int32_t>(offsetof(CSCSLoading_tA5936CA8227F956F2FBA41BEDC75DE00A57BAB2A_StaticFields, ___state_5)); }
	inline int32_t get_state_5() const { return ___state_5; }
	inline int32_t* get_address_of_state_5() { return &___state_5; }
	inline void set_state_5(int32_t value)
	{
		___state_5 = value;
	}
};


// CSCard
struct CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// CSCardList2D CSCard::cardDatas
	CSCardList2D_tB265A74FEF9F4D8A544A88653A96C54F1F5B66F5 * ___cardDatas_4;
	// System.Action`1<CSCard> CSCard::selectedEvent
	Action_1_t920BC933029F70EEB3BF5271315DE37429A513B5 * ___selectedEvent_5;
	// UnityEngine.Sprite CSCard::_backSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ____backSprite_6;
	// UnityEngine.UI.Image CSCard::_image
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ____image_7;
	// UnityEngine.UI.Button CSCard::_botton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ____botton_8;
	// CSCardData CSCard::_data
	CSCardData_tD1BD9207FB0542F11285FAA8DAC8A02F88C6C9B7 * ____data_9;
	// System.Boolean CSCard::_flip
	bool ____flip_10;
	// CSCardValue CSCard::_cardValue
	CSCardValue_tEDFD24331AA54328DA51DA03E0123741F8729D38  ____cardValue_11;

public:
	inline static int32_t get_offset_of_cardDatas_4() { return static_cast<int32_t>(offsetof(CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2, ___cardDatas_4)); }
	inline CSCardList2D_tB265A74FEF9F4D8A544A88653A96C54F1F5B66F5 * get_cardDatas_4() const { return ___cardDatas_4; }
	inline CSCardList2D_tB265A74FEF9F4D8A544A88653A96C54F1F5B66F5 ** get_address_of_cardDatas_4() { return &___cardDatas_4; }
	inline void set_cardDatas_4(CSCardList2D_tB265A74FEF9F4D8A544A88653A96C54F1F5B66F5 * value)
	{
		___cardDatas_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cardDatas_4), (void*)value);
	}

	inline static int32_t get_offset_of_selectedEvent_5() { return static_cast<int32_t>(offsetof(CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2, ___selectedEvent_5)); }
	inline Action_1_t920BC933029F70EEB3BF5271315DE37429A513B5 * get_selectedEvent_5() const { return ___selectedEvent_5; }
	inline Action_1_t920BC933029F70EEB3BF5271315DE37429A513B5 ** get_address_of_selectedEvent_5() { return &___selectedEvent_5; }
	inline void set_selectedEvent_5(Action_1_t920BC933029F70EEB3BF5271315DE37429A513B5 * value)
	{
		___selectedEvent_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selectedEvent_5), (void*)value);
	}

	inline static int32_t get_offset_of__backSprite_6() { return static_cast<int32_t>(offsetof(CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2, ____backSprite_6)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get__backSprite_6() const { return ____backSprite_6; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of__backSprite_6() { return &____backSprite_6; }
	inline void set__backSprite_6(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		____backSprite_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____backSprite_6), (void*)value);
	}

	inline static int32_t get_offset_of__image_7() { return static_cast<int32_t>(offsetof(CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2, ____image_7)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get__image_7() const { return ____image_7; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of__image_7() { return &____image_7; }
	inline void set__image_7(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		____image_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____image_7), (void*)value);
	}

	inline static int32_t get_offset_of__botton_8() { return static_cast<int32_t>(offsetof(CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2, ____botton_8)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get__botton_8() const { return ____botton_8; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of__botton_8() { return &____botton_8; }
	inline void set__botton_8(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		____botton_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____botton_8), (void*)value);
	}

	inline static int32_t get_offset_of__data_9() { return static_cast<int32_t>(offsetof(CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2, ____data_9)); }
	inline CSCardData_tD1BD9207FB0542F11285FAA8DAC8A02F88C6C9B7 * get__data_9() const { return ____data_9; }
	inline CSCardData_tD1BD9207FB0542F11285FAA8DAC8A02F88C6C9B7 ** get_address_of__data_9() { return &____data_9; }
	inline void set__data_9(CSCardData_tD1BD9207FB0542F11285FAA8DAC8A02F88C6C9B7 * value)
	{
		____data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_9), (void*)value);
	}

	inline static int32_t get_offset_of__flip_10() { return static_cast<int32_t>(offsetof(CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2, ____flip_10)); }
	inline bool get__flip_10() const { return ____flip_10; }
	inline bool* get_address_of__flip_10() { return &____flip_10; }
	inline void set__flip_10(bool value)
	{
		____flip_10 = value;
	}

	inline static int32_t get_offset_of__cardValue_11() { return static_cast<int32_t>(offsetof(CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2, ____cardValue_11)); }
	inline CSCardValue_tEDFD24331AA54328DA51DA03E0123741F8729D38  get__cardValue_11() const { return ____cardValue_11; }
	inline CSCardValue_tEDFD24331AA54328DA51DA03E0123741F8729D38 * get_address_of__cardValue_11() { return &____cardValue_11; }
	inline void set__cardValue_11(CSCardValue_tEDFD24331AA54328DA51DA03E0123741F8729D38  value)
	{
		____cardValue_11 = value;
	}
};


// CSExperiencePanel
struct CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.RectTransform CSExperiencePanel::progress
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___progress_4;
	// UnityEngine.RectTransform CSExperiencePanel::bar
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___bar_5;
	// UnityEngine.GameObject CSExperiencePanel::partilcePrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___partilcePrefab_6;
	// UnityEngine.Transform CSExperiencePanel::icon
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___icon_7;
	// System.Single CSExperiencePanel::power
	float ___power_8;
	// System.Single CSExperiencePanel::increasePercent
	float ___increasePercent_9;
	// UnityEngine.UI.Text CSExperiencePanel::levelText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___levelText_10;
	// UnityEngine.UI.Text CSExperiencePanel::percentText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___percentText_11;
	// CSLevelUpAlert CSExperiencePanel::alert
	CSLevelUpAlert_tEF4E42E838D5DB762ABB678F2A29B38E6FD491CB * ___alert_12;
	// UnityEngine.Vector2 CSExperiencePanel::_size
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____size_13;
	// System.Single CSExperiencePanel::_max
	float ____max_14;
	// System.Single CSExperiencePanel::_xp
	float ____xp_15;
	// System.Int32 CSExperiencePanel::_level
	int32_t ____level_16;

public:
	inline static int32_t get_offset_of_progress_4() { return static_cast<int32_t>(offsetof(CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F, ___progress_4)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_progress_4() const { return ___progress_4; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_progress_4() { return &___progress_4; }
	inline void set_progress_4(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___progress_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___progress_4), (void*)value);
	}

	inline static int32_t get_offset_of_bar_5() { return static_cast<int32_t>(offsetof(CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F, ___bar_5)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_bar_5() const { return ___bar_5; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_bar_5() { return &___bar_5; }
	inline void set_bar_5(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___bar_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bar_5), (void*)value);
	}

	inline static int32_t get_offset_of_partilcePrefab_6() { return static_cast<int32_t>(offsetof(CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F, ___partilcePrefab_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_partilcePrefab_6() const { return ___partilcePrefab_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_partilcePrefab_6() { return &___partilcePrefab_6; }
	inline void set_partilcePrefab_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___partilcePrefab_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___partilcePrefab_6), (void*)value);
	}

	inline static int32_t get_offset_of_icon_7() { return static_cast<int32_t>(offsetof(CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F, ___icon_7)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_icon_7() const { return ___icon_7; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_icon_7() { return &___icon_7; }
	inline void set_icon_7(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___icon_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___icon_7), (void*)value);
	}

	inline static int32_t get_offset_of_power_8() { return static_cast<int32_t>(offsetof(CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F, ___power_8)); }
	inline float get_power_8() const { return ___power_8; }
	inline float* get_address_of_power_8() { return &___power_8; }
	inline void set_power_8(float value)
	{
		___power_8 = value;
	}

	inline static int32_t get_offset_of_increasePercent_9() { return static_cast<int32_t>(offsetof(CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F, ___increasePercent_9)); }
	inline float get_increasePercent_9() const { return ___increasePercent_9; }
	inline float* get_address_of_increasePercent_9() { return &___increasePercent_9; }
	inline void set_increasePercent_9(float value)
	{
		___increasePercent_9 = value;
	}

	inline static int32_t get_offset_of_levelText_10() { return static_cast<int32_t>(offsetof(CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F, ___levelText_10)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_levelText_10() const { return ___levelText_10; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_levelText_10() { return &___levelText_10; }
	inline void set_levelText_10(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___levelText_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___levelText_10), (void*)value);
	}

	inline static int32_t get_offset_of_percentText_11() { return static_cast<int32_t>(offsetof(CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F, ___percentText_11)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_percentText_11() const { return ___percentText_11; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_percentText_11() { return &___percentText_11; }
	inline void set_percentText_11(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___percentText_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___percentText_11), (void*)value);
	}

	inline static int32_t get_offset_of_alert_12() { return static_cast<int32_t>(offsetof(CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F, ___alert_12)); }
	inline CSLevelUpAlert_tEF4E42E838D5DB762ABB678F2A29B38E6FD491CB * get_alert_12() const { return ___alert_12; }
	inline CSLevelUpAlert_tEF4E42E838D5DB762ABB678F2A29B38E6FD491CB ** get_address_of_alert_12() { return &___alert_12; }
	inline void set_alert_12(CSLevelUpAlert_tEF4E42E838D5DB762ABB678F2A29B38E6FD491CB * value)
	{
		___alert_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___alert_12), (void*)value);
	}

	inline static int32_t get_offset_of__size_13() { return static_cast<int32_t>(offsetof(CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F, ____size_13)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__size_13() const { return ____size_13; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__size_13() { return &____size_13; }
	inline void set__size_13(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____size_13 = value;
	}

	inline static int32_t get_offset_of__max_14() { return static_cast<int32_t>(offsetof(CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F, ____max_14)); }
	inline float get__max_14() const { return ____max_14; }
	inline float* get_address_of__max_14() { return &____max_14; }
	inline void set__max_14(float value)
	{
		____max_14 = value;
	}

	inline static int32_t get_offset_of__xp_15() { return static_cast<int32_t>(offsetof(CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F, ____xp_15)); }
	inline float get__xp_15() const { return ____xp_15; }
	inline float* get_address_of__xp_15() { return &____xp_15; }
	inline void set__xp_15(float value)
	{
		____xp_15 = value;
	}

	inline static int32_t get_offset_of__level_16() { return static_cast<int32_t>(offsetof(CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F, ____level_16)); }
	inline int32_t get__level_16() const { return ____level_16; }
	inline int32_t* get_address_of__level_16() { return &____level_16; }
	inline void set__level_16(int32_t value)
	{
		____level_16 = value;
	}
};


// CSFPSDisplay
struct CSFPSDisplay_t6484CE94D5BAEFEA86EC6682EAC68AD825679FDB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single CSFPSDisplay::deltaTime
	float ___deltaTime_4;

public:
	inline static int32_t get_offset_of_deltaTime_4() { return static_cast<int32_t>(offsetof(CSFPSDisplay_t6484CE94D5BAEFEA86EC6682EAC68AD825679FDB, ___deltaTime_4)); }
	inline float get_deltaTime_4() const { return ___deltaTime_4; }
	inline float* get_address_of_deltaTime_4() { return &___deltaTime_4; }
	inline void set_deltaTime_4(float value)
	{
		___deltaTime_4 = value;
	}
};


// CSFindMatchManager
struct CSFindMatchManager_tB268492BDB31FEA9BB0A2B1C1883985E21485B90  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

struct CSFindMatchManager_tB268492BDB31FEA9BB0A2B1C1883985E21485B90_StaticFields
{
public:
	// CSFindMatchManager CSFindMatchManager::instance
	CSFindMatchManager_tB268492BDB31FEA9BB0A2B1C1883985E21485B90 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(CSFindMatchManager_tB268492BDB31FEA9BB0A2B1C1883985E21485B90_StaticFields, ___instance_4)); }
	inline CSFindMatchManager_tB268492BDB31FEA9BB0A2B1C1883985E21485B90 * get_instance_4() const { return ___instance_4; }
	inline CSFindMatchManager_tB268492BDB31FEA9BB0A2B1C1883985E21485B90 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(CSFindMatchManager_tB268492BDB31FEA9BB0A2B1C1883985E21485B90 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}
};


// CSFreeGamePanel
struct CSFreeGamePanel_tC3C7EE40E9C939B3B63EFCE22621ED5792C3D7E6  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject CSFreeGamePanel::freeSpinGameObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___freeSpinGameObject_4;
	// System.Boolean CSFreeGamePanel::_freeSpinEnable
	bool ____freeSpinEnable_5;
	// TMPro.TextMeshProUGUI CSFreeGamePanel::freeSpinsLabel
	TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * ___freeSpinsLabel_6;
	// System.Int32 CSFreeGamePanel::_freeSpins
	int32_t ____freeSpins_7;
	// UnityEngine.UI.Text CSFreeGamePanel::winLabel
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___winLabel_8;
	// System.Single CSFreeGamePanel::_win
	float ____win_9;

public:
	inline static int32_t get_offset_of_freeSpinGameObject_4() { return static_cast<int32_t>(offsetof(CSFreeGamePanel_tC3C7EE40E9C939B3B63EFCE22621ED5792C3D7E6, ___freeSpinGameObject_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_freeSpinGameObject_4() const { return ___freeSpinGameObject_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_freeSpinGameObject_4() { return &___freeSpinGameObject_4; }
	inline void set_freeSpinGameObject_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___freeSpinGameObject_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___freeSpinGameObject_4), (void*)value);
	}

	inline static int32_t get_offset_of__freeSpinEnable_5() { return static_cast<int32_t>(offsetof(CSFreeGamePanel_tC3C7EE40E9C939B3B63EFCE22621ED5792C3D7E6, ____freeSpinEnable_5)); }
	inline bool get__freeSpinEnable_5() const { return ____freeSpinEnable_5; }
	inline bool* get_address_of__freeSpinEnable_5() { return &____freeSpinEnable_5; }
	inline void set__freeSpinEnable_5(bool value)
	{
		____freeSpinEnable_5 = value;
	}

	inline static int32_t get_offset_of_freeSpinsLabel_6() { return static_cast<int32_t>(offsetof(CSFreeGamePanel_tC3C7EE40E9C939B3B63EFCE22621ED5792C3D7E6, ___freeSpinsLabel_6)); }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * get_freeSpinsLabel_6() const { return ___freeSpinsLabel_6; }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 ** get_address_of_freeSpinsLabel_6() { return &___freeSpinsLabel_6; }
	inline void set_freeSpinsLabel_6(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * value)
	{
		___freeSpinsLabel_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___freeSpinsLabel_6), (void*)value);
	}

	inline static int32_t get_offset_of__freeSpins_7() { return static_cast<int32_t>(offsetof(CSFreeGamePanel_tC3C7EE40E9C939B3B63EFCE22621ED5792C3D7E6, ____freeSpins_7)); }
	inline int32_t get__freeSpins_7() const { return ____freeSpins_7; }
	inline int32_t* get_address_of__freeSpins_7() { return &____freeSpins_7; }
	inline void set__freeSpins_7(int32_t value)
	{
		____freeSpins_7 = value;
	}

	inline static int32_t get_offset_of_winLabel_8() { return static_cast<int32_t>(offsetof(CSFreeGamePanel_tC3C7EE40E9C939B3B63EFCE22621ED5792C3D7E6, ___winLabel_8)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_winLabel_8() const { return ___winLabel_8; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_winLabel_8() { return &___winLabel_8; }
	inline void set_winLabel_8(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___winLabel_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___winLabel_8), (void*)value);
	}

	inline static int32_t get_offset_of__win_9() { return static_cast<int32_t>(offsetof(CSFreeGamePanel_tC3C7EE40E9C939B3B63EFCE22621ED5792C3D7E6, ____win_9)); }
	inline float get__win_9() const { return ____win_9; }
	inline float* get_address_of__win_9() { return &____win_9; }
	inline void set__win_9(float value)
	{
		____win_9 = value;
	}
};


// CSGame
struct CSGame_tDA8890AC3DC5588FBEAD11485E2A5478C5E22E32  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// CSGameManager
struct CSGameManager_t5E0B6D2E1EE4AE937D9E6A35B3EBD72CE0B2BAB1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String CSGameManager::androidAppLink
	String_t* ___androidAppLink_5;
	// System.String CSGameManager::iosAppLink
	String_t* ___iosAppLink_6;
	// System.String CSGameManager::supportEmail
	String_t* ___supportEmail_7;

public:
	inline static int32_t get_offset_of_androidAppLink_5() { return static_cast<int32_t>(offsetof(CSGameManager_t5E0B6D2E1EE4AE937D9E6A35B3EBD72CE0B2BAB1, ___androidAppLink_5)); }
	inline String_t* get_androidAppLink_5() const { return ___androidAppLink_5; }
	inline String_t** get_address_of_androidAppLink_5() { return &___androidAppLink_5; }
	inline void set_androidAppLink_5(String_t* value)
	{
		___androidAppLink_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___androidAppLink_5), (void*)value);
	}

	inline static int32_t get_offset_of_iosAppLink_6() { return static_cast<int32_t>(offsetof(CSGameManager_t5E0B6D2E1EE4AE937D9E6A35B3EBD72CE0B2BAB1, ___iosAppLink_6)); }
	inline String_t* get_iosAppLink_6() const { return ___iosAppLink_6; }
	inline String_t** get_address_of_iosAppLink_6() { return &___iosAppLink_6; }
	inline void set_iosAppLink_6(String_t* value)
	{
		___iosAppLink_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___iosAppLink_6), (void*)value);
	}

	inline static int32_t get_offset_of_supportEmail_7() { return static_cast<int32_t>(offsetof(CSGameManager_t5E0B6D2E1EE4AE937D9E6A35B3EBD72CE0B2BAB1, ___supportEmail_7)); }
	inline String_t* get_supportEmail_7() const { return ___supportEmail_7; }
	inline String_t** get_address_of_supportEmail_7() { return &___supportEmail_7; }
	inline void set_supportEmail_7(String_t* value)
	{
		___supportEmail_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___supportEmail_7), (void*)value);
	}
};

struct CSGameManager_t5E0B6D2E1EE4AE937D9E6A35B3EBD72CE0B2BAB1_StaticFields
{
public:
	// CSGameManager CSGameManager::instance
	CSGameManager_t5E0B6D2E1EE4AE937D9E6A35B3EBD72CE0B2BAB1 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(CSGameManager_t5E0B6D2E1EE4AE937D9E6A35B3EBD72CE0B2BAB1_StaticFields, ___instance_4)); }
	inline CSGameManager_t5E0B6D2E1EE4AE937D9E6A35B3EBD72CE0B2BAB1 * get_instance_4() const { return ___instance_4; }
	inline CSGameManager_t5E0B6D2E1EE4AE937D9E6A35B3EBD72CE0B2BAB1 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(CSGameManager_t5E0B6D2E1EE4AE937D9E6A35B3EBD72CE0B2BAB1 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}
};


// CSGameScaler
struct CSGameScaler_t36A69A7559FB1AE776F70D07631D39C3FD6437FF  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// CSGameSettings
struct CSGameSettings_t150C9D4055C2B31EC9689A935814614FF0365AF2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.UInt32 CSGameSettings::lastWin
	uint32_t ___lastWin_5;
	// CSGameData CSGameSettings::data
	CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04 * ___data_6;

public:
	inline static int32_t get_offset_of_lastWin_5() { return static_cast<int32_t>(offsetof(CSGameSettings_t150C9D4055C2B31EC9689A935814614FF0365AF2, ___lastWin_5)); }
	inline uint32_t get_lastWin_5() const { return ___lastWin_5; }
	inline uint32_t* get_address_of_lastWin_5() { return &___lastWin_5; }
	inline void set_lastWin_5(uint32_t value)
	{
		___lastWin_5 = value;
	}

	inline static int32_t get_offset_of_data_6() { return static_cast<int32_t>(offsetof(CSGameSettings_t150C9D4055C2B31EC9689A935814614FF0365AF2, ___data_6)); }
	inline CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04 * get_data_6() const { return ___data_6; }
	inline CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04 ** get_address_of_data_6() { return &___data_6; }
	inline void set_data_6(CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04 * value)
	{
		___data_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_6), (void*)value);
	}
};

struct CSGameSettings_t150C9D4055C2B31EC9689A935814614FF0365AF2_StaticFields
{
public:
	// CSGameSettings CSGameSettings::instance
	CSGameSettings_t150C9D4055C2B31EC9689A935814614FF0365AF2 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(CSGameSettings_t150C9D4055C2B31EC9689A935814614FF0365AF2_StaticFields, ___instance_4)); }
	inline CSGameSettings_t150C9D4055C2B31EC9689A935814614FF0365AF2 * get_instance_4() const { return ___instance_4; }
	inline CSGameSettings_t150C9D4055C2B31EC9689A935814614FF0365AF2 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(CSGameSettings_t150C9D4055C2B31EC9689A935814614FF0365AF2 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}
};


// CSGameStore
struct CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TMPro.TextMeshProUGUI CSGameStore::coinsLabel
	TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * ___coinsLabel_4;
	// CSBankCoinPanel CSGameStore::coinPanel
	CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 * ___coinPanel_5;
	// System.Single CSGameStore::duration
	float ___duration_6;
	// UnityEngine.GameObject CSGameStore::board
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___board_7;
	// UnityEngine.UI.Image CSGameStore::_background
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ____background_8;
	// UnityEngine.CanvasGroup CSGameStore::_canvas
	CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * ____canvas_9;
	// System.Int32 CSGameStore::_scaleId
	int32_t ____scaleId_10;
	// System.Int32 CSGameStore::_alphaId
	int32_t ____alphaId_11;
	// System.Int32 CSGameStore::_alphaBoardId
	int32_t ____alphaBoardId_12;
	// System.Boolean CSGameStore::_active
	bool ____active_13;
	// System.Single CSGameStore::_coins
	float ____coins_14;

public:
	inline static int32_t get_offset_of_coinsLabel_4() { return static_cast<int32_t>(offsetof(CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D, ___coinsLabel_4)); }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * get_coinsLabel_4() const { return ___coinsLabel_4; }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 ** get_address_of_coinsLabel_4() { return &___coinsLabel_4; }
	inline void set_coinsLabel_4(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * value)
	{
		___coinsLabel_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___coinsLabel_4), (void*)value);
	}

	inline static int32_t get_offset_of_coinPanel_5() { return static_cast<int32_t>(offsetof(CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D, ___coinPanel_5)); }
	inline CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 * get_coinPanel_5() const { return ___coinPanel_5; }
	inline CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 ** get_address_of_coinPanel_5() { return &___coinPanel_5; }
	inline void set_coinPanel_5(CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 * value)
	{
		___coinPanel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___coinPanel_5), (void*)value);
	}

	inline static int32_t get_offset_of_duration_6() { return static_cast<int32_t>(offsetof(CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D, ___duration_6)); }
	inline float get_duration_6() const { return ___duration_6; }
	inline float* get_address_of_duration_6() { return &___duration_6; }
	inline void set_duration_6(float value)
	{
		___duration_6 = value;
	}

	inline static int32_t get_offset_of_board_7() { return static_cast<int32_t>(offsetof(CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D, ___board_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_board_7() const { return ___board_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_board_7() { return &___board_7; }
	inline void set_board_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___board_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___board_7), (void*)value);
	}

	inline static int32_t get_offset_of__background_8() { return static_cast<int32_t>(offsetof(CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D, ____background_8)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get__background_8() const { return ____background_8; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of__background_8() { return &____background_8; }
	inline void set__background_8(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		____background_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____background_8), (void*)value);
	}

	inline static int32_t get_offset_of__canvas_9() { return static_cast<int32_t>(offsetof(CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D, ____canvas_9)); }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * get__canvas_9() const { return ____canvas_9; }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F ** get_address_of__canvas_9() { return &____canvas_9; }
	inline void set__canvas_9(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * value)
	{
		____canvas_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____canvas_9), (void*)value);
	}

	inline static int32_t get_offset_of__scaleId_10() { return static_cast<int32_t>(offsetof(CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D, ____scaleId_10)); }
	inline int32_t get__scaleId_10() const { return ____scaleId_10; }
	inline int32_t* get_address_of__scaleId_10() { return &____scaleId_10; }
	inline void set__scaleId_10(int32_t value)
	{
		____scaleId_10 = value;
	}

	inline static int32_t get_offset_of__alphaId_11() { return static_cast<int32_t>(offsetof(CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D, ____alphaId_11)); }
	inline int32_t get__alphaId_11() const { return ____alphaId_11; }
	inline int32_t* get_address_of__alphaId_11() { return &____alphaId_11; }
	inline void set__alphaId_11(int32_t value)
	{
		____alphaId_11 = value;
	}

	inline static int32_t get_offset_of__alphaBoardId_12() { return static_cast<int32_t>(offsetof(CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D, ____alphaBoardId_12)); }
	inline int32_t get__alphaBoardId_12() const { return ____alphaBoardId_12; }
	inline int32_t* get_address_of__alphaBoardId_12() { return &____alphaBoardId_12; }
	inline void set__alphaBoardId_12(int32_t value)
	{
		____alphaBoardId_12 = value;
	}

	inline static int32_t get_offset_of__active_13() { return static_cast<int32_t>(offsetof(CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D, ____active_13)); }
	inline bool get__active_13() const { return ____active_13; }
	inline bool* get_address_of__active_13() { return &____active_13; }
	inline void set__active_13(bool value)
	{
		____active_13 = value;
	}

	inline static int32_t get_offset_of__coins_14() { return static_cast<int32_t>(offsetof(CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D, ____coins_14)); }
	inline float get__coins_14() const { return ____coins_14; }
	inline float* get_address_of__coins_14() { return &____coins_14; }
	inline void set__coins_14(float value)
	{
		____coins_14 = value;
	}
};


// CSGlowAnimation
struct CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single CSGlowAnimation::location
	float ___location_4;
	// System.Single CSGlowAnimation::width
	float ___width_5;
	// System.Single CSGlowAnimation::duration
	float ___duration_6;
	// System.Boolean CSGlowAnimation::animateOnWake
	bool ___animateOnWake_7;
	// System.Int32 CSGlowAnimation::repeat
	int32_t ___repeat_8;
	// System.Boolean CSGlowAnimation::ignoreTimeScale
	bool ___ignoreTimeScale_9;
	// UnityEngine.Material CSGlowAnimation::_material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ____material_10;
	// System.Int32 CSGlowAnimation::_actionId
	int32_t ____actionId_11;
	// System.Boolean CSGlowAnimation::_enable
	bool ____enable_12;

public:
	inline static int32_t get_offset_of_location_4() { return static_cast<int32_t>(offsetof(CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186, ___location_4)); }
	inline float get_location_4() const { return ___location_4; }
	inline float* get_address_of_location_4() { return &___location_4; }
	inline void set_location_4(float value)
	{
		___location_4 = value;
	}

	inline static int32_t get_offset_of_width_5() { return static_cast<int32_t>(offsetof(CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186, ___width_5)); }
	inline float get_width_5() const { return ___width_5; }
	inline float* get_address_of_width_5() { return &___width_5; }
	inline void set_width_5(float value)
	{
		___width_5 = value;
	}

	inline static int32_t get_offset_of_duration_6() { return static_cast<int32_t>(offsetof(CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186, ___duration_6)); }
	inline float get_duration_6() const { return ___duration_6; }
	inline float* get_address_of_duration_6() { return &___duration_6; }
	inline void set_duration_6(float value)
	{
		___duration_6 = value;
	}

	inline static int32_t get_offset_of_animateOnWake_7() { return static_cast<int32_t>(offsetof(CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186, ___animateOnWake_7)); }
	inline bool get_animateOnWake_7() const { return ___animateOnWake_7; }
	inline bool* get_address_of_animateOnWake_7() { return &___animateOnWake_7; }
	inline void set_animateOnWake_7(bool value)
	{
		___animateOnWake_7 = value;
	}

	inline static int32_t get_offset_of_repeat_8() { return static_cast<int32_t>(offsetof(CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186, ___repeat_8)); }
	inline int32_t get_repeat_8() const { return ___repeat_8; }
	inline int32_t* get_address_of_repeat_8() { return &___repeat_8; }
	inline void set_repeat_8(int32_t value)
	{
		___repeat_8 = value;
	}

	inline static int32_t get_offset_of_ignoreTimeScale_9() { return static_cast<int32_t>(offsetof(CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186, ___ignoreTimeScale_9)); }
	inline bool get_ignoreTimeScale_9() const { return ___ignoreTimeScale_9; }
	inline bool* get_address_of_ignoreTimeScale_9() { return &___ignoreTimeScale_9; }
	inline void set_ignoreTimeScale_9(bool value)
	{
		___ignoreTimeScale_9 = value;
	}

	inline static int32_t get_offset_of__material_10() { return static_cast<int32_t>(offsetof(CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186, ____material_10)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get__material_10() const { return ____material_10; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of__material_10() { return &____material_10; }
	inline void set__material_10(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		____material_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____material_10), (void*)value);
	}

	inline static int32_t get_offset_of__actionId_11() { return static_cast<int32_t>(offsetof(CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186, ____actionId_11)); }
	inline int32_t get__actionId_11() const { return ____actionId_11; }
	inline int32_t* get_address_of__actionId_11() { return &____actionId_11; }
	inline void set__actionId_11(int32_t value)
	{
		____actionId_11 = value;
	}

	inline static int32_t get_offset_of__enable_12() { return static_cast<int32_t>(offsetof(CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186, ____enable_12)); }
	inline bool get__enable_12() const { return ____enable_12; }
	inline bool* get_address_of__enable_12() { return &____enable_12; }
	inline void set__enable_12(bool value)
	{
		____enable_12 = value;
	}
};


// CSIAPManager
struct CSIAPManager_t0F7D227EFEEBC9951CD4D135B5F6299C67906899  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Action`2<UnityEngine.Purchasing.Product,CSIAPProduct> CSIAPManager::handleSuccessPurchase
	Action_2_t72FC6CF8D9D151A7D4DF016104A186B035F2FC31 * ___handleSuccessPurchase_4;
	// System.Action`2<UnityEngine.Purchasing.Product,CSIAPProduct> CSIAPManager::handleFaliedPurchase
	Action_2_t72FC6CF8D9D151A7D4DF016104A186B035F2FC31 * ___handleFaliedPurchase_5;
	// System.Collections.Generic.List`1<CSIAPProduct> CSIAPManager::products
	List_1_t164CAD3B766922142F98E0633949D448FA07B5BE * ___products_6;

public:
	inline static int32_t get_offset_of_handleSuccessPurchase_4() { return static_cast<int32_t>(offsetof(CSIAPManager_t0F7D227EFEEBC9951CD4D135B5F6299C67906899, ___handleSuccessPurchase_4)); }
	inline Action_2_t72FC6CF8D9D151A7D4DF016104A186B035F2FC31 * get_handleSuccessPurchase_4() const { return ___handleSuccessPurchase_4; }
	inline Action_2_t72FC6CF8D9D151A7D4DF016104A186B035F2FC31 ** get_address_of_handleSuccessPurchase_4() { return &___handleSuccessPurchase_4; }
	inline void set_handleSuccessPurchase_4(Action_2_t72FC6CF8D9D151A7D4DF016104A186B035F2FC31 * value)
	{
		___handleSuccessPurchase_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___handleSuccessPurchase_4), (void*)value);
	}

	inline static int32_t get_offset_of_handleFaliedPurchase_5() { return static_cast<int32_t>(offsetof(CSIAPManager_t0F7D227EFEEBC9951CD4D135B5F6299C67906899, ___handleFaliedPurchase_5)); }
	inline Action_2_t72FC6CF8D9D151A7D4DF016104A186B035F2FC31 * get_handleFaliedPurchase_5() const { return ___handleFaliedPurchase_5; }
	inline Action_2_t72FC6CF8D9D151A7D4DF016104A186B035F2FC31 ** get_address_of_handleFaliedPurchase_5() { return &___handleFaliedPurchase_5; }
	inline void set_handleFaliedPurchase_5(Action_2_t72FC6CF8D9D151A7D4DF016104A186B035F2FC31 * value)
	{
		___handleFaliedPurchase_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___handleFaliedPurchase_5), (void*)value);
	}

	inline static int32_t get_offset_of_products_6() { return static_cast<int32_t>(offsetof(CSIAPManager_t0F7D227EFEEBC9951CD4D135B5F6299C67906899, ___products_6)); }
	inline List_1_t164CAD3B766922142F98E0633949D448FA07B5BE * get_products_6() const { return ___products_6; }
	inline List_1_t164CAD3B766922142F98E0633949D448FA07B5BE ** get_address_of_products_6() { return &___products_6; }
	inline void set_products_6(List_1_t164CAD3B766922142F98E0633949D448FA07B5BE * value)
	{
		___products_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___products_6), (void*)value);
	}
};

struct CSIAPManager_t0F7D227EFEEBC9951CD4D135B5F6299C67906899_StaticFields
{
public:
	// UnityEngine.Purchasing.IStoreController CSIAPManager::m_StoreController
	RuntimeObject* ___m_StoreController_7;
	// UnityEngine.Purchasing.IExtensionProvider CSIAPManager::m_StoreExtensionProvider
	RuntimeObject* ___m_StoreExtensionProvider_8;
	// CSIAPManager CSIAPManager::instance
	CSIAPManager_t0F7D227EFEEBC9951CD4D135B5F6299C67906899 * ___instance_9;

public:
	inline static int32_t get_offset_of_m_StoreController_7() { return static_cast<int32_t>(offsetof(CSIAPManager_t0F7D227EFEEBC9951CD4D135B5F6299C67906899_StaticFields, ___m_StoreController_7)); }
	inline RuntimeObject* get_m_StoreController_7() const { return ___m_StoreController_7; }
	inline RuntimeObject** get_address_of_m_StoreController_7() { return &___m_StoreController_7; }
	inline void set_m_StoreController_7(RuntimeObject* value)
	{
		___m_StoreController_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StoreController_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_StoreExtensionProvider_8() { return static_cast<int32_t>(offsetof(CSIAPManager_t0F7D227EFEEBC9951CD4D135B5F6299C67906899_StaticFields, ___m_StoreExtensionProvider_8)); }
	inline RuntimeObject* get_m_StoreExtensionProvider_8() const { return ___m_StoreExtensionProvider_8; }
	inline RuntimeObject** get_address_of_m_StoreExtensionProvider_8() { return &___m_StoreExtensionProvider_8; }
	inline void set_m_StoreExtensionProvider_8(RuntimeObject* value)
	{
		___m_StoreExtensionProvider_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StoreExtensionProvider_8), (void*)value);
	}

	inline static int32_t get_offset_of_instance_9() { return static_cast<int32_t>(offsetof(CSIAPManager_t0F7D227EFEEBC9951CD4D135B5F6299C67906899_StaticFields, ___instance_9)); }
	inline CSIAPManager_t0F7D227EFEEBC9951CD4D135B5F6299C67906899 * get_instance_9() const { return ___instance_9; }
	inline CSIAPManager_t0F7D227EFEEBC9951CD4D135B5F6299C67906899 ** get_address_of_instance_9() { return &___instance_9; }
	inline void set_instance_9(CSIAPManager_t0F7D227EFEEBC9951CD4D135B5F6299C67906899 * value)
	{
		___instance_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_9), (void*)value);
	}
};


// CSInfo
struct CSInfo_tD22B0C9AD79CB449BCBC64512307F5EFAE046AA1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single CSInfo::duration
	float ___duration_4;
	// UnityEngine.GameObject CSInfo::board
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___board_5;
	// UnityEngine.UI.Image CSInfo::_background
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ____background_6;
	// UnityEngine.CanvasGroup CSInfo::_canvas
	CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * ____canvas_7;
	// System.Int32 CSInfo::_scaleId
	int32_t ____scaleId_8;
	// System.Int32 CSInfo::_alphaId
	int32_t ____alphaId_9;
	// System.Int32 CSInfo::_alphaBoardId
	int32_t ____alphaBoardId_10;
	// System.Boolean CSInfo::_active
	bool ____active_11;

public:
	inline static int32_t get_offset_of_duration_4() { return static_cast<int32_t>(offsetof(CSInfo_tD22B0C9AD79CB449BCBC64512307F5EFAE046AA1, ___duration_4)); }
	inline float get_duration_4() const { return ___duration_4; }
	inline float* get_address_of_duration_4() { return &___duration_4; }
	inline void set_duration_4(float value)
	{
		___duration_4 = value;
	}

	inline static int32_t get_offset_of_board_5() { return static_cast<int32_t>(offsetof(CSInfo_tD22B0C9AD79CB449BCBC64512307F5EFAE046AA1, ___board_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_board_5() const { return ___board_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_board_5() { return &___board_5; }
	inline void set_board_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___board_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___board_5), (void*)value);
	}

	inline static int32_t get_offset_of__background_6() { return static_cast<int32_t>(offsetof(CSInfo_tD22B0C9AD79CB449BCBC64512307F5EFAE046AA1, ____background_6)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get__background_6() const { return ____background_6; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of__background_6() { return &____background_6; }
	inline void set__background_6(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		____background_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____background_6), (void*)value);
	}

	inline static int32_t get_offset_of__canvas_7() { return static_cast<int32_t>(offsetof(CSInfo_tD22B0C9AD79CB449BCBC64512307F5EFAE046AA1, ____canvas_7)); }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * get__canvas_7() const { return ____canvas_7; }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F ** get_address_of__canvas_7() { return &____canvas_7; }
	inline void set__canvas_7(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * value)
	{
		____canvas_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____canvas_7), (void*)value);
	}

	inline static int32_t get_offset_of__scaleId_8() { return static_cast<int32_t>(offsetof(CSInfo_tD22B0C9AD79CB449BCBC64512307F5EFAE046AA1, ____scaleId_8)); }
	inline int32_t get__scaleId_8() const { return ____scaleId_8; }
	inline int32_t* get_address_of__scaleId_8() { return &____scaleId_8; }
	inline void set__scaleId_8(int32_t value)
	{
		____scaleId_8 = value;
	}

	inline static int32_t get_offset_of__alphaId_9() { return static_cast<int32_t>(offsetof(CSInfo_tD22B0C9AD79CB449BCBC64512307F5EFAE046AA1, ____alphaId_9)); }
	inline int32_t get__alphaId_9() const { return ____alphaId_9; }
	inline int32_t* get_address_of__alphaId_9() { return &____alphaId_9; }
	inline void set__alphaId_9(int32_t value)
	{
		____alphaId_9 = value;
	}

	inline static int32_t get_offset_of__alphaBoardId_10() { return static_cast<int32_t>(offsetof(CSInfo_tD22B0C9AD79CB449BCBC64512307F5EFAE046AA1, ____alphaBoardId_10)); }
	inline int32_t get__alphaBoardId_10() const { return ____alphaBoardId_10; }
	inline int32_t* get_address_of__alphaBoardId_10() { return &____alphaBoardId_10; }
	inline void set__alphaBoardId_10(int32_t value)
	{
		____alphaBoardId_10 = value;
	}

	inline static int32_t get_offset_of__active_11() { return static_cast<int32_t>(offsetof(CSInfo_tD22B0C9AD79CB449BCBC64512307F5EFAE046AA1, ____active_11)); }
	inline bool get__active_11() const { return ____active_11; }
	inline bool* get_address_of__active_11() { return &____active_11; }
	inline void set__active_11(bool value)
	{
		____active_11 = value;
	}
};


// CSLFBGAlertBoard
struct CSLFBGAlertBoard_t6EF319BFC3C51BF1443743AFC7030B7566305D45  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject CSLFBGAlertBoard::multiplierGameObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___multiplierGameObject_4;
	// TMPro.TextMeshProUGUI CSLFBGAlertBoard::multiplierLabel
	TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * ___multiplierLabel_5;
	// System.Int32 CSLFBGAlertBoard::_multiplier
	int32_t ____multiplier_6;
	// UnityEngine.GameObject CSLFBGAlertBoard::freeSpinsGameObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___freeSpinsGameObject_7;
	// TMPro.TextMeshProUGUI CSLFBGAlertBoard::freeSpinsLabel
	TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * ___freeSpinsLabel_8;
	// System.Int32 CSLFBGAlertBoard::_freeSpins
	int32_t ____freeSpins_9;

public:
	inline static int32_t get_offset_of_multiplierGameObject_4() { return static_cast<int32_t>(offsetof(CSLFBGAlertBoard_t6EF319BFC3C51BF1443743AFC7030B7566305D45, ___multiplierGameObject_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_multiplierGameObject_4() const { return ___multiplierGameObject_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_multiplierGameObject_4() { return &___multiplierGameObject_4; }
	inline void set_multiplierGameObject_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___multiplierGameObject_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___multiplierGameObject_4), (void*)value);
	}

	inline static int32_t get_offset_of_multiplierLabel_5() { return static_cast<int32_t>(offsetof(CSLFBGAlertBoard_t6EF319BFC3C51BF1443743AFC7030B7566305D45, ___multiplierLabel_5)); }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * get_multiplierLabel_5() const { return ___multiplierLabel_5; }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 ** get_address_of_multiplierLabel_5() { return &___multiplierLabel_5; }
	inline void set_multiplierLabel_5(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * value)
	{
		___multiplierLabel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___multiplierLabel_5), (void*)value);
	}

	inline static int32_t get_offset_of__multiplier_6() { return static_cast<int32_t>(offsetof(CSLFBGAlertBoard_t6EF319BFC3C51BF1443743AFC7030B7566305D45, ____multiplier_6)); }
	inline int32_t get__multiplier_6() const { return ____multiplier_6; }
	inline int32_t* get_address_of__multiplier_6() { return &____multiplier_6; }
	inline void set__multiplier_6(int32_t value)
	{
		____multiplier_6 = value;
	}

	inline static int32_t get_offset_of_freeSpinsGameObject_7() { return static_cast<int32_t>(offsetof(CSLFBGAlertBoard_t6EF319BFC3C51BF1443743AFC7030B7566305D45, ___freeSpinsGameObject_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_freeSpinsGameObject_7() const { return ___freeSpinsGameObject_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_freeSpinsGameObject_7() { return &___freeSpinsGameObject_7; }
	inline void set_freeSpinsGameObject_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___freeSpinsGameObject_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___freeSpinsGameObject_7), (void*)value);
	}

	inline static int32_t get_offset_of_freeSpinsLabel_8() { return static_cast<int32_t>(offsetof(CSLFBGAlertBoard_t6EF319BFC3C51BF1443743AFC7030B7566305D45, ___freeSpinsLabel_8)); }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * get_freeSpinsLabel_8() const { return ___freeSpinsLabel_8; }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 ** get_address_of_freeSpinsLabel_8() { return &___freeSpinsLabel_8; }
	inline void set_freeSpinsLabel_8(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * value)
	{
		___freeSpinsLabel_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___freeSpinsLabel_8), (void*)value);
	}

	inline static int32_t get_offset_of__freeSpins_9() { return static_cast<int32_t>(offsetof(CSLFBGAlertBoard_t6EF319BFC3C51BF1443743AFC7030B7566305D45, ____freeSpins_9)); }
	inline int32_t get__freeSpins_9() const { return ____freeSpins_9; }
	inline int32_t* get_address_of__freeSpins_9() { return &____freeSpins_9; }
	inline void set__freeSpins_9(int32_t value)
	{
		____freeSpins_9 = value;
	}
};


// CSLFProgressBar
struct CSLFProgressBar_t2236B83E6C32E322F6C3707BEEAD2FC5EE9F2035  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single CSLFProgressBar::_value
	float ____value_4;
	// UnityEngine.RectTransform CSLFProgressBar::bar
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___bar_5;
	// UnityEngine.UI.Text CSLFProgressBar::text
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___text_6;

public:
	inline static int32_t get_offset_of__value_4() { return static_cast<int32_t>(offsetof(CSLFProgressBar_t2236B83E6C32E322F6C3707BEEAD2FC5EE9F2035, ____value_4)); }
	inline float get__value_4() const { return ____value_4; }
	inline float* get_address_of__value_4() { return &____value_4; }
	inline void set__value_4(float value)
	{
		____value_4 = value;
	}

	inline static int32_t get_offset_of_bar_5() { return static_cast<int32_t>(offsetof(CSLFProgressBar_t2236B83E6C32E322F6C3707BEEAD2FC5EE9F2035, ___bar_5)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_bar_5() const { return ___bar_5; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_bar_5() { return &___bar_5; }
	inline void set_bar_5(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___bar_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bar_5), (void*)value);
	}

	inline static int32_t get_offset_of_text_6() { return static_cast<int32_t>(offsetof(CSLFProgressBar_t2236B83E6C32E322F6C3707BEEAD2FC5EE9F2035, ___text_6)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_text_6() const { return ___text_6; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_text_6() { return &___text_6; }
	inline void set_text_6(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___text_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___text_6), (void*)value);
	}
};


// CSMenuToggle
struct CSMenuToggle_t3572C0C8BADDD21482DA7B799E56C0403AD3516E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// CSSettingsPanelSmall CSMenuToggle::miniSettings
	CSSettingsPanelSmall_t436B210ADD3C25F979D822BD214FBDE286D44CA0 * ___miniSettings_4;
	// UnityEngine.UI.Toggle CSMenuToggle::_toggle
	Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * ____toggle_5;
	// UnityEngine.LayerMask CSMenuToggle::_toggleMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ____toggleMask_6;

public:
	inline static int32_t get_offset_of_miniSettings_4() { return static_cast<int32_t>(offsetof(CSMenuToggle_t3572C0C8BADDD21482DA7B799E56C0403AD3516E, ___miniSettings_4)); }
	inline CSSettingsPanelSmall_t436B210ADD3C25F979D822BD214FBDE286D44CA0 * get_miniSettings_4() const { return ___miniSettings_4; }
	inline CSSettingsPanelSmall_t436B210ADD3C25F979D822BD214FBDE286D44CA0 ** get_address_of_miniSettings_4() { return &___miniSettings_4; }
	inline void set_miniSettings_4(CSSettingsPanelSmall_t436B210ADD3C25F979D822BD214FBDE286D44CA0 * value)
	{
		___miniSettings_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___miniSettings_4), (void*)value);
	}

	inline static int32_t get_offset_of__toggle_5() { return static_cast<int32_t>(offsetof(CSMenuToggle_t3572C0C8BADDD21482DA7B799E56C0403AD3516E, ____toggle_5)); }
	inline Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * get__toggle_5() const { return ____toggle_5; }
	inline Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E ** get_address_of__toggle_5() { return &____toggle_5; }
	inline void set__toggle_5(Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * value)
	{
		____toggle_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____toggle_5), (void*)value);
	}

	inline static int32_t get_offset_of__toggleMask_6() { return static_cast<int32_t>(offsetof(CSMenuToggle_t3572C0C8BADDD21482DA7B799E56C0403AD3516E, ____toggleMask_6)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get__toggleMask_6() const { return ____toggleMask_6; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of__toggleMask_6() { return &____toggleMask_6; }
	inline void set__toggleMask_6(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		____toggleMask_6 = value;
	}
};


// CSReel
struct CSReel_t8454490940D72C04785CF2BF71C41073113E1890  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject CSReel::symbol
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___symbol_4;
	// UnityEngine.Vector2 CSReel::_tileSize
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____tileSize_5;
	// System.Int32 CSReel::_count
	int32_t ____count_6;
	// CSSymbol[] CSReel::_symbols
	CSSymbolU5BU5D_tF220DF3FE5F1E80568DA2518BF6970BE5AB3E4EA* ____symbols_7;
	// UnityEngine.Vector2 CSReel::_size
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____size_8;
	// System.Int32 CSReel::_column
	int32_t ____column_9;
	// CS2DBoolArray CSReel::symbolEnabled
	CS2DBoolArray_t4A5EA41B80365341FAB8F7B222392F23F5083434 * ___symbolEnabled_10;
	// CSReelRandom CSReel::_reelRandom
	CSReelRandom_t16B21B9455802008C805A265B104CD5F2D02423B * ____reelRandom_11;

public:
	inline static int32_t get_offset_of_symbol_4() { return static_cast<int32_t>(offsetof(CSReel_t8454490940D72C04785CF2BF71C41073113E1890, ___symbol_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_symbol_4() const { return ___symbol_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_symbol_4() { return &___symbol_4; }
	inline void set_symbol_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___symbol_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___symbol_4), (void*)value);
	}

	inline static int32_t get_offset_of__tileSize_5() { return static_cast<int32_t>(offsetof(CSReel_t8454490940D72C04785CF2BF71C41073113E1890, ____tileSize_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__tileSize_5() const { return ____tileSize_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__tileSize_5() { return &____tileSize_5; }
	inline void set__tileSize_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____tileSize_5 = value;
	}

	inline static int32_t get_offset_of__count_6() { return static_cast<int32_t>(offsetof(CSReel_t8454490940D72C04785CF2BF71C41073113E1890, ____count_6)); }
	inline int32_t get__count_6() const { return ____count_6; }
	inline int32_t* get_address_of__count_6() { return &____count_6; }
	inline void set__count_6(int32_t value)
	{
		____count_6 = value;
	}

	inline static int32_t get_offset_of__symbols_7() { return static_cast<int32_t>(offsetof(CSReel_t8454490940D72C04785CF2BF71C41073113E1890, ____symbols_7)); }
	inline CSSymbolU5BU5D_tF220DF3FE5F1E80568DA2518BF6970BE5AB3E4EA* get__symbols_7() const { return ____symbols_7; }
	inline CSSymbolU5BU5D_tF220DF3FE5F1E80568DA2518BF6970BE5AB3E4EA** get_address_of__symbols_7() { return &____symbols_7; }
	inline void set__symbols_7(CSSymbolU5BU5D_tF220DF3FE5F1E80568DA2518BF6970BE5AB3E4EA* value)
	{
		____symbols_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____symbols_7), (void*)value);
	}

	inline static int32_t get_offset_of__size_8() { return static_cast<int32_t>(offsetof(CSReel_t8454490940D72C04785CF2BF71C41073113E1890, ____size_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__size_8() const { return ____size_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__size_8() { return &____size_8; }
	inline void set__size_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____size_8 = value;
	}

	inline static int32_t get_offset_of__column_9() { return static_cast<int32_t>(offsetof(CSReel_t8454490940D72C04785CF2BF71C41073113E1890, ____column_9)); }
	inline int32_t get__column_9() const { return ____column_9; }
	inline int32_t* get_address_of__column_9() { return &____column_9; }
	inline void set__column_9(int32_t value)
	{
		____column_9 = value;
	}

	inline static int32_t get_offset_of_symbolEnabled_10() { return static_cast<int32_t>(offsetof(CSReel_t8454490940D72C04785CF2BF71C41073113E1890, ___symbolEnabled_10)); }
	inline CS2DBoolArray_t4A5EA41B80365341FAB8F7B222392F23F5083434 * get_symbolEnabled_10() const { return ___symbolEnabled_10; }
	inline CS2DBoolArray_t4A5EA41B80365341FAB8F7B222392F23F5083434 ** get_address_of_symbolEnabled_10() { return &___symbolEnabled_10; }
	inline void set_symbolEnabled_10(CS2DBoolArray_t4A5EA41B80365341FAB8F7B222392F23F5083434 * value)
	{
		___symbolEnabled_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___symbolEnabled_10), (void*)value);
	}

	inline static int32_t get_offset_of__reelRandom_11() { return static_cast<int32_t>(offsetof(CSReel_t8454490940D72C04785CF2BF71C41073113E1890, ____reelRandom_11)); }
	inline CSReelRandom_t16B21B9455802008C805A265B104CD5F2D02423B * get__reelRandom_11() const { return ____reelRandom_11; }
	inline CSReelRandom_t16B21B9455802008C805A265B104CD5F2D02423B ** get_address_of__reelRandom_11() { return &____reelRandom_11; }
	inline void set__reelRandom_11(CSReelRandom_t16B21B9455802008C805A265B104CD5F2D02423B * value)
	{
		____reelRandom_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____reelRandom_11), (void*)value);
	}
};


// CSReelAutoSpin
struct CSReelAutoSpin_tE3C4581E7195FF30D21E5E5EFDE1BA9344C385E4  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Sprite CSReelAutoSpin::normalSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___normalSprite_4;
	// UnityEngine.UI.SpriteState CSReelAutoSpin::state
	SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  ___state_5;
	// System.Single CSReelAutoSpin::holdDuration
	float ___holdDuration_6;
	// System.Single CSReelAutoSpin::_startTime
	float ____startTime_7;
	// System.Boolean CSReelAutoSpin::_start
	bool ____start_8;
	// CSReels CSReelAutoSpin::_reels
	CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47 * ____reels_9;

public:
	inline static int32_t get_offset_of_normalSprite_4() { return static_cast<int32_t>(offsetof(CSReelAutoSpin_tE3C4581E7195FF30D21E5E5EFDE1BA9344C385E4, ___normalSprite_4)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_normalSprite_4() const { return ___normalSprite_4; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_normalSprite_4() { return &___normalSprite_4; }
	inline void set_normalSprite_4(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___normalSprite_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___normalSprite_4), (void*)value);
	}

	inline static int32_t get_offset_of_state_5() { return static_cast<int32_t>(offsetof(CSReelAutoSpin_tE3C4581E7195FF30D21E5E5EFDE1BA9344C385E4, ___state_5)); }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  get_state_5() const { return ___state_5; }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E * get_address_of_state_5() { return &___state_5; }
	inline void set_state_5(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  value)
	{
		___state_5 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___state_5))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___state_5))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___state_5))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___state_5))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_holdDuration_6() { return static_cast<int32_t>(offsetof(CSReelAutoSpin_tE3C4581E7195FF30D21E5E5EFDE1BA9344C385E4, ___holdDuration_6)); }
	inline float get_holdDuration_6() const { return ___holdDuration_6; }
	inline float* get_address_of_holdDuration_6() { return &___holdDuration_6; }
	inline void set_holdDuration_6(float value)
	{
		___holdDuration_6 = value;
	}

	inline static int32_t get_offset_of__startTime_7() { return static_cast<int32_t>(offsetof(CSReelAutoSpin_tE3C4581E7195FF30D21E5E5EFDE1BA9344C385E4, ____startTime_7)); }
	inline float get__startTime_7() const { return ____startTime_7; }
	inline float* get_address_of__startTime_7() { return &____startTime_7; }
	inline void set__startTime_7(float value)
	{
		____startTime_7 = value;
	}

	inline static int32_t get_offset_of__start_8() { return static_cast<int32_t>(offsetof(CSReelAutoSpin_tE3C4581E7195FF30D21E5E5EFDE1BA9344C385E4, ____start_8)); }
	inline bool get__start_8() const { return ____start_8; }
	inline bool* get_address_of__start_8() { return &____start_8; }
	inline void set__start_8(bool value)
	{
		____start_8 = value;
	}

	inline static int32_t get_offset_of__reels_9() { return static_cast<int32_t>(offsetof(CSReelAutoSpin_tE3C4581E7195FF30D21E5E5EFDE1BA9344C385E4, ____reels_9)); }
	inline CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47 * get__reels_9() const { return ____reels_9; }
	inline CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47 ** get_address_of__reels_9() { return &____reels_9; }
	inline void set__reels_9(CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47 * value)
	{
		____reels_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____reels_9), (void*)value);
	}
};


// CSReels
struct CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Action`1<System.Boolean> CSReels::FreeGameValueChangedEvent
	Action_1_tCE2D770918A65CAD277C08C4E8C05385EA267E83 * ___FreeGameValueChangedEvent_4;
	// UnityEngine.GameObject CSReels::reel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___reel_5;
	// CSLine[] CSReels::lines
	CSLineU5BU5D_t858A6F64F7D40673B867E58C0664C80F3F154FB2* ___lines_6;
	// CSBottomPanel CSReels::basePanel
	CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38 * ___basePanel_7;
	// CSFreeGamePanel CSReels::freeGamePanel
	CSFreeGamePanel_tC3C7EE40E9C939B3B63EFCE22621ED5792C3D7E6 * ___freeGamePanel_8;
	// HorizontalScrollSnap CSReels::scrollView
	HorizontalScrollSnap_tE6B632D4D5CDD31DFC9CF915D070FD422F1597BA * ___scrollView_9;
	// CSTopPanel CSReels::topPanel
	CSTopPanel_t94C2B84ED826C2C503575317319F623658C0C4D8 * ___topPanel_10;
	// CSAlertRewardAnim CSReels::rewardAlert
	CSAlertRewardAnim_t13B8641467B4284296D29B346C9760D59E21E9E0 * ___rewardAlert_11;
	// CSReel[] CSReels::reels
	CSReelU5BU5D_tC3ABB496276FFD59F38137BCBDB1F126B84C1A27* ___reels_12;
	// UnityEngine.Vector2 CSReels::_tileSize
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____tileSize_13;
	// System.Boolean CSReels::_autoSpin
	bool ____autoSpin_14;
	// CSReelAutoSpin CSReels::_stateAutoSpin
	CSReelAutoSpin_tE3C4581E7195FF30D21E5E5EFDE1BA9344C385E4 * ____stateAutoSpin_15;
	// System.Boolean CSReels::_enable
	bool ____enable_16;
	// CSReelsAnimation CSReels::_reelAnimation
	CSReelsAnimation_t04EC01AF43D2016CB036755318F8873C79F9714D * ____reelAnimation_17;
	// System.Boolean CSReels::_freeGame
	bool ____freeGame_18;

public:
	inline static int32_t get_offset_of_FreeGameValueChangedEvent_4() { return static_cast<int32_t>(offsetof(CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47, ___FreeGameValueChangedEvent_4)); }
	inline Action_1_tCE2D770918A65CAD277C08C4E8C05385EA267E83 * get_FreeGameValueChangedEvent_4() const { return ___FreeGameValueChangedEvent_4; }
	inline Action_1_tCE2D770918A65CAD277C08C4E8C05385EA267E83 ** get_address_of_FreeGameValueChangedEvent_4() { return &___FreeGameValueChangedEvent_4; }
	inline void set_FreeGameValueChangedEvent_4(Action_1_tCE2D770918A65CAD277C08C4E8C05385EA267E83 * value)
	{
		___FreeGameValueChangedEvent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FreeGameValueChangedEvent_4), (void*)value);
	}

	inline static int32_t get_offset_of_reel_5() { return static_cast<int32_t>(offsetof(CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47, ___reel_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_reel_5() const { return ___reel_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_reel_5() { return &___reel_5; }
	inline void set_reel_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___reel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reel_5), (void*)value);
	}

	inline static int32_t get_offset_of_lines_6() { return static_cast<int32_t>(offsetof(CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47, ___lines_6)); }
	inline CSLineU5BU5D_t858A6F64F7D40673B867E58C0664C80F3F154FB2* get_lines_6() const { return ___lines_6; }
	inline CSLineU5BU5D_t858A6F64F7D40673B867E58C0664C80F3F154FB2** get_address_of_lines_6() { return &___lines_6; }
	inline void set_lines_6(CSLineU5BU5D_t858A6F64F7D40673B867E58C0664C80F3F154FB2* value)
	{
		___lines_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lines_6), (void*)value);
	}

	inline static int32_t get_offset_of_basePanel_7() { return static_cast<int32_t>(offsetof(CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47, ___basePanel_7)); }
	inline CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38 * get_basePanel_7() const { return ___basePanel_7; }
	inline CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38 ** get_address_of_basePanel_7() { return &___basePanel_7; }
	inline void set_basePanel_7(CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38 * value)
	{
		___basePanel_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___basePanel_7), (void*)value);
	}

	inline static int32_t get_offset_of_freeGamePanel_8() { return static_cast<int32_t>(offsetof(CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47, ___freeGamePanel_8)); }
	inline CSFreeGamePanel_tC3C7EE40E9C939B3B63EFCE22621ED5792C3D7E6 * get_freeGamePanel_8() const { return ___freeGamePanel_8; }
	inline CSFreeGamePanel_tC3C7EE40E9C939B3B63EFCE22621ED5792C3D7E6 ** get_address_of_freeGamePanel_8() { return &___freeGamePanel_8; }
	inline void set_freeGamePanel_8(CSFreeGamePanel_tC3C7EE40E9C939B3B63EFCE22621ED5792C3D7E6 * value)
	{
		___freeGamePanel_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___freeGamePanel_8), (void*)value);
	}

	inline static int32_t get_offset_of_scrollView_9() { return static_cast<int32_t>(offsetof(CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47, ___scrollView_9)); }
	inline HorizontalScrollSnap_tE6B632D4D5CDD31DFC9CF915D070FD422F1597BA * get_scrollView_9() const { return ___scrollView_9; }
	inline HorizontalScrollSnap_tE6B632D4D5CDD31DFC9CF915D070FD422F1597BA ** get_address_of_scrollView_9() { return &___scrollView_9; }
	inline void set_scrollView_9(HorizontalScrollSnap_tE6B632D4D5CDD31DFC9CF915D070FD422F1597BA * value)
	{
		___scrollView_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___scrollView_9), (void*)value);
	}

	inline static int32_t get_offset_of_topPanel_10() { return static_cast<int32_t>(offsetof(CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47, ___topPanel_10)); }
	inline CSTopPanel_t94C2B84ED826C2C503575317319F623658C0C4D8 * get_topPanel_10() const { return ___topPanel_10; }
	inline CSTopPanel_t94C2B84ED826C2C503575317319F623658C0C4D8 ** get_address_of_topPanel_10() { return &___topPanel_10; }
	inline void set_topPanel_10(CSTopPanel_t94C2B84ED826C2C503575317319F623658C0C4D8 * value)
	{
		___topPanel_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___topPanel_10), (void*)value);
	}

	inline static int32_t get_offset_of_rewardAlert_11() { return static_cast<int32_t>(offsetof(CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47, ___rewardAlert_11)); }
	inline CSAlertRewardAnim_t13B8641467B4284296D29B346C9760D59E21E9E0 * get_rewardAlert_11() const { return ___rewardAlert_11; }
	inline CSAlertRewardAnim_t13B8641467B4284296D29B346C9760D59E21E9E0 ** get_address_of_rewardAlert_11() { return &___rewardAlert_11; }
	inline void set_rewardAlert_11(CSAlertRewardAnim_t13B8641467B4284296D29B346C9760D59E21E9E0 * value)
	{
		___rewardAlert_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rewardAlert_11), (void*)value);
	}

	inline static int32_t get_offset_of_reels_12() { return static_cast<int32_t>(offsetof(CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47, ___reels_12)); }
	inline CSReelU5BU5D_tC3ABB496276FFD59F38137BCBDB1F126B84C1A27* get_reels_12() const { return ___reels_12; }
	inline CSReelU5BU5D_tC3ABB496276FFD59F38137BCBDB1F126B84C1A27** get_address_of_reels_12() { return &___reels_12; }
	inline void set_reels_12(CSReelU5BU5D_tC3ABB496276FFD59F38137BCBDB1F126B84C1A27* value)
	{
		___reels_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reels_12), (void*)value);
	}

	inline static int32_t get_offset_of__tileSize_13() { return static_cast<int32_t>(offsetof(CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47, ____tileSize_13)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__tileSize_13() const { return ____tileSize_13; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__tileSize_13() { return &____tileSize_13; }
	inline void set__tileSize_13(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____tileSize_13 = value;
	}

	inline static int32_t get_offset_of__autoSpin_14() { return static_cast<int32_t>(offsetof(CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47, ____autoSpin_14)); }
	inline bool get__autoSpin_14() const { return ____autoSpin_14; }
	inline bool* get_address_of__autoSpin_14() { return &____autoSpin_14; }
	inline void set__autoSpin_14(bool value)
	{
		____autoSpin_14 = value;
	}

	inline static int32_t get_offset_of__stateAutoSpin_15() { return static_cast<int32_t>(offsetof(CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47, ____stateAutoSpin_15)); }
	inline CSReelAutoSpin_tE3C4581E7195FF30D21E5E5EFDE1BA9344C385E4 * get__stateAutoSpin_15() const { return ____stateAutoSpin_15; }
	inline CSReelAutoSpin_tE3C4581E7195FF30D21E5E5EFDE1BA9344C385E4 ** get_address_of__stateAutoSpin_15() { return &____stateAutoSpin_15; }
	inline void set__stateAutoSpin_15(CSReelAutoSpin_tE3C4581E7195FF30D21E5E5EFDE1BA9344C385E4 * value)
	{
		____stateAutoSpin_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stateAutoSpin_15), (void*)value);
	}

	inline static int32_t get_offset_of__enable_16() { return static_cast<int32_t>(offsetof(CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47, ____enable_16)); }
	inline bool get__enable_16() const { return ____enable_16; }
	inline bool* get_address_of__enable_16() { return &____enable_16; }
	inline void set__enable_16(bool value)
	{
		____enable_16 = value;
	}

	inline static int32_t get_offset_of__reelAnimation_17() { return static_cast<int32_t>(offsetof(CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47, ____reelAnimation_17)); }
	inline CSReelsAnimation_t04EC01AF43D2016CB036755318F8873C79F9714D * get__reelAnimation_17() const { return ____reelAnimation_17; }
	inline CSReelsAnimation_t04EC01AF43D2016CB036755318F8873C79F9714D ** get_address_of__reelAnimation_17() { return &____reelAnimation_17; }
	inline void set__reelAnimation_17(CSReelsAnimation_t04EC01AF43D2016CB036755318F8873C79F9714D * value)
	{
		____reelAnimation_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____reelAnimation_17), (void*)value);
	}

	inline static int32_t get_offset_of__freeGame_18() { return static_cast<int32_t>(offsetof(CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47, ____freeGame_18)); }
	inline bool get__freeGame_18() const { return ____freeGame_18; }
	inline bool* get_address_of__freeGame_18() { return &____freeGame_18; }
	inline void set__freeGame_18(bool value)
	{
		____freeGame_18 = value;
	}
};


// CSReelsAnimation
struct CSReelsAnimation_t04EC01AF43D2016CB036755318F8873C79F9714D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// CSReels CSReelsAnimation::_reels
	CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47 * ____reels_4;
	// UnityEngine.UI.Image CSReelsAnimation::_line
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ____line_5;
	// System.Collections.IEnumerator CSReelsAnimation::_paylineCoroutine
	RuntimeObject* ____paylineCoroutine_6;
	// CSListNavigation`1<CSPayline> CSReelsAnimation::_paylines
	CSListNavigation_1_t4AC590C238DA470F8CF25714CBC444E615E3C5C1 * ____paylines_7;

public:
	inline static int32_t get_offset_of__reels_4() { return static_cast<int32_t>(offsetof(CSReelsAnimation_t04EC01AF43D2016CB036755318F8873C79F9714D, ____reels_4)); }
	inline CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47 * get__reels_4() const { return ____reels_4; }
	inline CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47 ** get_address_of__reels_4() { return &____reels_4; }
	inline void set__reels_4(CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47 * value)
	{
		____reels_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____reels_4), (void*)value);
	}

	inline static int32_t get_offset_of__line_5() { return static_cast<int32_t>(offsetof(CSReelsAnimation_t04EC01AF43D2016CB036755318F8873C79F9714D, ____line_5)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get__line_5() const { return ____line_5; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of__line_5() { return &____line_5; }
	inline void set__line_5(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		____line_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____line_5), (void*)value);
	}

	inline static int32_t get_offset_of__paylineCoroutine_6() { return static_cast<int32_t>(offsetof(CSReelsAnimation_t04EC01AF43D2016CB036755318F8873C79F9714D, ____paylineCoroutine_6)); }
	inline RuntimeObject* get__paylineCoroutine_6() const { return ____paylineCoroutine_6; }
	inline RuntimeObject** get_address_of__paylineCoroutine_6() { return &____paylineCoroutine_6; }
	inline void set__paylineCoroutine_6(RuntimeObject* value)
	{
		____paylineCoroutine_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____paylineCoroutine_6), (void*)value);
	}

	inline static int32_t get_offset_of__paylines_7() { return static_cast<int32_t>(offsetof(CSReelsAnimation_t04EC01AF43D2016CB036755318F8873C79F9714D, ____paylines_7)); }
	inline CSListNavigation_1_t4AC590C238DA470F8CF25714CBC444E615E3C5C1 * get__paylines_7() const { return ____paylines_7; }
	inline CSListNavigation_1_t4AC590C238DA470F8CF25714CBC444E615E3C5C1 ** get_address_of__paylines_7() { return &____paylines_7; }
	inline void set__paylines_7(CSListNavigation_1_t4AC590C238DA470F8CF25714CBC444E615E3C5C1 * value)
	{
		____paylines_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____paylines_7), (void*)value);
	}
};


// CSSceneManager
struct CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Action`2<System.Single,System.Boolean> CSSceneManager::_callback
	Action_2_tBF5D9EB992F91A3DA045CAF342A5DE18B805F91D * ____callback_5;
	// UnityEngine.AsyncOperation CSSceneManager::_sceneAsync
	AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86 * ____sceneAsync_6;
	// UnityEngine.AsyncOperation CSSceneManager::_assetAsync
	AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86 * ____assetAsync_7;
	// System.Single CSSceneManager::_minsec
	float ____minsec_8;
	// System.Single CSSceneManager::_elapsed
	float ____elapsed_9;
	// System.String CSSceneManager::_sceneName
	String_t* ____sceneName_10;
	// System.Int32 CSSceneManager::_sceneIdx
	int32_t ____sceneIdx_11;
	// System.Single CSSceneManager::_sceneProgress
	float ____sceneProgress_12;
	// System.Single CSSceneManager::_assetProgress
	float ____assetProgress_13;
	// System.Boolean CSSceneManager::_sceneComplete
	bool ____sceneComplete_14;
	// System.Boolean CSSceneManager::_assetComplete
	bool ____assetComplete_15;
	// System.Boolean CSSceneManager::_enable
	bool ____enable_16;

public:
	inline static int32_t get_offset_of__callback_5() { return static_cast<int32_t>(offsetof(CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395, ____callback_5)); }
	inline Action_2_tBF5D9EB992F91A3DA045CAF342A5DE18B805F91D * get__callback_5() const { return ____callback_5; }
	inline Action_2_tBF5D9EB992F91A3DA045CAF342A5DE18B805F91D ** get_address_of__callback_5() { return &____callback_5; }
	inline void set__callback_5(Action_2_tBF5D9EB992F91A3DA045CAF342A5DE18B805F91D * value)
	{
		____callback_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____callback_5), (void*)value);
	}

	inline static int32_t get_offset_of__sceneAsync_6() { return static_cast<int32_t>(offsetof(CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395, ____sceneAsync_6)); }
	inline AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86 * get__sceneAsync_6() const { return ____sceneAsync_6; }
	inline AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86 ** get_address_of__sceneAsync_6() { return &____sceneAsync_6; }
	inline void set__sceneAsync_6(AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86 * value)
	{
		____sceneAsync_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sceneAsync_6), (void*)value);
	}

	inline static int32_t get_offset_of__assetAsync_7() { return static_cast<int32_t>(offsetof(CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395, ____assetAsync_7)); }
	inline AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86 * get__assetAsync_7() const { return ____assetAsync_7; }
	inline AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86 ** get_address_of__assetAsync_7() { return &____assetAsync_7; }
	inline void set__assetAsync_7(AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86 * value)
	{
		____assetAsync_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____assetAsync_7), (void*)value);
	}

	inline static int32_t get_offset_of__minsec_8() { return static_cast<int32_t>(offsetof(CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395, ____minsec_8)); }
	inline float get__minsec_8() const { return ____minsec_8; }
	inline float* get_address_of__minsec_8() { return &____minsec_8; }
	inline void set__minsec_8(float value)
	{
		____minsec_8 = value;
	}

	inline static int32_t get_offset_of__elapsed_9() { return static_cast<int32_t>(offsetof(CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395, ____elapsed_9)); }
	inline float get__elapsed_9() const { return ____elapsed_9; }
	inline float* get_address_of__elapsed_9() { return &____elapsed_9; }
	inline void set__elapsed_9(float value)
	{
		____elapsed_9 = value;
	}

	inline static int32_t get_offset_of__sceneName_10() { return static_cast<int32_t>(offsetof(CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395, ____sceneName_10)); }
	inline String_t* get__sceneName_10() const { return ____sceneName_10; }
	inline String_t** get_address_of__sceneName_10() { return &____sceneName_10; }
	inline void set__sceneName_10(String_t* value)
	{
		____sceneName_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sceneName_10), (void*)value);
	}

	inline static int32_t get_offset_of__sceneIdx_11() { return static_cast<int32_t>(offsetof(CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395, ____sceneIdx_11)); }
	inline int32_t get__sceneIdx_11() const { return ____sceneIdx_11; }
	inline int32_t* get_address_of__sceneIdx_11() { return &____sceneIdx_11; }
	inline void set__sceneIdx_11(int32_t value)
	{
		____sceneIdx_11 = value;
	}

	inline static int32_t get_offset_of__sceneProgress_12() { return static_cast<int32_t>(offsetof(CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395, ____sceneProgress_12)); }
	inline float get__sceneProgress_12() const { return ____sceneProgress_12; }
	inline float* get_address_of__sceneProgress_12() { return &____sceneProgress_12; }
	inline void set__sceneProgress_12(float value)
	{
		____sceneProgress_12 = value;
	}

	inline static int32_t get_offset_of__assetProgress_13() { return static_cast<int32_t>(offsetof(CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395, ____assetProgress_13)); }
	inline float get__assetProgress_13() const { return ____assetProgress_13; }
	inline float* get_address_of__assetProgress_13() { return &____assetProgress_13; }
	inline void set__assetProgress_13(float value)
	{
		____assetProgress_13 = value;
	}

	inline static int32_t get_offset_of__sceneComplete_14() { return static_cast<int32_t>(offsetof(CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395, ____sceneComplete_14)); }
	inline bool get__sceneComplete_14() const { return ____sceneComplete_14; }
	inline bool* get_address_of__sceneComplete_14() { return &____sceneComplete_14; }
	inline void set__sceneComplete_14(bool value)
	{
		____sceneComplete_14 = value;
	}

	inline static int32_t get_offset_of__assetComplete_15() { return static_cast<int32_t>(offsetof(CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395, ____assetComplete_15)); }
	inline bool get__assetComplete_15() const { return ____assetComplete_15; }
	inline bool* get_address_of__assetComplete_15() { return &____assetComplete_15; }
	inline void set__assetComplete_15(bool value)
	{
		____assetComplete_15 = value;
	}

	inline static int32_t get_offset_of__enable_16() { return static_cast<int32_t>(offsetof(CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395, ____enable_16)); }
	inline bool get__enable_16() const { return ____enable_16; }
	inline bool* get_address_of__enable_16() { return &____enable_16; }
	inline void set__enable_16(bool value)
	{
		____enable_16 = value;
	}
};

struct CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395_StaticFields
{
public:
	// CSSceneManager CSSceneManager::instance
	CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395_StaticFields, ___instance_4)); }
	inline CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395 * get_instance_4() const { return ___instance_4; }
	inline CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}
};


// CSSettingsPanel
struct CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single CSSettingsPanel::duration
	float ___duration_4;
	// UnityEngine.GameObject CSSettingsPanel::board
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___board_5;
	// UnityEngine.CanvasGroup CSSettingsPanel::fbInvite
	CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * ___fbInvite_6;
	// UnityEngine.CanvasGroup CSSettingsPanel::fbLogin
	CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * ___fbLogin_7;
	// CSBankCoinPanel CSSettingsPanel::coinPanel
	CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 * ___coinPanel_8;
	// UnityEngine.UI.Image CSSettingsPanel::_background
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ____background_9;
	// UnityEngine.CanvasGroup CSSettingsPanel::_canvas
	CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * ____canvas_10;
	// System.Int32 CSSettingsPanel::_scaleId
	int32_t ____scaleId_11;
	// System.Int32 CSSettingsPanel::_alphaId
	int32_t ____alphaId_12;
	// System.Int32 CSSettingsPanel::_alphaBoardId
	int32_t ____alphaBoardId_13;
	// System.Boolean CSSettingsPanel::_active
	bool ____active_14;

public:
	inline static int32_t get_offset_of_duration_4() { return static_cast<int32_t>(offsetof(CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842, ___duration_4)); }
	inline float get_duration_4() const { return ___duration_4; }
	inline float* get_address_of_duration_4() { return &___duration_4; }
	inline void set_duration_4(float value)
	{
		___duration_4 = value;
	}

	inline static int32_t get_offset_of_board_5() { return static_cast<int32_t>(offsetof(CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842, ___board_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_board_5() const { return ___board_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_board_5() { return &___board_5; }
	inline void set_board_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___board_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___board_5), (void*)value);
	}

	inline static int32_t get_offset_of_fbInvite_6() { return static_cast<int32_t>(offsetof(CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842, ___fbInvite_6)); }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * get_fbInvite_6() const { return ___fbInvite_6; }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F ** get_address_of_fbInvite_6() { return &___fbInvite_6; }
	inline void set_fbInvite_6(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * value)
	{
		___fbInvite_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fbInvite_6), (void*)value);
	}

	inline static int32_t get_offset_of_fbLogin_7() { return static_cast<int32_t>(offsetof(CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842, ___fbLogin_7)); }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * get_fbLogin_7() const { return ___fbLogin_7; }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F ** get_address_of_fbLogin_7() { return &___fbLogin_7; }
	inline void set_fbLogin_7(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * value)
	{
		___fbLogin_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fbLogin_7), (void*)value);
	}

	inline static int32_t get_offset_of_coinPanel_8() { return static_cast<int32_t>(offsetof(CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842, ___coinPanel_8)); }
	inline CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 * get_coinPanel_8() const { return ___coinPanel_8; }
	inline CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 ** get_address_of_coinPanel_8() { return &___coinPanel_8; }
	inline void set_coinPanel_8(CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 * value)
	{
		___coinPanel_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___coinPanel_8), (void*)value);
	}

	inline static int32_t get_offset_of__background_9() { return static_cast<int32_t>(offsetof(CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842, ____background_9)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get__background_9() const { return ____background_9; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of__background_9() { return &____background_9; }
	inline void set__background_9(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		____background_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____background_9), (void*)value);
	}

	inline static int32_t get_offset_of__canvas_10() { return static_cast<int32_t>(offsetof(CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842, ____canvas_10)); }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * get__canvas_10() const { return ____canvas_10; }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F ** get_address_of__canvas_10() { return &____canvas_10; }
	inline void set__canvas_10(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * value)
	{
		____canvas_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____canvas_10), (void*)value);
	}

	inline static int32_t get_offset_of__scaleId_11() { return static_cast<int32_t>(offsetof(CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842, ____scaleId_11)); }
	inline int32_t get__scaleId_11() const { return ____scaleId_11; }
	inline int32_t* get_address_of__scaleId_11() { return &____scaleId_11; }
	inline void set__scaleId_11(int32_t value)
	{
		____scaleId_11 = value;
	}

	inline static int32_t get_offset_of__alphaId_12() { return static_cast<int32_t>(offsetof(CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842, ____alphaId_12)); }
	inline int32_t get__alphaId_12() const { return ____alphaId_12; }
	inline int32_t* get_address_of__alphaId_12() { return &____alphaId_12; }
	inline void set__alphaId_12(int32_t value)
	{
		____alphaId_12 = value;
	}

	inline static int32_t get_offset_of__alphaBoardId_13() { return static_cast<int32_t>(offsetof(CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842, ____alphaBoardId_13)); }
	inline int32_t get__alphaBoardId_13() const { return ____alphaBoardId_13; }
	inline int32_t* get_address_of__alphaBoardId_13() { return &____alphaBoardId_13; }
	inline void set__alphaBoardId_13(int32_t value)
	{
		____alphaBoardId_13 = value;
	}

	inline static int32_t get_offset_of__active_14() { return static_cast<int32_t>(offsetof(CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842, ____active_14)); }
	inline bool get__active_14() const { return ____active_14; }
	inline bool* get_address_of__active_14() { return &____active_14; }
	inline void set__active_14(bool value)
	{
		____active_14 = value;
	}
};


// CSSettingsPanelSmall
struct CSSettingsPanelSmall_t436B210ADD3C25F979D822BD214FBDE286D44CA0  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// CSSettingsPanel CSSettingsPanelSmall::settings
	CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842 * ___settings_4;
	// System.Single CSSettingsPanelSmall::duration
	float ___duration_5;
	// UnityEngine.CanvasGroup CSSettingsPanelSmall::_canvasGroup
	CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * ____canvasGroup_6;
	// System.Boolean CSSettingsPanelSmall::_show
	bool ____show_7;

public:
	inline static int32_t get_offset_of_settings_4() { return static_cast<int32_t>(offsetof(CSSettingsPanelSmall_t436B210ADD3C25F979D822BD214FBDE286D44CA0, ___settings_4)); }
	inline CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842 * get_settings_4() const { return ___settings_4; }
	inline CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842 ** get_address_of_settings_4() { return &___settings_4; }
	inline void set_settings_4(CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842 * value)
	{
		___settings_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___settings_4), (void*)value);
	}

	inline static int32_t get_offset_of_duration_5() { return static_cast<int32_t>(offsetof(CSSettingsPanelSmall_t436B210ADD3C25F979D822BD214FBDE286D44CA0, ___duration_5)); }
	inline float get_duration_5() const { return ___duration_5; }
	inline float* get_address_of_duration_5() { return &___duration_5; }
	inline void set_duration_5(float value)
	{
		___duration_5 = value;
	}

	inline static int32_t get_offset_of__canvasGroup_6() { return static_cast<int32_t>(offsetof(CSSettingsPanelSmall_t436B210ADD3C25F979D822BD214FBDE286D44CA0, ____canvasGroup_6)); }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * get__canvasGroup_6() const { return ____canvasGroup_6; }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F ** get_address_of__canvasGroup_6() { return &____canvasGroup_6; }
	inline void set__canvasGroup_6(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * value)
	{
		____canvasGroup_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____canvasGroup_6), (void*)value);
	}

	inline static int32_t get_offset_of__show_7() { return static_cast<int32_t>(offsetof(CSSettingsPanelSmall_t436B210ADD3C25F979D822BD214FBDE286D44CA0, ____show_7)); }
	inline bool get__show_7() const { return ____show_7; }
	inline bool* get_address_of__show_7() { return &____show_7; }
	inline void set__show_7(bool value)
	{
		____show_7 = value;
	}
};


// CSSliderGroup
struct CSSliderGroup_tDC34B5CDF947DBA827407A2B6D39F5E7ADC28F4C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Slider[] CSSliderGroup::sliders
	SliderU5BU5D_t819F98171147BE3B24976AC62AB8B1900BDEE01A* ___sliders_4;
	// System.Single CSSliderGroup::startValue
	float ___startValue_5;

public:
	inline static int32_t get_offset_of_sliders_4() { return static_cast<int32_t>(offsetof(CSSliderGroup_tDC34B5CDF947DBA827407A2B6D39F5E7ADC28F4C, ___sliders_4)); }
	inline SliderU5BU5D_t819F98171147BE3B24976AC62AB8B1900BDEE01A* get_sliders_4() const { return ___sliders_4; }
	inline SliderU5BU5D_t819F98171147BE3B24976AC62AB8B1900BDEE01A** get_address_of_sliders_4() { return &___sliders_4; }
	inline void set_sliders_4(SliderU5BU5D_t819F98171147BE3B24976AC62AB8B1900BDEE01A* value)
	{
		___sliders_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sliders_4), (void*)value);
	}

	inline static int32_t get_offset_of_startValue_5() { return static_cast<int32_t>(offsetof(CSSliderGroup_tDC34B5CDF947DBA827407A2B6D39F5E7ADC28F4C, ___startValue_5)); }
	inline float get_startValue_5() const { return ___startValue_5; }
	inline float* get_address_of_startValue_5() { return &___startValue_5; }
	inline void set_startValue_5(float value)
	{
		___startValue_5 = value;
	}
};


// CSSoundManager
struct CSSoundManager_t413846E33ECF342195F7334420A0F2BAE7B62736  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Sound[] CSSoundManager::sounds
	SoundU5BU5D_tA5C580C0BFABFBC56CF4F26FC1765B4F5194E2DF* ___sounds_5;
	// System.String CSSoundManager::_playingMusic
	String_t* ____playingMusic_6;

public:
	inline static int32_t get_offset_of_sounds_5() { return static_cast<int32_t>(offsetof(CSSoundManager_t413846E33ECF342195F7334420A0F2BAE7B62736, ___sounds_5)); }
	inline SoundU5BU5D_tA5C580C0BFABFBC56CF4F26FC1765B4F5194E2DF* get_sounds_5() const { return ___sounds_5; }
	inline SoundU5BU5D_tA5C580C0BFABFBC56CF4F26FC1765B4F5194E2DF** get_address_of_sounds_5() { return &___sounds_5; }
	inline void set_sounds_5(SoundU5BU5D_tA5C580C0BFABFBC56CF4F26FC1765B4F5194E2DF* value)
	{
		___sounds_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sounds_5), (void*)value);
	}

	inline static int32_t get_offset_of__playingMusic_6() { return static_cast<int32_t>(offsetof(CSSoundManager_t413846E33ECF342195F7334420A0F2BAE7B62736, ____playingMusic_6)); }
	inline String_t* get__playingMusic_6() const { return ____playingMusic_6; }
	inline String_t** get_address_of__playingMusic_6() { return &____playingMusic_6; }
	inline void set__playingMusic_6(String_t* value)
	{
		____playingMusic_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____playingMusic_6), (void*)value);
	}
};

struct CSSoundManager_t413846E33ECF342195F7334420A0F2BAE7B62736_StaticFields
{
public:
	// CSSoundManager CSSoundManager::instance
	CSSoundManager_t413846E33ECF342195F7334420A0F2BAE7B62736 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(CSSoundManager_t413846E33ECF342195F7334420A0F2BAE7B62736_StaticFields, ___instance_4)); }
	inline CSSoundManager_t413846E33ECF342195F7334420A0F2BAE7B62736 * get_instance_4() const { return ___instance_4; }
	inline CSSoundManager_t413846E33ECF342195F7334420A0F2BAE7B62736 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(CSSoundManager_t413846E33ECF342195F7334420A0F2BAE7B62736 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}
};


// CSStoreCoinPack
struct CSStoreCoinPack_tF11AD02E4445D9C591F431372635667C4B9D6551  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Text CSStoreCoinPack::price
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___price_4;
	// UnityEngine.UI.Text CSStoreCoinPack::total
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___total_5;
	// CSIAPProduct CSStoreCoinPack::product
	CSIAPProduct_tF735AB0F8AA68F1481FA63E8AF5EB9AF02F7EAA2 * ___product_6;

public:
	inline static int32_t get_offset_of_price_4() { return static_cast<int32_t>(offsetof(CSStoreCoinPack_tF11AD02E4445D9C591F431372635667C4B9D6551, ___price_4)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_price_4() const { return ___price_4; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_price_4() { return &___price_4; }
	inline void set_price_4(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___price_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___price_4), (void*)value);
	}

	inline static int32_t get_offset_of_total_5() { return static_cast<int32_t>(offsetof(CSStoreCoinPack_tF11AD02E4445D9C591F431372635667C4B9D6551, ___total_5)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_total_5() const { return ___total_5; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_total_5() { return &___total_5; }
	inline void set_total_5(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___total_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___total_5), (void*)value);
	}

	inline static int32_t get_offset_of_product_6() { return static_cast<int32_t>(offsetof(CSStoreCoinPack_tF11AD02E4445D9C591F431372635667C4B9D6551, ___product_6)); }
	inline CSIAPProduct_tF735AB0F8AA68F1481FA63E8AF5EB9AF02F7EAA2 * get_product_6() const { return ___product_6; }
	inline CSIAPProduct_tF735AB0F8AA68F1481FA63E8AF5EB9AF02F7EAA2 ** get_address_of_product_6() { return &___product_6; }
	inline void set_product_6(CSIAPProduct_tF735AB0F8AA68F1481FA63E8AF5EB9AF02F7EAA2 * value)
	{
		___product_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___product_6), (void*)value);
	}
};


// CSSymbol
struct CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// CSSymbolData[] CSSymbol::data
	CSSymbolDataU5BU5D_tFCB90478E34016E4E24E2CAC2B35B9759D7C174D* ___data_4;
	// CSSymbolData CSSymbol::curr
	CSSymbolData_t189F10C509C4A90E39640E27AA4367BCC44D5816 * ___curr_5;
	// System.Collections.Generic.Dictionary`2<CSSymbolType,CSSymbolData> CSSymbol::dictionaryData
	Dictionary_2_t0B8BEAF41857685DF2F0FB4B87D806425B9E3188 * ___dictionaryData_6;
	// CSSymbolType CSSymbol::type
	int32_t ___type_7;
	// CSCell CSSymbol::cell
	CSCell_tEC2CF7F1268B4BE5C1155F2B9476449C3A14383D * ___cell_8;
	// CSSymbol CSSymbol::replacement
	CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555 * ___replacement_9;
	// CSSymbolPercent[] CSSymbol::percents
	CSSymbolPercentU5BU5D_tFBD1ACC0D6BF2C5F9D97B45EEC1FA6E3D395FF8D* ___percents_10;
	// UnityEngine.UI.Image CSSymbol::_image
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ____image_11;
	// System.Int32 CSSymbol::_animationId
	int32_t ____animationId_12;
	// UnityEngine.ParticleSystem CSSymbol::_particle
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ____particle_13;
	// UnityEngine.RectTransform CSSymbol::_rect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ____rect_14;
	// System.Single CSSymbol::_totalChance
	float ____totalChance_15;

public:
	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555, ___data_4)); }
	inline CSSymbolDataU5BU5D_tFCB90478E34016E4E24E2CAC2B35B9759D7C174D* get_data_4() const { return ___data_4; }
	inline CSSymbolDataU5BU5D_tFCB90478E34016E4E24E2CAC2B35B9759D7C174D** get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(CSSymbolDataU5BU5D_tFCB90478E34016E4E24E2CAC2B35B9759D7C174D* value)
	{
		___data_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_4), (void*)value);
	}

	inline static int32_t get_offset_of_curr_5() { return static_cast<int32_t>(offsetof(CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555, ___curr_5)); }
	inline CSSymbolData_t189F10C509C4A90E39640E27AA4367BCC44D5816 * get_curr_5() const { return ___curr_5; }
	inline CSSymbolData_t189F10C509C4A90E39640E27AA4367BCC44D5816 ** get_address_of_curr_5() { return &___curr_5; }
	inline void set_curr_5(CSSymbolData_t189F10C509C4A90E39640E27AA4367BCC44D5816 * value)
	{
		___curr_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___curr_5), (void*)value);
	}

	inline static int32_t get_offset_of_dictionaryData_6() { return static_cast<int32_t>(offsetof(CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555, ___dictionaryData_6)); }
	inline Dictionary_2_t0B8BEAF41857685DF2F0FB4B87D806425B9E3188 * get_dictionaryData_6() const { return ___dictionaryData_6; }
	inline Dictionary_2_t0B8BEAF41857685DF2F0FB4B87D806425B9E3188 ** get_address_of_dictionaryData_6() { return &___dictionaryData_6; }
	inline void set_dictionaryData_6(Dictionary_2_t0B8BEAF41857685DF2F0FB4B87D806425B9E3188 * value)
	{
		___dictionaryData_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dictionaryData_6), (void*)value);
	}

	inline static int32_t get_offset_of_type_7() { return static_cast<int32_t>(offsetof(CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555, ___type_7)); }
	inline int32_t get_type_7() const { return ___type_7; }
	inline int32_t* get_address_of_type_7() { return &___type_7; }
	inline void set_type_7(int32_t value)
	{
		___type_7 = value;
	}

	inline static int32_t get_offset_of_cell_8() { return static_cast<int32_t>(offsetof(CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555, ___cell_8)); }
	inline CSCell_tEC2CF7F1268B4BE5C1155F2B9476449C3A14383D * get_cell_8() const { return ___cell_8; }
	inline CSCell_tEC2CF7F1268B4BE5C1155F2B9476449C3A14383D ** get_address_of_cell_8() { return &___cell_8; }
	inline void set_cell_8(CSCell_tEC2CF7F1268B4BE5C1155F2B9476449C3A14383D * value)
	{
		___cell_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cell_8), (void*)value);
	}

	inline static int32_t get_offset_of_replacement_9() { return static_cast<int32_t>(offsetof(CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555, ___replacement_9)); }
	inline CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555 * get_replacement_9() const { return ___replacement_9; }
	inline CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555 ** get_address_of_replacement_9() { return &___replacement_9; }
	inline void set_replacement_9(CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555 * value)
	{
		___replacement_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___replacement_9), (void*)value);
	}

	inline static int32_t get_offset_of_percents_10() { return static_cast<int32_t>(offsetof(CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555, ___percents_10)); }
	inline CSSymbolPercentU5BU5D_tFBD1ACC0D6BF2C5F9D97B45EEC1FA6E3D395FF8D* get_percents_10() const { return ___percents_10; }
	inline CSSymbolPercentU5BU5D_tFBD1ACC0D6BF2C5F9D97B45EEC1FA6E3D395FF8D** get_address_of_percents_10() { return &___percents_10; }
	inline void set_percents_10(CSSymbolPercentU5BU5D_tFBD1ACC0D6BF2C5F9D97B45EEC1FA6E3D395FF8D* value)
	{
		___percents_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___percents_10), (void*)value);
	}

	inline static int32_t get_offset_of__image_11() { return static_cast<int32_t>(offsetof(CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555, ____image_11)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get__image_11() const { return ____image_11; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of__image_11() { return &____image_11; }
	inline void set__image_11(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		____image_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____image_11), (void*)value);
	}

	inline static int32_t get_offset_of__animationId_12() { return static_cast<int32_t>(offsetof(CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555, ____animationId_12)); }
	inline int32_t get__animationId_12() const { return ____animationId_12; }
	inline int32_t* get_address_of__animationId_12() { return &____animationId_12; }
	inline void set__animationId_12(int32_t value)
	{
		____animationId_12 = value;
	}

	inline static int32_t get_offset_of__particle_13() { return static_cast<int32_t>(offsetof(CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555, ____particle_13)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get__particle_13() const { return ____particle_13; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of__particle_13() { return &____particle_13; }
	inline void set__particle_13(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		____particle_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____particle_13), (void*)value);
	}

	inline static int32_t get_offset_of__rect_14() { return static_cast<int32_t>(offsetof(CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555, ____rect_14)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get__rect_14() const { return ____rect_14; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of__rect_14() { return &____rect_14; }
	inline void set__rect_14(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		____rect_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rect_14), (void*)value);
	}

	inline static int32_t get_offset_of__totalChance_15() { return static_cast<int32_t>(offsetof(CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555, ____totalChance_15)); }
	inline float get__totalChance_15() const { return ____totalChance_15; }
	inline float* get_address_of__totalChance_15() { return &____totalChance_15; }
	inline void set__totalChance_15(float value)
	{
		____totalChance_15 = value;
	}
};


// CSTimerManager
struct CSTimerManager_t6F1C7120C836A3FC70D493E50F2F43B023E5F4EA  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Action`2<CSTimer,System.String> CSTimerManager::TimerCreatedEvent
	Action_2_tCF09AEE84B095AE2F1E794A351B70E78CB615A6B * ___TimerCreatedEvent_4;
	// System.Collections.Generic.Dictionary`2<System.String,CSTimer> CSTimerManager::_timers
	Dictionary_2_t9F4C1D688345D026BBBA09BE046EAF8BF6564015 * ____timers_6;
	// System.Collections.Generic.List`1<CSTimer> CSTimerManager::_add
	List_1_tC45461C839422BCF03CB1E007A1F16EE75DFE211 * ____add_7;
	// System.Collections.Generic.List`1<CSTimer> CSTimerManager::_remove
	List_1_tC45461C839422BCF03CB1E007A1F16EE75DFE211 * ____remove_8;

public:
	inline static int32_t get_offset_of_TimerCreatedEvent_4() { return static_cast<int32_t>(offsetof(CSTimerManager_t6F1C7120C836A3FC70D493E50F2F43B023E5F4EA, ___TimerCreatedEvent_4)); }
	inline Action_2_tCF09AEE84B095AE2F1E794A351B70E78CB615A6B * get_TimerCreatedEvent_4() const { return ___TimerCreatedEvent_4; }
	inline Action_2_tCF09AEE84B095AE2F1E794A351B70E78CB615A6B ** get_address_of_TimerCreatedEvent_4() { return &___TimerCreatedEvent_4; }
	inline void set_TimerCreatedEvent_4(Action_2_tCF09AEE84B095AE2F1E794A351B70E78CB615A6B * value)
	{
		___TimerCreatedEvent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TimerCreatedEvent_4), (void*)value);
	}

	inline static int32_t get_offset_of__timers_6() { return static_cast<int32_t>(offsetof(CSTimerManager_t6F1C7120C836A3FC70D493E50F2F43B023E5F4EA, ____timers_6)); }
	inline Dictionary_2_t9F4C1D688345D026BBBA09BE046EAF8BF6564015 * get__timers_6() const { return ____timers_6; }
	inline Dictionary_2_t9F4C1D688345D026BBBA09BE046EAF8BF6564015 ** get_address_of__timers_6() { return &____timers_6; }
	inline void set__timers_6(Dictionary_2_t9F4C1D688345D026BBBA09BE046EAF8BF6564015 * value)
	{
		____timers_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____timers_6), (void*)value);
	}

	inline static int32_t get_offset_of__add_7() { return static_cast<int32_t>(offsetof(CSTimerManager_t6F1C7120C836A3FC70D493E50F2F43B023E5F4EA, ____add_7)); }
	inline List_1_tC45461C839422BCF03CB1E007A1F16EE75DFE211 * get__add_7() const { return ____add_7; }
	inline List_1_tC45461C839422BCF03CB1E007A1F16EE75DFE211 ** get_address_of__add_7() { return &____add_7; }
	inline void set__add_7(List_1_tC45461C839422BCF03CB1E007A1F16EE75DFE211 * value)
	{
		____add_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____add_7), (void*)value);
	}

	inline static int32_t get_offset_of__remove_8() { return static_cast<int32_t>(offsetof(CSTimerManager_t6F1C7120C836A3FC70D493E50F2F43B023E5F4EA, ____remove_8)); }
	inline List_1_tC45461C839422BCF03CB1E007A1F16EE75DFE211 * get__remove_8() const { return ____remove_8; }
	inline List_1_tC45461C839422BCF03CB1E007A1F16EE75DFE211 ** get_address_of__remove_8() { return &____remove_8; }
	inline void set__remove_8(List_1_tC45461C839422BCF03CB1E007A1F16EE75DFE211 * value)
	{
		____remove_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remove_8), (void*)value);
	}
};

struct CSTimerManager_t6F1C7120C836A3FC70D493E50F2F43B023E5F4EA_StaticFields
{
public:
	// CSTimerManager CSTimerManager::instance
	CSTimerManager_t6F1C7120C836A3FC70D493E50F2F43B023E5F4EA * ___instance_5;

public:
	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(CSTimerManager_t6F1C7120C836A3FC70D493E50F2F43B023E5F4EA_StaticFields, ___instance_5)); }
	inline CSTimerManager_t6F1C7120C836A3FC70D493E50F2F43B023E5F4EA * get_instance_5() const { return ___instance_5; }
	inline CSTimerManager_t6F1C7120C836A3FC70D493E50F2F43B023E5F4EA ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(CSTimerManager_t6F1C7120C836A3FC70D493E50F2F43B023E5F4EA * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_5), (void*)value);
	}
};


// CSToggleGroupHelper
struct CSToggleGroupHelper_t0B4982EA19DDA807327BDCF285651D4FB8C4551A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Toggle> CSToggleGroupHelper::source_list
	List_1_tECEEA56321275CFF8DECB929786CE364F743B07D * ___source_list_4;
	// System.Reflection.FieldInfo CSToggleGroupHelper::_toggleListMember
	FieldInfo_t * ____toggleListMember_5;
	// UnityEngine.UI.ToggleGroup CSToggleGroupHelper::m_Group
	ToggleGroup_t12E1DFDEB3FFD979A20299EE42A94388AC619C95 * ___m_Group_6;

public:
	inline static int32_t get_offset_of_source_list_4() { return static_cast<int32_t>(offsetof(CSToggleGroupHelper_t0B4982EA19DDA807327BDCF285651D4FB8C4551A, ___source_list_4)); }
	inline List_1_tECEEA56321275CFF8DECB929786CE364F743B07D * get_source_list_4() const { return ___source_list_4; }
	inline List_1_tECEEA56321275CFF8DECB929786CE364F743B07D ** get_address_of_source_list_4() { return &___source_list_4; }
	inline void set_source_list_4(List_1_tECEEA56321275CFF8DECB929786CE364F743B07D * value)
	{
		___source_list_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_list_4), (void*)value);
	}

	inline static int32_t get_offset_of__toggleListMember_5() { return static_cast<int32_t>(offsetof(CSToggleGroupHelper_t0B4982EA19DDA807327BDCF285651D4FB8C4551A, ____toggleListMember_5)); }
	inline FieldInfo_t * get__toggleListMember_5() const { return ____toggleListMember_5; }
	inline FieldInfo_t ** get_address_of__toggleListMember_5() { return &____toggleListMember_5; }
	inline void set__toggleListMember_5(FieldInfo_t * value)
	{
		____toggleListMember_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____toggleListMember_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Group_6() { return static_cast<int32_t>(offsetof(CSToggleGroupHelper_t0B4982EA19DDA807327BDCF285651D4FB8C4551A, ___m_Group_6)); }
	inline ToggleGroup_t12E1DFDEB3FFD979A20299EE42A94388AC619C95 * get_m_Group_6() const { return ___m_Group_6; }
	inline ToggleGroup_t12E1DFDEB3FFD979A20299EE42A94388AC619C95 ** get_address_of_m_Group_6() { return &___m_Group_6; }
	inline void set_m_Group_6(ToggleGroup_t12E1DFDEB3FFD979A20299EE42A94388AC619C95 * value)
	{
		___m_Group_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Group_6), (void*)value);
	}
};


// CSToggleTap
struct CSToggleTap_tEE66A0694739ECD7180FF6620159D08C313652E0  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// CSTopPanel
struct CSTopPanel_t94C2B84ED826C2C503575317319F623658C0C4D8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// CSGameStore CSTopPanel::store
	CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D * ___store_4;
	// CSExperiencePanel CSTopPanel::xpPanel
	CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F * ___xpPanel_5;
	// CSBankCoinPanel CSTopPanel::coinPanel
	CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 * ___coinPanel_6;

public:
	inline static int32_t get_offset_of_store_4() { return static_cast<int32_t>(offsetof(CSTopPanel_t94C2B84ED826C2C503575317319F623658C0C4D8, ___store_4)); }
	inline CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D * get_store_4() const { return ___store_4; }
	inline CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D ** get_address_of_store_4() { return &___store_4; }
	inline void set_store_4(CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D * value)
	{
		___store_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___store_4), (void*)value);
	}

	inline static int32_t get_offset_of_xpPanel_5() { return static_cast<int32_t>(offsetof(CSTopPanel_t94C2B84ED826C2C503575317319F623658C0C4D8, ___xpPanel_5)); }
	inline CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F * get_xpPanel_5() const { return ___xpPanel_5; }
	inline CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F ** get_address_of_xpPanel_5() { return &___xpPanel_5; }
	inline void set_xpPanel_5(CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F * value)
	{
		___xpPanel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___xpPanel_5), (void*)value);
	}

	inline static int32_t get_offset_of_coinPanel_6() { return static_cast<int32_t>(offsetof(CSTopPanel_t94C2B84ED826C2C503575317319F623658C0C4D8, ___coinPanel_6)); }
	inline CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 * get_coinPanel_6() const { return ___coinPanel_6; }
	inline CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 ** get_address_of_coinPanel_6() { return &___coinPanel_6; }
	inline void set_coinPanel_6(CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 * value)
	{
		___coinPanel_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___coinPanel_6), (void*)value);
	}
};


// CSZLGamble
struct CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform CSZLGamble::cardParent
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___cardParent_4;
	// UnityEngine.GameObject CSZLGamble::contentPrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___contentPrefab_5;
	// CSZLGambleContent CSZLGamble::_content
	CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D * ____content_6;
	// UnityEngine.UI.Image CSZLGamble::background
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___background_7;
	// UnityEngine.CanvasGroup CSZLGamble::canvas
	CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * ___canvas_8;
	// UnityEngine.UI.Text CSZLGamble::bankText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___bankText_9;
	// UnityEngine.UI.Text CSZLGamble::betText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___betText_10;
	// UnityEngine.UI.Button CSZLGamble::collectButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___collectButton_11;
	// CSAlertRewardAnim CSZLGamble::alert
	CSAlertRewardAnim_t13B8641467B4284296D29B346C9760D59E21E9E0 * ___alert_12;
	// CSBottomPanel CSZLGamble::bottomPanel
	CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38 * ___bottomPanel_13;
	// System.Single CSZLGamble::_bet
	float ____bet_14;
	// System.Single CSZLGamble::_bank
	float ____bank_15;
	// System.Boolean CSZLGamble::_interactable
	bool ____interactable_16;
	// System.Boolean CSZLGamble::_enable
	bool ____enable_17;

public:
	inline static int32_t get_offset_of_cardParent_4() { return static_cast<int32_t>(offsetof(CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C, ___cardParent_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_cardParent_4() const { return ___cardParent_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_cardParent_4() { return &___cardParent_4; }
	inline void set_cardParent_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___cardParent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cardParent_4), (void*)value);
	}

	inline static int32_t get_offset_of_contentPrefab_5() { return static_cast<int32_t>(offsetof(CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C, ___contentPrefab_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_contentPrefab_5() const { return ___contentPrefab_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_contentPrefab_5() { return &___contentPrefab_5; }
	inline void set_contentPrefab_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___contentPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___contentPrefab_5), (void*)value);
	}

	inline static int32_t get_offset_of__content_6() { return static_cast<int32_t>(offsetof(CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C, ____content_6)); }
	inline CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D * get__content_6() const { return ____content_6; }
	inline CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D ** get_address_of__content_6() { return &____content_6; }
	inline void set__content_6(CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D * value)
	{
		____content_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____content_6), (void*)value);
	}

	inline static int32_t get_offset_of_background_7() { return static_cast<int32_t>(offsetof(CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C, ___background_7)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_background_7() const { return ___background_7; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_background_7() { return &___background_7; }
	inline void set_background_7(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___background_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___background_7), (void*)value);
	}

	inline static int32_t get_offset_of_canvas_8() { return static_cast<int32_t>(offsetof(CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C, ___canvas_8)); }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * get_canvas_8() const { return ___canvas_8; }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F ** get_address_of_canvas_8() { return &___canvas_8; }
	inline void set_canvas_8(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * value)
	{
		___canvas_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvas_8), (void*)value);
	}

	inline static int32_t get_offset_of_bankText_9() { return static_cast<int32_t>(offsetof(CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C, ___bankText_9)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_bankText_9() const { return ___bankText_9; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_bankText_9() { return &___bankText_9; }
	inline void set_bankText_9(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___bankText_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bankText_9), (void*)value);
	}

	inline static int32_t get_offset_of_betText_10() { return static_cast<int32_t>(offsetof(CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C, ___betText_10)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_betText_10() const { return ___betText_10; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_betText_10() { return &___betText_10; }
	inline void set_betText_10(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___betText_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___betText_10), (void*)value);
	}

	inline static int32_t get_offset_of_collectButton_11() { return static_cast<int32_t>(offsetof(CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C, ___collectButton_11)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_collectButton_11() const { return ___collectButton_11; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_collectButton_11() { return &___collectButton_11; }
	inline void set_collectButton_11(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___collectButton_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___collectButton_11), (void*)value);
	}

	inline static int32_t get_offset_of_alert_12() { return static_cast<int32_t>(offsetof(CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C, ___alert_12)); }
	inline CSAlertRewardAnim_t13B8641467B4284296D29B346C9760D59E21E9E0 * get_alert_12() const { return ___alert_12; }
	inline CSAlertRewardAnim_t13B8641467B4284296D29B346C9760D59E21E9E0 ** get_address_of_alert_12() { return &___alert_12; }
	inline void set_alert_12(CSAlertRewardAnim_t13B8641467B4284296D29B346C9760D59E21E9E0 * value)
	{
		___alert_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___alert_12), (void*)value);
	}

	inline static int32_t get_offset_of_bottomPanel_13() { return static_cast<int32_t>(offsetof(CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C, ___bottomPanel_13)); }
	inline CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38 * get_bottomPanel_13() const { return ___bottomPanel_13; }
	inline CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38 ** get_address_of_bottomPanel_13() { return &___bottomPanel_13; }
	inline void set_bottomPanel_13(CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38 * value)
	{
		___bottomPanel_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bottomPanel_13), (void*)value);
	}

	inline static int32_t get_offset_of__bet_14() { return static_cast<int32_t>(offsetof(CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C, ____bet_14)); }
	inline float get__bet_14() const { return ____bet_14; }
	inline float* get_address_of__bet_14() { return &____bet_14; }
	inline void set__bet_14(float value)
	{
		____bet_14 = value;
	}

	inline static int32_t get_offset_of__bank_15() { return static_cast<int32_t>(offsetof(CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C, ____bank_15)); }
	inline float get__bank_15() const { return ____bank_15; }
	inline float* get_address_of__bank_15() { return &____bank_15; }
	inline void set__bank_15(float value)
	{
		____bank_15 = value;
	}

	inline static int32_t get_offset_of__interactable_16() { return static_cast<int32_t>(offsetof(CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C, ____interactable_16)); }
	inline bool get__interactable_16() const { return ____interactable_16; }
	inline bool* get_address_of__interactable_16() { return &____interactable_16; }
	inline void set__interactable_16(bool value)
	{
		____interactable_16 = value;
	}

	inline static int32_t get_offset_of__enable_17() { return static_cast<int32_t>(offsetof(CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C, ____enable_17)); }
	inline bool get__enable_17() const { return ____enable_17; }
	inline bool* get_address_of__enable_17() { return &____enable_17; }
	inline void set__enable_17(bool value)
	{
		____enable_17 = value;
	}
};


// CSZLGambleContent
struct CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Action`2<System.Boolean,CSZLGambleContent> CSZLGambleContent::ResultEvent
	Action_2_t6263456D797EA3E0910B4DCAB4AA76BB2A78EB45 * ___ResultEvent_4;
	// System.Action`3<System.Boolean,CSCard,CSCard> CSZLGambleContent::CardSelectedEvent
	Action_3_t14CBE6B9894E7C8B946B2CDA30D69657CCF537C8 * ___CardSelectedEvent_5;
	// System.Collections.Generic.List`1<CSCardValue> CSZLGambleContent::cardDeck
	List_1_t4CDCC84CA5994FB6E22235E4D8E37718C18D58B6 * ___cardDeck_6;
	// UnityEngine.GameObject CSZLGambleContent::cardPrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cardPrefab_7;
	// UnityEngine.Transform[] CSZLGambleContent::_cards
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ____cards_8;
	// UnityEngine.CanvasGroup CSZLGambleContent::_canvas
	CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * ____canvas_9;
	// System.Single CSZLGambleContent::space
	float ___space_10;
	// System.Boolean CSZLGambleContent::_enable
	bool ____enable_11;

public:
	inline static int32_t get_offset_of_ResultEvent_4() { return static_cast<int32_t>(offsetof(CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D, ___ResultEvent_4)); }
	inline Action_2_t6263456D797EA3E0910B4DCAB4AA76BB2A78EB45 * get_ResultEvent_4() const { return ___ResultEvent_4; }
	inline Action_2_t6263456D797EA3E0910B4DCAB4AA76BB2A78EB45 ** get_address_of_ResultEvent_4() { return &___ResultEvent_4; }
	inline void set_ResultEvent_4(Action_2_t6263456D797EA3E0910B4DCAB4AA76BB2A78EB45 * value)
	{
		___ResultEvent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ResultEvent_4), (void*)value);
	}

	inline static int32_t get_offset_of_CardSelectedEvent_5() { return static_cast<int32_t>(offsetof(CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D, ___CardSelectedEvent_5)); }
	inline Action_3_t14CBE6B9894E7C8B946B2CDA30D69657CCF537C8 * get_CardSelectedEvent_5() const { return ___CardSelectedEvent_5; }
	inline Action_3_t14CBE6B9894E7C8B946B2CDA30D69657CCF537C8 ** get_address_of_CardSelectedEvent_5() { return &___CardSelectedEvent_5; }
	inline void set_CardSelectedEvent_5(Action_3_t14CBE6B9894E7C8B946B2CDA30D69657CCF537C8 * value)
	{
		___CardSelectedEvent_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CardSelectedEvent_5), (void*)value);
	}

	inline static int32_t get_offset_of_cardDeck_6() { return static_cast<int32_t>(offsetof(CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D, ___cardDeck_6)); }
	inline List_1_t4CDCC84CA5994FB6E22235E4D8E37718C18D58B6 * get_cardDeck_6() const { return ___cardDeck_6; }
	inline List_1_t4CDCC84CA5994FB6E22235E4D8E37718C18D58B6 ** get_address_of_cardDeck_6() { return &___cardDeck_6; }
	inline void set_cardDeck_6(List_1_t4CDCC84CA5994FB6E22235E4D8E37718C18D58B6 * value)
	{
		___cardDeck_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cardDeck_6), (void*)value);
	}

	inline static int32_t get_offset_of_cardPrefab_7() { return static_cast<int32_t>(offsetof(CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D, ___cardPrefab_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cardPrefab_7() const { return ___cardPrefab_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cardPrefab_7() { return &___cardPrefab_7; }
	inline void set_cardPrefab_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cardPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cardPrefab_7), (void*)value);
	}

	inline static int32_t get_offset_of__cards_8() { return static_cast<int32_t>(offsetof(CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D, ____cards_8)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get__cards_8() const { return ____cards_8; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of__cards_8() { return &____cards_8; }
	inline void set__cards_8(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		____cards_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____cards_8), (void*)value);
	}

	inline static int32_t get_offset_of__canvas_9() { return static_cast<int32_t>(offsetof(CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D, ____canvas_9)); }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * get__canvas_9() const { return ____canvas_9; }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F ** get_address_of__canvas_9() { return &____canvas_9; }
	inline void set__canvas_9(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * value)
	{
		____canvas_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____canvas_9), (void*)value);
	}

	inline static int32_t get_offset_of_space_10() { return static_cast<int32_t>(offsetof(CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D, ___space_10)); }
	inline float get_space_10() const { return ___space_10; }
	inline float* get_address_of_space_10() { return &___space_10; }
	inline void set_space_10(float value)
	{
		___space_10 = value;
	}

	inline static int32_t get_offset_of__enable_11() { return static_cast<int32_t>(offsetof(CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D, ____enable_11)); }
	inline bool get__enable_11() const { return ____enable_11; }
	inline bool* get_address_of__enable_11() { return &____enable_11; }
	inline void set__enable_11(bool value)
	{
		____enable_11 = value;
	}
};


// TMPro.Examples.CameraController
struct CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform TMPro.Examples.CameraController::cameraTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___cameraTransform_4;
	// UnityEngine.Transform TMPro.Examples.CameraController::dummyTarget
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___dummyTarget_5;
	// UnityEngine.Transform TMPro.Examples.CameraController::CameraTarget
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___CameraTarget_6;
	// System.Single TMPro.Examples.CameraController::FollowDistance
	float ___FollowDistance_7;
	// System.Single TMPro.Examples.CameraController::MaxFollowDistance
	float ___MaxFollowDistance_8;
	// System.Single TMPro.Examples.CameraController::MinFollowDistance
	float ___MinFollowDistance_9;
	// System.Single TMPro.Examples.CameraController::ElevationAngle
	float ___ElevationAngle_10;
	// System.Single TMPro.Examples.CameraController::MaxElevationAngle
	float ___MaxElevationAngle_11;
	// System.Single TMPro.Examples.CameraController::MinElevationAngle
	float ___MinElevationAngle_12;
	// System.Single TMPro.Examples.CameraController::OrbitalAngle
	float ___OrbitalAngle_13;
	// TMPro.Examples.CameraController/CameraModes TMPro.Examples.CameraController::CameraMode
	int32_t ___CameraMode_14;
	// System.Boolean TMPro.Examples.CameraController::MovementSmoothing
	bool ___MovementSmoothing_15;
	// System.Boolean TMPro.Examples.CameraController::RotationSmoothing
	bool ___RotationSmoothing_16;
	// System.Boolean TMPro.Examples.CameraController::previousSmoothing
	bool ___previousSmoothing_17;
	// System.Single TMPro.Examples.CameraController::MovementSmoothingValue
	float ___MovementSmoothingValue_18;
	// System.Single TMPro.Examples.CameraController::RotationSmoothingValue
	float ___RotationSmoothingValue_19;
	// System.Single TMPro.Examples.CameraController::MoveSensitivity
	float ___MoveSensitivity_20;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::currentVelocity
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___currentVelocity_21;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::desiredPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___desiredPosition_22;
	// System.Single TMPro.Examples.CameraController::mouseX
	float ___mouseX_23;
	// System.Single TMPro.Examples.CameraController::mouseY
	float ___mouseY_24;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::moveVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___moveVector_25;
	// System.Single TMPro.Examples.CameraController::mouseWheel
	float ___mouseWheel_26;

public:
	inline static int32_t get_offset_of_cameraTransform_4() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___cameraTransform_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_cameraTransform_4() const { return ___cameraTransform_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_cameraTransform_4() { return &___cameraTransform_4; }
	inline void set_cameraTransform_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___cameraTransform_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cameraTransform_4), (void*)value);
	}

	inline static int32_t get_offset_of_dummyTarget_5() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___dummyTarget_5)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_dummyTarget_5() const { return ___dummyTarget_5; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_dummyTarget_5() { return &___dummyTarget_5; }
	inline void set_dummyTarget_5(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___dummyTarget_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dummyTarget_5), (void*)value);
	}

	inline static int32_t get_offset_of_CameraTarget_6() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___CameraTarget_6)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_CameraTarget_6() const { return ___CameraTarget_6; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_CameraTarget_6() { return &___CameraTarget_6; }
	inline void set_CameraTarget_6(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___CameraTarget_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CameraTarget_6), (void*)value);
	}

	inline static int32_t get_offset_of_FollowDistance_7() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___FollowDistance_7)); }
	inline float get_FollowDistance_7() const { return ___FollowDistance_7; }
	inline float* get_address_of_FollowDistance_7() { return &___FollowDistance_7; }
	inline void set_FollowDistance_7(float value)
	{
		___FollowDistance_7 = value;
	}

	inline static int32_t get_offset_of_MaxFollowDistance_8() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___MaxFollowDistance_8)); }
	inline float get_MaxFollowDistance_8() const { return ___MaxFollowDistance_8; }
	inline float* get_address_of_MaxFollowDistance_8() { return &___MaxFollowDistance_8; }
	inline void set_MaxFollowDistance_8(float value)
	{
		___MaxFollowDistance_8 = value;
	}

	inline static int32_t get_offset_of_MinFollowDistance_9() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___MinFollowDistance_9)); }
	inline float get_MinFollowDistance_9() const { return ___MinFollowDistance_9; }
	inline float* get_address_of_MinFollowDistance_9() { return &___MinFollowDistance_9; }
	inline void set_MinFollowDistance_9(float value)
	{
		___MinFollowDistance_9 = value;
	}

	inline static int32_t get_offset_of_ElevationAngle_10() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___ElevationAngle_10)); }
	inline float get_ElevationAngle_10() const { return ___ElevationAngle_10; }
	inline float* get_address_of_ElevationAngle_10() { return &___ElevationAngle_10; }
	inline void set_ElevationAngle_10(float value)
	{
		___ElevationAngle_10 = value;
	}

	inline static int32_t get_offset_of_MaxElevationAngle_11() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___MaxElevationAngle_11)); }
	inline float get_MaxElevationAngle_11() const { return ___MaxElevationAngle_11; }
	inline float* get_address_of_MaxElevationAngle_11() { return &___MaxElevationAngle_11; }
	inline void set_MaxElevationAngle_11(float value)
	{
		___MaxElevationAngle_11 = value;
	}

	inline static int32_t get_offset_of_MinElevationAngle_12() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___MinElevationAngle_12)); }
	inline float get_MinElevationAngle_12() const { return ___MinElevationAngle_12; }
	inline float* get_address_of_MinElevationAngle_12() { return &___MinElevationAngle_12; }
	inline void set_MinElevationAngle_12(float value)
	{
		___MinElevationAngle_12 = value;
	}

	inline static int32_t get_offset_of_OrbitalAngle_13() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___OrbitalAngle_13)); }
	inline float get_OrbitalAngle_13() const { return ___OrbitalAngle_13; }
	inline float* get_address_of_OrbitalAngle_13() { return &___OrbitalAngle_13; }
	inline void set_OrbitalAngle_13(float value)
	{
		___OrbitalAngle_13 = value;
	}

	inline static int32_t get_offset_of_CameraMode_14() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___CameraMode_14)); }
	inline int32_t get_CameraMode_14() const { return ___CameraMode_14; }
	inline int32_t* get_address_of_CameraMode_14() { return &___CameraMode_14; }
	inline void set_CameraMode_14(int32_t value)
	{
		___CameraMode_14 = value;
	}

	inline static int32_t get_offset_of_MovementSmoothing_15() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___MovementSmoothing_15)); }
	inline bool get_MovementSmoothing_15() const { return ___MovementSmoothing_15; }
	inline bool* get_address_of_MovementSmoothing_15() { return &___MovementSmoothing_15; }
	inline void set_MovementSmoothing_15(bool value)
	{
		___MovementSmoothing_15 = value;
	}

	inline static int32_t get_offset_of_RotationSmoothing_16() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___RotationSmoothing_16)); }
	inline bool get_RotationSmoothing_16() const { return ___RotationSmoothing_16; }
	inline bool* get_address_of_RotationSmoothing_16() { return &___RotationSmoothing_16; }
	inline void set_RotationSmoothing_16(bool value)
	{
		___RotationSmoothing_16 = value;
	}

	inline static int32_t get_offset_of_previousSmoothing_17() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___previousSmoothing_17)); }
	inline bool get_previousSmoothing_17() const { return ___previousSmoothing_17; }
	inline bool* get_address_of_previousSmoothing_17() { return &___previousSmoothing_17; }
	inline void set_previousSmoothing_17(bool value)
	{
		___previousSmoothing_17 = value;
	}

	inline static int32_t get_offset_of_MovementSmoothingValue_18() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___MovementSmoothingValue_18)); }
	inline float get_MovementSmoothingValue_18() const { return ___MovementSmoothingValue_18; }
	inline float* get_address_of_MovementSmoothingValue_18() { return &___MovementSmoothingValue_18; }
	inline void set_MovementSmoothingValue_18(float value)
	{
		___MovementSmoothingValue_18 = value;
	}

	inline static int32_t get_offset_of_RotationSmoothingValue_19() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___RotationSmoothingValue_19)); }
	inline float get_RotationSmoothingValue_19() const { return ___RotationSmoothingValue_19; }
	inline float* get_address_of_RotationSmoothingValue_19() { return &___RotationSmoothingValue_19; }
	inline void set_RotationSmoothingValue_19(float value)
	{
		___RotationSmoothingValue_19 = value;
	}

	inline static int32_t get_offset_of_MoveSensitivity_20() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___MoveSensitivity_20)); }
	inline float get_MoveSensitivity_20() const { return ___MoveSensitivity_20; }
	inline float* get_address_of_MoveSensitivity_20() { return &___MoveSensitivity_20; }
	inline void set_MoveSensitivity_20(float value)
	{
		___MoveSensitivity_20 = value;
	}

	inline static int32_t get_offset_of_currentVelocity_21() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___currentVelocity_21)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_currentVelocity_21() const { return ___currentVelocity_21; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_currentVelocity_21() { return &___currentVelocity_21; }
	inline void set_currentVelocity_21(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___currentVelocity_21 = value;
	}

	inline static int32_t get_offset_of_desiredPosition_22() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___desiredPosition_22)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_desiredPosition_22() const { return ___desiredPosition_22; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_desiredPosition_22() { return &___desiredPosition_22; }
	inline void set_desiredPosition_22(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___desiredPosition_22 = value;
	}

	inline static int32_t get_offset_of_mouseX_23() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___mouseX_23)); }
	inline float get_mouseX_23() const { return ___mouseX_23; }
	inline float* get_address_of_mouseX_23() { return &___mouseX_23; }
	inline void set_mouseX_23(float value)
	{
		___mouseX_23 = value;
	}

	inline static int32_t get_offset_of_mouseY_24() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___mouseY_24)); }
	inline float get_mouseY_24() const { return ___mouseY_24; }
	inline float* get_address_of_mouseY_24() { return &___mouseY_24; }
	inline void set_mouseY_24(float value)
	{
		___mouseY_24 = value;
	}

	inline static int32_t get_offset_of_moveVector_25() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___moveVector_25)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_moveVector_25() const { return ___moveVector_25; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_moveVector_25() { return &___moveVector_25; }
	inline void set_moveVector_25(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___moveVector_25 = value;
	}

	inline static int32_t get_offset_of_mouseWheel_26() { return static_cast<int32_t>(offsetof(CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D, ___mouseWheel_26)); }
	inline float get_mouseWheel_26() const { return ___mouseWheel_26; }
	inline float* get_address_of_mouseWheel_26() { return &___mouseWheel_26; }
	inline void set_mouseWheel_26(float value)
	{
		___mouseWheel_26 = value;
	}
};


// ChatController
struct ChatController_tDC30FEF32E5CAE3B9A7502F307C6281C89F25061  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TMPro.TMP_InputField ChatController::TMP_ChatInput
	TMP_InputField_tD50B4F3E6822EAC2720FAED56B86E98183F61D59 * ___TMP_ChatInput_4;
	// TMPro.TMP_Text ChatController::TMP_ChatOutput
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___TMP_ChatOutput_5;
	// UnityEngine.UI.Scrollbar ChatController::ChatScrollbar
	Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * ___ChatScrollbar_6;

public:
	inline static int32_t get_offset_of_TMP_ChatInput_4() { return static_cast<int32_t>(offsetof(ChatController_tDC30FEF32E5CAE3B9A7502F307C6281C89F25061, ___TMP_ChatInput_4)); }
	inline TMP_InputField_tD50B4F3E6822EAC2720FAED56B86E98183F61D59 * get_TMP_ChatInput_4() const { return ___TMP_ChatInput_4; }
	inline TMP_InputField_tD50B4F3E6822EAC2720FAED56B86E98183F61D59 ** get_address_of_TMP_ChatInput_4() { return &___TMP_ChatInput_4; }
	inline void set_TMP_ChatInput_4(TMP_InputField_tD50B4F3E6822EAC2720FAED56B86E98183F61D59 * value)
	{
		___TMP_ChatInput_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TMP_ChatInput_4), (void*)value);
	}

	inline static int32_t get_offset_of_TMP_ChatOutput_5() { return static_cast<int32_t>(offsetof(ChatController_tDC30FEF32E5CAE3B9A7502F307C6281C89F25061, ___TMP_ChatOutput_5)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_TMP_ChatOutput_5() const { return ___TMP_ChatOutput_5; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_TMP_ChatOutput_5() { return &___TMP_ChatOutput_5; }
	inline void set_TMP_ChatOutput_5(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___TMP_ChatOutput_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TMP_ChatOutput_5), (void*)value);
	}

	inline static int32_t get_offset_of_ChatScrollbar_6() { return static_cast<int32_t>(offsetof(ChatController_tDC30FEF32E5CAE3B9A7502F307C6281C89F25061, ___ChatScrollbar_6)); }
	inline Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * get_ChatScrollbar_6() const { return ___ChatScrollbar_6; }
	inline Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 ** get_address_of_ChatScrollbar_6() { return &___ChatScrollbar_6; }
	inline void set_ChatScrollbar_6(Scrollbar_tECAC7FD315210FC856A3EC60AE1847A66AAF6C28 * value)
	{
		___ChatScrollbar_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChatScrollbar_6), (void*)value);
	}
};


// EnvMapAnimator
struct EnvMapAnimator_t7C30A094A3786710A896AD9A6FA46B13EFEB72FD  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector3 EnvMapAnimator::RotationSpeeds
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___RotationSpeeds_4;
	// TMPro.TMP_Text EnvMapAnimator::m_textMeshPro
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___m_textMeshPro_5;
	// UnityEngine.Material EnvMapAnimator::m_material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_material_6;

public:
	inline static int32_t get_offset_of_RotationSpeeds_4() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t7C30A094A3786710A896AD9A6FA46B13EFEB72FD, ___RotationSpeeds_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_RotationSpeeds_4() const { return ___RotationSpeeds_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_RotationSpeeds_4() { return &___RotationSpeeds_4; }
	inline void set_RotationSpeeds_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___RotationSpeeds_4 = value;
	}

	inline static int32_t get_offset_of_m_textMeshPro_5() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t7C30A094A3786710A896AD9A6FA46B13EFEB72FD, ___m_textMeshPro_5)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_m_textMeshPro_5() const { return ___m_textMeshPro_5; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_m_textMeshPro_5() { return &___m_textMeshPro_5; }
	inline void set_m_textMeshPro_5(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___m_textMeshPro_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_textMeshPro_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_material_6() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t7C30A094A3786710A896AD9A6FA46B13EFEB72FD, ___m_material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_material_6() const { return ___m_material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_material_6() { return &___m_material_6; }
	inline void set_m_material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_material_6), (void*)value);
	}
};


// LeanTester
struct LeanTester_t537C3FE41ED244252241E5B02F6B1BA6DA437297  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single LeanTester::timeout
	float ___timeout_4;

public:
	inline static int32_t get_offset_of_timeout_4() { return static_cast<int32_t>(offsetof(LeanTester_t537C3FE41ED244252241E5B02F6B1BA6DA437297, ___timeout_4)); }
	inline float get_timeout_4() const { return ___timeout_4; }
	inline float* get_address_of_timeout_4() { return &___timeout_4; }
	inline void set_timeout_4(float value)
	{
		___timeout_4 = value;
	}
};


// LeanTween
struct LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

struct LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields
{
public:
	// System.Boolean LeanTween::throwErrors
	bool ___throwErrors_4;
	// System.Single LeanTween::tau
	float ___tau_5;
	// System.Single LeanTween::PI_DIV2
	float ___PI_DIV2_6;
	// LTSeq[] LeanTween::sequences
	LTSeqU5BU5D_t5C740B5E327381CE2E6FD41B011EA9A72355FD51* ___sequences_7;
	// LTDescr[] LeanTween::tweens
	LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F* ___tweens_8;
	// System.Int32[] LeanTween::tweensFinished
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___tweensFinished_9;
	// System.Int32[] LeanTween::tweensFinishedIds
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___tweensFinishedIds_10;
	// LTDescr LeanTween::tween
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___tween_11;
	// System.Int32 LeanTween::tweenMaxSearch
	int32_t ___tweenMaxSearch_12;
	// System.Int32 LeanTween::maxTweens
	int32_t ___maxTweens_13;
	// System.Int32 LeanTween::maxSequences
	int32_t ___maxSequences_14;
	// System.Int32 LeanTween::frameRendered
	int32_t ___frameRendered_15;
	// UnityEngine.GameObject LeanTween::_tweenEmpty
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____tweenEmpty_16;
	// System.Single LeanTween::dtEstimated
	float ___dtEstimated_17;
	// System.Single LeanTween::dtManual
	float ___dtManual_18;
	// System.Single LeanTween::dtActual
	float ___dtActual_19;
	// System.UInt32 LeanTween::global_counter
	uint32_t ___global_counter_20;
	// System.Int32 LeanTween::i
	int32_t ___i_21;
	// System.Int32 LeanTween::j
	int32_t ___j_22;
	// System.Int32 LeanTween::finishedCnt
	int32_t ___finishedCnt_23;
	// UnityEngine.AnimationCurve LeanTween::punch
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___punch_24;
	// UnityEngine.AnimationCurve LeanTween::shake
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shake_25;
	// System.Int32 LeanTween::maxTweenReached
	int32_t ___maxTweenReached_26;
	// System.Int32 LeanTween::startSearch
	int32_t ___startSearch_27;
	// LTDescr LeanTween::d
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___d_28;
	// System.Action`1<LTEvent>[] LeanTween::eventListeners
	Action_1U5BU5D_t930658157BBD9409C35E85E5A34B23FC4EEF0772* ___eventListeners_29;
	// UnityEngine.GameObject[] LeanTween::goListeners
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___goListeners_30;
	// System.Int32 LeanTween::eventsMaxSearch
	int32_t ___eventsMaxSearch_31;
	// System.Int32 LeanTween::EVENTS_MAX
	int32_t ___EVENTS_MAX_32;
	// System.Int32 LeanTween::LISTENERS_MAX
	int32_t ___LISTENERS_MAX_33;
	// System.Int32 LeanTween::INIT_LISTENERS_MAX
	int32_t ___INIT_LISTENERS_MAX_34;

public:
	inline static int32_t get_offset_of_throwErrors_4() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___throwErrors_4)); }
	inline bool get_throwErrors_4() const { return ___throwErrors_4; }
	inline bool* get_address_of_throwErrors_4() { return &___throwErrors_4; }
	inline void set_throwErrors_4(bool value)
	{
		___throwErrors_4 = value;
	}

	inline static int32_t get_offset_of_tau_5() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___tau_5)); }
	inline float get_tau_5() const { return ___tau_5; }
	inline float* get_address_of_tau_5() { return &___tau_5; }
	inline void set_tau_5(float value)
	{
		___tau_5 = value;
	}

	inline static int32_t get_offset_of_PI_DIV2_6() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___PI_DIV2_6)); }
	inline float get_PI_DIV2_6() const { return ___PI_DIV2_6; }
	inline float* get_address_of_PI_DIV2_6() { return &___PI_DIV2_6; }
	inline void set_PI_DIV2_6(float value)
	{
		___PI_DIV2_6 = value;
	}

	inline static int32_t get_offset_of_sequences_7() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___sequences_7)); }
	inline LTSeqU5BU5D_t5C740B5E327381CE2E6FD41B011EA9A72355FD51* get_sequences_7() const { return ___sequences_7; }
	inline LTSeqU5BU5D_t5C740B5E327381CE2E6FD41B011EA9A72355FD51** get_address_of_sequences_7() { return &___sequences_7; }
	inline void set_sequences_7(LTSeqU5BU5D_t5C740B5E327381CE2E6FD41B011EA9A72355FD51* value)
	{
		___sequences_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sequences_7), (void*)value);
	}

	inline static int32_t get_offset_of_tweens_8() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___tweens_8)); }
	inline LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F* get_tweens_8() const { return ___tweens_8; }
	inline LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F** get_address_of_tweens_8() { return &___tweens_8; }
	inline void set_tweens_8(LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F* value)
	{
		___tweens_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tweens_8), (void*)value);
	}

	inline static int32_t get_offset_of_tweensFinished_9() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___tweensFinished_9)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_tweensFinished_9() const { return ___tweensFinished_9; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_tweensFinished_9() { return &___tweensFinished_9; }
	inline void set_tweensFinished_9(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___tweensFinished_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tweensFinished_9), (void*)value);
	}

	inline static int32_t get_offset_of_tweensFinishedIds_10() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___tweensFinishedIds_10)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_tweensFinishedIds_10() const { return ___tweensFinishedIds_10; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_tweensFinishedIds_10() { return &___tweensFinishedIds_10; }
	inline void set_tweensFinishedIds_10(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___tweensFinishedIds_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tweensFinishedIds_10), (void*)value);
	}

	inline static int32_t get_offset_of_tween_11() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___tween_11)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_tween_11() const { return ___tween_11; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_tween_11() { return &___tween_11; }
	inline void set_tween_11(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___tween_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tween_11), (void*)value);
	}

	inline static int32_t get_offset_of_tweenMaxSearch_12() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___tweenMaxSearch_12)); }
	inline int32_t get_tweenMaxSearch_12() const { return ___tweenMaxSearch_12; }
	inline int32_t* get_address_of_tweenMaxSearch_12() { return &___tweenMaxSearch_12; }
	inline void set_tweenMaxSearch_12(int32_t value)
	{
		___tweenMaxSearch_12 = value;
	}

	inline static int32_t get_offset_of_maxTweens_13() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___maxTweens_13)); }
	inline int32_t get_maxTweens_13() const { return ___maxTweens_13; }
	inline int32_t* get_address_of_maxTweens_13() { return &___maxTweens_13; }
	inline void set_maxTweens_13(int32_t value)
	{
		___maxTweens_13 = value;
	}

	inline static int32_t get_offset_of_maxSequences_14() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___maxSequences_14)); }
	inline int32_t get_maxSequences_14() const { return ___maxSequences_14; }
	inline int32_t* get_address_of_maxSequences_14() { return &___maxSequences_14; }
	inline void set_maxSequences_14(int32_t value)
	{
		___maxSequences_14 = value;
	}

	inline static int32_t get_offset_of_frameRendered_15() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___frameRendered_15)); }
	inline int32_t get_frameRendered_15() const { return ___frameRendered_15; }
	inline int32_t* get_address_of_frameRendered_15() { return &___frameRendered_15; }
	inline void set_frameRendered_15(int32_t value)
	{
		___frameRendered_15 = value;
	}

	inline static int32_t get_offset_of__tweenEmpty_16() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ____tweenEmpty_16)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__tweenEmpty_16() const { return ____tweenEmpty_16; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__tweenEmpty_16() { return &____tweenEmpty_16; }
	inline void set__tweenEmpty_16(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____tweenEmpty_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____tweenEmpty_16), (void*)value);
	}

	inline static int32_t get_offset_of_dtEstimated_17() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___dtEstimated_17)); }
	inline float get_dtEstimated_17() const { return ___dtEstimated_17; }
	inline float* get_address_of_dtEstimated_17() { return &___dtEstimated_17; }
	inline void set_dtEstimated_17(float value)
	{
		___dtEstimated_17 = value;
	}

	inline static int32_t get_offset_of_dtManual_18() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___dtManual_18)); }
	inline float get_dtManual_18() const { return ___dtManual_18; }
	inline float* get_address_of_dtManual_18() { return &___dtManual_18; }
	inline void set_dtManual_18(float value)
	{
		___dtManual_18 = value;
	}

	inline static int32_t get_offset_of_dtActual_19() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___dtActual_19)); }
	inline float get_dtActual_19() const { return ___dtActual_19; }
	inline float* get_address_of_dtActual_19() { return &___dtActual_19; }
	inline void set_dtActual_19(float value)
	{
		___dtActual_19 = value;
	}

	inline static int32_t get_offset_of_global_counter_20() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___global_counter_20)); }
	inline uint32_t get_global_counter_20() const { return ___global_counter_20; }
	inline uint32_t* get_address_of_global_counter_20() { return &___global_counter_20; }
	inline void set_global_counter_20(uint32_t value)
	{
		___global_counter_20 = value;
	}

	inline static int32_t get_offset_of_i_21() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___i_21)); }
	inline int32_t get_i_21() const { return ___i_21; }
	inline int32_t* get_address_of_i_21() { return &___i_21; }
	inline void set_i_21(int32_t value)
	{
		___i_21 = value;
	}

	inline static int32_t get_offset_of_j_22() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___j_22)); }
	inline int32_t get_j_22() const { return ___j_22; }
	inline int32_t* get_address_of_j_22() { return &___j_22; }
	inline void set_j_22(int32_t value)
	{
		___j_22 = value;
	}

	inline static int32_t get_offset_of_finishedCnt_23() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___finishedCnt_23)); }
	inline int32_t get_finishedCnt_23() const { return ___finishedCnt_23; }
	inline int32_t* get_address_of_finishedCnt_23() { return &___finishedCnt_23; }
	inline void set_finishedCnt_23(int32_t value)
	{
		___finishedCnt_23 = value;
	}

	inline static int32_t get_offset_of_punch_24() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___punch_24)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_punch_24() const { return ___punch_24; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_punch_24() { return &___punch_24; }
	inline void set_punch_24(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___punch_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___punch_24), (void*)value);
	}

	inline static int32_t get_offset_of_shake_25() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___shake_25)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_shake_25() const { return ___shake_25; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_shake_25() { return &___shake_25; }
	inline void set_shake_25(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___shake_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shake_25), (void*)value);
	}

	inline static int32_t get_offset_of_maxTweenReached_26() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___maxTweenReached_26)); }
	inline int32_t get_maxTweenReached_26() const { return ___maxTweenReached_26; }
	inline int32_t* get_address_of_maxTweenReached_26() { return &___maxTweenReached_26; }
	inline void set_maxTweenReached_26(int32_t value)
	{
		___maxTweenReached_26 = value;
	}

	inline static int32_t get_offset_of_startSearch_27() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___startSearch_27)); }
	inline int32_t get_startSearch_27() const { return ___startSearch_27; }
	inline int32_t* get_address_of_startSearch_27() { return &___startSearch_27; }
	inline void set_startSearch_27(int32_t value)
	{
		___startSearch_27 = value;
	}

	inline static int32_t get_offset_of_d_28() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___d_28)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_d_28() const { return ___d_28; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_d_28() { return &___d_28; }
	inline void set_d_28(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___d_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___d_28), (void*)value);
	}

	inline static int32_t get_offset_of_eventListeners_29() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___eventListeners_29)); }
	inline Action_1U5BU5D_t930658157BBD9409C35E85E5A34B23FC4EEF0772* get_eventListeners_29() const { return ___eventListeners_29; }
	inline Action_1U5BU5D_t930658157BBD9409C35E85E5A34B23FC4EEF0772** get_address_of_eventListeners_29() { return &___eventListeners_29; }
	inline void set_eventListeners_29(Action_1U5BU5D_t930658157BBD9409C35E85E5A34B23FC4EEF0772* value)
	{
		___eventListeners_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eventListeners_29), (void*)value);
	}

	inline static int32_t get_offset_of_goListeners_30() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___goListeners_30)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_goListeners_30() const { return ___goListeners_30; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_goListeners_30() { return &___goListeners_30; }
	inline void set_goListeners_30(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___goListeners_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___goListeners_30), (void*)value);
	}

	inline static int32_t get_offset_of_eventsMaxSearch_31() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___eventsMaxSearch_31)); }
	inline int32_t get_eventsMaxSearch_31() const { return ___eventsMaxSearch_31; }
	inline int32_t* get_address_of_eventsMaxSearch_31() { return &___eventsMaxSearch_31; }
	inline void set_eventsMaxSearch_31(int32_t value)
	{
		___eventsMaxSearch_31 = value;
	}

	inline static int32_t get_offset_of_EVENTS_MAX_32() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___EVENTS_MAX_32)); }
	inline int32_t get_EVENTS_MAX_32() const { return ___EVENTS_MAX_32; }
	inline int32_t* get_address_of_EVENTS_MAX_32() { return &___EVENTS_MAX_32; }
	inline void set_EVENTS_MAX_32(int32_t value)
	{
		___EVENTS_MAX_32 = value;
	}

	inline static int32_t get_offset_of_LISTENERS_MAX_33() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___LISTENERS_MAX_33)); }
	inline int32_t get_LISTENERS_MAX_33() const { return ___LISTENERS_MAX_33; }
	inline int32_t* get_address_of_LISTENERS_MAX_33() { return &___LISTENERS_MAX_33; }
	inline void set_LISTENERS_MAX_33(int32_t value)
	{
		___LISTENERS_MAX_33 = value;
	}

	inline static int32_t get_offset_of_INIT_LISTENERS_MAX_34() { return static_cast<int32_t>(offsetof(LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields, ___INIT_LISTENERS_MAX_34)); }
	inline int32_t get_INIT_LISTENERS_MAX_34() const { return ___INIT_LISTENERS_MAX_34; }
	inline int32_t* get_address_of_INIT_LISTENERS_MAX_34() { return &___INIT_LISTENERS_MAX_34; }
	inline void set_INIT_LISTENERS_MAX_34(int32_t value)
	{
		___INIT_LISTENERS_MAX_34 = value;
	}
};


// TMPro.Examples.ObjectSpin
struct ObjectSpin_t964A55502D58672749F39DBA3AD193FB11AA0AFA  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single TMPro.Examples.ObjectSpin::SpinSpeed
	float ___SpinSpeed_4;
	// System.Int32 TMPro.Examples.ObjectSpin::RotationRange
	int32_t ___RotationRange_5;
	// UnityEngine.Transform TMPro.Examples.ObjectSpin::m_transform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_transform_6;
	// System.Single TMPro.Examples.ObjectSpin::m_time
	float ___m_time_7;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_prevPOS
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_prevPOS_8;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_initial_Rotation
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_initial_Rotation_9;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_initial_Position
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_initial_Position_10;
	// UnityEngine.Color32 TMPro.Examples.ObjectSpin::m_lightColor
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___m_lightColor_11;
	// System.Int32 TMPro.Examples.ObjectSpin::frames
	int32_t ___frames_12;
	// TMPro.Examples.ObjectSpin/MotionType TMPro.Examples.ObjectSpin::Motion
	int32_t ___Motion_13;

public:
	inline static int32_t get_offset_of_SpinSpeed_4() { return static_cast<int32_t>(offsetof(ObjectSpin_t964A55502D58672749F39DBA3AD193FB11AA0AFA, ___SpinSpeed_4)); }
	inline float get_SpinSpeed_4() const { return ___SpinSpeed_4; }
	inline float* get_address_of_SpinSpeed_4() { return &___SpinSpeed_4; }
	inline void set_SpinSpeed_4(float value)
	{
		___SpinSpeed_4 = value;
	}

	inline static int32_t get_offset_of_RotationRange_5() { return static_cast<int32_t>(offsetof(ObjectSpin_t964A55502D58672749F39DBA3AD193FB11AA0AFA, ___RotationRange_5)); }
	inline int32_t get_RotationRange_5() const { return ___RotationRange_5; }
	inline int32_t* get_address_of_RotationRange_5() { return &___RotationRange_5; }
	inline void set_RotationRange_5(int32_t value)
	{
		___RotationRange_5 = value;
	}

	inline static int32_t get_offset_of_m_transform_6() { return static_cast<int32_t>(offsetof(ObjectSpin_t964A55502D58672749F39DBA3AD193FB11AA0AFA, ___m_transform_6)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_transform_6() const { return ___m_transform_6; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_transform_6() { return &___m_transform_6; }
	inline void set_m_transform_6(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_transform_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_transform_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_time_7() { return static_cast<int32_t>(offsetof(ObjectSpin_t964A55502D58672749F39DBA3AD193FB11AA0AFA, ___m_time_7)); }
	inline float get_m_time_7() const { return ___m_time_7; }
	inline float* get_address_of_m_time_7() { return &___m_time_7; }
	inline void set_m_time_7(float value)
	{
		___m_time_7 = value;
	}

	inline static int32_t get_offset_of_m_prevPOS_8() { return static_cast<int32_t>(offsetof(ObjectSpin_t964A55502D58672749F39DBA3AD193FB11AA0AFA, ___m_prevPOS_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_prevPOS_8() const { return ___m_prevPOS_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_prevPOS_8() { return &___m_prevPOS_8; }
	inline void set_m_prevPOS_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_prevPOS_8 = value;
	}

	inline static int32_t get_offset_of_m_initial_Rotation_9() { return static_cast<int32_t>(offsetof(ObjectSpin_t964A55502D58672749F39DBA3AD193FB11AA0AFA, ___m_initial_Rotation_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_initial_Rotation_9() const { return ___m_initial_Rotation_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_initial_Rotation_9() { return &___m_initial_Rotation_9; }
	inline void set_m_initial_Rotation_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_initial_Rotation_9 = value;
	}

	inline static int32_t get_offset_of_m_initial_Position_10() { return static_cast<int32_t>(offsetof(ObjectSpin_t964A55502D58672749F39DBA3AD193FB11AA0AFA, ___m_initial_Position_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_initial_Position_10() const { return ___m_initial_Position_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_initial_Position_10() { return &___m_initial_Position_10; }
	inline void set_m_initial_Position_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_initial_Position_10 = value;
	}

	inline static int32_t get_offset_of_m_lightColor_11() { return static_cast<int32_t>(offsetof(ObjectSpin_t964A55502D58672749F39DBA3AD193FB11AA0AFA, ___m_lightColor_11)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_m_lightColor_11() const { return ___m_lightColor_11; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_m_lightColor_11() { return &___m_lightColor_11; }
	inline void set_m_lightColor_11(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___m_lightColor_11 = value;
	}

	inline static int32_t get_offset_of_frames_12() { return static_cast<int32_t>(offsetof(ObjectSpin_t964A55502D58672749F39DBA3AD193FB11AA0AFA, ___frames_12)); }
	inline int32_t get_frames_12() const { return ___frames_12; }
	inline int32_t* get_address_of_frames_12() { return &___frames_12; }
	inline void set_frames_12(int32_t value)
	{
		___frames_12 = value;
	}

	inline static int32_t get_offset_of_Motion_13() { return static_cast<int32_t>(offsetof(ObjectSpin_t964A55502D58672749F39DBA3AD193FB11AA0AFA, ___Motion_13)); }
	inline int32_t get_Motion_13() const { return ___Motion_13; }
	inline int32_t* get_address_of_Motion_13() { return &___Motion_13; }
	inline void set_Motion_13(int32_t value)
	{
		___Motion_13 = value;
	}
};


// DentedPixel.LTExamples.PathBezier
struct PathBezier_t3A13E1125CFCBAC9592FD3E738EBD51E56258816  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform[] DentedPixel.LTExamples.PathBezier::trans
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ___trans_4;
	// LTBezierPath DentedPixel.LTExamples.PathBezier::cr
	LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48 * ___cr_5;
	// UnityEngine.GameObject DentedPixel.LTExamples.PathBezier::avatar1
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___avatar1_6;
	// System.Single DentedPixel.LTExamples.PathBezier::iter
	float ___iter_7;

public:
	inline static int32_t get_offset_of_trans_4() { return static_cast<int32_t>(offsetof(PathBezier_t3A13E1125CFCBAC9592FD3E738EBD51E56258816, ___trans_4)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get_trans_4() const { return ___trans_4; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of_trans_4() { return &___trans_4; }
	inline void set_trans_4(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		___trans_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trans_4), (void*)value);
	}

	inline static int32_t get_offset_of_cr_5() { return static_cast<int32_t>(offsetof(PathBezier_t3A13E1125CFCBAC9592FD3E738EBD51E56258816, ___cr_5)); }
	inline LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48 * get_cr_5() const { return ___cr_5; }
	inline LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48 ** get_address_of_cr_5() { return &___cr_5; }
	inline void set_cr_5(LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48 * value)
	{
		___cr_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cr_5), (void*)value);
	}

	inline static int32_t get_offset_of_avatar1_6() { return static_cast<int32_t>(offsetof(PathBezier_t3A13E1125CFCBAC9592FD3E738EBD51E56258816, ___avatar1_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_avatar1_6() const { return ___avatar1_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_avatar1_6() { return &___avatar1_6; }
	inline void set_avatar1_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___avatar1_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___avatar1_6), (void*)value);
	}

	inline static int32_t get_offset_of_iter_7() { return static_cast<int32_t>(offsetof(PathBezier_t3A13E1125CFCBAC9592FD3E738EBD51E56258816, ___iter_7)); }
	inline float get_iter_7() const { return ___iter_7; }
	inline float* get_address_of_iter_7() { return &___iter_7; }
	inline void set_iter_7(float value)
	{
		___iter_7 = value;
	}
};


// ScrollSnapBase
struct ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.RectTransform ScrollSnapBase::_screensContainer
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ____screensContainer_4;
	// System.Boolean ScrollSnapBase::_isVertical
	bool ____isVertical_5;
	// System.Int32 ScrollSnapBase::_screens
	int32_t ____screens_6;
	// System.Single ScrollSnapBase::_scrollStartPosition
	float ____scrollStartPosition_7;
	// System.Single ScrollSnapBase::_childSize
	float ____childSize_8;
	// System.Single ScrollSnapBase::_childPos
	float ____childPos_9;
	// System.Single ScrollSnapBase::_maskSize
	float ____maskSize_10;
	// UnityEngine.Vector2 ScrollSnapBase::_childAnchorPoint
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____childAnchorPoint_11;
	// UnityEngine.UI.ScrollRect ScrollSnapBase::_scroll_rect
	ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * ____scroll_rect_12;
	// UnityEngine.Vector3 ScrollSnapBase::_lerp_target
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____lerp_target_13;
	// System.Boolean ScrollSnapBase::_lerp
	bool ____lerp_14;
	// System.Boolean ScrollSnapBase::_pointerDown
	bool ____pointerDown_15;
	// System.Boolean ScrollSnapBase::_settled
	bool ____settled_16;
	// UnityEngine.Vector3 ScrollSnapBase::_startPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____startPosition_17;
	// System.Int32 ScrollSnapBase::_currentPage
	int32_t ____currentPage_18;
	// System.Int32 ScrollSnapBase::_previousPage
	int32_t ____previousPage_19;
	// System.Int32 ScrollSnapBase::_halfNoVisibleItems
	int32_t ____halfNoVisibleItems_20;
	// System.Int32 ScrollSnapBase::_bottomItem
	int32_t ____bottomItem_21;
	// System.Int32 ScrollSnapBase::_topItem
	int32_t ____topItem_22;
	// System.Int32 ScrollSnapBase::StartingScreen
	int32_t ___StartingScreen_23;
	// System.Single ScrollSnapBase::PageStep
	float ___PageStep_24;
	// UnityEngine.GameObject ScrollSnapBase::Pagination
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Pagination_25;
	// UnityEngine.GameObject ScrollSnapBase::NextButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___NextButton_26;
	// UnityEngine.GameObject ScrollSnapBase::PrevButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___PrevButton_27;
	// System.Single ScrollSnapBase::transitionSpeed
	float ___transitionSpeed_28;
	// System.Boolean ScrollSnapBase::UseFastSwipe
	bool ___UseFastSwipe_29;
	// System.Int32 ScrollSnapBase::FastSwipeThreshold
	int32_t ___FastSwipeThreshold_30;
	// System.Int32 ScrollSnapBase::SwipeVelocityThreshold
	int32_t ___SwipeVelocityThreshold_31;
	// UnityEngine.RectTransform ScrollSnapBase::MaskArea
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___MaskArea_32;
	// System.Single ScrollSnapBase::MaskBuffer
	float ___MaskBuffer_33;
	// System.Boolean ScrollSnapBase::UseParentTransform
	bool ___UseParentTransform_34;
	// UnityEngine.GameObject[] ScrollSnapBase::ChildObjects
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___ChildObjects_35;
	// System.Boolean ScrollSnapBase::ignoreTimeScale
	bool ___ignoreTimeScale_36;
	// ScrollSnapBase/SelectionChangeStartEvent ScrollSnapBase::m_OnSelectionChangeStartEvent
	SelectionChangeStartEvent_tE0687452817ACBB2DEECBBF8D2E996D0137F7FE0 * ___m_OnSelectionChangeStartEvent_37;
	// ScrollSnapBase/SelectionPageChangedEvent ScrollSnapBase::m_OnSelectionPageChangedEvent
	SelectionPageChangedEvent_tB33544091EB61DE45C9FF3A71E4DA084BDAA6DBC * ___m_OnSelectionPageChangedEvent_38;
	// ScrollSnapBase/SelectionChangeEndEvent ScrollSnapBase::m_OnSelectionChangeEndEvent
	SelectionChangeEndEvent_t61026CA54A4A81045E6FA5F1D8C79E556527C29E * ___m_OnSelectionChangeEndEvent_39;

public:
	inline static int32_t get_offset_of__screensContainer_4() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ____screensContainer_4)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get__screensContainer_4() const { return ____screensContainer_4; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of__screensContainer_4() { return &____screensContainer_4; }
	inline void set__screensContainer_4(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		____screensContainer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____screensContainer_4), (void*)value);
	}

	inline static int32_t get_offset_of__isVertical_5() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ____isVertical_5)); }
	inline bool get__isVertical_5() const { return ____isVertical_5; }
	inline bool* get_address_of__isVertical_5() { return &____isVertical_5; }
	inline void set__isVertical_5(bool value)
	{
		____isVertical_5 = value;
	}

	inline static int32_t get_offset_of__screens_6() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ____screens_6)); }
	inline int32_t get__screens_6() const { return ____screens_6; }
	inline int32_t* get_address_of__screens_6() { return &____screens_6; }
	inline void set__screens_6(int32_t value)
	{
		____screens_6 = value;
	}

	inline static int32_t get_offset_of__scrollStartPosition_7() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ____scrollStartPosition_7)); }
	inline float get__scrollStartPosition_7() const { return ____scrollStartPosition_7; }
	inline float* get_address_of__scrollStartPosition_7() { return &____scrollStartPosition_7; }
	inline void set__scrollStartPosition_7(float value)
	{
		____scrollStartPosition_7 = value;
	}

	inline static int32_t get_offset_of__childSize_8() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ____childSize_8)); }
	inline float get__childSize_8() const { return ____childSize_8; }
	inline float* get_address_of__childSize_8() { return &____childSize_8; }
	inline void set__childSize_8(float value)
	{
		____childSize_8 = value;
	}

	inline static int32_t get_offset_of__childPos_9() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ____childPos_9)); }
	inline float get__childPos_9() const { return ____childPos_9; }
	inline float* get_address_of__childPos_9() { return &____childPos_9; }
	inline void set__childPos_9(float value)
	{
		____childPos_9 = value;
	}

	inline static int32_t get_offset_of__maskSize_10() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ____maskSize_10)); }
	inline float get__maskSize_10() const { return ____maskSize_10; }
	inline float* get_address_of__maskSize_10() { return &____maskSize_10; }
	inline void set__maskSize_10(float value)
	{
		____maskSize_10 = value;
	}

	inline static int32_t get_offset_of__childAnchorPoint_11() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ____childAnchorPoint_11)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__childAnchorPoint_11() const { return ____childAnchorPoint_11; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__childAnchorPoint_11() { return &____childAnchorPoint_11; }
	inline void set__childAnchorPoint_11(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____childAnchorPoint_11 = value;
	}

	inline static int32_t get_offset_of__scroll_rect_12() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ____scroll_rect_12)); }
	inline ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * get__scroll_rect_12() const { return ____scroll_rect_12; }
	inline ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA ** get_address_of__scroll_rect_12() { return &____scroll_rect_12; }
	inline void set__scroll_rect_12(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * value)
	{
		____scroll_rect_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____scroll_rect_12), (void*)value);
	}

	inline static int32_t get_offset_of__lerp_target_13() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ____lerp_target_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__lerp_target_13() const { return ____lerp_target_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__lerp_target_13() { return &____lerp_target_13; }
	inline void set__lerp_target_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____lerp_target_13 = value;
	}

	inline static int32_t get_offset_of__lerp_14() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ____lerp_14)); }
	inline bool get__lerp_14() const { return ____lerp_14; }
	inline bool* get_address_of__lerp_14() { return &____lerp_14; }
	inline void set__lerp_14(bool value)
	{
		____lerp_14 = value;
	}

	inline static int32_t get_offset_of__pointerDown_15() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ____pointerDown_15)); }
	inline bool get__pointerDown_15() const { return ____pointerDown_15; }
	inline bool* get_address_of__pointerDown_15() { return &____pointerDown_15; }
	inline void set__pointerDown_15(bool value)
	{
		____pointerDown_15 = value;
	}

	inline static int32_t get_offset_of__settled_16() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ____settled_16)); }
	inline bool get__settled_16() const { return ____settled_16; }
	inline bool* get_address_of__settled_16() { return &____settled_16; }
	inline void set__settled_16(bool value)
	{
		____settled_16 = value;
	}

	inline static int32_t get_offset_of__startPosition_17() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ____startPosition_17)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__startPosition_17() const { return ____startPosition_17; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__startPosition_17() { return &____startPosition_17; }
	inline void set__startPosition_17(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____startPosition_17 = value;
	}

	inline static int32_t get_offset_of__currentPage_18() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ____currentPage_18)); }
	inline int32_t get__currentPage_18() const { return ____currentPage_18; }
	inline int32_t* get_address_of__currentPage_18() { return &____currentPage_18; }
	inline void set__currentPage_18(int32_t value)
	{
		____currentPage_18 = value;
	}

	inline static int32_t get_offset_of__previousPage_19() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ____previousPage_19)); }
	inline int32_t get__previousPage_19() const { return ____previousPage_19; }
	inline int32_t* get_address_of__previousPage_19() { return &____previousPage_19; }
	inline void set__previousPage_19(int32_t value)
	{
		____previousPage_19 = value;
	}

	inline static int32_t get_offset_of__halfNoVisibleItems_20() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ____halfNoVisibleItems_20)); }
	inline int32_t get__halfNoVisibleItems_20() const { return ____halfNoVisibleItems_20; }
	inline int32_t* get_address_of__halfNoVisibleItems_20() { return &____halfNoVisibleItems_20; }
	inline void set__halfNoVisibleItems_20(int32_t value)
	{
		____halfNoVisibleItems_20 = value;
	}

	inline static int32_t get_offset_of__bottomItem_21() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ____bottomItem_21)); }
	inline int32_t get__bottomItem_21() const { return ____bottomItem_21; }
	inline int32_t* get_address_of__bottomItem_21() { return &____bottomItem_21; }
	inline void set__bottomItem_21(int32_t value)
	{
		____bottomItem_21 = value;
	}

	inline static int32_t get_offset_of__topItem_22() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ____topItem_22)); }
	inline int32_t get__topItem_22() const { return ____topItem_22; }
	inline int32_t* get_address_of__topItem_22() { return &____topItem_22; }
	inline void set__topItem_22(int32_t value)
	{
		____topItem_22 = value;
	}

	inline static int32_t get_offset_of_StartingScreen_23() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ___StartingScreen_23)); }
	inline int32_t get_StartingScreen_23() const { return ___StartingScreen_23; }
	inline int32_t* get_address_of_StartingScreen_23() { return &___StartingScreen_23; }
	inline void set_StartingScreen_23(int32_t value)
	{
		___StartingScreen_23 = value;
	}

	inline static int32_t get_offset_of_PageStep_24() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ___PageStep_24)); }
	inline float get_PageStep_24() const { return ___PageStep_24; }
	inline float* get_address_of_PageStep_24() { return &___PageStep_24; }
	inline void set_PageStep_24(float value)
	{
		___PageStep_24 = value;
	}

	inline static int32_t get_offset_of_Pagination_25() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ___Pagination_25)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Pagination_25() const { return ___Pagination_25; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Pagination_25() { return &___Pagination_25; }
	inline void set_Pagination_25(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Pagination_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Pagination_25), (void*)value);
	}

	inline static int32_t get_offset_of_NextButton_26() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ___NextButton_26)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_NextButton_26() const { return ___NextButton_26; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_NextButton_26() { return &___NextButton_26; }
	inline void set_NextButton_26(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___NextButton_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___NextButton_26), (void*)value);
	}

	inline static int32_t get_offset_of_PrevButton_27() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ___PrevButton_27)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_PrevButton_27() const { return ___PrevButton_27; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_PrevButton_27() { return &___PrevButton_27; }
	inline void set_PrevButton_27(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___PrevButton_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PrevButton_27), (void*)value);
	}

	inline static int32_t get_offset_of_transitionSpeed_28() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ___transitionSpeed_28)); }
	inline float get_transitionSpeed_28() const { return ___transitionSpeed_28; }
	inline float* get_address_of_transitionSpeed_28() { return &___transitionSpeed_28; }
	inline void set_transitionSpeed_28(float value)
	{
		___transitionSpeed_28 = value;
	}

	inline static int32_t get_offset_of_UseFastSwipe_29() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ___UseFastSwipe_29)); }
	inline bool get_UseFastSwipe_29() const { return ___UseFastSwipe_29; }
	inline bool* get_address_of_UseFastSwipe_29() { return &___UseFastSwipe_29; }
	inline void set_UseFastSwipe_29(bool value)
	{
		___UseFastSwipe_29 = value;
	}

	inline static int32_t get_offset_of_FastSwipeThreshold_30() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ___FastSwipeThreshold_30)); }
	inline int32_t get_FastSwipeThreshold_30() const { return ___FastSwipeThreshold_30; }
	inline int32_t* get_address_of_FastSwipeThreshold_30() { return &___FastSwipeThreshold_30; }
	inline void set_FastSwipeThreshold_30(int32_t value)
	{
		___FastSwipeThreshold_30 = value;
	}

	inline static int32_t get_offset_of_SwipeVelocityThreshold_31() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ___SwipeVelocityThreshold_31)); }
	inline int32_t get_SwipeVelocityThreshold_31() const { return ___SwipeVelocityThreshold_31; }
	inline int32_t* get_address_of_SwipeVelocityThreshold_31() { return &___SwipeVelocityThreshold_31; }
	inline void set_SwipeVelocityThreshold_31(int32_t value)
	{
		___SwipeVelocityThreshold_31 = value;
	}

	inline static int32_t get_offset_of_MaskArea_32() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ___MaskArea_32)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_MaskArea_32() const { return ___MaskArea_32; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_MaskArea_32() { return &___MaskArea_32; }
	inline void set_MaskArea_32(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___MaskArea_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MaskArea_32), (void*)value);
	}

	inline static int32_t get_offset_of_MaskBuffer_33() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ___MaskBuffer_33)); }
	inline float get_MaskBuffer_33() const { return ___MaskBuffer_33; }
	inline float* get_address_of_MaskBuffer_33() { return &___MaskBuffer_33; }
	inline void set_MaskBuffer_33(float value)
	{
		___MaskBuffer_33 = value;
	}

	inline static int32_t get_offset_of_UseParentTransform_34() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ___UseParentTransform_34)); }
	inline bool get_UseParentTransform_34() const { return ___UseParentTransform_34; }
	inline bool* get_address_of_UseParentTransform_34() { return &___UseParentTransform_34; }
	inline void set_UseParentTransform_34(bool value)
	{
		___UseParentTransform_34 = value;
	}

	inline static int32_t get_offset_of_ChildObjects_35() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ___ChildObjects_35)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_ChildObjects_35() const { return ___ChildObjects_35; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_ChildObjects_35() { return &___ChildObjects_35; }
	inline void set_ChildObjects_35(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___ChildObjects_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChildObjects_35), (void*)value);
	}

	inline static int32_t get_offset_of_ignoreTimeScale_36() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ___ignoreTimeScale_36)); }
	inline bool get_ignoreTimeScale_36() const { return ___ignoreTimeScale_36; }
	inline bool* get_address_of_ignoreTimeScale_36() { return &___ignoreTimeScale_36; }
	inline void set_ignoreTimeScale_36(bool value)
	{
		___ignoreTimeScale_36 = value;
	}

	inline static int32_t get_offset_of_m_OnSelectionChangeStartEvent_37() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ___m_OnSelectionChangeStartEvent_37)); }
	inline SelectionChangeStartEvent_tE0687452817ACBB2DEECBBF8D2E996D0137F7FE0 * get_m_OnSelectionChangeStartEvent_37() const { return ___m_OnSelectionChangeStartEvent_37; }
	inline SelectionChangeStartEvent_tE0687452817ACBB2DEECBBF8D2E996D0137F7FE0 ** get_address_of_m_OnSelectionChangeStartEvent_37() { return &___m_OnSelectionChangeStartEvent_37; }
	inline void set_m_OnSelectionChangeStartEvent_37(SelectionChangeStartEvent_tE0687452817ACBB2DEECBBF8D2E996D0137F7FE0 * value)
	{
		___m_OnSelectionChangeStartEvent_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnSelectionChangeStartEvent_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnSelectionPageChangedEvent_38() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ___m_OnSelectionPageChangedEvent_38)); }
	inline SelectionPageChangedEvent_tB33544091EB61DE45C9FF3A71E4DA084BDAA6DBC * get_m_OnSelectionPageChangedEvent_38() const { return ___m_OnSelectionPageChangedEvent_38; }
	inline SelectionPageChangedEvent_tB33544091EB61DE45C9FF3A71E4DA084BDAA6DBC ** get_address_of_m_OnSelectionPageChangedEvent_38() { return &___m_OnSelectionPageChangedEvent_38; }
	inline void set_m_OnSelectionPageChangedEvent_38(SelectionPageChangedEvent_tB33544091EB61DE45C9FF3A71E4DA084BDAA6DBC * value)
	{
		___m_OnSelectionPageChangedEvent_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnSelectionPageChangedEvent_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnSelectionChangeEndEvent_39() { return static_cast<int32_t>(offsetof(ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345, ___m_OnSelectionChangeEndEvent_39)); }
	inline SelectionChangeEndEvent_t61026CA54A4A81045E6FA5F1D8C79E556527C29E * get_m_OnSelectionChangeEndEvent_39() const { return ___m_OnSelectionChangeEndEvent_39; }
	inline SelectionChangeEndEvent_t61026CA54A4A81045E6FA5F1D8C79E556527C29E ** get_address_of_m_OnSelectionChangeEndEvent_39() { return &___m_OnSelectionChangeEndEvent_39; }
	inline void set_m_OnSelectionChangeEndEvent_39(SelectionChangeEndEvent_t61026CA54A4A81045E6FA5F1D8C79E556527C29E * value)
	{
		___m_OnSelectionChangeEndEvent_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnSelectionChangeEndEvent_39), (void*)value);
	}
};


// TMPro.Examples.ShaderPropAnimator
struct ShaderPropAnimator_t44FB2882ECA0D2C780444EB96B9550F5D245CFB8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Renderer TMPro.Examples.ShaderPropAnimator::m_Renderer
	Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * ___m_Renderer_4;
	// UnityEngine.Material TMPro.Examples.ShaderPropAnimator::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_5;
	// UnityEngine.AnimationCurve TMPro.Examples.ShaderPropAnimator::GlowCurve
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___GlowCurve_6;
	// System.Single TMPro.Examples.ShaderPropAnimator::m_frame
	float ___m_frame_7;

public:
	inline static int32_t get_offset_of_m_Renderer_4() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t44FB2882ECA0D2C780444EB96B9550F5D245CFB8, ___m_Renderer_4)); }
	inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * get_m_Renderer_4() const { return ___m_Renderer_4; }
	inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C ** get_address_of_m_Renderer_4() { return &___m_Renderer_4; }
	inline void set_m_Renderer_4(Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * value)
	{
		___m_Renderer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Renderer_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Material_5() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t44FB2882ECA0D2C780444EB96B9550F5D245CFB8, ___m_Material_5)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_5() const { return ___m_Material_5; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_5() { return &___m_Material_5; }
	inline void set_m_Material_5(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_5), (void*)value);
	}

	inline static int32_t get_offset_of_GlowCurve_6() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t44FB2882ECA0D2C780444EB96B9550F5D245CFB8, ___GlowCurve_6)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_GlowCurve_6() const { return ___GlowCurve_6; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_GlowCurve_6() { return &___GlowCurve_6; }
	inline void set_GlowCurve_6(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___GlowCurve_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GlowCurve_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_frame_7() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t44FB2882ECA0D2C780444EB96B9550F5D245CFB8, ___m_frame_7)); }
	inline float get_m_frame_7() const { return ___m_frame_7; }
	inline float* get_address_of_m_frame_7() { return &___m_frame_7; }
	inline void set_m_frame_7(float value)
	{
		___m_frame_7 = value;
	}
};


// TMPro.Examples.SimpleScript
struct SimpleScript_t961D2D3FD565D738420646A1EF42244EDD972121  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TMPro.TextMeshPro TMPro.Examples.SimpleScript::m_textMeshPro
	TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * ___m_textMeshPro_4;
	// System.Single TMPro.Examples.SimpleScript::m_frame
	float ___m_frame_6;

public:
	inline static int32_t get_offset_of_m_textMeshPro_4() { return static_cast<int32_t>(offsetof(SimpleScript_t961D2D3FD565D738420646A1EF42244EDD972121, ___m_textMeshPro_4)); }
	inline TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * get_m_textMeshPro_4() const { return ___m_textMeshPro_4; }
	inline TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 ** get_address_of_m_textMeshPro_4() { return &___m_textMeshPro_4; }
	inline void set_m_textMeshPro_4(TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * value)
	{
		___m_textMeshPro_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_textMeshPro_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_frame_6() { return static_cast<int32_t>(offsetof(SimpleScript_t961D2D3FD565D738420646A1EF42244EDD972121, ___m_frame_6)); }
	inline float get_m_frame_6() const { return ___m_frame_6; }
	inline float* get_address_of_m_frame_6() { return &___m_frame_6; }
	inline void set_m_frame_6(float value)
	{
		___m_frame_6 = value;
	}
};


// TMPro.Examples.SkewTextExample
struct SkewTextExample_tA9F165D1BE969ED73FB86CA6C641386C22F1EE07  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TMPro.TMP_Text TMPro.Examples.SkewTextExample::m_TextComponent
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___m_TextComponent_4;
	// UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::VertexCurve
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___VertexCurve_5;
	// System.Single TMPro.Examples.SkewTextExample::CurveScale
	float ___CurveScale_6;
	// System.Single TMPro.Examples.SkewTextExample::ShearAmount
	float ___ShearAmount_7;

public:
	inline static int32_t get_offset_of_m_TextComponent_4() { return static_cast<int32_t>(offsetof(SkewTextExample_tA9F165D1BE969ED73FB86CA6C641386C22F1EE07, ___m_TextComponent_4)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_m_TextComponent_4() const { return ___m_TextComponent_4; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_m_TextComponent_4() { return &___m_TextComponent_4; }
	inline void set_m_TextComponent_4(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___m_TextComponent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextComponent_4), (void*)value);
	}

	inline static int32_t get_offset_of_VertexCurve_5() { return static_cast<int32_t>(offsetof(SkewTextExample_tA9F165D1BE969ED73FB86CA6C641386C22F1EE07, ___VertexCurve_5)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_VertexCurve_5() const { return ___VertexCurve_5; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_VertexCurve_5() { return &___VertexCurve_5; }
	inline void set_VertexCurve_5(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___VertexCurve_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___VertexCurve_5), (void*)value);
	}

	inline static int32_t get_offset_of_CurveScale_6() { return static_cast<int32_t>(offsetof(SkewTextExample_tA9F165D1BE969ED73FB86CA6C641386C22F1EE07, ___CurveScale_6)); }
	inline float get_CurveScale_6() const { return ___CurveScale_6; }
	inline float* get_address_of_CurveScale_6() { return &___CurveScale_6; }
	inline void set_CurveScale_6(float value)
	{
		___CurveScale_6 = value;
	}

	inline static int32_t get_offset_of_ShearAmount_7() { return static_cast<int32_t>(offsetof(SkewTextExample_tA9F165D1BE969ED73FB86CA6C641386C22F1EE07, ___ShearAmount_7)); }
	inline float get_ShearAmount_7() const { return ___ShearAmount_7; }
	inline float* get_address_of_ShearAmount_7() { return &___ShearAmount_7; }
	inline void set_ShearAmount_7(float value)
	{
		___ShearAmount_7 = value;
	}
};


// TMPro.Examples.TMP_ExampleScript_01
struct TMP_ExampleScript_01_t17145EFFB693D3CE0896E7B0777A7BCD07EF4F41  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TMPro.Examples.TMP_ExampleScript_01/objectType TMPro.Examples.TMP_ExampleScript_01::ObjectType
	int32_t ___ObjectType_4;
	// System.Boolean TMPro.Examples.TMP_ExampleScript_01::isStatic
	bool ___isStatic_5;
	// TMPro.TMP_Text TMPro.Examples.TMP_ExampleScript_01::m_text
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___m_text_6;
	// System.Int32 TMPro.Examples.TMP_ExampleScript_01::count
	int32_t ___count_8;

public:
	inline static int32_t get_offset_of_ObjectType_4() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t17145EFFB693D3CE0896E7B0777A7BCD07EF4F41, ___ObjectType_4)); }
	inline int32_t get_ObjectType_4() const { return ___ObjectType_4; }
	inline int32_t* get_address_of_ObjectType_4() { return &___ObjectType_4; }
	inline void set_ObjectType_4(int32_t value)
	{
		___ObjectType_4 = value;
	}

	inline static int32_t get_offset_of_isStatic_5() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t17145EFFB693D3CE0896E7B0777A7BCD07EF4F41, ___isStatic_5)); }
	inline bool get_isStatic_5() const { return ___isStatic_5; }
	inline bool* get_address_of_isStatic_5() { return &___isStatic_5; }
	inline void set_isStatic_5(bool value)
	{
		___isStatic_5 = value;
	}

	inline static int32_t get_offset_of_m_text_6() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t17145EFFB693D3CE0896E7B0777A7BCD07EF4F41, ___m_text_6)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_m_text_6() const { return ___m_text_6; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_m_text_6() { return &___m_text_6; }
	inline void set_m_text_6(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___m_text_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_text_6), (void*)value);
	}

	inline static int32_t get_offset_of_count_8() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t17145EFFB693D3CE0896E7B0777A7BCD07EF4F41, ___count_8)); }
	inline int32_t get_count_8() const { return ___count_8; }
	inline int32_t* get_address_of_count_8() { return &___count_8; }
	inline void set_count_8(int32_t value)
	{
		___count_8 = value;
	}
};


// TMPro.Examples.TMP_FrameRateCounter
struct TMP_FrameRateCounter_t925B27BFB86BA834FD1FD76E0F274B216E976E0F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single TMPro.Examples.TMP_FrameRateCounter::UpdateInterval
	float ___UpdateInterval_4;
	// System.Single TMPro.Examples.TMP_FrameRateCounter::m_LastInterval
	float ___m_LastInterval_5;
	// System.Int32 TMPro.Examples.TMP_FrameRateCounter::m_Frames
	int32_t ___m_Frames_6;
	// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_FrameRateCounter::AnchorPosition
	int32_t ___AnchorPosition_7;
	// System.String TMPro.Examples.TMP_FrameRateCounter::htmlColorTag
	String_t* ___htmlColorTag_8;
	// TMPro.TextMeshPro TMPro.Examples.TMP_FrameRateCounter::m_TextMeshPro
	TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * ___m_TextMeshPro_10;
	// UnityEngine.Transform TMPro.Examples.TMP_FrameRateCounter::m_frameCounter_transform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_frameCounter_transform_11;
	// UnityEngine.Camera TMPro.Examples.TMP_FrameRateCounter::m_camera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___m_camera_12;
	// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_FrameRateCounter::last_AnchorPosition
	int32_t ___last_AnchorPosition_13;

public:
	inline static int32_t get_offset_of_UpdateInterval_4() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t925B27BFB86BA834FD1FD76E0F274B216E976E0F, ___UpdateInterval_4)); }
	inline float get_UpdateInterval_4() const { return ___UpdateInterval_4; }
	inline float* get_address_of_UpdateInterval_4() { return &___UpdateInterval_4; }
	inline void set_UpdateInterval_4(float value)
	{
		___UpdateInterval_4 = value;
	}

	inline static int32_t get_offset_of_m_LastInterval_5() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t925B27BFB86BA834FD1FD76E0F274B216E976E0F, ___m_LastInterval_5)); }
	inline float get_m_LastInterval_5() const { return ___m_LastInterval_5; }
	inline float* get_address_of_m_LastInterval_5() { return &___m_LastInterval_5; }
	inline void set_m_LastInterval_5(float value)
	{
		___m_LastInterval_5 = value;
	}

	inline static int32_t get_offset_of_m_Frames_6() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t925B27BFB86BA834FD1FD76E0F274B216E976E0F, ___m_Frames_6)); }
	inline int32_t get_m_Frames_6() const { return ___m_Frames_6; }
	inline int32_t* get_address_of_m_Frames_6() { return &___m_Frames_6; }
	inline void set_m_Frames_6(int32_t value)
	{
		___m_Frames_6 = value;
	}

	inline static int32_t get_offset_of_AnchorPosition_7() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t925B27BFB86BA834FD1FD76E0F274B216E976E0F, ___AnchorPosition_7)); }
	inline int32_t get_AnchorPosition_7() const { return ___AnchorPosition_7; }
	inline int32_t* get_address_of_AnchorPosition_7() { return &___AnchorPosition_7; }
	inline void set_AnchorPosition_7(int32_t value)
	{
		___AnchorPosition_7 = value;
	}

	inline static int32_t get_offset_of_htmlColorTag_8() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t925B27BFB86BA834FD1FD76E0F274B216E976E0F, ___htmlColorTag_8)); }
	inline String_t* get_htmlColorTag_8() const { return ___htmlColorTag_8; }
	inline String_t** get_address_of_htmlColorTag_8() { return &___htmlColorTag_8; }
	inline void set_htmlColorTag_8(String_t* value)
	{
		___htmlColorTag_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___htmlColorTag_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextMeshPro_10() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t925B27BFB86BA834FD1FD76E0F274B216E976E0F, ___m_TextMeshPro_10)); }
	inline TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * get_m_TextMeshPro_10() const { return ___m_TextMeshPro_10; }
	inline TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 ** get_address_of_m_TextMeshPro_10() { return &___m_TextMeshPro_10; }
	inline void set_m_TextMeshPro_10(TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * value)
	{
		___m_TextMeshPro_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextMeshPro_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_frameCounter_transform_11() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t925B27BFB86BA834FD1FD76E0F274B216E976E0F, ___m_frameCounter_transform_11)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_frameCounter_transform_11() const { return ___m_frameCounter_transform_11; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_frameCounter_transform_11() { return &___m_frameCounter_transform_11; }
	inline void set_m_frameCounter_transform_11(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_frameCounter_transform_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_frameCounter_transform_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_camera_12() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t925B27BFB86BA834FD1FD76E0F274B216E976E0F, ___m_camera_12)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_m_camera_12() const { return ___m_camera_12; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_m_camera_12() { return &___m_camera_12; }
	inline void set_m_camera_12(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___m_camera_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_camera_12), (void*)value);
	}

	inline static int32_t get_offset_of_last_AnchorPosition_13() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t925B27BFB86BA834FD1FD76E0F274B216E976E0F, ___last_AnchorPosition_13)); }
	inline int32_t get_last_AnchorPosition_13() const { return ___last_AnchorPosition_13; }
	inline int32_t* get_address_of_last_AnchorPosition_13() { return &___last_AnchorPosition_13; }
	inline void set_last_AnchorPosition_13(int32_t value)
	{
		___last_AnchorPosition_13 = value;
	}
};


// TMPro.Examples.TMP_TextEventCheck
struct TMP_TextEventCheck_t0DC1E9006D6A9A20B2BDA99A8D3C6821647061C7  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TMPro.TMP_TextEventHandler TMPro.Examples.TMP_TextEventCheck::TextEventHandler
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553 * ___TextEventHandler_4;

public:
	inline static int32_t get_offset_of_TextEventHandler_4() { return static_cast<int32_t>(offsetof(TMP_TextEventCheck_t0DC1E9006D6A9A20B2BDA99A8D3C6821647061C7, ___TextEventHandler_4)); }
	inline TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553 * get_TextEventHandler_4() const { return ___TextEventHandler_4; }
	inline TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553 ** get_address_of_TextEventHandler_4() { return &___TextEventHandler_4; }
	inline void set_TextEventHandler_4(TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553 * value)
	{
		___TextEventHandler_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TextEventHandler_4), (void*)value);
	}
};


// TMPro.TMP_TextEventHandler
struct TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::m_OnCharacterSelection
	CharacterSelectionEvent_t3F24E19931D51E4616A11C4D690BC0B3D738D64F * ___m_OnCharacterSelection_4;
	// TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::m_OnSpriteSelection
	SpriteSelectionEvent_tAF7524A6DF6D4849E206726E3AC085BCB91B4618 * ___m_OnSpriteSelection_5;
	// TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::m_OnWordSelection
	WordSelectionEvent_t3FE09C7D7E726DC634003A28E2B3CB78B42AE6F8 * ___m_OnWordSelection_6;
	// TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::m_OnLineSelection
	LineSelectionEvent_tBD0FAC01BF9DE63FE60817F0B760E24E951C4712 * ___m_OnLineSelection_7;
	// TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::m_OnLinkSelection
	LinkSelectionEvent_tDDEFCE74540F682E52A3445C536D3F29A91F98AF * ___m_OnLinkSelection_8;
	// TMPro.TMP_Text TMPro.TMP_TextEventHandler::m_TextComponent
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___m_TextComponent_9;
	// UnityEngine.Camera TMPro.TMP_TextEventHandler::m_Camera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___m_Camera_10;
	// UnityEngine.Canvas TMPro.TMP_TextEventHandler::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_11;
	// System.Int32 TMPro.TMP_TextEventHandler::m_selectedLink
	int32_t ___m_selectedLink_12;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastCharIndex
	int32_t ___m_lastCharIndex_13;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastWordIndex
	int32_t ___m_lastWordIndex_14;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastLineIndex
	int32_t ___m_lastLineIndex_15;

public:
	inline static int32_t get_offset_of_m_OnCharacterSelection_4() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553, ___m_OnCharacterSelection_4)); }
	inline CharacterSelectionEvent_t3F24E19931D51E4616A11C4D690BC0B3D738D64F * get_m_OnCharacterSelection_4() const { return ___m_OnCharacterSelection_4; }
	inline CharacterSelectionEvent_t3F24E19931D51E4616A11C4D690BC0B3D738D64F ** get_address_of_m_OnCharacterSelection_4() { return &___m_OnCharacterSelection_4; }
	inline void set_m_OnCharacterSelection_4(CharacterSelectionEvent_t3F24E19931D51E4616A11C4D690BC0B3D738D64F * value)
	{
		___m_OnCharacterSelection_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCharacterSelection_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnSpriteSelection_5() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553, ___m_OnSpriteSelection_5)); }
	inline SpriteSelectionEvent_tAF7524A6DF6D4849E206726E3AC085BCB91B4618 * get_m_OnSpriteSelection_5() const { return ___m_OnSpriteSelection_5; }
	inline SpriteSelectionEvent_tAF7524A6DF6D4849E206726E3AC085BCB91B4618 ** get_address_of_m_OnSpriteSelection_5() { return &___m_OnSpriteSelection_5; }
	inline void set_m_OnSpriteSelection_5(SpriteSelectionEvent_tAF7524A6DF6D4849E206726E3AC085BCB91B4618 * value)
	{
		___m_OnSpriteSelection_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnSpriteSelection_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnWordSelection_6() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553, ___m_OnWordSelection_6)); }
	inline WordSelectionEvent_t3FE09C7D7E726DC634003A28E2B3CB78B42AE6F8 * get_m_OnWordSelection_6() const { return ___m_OnWordSelection_6; }
	inline WordSelectionEvent_t3FE09C7D7E726DC634003A28E2B3CB78B42AE6F8 ** get_address_of_m_OnWordSelection_6() { return &___m_OnWordSelection_6; }
	inline void set_m_OnWordSelection_6(WordSelectionEvent_t3FE09C7D7E726DC634003A28E2B3CB78B42AE6F8 * value)
	{
		___m_OnWordSelection_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnWordSelection_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnLineSelection_7() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553, ___m_OnLineSelection_7)); }
	inline LineSelectionEvent_tBD0FAC01BF9DE63FE60817F0B760E24E951C4712 * get_m_OnLineSelection_7() const { return ___m_OnLineSelection_7; }
	inline LineSelectionEvent_tBD0FAC01BF9DE63FE60817F0B760E24E951C4712 ** get_address_of_m_OnLineSelection_7() { return &___m_OnLineSelection_7; }
	inline void set_m_OnLineSelection_7(LineSelectionEvent_tBD0FAC01BF9DE63FE60817F0B760E24E951C4712 * value)
	{
		___m_OnLineSelection_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnLineSelection_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnLinkSelection_8() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553, ___m_OnLinkSelection_8)); }
	inline LinkSelectionEvent_tDDEFCE74540F682E52A3445C536D3F29A91F98AF * get_m_OnLinkSelection_8() const { return ___m_OnLinkSelection_8; }
	inline LinkSelectionEvent_tDDEFCE74540F682E52A3445C536D3F29A91F98AF ** get_address_of_m_OnLinkSelection_8() { return &___m_OnLinkSelection_8; }
	inline void set_m_OnLinkSelection_8(LinkSelectionEvent_tDDEFCE74540F682E52A3445C536D3F29A91F98AF * value)
	{
		___m_OnLinkSelection_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnLinkSelection_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextComponent_9() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553, ___m_TextComponent_9)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_m_TextComponent_9() const { return ___m_TextComponent_9; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_m_TextComponent_9() { return &___m_TextComponent_9; }
	inline void set_m_TextComponent_9(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___m_TextComponent_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextComponent_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_Camera_10() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553, ___m_Camera_10)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_m_Camera_10() const { return ___m_Camera_10; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_m_Camera_10() { return &___m_Camera_10; }
	inline void set_m_Camera_10(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___m_Camera_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Camera_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553, ___m_Canvas_11)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_selectedLink_12() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553, ___m_selectedLink_12)); }
	inline int32_t get_m_selectedLink_12() const { return ___m_selectedLink_12; }
	inline int32_t* get_address_of_m_selectedLink_12() { return &___m_selectedLink_12; }
	inline void set_m_selectedLink_12(int32_t value)
	{
		___m_selectedLink_12 = value;
	}

	inline static int32_t get_offset_of_m_lastCharIndex_13() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553, ___m_lastCharIndex_13)); }
	inline int32_t get_m_lastCharIndex_13() const { return ___m_lastCharIndex_13; }
	inline int32_t* get_address_of_m_lastCharIndex_13() { return &___m_lastCharIndex_13; }
	inline void set_m_lastCharIndex_13(int32_t value)
	{
		___m_lastCharIndex_13 = value;
	}

	inline static int32_t get_offset_of_m_lastWordIndex_14() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553, ___m_lastWordIndex_14)); }
	inline int32_t get_m_lastWordIndex_14() const { return ___m_lastWordIndex_14; }
	inline int32_t* get_address_of_m_lastWordIndex_14() { return &___m_lastWordIndex_14; }
	inline void set_m_lastWordIndex_14(int32_t value)
	{
		___m_lastWordIndex_14 = value;
	}

	inline static int32_t get_offset_of_m_lastLineIndex_15() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553, ___m_lastLineIndex_15)); }
	inline int32_t get_m_lastLineIndex_15() const { return ___m_lastLineIndex_15; }
	inline int32_t* get_address_of_m_lastLineIndex_15() { return &___m_lastLineIndex_15; }
	inline void set_m_lastLineIndex_15(int32_t value)
	{
		___m_lastLineIndex_15 = value;
	}
};


// TMPro.Examples.TMP_TextInfoDebugTool
struct TMP_TextInfoDebugTool_t9EE985ADF1BE5017A4827EDA15767080A2820E2F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// TMPro.Examples.TMP_TextSelector_A
struct TMP_TextSelector_A_t29DB4724707E8DC222ACD5D3134F801191B96B25  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TMPro.TextMeshPro TMPro.Examples.TMP_TextSelector_A::m_TextMeshPro
	TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * ___m_TextMeshPro_4;
	// UnityEngine.Camera TMPro.Examples.TMP_TextSelector_A::m_Camera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___m_Camera_5;
	// System.Boolean TMPro.Examples.TMP_TextSelector_A::m_isHoveringObject
	bool ___m_isHoveringObject_6;
	// System.Int32 TMPro.Examples.TMP_TextSelector_A::m_selectedLink
	int32_t ___m_selectedLink_7;
	// System.Int32 TMPro.Examples.TMP_TextSelector_A::m_lastCharIndex
	int32_t ___m_lastCharIndex_8;
	// System.Int32 TMPro.Examples.TMP_TextSelector_A::m_lastWordIndex
	int32_t ___m_lastWordIndex_9;

public:
	inline static int32_t get_offset_of_m_TextMeshPro_4() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t29DB4724707E8DC222ACD5D3134F801191B96B25, ___m_TextMeshPro_4)); }
	inline TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * get_m_TextMeshPro_4() const { return ___m_TextMeshPro_4; }
	inline TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 ** get_address_of_m_TextMeshPro_4() { return &___m_TextMeshPro_4; }
	inline void set_m_TextMeshPro_4(TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * value)
	{
		___m_TextMeshPro_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextMeshPro_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Camera_5() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t29DB4724707E8DC222ACD5D3134F801191B96B25, ___m_Camera_5)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_m_Camera_5() const { return ___m_Camera_5; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_m_Camera_5() { return &___m_Camera_5; }
	inline void set_m_Camera_5(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___m_Camera_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Camera_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_isHoveringObject_6() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t29DB4724707E8DC222ACD5D3134F801191B96B25, ___m_isHoveringObject_6)); }
	inline bool get_m_isHoveringObject_6() const { return ___m_isHoveringObject_6; }
	inline bool* get_address_of_m_isHoveringObject_6() { return &___m_isHoveringObject_6; }
	inline void set_m_isHoveringObject_6(bool value)
	{
		___m_isHoveringObject_6 = value;
	}

	inline static int32_t get_offset_of_m_selectedLink_7() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t29DB4724707E8DC222ACD5D3134F801191B96B25, ___m_selectedLink_7)); }
	inline int32_t get_m_selectedLink_7() const { return ___m_selectedLink_7; }
	inline int32_t* get_address_of_m_selectedLink_7() { return &___m_selectedLink_7; }
	inline void set_m_selectedLink_7(int32_t value)
	{
		___m_selectedLink_7 = value;
	}

	inline static int32_t get_offset_of_m_lastCharIndex_8() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t29DB4724707E8DC222ACD5D3134F801191B96B25, ___m_lastCharIndex_8)); }
	inline int32_t get_m_lastCharIndex_8() const { return ___m_lastCharIndex_8; }
	inline int32_t* get_address_of_m_lastCharIndex_8() { return &___m_lastCharIndex_8; }
	inline void set_m_lastCharIndex_8(int32_t value)
	{
		___m_lastCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_m_lastWordIndex_9() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t29DB4724707E8DC222ACD5D3134F801191B96B25, ___m_lastWordIndex_9)); }
	inline int32_t get_m_lastWordIndex_9() const { return ___m_lastWordIndex_9; }
	inline int32_t* get_address_of_m_lastWordIndex_9() { return &___m_lastWordIndex_9; }
	inline void set_m_lastWordIndex_9(int32_t value)
	{
		___m_lastWordIndex_9 = value;
	}
};


// TMPro.Examples.TMP_TextSelector_B
struct TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.RectTransform TMPro.Examples.TMP_TextSelector_B::TextPopup_Prefab_01
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___TextPopup_Prefab_01_4;
	// UnityEngine.RectTransform TMPro.Examples.TMP_TextSelector_B::m_TextPopup_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_TextPopup_RectTransform_5;
	// TMPro.TextMeshProUGUI TMPro.Examples.TMP_TextSelector_B::m_TextPopup_TMPComponent
	TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * ___m_TextPopup_TMPComponent_6;
	// TMPro.TextMeshProUGUI TMPro.Examples.TMP_TextSelector_B::m_TextMeshPro
	TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * ___m_TextMeshPro_9;
	// UnityEngine.Canvas TMPro.Examples.TMP_TextSelector_B::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_10;
	// UnityEngine.Camera TMPro.Examples.TMP_TextSelector_B::m_Camera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___m_Camera_11;
	// System.Boolean TMPro.Examples.TMP_TextSelector_B::isHoveringObject
	bool ___isHoveringObject_12;
	// System.Int32 TMPro.Examples.TMP_TextSelector_B::m_selectedWord
	int32_t ___m_selectedWord_13;
	// System.Int32 TMPro.Examples.TMP_TextSelector_B::m_selectedLink
	int32_t ___m_selectedLink_14;
	// System.Int32 TMPro.Examples.TMP_TextSelector_B::m_lastIndex
	int32_t ___m_lastIndex_15;
	// UnityEngine.Matrix4x4 TMPro.Examples.TMP_TextSelector_B::m_matrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___m_matrix_16;
	// TMPro.TMP_MeshInfo[] TMPro.Examples.TMP_TextSelector_B::m_cachedMeshInfoVertexData
	TMP_MeshInfoU5BU5D_t6C0A65D18C54B6FA681B2EB0676B83116FD03119* ___m_cachedMeshInfoVertexData_17;

public:
	inline static int32_t get_offset_of_TextPopup_Prefab_01_4() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF, ___TextPopup_Prefab_01_4)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_TextPopup_Prefab_01_4() const { return ___TextPopup_Prefab_01_4; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_TextPopup_Prefab_01_4() { return &___TextPopup_Prefab_01_4; }
	inline void set_TextPopup_Prefab_01_4(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___TextPopup_Prefab_01_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TextPopup_Prefab_01_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextPopup_RectTransform_5() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF, ___m_TextPopup_RectTransform_5)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_TextPopup_RectTransform_5() const { return ___m_TextPopup_RectTransform_5; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_TextPopup_RectTransform_5() { return &___m_TextPopup_RectTransform_5; }
	inline void set_m_TextPopup_RectTransform_5(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_TextPopup_RectTransform_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextPopup_RectTransform_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextPopup_TMPComponent_6() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF, ___m_TextPopup_TMPComponent_6)); }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * get_m_TextPopup_TMPComponent_6() const { return ___m_TextPopup_TMPComponent_6; }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 ** get_address_of_m_TextPopup_TMPComponent_6() { return &___m_TextPopup_TMPComponent_6; }
	inline void set_m_TextPopup_TMPComponent_6(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * value)
	{
		___m_TextPopup_TMPComponent_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextPopup_TMPComponent_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextMeshPro_9() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF, ___m_TextMeshPro_9)); }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * get_m_TextMeshPro_9() const { return ___m_TextMeshPro_9; }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 ** get_address_of_m_TextMeshPro_9() { return &___m_TextMeshPro_9; }
	inline void set_m_TextMeshPro_9(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * value)
	{
		___m_TextMeshPro_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextMeshPro_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_10() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF, ___m_Canvas_10)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_10() const { return ___m_Canvas_10; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_10() { return &___m_Canvas_10; }
	inline void set_m_Canvas_10(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_Camera_11() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF, ___m_Camera_11)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_m_Camera_11() const { return ___m_Camera_11; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_m_Camera_11() { return &___m_Camera_11; }
	inline void set_m_Camera_11(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___m_Camera_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Camera_11), (void*)value);
	}

	inline static int32_t get_offset_of_isHoveringObject_12() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF, ___isHoveringObject_12)); }
	inline bool get_isHoveringObject_12() const { return ___isHoveringObject_12; }
	inline bool* get_address_of_isHoveringObject_12() { return &___isHoveringObject_12; }
	inline void set_isHoveringObject_12(bool value)
	{
		___isHoveringObject_12 = value;
	}

	inline static int32_t get_offset_of_m_selectedWord_13() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF, ___m_selectedWord_13)); }
	inline int32_t get_m_selectedWord_13() const { return ___m_selectedWord_13; }
	inline int32_t* get_address_of_m_selectedWord_13() { return &___m_selectedWord_13; }
	inline void set_m_selectedWord_13(int32_t value)
	{
		___m_selectedWord_13 = value;
	}

	inline static int32_t get_offset_of_m_selectedLink_14() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF, ___m_selectedLink_14)); }
	inline int32_t get_m_selectedLink_14() const { return ___m_selectedLink_14; }
	inline int32_t* get_address_of_m_selectedLink_14() { return &___m_selectedLink_14; }
	inline void set_m_selectedLink_14(int32_t value)
	{
		___m_selectedLink_14 = value;
	}

	inline static int32_t get_offset_of_m_lastIndex_15() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF, ___m_lastIndex_15)); }
	inline int32_t get_m_lastIndex_15() const { return ___m_lastIndex_15; }
	inline int32_t* get_address_of_m_lastIndex_15() { return &___m_lastIndex_15; }
	inline void set_m_lastIndex_15(int32_t value)
	{
		___m_lastIndex_15 = value;
	}

	inline static int32_t get_offset_of_m_matrix_16() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF, ___m_matrix_16)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_m_matrix_16() const { return ___m_matrix_16; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_m_matrix_16() { return &___m_matrix_16; }
	inline void set_m_matrix_16(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___m_matrix_16 = value;
	}

	inline static int32_t get_offset_of_m_cachedMeshInfoVertexData_17() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF, ___m_cachedMeshInfoVertexData_17)); }
	inline TMP_MeshInfoU5BU5D_t6C0A65D18C54B6FA681B2EB0676B83116FD03119* get_m_cachedMeshInfoVertexData_17() const { return ___m_cachedMeshInfoVertexData_17; }
	inline TMP_MeshInfoU5BU5D_t6C0A65D18C54B6FA681B2EB0676B83116FD03119** get_address_of_m_cachedMeshInfoVertexData_17() { return &___m_cachedMeshInfoVertexData_17; }
	inline void set_m_cachedMeshInfoVertexData_17(TMP_MeshInfoU5BU5D_t6C0A65D18C54B6FA681B2EB0676B83116FD03119* value)
	{
		___m_cachedMeshInfoVertexData_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_cachedMeshInfoVertexData_17), (void*)value);
	}
};


// TMPro.Examples.TMP_UiFrameRateCounter
struct TMP_UiFrameRateCounter_tCC4ADB6034B0FDF19DCA0129D36ACE86CB159B08  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single TMPro.Examples.TMP_UiFrameRateCounter::UpdateInterval
	float ___UpdateInterval_4;
	// System.Single TMPro.Examples.TMP_UiFrameRateCounter::m_LastInterval
	float ___m_LastInterval_5;
	// System.Int32 TMPro.Examples.TMP_UiFrameRateCounter::m_Frames
	int32_t ___m_Frames_6;
	// TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_UiFrameRateCounter::AnchorPosition
	int32_t ___AnchorPosition_7;
	// System.String TMPro.Examples.TMP_UiFrameRateCounter::htmlColorTag
	String_t* ___htmlColorTag_8;
	// TMPro.TextMeshProUGUI TMPro.Examples.TMP_UiFrameRateCounter::m_TextMeshPro
	TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * ___m_TextMeshPro_10;
	// UnityEngine.RectTransform TMPro.Examples.TMP_UiFrameRateCounter::m_frameCounter_transform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_frameCounter_transform_11;
	// TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_UiFrameRateCounter::last_AnchorPosition
	int32_t ___last_AnchorPosition_12;

public:
	inline static int32_t get_offset_of_UpdateInterval_4() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_tCC4ADB6034B0FDF19DCA0129D36ACE86CB159B08, ___UpdateInterval_4)); }
	inline float get_UpdateInterval_4() const { return ___UpdateInterval_4; }
	inline float* get_address_of_UpdateInterval_4() { return &___UpdateInterval_4; }
	inline void set_UpdateInterval_4(float value)
	{
		___UpdateInterval_4 = value;
	}

	inline static int32_t get_offset_of_m_LastInterval_5() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_tCC4ADB6034B0FDF19DCA0129D36ACE86CB159B08, ___m_LastInterval_5)); }
	inline float get_m_LastInterval_5() const { return ___m_LastInterval_5; }
	inline float* get_address_of_m_LastInterval_5() { return &___m_LastInterval_5; }
	inline void set_m_LastInterval_5(float value)
	{
		___m_LastInterval_5 = value;
	}

	inline static int32_t get_offset_of_m_Frames_6() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_tCC4ADB6034B0FDF19DCA0129D36ACE86CB159B08, ___m_Frames_6)); }
	inline int32_t get_m_Frames_6() const { return ___m_Frames_6; }
	inline int32_t* get_address_of_m_Frames_6() { return &___m_Frames_6; }
	inline void set_m_Frames_6(int32_t value)
	{
		___m_Frames_6 = value;
	}

	inline static int32_t get_offset_of_AnchorPosition_7() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_tCC4ADB6034B0FDF19DCA0129D36ACE86CB159B08, ___AnchorPosition_7)); }
	inline int32_t get_AnchorPosition_7() const { return ___AnchorPosition_7; }
	inline int32_t* get_address_of_AnchorPosition_7() { return &___AnchorPosition_7; }
	inline void set_AnchorPosition_7(int32_t value)
	{
		___AnchorPosition_7 = value;
	}

	inline static int32_t get_offset_of_htmlColorTag_8() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_tCC4ADB6034B0FDF19DCA0129D36ACE86CB159B08, ___htmlColorTag_8)); }
	inline String_t* get_htmlColorTag_8() const { return ___htmlColorTag_8; }
	inline String_t** get_address_of_htmlColorTag_8() { return &___htmlColorTag_8; }
	inline void set_htmlColorTag_8(String_t* value)
	{
		___htmlColorTag_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___htmlColorTag_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextMeshPro_10() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_tCC4ADB6034B0FDF19DCA0129D36ACE86CB159B08, ___m_TextMeshPro_10)); }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * get_m_TextMeshPro_10() const { return ___m_TextMeshPro_10; }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 ** get_address_of_m_TextMeshPro_10() { return &___m_TextMeshPro_10; }
	inline void set_m_TextMeshPro_10(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * value)
	{
		___m_TextMeshPro_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextMeshPro_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_frameCounter_transform_11() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_tCC4ADB6034B0FDF19DCA0129D36ACE86CB159B08, ___m_frameCounter_transform_11)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_frameCounter_transform_11() const { return ___m_frameCounter_transform_11; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_frameCounter_transform_11() { return &___m_frameCounter_transform_11; }
	inline void set_m_frameCounter_transform_11(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_frameCounter_transform_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_frameCounter_transform_11), (void*)value);
	}

	inline static int32_t get_offset_of_last_AnchorPosition_12() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_tCC4ADB6034B0FDF19DCA0129D36ACE86CB159B08, ___last_AnchorPosition_12)); }
	inline int32_t get_last_AnchorPosition_12() const { return ___last_AnchorPosition_12; }
	inline int32_t* get_address_of_last_AnchorPosition_12() { return &___last_AnchorPosition_12; }
	inline void set_last_AnchorPosition_12(int32_t value)
	{
		___last_AnchorPosition_12 = value;
	}
};


// TMPro.Examples.TMPro_InstructionOverlay
struct TMPro_InstructionOverlay_t3D749999E580901C7F6C778105AEBC290AC141CB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions TMPro.Examples.TMPro_InstructionOverlay::AnchorPosition
	int32_t ___AnchorPosition_4;
	// TMPro.TextMeshPro TMPro.Examples.TMPro_InstructionOverlay::m_TextMeshPro
	TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * ___m_TextMeshPro_6;
	// TMPro.TextContainer TMPro.Examples.TMPro_InstructionOverlay::m_textContainer
	TextContainer_t397B1340CD69150F936048DB53D857135408D2A1 * ___m_textContainer_7;
	// UnityEngine.Transform TMPro.Examples.TMPro_InstructionOverlay::m_frameCounter_transform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_frameCounter_transform_8;
	// UnityEngine.Camera TMPro.Examples.TMPro_InstructionOverlay::m_camera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___m_camera_9;

public:
	inline static int32_t get_offset_of_AnchorPosition_4() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t3D749999E580901C7F6C778105AEBC290AC141CB, ___AnchorPosition_4)); }
	inline int32_t get_AnchorPosition_4() const { return ___AnchorPosition_4; }
	inline int32_t* get_address_of_AnchorPosition_4() { return &___AnchorPosition_4; }
	inline void set_AnchorPosition_4(int32_t value)
	{
		___AnchorPosition_4 = value;
	}

	inline static int32_t get_offset_of_m_TextMeshPro_6() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t3D749999E580901C7F6C778105AEBC290AC141CB, ___m_TextMeshPro_6)); }
	inline TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * get_m_TextMeshPro_6() const { return ___m_TextMeshPro_6; }
	inline TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 ** get_address_of_m_TextMeshPro_6() { return &___m_TextMeshPro_6; }
	inline void set_m_TextMeshPro_6(TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * value)
	{
		___m_TextMeshPro_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextMeshPro_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_textContainer_7() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t3D749999E580901C7F6C778105AEBC290AC141CB, ___m_textContainer_7)); }
	inline TextContainer_t397B1340CD69150F936048DB53D857135408D2A1 * get_m_textContainer_7() const { return ___m_textContainer_7; }
	inline TextContainer_t397B1340CD69150F936048DB53D857135408D2A1 ** get_address_of_m_textContainer_7() { return &___m_textContainer_7; }
	inline void set_m_textContainer_7(TextContainer_t397B1340CD69150F936048DB53D857135408D2A1 * value)
	{
		___m_textContainer_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_textContainer_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_frameCounter_transform_8() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t3D749999E580901C7F6C778105AEBC290AC141CB, ___m_frameCounter_transform_8)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_frameCounter_transform_8() const { return ___m_frameCounter_transform_8; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_frameCounter_transform_8() { return &___m_frameCounter_transform_8; }
	inline void set_m_frameCounter_transform_8(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_frameCounter_transform_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_frameCounter_transform_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_camera_9() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t3D749999E580901C7F6C778105AEBC290AC141CB, ___m_camera_9)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_m_camera_9() const { return ___m_camera_9; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_m_camera_9() { return &___m_camera_9; }
	inline void set_m_camera_9(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___m_camera_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_camera_9), (void*)value);
	}
};


// TMPro.Examples.TeleType
struct TeleType_t92B5ED4FE818F912A562F085CE035F6B42834934  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String TMPro.Examples.TeleType::label01
	String_t* ___label01_4;
	// System.String TMPro.Examples.TeleType::label02
	String_t* ___label02_5;
	// TMPro.TMP_Text TMPro.Examples.TeleType::m_textMeshPro
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___m_textMeshPro_6;

public:
	inline static int32_t get_offset_of_label01_4() { return static_cast<int32_t>(offsetof(TeleType_t92B5ED4FE818F912A562F085CE035F6B42834934, ___label01_4)); }
	inline String_t* get_label01_4() const { return ___label01_4; }
	inline String_t** get_address_of_label01_4() { return &___label01_4; }
	inline void set_label01_4(String_t* value)
	{
		___label01_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___label01_4), (void*)value);
	}

	inline static int32_t get_offset_of_label02_5() { return static_cast<int32_t>(offsetof(TeleType_t92B5ED4FE818F912A562F085CE035F6B42834934, ___label02_5)); }
	inline String_t* get_label02_5() const { return ___label02_5; }
	inline String_t** get_address_of_label02_5() { return &___label02_5; }
	inline void set_label02_5(String_t* value)
	{
		___label02_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___label02_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_6() { return static_cast<int32_t>(offsetof(TeleType_t92B5ED4FE818F912A562F085CE035F6B42834934, ___m_textMeshPro_6)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_m_textMeshPro_6() const { return ___m_textMeshPro_6; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_m_textMeshPro_6() { return &___m_textMeshPro_6; }
	inline void set_m_textMeshPro_6(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___m_textMeshPro_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_textMeshPro_6), (void*)value);
	}
};


// DentedPixel.LTExamples.TestingUnitTests
struct TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cube1
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cube1_4;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cube2
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cube2_5;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cube3
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cube3_6;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cube4
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cube4_7;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cubeAlpha1
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeAlpha1_8;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cubeAlpha2
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___cubeAlpha2_9;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests::eventGameObjectWasCalled
	bool ___eventGameObjectWasCalled_10;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests::eventGeneralWasCalled
	bool ___eventGeneralWasCalled_11;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests::lt1Id
	int32_t ___lt1Id_12;
	// LTDescr DentedPixel.LTExamples.TestingUnitTests::lt2
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___lt2_13;
	// LTDescr DentedPixel.LTExamples.TestingUnitTests::lt3
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___lt3_14;
	// LTDescr DentedPixel.LTExamples.TestingUnitTests::lt4
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * ___lt4_15;
	// LTDescr[] DentedPixel.LTExamples.TestingUnitTests::groupTweens
	LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F* ___groupTweens_16;
	// UnityEngine.GameObject[] DentedPixel.LTExamples.TestingUnitTests::groupGOs
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___groupGOs_17;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests::groupTweensCnt
	int32_t ___groupTweensCnt_18;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests::rotateRepeat
	int32_t ___rotateRepeat_19;
	// System.Int32 DentedPixel.LTExamples.TestingUnitTests::rotateRepeatAngle
	int32_t ___rotateRepeatAngle_20;
	// UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::boxNoCollider
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___boxNoCollider_21;
	// System.Single DentedPixel.LTExamples.TestingUnitTests::timeElapsedNormalTimeScale
	float ___timeElapsedNormalTimeScale_22;
	// System.Single DentedPixel.LTExamples.TestingUnitTests::timeElapsedIgnoreTimeScale
	float ___timeElapsedIgnoreTimeScale_23;
	// System.Boolean DentedPixel.LTExamples.TestingUnitTests::pauseTweenDidFinish
	bool ___pauseTweenDidFinish_24;

public:
	inline static int32_t get_offset_of_cube1_4() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___cube1_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cube1_4() const { return ___cube1_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cube1_4() { return &___cube1_4; }
	inline void set_cube1_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cube1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cube1_4), (void*)value);
	}

	inline static int32_t get_offset_of_cube2_5() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___cube2_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cube2_5() const { return ___cube2_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cube2_5() { return &___cube2_5; }
	inline void set_cube2_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cube2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cube2_5), (void*)value);
	}

	inline static int32_t get_offset_of_cube3_6() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___cube3_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cube3_6() const { return ___cube3_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cube3_6() { return &___cube3_6; }
	inline void set_cube3_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cube3_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cube3_6), (void*)value);
	}

	inline static int32_t get_offset_of_cube4_7() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___cube4_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cube4_7() const { return ___cube4_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cube4_7() { return &___cube4_7; }
	inline void set_cube4_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cube4_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cube4_7), (void*)value);
	}

	inline static int32_t get_offset_of_cubeAlpha1_8() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___cubeAlpha1_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeAlpha1_8() const { return ___cubeAlpha1_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeAlpha1_8() { return &___cubeAlpha1_8; }
	inline void set_cubeAlpha1_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeAlpha1_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeAlpha1_8), (void*)value);
	}

	inline static int32_t get_offset_of_cubeAlpha2_9() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___cubeAlpha2_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_cubeAlpha2_9() const { return ___cubeAlpha2_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_cubeAlpha2_9() { return &___cubeAlpha2_9; }
	inline void set_cubeAlpha2_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___cubeAlpha2_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cubeAlpha2_9), (void*)value);
	}

	inline static int32_t get_offset_of_eventGameObjectWasCalled_10() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___eventGameObjectWasCalled_10)); }
	inline bool get_eventGameObjectWasCalled_10() const { return ___eventGameObjectWasCalled_10; }
	inline bool* get_address_of_eventGameObjectWasCalled_10() { return &___eventGameObjectWasCalled_10; }
	inline void set_eventGameObjectWasCalled_10(bool value)
	{
		___eventGameObjectWasCalled_10 = value;
	}

	inline static int32_t get_offset_of_eventGeneralWasCalled_11() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___eventGeneralWasCalled_11)); }
	inline bool get_eventGeneralWasCalled_11() const { return ___eventGeneralWasCalled_11; }
	inline bool* get_address_of_eventGeneralWasCalled_11() { return &___eventGeneralWasCalled_11; }
	inline void set_eventGeneralWasCalled_11(bool value)
	{
		___eventGeneralWasCalled_11 = value;
	}

	inline static int32_t get_offset_of_lt1Id_12() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___lt1Id_12)); }
	inline int32_t get_lt1Id_12() const { return ___lt1Id_12; }
	inline int32_t* get_address_of_lt1Id_12() { return &___lt1Id_12; }
	inline void set_lt1Id_12(int32_t value)
	{
		___lt1Id_12 = value;
	}

	inline static int32_t get_offset_of_lt2_13() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___lt2_13)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_lt2_13() const { return ___lt2_13; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_lt2_13() { return &___lt2_13; }
	inline void set_lt2_13(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___lt2_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lt2_13), (void*)value);
	}

	inline static int32_t get_offset_of_lt3_14() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___lt3_14)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_lt3_14() const { return ___lt3_14; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_lt3_14() { return &___lt3_14; }
	inline void set_lt3_14(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___lt3_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lt3_14), (void*)value);
	}

	inline static int32_t get_offset_of_lt4_15() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___lt4_15)); }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * get_lt4_15() const { return ___lt4_15; }
	inline LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F ** get_address_of_lt4_15() { return &___lt4_15; }
	inline void set_lt4_15(LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F * value)
	{
		___lt4_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lt4_15), (void*)value);
	}

	inline static int32_t get_offset_of_groupTweens_16() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___groupTweens_16)); }
	inline LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F* get_groupTweens_16() const { return ___groupTweens_16; }
	inline LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F** get_address_of_groupTweens_16() { return &___groupTweens_16; }
	inline void set_groupTweens_16(LTDescrU5BU5D_t731554F94C09A2A31CB19EA8663E934537DF797F* value)
	{
		___groupTweens_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___groupTweens_16), (void*)value);
	}

	inline static int32_t get_offset_of_groupGOs_17() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___groupGOs_17)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_groupGOs_17() const { return ___groupGOs_17; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_groupGOs_17() { return &___groupGOs_17; }
	inline void set_groupGOs_17(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___groupGOs_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___groupGOs_17), (void*)value);
	}

	inline static int32_t get_offset_of_groupTweensCnt_18() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___groupTweensCnt_18)); }
	inline int32_t get_groupTweensCnt_18() const { return ___groupTweensCnt_18; }
	inline int32_t* get_address_of_groupTweensCnt_18() { return &___groupTweensCnt_18; }
	inline void set_groupTweensCnt_18(int32_t value)
	{
		___groupTweensCnt_18 = value;
	}

	inline static int32_t get_offset_of_rotateRepeat_19() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___rotateRepeat_19)); }
	inline int32_t get_rotateRepeat_19() const { return ___rotateRepeat_19; }
	inline int32_t* get_address_of_rotateRepeat_19() { return &___rotateRepeat_19; }
	inline void set_rotateRepeat_19(int32_t value)
	{
		___rotateRepeat_19 = value;
	}

	inline static int32_t get_offset_of_rotateRepeatAngle_20() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___rotateRepeatAngle_20)); }
	inline int32_t get_rotateRepeatAngle_20() const { return ___rotateRepeatAngle_20; }
	inline int32_t* get_address_of_rotateRepeatAngle_20() { return &___rotateRepeatAngle_20; }
	inline void set_rotateRepeatAngle_20(int32_t value)
	{
		___rotateRepeatAngle_20 = value;
	}

	inline static int32_t get_offset_of_boxNoCollider_21() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___boxNoCollider_21)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_boxNoCollider_21() const { return ___boxNoCollider_21; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_boxNoCollider_21() { return &___boxNoCollider_21; }
	inline void set_boxNoCollider_21(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___boxNoCollider_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___boxNoCollider_21), (void*)value);
	}

	inline static int32_t get_offset_of_timeElapsedNormalTimeScale_22() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___timeElapsedNormalTimeScale_22)); }
	inline float get_timeElapsedNormalTimeScale_22() const { return ___timeElapsedNormalTimeScale_22; }
	inline float* get_address_of_timeElapsedNormalTimeScale_22() { return &___timeElapsedNormalTimeScale_22; }
	inline void set_timeElapsedNormalTimeScale_22(float value)
	{
		___timeElapsedNormalTimeScale_22 = value;
	}

	inline static int32_t get_offset_of_timeElapsedIgnoreTimeScale_23() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___timeElapsedIgnoreTimeScale_23)); }
	inline float get_timeElapsedIgnoreTimeScale_23() const { return ___timeElapsedIgnoreTimeScale_23; }
	inline float* get_address_of_timeElapsedIgnoreTimeScale_23() { return &___timeElapsedIgnoreTimeScale_23; }
	inline void set_timeElapsedIgnoreTimeScale_23(float value)
	{
		___timeElapsedIgnoreTimeScale_23 = value;
	}

	inline static int32_t get_offset_of_pauseTweenDidFinish_24() { return static_cast<int32_t>(offsetof(TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757, ___pauseTweenDidFinish_24)); }
	inline bool get_pauseTweenDidFinish_24() const { return ___pauseTweenDidFinish_24; }
	inline bool* get_address_of_pauseTweenDidFinish_24() { return &___pauseTweenDidFinish_24; }
	inline void set_pauseTweenDidFinish_24(bool value)
	{
		___pauseTweenDidFinish_24 = value;
	}
};


// TMPro.Examples.TextConsoleSimulator
struct TextConsoleSimulator_t82904A7668D46B20E576B30E4CE367C0B66C5289  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator::m_TextComponent
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___m_TextComponent_4;
	// System.Boolean TMPro.Examples.TextConsoleSimulator::hasTextChanged
	bool ___hasTextChanged_5;

public:
	inline static int32_t get_offset_of_m_TextComponent_4() { return static_cast<int32_t>(offsetof(TextConsoleSimulator_t82904A7668D46B20E576B30E4CE367C0B66C5289, ___m_TextComponent_4)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_m_TextComponent_4() const { return ___m_TextComponent_4; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_m_TextComponent_4() { return &___m_TextComponent_4; }
	inline void set_m_TextComponent_4(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___m_TextComponent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextComponent_4), (void*)value);
	}

	inline static int32_t get_offset_of_hasTextChanged_5() { return static_cast<int32_t>(offsetof(TextConsoleSimulator_t82904A7668D46B20E576B30E4CE367C0B66C5289, ___hasTextChanged_5)); }
	inline bool get_hasTextChanged_5() const { return ___hasTextChanged_5; }
	inline bool* get_address_of_hasTextChanged_5() { return &___hasTextChanged_5; }
	inline void set_hasTextChanged_5(bool value)
	{
		___hasTextChanged_5 = value;
	}
};


// TMPro.Examples.TextMeshProFloatingText
struct TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Font TMPro.Examples.TextMeshProFloatingText::TheFont
	Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 * ___TheFont_4;
	// UnityEngine.GameObject TMPro.Examples.TextMeshProFloatingText::m_floatingText
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_floatingText_5;
	// TMPro.TextMeshPro TMPro.Examples.TextMeshProFloatingText::m_textMeshPro
	TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * ___m_textMeshPro_6;
	// UnityEngine.TextMesh TMPro.Examples.TextMeshProFloatingText::m_textMesh
	TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * ___m_textMesh_7;
	// UnityEngine.Transform TMPro.Examples.TextMeshProFloatingText::m_transform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_transform_8;
	// UnityEngine.Transform TMPro.Examples.TextMeshProFloatingText::m_floatingText_Transform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_floatingText_Transform_9;
	// UnityEngine.Transform TMPro.Examples.TextMeshProFloatingText::m_cameraTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_cameraTransform_10;
	// UnityEngine.Vector3 TMPro.Examples.TextMeshProFloatingText::lastPOS
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lastPOS_11;
	// UnityEngine.Quaternion TMPro.Examples.TextMeshProFloatingText::lastRotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___lastRotation_12;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText::SpawnType
	int32_t ___SpawnType_13;

public:
	inline static int32_t get_offset_of_TheFont_4() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200, ___TheFont_4)); }
	inline Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 * get_TheFont_4() const { return ___TheFont_4; }
	inline Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 ** get_address_of_TheFont_4() { return &___TheFont_4; }
	inline void set_TheFont_4(Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 * value)
	{
		___TheFont_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TheFont_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_floatingText_5() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200, ___m_floatingText_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_floatingText_5() const { return ___m_floatingText_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_floatingText_5() { return &___m_floatingText_5; }
	inline void set_m_floatingText_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_floatingText_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_floatingText_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_6() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200, ___m_textMeshPro_6)); }
	inline TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * get_m_textMeshPro_6() const { return ___m_textMeshPro_6; }
	inline TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 ** get_address_of_m_textMeshPro_6() { return &___m_textMeshPro_6; }
	inline void set_m_textMeshPro_6(TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * value)
	{
		___m_textMeshPro_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_textMeshPro_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_textMesh_7() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200, ___m_textMesh_7)); }
	inline TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * get_m_textMesh_7() const { return ___m_textMesh_7; }
	inline TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 ** get_address_of_m_textMesh_7() { return &___m_textMesh_7; }
	inline void set_m_textMesh_7(TextMesh_t830C2452CE189A0D35CD9ED26B6B74D506B01273 * value)
	{
		___m_textMesh_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_textMesh_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_transform_8() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200, ___m_transform_8)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_transform_8() const { return ___m_transform_8; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_transform_8() { return &___m_transform_8; }
	inline void set_m_transform_8(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_transform_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_transform_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_floatingText_Transform_9() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200, ___m_floatingText_Transform_9)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_floatingText_Transform_9() const { return ___m_floatingText_Transform_9; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_floatingText_Transform_9() { return &___m_floatingText_Transform_9; }
	inline void set_m_floatingText_Transform_9(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_floatingText_Transform_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_floatingText_Transform_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_cameraTransform_10() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200, ___m_cameraTransform_10)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_cameraTransform_10() const { return ___m_cameraTransform_10; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_cameraTransform_10() { return &___m_cameraTransform_10; }
	inline void set_m_cameraTransform_10(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_cameraTransform_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_cameraTransform_10), (void*)value);
	}

	inline static int32_t get_offset_of_lastPOS_11() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200, ___lastPOS_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_lastPOS_11() const { return ___lastPOS_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_lastPOS_11() { return &___lastPOS_11; }
	inline void set_lastPOS_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___lastPOS_11 = value;
	}

	inline static int32_t get_offset_of_lastRotation_12() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200, ___lastRotation_12)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_lastRotation_12() const { return ___lastRotation_12; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_lastRotation_12() { return &___lastRotation_12; }
	inline void set_lastRotation_12(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___lastRotation_12 = value;
	}

	inline static int32_t get_offset_of_SpawnType_13() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200, ___SpawnType_13)); }
	inline int32_t get_SpawnType_13() const { return ___SpawnType_13; }
	inline int32_t* get_address_of_SpawnType_13() { return &___SpawnType_13; }
	inline void set_SpawnType_13(int32_t value)
	{
		___SpawnType_13 = value;
	}
};


// TMPro.Examples.TextMeshSpawner
struct TextMeshSpawner_t44E5536D2A0C022FA8037C71741171A999A86364  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 TMPro.Examples.TextMeshSpawner::SpawnType
	int32_t ___SpawnType_4;
	// System.Int32 TMPro.Examples.TextMeshSpawner::NumberOfNPC
	int32_t ___NumberOfNPC_5;
	// UnityEngine.Font TMPro.Examples.TextMeshSpawner::TheFont
	Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 * ___TheFont_6;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshSpawner::floatingText_Script
	TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200 * ___floatingText_Script_7;

public:
	inline static int32_t get_offset_of_SpawnType_4() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t44E5536D2A0C022FA8037C71741171A999A86364, ___SpawnType_4)); }
	inline int32_t get_SpawnType_4() const { return ___SpawnType_4; }
	inline int32_t* get_address_of_SpawnType_4() { return &___SpawnType_4; }
	inline void set_SpawnType_4(int32_t value)
	{
		___SpawnType_4 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_5() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t44E5536D2A0C022FA8037C71741171A999A86364, ___NumberOfNPC_5)); }
	inline int32_t get_NumberOfNPC_5() const { return ___NumberOfNPC_5; }
	inline int32_t* get_address_of_NumberOfNPC_5() { return &___NumberOfNPC_5; }
	inline void set_NumberOfNPC_5(int32_t value)
	{
		___NumberOfNPC_5 = value;
	}

	inline static int32_t get_offset_of_TheFont_6() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t44E5536D2A0C022FA8037C71741171A999A86364, ___TheFont_6)); }
	inline Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 * get_TheFont_6() const { return ___TheFont_6; }
	inline Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 ** get_address_of_TheFont_6() { return &___TheFont_6; }
	inline void set_TheFont_6(Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 * value)
	{
		___TheFont_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TheFont_6), (void*)value);
	}

	inline static int32_t get_offset_of_floatingText_Script_7() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t44E5536D2A0C022FA8037C71741171A999A86364, ___floatingText_Script_7)); }
	inline TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200 * get_floatingText_Script_7() const { return ___floatingText_Script_7; }
	inline TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200 ** get_address_of_floatingText_Script_7() { return &___floatingText_Script_7; }
	inline void set_floatingText_Script_7(TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200 * value)
	{
		___floatingText_Script_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___floatingText_Script_7), (void*)value);
	}
};


// TMPro.Examples.VertexColorCycler
struct VertexColorCycler_t37E80E87D8EAD0D7757CDA45BD5D3AA82FD28374  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TMPro.TMP_Text TMPro.Examples.VertexColorCycler::m_TextComponent
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___m_TextComponent_4;

public:
	inline static int32_t get_offset_of_m_TextComponent_4() { return static_cast<int32_t>(offsetof(VertexColorCycler_t37E80E87D8EAD0D7757CDA45BD5D3AA82FD28374, ___m_TextComponent_4)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_m_TextComponent_4() const { return ___m_TextComponent_4; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_m_TextComponent_4() { return &___m_TextComponent_4; }
	inline void set_m_TextComponent_4(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___m_TextComponent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextComponent_4), (void*)value);
	}
};


// TMPro.Examples.VertexJitter
struct VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single TMPro.Examples.VertexJitter::AngleMultiplier
	float ___AngleMultiplier_4;
	// System.Single TMPro.Examples.VertexJitter::SpeedMultiplier
	float ___SpeedMultiplier_5;
	// System.Single TMPro.Examples.VertexJitter::CurveScale
	float ___CurveScale_6;
	// TMPro.TMP_Text TMPro.Examples.VertexJitter::m_TextComponent
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___m_TextComponent_7;
	// System.Boolean TMPro.Examples.VertexJitter::hasTextChanged
	bool ___hasTextChanged_8;

public:
	inline static int32_t get_offset_of_AngleMultiplier_4() { return static_cast<int32_t>(offsetof(VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A, ___AngleMultiplier_4)); }
	inline float get_AngleMultiplier_4() const { return ___AngleMultiplier_4; }
	inline float* get_address_of_AngleMultiplier_4() { return &___AngleMultiplier_4; }
	inline void set_AngleMultiplier_4(float value)
	{
		___AngleMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_5() { return static_cast<int32_t>(offsetof(VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A, ___SpeedMultiplier_5)); }
	inline float get_SpeedMultiplier_5() const { return ___SpeedMultiplier_5; }
	inline float* get_address_of_SpeedMultiplier_5() { return &___SpeedMultiplier_5; }
	inline void set_SpeedMultiplier_5(float value)
	{
		___SpeedMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_CurveScale_6() { return static_cast<int32_t>(offsetof(VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A, ___CurveScale_6)); }
	inline float get_CurveScale_6() const { return ___CurveScale_6; }
	inline float* get_address_of_CurveScale_6() { return &___CurveScale_6; }
	inline void set_CurveScale_6(float value)
	{
		___CurveScale_6 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_7() { return static_cast<int32_t>(offsetof(VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A, ___m_TextComponent_7)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_m_TextComponent_7() const { return ___m_TextComponent_7; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_m_TextComponent_7() { return &___m_TextComponent_7; }
	inline void set_m_TextComponent_7(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___m_TextComponent_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextComponent_7), (void*)value);
	}

	inline static int32_t get_offset_of_hasTextChanged_8() { return static_cast<int32_t>(offsetof(VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A, ___hasTextChanged_8)); }
	inline bool get_hasTextChanged_8() const { return ___hasTextChanged_8; }
	inline bool* get_address_of_hasTextChanged_8() { return &___hasTextChanged_8; }
	inline void set_hasTextChanged_8(bool value)
	{
		___hasTextChanged_8 = value;
	}
};


// TMPro.Examples.VertexShakeA
struct VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single TMPro.Examples.VertexShakeA::AngleMultiplier
	float ___AngleMultiplier_4;
	// System.Single TMPro.Examples.VertexShakeA::SpeedMultiplier
	float ___SpeedMultiplier_5;
	// System.Single TMPro.Examples.VertexShakeA::ScaleMultiplier
	float ___ScaleMultiplier_6;
	// System.Single TMPro.Examples.VertexShakeA::RotationMultiplier
	float ___RotationMultiplier_7;
	// TMPro.TMP_Text TMPro.Examples.VertexShakeA::m_TextComponent
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___m_TextComponent_8;
	// System.Boolean TMPro.Examples.VertexShakeA::hasTextChanged
	bool ___hasTextChanged_9;

public:
	inline static int32_t get_offset_of_AngleMultiplier_4() { return static_cast<int32_t>(offsetof(VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A, ___AngleMultiplier_4)); }
	inline float get_AngleMultiplier_4() const { return ___AngleMultiplier_4; }
	inline float* get_address_of_AngleMultiplier_4() { return &___AngleMultiplier_4; }
	inline void set_AngleMultiplier_4(float value)
	{
		___AngleMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_5() { return static_cast<int32_t>(offsetof(VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A, ___SpeedMultiplier_5)); }
	inline float get_SpeedMultiplier_5() const { return ___SpeedMultiplier_5; }
	inline float* get_address_of_SpeedMultiplier_5() { return &___SpeedMultiplier_5; }
	inline void set_SpeedMultiplier_5(float value)
	{
		___SpeedMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_ScaleMultiplier_6() { return static_cast<int32_t>(offsetof(VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A, ___ScaleMultiplier_6)); }
	inline float get_ScaleMultiplier_6() const { return ___ScaleMultiplier_6; }
	inline float* get_address_of_ScaleMultiplier_6() { return &___ScaleMultiplier_6; }
	inline void set_ScaleMultiplier_6(float value)
	{
		___ScaleMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_RotationMultiplier_7() { return static_cast<int32_t>(offsetof(VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A, ___RotationMultiplier_7)); }
	inline float get_RotationMultiplier_7() const { return ___RotationMultiplier_7; }
	inline float* get_address_of_RotationMultiplier_7() { return &___RotationMultiplier_7; }
	inline void set_RotationMultiplier_7(float value)
	{
		___RotationMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_8() { return static_cast<int32_t>(offsetof(VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A, ___m_TextComponent_8)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_m_TextComponent_8() const { return ___m_TextComponent_8; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_m_TextComponent_8() { return &___m_TextComponent_8; }
	inline void set_m_TextComponent_8(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___m_TextComponent_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextComponent_8), (void*)value);
	}

	inline static int32_t get_offset_of_hasTextChanged_9() { return static_cast<int32_t>(offsetof(VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A, ___hasTextChanged_9)); }
	inline bool get_hasTextChanged_9() const { return ___hasTextChanged_9; }
	inline bool* get_address_of_hasTextChanged_9() { return &___hasTextChanged_9; }
	inline void set_hasTextChanged_9(bool value)
	{
		___hasTextChanged_9 = value;
	}
};


// TMPro.Examples.VertexShakeB
struct VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single TMPro.Examples.VertexShakeB::AngleMultiplier
	float ___AngleMultiplier_4;
	// System.Single TMPro.Examples.VertexShakeB::SpeedMultiplier
	float ___SpeedMultiplier_5;
	// System.Single TMPro.Examples.VertexShakeB::CurveScale
	float ___CurveScale_6;
	// TMPro.TMP_Text TMPro.Examples.VertexShakeB::m_TextComponent
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___m_TextComponent_7;
	// System.Boolean TMPro.Examples.VertexShakeB::hasTextChanged
	bool ___hasTextChanged_8;

public:
	inline static int32_t get_offset_of_AngleMultiplier_4() { return static_cast<int32_t>(offsetof(VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9, ___AngleMultiplier_4)); }
	inline float get_AngleMultiplier_4() const { return ___AngleMultiplier_4; }
	inline float* get_address_of_AngleMultiplier_4() { return &___AngleMultiplier_4; }
	inline void set_AngleMultiplier_4(float value)
	{
		___AngleMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_5() { return static_cast<int32_t>(offsetof(VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9, ___SpeedMultiplier_5)); }
	inline float get_SpeedMultiplier_5() const { return ___SpeedMultiplier_5; }
	inline float* get_address_of_SpeedMultiplier_5() { return &___SpeedMultiplier_5; }
	inline void set_SpeedMultiplier_5(float value)
	{
		___SpeedMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_CurveScale_6() { return static_cast<int32_t>(offsetof(VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9, ___CurveScale_6)); }
	inline float get_CurveScale_6() const { return ___CurveScale_6; }
	inline float* get_address_of_CurveScale_6() { return &___CurveScale_6; }
	inline void set_CurveScale_6(float value)
	{
		___CurveScale_6 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_7() { return static_cast<int32_t>(offsetof(VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9, ___m_TextComponent_7)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_m_TextComponent_7() const { return ___m_TextComponent_7; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_m_TextComponent_7() { return &___m_TextComponent_7; }
	inline void set_m_TextComponent_7(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___m_TextComponent_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextComponent_7), (void*)value);
	}

	inline static int32_t get_offset_of_hasTextChanged_8() { return static_cast<int32_t>(offsetof(VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9, ___hasTextChanged_8)); }
	inline bool get_hasTextChanged_8() const { return ___hasTextChanged_8; }
	inline bool* get_address_of_hasTextChanged_8() { return &___hasTextChanged_8; }
	inline void set_hasTextChanged_8(bool value)
	{
		___hasTextChanged_8 = value;
	}
};


// TMPro.Examples.VertexZoom
struct VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single TMPro.Examples.VertexZoom::AngleMultiplier
	float ___AngleMultiplier_4;
	// System.Single TMPro.Examples.VertexZoom::SpeedMultiplier
	float ___SpeedMultiplier_5;
	// System.Single TMPro.Examples.VertexZoom::CurveScale
	float ___CurveScale_6;
	// TMPro.TMP_Text TMPro.Examples.VertexZoom::m_TextComponent
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___m_TextComponent_7;
	// System.Boolean TMPro.Examples.VertexZoom::hasTextChanged
	bool ___hasTextChanged_8;

public:
	inline static int32_t get_offset_of_AngleMultiplier_4() { return static_cast<int32_t>(offsetof(VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163, ___AngleMultiplier_4)); }
	inline float get_AngleMultiplier_4() const { return ___AngleMultiplier_4; }
	inline float* get_address_of_AngleMultiplier_4() { return &___AngleMultiplier_4; }
	inline void set_AngleMultiplier_4(float value)
	{
		___AngleMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_5() { return static_cast<int32_t>(offsetof(VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163, ___SpeedMultiplier_5)); }
	inline float get_SpeedMultiplier_5() const { return ___SpeedMultiplier_5; }
	inline float* get_address_of_SpeedMultiplier_5() { return &___SpeedMultiplier_5; }
	inline void set_SpeedMultiplier_5(float value)
	{
		___SpeedMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_CurveScale_6() { return static_cast<int32_t>(offsetof(VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163, ___CurveScale_6)); }
	inline float get_CurveScale_6() const { return ___CurveScale_6; }
	inline float* get_address_of_CurveScale_6() { return &___CurveScale_6; }
	inline void set_CurveScale_6(float value)
	{
		___CurveScale_6 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_7() { return static_cast<int32_t>(offsetof(VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163, ___m_TextComponent_7)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_m_TextComponent_7() const { return ___m_TextComponent_7; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_m_TextComponent_7() { return &___m_TextComponent_7; }
	inline void set_m_TextComponent_7(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___m_TextComponent_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextComponent_7), (void*)value);
	}

	inline static int32_t get_offset_of_hasTextChanged_8() { return static_cast<int32_t>(offsetof(VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163, ___hasTextChanged_8)); }
	inline bool get_hasTextChanged_8() const { return ___hasTextChanged_8; }
	inline bool* get_address_of_hasTextChanged_8() { return &___hasTextChanged_8; }
	inline void set_hasTextChanged_8(bool value)
	{
		___hasTextChanged_8 = value;
	}
};


// TMPro.Examples.WarpTextExample
struct WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TMPro.TMP_Text TMPro.Examples.WarpTextExample::m_TextComponent
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___m_TextComponent_4;
	// UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::VertexCurve
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___VertexCurve_5;
	// System.Single TMPro.Examples.WarpTextExample::AngleMultiplier
	float ___AngleMultiplier_6;
	// System.Single TMPro.Examples.WarpTextExample::SpeedMultiplier
	float ___SpeedMultiplier_7;
	// System.Single TMPro.Examples.WarpTextExample::CurveScale
	float ___CurveScale_8;

public:
	inline static int32_t get_offset_of_m_TextComponent_4() { return static_cast<int32_t>(offsetof(WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96, ___m_TextComponent_4)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_m_TextComponent_4() const { return ___m_TextComponent_4; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_m_TextComponent_4() { return &___m_TextComponent_4; }
	inline void set_m_TextComponent_4(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___m_TextComponent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextComponent_4), (void*)value);
	}

	inline static int32_t get_offset_of_VertexCurve_5() { return static_cast<int32_t>(offsetof(WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96, ___VertexCurve_5)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_VertexCurve_5() const { return ___VertexCurve_5; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_VertexCurve_5() { return &___VertexCurve_5; }
	inline void set_VertexCurve_5(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___VertexCurve_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___VertexCurve_5), (void*)value);
	}

	inline static int32_t get_offset_of_AngleMultiplier_6() { return static_cast<int32_t>(offsetof(WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96, ___AngleMultiplier_6)); }
	inline float get_AngleMultiplier_6() const { return ___AngleMultiplier_6; }
	inline float* get_address_of_AngleMultiplier_6() { return &___AngleMultiplier_6; }
	inline void set_AngleMultiplier_6(float value)
	{
		___AngleMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_7() { return static_cast<int32_t>(offsetof(WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96, ___SpeedMultiplier_7)); }
	inline float get_SpeedMultiplier_7() const { return ___SpeedMultiplier_7; }
	inline float* get_address_of_SpeedMultiplier_7() { return &___SpeedMultiplier_7; }
	inline void set_SpeedMultiplier_7(float value)
	{
		___SpeedMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_CurveScale_8() { return static_cast<int32_t>(offsetof(WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96, ___CurveScale_8)); }
	inline float get_CurveScale_8() const { return ___CurveScale_8; }
	inline float* get_address_of_CurveScale_8() { return &___CurveScale_8; }
	inline void set_CurveScale_8(float value)
	{
		___CurveScale_8 = value;
	}
};


// CSAlertRewardAnim
struct CSAlertRewardAnim_t13B8641467B4284296D29B346C9760D59E21E9E0  : public CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A
{
public:
	// CSBankCoinPanel CSAlertRewardAnim::coinPanel
	CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 * ___coinPanel_19;
	// TMPro.TextMeshProUGUI CSAlertRewardAnim::rewardLabel
	TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * ___rewardLabel_20;
	// System.Single CSAlertRewardAnim::_reward
	float ____reward_21;

public:
	inline static int32_t get_offset_of_coinPanel_19() { return static_cast<int32_t>(offsetof(CSAlertRewardAnim_t13B8641467B4284296D29B346C9760D59E21E9E0, ___coinPanel_19)); }
	inline CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 * get_coinPanel_19() const { return ___coinPanel_19; }
	inline CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 ** get_address_of_coinPanel_19() { return &___coinPanel_19; }
	inline void set_coinPanel_19(CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029 * value)
	{
		___coinPanel_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___coinPanel_19), (void*)value);
	}

	inline static int32_t get_offset_of_rewardLabel_20() { return static_cast<int32_t>(offsetof(CSAlertRewardAnim_t13B8641467B4284296D29B346C9760D59E21E9E0, ___rewardLabel_20)); }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * get_rewardLabel_20() const { return ___rewardLabel_20; }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 ** get_address_of_rewardLabel_20() { return &___rewardLabel_20; }
	inline void set_rewardLabel_20(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * value)
	{
		___rewardLabel_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rewardLabel_20), (void*)value);
	}

	inline static int32_t get_offset_of__reward_21() { return static_cast<int32_t>(offsetof(CSAlertRewardAnim_t13B8641467B4284296D29B346C9760D59E21E9E0, ____reward_21)); }
	inline float get__reward_21() const { return ____reward_21; }
	inline float* get_address_of__reward_21() { return &____reward_21; }
	inline void set__reward_21(float value)
	{
		____reward_21 = value;
	}
};


// CSCSBottomPanel
struct CSCSBottomPanel_tA2ABC8CC6C066B3AEA8E2F2BF5C88AF9DAE79726  : public CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38
{
public:
	// UnityEngine.UI.Button CSCSBottomPanel::gamble
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___gamble_17;
	// CSCSGamble CSCSBottomPanel::gambleGame
	CSCSGamble_tC4C1846CB1F82B4350A1D3E73034D7581A80B93F * ___gambleGame_18;
	// CSReels CSCSBottomPanel::reels
	CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47 * ___reels_19;
	// UnityEngine.Vector3 CSCSBottomPanel::_startPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____startPosition_20;
	// System.Boolean CSCSBottomPanel::_showGamble
	bool ____showGamble_21;

public:
	inline static int32_t get_offset_of_gamble_17() { return static_cast<int32_t>(offsetof(CSCSBottomPanel_tA2ABC8CC6C066B3AEA8E2F2BF5C88AF9DAE79726, ___gamble_17)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_gamble_17() const { return ___gamble_17; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_gamble_17() { return &___gamble_17; }
	inline void set_gamble_17(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___gamble_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gamble_17), (void*)value);
	}

	inline static int32_t get_offset_of_gambleGame_18() { return static_cast<int32_t>(offsetof(CSCSBottomPanel_tA2ABC8CC6C066B3AEA8E2F2BF5C88AF9DAE79726, ___gambleGame_18)); }
	inline CSCSGamble_tC4C1846CB1F82B4350A1D3E73034D7581A80B93F * get_gambleGame_18() const { return ___gambleGame_18; }
	inline CSCSGamble_tC4C1846CB1F82B4350A1D3E73034D7581A80B93F ** get_address_of_gambleGame_18() { return &___gambleGame_18; }
	inline void set_gambleGame_18(CSCSGamble_tC4C1846CB1F82B4350A1D3E73034D7581A80B93F * value)
	{
		___gambleGame_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gambleGame_18), (void*)value);
	}

	inline static int32_t get_offset_of_reels_19() { return static_cast<int32_t>(offsetof(CSCSBottomPanel_tA2ABC8CC6C066B3AEA8E2F2BF5C88AF9DAE79726, ___reels_19)); }
	inline CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47 * get_reels_19() const { return ___reels_19; }
	inline CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47 ** get_address_of_reels_19() { return &___reels_19; }
	inline void set_reels_19(CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47 * value)
	{
		___reels_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reels_19), (void*)value);
	}

	inline static int32_t get_offset_of__startPosition_20() { return static_cast<int32_t>(offsetof(CSCSBottomPanel_tA2ABC8CC6C066B3AEA8E2F2BF5C88AF9DAE79726, ____startPosition_20)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__startPosition_20() const { return ____startPosition_20; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__startPosition_20() { return &____startPosition_20; }
	inline void set__startPosition_20(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____startPosition_20 = value;
	}

	inline static int32_t get_offset_of__showGamble_21() { return static_cast<int32_t>(offsetof(CSCSBottomPanel_tA2ABC8CC6C066B3AEA8E2F2BF5C88AF9DAE79726, ____showGamble_21)); }
	inline bool get__showGamble_21() const { return ____showGamble_21; }
	inline bool* get_address_of__showGamble_21() { return &____showGamble_21; }
	inline void set__showGamble_21(bool value)
	{
		____showGamble_21 = value;
	}
};


// CSCSFreeGamePanel
struct CSCSFreeGamePanel_tDC8A9E56CC4EEB6094171E5DF29FB0D31A409A32  : public CSFreeGamePanel_tC3C7EE40E9C939B3B63EFCE22621ED5792C3D7E6
{
public:
	// UnityEngine.UI.Text CSCSFreeGamePanel::totalBetLabel
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___totalBetLabel_10;
	// System.Int32 CSCSFreeGamePanel::_totalBet
	int32_t ____totalBet_11;
	// UnityEngine.GameObject CSCSFreeGamePanel::multiplierGameObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___multiplierGameObject_12;
	// System.Boolean CSCSFreeGamePanel::_muliplierEnable
	bool ____muliplierEnable_13;
	// TMPro.TextMeshProUGUI CSCSFreeGamePanel::multiplierLabel
	TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * ___multiplierLabel_14;
	// System.Int32 CSCSFreeGamePanel::_multiplier
	int32_t ____multiplier_15;

public:
	inline static int32_t get_offset_of_totalBetLabel_10() { return static_cast<int32_t>(offsetof(CSCSFreeGamePanel_tDC8A9E56CC4EEB6094171E5DF29FB0D31A409A32, ___totalBetLabel_10)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_totalBetLabel_10() const { return ___totalBetLabel_10; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_totalBetLabel_10() { return &___totalBetLabel_10; }
	inline void set_totalBetLabel_10(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___totalBetLabel_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___totalBetLabel_10), (void*)value);
	}

	inline static int32_t get_offset_of__totalBet_11() { return static_cast<int32_t>(offsetof(CSCSFreeGamePanel_tDC8A9E56CC4EEB6094171E5DF29FB0D31A409A32, ____totalBet_11)); }
	inline int32_t get__totalBet_11() const { return ____totalBet_11; }
	inline int32_t* get_address_of__totalBet_11() { return &____totalBet_11; }
	inline void set__totalBet_11(int32_t value)
	{
		____totalBet_11 = value;
	}

	inline static int32_t get_offset_of_multiplierGameObject_12() { return static_cast<int32_t>(offsetof(CSCSFreeGamePanel_tDC8A9E56CC4EEB6094171E5DF29FB0D31A409A32, ___multiplierGameObject_12)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_multiplierGameObject_12() const { return ___multiplierGameObject_12; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_multiplierGameObject_12() { return &___multiplierGameObject_12; }
	inline void set_multiplierGameObject_12(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___multiplierGameObject_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___multiplierGameObject_12), (void*)value);
	}

	inline static int32_t get_offset_of__muliplierEnable_13() { return static_cast<int32_t>(offsetof(CSCSFreeGamePanel_tDC8A9E56CC4EEB6094171E5DF29FB0D31A409A32, ____muliplierEnable_13)); }
	inline bool get__muliplierEnable_13() const { return ____muliplierEnable_13; }
	inline bool* get_address_of__muliplierEnable_13() { return &____muliplierEnable_13; }
	inline void set__muliplierEnable_13(bool value)
	{
		____muliplierEnable_13 = value;
	}

	inline static int32_t get_offset_of_multiplierLabel_14() { return static_cast<int32_t>(offsetof(CSCSFreeGamePanel_tDC8A9E56CC4EEB6094171E5DF29FB0D31A409A32, ___multiplierLabel_14)); }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * get_multiplierLabel_14() const { return ___multiplierLabel_14; }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 ** get_address_of_multiplierLabel_14() { return &___multiplierLabel_14; }
	inline void set_multiplierLabel_14(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * value)
	{
		___multiplierLabel_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___multiplierLabel_14), (void*)value);
	}

	inline static int32_t get_offset_of__multiplier_15() { return static_cast<int32_t>(offsetof(CSCSFreeGamePanel_tDC8A9E56CC4EEB6094171E5DF29FB0D31A409A32, ____multiplier_15)); }
	inline int32_t get__multiplier_15() const { return ____multiplier_15; }
	inline int32_t* get_address_of__multiplier_15() { return &____multiplier_15; }
	inline void set__multiplier_15(int32_t value)
	{
		____multiplier_15 = value;
	}
};


// CSCSGamble
struct CSCSGamble_tC4C1846CB1F82B4350A1D3E73034D7581A80B93F  : public CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C
{
public:

public:
};


// CSCSReels
struct CSCSReels_tF384AA60A9E6FA432BA6D99169CCCA17C7ACECD4  : public CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47
{
public:
	// CSCSWinAlert CSCSReels::alert
	CSCSWinAlert_tE1DC42587F7A4AF4EE8AA718FAB6269314AB6B79 * ___alert_19;
	// CSCSBonusGameSettings CSCSReels::_settings
	CSCSBonusGameSettings_t6311F6E406F6804E8E84F28F2155273091F40076  ____settings_20;

public:
	inline static int32_t get_offset_of_alert_19() { return static_cast<int32_t>(offsetof(CSCSReels_tF384AA60A9E6FA432BA6D99169CCCA17C7ACECD4, ___alert_19)); }
	inline CSCSWinAlert_tE1DC42587F7A4AF4EE8AA718FAB6269314AB6B79 * get_alert_19() const { return ___alert_19; }
	inline CSCSWinAlert_tE1DC42587F7A4AF4EE8AA718FAB6269314AB6B79 ** get_address_of_alert_19() { return &___alert_19; }
	inline void set_alert_19(CSCSWinAlert_tE1DC42587F7A4AF4EE8AA718FAB6269314AB6B79 * value)
	{
		___alert_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___alert_19), (void*)value);
	}

	inline static int32_t get_offset_of__settings_20() { return static_cast<int32_t>(offsetof(CSCSReels_tF384AA60A9E6FA432BA6D99169CCCA17C7ACECD4, ____settings_20)); }
	inline CSCSBonusGameSettings_t6311F6E406F6804E8E84F28F2155273091F40076  get__settings_20() const { return ____settings_20; }
	inline CSCSBonusGameSettings_t6311F6E406F6804E8E84F28F2155273091F40076 * get_address_of__settings_20() { return &____settings_20; }
	inline void set__settings_20(CSCSBonusGameSettings_t6311F6E406F6804E8E84F28F2155273091F40076  value)
	{
		____settings_20 = value;
	}
};


// CSCSSymbol
struct CSCSSymbol_t8345573733BC4EA8ACBB5F72DC53A24A4A56222D  : public CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555
{
public:

public:
};


// CSCSTopPanel
struct CSCSTopPanel_tE86FE3383C370373A110DAE0A65274B7A9FBFBD8  : public CSTopPanel_t94C2B84ED826C2C503575317319F623658C0C4D8
{
public:
	// CSInfo CSCSTopPanel::info
	CSInfo_tD22B0C9AD79CB449BCBC64512307F5EFAE046AA1 * ___info_7;

public:
	inline static int32_t get_offset_of_info_7() { return static_cast<int32_t>(offsetof(CSCSTopPanel_tE86FE3383C370373A110DAE0A65274B7A9FBFBD8, ___info_7)); }
	inline CSInfo_tD22B0C9AD79CB449BCBC64512307F5EFAE046AA1 * get_info_7() const { return ___info_7; }
	inline CSInfo_tD22B0C9AD79CB449BCBC64512307F5EFAE046AA1 ** get_address_of_info_7() { return &___info_7; }
	inline void set_info_7(CSInfo_tD22B0C9AD79CB449BCBC64512307F5EFAE046AA1 * value)
	{
		___info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___info_7), (void*)value);
	}
};


// HorizontalScrollSnap
struct HorizontalScrollSnap_tE6B632D4D5CDD31DFC9CF915D070FD422F1597BA  : public ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345
{
public:

public:
};


// CSCSWinAlert
struct CSCSWinAlert_tE1DC42587F7A4AF4EE8AA718FAB6269314AB6B79  : public CSAlertRewardAnim_t13B8641467B4284296D29B346C9760D59E21E9E0
{
public:
	// CSLFBGAlertBoard CSCSWinAlert::boardScript
	CSLFBGAlertBoard_t6EF319BFC3C51BF1443743AFC7030B7566305D45 * ___boardScript_22;
	// CSReels CSCSWinAlert::reels
	CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47 * ___reels_23;
	// CSCSBonusGameSettings CSCSWinAlert::_settings
	CSCSBonusGameSettings_t6311F6E406F6804E8E84F28F2155273091F40076  ____settings_24;

public:
	inline static int32_t get_offset_of_boardScript_22() { return static_cast<int32_t>(offsetof(CSCSWinAlert_tE1DC42587F7A4AF4EE8AA718FAB6269314AB6B79, ___boardScript_22)); }
	inline CSLFBGAlertBoard_t6EF319BFC3C51BF1443743AFC7030B7566305D45 * get_boardScript_22() const { return ___boardScript_22; }
	inline CSLFBGAlertBoard_t6EF319BFC3C51BF1443743AFC7030B7566305D45 ** get_address_of_boardScript_22() { return &___boardScript_22; }
	inline void set_boardScript_22(CSLFBGAlertBoard_t6EF319BFC3C51BF1443743AFC7030B7566305D45 * value)
	{
		___boardScript_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___boardScript_22), (void*)value);
	}

	inline static int32_t get_offset_of_reels_23() { return static_cast<int32_t>(offsetof(CSCSWinAlert_tE1DC42587F7A4AF4EE8AA718FAB6269314AB6B79, ___reels_23)); }
	inline CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47 * get_reels_23() const { return ___reels_23; }
	inline CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47 ** get_address_of_reels_23() { return &___reels_23; }
	inline void set_reels_23(CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47 * value)
	{
		___reels_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reels_23), (void*)value);
	}

	inline static int32_t get_offset_of__settings_24() { return static_cast<int32_t>(offsetof(CSCSWinAlert_tE1DC42587F7A4AF4EE8AA718FAB6269314AB6B79, ____settings_24)); }
	inline CSCSBonusGameSettings_t6311F6E406F6804E8E84F28F2155273091F40076  get__settings_24() const { return ____settings_24; }
	inline CSCSBonusGameSettings_t6311F6E406F6804E8E84F28F2155273091F40076 * get_address_of__settings_24() { return &____settings_24; }
	inline void set__settings_24(CSCSBonusGameSettings_t6311F6E406F6804E8E84F28F2155273091F40076  value)
	{
		____settings_24 = value;
	}
};


// CSLevelUpAlert
struct CSLevelUpAlert_tEF4E42E838D5DB762ABB678F2A29B38E6FD491CB  : public CSAlertRewardAnim_t13B8641467B4284296D29B346C9760D59E21E9E0
{
public:
	// System.Int32 CSLevelUpAlert::_level
	int32_t ____level_22;
	// TMPro.TextMeshProUGUI CSLevelUpAlert::level
	TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * ___level_23;
	// UnityEngine.RectTransform CSLevelUpAlert::_levelUpText
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ____levelUpText_24;
	// UnityEngine.RectTransform CSLevelUpAlert::_stars
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ____stars_25;
	// System.Collections.Generic.Dictionary`2<System.Int32,StarData> CSLevelUpAlert::_starPos
	Dictionary_2_tA165F361FD3982DB0143EB119AB88636E6991F01 * ____starPos_26;

public:
	inline static int32_t get_offset_of__level_22() { return static_cast<int32_t>(offsetof(CSLevelUpAlert_tEF4E42E838D5DB762ABB678F2A29B38E6FD491CB, ____level_22)); }
	inline int32_t get__level_22() const { return ____level_22; }
	inline int32_t* get_address_of__level_22() { return &____level_22; }
	inline void set__level_22(int32_t value)
	{
		____level_22 = value;
	}

	inline static int32_t get_offset_of_level_23() { return static_cast<int32_t>(offsetof(CSLevelUpAlert_tEF4E42E838D5DB762ABB678F2A29B38E6FD491CB, ___level_23)); }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * get_level_23() const { return ___level_23; }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 ** get_address_of_level_23() { return &___level_23; }
	inline void set_level_23(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * value)
	{
		___level_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___level_23), (void*)value);
	}

	inline static int32_t get_offset_of__levelUpText_24() { return static_cast<int32_t>(offsetof(CSLevelUpAlert_tEF4E42E838D5DB762ABB678F2A29B38E6FD491CB, ____levelUpText_24)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get__levelUpText_24() const { return ____levelUpText_24; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of__levelUpText_24() { return &____levelUpText_24; }
	inline void set__levelUpText_24(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		____levelUpText_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____levelUpText_24), (void*)value);
	}

	inline static int32_t get_offset_of__stars_25() { return static_cast<int32_t>(offsetof(CSLevelUpAlert_tEF4E42E838D5DB762ABB678F2A29B38E6FD491CB, ____stars_25)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get__stars_25() const { return ____stars_25; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of__stars_25() { return &____stars_25; }
	inline void set__stars_25(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		____stars_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stars_25), (void*)value);
	}

	inline static int32_t get_offset_of__starPos_26() { return static_cast<int32_t>(offsetof(CSLevelUpAlert_tEF4E42E838D5DB762ABB678F2A29B38E6FD491CB, ____starPos_26)); }
	inline Dictionary_2_tA165F361FD3982DB0143EB119AB88636E6991F01 * get__starPos_26() const { return ____starPos_26; }
	inline Dictionary_2_tA165F361FD3982DB0143EB119AB88636E6991F01 ** get_address_of__starPos_26() { return &____starPos_26; }
	inline void set__starPos_26(Dictionary_2_tA165F361FD3982DB0143EB119AB88636E6991F01 * value)
	{
		____starPos_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____starPos_26), (void*)value);
	}
};


// CSZLGambleAlert
struct CSZLGambleAlert_t4ADF9CEC8DA77AE4616BFE0ABBF6C3A17E86F231  : public CSAlertRewardAnim_t13B8641467B4284296D29B346C9760D59E21E9E0
{
public:
	// CSBottomPanel CSZLGambleAlert::hudPanel
	CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38 * ___hudPanel_22;

public:
	inline static int32_t get_offset_of_hudPanel_22() { return static_cast<int32_t>(offsetof(CSZLGambleAlert_t4ADF9CEC8DA77AE4616BFE0ABBF6C3A17E86F231, ___hudPanel_22)); }
	inline CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38 * get_hudPanel_22() const { return ___hudPanel_22; }
	inline CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38 ** get_address_of_hudPanel_22() { return &___hudPanel_22; }
	inline void set_hudPanel_22(CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38 * value)
	{
		___hudPanel_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hudPanel_22), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3360[4] = 
{
	U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6::get_offset_of_U3CU3E1__state_0(),
	U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6::get_offset_of_U3CU3E2__current_1(),
	U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6::get_offset_of_U3CU3E4__this_2(),
	U3CtimeoutCheckU3Ed__2_t0477BBE86F68913A187266A302E855A27D6286B6::get_offset_of_U3CpauseEndTimeU3E5__2_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3361[1] = 
{
	LeanTester_t537C3FE41ED244252241E5B02F6B1BA6DA437297::get_offset_of_timeout_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3362[6] = 
{
	LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields::get_offset_of_expected_0(),
	LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields::get_offset_of_tests_1(),
	LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields::get_offset_of_passes_2(),
	LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields::get_offset_of_timeout_3(),
	LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields::get_offset_of_timeoutStarted_4(),
	LeanTest_tD21F4A4ADCD2017FEFC0BA0FDAB5D291DA643F7F_StaticFields::get_offset_of_testsFinished_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3363[52] = 
{
	TweenAction_tB7B9473B02F7CC04ED4083C3934285274119AE0B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3364[40] = 
{
	LeanTweenType_tAE51C34373F1326AC0BB9DB0F7EF1883603D55A9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3365[11] = 
{
	LeanProp_tEF273BF6EB0960483D03A097A40313D700DE3538::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3366[3] = 
{
	U3CU3Ec__DisplayClass193_0_tAD697F408BB575C622FD5E018C1F75AB28CBAF48::get_offset_of_d_0(),
	U3CU3Ec__DisplayClass193_0_tAD697F408BB575C622FD5E018C1F75AB28CBAF48::get_offset_of_smoothTime_1(),
	U3CU3Ec__DisplayClass193_0_tAD697F408BB575C622FD5E018C1F75AB28CBAF48::get_offset_of_maxSpeed_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3367[5] = 
{
	U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814::get_offset_of_d_0(),
	U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814::get_offset_of_smoothTime_1(),
	U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814::get_offset_of_maxSpeed_2(),
	U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814::get_offset_of_friction_3(),
	U3CU3Ec__DisplayClass194_0_t6FE45F50B4F93FED778E7D54F0EF582EA8252814::get_offset_of_accelRate_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3368[6] = 
{
	U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4::get_offset_of_d_0(),
	U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4::get_offset_of_smoothTime_1(),
	U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4::get_offset_of_maxSpeed_2(),
	U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4::get_offset_of_friction_3(),
	U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4::get_offset_of_accelRate_4(),
	U3CU3Ec__DisplayClass195_0_tF0E800EBCA190980873A4098BBEEFCFBD61950B4::get_offset_of_hitDamping_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3369[2] = 
{
	U3CU3Ec__DisplayClass196_0_t4FA263E9CE532093F4513F5750D54960760E3F78::get_offset_of_d_0(),
	U3CU3Ec__DisplayClass196_0_t4FA263E9CE532093F4513F5750D54960760E3F78::get_offset_of_moveSpeed_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3370[31] = 
{
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_throwErrors_4(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_tau_5(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_PI_DIV2_6(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_sequences_7(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_tweens_8(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_tweensFinished_9(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_tweensFinishedIds_10(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_tween_11(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_tweenMaxSearch_12(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_maxTweens_13(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_maxSequences_14(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_frameRendered_15(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of__tweenEmpty_16(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_dtEstimated_17(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_dtManual_18(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_dtActual_19(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_global_counter_20(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_i_21(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_j_22(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_finishedCnt_23(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_punch_24(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_shake_25(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_maxTweenReached_26(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_startSearch_27(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_d_28(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_eventListeners_29(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_goListeners_30(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_eventsMaxSearch_31(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_EVENTS_MAX_32(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_LISTENERS_MAX_33(),
	LeanTween_t24A1AC58CDA8E051E941E4BA7709AA48344ABFA2_StaticFields::get_offset_of_INIT_LISTENERS_MAX_34(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3372[7] = 
{
	LTBezier_tD081588F24017B409080ADD7EC92332888511C41::get_offset_of_length_0(),
	LTBezier_tD081588F24017B409080ADD7EC92332888511C41::get_offset_of_a_1(),
	LTBezier_tD081588F24017B409080ADD7EC92332888511C41::get_offset_of_aa_2(),
	LTBezier_tD081588F24017B409080ADD7EC92332888511C41::get_offset_of_bb_3(),
	LTBezier_tD081588F24017B409080ADD7EC92332888511C41::get_offset_of_cc_4(),
	LTBezier_tD081588F24017B409080ADD7EC92332888511C41::get_offset_of_len_5(),
	LTBezier_tD081588F24017B409080ADD7EC92332888511C41::get_offset_of_arcLengths_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3373[8] = 
{
	LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48::get_offset_of_pts_0(),
	LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48::get_offset_of_length_1(),
	LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48::get_offset_of_orientToPath_2(),
	LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48::get_offset_of_orientToPath2d_3(),
	LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48::get_offset_of_beziers_4(),
	LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48::get_offset_of_lengthRatio_5(),
	LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48::get_offset_of_currentBezier_6(),
	LTBezierPath_tEB367BB294F41F1D4B9B728E83788A4A2EB8FD48::get_offset_of_previousBezier_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3374[11] = 
{
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701_StaticFields::get_offset_of_DISTANCE_COUNT_0(),
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701_StaticFields::get_offset_of_SUBLINE_COUNT_1(),
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701::get_offset_of_distance_2(),
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701::get_offset_of_constantSpeed_3(),
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701::get_offset_of_pts_4(),
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701::get_offset_of_ptsAdj_5(),
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701::get_offset_of_ptsAdjLength_6(),
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701::get_offset_of_orientToPath_7(),
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701::get_offset_of_orientToPath2d_8(),
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701::get_offset_of_numSections_9(),
	LTSpline_t4DE3CFCB0B4DB70C4798ABA88BBFFD46A515C701::get_offset_of_currPt_10(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3375[21] = 
{
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of__rect_0(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_alpha_1(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_rotation_2(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_pivot_3(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_margin_4(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_relativeRect_5(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_rotateEnabled_6(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_rotateFinished_7(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_alphaEnabled_8(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_labelStr_9(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_type_10(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_style_11(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_useColor_12(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_color_13(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_fontScaleToFit_14(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_useSimpleScale_15(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_sizeByHeight_16(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_texture_17(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of__id_18(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD::get_offset_of_counter_19(),
	LTRect_t021E2200FFDBE6E949B5557CCEE72E8062D2DABD_StaticFields::get_offset_of_colorTouched_20(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3376[2] = 
{
	LTEvent_tFD435CD71D73BB5C8DBB11E20B4E3EF3ECC428EA::get_offset_of_id_0(),
	LTEvent_tFD435CD71D73BB5C8DBB11E20B4E3EF3ECC428EA::get_offset_of_data_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3377[3] = 
{
	Element_Type_tCB9236487D710B365968D3F055C4CCCA555A6BFE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3378[12] = 
{
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_RECT_LEVELS_0(),
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_RECTS_PER_LEVEL_1(),
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_BUTTONS_MAX_2(),
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_levels_3(),
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_levelDepths_4(),
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_buttons_5(),
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_buttonLevels_6(),
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_buttonLastFrame_7(),
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_r_8(),
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_color_9(),
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_isGUIEnabled_10(),
	LTGUI_tB4EBCE24B8306D2D3E296334D3E0DB3D226DE816_StaticFields::get_offset_of_global_counter_11(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3382[3] = 
{
	U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58_StaticFields::get_offset_of_U3CU3E9__113_0_1(),
	U3CU3Ec_t2A61C4AA2F0D1C194C052F501E3F0ED7D8F67D58_StaticFields::get_offset_of_U3CU3E9__114_0_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3383[48] = 
{
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_toggle_0(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_useEstimatedTime_1(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_useFrames_2(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_useManualTime_3(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_usesNormalDt_4(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_hasInitiliazed_5(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_hasExtraOnCompletes_6(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_hasPhysics_7(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_onCompleteOnRepeat_8(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_onCompleteOnStart_9(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_useRecursion_10(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_ratioPassed_11(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_passed_12(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_delay_13(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_time_14(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_speed_15(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_lastVal_16(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of__id_17(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_loopCount_18(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_counter_19(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_direction_20(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_directionLast_21(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_overshoot_22(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_period_23(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_scale_24(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_destroyOnComplete_25(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_trans_26(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_fromInternal_27(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_toInternal_28(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_diff_29(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_diffDiv2_30(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_type_31(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_easeType_32(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_loopType_33(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_hasUpdateCallback_34(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_easeMethod_35(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_U3CeaseInternalU3Ek__BackingField_36(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_U3CinitInternalU3Ek__BackingField_37(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_spriteRen_38(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_rectTransform_39(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_uiText_40(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_uiImage_41(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_rawImage_42(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of_sprites_43(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F::get_offset_of__optional_44(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_StaticFields::get_offset_of_val_45(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_StaticFields::get_offset_of_dt_46(),
	LTDescr_t9A3CDAF54A7C42CE3B0D73AAE3087D8C910F602F_StaticFields::get_offset_of_newVect_47(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3384[24] = 
{
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3CtoTransU3Ek__BackingField_0(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3CpointU3Ek__BackingField_1(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3CaxisU3Ek__BackingField_2(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ClastValU3Ek__BackingField_3(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3CorigRotationU3Ek__BackingField_4(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3CpathU3Ek__BackingField_5(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3CsplineU3Ek__BackingField_6(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_animationCurve_7(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_initFrameCount_8(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_color_9(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3CltRectU3Ek__BackingField_10(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConUpdateFloatU3Ek__BackingField_11(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConUpdateFloatRatioU3Ek__BackingField_12(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConUpdateFloatObjectU3Ek__BackingField_13(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConUpdateVector2U3Ek__BackingField_14(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConUpdateVector3U3Ek__BackingField_15(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConUpdateVector3ObjectU3Ek__BackingField_16(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConUpdateColorU3Ek__BackingField_17(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConUpdateColorObjectU3Ek__BackingField_18(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConCompleteU3Ek__BackingField_19(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConCompleteObjectU3Ek__BackingField_20(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConCompleteParamU3Ek__BackingField_21(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConUpdateParamU3Ek__BackingField_22(),
	LTDescrOptional_t588EC8F737F42C3A4113E515F4A80CE334ED5DA2::get_offset_of_U3ConStartU3Ek__BackingField_23(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3385[9] = 
{
	LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222::get_offset_of_previous_0(),
	LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222::get_offset_of_current_1(),
	LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222::get_offset_of_tween_2(),
	LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222::get_offset_of_totalDelay_3(),
	LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222::get_offset_of_timeScale_4(),
	LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222::get_offset_of_debugIter_5(),
	LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222::get_offset_of_counter_6(),
	LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222::get_offset_of_toggle_7(),
	LTSeq_tB84973C47F9384BFC34A4F8D1D74A3B622FDF222::get_offset_of__id_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3386[2] = 
{
	U3CU3Ec__DisplayClass19_0_t8F539BCA90B9E5930ECA2AC7790897B05CD50FC7::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass19_0_t8F539BCA90B9E5930ECA2AC7790897B05CD50FC7::get_offset_of_callback_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3387[2] = 
{
	U3CU3Ec__DisplayClass21_0_tC1A4F6BBD31A2498ECF79C6D507777B1C8612889::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass21_0_tC1A4F6BBD31A2498ECF79C6D507777B1C8612889::get_offset_of_callback_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3388[15] = 
{
	CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A::get_offset_of_fireworks_4(),
	CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A::get_offset_of_ribbon_5(),
	CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A::get_offset_of_title_6(),
	CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A::get_offset_of_board_7(),
	CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A::get_offset_of_canvas_8(),
	CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A::get_offset_of_innerCanvas_9(),
	CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A::get_offset_of__title_10(),
	CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A::get_offset_of__board_11(),
	CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A::get_offset_of__ribbonWidth_12(),
	CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A::get_offset_of__image_13(),
	CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A::get_offset_of__particle_14(),
	CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A::get_offset_of__glowScript_15(),
	CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A::get_offset_of__titleShowPosition_16(),
	CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A::get_offset_of__boardShowPosition_17(),
	CSAlert_t1DE276C4EE8E42CD9F205DA599DDA4B0493BA07A::get_offset_of__active_18(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3389[3] = 
{
	CSAlertRewardAnim_t13B8641467B4284296D29B346C9760D59E21E9E0::get_offset_of_coinPanel_19(),
	CSAlertRewardAnim_t13B8641467B4284296D29B346C9760D59E21E9E0::get_offset_of_rewardLabel_20(),
	CSAlertRewardAnim_t13B8641467B4284296D29B346C9760D59E21E9E0::get_offset_of__reward_21(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3390[5] = 
{
	CSLevelUpAlert_tEF4E42E838D5DB762ABB678F2A29B38E6FD491CB::get_offset_of__level_22(),
	CSLevelUpAlert_tEF4E42E838D5DB762ABB678F2A29B38E6FD491CB::get_offset_of_level_23(),
	CSLevelUpAlert_tEF4E42E838D5DB762ABB678F2A29B38E6FD491CB::get_offset_of__levelUpText_24(),
	CSLevelUpAlert_tEF4E42E838D5DB762ABB678F2A29B38E6FD491CB::get_offset_of__stars_25(),
	CSLevelUpAlert_tEF4E42E838D5DB762ABB678F2A29B38E6FD491CB::get_offset_of__starPos_26(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3391[3] = 
{
	StarData_t2FA473959759AC85532D9EC701C3715E28D2219E::get_offset_of_position_0(),
	StarData_t2FA473959759AC85532D9EC701C3715E28D2219E::get_offset_of_rotation_1(),
	StarData_t2FA473959759AC85532D9EC701C3715E28D2219E::get_offset_of_color_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3392[3] = 
{
	CSCSBlock_t46BF4D64718D4116A97602551C8D8A7EE394E14A::get_offset_of_blick_4(),
	CSCSBlock_t46BF4D64718D4116A97602551C8D8A7EE394E14A::get_offset_of__rectTransform_5(),
	CSCSBlock_t46BF4D64718D4116A97602551C8D8A7EE394E14A::get_offset_of__animate_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3393[5] = 
{
	CSCSBottomPanel_tA2ABC8CC6C066B3AEA8E2F2BF5C88AF9DAE79726::get_offset_of_gamble_17(),
	CSCSBottomPanel_tA2ABC8CC6C066B3AEA8E2F2BF5C88AF9DAE79726::get_offset_of_gambleGame_18(),
	CSCSBottomPanel_tA2ABC8CC6C066B3AEA8E2F2BF5C88AF9DAE79726::get_offset_of_reels_19(),
	CSCSBottomPanel_tA2ABC8CC6C066B3AEA8E2F2BF5C88AF9DAE79726::get_offset_of__startPosition_20(),
	CSCSBottomPanel_tA2ABC8CC6C066B3AEA8E2F2BF5C88AF9DAE79726::get_offset_of__showGamble_21(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3394[4] = 
{
	CSCSFreeGameInterface_t77251F89137DEC1449785ADDF44AEDE47985BEC8::get_offset_of_freeGameSprite_4(),
	CSCSFreeGameInterface_t77251F89137DEC1449785ADDF44AEDE47985BEC8::get_offset_of_reels_5(),
	CSCSFreeGameInterface_t77251F89137DEC1449785ADDF44AEDE47985BEC8::get_offset_of__gamePlaySprite_6(),
	CSCSFreeGameInterface_t77251F89137DEC1449785ADDF44AEDE47985BEC8::get_offset_of__image_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3395[6] = 
{
	CSCSFreeGamePanel_tDC8A9E56CC4EEB6094171E5DF29FB0D31A409A32::get_offset_of_totalBetLabel_10(),
	CSCSFreeGamePanel_tDC8A9E56CC4EEB6094171E5DF29FB0D31A409A32::get_offset_of__totalBet_11(),
	CSCSFreeGamePanel_tDC8A9E56CC4EEB6094171E5DF29FB0D31A409A32::get_offset_of_multiplierGameObject_12(),
	CSCSFreeGamePanel_tDC8A9E56CC4EEB6094171E5DF29FB0D31A409A32::get_offset_of__muliplierEnable_13(),
	CSCSFreeGamePanel_tDC8A9E56CC4EEB6094171E5DF29FB0D31A409A32::get_offset_of_multiplierLabel_14(),
	CSCSFreeGamePanel_tDC8A9E56CC4EEB6094171E5DF29FB0D31A409A32::get_offset_of__multiplier_15(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3397[3] = 
{
	CSLoadingState_t34C48CC058E619064852C5A2B460F068EF95B644::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3398[2] = 
{
	U3CU3Ec_tE219C1852668FC6947326874826AFCD61DFDD86B_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tE219C1852668FC6947326874826AFCD61DFDD86B_StaticFields::get_offset_of_U3CU3E9__4_1_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3399[2] = 
{
	CSCSLoading_tA5936CA8227F956F2FBA41BEDC75DE00A57BAB2A::get_offset_of_progressBar_4(),
	CSCSLoading_tA5936CA8227F956F2FBA41BEDC75DE00A57BAB2A_StaticFields::get_offset_of_state_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3400[3] = 
{
	CSCSBonusGameSettings_t6311F6E406F6804E8E84F28F2155273091F40076::get_offset_of_freeSpins_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CSCSBonusGameSettings_t6311F6E406F6804E8E84F28F2155273091F40076::get_offset_of_coins_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CSCSBonusGameSettings_t6311F6E406F6804E8E84F28F2155273091F40076::get_offset_of_multiplier_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3401[2] = 
{
	CSCSReels_tF384AA60A9E6FA432BA6D99169CCCA17C7ACECD4::get_offset_of_alert_19(),
	CSCSReels_tF384AA60A9E6FA432BA6D99169CCCA17C7ACECD4::get_offset_of__settings_20(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3403[1] = 
{
	CSCSTopPanel_tE86FE3383C370373A110DAE0A65274B7A9FBFBD8::get_offset_of_info_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3404[3] = 
{
	CSCSWinAlert_tE1DC42587F7A4AF4EE8AA718FAB6269314AB6B79::get_offset_of_boardScript_22(),
	CSCSWinAlert_tE1DC42587F7A4AF4EE8AA718FAB6269314AB6B79::get_offset_of_reels_23(),
	CSCSWinAlert_tE1DC42587F7A4AF4EE8AA718FAB6269314AB6B79::get_offset_of__settings_24(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3405[2] = 
{
	U3CU3Ec__DisplayClass25_0_t9017258DBD08C6FFAA044EA2B01242385746C17F::get_offset_of_color_0(),
	U3CU3Ec__DisplayClass25_0_t9017258DBD08C6FFAA044EA2B01242385746C17F::get_offset_of_image_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3406[11] = 
{
	CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D::get_offset_of_coinsLabel_4(),
	CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D::get_offset_of_coinPanel_5(),
	CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D::get_offset_of_duration_6(),
	CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D::get_offset_of_board_7(),
	CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D::get_offset_of__background_8(),
	CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D::get_offset_of__canvas_9(),
	CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D::get_offset_of__scaleId_10(),
	CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D::get_offset_of__alphaId_11(),
	CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D::get_offset_of__alphaBoardId_12(),
	CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D::get_offset_of__active_13(),
	CSGameStore_tCB665DC8088BA2D4E712B8AA2CB94C3CEBA73F7D::get_offset_of__coins_14(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3407[6] = 
{
	CSLFBGAlertBoard_t6EF319BFC3C51BF1443743AFC7030B7566305D45::get_offset_of_multiplierGameObject_4(),
	CSLFBGAlertBoard_t6EF319BFC3C51BF1443743AFC7030B7566305D45::get_offset_of_multiplierLabel_5(),
	CSLFBGAlertBoard_t6EF319BFC3C51BF1443743AFC7030B7566305D45::get_offset_of__multiplier_6(),
	CSLFBGAlertBoard_t6EF319BFC3C51BF1443743AFC7030B7566305D45::get_offset_of_freeSpinsGameObject_7(),
	CSLFBGAlertBoard_t6EF319BFC3C51BF1443743AFC7030B7566305D45::get_offset_of_freeSpinsLabel_8(),
	CSLFBGAlertBoard_t6EF319BFC3C51BF1443743AFC7030B7566305D45::get_offset_of__freeSpins_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3408[3] = 
{
	CSLFProgressBar_t2236B83E6C32E322F6C3707BEEAD2FC5EE9F2035::get_offset_of__value_4(),
	CSLFProgressBar_t2236B83E6C32E322F6C3707BEEAD2FC5EE9F2035::get_offset_of_bar_5(),
	CSLFProgressBar_t2236B83E6C32E322F6C3707BEEAD2FC5EE9F2035::get_offset_of_text_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3409[3] = 
{
	CSStoreCoinPack_tF11AD02E4445D9C591F431372635667C4B9D6551::get_offset_of_price_4(),
	CSStoreCoinPack_tF11AD02E4445D9C591F431372635667C4B9D6551::get_offset_of_total_5(),
	CSStoreCoinPack_tF11AD02E4445D9C591F431372635667C4B9D6551::get_offset_of_product_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3410[2] = 
{
	U3CU3Ec__DisplayClass18_0_t880A5064739A67D17371896B595087DF248DCBB7::get_offset_of_label_0(),
	U3CU3Ec__DisplayClass18_0_t880A5064739A67D17371896B595087DF248DCBB7::get_offset_of_U3CU3E4__this_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3411[6] = 
{
	CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029::get_offset_of_bankValueChanged_4(),
	CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029::get_offset_of_text_5(),
	CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029::get_offset_of_particle_6(),
	CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029::get_offset_of_coinIcon_7(),
	CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029::get_offset_of_formatText_8(),
	CSBankCoinPanel_t2331E4E95D8C86A8B98DA00BB090EC3449FF4029::get_offset_of__bank_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3412[13] = 
{
	CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38::get_offset_of_coins_4(),
	CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38::get_offset_of_spinButton_5(),
	CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38::get_offset_of_betLabel_6(),
	CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38::get_offset_of__totalBet_7(),
	CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38::get_offset_of_winLabel_8(),
	CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38::get_offset_of__win_9(),
	CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38::get_offset_of_minBet_10(),
	CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38::get_offset_of_maxStep_11(),
	CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38::get_offset_of_xp_12(),
	CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38::get_offset_of__step_13(),
	CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38::get_offset_of__enableSpin_14(),
	CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38::get_offset_of__buttons_15(),
	CSBottomPanel_t6D6A369AB7889E1CD3A353130FF1F62AE05B4D38::get_offset_of__enable_16(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3413[5] = 
{
	CSSuit_tAA75CE75D9A835173D3678A1607BF238058BCC02::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3414[14] = 
{
	CSRank_tAD01A3314E9998618FECABE21AE478ADA68673BC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3415[2] = 
{
	CSCardValue_tEDFD24331AA54328DA51DA03E0123741F8729D38::get_offset_of_suit_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CSCardValue_tEDFD24331AA54328DA51DA03E0123741F8729D38::get_offset_of_rank_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3416[8] = 
{
	CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2::get_offset_of_cardDatas_4(),
	CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2::get_offset_of_selectedEvent_5(),
	CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2::get_offset_of__backSprite_6(),
	CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2::get_offset_of__image_7(),
	CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2::get_offset_of__botton_8(),
	CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2::get_offset_of__data_9(),
	CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2::get_offset_of__flip_10(),
	CSCard_t2D73E1C1F7A5EE354F95BB798BBFD300141559D2::get_offset_of__cardValue_11(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3417[2] = 
{
	CSCardData_tD1BD9207FB0542F11285FAA8DAC8A02F88C6C9B7::get_offset_of_sprite_4(),
	CSCardData_tD1BD9207FB0542F11285FAA8DAC8A02F88C6C9B7::get_offset_of_value_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3418[6] = 
{
	U3CU3Ec__DisplayClass21_0_tFE6AAC70CEF4F120217A108769FDB473CF53C25E::get_offset_of_v_0(),
	U3CU3Ec__DisplayClass21_0_tFE6AAC70CEF4F120217A108769FDB473CF53C25E::get_offset_of_prev_1(),
	U3CU3Ec__DisplayClass21_0_tFE6AAC70CEF4F120217A108769FDB473CF53C25E::get_offset_of_m_2(),
	U3CU3Ec__DisplayClass21_0_tFE6AAC70CEF4F120217A108769FDB473CF53C25E::get_offset_of_l_3(),
	U3CU3Ec__DisplayClass21_0_tFE6AAC70CEF4F120217A108769FDB473CF53C25E::get_offset_of_U3CU3E4__this_4(),
	U3CU3Ec__DisplayClass21_0_tFE6AAC70CEF4F120217A108769FDB473CF53C25E::get_offset_of_curLevel_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3419[13] = 
{
	CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F::get_offset_of_progress_4(),
	CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F::get_offset_of_bar_5(),
	CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F::get_offset_of_partilcePrefab_6(),
	CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F::get_offset_of_icon_7(),
	CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F::get_offset_of_power_8(),
	CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F::get_offset_of_increasePercent_9(),
	CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F::get_offset_of_levelText_10(),
	CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F::get_offset_of_percentText_11(),
	CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F::get_offset_of_alert_12(),
	CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F::get_offset_of__size_13(),
	CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F::get_offset_of__max_14(),
	CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F::get_offset_of__xp_15(),
	CSExperiencePanel_t62B5AE339B7E60E2C379E594FF095586F8FDE95F::get_offset_of__level_16(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3420[1] = 
{
	CSFindMatchManager_tB268492BDB31FEA9BB0A2B1C1883985E21485B90_StaticFields::get_offset_of_instance_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3421[1] = 
{
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3422[4] = 
{
	CSPayline_t3562100776857062BF81F66EEEBBF159B5772FCF::get_offset_of_line_0(),
	CSPayline_t3562100776857062BF81F66EEEBBF159B5772FCF::get_offset_of_type_1(),
	CSPayline_t3562100776857062BF81F66EEEBBF159B5772FCF::get_offset_of_symbols_2(),
	CSPayline_t3562100776857062BF81F66EEEBBF159B5772FCF::get_offset_of_win_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3423[1] = 
{
	CSScatterPlayLine_t18E8F2CA43C352FAAF3B96A656764BE385FCE0F0::get_offset_of_runned_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3424[6] = 
{
	CSFreeGamePanel_tC3C7EE40E9C939B3B63EFCE22621ED5792C3D7E6::get_offset_of_freeSpinGameObject_4(),
	CSFreeGamePanel_tC3C7EE40E9C939B3B63EFCE22621ED5792C3D7E6::get_offset_of__freeSpinEnable_5(),
	CSFreeGamePanel_tC3C7EE40E9C939B3B63EFCE22621ED5792C3D7E6::get_offset_of_freeSpinsLabel_6(),
	CSFreeGamePanel_tC3C7EE40E9C939B3B63EFCE22621ED5792C3D7E6::get_offset_of__freeSpins_7(),
	CSFreeGamePanel_tC3C7EE40E9C939B3B63EFCE22621ED5792C3D7E6::get_offset_of_winLabel_8(),
	CSFreeGamePanel_tC3C7EE40E9C939B3B63EFCE22621ED5792C3D7E6::get_offset_of__win_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3426[4] = 
{
	CSGameManager_t5E0B6D2E1EE4AE937D9E6A35B3EBD72CE0B2BAB1_StaticFields::get_offset_of_instance_4(),
	CSGameManager_t5E0B6D2E1EE4AE937D9E6A35B3EBD72CE0B2BAB1::get_offset_of_androidAppLink_5(),
	CSGameManager_t5E0B6D2E1EE4AE937D9E6A35B3EBD72CE0B2BAB1::get_offset_of_iosAppLink_6(),
	CSGameManager_t5E0B6D2E1EE4AE937D9E6A35B3EBD72CE0B2BAB1::get_offset_of_supportEmail_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3427[3] = 
{
	CSGameSettings_t150C9D4055C2B31EC9689A935814614FF0365AF2_StaticFields::get_offset_of_instance_4(),
	CSGameSettings_t150C9D4055C2B31EC9689A935814614FF0365AF2::get_offset_of_lastWin_5(),
	CSGameSettings_t150C9D4055C2B31EC9689A935814614FF0365AF2::get_offset_of_data_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3428[14] = 
{
	CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04::get_offset_of_sound_0(),
	CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04::get_offset_of_music_1(),
	CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04::get_offset_of_highscore_2(),
	CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04::get_offset_of_playcount_3(),
	CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04::get_offset_of_ads_4(),
	CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04::get_offset_of_coins_5(),
	CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04::get_offset_of_installDate_6(),
	CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04::get_offset_of_timers_7(),
	CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04::get_offset_of_xp_8(),
	CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04::get_offset_of_level_9(),
	CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04::get_offset_of_installDays_10(),
	CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04::get_offset_of_selectedTheme_11(),
	CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04::get_offset_of_classicSevenBetStep_12(),
	CSGameData_t6837245AD52A4D893CBEA1AE60FA7AAE5EA5CC04::get_offset_of_volume_13(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3429[1] = 
{
	U3CU3Ec__DisplayClass8_0_t4BC79262157EE77EC1C80AD29B85A9CA0EBBB225::get_offset_of_builder_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3430[2] = 
{
	U3CU3Ec_t33621A848112F597C3E83557E9C2E45798792FD9_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t33621A848112F597C3E83557E9C2E45798792FD9_StaticFields::get_offset_of_U3CU3E9__15_0_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3431[6] = 
{
	CSIAPManager_t0F7D227EFEEBC9951CD4D135B5F6299C67906899::get_offset_of_handleSuccessPurchase_4(),
	CSIAPManager_t0F7D227EFEEBC9951CD4D135B5F6299C67906899::get_offset_of_handleFaliedPurchase_5(),
	CSIAPManager_t0F7D227EFEEBC9951CD4D135B5F6299C67906899::get_offset_of_products_6(),
	CSIAPManager_t0F7D227EFEEBC9951CD4D135B5F6299C67906899_StaticFields::get_offset_of_m_StoreController_7(),
	CSIAPManager_t0F7D227EFEEBC9951CD4D135B5F6299C67906899_StaticFields::get_offset_of_m_StoreExtensionProvider_8(),
	CSIAPManager_t0F7D227EFEEBC9951CD4D135B5F6299C67906899_StaticFields::get_offset_of_instance_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3432[3] = 
{
	CSIAPProduct_tF735AB0F8AA68F1481FA63E8AF5EB9AF02F7EAA2::get_offset_of_productId_4(),
	CSIAPProduct_tF735AB0F8AA68F1481FA63E8AF5EB9AF02F7EAA2::get_offset_of_type_5(),
	CSIAPProduct_tF735AB0F8AA68F1481FA63E8AF5EB9AF02F7EAA2::get_offset_of_coins_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3433[2] = 
{
	U3CU3Ec__DisplayClass17_0_tEB2C08B0350F5EF9B0D4987C5D7758335B4E8EA4::get_offset_of_color_0(),
	U3CU3Ec__DisplayClass17_0_tEB2C08B0350F5EF9B0D4987C5D7758335B4E8EA4::get_offset_of_image_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3434[8] = 
{
	CSInfo_tD22B0C9AD79CB449BCBC64512307F5EFAE046AA1::get_offset_of_duration_4(),
	CSInfo_tD22B0C9AD79CB449BCBC64512307F5EFAE046AA1::get_offset_of_board_5(),
	CSInfo_tD22B0C9AD79CB449BCBC64512307F5EFAE046AA1::get_offset_of__background_6(),
	CSInfo_tD22B0C9AD79CB449BCBC64512307F5EFAE046AA1::get_offset_of__canvas_7(),
	CSInfo_tD22B0C9AD79CB449BCBC64512307F5EFAE046AA1::get_offset_of__scaleId_8(),
	CSInfo_tD22B0C9AD79CB449BCBC64512307F5EFAE046AA1::get_offset_of__alphaId_9(),
	CSInfo_tD22B0C9AD79CB449BCBC64512307F5EFAE046AA1::get_offset_of__alphaBoardId_10(),
	CSInfo_tD22B0C9AD79CB449BCBC64512307F5EFAE046AA1::get_offset_of__active_11(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3435[3] = 
{
	CSLine_t3E62A34AE974F1DA0D692280F027A34103D5214C::get_offset_of_number_4(),
	CSLine_t3E62A34AE974F1DA0D692280F027A34103D5214C::get_offset_of_sprite_5(),
	CSLine_t3E62A34AE974F1DA0D692280F027A34103D5214C::get_offset_of_line_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3436[3] = 
{
	CSMenuToggle_t3572C0C8BADDD21482DA7B799E56C0403AD3516E::get_offset_of_miniSettings_4(),
	CSMenuToggle_t3572C0C8BADDD21482DA7B799E56C0403AD3516E::get_offset_of__toggle_5(),
	CSMenuToggle_t3572C0C8BADDD21482DA7B799E56C0403AD3516E::get_offset_of__toggleMask_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3437[1] = 
{
	U3CU3Ec__DisplayClass16_0_t36ED014080145BD25341EBC04BC5C89BD97234E4::get_offset_of_callback_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3438[7] = 
{
	U3CU3Ec__DisplayClass17_0_t32F44E19E309C7E6C4B68A468C4CA94053003A60::get_offset_of_h_0(),
	U3CU3Ec__DisplayClass17_0_t32F44E19E309C7E6C4B68A468C4CA94053003A60::get_offset_of_U3CU3E4__this_1(),
	U3CU3Ec__DisplayClass17_0_t32F44E19E309C7E6C4B68A468C4CA94053003A60::get_offset_of_prev_2(),
	U3CU3Ec__DisplayClass17_0_t32F44E19E309C7E6C4B68A468C4CA94053003A60::get_offset_of_min_3(),
	U3CU3Ec__DisplayClass17_0_t32F44E19E309C7E6C4B68A468C4CA94053003A60::get_offset_of_max_4(),
	U3CU3Ec__DisplayClass17_0_t32F44E19E309C7E6C4B68A468C4CA94053003A60::get_offset_of_lastRoll_5(),
	U3CU3Ec__DisplayClass17_0_t32F44E19E309C7E6C4B68A468C4CA94053003A60::get_offset_of_count_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3439[8] = 
{
	CSReel_t8454490940D72C04785CF2BF71C41073113E1890::get_offset_of_symbol_4(),
	CSReel_t8454490940D72C04785CF2BF71C41073113E1890::get_offset_of__tileSize_5(),
	CSReel_t8454490940D72C04785CF2BF71C41073113E1890::get_offset_of__count_6(),
	CSReel_t8454490940D72C04785CF2BF71C41073113E1890::get_offset_of__symbols_7(),
	CSReel_t8454490940D72C04785CF2BF71C41073113E1890::get_offset_of__size_8(),
	CSReel_t8454490940D72C04785CF2BF71C41073113E1890::get_offset_of__column_9(),
	CSReel_t8454490940D72C04785CF2BF71C41073113E1890::get_offset_of_symbolEnabled_10(),
	CSReel_t8454490940D72C04785CF2BF71C41073113E1890::get_offset_of__reelRandom_11(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3440[6] = 
{
	CSReelAutoSpin_tE3C4581E7195FF30D21E5E5EFDE1BA9344C385E4::get_offset_of_normalSprite_4(),
	CSReelAutoSpin_tE3C4581E7195FF30D21E5E5EFDE1BA9344C385E4::get_offset_of_state_5(),
	CSReelAutoSpin_tE3C4581E7195FF30D21E5E5EFDE1BA9344C385E4::get_offset_of_holdDuration_6(),
	CSReelAutoSpin_tE3C4581E7195FF30D21E5E5EFDE1BA9344C385E4::get_offset_of__startTime_7(),
	CSReelAutoSpin_tE3C4581E7195FF30D21E5E5EFDE1BA9344C385E4::get_offset_of__start_8(),
	CSReelAutoSpin_tE3C4581E7195FF30D21E5E5EFDE1BA9344C385E4::get_offset_of__reels_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3441[2] = 
{
	CSReelRandom_t16B21B9455802008C805A265B104CD5F2D02423B::get_offset_of_symbols_0(),
	CSReelRandom_t16B21B9455802008C805A265B104CD5F2D02423B::get_offset_of__totalChance_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3443[15] = 
{
	CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47::get_offset_of_FreeGameValueChangedEvent_4(),
	CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47::get_offset_of_reel_5(),
	CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47::get_offset_of_lines_6(),
	CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47::get_offset_of_basePanel_7(),
	CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47::get_offset_of_freeGamePanel_8(),
	CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47::get_offset_of_scrollView_9(),
	CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47::get_offset_of_topPanel_10(),
	CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47::get_offset_of_rewardAlert_11(),
	CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47::get_offset_of_reels_12(),
	CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47::get_offset_of__tileSize_13(),
	CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47::get_offset_of__autoSpin_14(),
	CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47::get_offset_of__stateAutoSpin_15(),
	CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47::get_offset_of__enable_16(),
	CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47::get_offset_of__reelAnimation_17(),
	CSReels_t0C000EF193C44F1E573B0682F44E9771542E5B47::get_offset_of__freeGame_18(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3444[4] = 
{
	U3CAnimatePaylinesU3Ed__7_t6E550BE059940F53440A4AB80C861629B3C68FE6::get_offset_of_U3CU3E1__state_0(),
	U3CAnimatePaylinesU3Ed__7_t6E550BE059940F53440A4AB80C861629B3C68FE6::get_offset_of_U3CU3E2__current_1(),
	U3CAnimatePaylinesU3Ed__7_t6E550BE059940F53440A4AB80C861629B3C68FE6::get_offset_of_U3CU3E4__this_2(),
	U3CAnimatePaylinesU3Ed__7_t6E550BE059940F53440A4AB80C861629B3C68FE6::get_offset_of_paylines_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3445[7] = 
{
	U3CAnimatePaylinesFreeGameU3Ed__8_t54D4BB7E0BA22FAA724545794C17ECAED600F58C::get_offset_of_U3CU3E1__state_0(),
	U3CAnimatePaylinesFreeGameU3Ed__8_t54D4BB7E0BA22FAA724545794C17ECAED600F58C::get_offset_of_U3CU3E2__current_1(),
	U3CAnimatePaylinesFreeGameU3Ed__8_t54D4BB7E0BA22FAA724545794C17ECAED600F58C::get_offset_of_U3CU3E4__this_2(),
	U3CAnimatePaylinesFreeGameU3Ed__8_t54D4BB7E0BA22FAA724545794C17ECAED600F58C::get_offset_of_paylines_3(),
	U3CAnimatePaylinesFreeGameU3Ed__8_t54D4BB7E0BA22FAA724545794C17ECAED600F58C::get_offset_of_callback_4(),
	U3CAnimatePaylinesFreeGameU3Ed__8_t54D4BB7E0BA22FAA724545794C17ECAED600F58C::get_offset_of_U3CmultiplierU3E5__2_5(),
	U3CAnimatePaylinesFreeGameU3Ed__8_t54D4BB7E0BA22FAA724545794C17ECAED600F58C::get_offset_of_U3CiU3E5__3_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3446[4] = 
{
	CSReelsAnimation_t04EC01AF43D2016CB036755318F8873C79F9714D::get_offset_of__reels_4(),
	CSReelsAnimation_t04EC01AF43D2016CB036755318F8873C79F9714D::get_offset_of__line_5(),
	CSReelsAnimation_t04EC01AF43D2016CB036755318F8873C79F9714D::get_offset_of__paylineCoroutine_6(),
	CSReelsAnimation_t04EC01AF43D2016CB036755318F8873C79F9714D::get_offset_of__paylines_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3447[13] = 
{
	CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395_StaticFields::get_offset_of_instance_4(),
	CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395::get_offset_of__callback_5(),
	CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395::get_offset_of__sceneAsync_6(),
	CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395::get_offset_of__assetAsync_7(),
	CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395::get_offset_of__minsec_8(),
	CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395::get_offset_of__elapsed_9(),
	CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395::get_offset_of__sceneName_10(),
	CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395::get_offset_of__sceneIdx_11(),
	CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395::get_offset_of__sceneProgress_12(),
	CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395::get_offset_of__assetProgress_13(),
	CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395::get_offset_of__sceneComplete_14(),
	CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395::get_offset_of__assetComplete_15(),
	CSSceneManager_tA39C3A53B3723432B8F0F9B7E5D2FEDEF1E34395::get_offset_of__enable_16(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3448[2] = 
{
	U3CU3Ec__DisplayClass21_0_t1469F90E7A3CCE2352886D7ADB52106B3D49C40D::get_offset_of_color_0(),
	U3CU3Ec__DisplayClass21_0_t1469F90E7A3CCE2352886D7ADB52106B3D49C40D::get_offset_of_image_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3449[11] = 
{
	CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842::get_offset_of_duration_4(),
	CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842::get_offset_of_board_5(),
	CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842::get_offset_of_fbInvite_6(),
	CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842::get_offset_of_fbLogin_7(),
	CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842::get_offset_of_coinPanel_8(),
	CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842::get_offset_of__background_9(),
	CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842::get_offset_of__canvas_10(),
	CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842::get_offset_of__scaleId_11(),
	CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842::get_offset_of__alphaId_12(),
	CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842::get_offset_of__alphaBoardId_13(),
	CSSettingsPanel_t4F8FB2B13B81A9E99996E73594F57A8BBF97E842::get_offset_of__active_14(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3450[4] = 
{
	CSSettingsPanelSmall_t436B210ADD3C25F979D822BD214FBDE286D44CA0::get_offset_of_settings_4(),
	CSSettingsPanelSmall_t436B210ADD3C25F979D822BD214FBDE286D44CA0::get_offset_of_duration_5(),
	CSSettingsPanelSmall_t436B210ADD3C25F979D822BD214FBDE286D44CA0::get_offset_of__canvasGroup_6(),
	CSSettingsPanelSmall_t436B210ADD3C25F979D822BD214FBDE286D44CA0::get_offset_of__show_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3451[4] = 
{
	U3CU3Ec_tBDA41C01FAABEE4BF31833F05FAC1849607F7751_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tBDA41C01FAABEE4BF31833F05FAC1849607F7751_StaticFields::get_offset_of_U3CU3E9__17_0_1(),
	U3CU3Ec_tBDA41C01FAABEE4BF31833F05FAC1849607F7751_StaticFields::get_offset_of_U3CU3E9__18_0_2(),
	U3CU3Ec_tBDA41C01FAABEE4BF31833F05FAC1849607F7751_StaticFields::get_offset_of_U3CU3E9__19_0_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3452[1] = 
{
	U3CU3Ec__DisplayClass20_0_t4C31C11F3EDCDD19943B5AD0555C102F58BAB0AF::get_offset_of_clipName_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3453[1] = 
{
	U3CU3Ec__DisplayClass21_0_t7FED1675E28C0A8A3F352F1EC92CE5D067447617::get_offset_of_value_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3454[3] = 
{
	CSSoundManager_t413846E33ECF342195F7334420A0F2BAE7B62736_StaticFields::get_offset_of_instance_4(),
	CSSoundManager_t413846E33ECF342195F7334420A0F2BAE7B62736::get_offset_of_sounds_5(),
	CSSoundManager_t413846E33ECF342195F7334420A0F2BAE7B62736::get_offset_of__playingMusic_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3455[4] = 
{
	Sound_tF983595F9C621A86B56E05F9778810369E90A0FE::get_offset_of_clip_0(),
	Sound_tF983595F9C621A86B56E05F9778810369E90A0FE::get_offset_of_loop_1(),
	Sound_tF983595F9C621A86B56E05F9778810369E90A0FE::get_offset_of_source_2(),
	Sound_tF983595F9C621A86B56E05F9778810369E90A0FE::get_offset_of_volume_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3456[15] = 
{
	CSSymbolType_tA08EC9FA0B5E0C098DA0501759A595E02935EC08::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3457[12] = 
{
	CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555::get_offset_of_data_4(),
	CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555::get_offset_of_curr_5(),
	CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555::get_offset_of_dictionaryData_6(),
	CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555::get_offset_of_type_7(),
	CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555::get_offset_of_cell_8(),
	CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555::get_offset_of_replacement_9(),
	CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555::get_offset_of_percents_10(),
	CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555::get_offset_of__image_11(),
	CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555::get_offset_of__animationId_12(),
	CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555::get_offset_of__particle_13(),
	CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555::get_offset_of__rect_14(),
	CSSymbol_tCB1EC9D0CE3DAA4D0F1AD7E2836E85827C0F2555::get_offset_of__totalChance_15(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3458[2] = 
{
	CSSymbolPercent_tB65FE49C56B898EB176325F3AA98A5C1A5BF340E::get_offset_of_percent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CSSymbolPercent_tB65FE49C56B898EB176325F3AA98A5C1A5BF340E::get_offset_of_type_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3459[5] = 
{
	CSSymbolData_t189F10C509C4A90E39640E27AA4367BCC44D5816::get_offset_of_type_4(),
	CSSymbolData_t189F10C509C4A90E39640E27AA4367BCC44D5816::get_offset_of_particle_5(),
	CSSymbolData_t189F10C509C4A90E39640E27AA4367BCC44D5816::get_offset_of_animationData_6(),
	CSSymbolData_t189F10C509C4A90E39640E27AA4367BCC44D5816::get_offset_of_sprite_7(),
	CSSymbolData_t189F10C509C4A90E39640E27AA4367BCC44D5816::get_offset_of_rule_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3460[3] = 
{
	CSAnimationData_t39AC7CF1C6BE740E5450F413840F4B8E08309645::get_offset_of_frames_0(),
	CSAnimationData_t39AC7CF1C6BE740E5450F413840F4B8E08309645::get_offset_of_duration_1(),
	CSAnimationData_t39AC7CF1C6BE740E5450F413840F4B8E08309645::get_offset_of_delay_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3461[2] = 
{
	CSRule_t57A6472E5A907AC0E251BCF987970B41AD1F0561::get_offset_of_reward_0(),
	CSRule_t57A6472E5A907AC0E251BCF987970B41AD1F0561::get_offset_of_substitutesForWild_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3462[3] = 
{
	CSTopPanel_t94C2B84ED826C2C503575317319F623658C0C4D8::get_offset_of_store_4(),
	CSTopPanel_t94C2B84ED826C2C503575317319F623658C0C4D8::get_offset_of_xpPanel_5(),
	CSTopPanel_t94C2B84ED826C2C503575317319F623658C0C4D8::get_offset_of_coinPanel_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3463[2] = 
{
	U3CU3Ec_t9179B94870D58F4201DEB8D7A718495A50CC068D_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t9179B94870D58F4201DEB8D7A718495A50CC068D_StaticFields::get_offset_of_U3CU3E9__15_0_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3464[12] = 
{
	CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8::get_offset_of_adMobAppIdiOS_4(),
	CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8::get_offset_of_adMobAppIdAndroid_5(),
	CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8::get_offset_of_adMobAppInterstitialiOS_6(),
	CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8::get_offset_of_adMobAppInterstitialAndroid_7(),
	CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8::get_offset_of_adMobAppRewardiOS_8(),
	CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8::get_offset_of_adMobAppRewardAndroid_9(),
	CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8_StaticFields::get_offset_of_instance_10(),
	CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8::get_offset_of__interstitial_11(),
	CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8::get_offset_of__live_12(),
	CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8::get_offset_of__rewardBasedVideo_13(),
	CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8::get_offset_of__RewardVideoCallback_14(),
	CSAdMobManager_tDAE44853B391C47744D4F8509924F3C64B8D33D8::get_offset_of__InterstitialCallback_15(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3465[2] = 
{
	U3CU3Ec_t6072D81B91FC0BA7572310D528126455DE9C73D8_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t6072D81B91FC0BA7572310D528126455DE9C73D8_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3467[2] = 
{
	CSCell_tEC2CF7F1268B4BE5C1155F2B9476449C3A14383D::get_offset_of_column_0(),
	CSCell_tEC2CF7F1268B4BE5C1155F2B9476449C3A14383D::get_offset_of_row_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3468[1] = 
{
	L2D_t890C859307906191E6AE39D17A20495B84F62703::get_offset_of_l1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3469[1] = 
{
	CS2DBoolArray_t4A5EA41B80365341FAB8F7B222392F23F5083434::get_offset_of_l0_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3470[1] = 
{
	CSEnumFlagAttribute_tC301B9CC7595F4CC930BD5F23373B22D42D7559F::get_offset_of_enumName_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3471[1] = 
{
	CSFPSDisplay_t6484CE94D5BAEFEA86EC6682EAC68AD825679FDB::get_offset_of_deltaTime_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3473[9] = 
{
	CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186::get_offset_of_location_4(),
	CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186::get_offset_of_width_5(),
	CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186::get_offset_of_duration_6(),
	CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186::get_offset_of_animateOnWake_7(),
	CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186::get_offset_of_repeat_8(),
	CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186::get_offset_of_ignoreTimeScale_9(),
	CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186::get_offset_of__material_10(),
	CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186::get_offset_of__actionId_11(),
	CSGlowAnimation_t7EE772933970FD9CCEE5EB5436315A3E32013186::get_offset_of__enable_12(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3474[1] = 
{
	List2D_tF34A588855096C9705F18D8CAAF87AA174FA71BD::get_offset_of_l1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3475[1] = 
{
	CSCardList2D_tB265A74FEF9F4D8A544A88653A96C54F1F5B66F5::get_offset_of_l0_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3476[2] = 
{
	U3CU3Ec__DisplayClass2_0_t9FF349AD8747383405A279B38F890A20822BE7C3::get_offset_of_item_0(),
	U3CU3Ec__DisplayClass2_0_t9FF349AD8747383405A279B38F890A20822BE7C3::get_offset_of_U3CU3E4__this_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3477[2] = 
{
	U3CU3Ec__DisplayClass3_0_t6630702C4953351F76E68CD3FBBFECD306EBD1E6::get_offset_of_item_0(),
	U3CU3Ec__DisplayClass3_0_t6630702C4953351F76E68CD3FBBFECD306EBD1E6::get_offset_of_U3CU3E4__this_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3478[2] = 
{
	CSSliderGroup_tDC34B5CDF947DBA827407A2B6D39F5E7ADC28F4C::get_offset_of_sliders_4(),
	CSSliderGroup_tDC34B5CDF947DBA827407A2B6D39F5E7ADC28F4C::get_offset_of_startValue_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3481[9] = 
{
	CSTimer_tA0601B382CE03F0C9C9A31E54C5EEDAD89CD7668::get_offset_of_timerTickEvent_0(),
	CSTimer_tA0601B382CE03F0C9C9A31E54C5EEDAD89CD7668::get_offset_of_timerStopEvent_1(),
	CSTimer_tA0601B382CE03F0C9C9A31E54C5EEDAD89CD7668::get_offset_of_startDate_2(),
	CSTimer_tA0601B382CE03F0C9C9A31E54C5EEDAD89CD7668::get_offset_of_endDate_3(),
	CSTimer_tA0601B382CE03F0C9C9A31E54C5EEDAD89CD7668::get_offset_of_time_4(),
	CSTimer_tA0601B382CE03F0C9C9A31E54C5EEDAD89CD7668::get_offset_of_key_5(),
	CSTimer_tA0601B382CE03F0C9C9A31E54C5EEDAD89CD7668::get_offset_of_percent_6(),
	CSTimer_tA0601B382CE03F0C9C9A31E54C5EEDAD89CD7668::get_offset_of__totalSec_7(),
	CSTimer_tA0601B382CE03F0C9C9A31E54C5EEDAD89CD7668::get_offset_of__running_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3483[3] = 
{
	CSTimerData_t4D5EF8636ACBC695F6934CEC7B653CD513CD1321::get_offset_of_startDate_0(),
	CSTimerData_t4D5EF8636ACBC695F6934CEC7B653CD513CD1321::get_offset_of_time_1(),
	CSTimerData_t4D5EF8636ACBC695F6934CEC7B653CD513CD1321::get_offset_of_key_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3485[5] = 
{
	CSTimerManager_t6F1C7120C836A3FC70D493E50F2F43B023E5F4EA::get_offset_of_TimerCreatedEvent_4(),
	CSTimerManager_t6F1C7120C836A3FC70D493E50F2F43B023E5F4EA_StaticFields::get_offset_of_instance_5(),
	CSTimerManager_t6F1C7120C836A3FC70D493E50F2F43B023E5F4EA::get_offset_of__timers_6(),
	CSTimerManager_t6F1C7120C836A3FC70D493E50F2F43B023E5F4EA::get_offset_of__add_7(),
	CSTimerManager_t6F1C7120C836A3FC70D493E50F2F43B023E5F4EA::get_offset_of__remove_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3486[2] = 
{
	U3CU3Ec__DisplayClass3_0_t6D6DFCE53002EDD377AB45444A01C94C56B4D51D::get_offset_of_tt_0(),
	U3CU3Ec__DisplayClass3_0_t6D6DFCE53002EDD377AB45444A01C94C56B4D51D::get_offset_of_U3CU3E4__this_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3487[3] = 
{
	CSToggleGroupHelper_t0B4982EA19DDA807327BDCF285651D4FB8C4551A::get_offset_of_source_list_4(),
	CSToggleGroupHelper_t0B4982EA19DDA807327BDCF285651D4FB8C4551A::get_offset_of__toggleListMember_5(),
	CSToggleGroupHelper_t0B4982EA19DDA807327BDCF285651D4FB8C4551A::get_offset_of_m_Group_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3488[2] = 
{
	U3CU3Ec_tAA9E5DE4FCDBFC55E762BEDDF4D98A3769F4F393_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tAA9E5DE4FCDBFC55E762BEDDF4D98A3769F4F393_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3490[4] = 
{
	CSTimeProperty_t880ACDE81B4949E6C83531AA6236E52D759F74D1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3491[5] = 
{
	U3CU3Ec__DisplayClass5_0_t2D1253B73E681503FD2FA9E03F5BCADF2379AF0C::get_offset_of_reverse_0(),
	U3CU3Ec__DisplayClass5_0_t2D1253B73E681503FD2FA9E03F5BCADF2379AF0C::get_offset_of_count_1(),
	U3CU3Ec__DisplayClass5_0_t2D1253B73E681503FD2FA9E03F5BCADF2379AF0C::get_offset_of_prev_2(),
	U3CU3Ec__DisplayClass5_0_t2D1253B73E681503FD2FA9E03F5BCADF2379AF0C::get_offset_of_image_3(),
	U3CU3Ec__DisplayClass5_0_t2D1253B73E681503FD2FA9E03F5BCADF2379AF0C::get_offset_of_animationData_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3492[3] = 
{
	U3CU3Ec__DisplayClass8_0_t8F8118C81CFC6E82AABAF5BE633AD6184F43C5AD::get_offset_of_label_0(),
	U3CU3Ec__DisplayClass8_0_t8F8118C81CFC6E82AABAF5BE633AD6184F43C5AD::get_offset_of_coin_1(),
	U3CU3Ec__DisplayClass8_0_t8F8118C81CFC6E82AABAF5BE633AD6184F43C5AD::get_offset_of_format_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3493[3] = 
{
	CSUtilities_tD1C88ACE4AB98642C64583DC0994B1341397001E_StaticFields::get_offset_of_min_0(),
	CSUtilities_tD1C88ACE4AB98642C64583DC0994B1341397001E_StaticFields::get_offset_of_idx_1(),
	CSUtilities_tD1C88ACE4AB98642C64583DC0994B1341397001E_StaticFields::get_offset_of_tokens_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3498[36] = 
{
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of__screensContainer_4(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of__isVertical_5(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of__screens_6(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of__scrollStartPosition_7(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of__childSize_8(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of__childPos_9(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of__maskSize_10(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of__childAnchorPoint_11(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of__scroll_rect_12(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of__lerp_target_13(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of__lerp_14(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of__pointerDown_15(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of__settled_16(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of__startPosition_17(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of__currentPage_18(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of__previousPage_19(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of__halfNoVisibleItems_20(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of__bottomItem_21(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of__topItem_22(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of_StartingScreen_23(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of_PageStep_24(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of_Pagination_25(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of_NextButton_26(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of_PrevButton_27(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of_transitionSpeed_28(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of_UseFastSwipe_29(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of_FastSwipeThreshold_30(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of_SwipeVelocityThreshold_31(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of_MaskArea_32(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of_MaskBuffer_33(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of_UseParentTransform_34(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of_ChildObjects_35(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of_ignoreTimeScale_36(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of_m_OnSelectionChangeStartEvent_37(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of_m_OnSelectionPageChangedEvent_38(),
	ScrollSnapBase_t7A5951B56C21C0943F38A7922B9F886B3EF72345::get_offset_of_m_OnSelectionChangeEndEvent_39(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3499[2] = 
{
	U3CU3Ec__DisplayClass26_0_tAAC530862CD320954956F11901125E5BC043CAAF::get_offset_of_callback_0(),
	U3CU3Ec__DisplayClass26_0_tAAC530862CD320954956F11901125E5BC043CAAF::get_offset_of_U3CU3E4__this_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3500[1] = 
{
	U3CU3Ec__DisplayClass27_0_t9F015BCA3E7B26527AC505BC00E81696F6F91E93::get_offset_of_callback_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3501[14] = 
{
	CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C::get_offset_of_cardParent_4(),
	CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C::get_offset_of_contentPrefab_5(),
	CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C::get_offset_of__content_6(),
	CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C::get_offset_of_background_7(),
	CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C::get_offset_of_canvas_8(),
	CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C::get_offset_of_bankText_9(),
	CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C::get_offset_of_betText_10(),
	CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C::get_offset_of_collectButton_11(),
	CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C::get_offset_of_alert_12(),
	CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C::get_offset_of_bottomPanel_13(),
	CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C::get_offset_of__bet_14(),
	CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C::get_offset_of__bank_15(),
	CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C::get_offset_of__interactable_16(),
	CSZLGamble_t83C7AF43844D12E5F08A7A1AFF73FF8D87B9B80C::get_offset_of__enable_17(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3502[1] = 
{
	CSZLGambleAlert_t4ADF9CEC8DA77AE4616BFE0ABBF6C3A17E86F231::get_offset_of_hudPanel_22(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3503[2] = 
{
	U3CU3Ec_t01DD19CD922EF68E93C6DB03BC98BAB04EE884B3_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t01DD19CD922EF68E93C6DB03BC98BAB04EE884B3_StaticFields::get_offset_of_U3CU3E9__17_0_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3504[3] = 
{
	U3CU3Ec__DisplayClass24_0_tCFDCFCD45FF9A7DE832D774051A4A4F7A94CBFA6::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass24_0_tCFDCFCD45FF9A7DE832D774051A4A4F7A94CBFA6::get_offset_of_first_1(),
	U3CU3Ec__DisplayClass24_0_tCFDCFCD45FF9A7DE832D774051A4A4F7A94CBFA6::get_offset_of_sender_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3505[2] = 
{
	U3CU3Ec__DisplayClass25_0_t1471EDB5201095CD33CA027507C8E9E78167B12E::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass25_0_t1471EDB5201095CD33CA027507C8E9E78167B12E::get_offset_of_win_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3506[8] = 
{
	CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D::get_offset_of_ResultEvent_4(),
	CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D::get_offset_of_CardSelectedEvent_5(),
	CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D::get_offset_of_cardDeck_6(),
	CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D::get_offset_of_cardPrefab_7(),
	CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D::get_offset_of__cards_8(),
	CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D::get_offset_of__canvas_9(),
	CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D::get_offset_of_space_10(),
	CSZLGambleContent_tCF8875458CD64BFA7B341730FF1EA34F0CAF645D::get_offset_of__enable_11(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3507[3] = 
{
	ChatController_tDC30FEF32E5CAE3B9A7502F307C6281C89F25061::get_offset_of_TMP_ChatInput_4(),
	ChatController_tDC30FEF32E5CAE3B9A7502F307C6281C89F25061::get_offset_of_TMP_ChatOutput_5(),
	ChatController_tDC30FEF32E5CAE3B9A7502F307C6281C89F25061::get_offset_of_ChatScrollbar_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3508[4] = 
{
	U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266::get_offset_of_U3CU3E4__this_2(),
	U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266::get_offset_of_U3CmatrixU3E5__2_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3509[3] = 
{
	EnvMapAnimator_t7C30A094A3786710A896AD9A6FA46B13EFEB72FD::get_offset_of_RotationSpeeds_4(),
	EnvMapAnimator_t7C30A094A3786710A896AD9A6FA46B13EFEB72FD::get_offset_of_m_textMeshPro_5(),
	EnvMapAnimator_t7C30A094A3786710A896AD9A6FA46B13EFEB72FD::get_offset_of_m_material_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3517[12] = 
{
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553::get_offset_of_m_OnCharacterSelection_4(),
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553::get_offset_of_m_OnSpriteSelection_5(),
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553::get_offset_of_m_OnWordSelection_6(),
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553::get_offset_of_m_OnLineSelection_7(),
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553::get_offset_of_m_OnLinkSelection_8(),
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553::get_offset_of_m_TextComponent_9(),
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553::get_offset_of_m_Camera_10(),
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553::get_offset_of_m_Canvas_11(),
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553::get_offset_of_m_selectedLink_12(),
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553::get_offset_of_m_lastCharIndex_13(),
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553::get_offset_of_m_lastWordIndex_14(),
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553::get_offset_of_m_lastLineIndex_15(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3518[4] = 
{
	U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640::get_offset_of_U3CU3E4__this_2(),
	U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640::get_offset_of_U3CiU3E5__2_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3519[10] = 
{
	Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934::get_offset_of_BenchmarkType_4(),
	Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934::get_offset_of_TMProFont_5(),
	Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934::get_offset_of_TextMeshFont_6(),
	Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934::get_offset_of_m_textMeshPro_7(),
	Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934::get_offset_of_m_textContainer_8(),
	Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934::get_offset_of_m_textMesh_9(),
	0,
	0,
	Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934::get_offset_of_m_material01_12(),
	Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934::get_offset_of_m_material02_13(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3520[4] = 
{
	U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3::get_offset_of_U3CU3E4__this_2(),
	U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3::get_offset_of_U3CiU3E5__2_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3521[10] = 
{
	Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B::get_offset_of_BenchmarkType_4(),
	Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B::get_offset_of_canvas_5(),
	Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B::get_offset_of_TMProFont_6(),
	Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B::get_offset_of_TextMeshFont_7(),
	Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B::get_offset_of_m_textMeshPro_8(),
	Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B::get_offset_of_m_textMesh_9(),
	0,
	0,
	Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B::get_offset_of_m_material01_12(),
	Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B::get_offset_of_m_material02_13(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3522[3] = 
{
	Benchmark02_tA0FA2BFB7AB5F7A8501D8DDFE0AF341C329663A1::get_offset_of_SpawnType_4(),
	Benchmark02_tA0FA2BFB7AB5F7A8501D8DDFE0AF341C329663A1::get_offset_of_NumberOfNPC_5(),
	Benchmark02_tA0FA2BFB7AB5F7A8501D8DDFE0AF341C329663A1::get_offset_of_floatingText_Script_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3523[3] = 
{
	Benchmark03_t8AC945A66AB915DDE86610B70E9FD1E1ED8FF3EA::get_offset_of_SpawnType_4(),
	Benchmark03_t8AC945A66AB915DDE86610B70E9FD1E1ED8FF3EA::get_offset_of_NumberOfNPC_5(),
	Benchmark03_t8AC945A66AB915DDE86610B70E9FD1E1ED8FF3EA::get_offset_of_TheFont_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3524[5] = 
{
	Benchmark04_t62DC44C021DD8E8057039D65F66E816CFE713D68::get_offset_of_SpawnType_4(),
	Benchmark04_t62DC44C021DD8E8057039D65F66E816CFE713D68::get_offset_of_MinPointSize_5(),
	Benchmark04_t62DC44C021DD8E8057039D65F66E816CFE713D68::get_offset_of_MaxPointSize_6(),
	Benchmark04_t62DC44C021DD8E8057039D65F66E816CFE713D68::get_offset_of_Steps_7(),
	Benchmark04_t62DC44C021DD8E8057039D65F66E816CFE713D68::get_offset_of_m_Transform_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3525[4] = 
{
	CameraModes_t86C030D336478B1715780ECE7AB12A629AD0ACE0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3526[25] = 
{
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_cameraTransform_4(),
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_dummyTarget_5(),
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_CameraTarget_6(),
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_FollowDistance_7(),
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_MaxFollowDistance_8(),
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_MinFollowDistance_9(),
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_ElevationAngle_10(),
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_MaxElevationAngle_11(),
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_MinElevationAngle_12(),
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_OrbitalAngle_13(),
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_CameraMode_14(),
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_MovementSmoothing_15(),
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_RotationSmoothing_16(),
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_previousSmoothing_17(),
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_MovementSmoothingValue_18(),
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_RotationSmoothingValue_19(),
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_MoveSensitivity_20(),
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_currentVelocity_21(),
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_desiredPosition_22(),
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_mouseX_23(),
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_mouseY_24(),
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_moveVector_25(),
	CameraController_tDF5613EA209FD0D5A5031FA5CBF05AEBEF441F6D::get_offset_of_mouseWheel_26(),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3527[4] = 
{
	MotionType_t7C60A9BB5280294C6FBC1DB5E747EFF94A342BF5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3528[10] = 
{
	ObjectSpin_t964A55502D58672749F39DBA3AD193FB11AA0AFA::get_offset_of_SpinSpeed_4(),
	ObjectSpin_t964A55502D58672749F39DBA3AD193FB11AA0AFA::get_offset_of_RotationRange_5(),
	ObjectSpin_t964A55502D58672749F39DBA3AD193FB11AA0AFA::get_offset_of_m_transform_6(),
	ObjectSpin_t964A55502D58672749F39DBA3AD193FB11AA0AFA::get_offset_of_m_time_7(),
	ObjectSpin_t964A55502D58672749F39DBA3AD193FB11AA0AFA::get_offset_of_m_prevPOS_8(),
	ObjectSpin_t964A55502D58672749F39DBA3AD193FB11AA0AFA::get_offset_of_m_initial_Rotation_9(),
	ObjectSpin_t964A55502D58672749F39DBA3AD193FB11AA0AFA::get_offset_of_m_initial_Position_10(),
	ObjectSpin_t964A55502D58672749F39DBA3AD193FB11AA0AFA::get_offset_of_m_lightColor_11(),
	ObjectSpin_t964A55502D58672749F39DBA3AD193FB11AA0AFA::get_offset_of_frames_12(),
	ObjectSpin_t964A55502D58672749F39DBA3AD193FB11AA0AFA::get_offset_of_Motion_13(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3529[3] = 
{
	U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D::get_offset_of_U3CU3E1__state_0(),
	U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D::get_offset_of_U3CU3E2__current_1(),
	U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D::get_offset_of_U3CU3E4__this_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3530[4] = 
{
	ShaderPropAnimator_t44FB2882ECA0D2C780444EB96B9550F5D245CFB8::get_offset_of_m_Renderer_4(),
	ShaderPropAnimator_t44FB2882ECA0D2C780444EB96B9550F5D245CFB8::get_offset_of_m_Material_5(),
	ShaderPropAnimator_t44FB2882ECA0D2C780444EB96B9550F5D245CFB8::get_offset_of_GlowCurve_6(),
	ShaderPropAnimator_t44FB2882ECA0D2C780444EB96B9550F5D245CFB8::get_offset_of_m_frame_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3531[3] = 
{
	SimpleScript_t961D2D3FD565D738420646A1EF42244EDD972121::get_offset_of_m_textMeshPro_4(),
	0,
	SimpleScript_t961D2D3FD565D738420646A1EF42244EDD972121::get_offset_of_m_frame_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3532[6] = 
{
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B::get_offset_of_U3CU3E1__state_0(),
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B::get_offset_of_U3CU3E2__current_1(),
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B::get_offset_of_U3CU3E4__this_2(),
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B::get_offset_of_U3Cold_CurveScaleU3E5__2_3(),
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B::get_offset_of_U3Cold_ShearValueU3E5__3_4(),
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B::get_offset_of_U3Cold_curveU3E5__4_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3533[4] = 
{
	SkewTextExample_tA9F165D1BE969ED73FB86CA6C641386C22F1EE07::get_offset_of_m_TextComponent_4(),
	SkewTextExample_tA9F165D1BE969ED73FB86CA6C641386C22F1EE07::get_offset_of_VertexCurve_5(),
	SkewTextExample_tA9F165D1BE969ED73FB86CA6C641386C22F1EE07::get_offset_of_CurveScale_6(),
	SkewTextExample_tA9F165D1BE969ED73FB86CA6C641386C22F1EE07::get_offset_of_ShearAmount_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3534[5] = 
{
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E::get_offset_of_U3CU3E4__this_2(),
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E::get_offset_of_U3CtotalVisibleCharactersU3E5__2_3(),
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E::get_offset_of_U3CcounterU3E5__3_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3535[3] = 
{
	TeleType_t92B5ED4FE818F912A562F085CE035F6B42834934::get_offset_of_label01_4(),
	TeleType_t92B5ED4FE818F912A562F085CE035F6B42834934::get_offset_of_label02_5(),
	TeleType_t92B5ED4FE818F912A562F085CE035F6B42834934::get_offset_of_m_textMeshPro_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3536[7] = 
{
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E::get_offset_of_U3CU3E1__state_0(),
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E::get_offset_of_U3CU3E2__current_1(),
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E::get_offset_of_textComponent_2(),
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E::get_offset_of_U3CU3E4__this_3(),
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E::get_offset_of_U3CtextInfoU3E5__2_4(),
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E::get_offset_of_U3CtotalVisibleCharactersU3E5__3_5(),
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E::get_offset_of_U3CvisibleCountU3E5__4_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3537[7] = 
{
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408::get_offset_of_U3CU3E1__state_0(),
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408::get_offset_of_U3CU3E2__current_1(),
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408::get_offset_of_textComponent_2(),
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408::get_offset_of_U3CtotalWordCountU3E5__2_3(),
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408::get_offset_of_U3CtotalVisibleCharactersU3E5__3_4(),
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408::get_offset_of_U3CcounterU3E5__4_5(),
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408::get_offset_of_U3CvisibleCountU3E5__5_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3538[2] = 
{
	TextConsoleSimulator_t82904A7668D46B20E576B30E4CE367C0B66C5289::get_offset_of_m_TextComponent_4(),
	TextConsoleSimulator_t82904A7668D46B20E576B30E4CE367C0B66C5289::get_offset_of_hasTextChanged_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3539[10] = 
{
	U3CDisplayTextMeshProFloatingTextU3Ed__12_t0F0EBDC5A3A2FF615C3205C8019AB8A81EDEF6FC::get_offset_of_U3CU3E1__state_0(),
	U3CDisplayTextMeshProFloatingTextU3Ed__12_t0F0EBDC5A3A2FF615C3205C8019AB8A81EDEF6FC::get_offset_of_U3CU3E2__current_1(),
	U3CDisplayTextMeshProFloatingTextU3Ed__12_t0F0EBDC5A3A2FF615C3205C8019AB8A81EDEF6FC::get_offset_of_U3CU3E4__this_2(),
	U3CDisplayTextMeshProFloatingTextU3Ed__12_t0F0EBDC5A3A2FF615C3205C8019AB8A81EDEF6FC::get_offset_of_U3CCountDurationU3E5__2_3(),
	U3CDisplayTextMeshProFloatingTextU3Ed__12_t0F0EBDC5A3A2FF615C3205C8019AB8A81EDEF6FC::get_offset_of_U3Cstarting_CountU3E5__3_4(),
	U3CDisplayTextMeshProFloatingTextU3Ed__12_t0F0EBDC5A3A2FF615C3205C8019AB8A81EDEF6FC::get_offset_of_U3Ccurrent_CountU3E5__4_5(),
	U3CDisplayTextMeshProFloatingTextU3Ed__12_t0F0EBDC5A3A2FF615C3205C8019AB8A81EDEF6FC::get_offset_of_U3Cstart_posU3E5__5_6(),
	U3CDisplayTextMeshProFloatingTextU3Ed__12_t0F0EBDC5A3A2FF615C3205C8019AB8A81EDEF6FC::get_offset_of_U3Cstart_colorU3E5__6_7(),
	U3CDisplayTextMeshProFloatingTextU3Ed__12_t0F0EBDC5A3A2FF615C3205C8019AB8A81EDEF6FC::get_offset_of_U3CalphaU3E5__7_8(),
	U3CDisplayTextMeshProFloatingTextU3Ed__12_t0F0EBDC5A3A2FF615C3205C8019AB8A81EDEF6FC::get_offset_of_U3CfadeDurationU3E5__8_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3540[10] = 
{
	U3CDisplayTextMeshFloatingTextU3Ed__13_t4B6A65678514F8D3CD0E6F44F8AB9DDA1228C55C::get_offset_of_U3CU3E1__state_0(),
	U3CDisplayTextMeshFloatingTextU3Ed__13_t4B6A65678514F8D3CD0E6F44F8AB9DDA1228C55C::get_offset_of_U3CU3E2__current_1(),
	U3CDisplayTextMeshFloatingTextU3Ed__13_t4B6A65678514F8D3CD0E6F44F8AB9DDA1228C55C::get_offset_of_U3CU3E4__this_2(),
	U3CDisplayTextMeshFloatingTextU3Ed__13_t4B6A65678514F8D3CD0E6F44F8AB9DDA1228C55C::get_offset_of_U3CCountDurationU3E5__2_3(),
	U3CDisplayTextMeshFloatingTextU3Ed__13_t4B6A65678514F8D3CD0E6F44F8AB9DDA1228C55C::get_offset_of_U3Cstarting_CountU3E5__3_4(),
	U3CDisplayTextMeshFloatingTextU3Ed__13_t4B6A65678514F8D3CD0E6F44F8AB9DDA1228C55C::get_offset_of_U3Ccurrent_CountU3E5__4_5(),
	U3CDisplayTextMeshFloatingTextU3Ed__13_t4B6A65678514F8D3CD0E6F44F8AB9DDA1228C55C::get_offset_of_U3Cstart_posU3E5__5_6(),
	U3CDisplayTextMeshFloatingTextU3Ed__13_t4B6A65678514F8D3CD0E6F44F8AB9DDA1228C55C::get_offset_of_U3Cstart_colorU3E5__6_7(),
	U3CDisplayTextMeshFloatingTextU3Ed__13_t4B6A65678514F8D3CD0E6F44F8AB9DDA1228C55C::get_offset_of_U3CalphaU3E5__7_8(),
	U3CDisplayTextMeshFloatingTextU3Ed__13_t4B6A65678514F8D3CD0E6F44F8AB9DDA1228C55C::get_offset_of_U3CfadeDurationU3E5__8_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3541[10] = 
{
	TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200::get_offset_of_TheFont_4(),
	TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200::get_offset_of_m_floatingText_5(),
	TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200::get_offset_of_m_textMeshPro_6(),
	TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200::get_offset_of_m_textMesh_7(),
	TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200::get_offset_of_m_transform_8(),
	TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200::get_offset_of_m_floatingText_Transform_9(),
	TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200::get_offset_of_m_cameraTransform_10(),
	TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200::get_offset_of_lastPOS_11(),
	TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200::get_offset_of_lastRotation_12(),
	TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200::get_offset_of_SpawnType_13(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3542[4] = 
{
	TextMeshSpawner_t44E5536D2A0C022FA8037C71741171A999A86364::get_offset_of_SpawnType_4(),
	TextMeshSpawner_t44E5536D2A0C022FA8037C71741171A999A86364::get_offset_of_NumberOfNPC_5(),
	TextMeshSpawner_t44E5536D2A0C022FA8037C71741171A999A86364::get_offset_of_TheFont_6(),
	TextMeshSpawner_t44E5536D2A0C022FA8037C71741171A999A86364::get_offset_of_floatingText_Script_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3543[5] = 
{
	FpsCounterAnchorPositions_t8925F6C50F79F9AFA64E5BF6270769F3A3634927::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3544[6] = 
{
	TMPro_InstructionOverlay_t3D749999E580901C7F6C778105AEBC290AC141CB::get_offset_of_AnchorPosition_4(),
	0,
	TMPro_InstructionOverlay_t3D749999E580901C7F6C778105AEBC290AC141CB::get_offset_of_m_TextMeshPro_6(),
	TMPro_InstructionOverlay_t3D749999E580901C7F6C778105AEBC290AC141CB::get_offset_of_m_textContainer_7(),
	TMPro_InstructionOverlay_t3D749999E580901C7F6C778105AEBC290AC141CB::get_offset_of_m_frameCounter_transform_8(),
	TMPro_InstructionOverlay_t3D749999E580901C7F6C778105AEBC290AC141CB::get_offset_of_m_camera_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3545[3] = 
{
	objectType_t5159C717EF79DEA9C8F7CF5F830EE5D4C4560036::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3546[5] = 
{
	TMP_ExampleScript_01_t17145EFFB693D3CE0896E7B0777A7BCD07EF4F41::get_offset_of_ObjectType_4(),
	TMP_ExampleScript_01_t17145EFFB693D3CE0896E7B0777A7BCD07EF4F41::get_offset_of_isStatic_5(),
	TMP_ExampleScript_01_t17145EFFB693D3CE0896E7B0777A7BCD07EF4F41::get_offset_of_m_text_6(),
	0,
	TMP_ExampleScript_01_t17145EFFB693D3CE0896E7B0777A7BCD07EF4F41::get_offset_of_count_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3547[5] = 
{
	FpsCounterAnchorPositions_tD7297D86C9462F5BC670EC9B05049FA736903F03::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3548[10] = 
{
	TMP_FrameRateCounter_t925B27BFB86BA834FD1FD76E0F274B216E976E0F::get_offset_of_UpdateInterval_4(),
	TMP_FrameRateCounter_t925B27BFB86BA834FD1FD76E0F274B216E976E0F::get_offset_of_m_LastInterval_5(),
	TMP_FrameRateCounter_t925B27BFB86BA834FD1FD76E0F274B216E976E0F::get_offset_of_m_Frames_6(),
	TMP_FrameRateCounter_t925B27BFB86BA834FD1FD76E0F274B216E976E0F::get_offset_of_AnchorPosition_7(),
	TMP_FrameRateCounter_t925B27BFB86BA834FD1FD76E0F274B216E976E0F::get_offset_of_htmlColorTag_8(),
	0,
	TMP_FrameRateCounter_t925B27BFB86BA834FD1FD76E0F274B216E976E0F::get_offset_of_m_TextMeshPro_10(),
	TMP_FrameRateCounter_t925B27BFB86BA834FD1FD76E0F274B216E976E0F::get_offset_of_m_frameCounter_transform_11(),
	TMP_FrameRateCounter_t925B27BFB86BA834FD1FD76E0F274B216E976E0F::get_offset_of_m_camera_12(),
	TMP_FrameRateCounter_t925B27BFB86BA834FD1FD76E0F274B216E976E0F::get_offset_of_last_AnchorPosition_13(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3549[1] = 
{
	TMP_TextEventCheck_t0DC1E9006D6A9A20B2BDA99A8D3C6821647061C7::get_offset_of_TextEventHandler_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3551[6] = 
{
	TMP_TextSelector_A_t29DB4724707E8DC222ACD5D3134F801191B96B25::get_offset_of_m_TextMeshPro_4(),
	TMP_TextSelector_A_t29DB4724707E8DC222ACD5D3134F801191B96B25::get_offset_of_m_Camera_5(),
	TMP_TextSelector_A_t29DB4724707E8DC222ACD5D3134F801191B96B25::get_offset_of_m_isHoveringObject_6(),
	TMP_TextSelector_A_t29DB4724707E8DC222ACD5D3134F801191B96B25::get_offset_of_m_selectedLink_7(),
	TMP_TextSelector_A_t29DB4724707E8DC222ACD5D3134F801191B96B25::get_offset_of_m_lastCharIndex_8(),
	TMP_TextSelector_A_t29DB4724707E8DC222ACD5D3134F801191B96B25::get_offset_of_m_lastWordIndex_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3552[14] = 
{
	TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF::get_offset_of_TextPopup_Prefab_01_4(),
	TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF::get_offset_of_m_TextPopup_RectTransform_5(),
	TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF::get_offset_of_m_TextPopup_TMPComponent_6(),
	0,
	0,
	TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF::get_offset_of_m_TextMeshPro_9(),
	TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF::get_offset_of_m_Canvas_10(),
	TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF::get_offset_of_m_Camera_11(),
	TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF::get_offset_of_isHoveringObject_12(),
	TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF::get_offset_of_m_selectedWord_13(),
	TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF::get_offset_of_m_selectedLink_14(),
	TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF::get_offset_of_m_lastIndex_15(),
	TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF::get_offset_of_m_matrix_16(),
	TMP_TextSelector_B_tFB29BADBB39B22B5B6F381F4BAB6CC3B7A1AA7EF::get_offset_of_m_cachedMeshInfoVertexData_17(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3553[5] = 
{
	FpsCounterAnchorPositions_t1E46DD5A81E44E1D3F7140DD75ABBBCB9B52DDCC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3554[9] = 
{
	TMP_UiFrameRateCounter_tCC4ADB6034B0FDF19DCA0129D36ACE86CB159B08::get_offset_of_UpdateInterval_4(),
	TMP_UiFrameRateCounter_tCC4ADB6034B0FDF19DCA0129D36ACE86CB159B08::get_offset_of_m_LastInterval_5(),
	TMP_UiFrameRateCounter_tCC4ADB6034B0FDF19DCA0129D36ACE86CB159B08::get_offset_of_m_Frames_6(),
	TMP_UiFrameRateCounter_tCC4ADB6034B0FDF19DCA0129D36ACE86CB159B08::get_offset_of_AnchorPosition_7(),
	TMP_UiFrameRateCounter_tCC4ADB6034B0FDF19DCA0129D36ACE86CB159B08::get_offset_of_htmlColorTag_8(),
	0,
	TMP_UiFrameRateCounter_tCC4ADB6034B0FDF19DCA0129D36ACE86CB159B08::get_offset_of_m_TextMeshPro_10(),
	TMP_UiFrameRateCounter_tCC4ADB6034B0FDF19DCA0129D36ACE86CB159B08::get_offset_of_m_frameCounter_transform_11(),
	TMP_UiFrameRateCounter_tCC4ADB6034B0FDF19DCA0129D36ACE86CB159B08::get_offset_of_last_AnchorPosition_12(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3555[5] = 
{
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF::get_offset_of_U3CU3E1__state_0(),
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF::get_offset_of_U3CU3E2__current_1(),
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF::get_offset_of_U3CU3E4__this_2(),
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF::get_offset_of_U3CtextInfoU3E5__2_3(),
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF::get_offset_of_U3CcurrentCharacterU3E5__3_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3556[1] = 
{
	VertexColorCycler_t37E80E87D8EAD0D7757CDA45BD5D3AA82FD28374::get_offset_of_m_TextComponent_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3557[3] = 
{
	VertexAnim_t3C1CE8C591FCD7E249D77127BED0B743EAD556F4::get_offset_of_angleRange_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexAnim_t3C1CE8C591FCD7E249D77127BED0B743EAD556F4::get_offset_of_angle_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexAnim_t3C1CE8C591FCD7E249D77127BED0B743EAD556F4::get_offset_of_speed_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3558[7] = 
{
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741::get_offset_of_U3CU3E1__state_0(),
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741::get_offset_of_U3CU3E2__current_1(),
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741::get_offset_of_U3CU3E4__this_2(),
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741::get_offset_of_U3CtextInfoU3E5__2_3(),
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741::get_offset_of_U3CloopCountU3E5__3_4(),
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741::get_offset_of_U3CvertexAnimU3E5__4_5(),
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741::get_offset_of_U3CcachedMeshInfoU3E5__5_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3559[5] = 
{
	VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A::get_offset_of_AngleMultiplier_4(),
	VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A::get_offset_of_SpeedMultiplier_5(),
	VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A::get_offset_of_CurveScale_6(),
	VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A::get_offset_of_m_TextComponent_7(),
	VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A::get_offset_of_hasTextChanged_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3560[5] = 
{
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155::get_offset_of_U3CU3E1__state_0(),
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155::get_offset_of_U3CU3E2__current_1(),
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155::get_offset_of_U3CU3E4__this_2(),
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155::get_offset_of_U3CtextInfoU3E5__2_3(),
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155::get_offset_of_U3CcopyOfVerticesU3E5__3_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3561[6] = 
{
	VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A::get_offset_of_AngleMultiplier_4(),
	VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A::get_offset_of_SpeedMultiplier_5(),
	VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A::get_offset_of_ScaleMultiplier_6(),
	VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A::get_offset_of_RotationMultiplier_7(),
	VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A::get_offset_of_m_TextComponent_8(),
	VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A::get_offset_of_hasTextChanged_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3562[5] = 
{
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6::get_offset_of_U3CU3E1__state_0(),
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6::get_offset_of_U3CU3E2__current_1(),
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6::get_offset_of_U3CU3E4__this_2(),
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6::get_offset_of_U3CtextInfoU3E5__2_3(),
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6::get_offset_of_U3CcopyOfVerticesU3E5__3_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3563[5] = 
{
	VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9::get_offset_of_AngleMultiplier_4(),
	VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9::get_offset_of_SpeedMultiplier_5(),
	VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9::get_offset_of_CurveScale_6(),
	VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9::get_offset_of_m_TextComponent_7(),
	VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9::get_offset_of_hasTextChanged_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3564[2] = 
{
	U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6::get_offset_of_modifiedCharScale_0(),
	U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6::get_offset_of_U3CU3E9__0_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3565[7] = 
{
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40::get_offset_of_U3CU3E1__state_0(),
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40::get_offset_of_U3CU3E2__current_1(),
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40::get_offset_of_U3CU3E4__this_2(),
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40::get_offset_of_U3CU3E8__1_3(),
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40::get_offset_of_U3CtextInfoU3E5__2_4(),
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40::get_offset_of_U3CcachedMeshInfoVertexDataU3E5__3_5(),
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40::get_offset_of_U3CscaleSortingOrderU3E5__4_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3566[5] = 
{
	VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163::get_offset_of_AngleMultiplier_4(),
	VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163::get_offset_of_SpeedMultiplier_5(),
	VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163::get_offset_of_CurveScale_6(),
	VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163::get_offset_of_m_TextComponent_7(),
	VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163::get_offset_of_hasTextChanged_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3567[5] = 
{
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76::get_offset_of_U3CU3E1__state_0(),
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76::get_offset_of_U3CU3E2__current_1(),
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76::get_offset_of_U3CU3E4__this_2(),
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76::get_offset_of_U3Cold_CurveScaleU3E5__2_3(),
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76::get_offset_of_U3Cold_curveU3E5__3_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3568[5] = 
{
	WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96::get_offset_of_m_TextComponent_4(),
	WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96::get_offset_of_VertexCurve_5(),
	WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96::get_offset_of_AngleMultiplier_6(),
	WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96::get_offset_of_SpeedMultiplier_7(),
	WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96::get_offset_of_CurveScale_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3570[4] = 
{
	PathBezier_t3A13E1125CFCBAC9592FD3E738EBD51E56258816::get_offset_of_trans_4(),
	PathBezier_t3A13E1125CFCBAC9592FD3E738EBD51E56258816::get_offset_of_cr_5(),
	PathBezier_t3A13E1125CFCBAC9592FD3E738EBD51E56258816::get_offset_of_avatar1_6(),
	PathBezier_t3A13E1125CFCBAC9592FD3E738EBD51E56258816::get_offset_of_iter_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3571[21] = 
{
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_cubes_1(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_tweenIds_2(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_onCompleteCount_3(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_cubeToTrans_4(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_cubeDestEnd_5(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_cubeSpline_6(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_jumpTimeId_7(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_jumpCube_8(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_zeroCube_9(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_cubeScale_10(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_cubeRotate_11(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_cubeRotateA_12(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_cubeRotateB_13(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_onStartTime_14(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_beforePos_15(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_beforePos2_16(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_totalEasingCheck_17(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_totalEasingCheckSuccess_18(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_value2UpdateCalled_19(),
	U3CU3Ec__DisplayClass22_0_t7E995104B373A95406291202970922855BD6536F::get_offset_of_U3CU3E9__21_20(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3572[2] = 
{
	U3CU3Ec__DisplayClass22_1_t041360FB309BAE73F44DAC9B4689082AC2FF5973::get_offset_of_beforeX_0(),
	U3CU3Ec__DisplayClass22_1_t041360FB309BAE73F44DAC9B4689082AC2FF5973::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3573[3] = 
{
	U3CU3Ec__DisplayClass22_2_tF5B9A247AE90B21641EE6BA256550BF6E5CBC1E8::get_offset_of_totalTweenTypeLength_0(),
	U3CU3Ec__DisplayClass22_2_tF5B9A247AE90B21641EE6BA256550BF6E5CBC1E8::get_offset_of_CSU24U3CU3E8__locals2_1(),
	U3CU3Ec__DisplayClass22_2_tF5B9A247AE90B21641EE6BA256550BF6E5CBC1E8::get_offset_of_U3CU3E9__24_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3574[7] = 
{
	U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields::get_offset_of_U3CU3E9__22_3_1(),
	U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields::get_offset_of_U3CU3E9__22_22_2(),
	U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields::get_offset_of_U3CU3E9__22_7_3(),
	U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields::get_offset_of_U3CU3E9__22_12_4(),
	U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields::get_offset_of_U3CU3E9__22_18_5(),
	U3CU3Ec_t6493133AD6E3C4496AEFFEA344663F7644D44104_StaticFields::get_offset_of_U3CU3E9__26_0_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3575[23] = 
{
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_pauseCount_1(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_cubeRound_2(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_onStartPos_3(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_onStartPosSpline_4(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_cubeSpline_5(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_cubeSeq_6(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_cubeBounds_7(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_didPassBounds_8(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_failPoint_9(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_setOnStartNum_10(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_setPosOnUpdate_11(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_setPosNum_12(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_hasGroupTweensCheckStarted_13(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_previousXlt4_14(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_onUpdateWasCalled_15(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_start_16(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_expectedTime_17(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_didGetCorrectOnUpdate_18(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_U3CU3E9__13_19(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_U3CU3E9__14_20(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_U3CU3E9__16_21(),
	U3CU3Ec__DisplayClass24_0_tC43013AD0D8C712305416629D866B5A735CABB1F::get_offset_of_U3CU3E9__15_22(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3576[5] = 
{
	U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65::get_offset_of_U3CU3E1__state_0(),
	U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65::get_offset_of_U3CU3E2__current_1(),
	U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65::get_offset_of_U3CU3E4__this_2(),
	U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65::get_offset_of_U3CU3E8__1_3(),
	U3CtimeBasedTestingU3Ed__24_tC48CA45DEF560DE9941A550792E59DE0D9DCDC65::get_offset_of_U3CdescriptionMatchCountU3E5__2_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3577[8] = 
{
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CU3E1__state_0(),
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CU3E2__current_1(),
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CU3E4__this_2(),
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CcubeCountU3E5__2_3(),
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CtweensAU3E5__3_4(),
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CaGOsU3E5__4_5(),
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CtweensBU3E5__5_6(),
	U3ClotsOfCancelsU3Ed__25_t388FFEC29BBCBE0385E38111223FE92F60D0E523::get_offset_of_U3CbGOsU3E5__6_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3578[3] = 
{
	U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB::get_offset_of_U3CU3E1__state_0(),
	U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB::get_offset_of_U3CU3E2__current_1(),
	U3CpauseTimeNowU3Ed__26_t010969A53700F11A0FBF373FD2C72B275E72E8DB::get_offset_of_U3CU3E4__this_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3579[21] = 
{
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_cube1_4(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_cube2_5(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_cube3_6(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_cube4_7(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_cubeAlpha1_8(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_cubeAlpha2_9(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_eventGameObjectWasCalled_10(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_eventGeneralWasCalled_11(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_lt1Id_12(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_lt2_13(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_lt3_14(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_lt4_15(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_groupTweens_16(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_groupGOs_17(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_groupTweensCnt_18(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_rotateRepeat_19(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_rotateRepeatAngle_20(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_boxNoCollider_21(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_timeElapsedNormalTimeScale_22(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_timeElapsedIgnoreTimeScale_23(),
	TestingUnitTests_t0F733B62958AE33D1F86657DFEA59A729C338757::get_offset_of_pauseTweenDidFinish_24(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
