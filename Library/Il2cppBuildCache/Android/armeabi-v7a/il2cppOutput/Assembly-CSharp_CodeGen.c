﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void OldGUIExamplesCS::Start()
extern void OldGUIExamplesCS_Start_mC33821BD3ADFD74B0B1BDD411FE35F29173AAE16 (void);
// 0x00000002 System.Void OldGUIExamplesCS::catMoved()
extern void OldGUIExamplesCS_catMoved_m0AFE2D767EEFFE2B23F700545EDCADBA4FDE4495 (void);
// 0x00000003 System.Void OldGUIExamplesCS::OnGUI()
extern void OldGUIExamplesCS_OnGUI_mFA5EA2B56824C4712729521C4545139AB167C4AA (void);
// 0x00000004 System.Void OldGUIExamplesCS::.ctor()
extern void OldGUIExamplesCS__ctor_m805206E5EF03EF6452AC2A3979FD378F1CB86998 (void);
// 0x00000005 System.Void TestingPunch::Start()
extern void TestingPunch_Start_m51A034719AB8587292861C585C70970EDEBE8877 (void);
// 0x00000006 System.Void TestingPunch::Update()
extern void TestingPunch_Update_m953417D5B76254AF9BF4E2BA996801A997F9D6D6 (void);
// 0x00000007 System.Void TestingPunch::tweenStatically(UnityEngine.GameObject)
extern void TestingPunch_tweenStatically_m0E13525CEE807E5FA77B94242B182868B1D129A0 (void);
// 0x00000008 System.Void TestingPunch::enterMiniGameStart(System.Object)
extern void TestingPunch_enterMiniGameStart_m017AD4178C7F47FB535AFCA0B39AB97A58F0D9BE (void);
// 0x00000009 System.Void TestingPunch::updateColor(UnityEngine.Color)
extern void TestingPunch_updateColor_mB0688DDA58E8C0CBA76525C4B31BD18EB3A2E4FC (void);
// 0x0000000A System.Void TestingPunch::delayedMethod(System.Object)
extern void TestingPunch_delayedMethod_mC7F5E34BF17C44BC3B311081B8ED1897F1CC08C3 (void);
// 0x0000000B System.Void TestingPunch::destroyOnComp(System.Object)
extern void TestingPunch_destroyOnComp_mB95F605847D2D64B1F4BF239600FAF788917BF44 (void);
// 0x0000000C System.String TestingPunch::curveToString(UnityEngine.AnimationCurve)
extern void TestingPunch_curveToString_mA840114D184FEA8169EE6B379C4513E282E09511 (void);
// 0x0000000D System.Void TestingPunch::.ctor()
extern void TestingPunch__ctor_m8AA41432E6E7079F6B683958FECD329615A21169 (void);
// 0x0000000E System.Void TestingPunch::<Update>b__4_0()
extern void TestingPunch_U3CUpdateU3Eb__4_0_m573E0207272AF5B03F77EBE674FC9B7C0B49791D (void);
// 0x0000000F System.Void TestingPunch::<Update>b__4_3()
extern void TestingPunch_U3CUpdateU3Eb__4_3_m4CC34D2970098316C32EBE67B368AB199A20A2E1 (void);
// 0x00000010 System.Void TestingPunch::<Update>b__4_7(UnityEngine.Vector2)
extern void TestingPunch_U3CUpdateU3Eb__4_7_m2E2AD49C20FF476ED389803BE00C0C87495D0DFC (void);
// 0x00000011 System.Void TestingPunch/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m6EF45FEFD275B14E2DFC1358F9AF89B57EF810AC (void);
// 0x00000012 System.Void TestingPunch/<>c__DisplayClass4_0::<Update>b__2(System.Single)
extern void U3CU3Ec__DisplayClass4_0_U3CUpdateU3Eb__2_m59E4E1443D5262BE2A23D2C28AB670EA4071ABF4 (void);
// 0x00000013 System.Void TestingPunch/<>c__DisplayClass4_1::.ctor()
extern void U3CU3Ec__DisplayClass4_1__ctor_mB4665A6E7864E352E8943BE5EFEDF0E53D1685AC (void);
// 0x00000014 System.Void TestingPunch/<>c__DisplayClass4_1::<Update>b__6()
extern void U3CU3Ec__DisplayClass4_1_U3CUpdateU3Eb__6_m954F24C94FA12A5815D1FED30C2506F75E8628F2 (void);
// 0x00000015 System.Void TestingPunch/<>c__DisplayClass4_2::.ctor()
extern void U3CU3Ec__DisplayClass4_2__ctor_m1584CFD1510446CA5FCCF444CC4C3A8416419C58 (void);
// 0x00000016 System.Void TestingPunch/<>c__DisplayClass4_2::<Update>b__8(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass4_2_U3CUpdateU3Eb__8_m06A77409E2EEAED1E858D86BDDD2CC58DE2CE83A (void);
// 0x00000017 System.Void TestingPunch/<>c::.cctor()
extern void U3CU3Ec__cctor_m66A08467936DA18B932008F1E6DC72EEB39F16A1 (void);
// 0x00000018 System.Void TestingPunch/<>c::.ctor()
extern void U3CU3Ec__ctor_mF00B82FD7B30077FBDE19AD680A208A4C3BA70F9 (void);
// 0x00000019 System.Void TestingPunch/<>c::<Update>b__4_1()
extern void U3CU3Ec_U3CUpdateU3Eb__4_1_mC4766FA978E4EE87A4519997A0A18B9FF21F128A (void);
// 0x0000001A System.Void TestingPunch/<>c::<Update>b__4_4(System.Single)
extern void U3CU3Ec_U3CUpdateU3Eb__4_4_mB29A35054C7AD0CC10D5E31667EDF5249A05D507 (void);
// 0x0000001B System.Void TestingPunch/<>c::<Update>b__4_5()
extern void U3CU3Ec_U3CUpdateU3Eb__4_5_mDC2E23D6DCE6159A01A4E5965CA5654E521A6203 (void);
// 0x0000001C System.Void TestingPunch/<>c::<tweenStatically>b__5_0(System.Single)
extern void U3CU3Ec_U3CtweenStaticallyU3Eb__5_0_m412A7565AC15AED058D5BD67A3BFF97E6450F355 (void);
// 0x0000001D System.Void TestingRigidbodyCS::Start()
extern void TestingRigidbodyCS_Start_m995B801673B71100B5483E039BFCF522BA624548 (void);
// 0x0000001E System.Void TestingRigidbodyCS::Update()
extern void TestingRigidbodyCS_Update_m8DD8457212175A81186F50921B999350FA9DEC0F (void);
// 0x0000001F System.Void TestingRigidbodyCS::.ctor()
extern void TestingRigidbodyCS__ctor_mDE99A4FEAFB79D731E586C9AF3656849987B741B (void);
// 0x00000020 System.Void Following::Start()
extern void Following_Start_m5CD86C24B70F949375792EFAEF58F657F06DBA18 (void);
// 0x00000021 System.Void Following::Update()
extern void Following_Update_mEB56FD4D3CE1029AB038A8DA8CE6C6704A9541DC (void);
// 0x00000022 System.Void Following::moveArrow()
extern void Following_moveArrow_mF5878CA6A071D36230DD6FA4209FFA765BAC640F (void);
// 0x00000023 System.Void Following::.ctor()
extern void Following__ctor_mA110CFAE2A03042B9F1FE53338D377A005CBAB13 (void);
// 0x00000024 System.Void GeneralAdvancedTechniques::Start()
extern void GeneralAdvancedTechniques_Start_m132CD028BB491EE7C59B5F5F1D1AB47EB7DAE306 (void);
// 0x00000025 System.Void GeneralAdvancedTechniques::.ctor()
extern void GeneralAdvancedTechniques__ctor_mB828FC801FA5649B544E8FF31A6E1FF8916563D8 (void);
// 0x00000026 System.Void GeneralAdvancedTechniques::<Start>b__10_0(System.Single)
extern void GeneralAdvancedTechniques_U3CStartU3Eb__10_0_mBBEFB6358D20EC1EFC4D6B51139D0DEEDFF86AD2 (void);
// 0x00000027 System.Void GeneralBasic::Start()
extern void GeneralBasic_Start_m159ABB4F96835B58FEB058E24A576462A119E871 (void);
// 0x00000028 System.Void GeneralBasic::advancedExamples()
extern void GeneralBasic_advancedExamples_mB7CE50C2F062F66FD7D0E1BD403DFDACE1D099AF (void);
// 0x00000029 System.Void GeneralBasic::.ctor()
extern void GeneralBasic__ctor_mF2DBA4586E298C9F216C4680C14837E6B5E095AD (void);
// 0x0000002A System.Void GeneralBasic::<advancedExamples>b__2_0()
extern void GeneralBasic_U3CadvancedExamplesU3Eb__2_0_m706BD8DE22C36E9A7EF6326EF1B7EAB6C780BF3D (void);
// 0x0000002B System.Void GeneralBasic/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mAE48CD4D687E71BADBB3C871A2194F0803D1EDC0 (void);
// 0x0000002C System.Void GeneralBasic/<>c__DisplayClass2_0::<advancedExamples>b__1()
extern void U3CU3Ec__DisplayClass2_0_U3CadvancedExamplesU3Eb__1_m9425EEE45EC19E72004ADE4D0B203F5448C21B27 (void);
// 0x0000002D System.Void GeneralBasic/<>c__DisplayClass2_0::<advancedExamples>b__2()
extern void U3CU3Ec__DisplayClass2_0_U3CadvancedExamplesU3Eb__2_mA4BA962A162AB1425F97630A3FB0FD395D77444C (void);
// 0x0000002E System.Void GeneralBasics2d::Start()
extern void GeneralBasics2d_Start_mD2ED7FA046D4AD2C347896AFF54984E3BDC73876 (void);
// 0x0000002F UnityEngine.GameObject GeneralBasics2d::createSpriteDude(System.String,UnityEngine.Vector3,System.Boolean)
extern void GeneralBasics2d_createSpriteDude_m5D7A1E41EEAADFE587F9AB375AA4C3B7617DF7B3 (void);
// 0x00000030 System.Void GeneralBasics2d::advancedExamples()
extern void GeneralBasics2d_advancedExamples_m7A48E45BB9836372021B76A523A2594A1E66F1EA (void);
// 0x00000031 System.Void GeneralBasics2d::.ctor()
extern void GeneralBasics2d__ctor_m5BBA8B80514CB8E8098C55AF472FADC2BA164C2A (void);
// 0x00000032 System.Void GeneralBasics2d::<advancedExamples>b__4_0()
extern void GeneralBasics2d_U3CadvancedExamplesU3Eb__4_0_mFB34B228D53D66D2DA60CBE36C863AAC135AAE41 (void);
// 0x00000033 System.Void GeneralBasics2d/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mF77915960E9F4C762CB8473CB99645EB20C2BB06 (void);
// 0x00000034 System.Void GeneralBasics2d/<>c__DisplayClass4_0::<advancedExamples>b__1()
extern void U3CU3Ec__DisplayClass4_0_U3CadvancedExamplesU3Eb__1_m34824CEE9AF028A0C15E9EF50B69F24B63AD82C2 (void);
// 0x00000035 System.Void GeneralBasics2d/<>c__DisplayClass4_0::<advancedExamples>b__2()
extern void U3CU3Ec__DisplayClass4_0_U3CadvancedExamplesU3Eb__2_m9486B659998B862E7D8D7F60D70F7E60023F33C1 (void);
// 0x00000036 System.Void GeneralCameraShake::Start()
extern void GeneralCameraShake_Start_mF7CB74F9B674B375BDCB29EC25FF2B568431E543 (void);
// 0x00000037 System.Void GeneralCameraShake::bigGuyJump()
extern void GeneralCameraShake_bigGuyJump_m502DE5D59ED9FEAD8F13549A9A2105E86D1F90E3 (void);
// 0x00000038 System.Void GeneralCameraShake::.ctor()
extern void GeneralCameraShake__ctor_m6B09C9BCA8DDFD913791AD123731FFC86E274FAD (void);
// 0x00000039 System.Void GeneralCameraShake/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m3C8CF484C54E2EE225CC0D8A11A5BAEB2686C557 (void);
// 0x0000003A System.Void GeneralCameraShake/<>c__DisplayClass4_0::<bigGuyJump>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CbigGuyJumpU3Eb__0_mA78BEF951754FC194ABA798EF9E551E30CD1DF91 (void);
// 0x0000003B System.Void GeneralCameraShake/<>c__DisplayClass4_0::<bigGuyJump>b__1()
extern void U3CU3Ec__DisplayClass4_0_U3CbigGuyJumpU3Eb__1_m4A2B4F54066C9921E28893C3580BB18C47C8E99F (void);
// 0x0000003C System.Void GeneralCameraShake/<>c__DisplayClass4_1::.ctor()
extern void U3CU3Ec__DisplayClass4_1__ctor_mE5D0BDBB5AC685B299E3E124147549F48D5360FD (void);
// 0x0000003D System.Void GeneralCameraShake/<>c__DisplayClass4_1::<bigGuyJump>b__2(System.Single)
extern void U3CU3Ec__DisplayClass4_1_U3CbigGuyJumpU3Eb__2_mE50BF2533D5A8E4215CA056E6C5A7B707C31615B (void);
// 0x0000003E System.Void GeneralEasingTypes::Start()
extern void GeneralEasingTypes_Start_m9918519E1FE8506C66C09A06FC0973544C9B6CFE (void);
// 0x0000003F System.Void GeneralEasingTypes::demoEaseTypes()
extern void GeneralEasingTypes_demoEaseTypes_mFAB0A2AA4E562BA44E4D99BB36EFC8BCC92CBD4E (void);
// 0x00000040 System.Void GeneralEasingTypes::resetLines()
extern void GeneralEasingTypes_resetLines_mF6F826BE8342FEBEBB97FE69AEDCD749AD38E290 (void);
// 0x00000041 System.Void GeneralEasingTypes::.ctor()
extern void GeneralEasingTypes__ctor_m826931FD7A0F6840EC3920EF742AE00023053424 (void);
// 0x00000042 System.Void GeneralEasingTypes/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m67D836D2CA426B728355DCE885351EF9BA44FB17 (void);
// 0x00000043 System.Void GeneralEasingTypes/<>c__DisplayClass4_0::<demoEaseTypes>b__0(System.Single)
extern void U3CU3Ec__DisplayClass4_0_U3CdemoEaseTypesU3Eb__0_mDA8F2DD5BE6F9B20420A94430FE2C634CC07EED3 (void);
// 0x00000044 System.Void GeneralEventsListeners::Awake()
extern void GeneralEventsListeners_Awake_m929CA13301F3FA1A5C1AA230681377F30E0E78C4 (void);
// 0x00000045 System.Void GeneralEventsListeners::Start()
extern void GeneralEventsListeners_Start_m1C63BB1DBC231496398533A9D365C0B2EAD4BE80 (void);
// 0x00000046 System.Void GeneralEventsListeners::jumpUp(LTEvent)
extern void GeneralEventsListeners_jumpUp_m2CBD7F9072D9EB312F608BFC683E63D3DFF1DB28 (void);
// 0x00000047 System.Void GeneralEventsListeners::changeColor(LTEvent)
extern void GeneralEventsListeners_changeColor_mF0F3863B481DE533D2DA0CCF3424BD1B231268BE (void);
// 0x00000048 System.Void GeneralEventsListeners::OnCollisionEnter(UnityEngine.Collision)
extern void GeneralEventsListeners_OnCollisionEnter_m296C0AD01860A6C98C1E293DD67D880BB0590E91 (void);
// 0x00000049 System.Void GeneralEventsListeners::OnCollisionStay(UnityEngine.Collision)
extern void GeneralEventsListeners_OnCollisionStay_m290268F1B803DC1E306F52423DA7A365442C7C31 (void);
// 0x0000004A System.Void GeneralEventsListeners::FixedUpdate()
extern void GeneralEventsListeners_FixedUpdate_m903B40B7AC9A96613D21A365E8BC005E87AA8439 (void);
// 0x0000004B System.Void GeneralEventsListeners::OnMouseDown()
extern void GeneralEventsListeners_OnMouseDown_m6B7190177327186B2D363437F5B96595367C7454 (void);
// 0x0000004C System.Void GeneralEventsListeners::.ctor()
extern void GeneralEventsListeners__ctor_m7B76E509C73273AA56871D0D0E9772F6FC2A3056 (void);
// 0x0000004D System.Void GeneralEventsListeners::<changeColor>b__8_0(UnityEngine.Color)
extern void GeneralEventsListeners_U3CchangeColorU3Eb__8_0_m8D567E7710ED42E4ED9DF3074F1F9E4AF19AF1A1 (void);
// 0x0000004E System.Void GeneralSequencer::Start()
extern void GeneralSequencer_Start_m8B0E62A3511525F37EF49CF5BA670A8FDC005FBC (void);
// 0x0000004F System.Void GeneralSequencer::.ctor()
extern void GeneralSequencer__ctor_mFD32ABD60398858ED0D676B843D61702B7FB1F75 (void);
// 0x00000050 System.Void GeneralSequencer::<Start>b__4_0()
extern void GeneralSequencer_U3CStartU3Eb__4_0_m01F09B25E4652D0AD12402145A95897D0E65C36F (void);
// 0x00000051 System.Void GeneralSimpleUI::Start()
extern void GeneralSimpleUI_Start_mE89FC53D2284D832DC21032BAB637D0C4075AC6D (void);
// 0x00000052 System.Void GeneralSimpleUI::.ctor()
extern void GeneralSimpleUI__ctor_mC03751F3B550ACC5742FCB2C9B381E1EA841C085 (void);
// 0x00000053 System.Void GeneralSimpleUI::<Start>b__1_0(UnityEngine.Vector2)
extern void GeneralSimpleUI_U3CStartU3Eb__1_0_mCC061524D85ACD8BAF6FE7D8CE1F2277735B3FC8 (void);
// 0x00000054 System.Void GeneralSimpleUI::<Start>b__1_2(UnityEngine.Vector3)
extern void GeneralSimpleUI_U3CStartU3Eb__1_2_m32632A4DB6420C70DED2CA0F5D16A52ABED4D202 (void);
// 0x00000055 System.Void GeneralSimpleUI::<Start>b__1_3(UnityEngine.Color)
extern void GeneralSimpleUI_U3CStartU3Eb__1_3_m81A54F1B0397D24C3ABC0367E5F3A96CD8938D25 (void);
// 0x00000056 System.Void GeneralSimpleUI/<>c::.cctor()
extern void U3CU3Ec__cctor_m2F58606DC92BEEAFADDAB84BDE6B467660090F3A (void);
// 0x00000057 System.Void GeneralSimpleUI/<>c::.ctor()
extern void U3CU3Ec__ctor_mB27080DC10CB152A62D2B7D7F2BFFA689E952C73 (void);
// 0x00000058 System.Void GeneralSimpleUI/<>c::<Start>b__1_1(System.Single)
extern void U3CU3Ec_U3CStartU3Eb__1_1_m3121B206ADF49779458AA5961E16BC6F6DFC8277 (void);
// 0x00000059 System.Void GeneralUISpace::Start()
extern void GeneralUISpace_Start_m5EE834954E03550B87B33D931A593651703C2223 (void);
// 0x0000005A System.Void GeneralUISpace::.ctor()
extern void GeneralUISpace__ctor_m007D52C81E8F67F6C5FDBD433EBDD1F82281CE87 (void);
// 0x0000005B System.Void GeneralUISpace/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_mBE556FCA9CC905E8AFDA137DCA924101E479875C (void);
// 0x0000005C System.Void GeneralUISpace/<>c__DisplayClass15_0::<Start>b__0(System.Single)
extern void U3CU3Ec__DisplayClass15_0_U3CStartU3Eb__0_m6F18D230FF8599CCA7A14221973A08E0447954B1 (void);
// 0x0000005D System.Void LogoCinematic::Awake()
extern void LogoCinematic_Awake_mC405113467B061C77307CFD90E064F5A651B3D22 (void);
// 0x0000005E System.Void LogoCinematic::Start()
extern void LogoCinematic_Start_m3E319B3DEC812349F8C37171CA5D4081276F7826 (void);
// 0x0000005F System.Void LogoCinematic::playBoom()
extern void LogoCinematic_playBoom_m787C8F6D4E17645300701C39C99D4E37893FEFAF (void);
// 0x00000060 System.Void LogoCinematic::.ctor()
extern void LogoCinematic__ctor_m4106D7C04CED9DBA0EA597C3A8961A01C01B88AD (void);
// 0x00000061 System.Void PathBezier2d::Start()
extern void PathBezier2d_Start_m6F6F8E4B60B7D179AE36BEE9EC0A264ED03D6849 (void);
// 0x00000062 System.Void PathBezier2d::OnDrawGizmos()
extern void PathBezier2d_OnDrawGizmos_m69311F42A05B822B9FCBD8CC4F0F4C4804AC9354 (void);
// 0x00000063 System.Void PathBezier2d::.ctor()
extern void PathBezier2d__ctor_m28D0B7A13B4DA0EDA3A009A105016F4A7E143055 (void);
// 0x00000064 System.Void ExampleSpline::Start()
extern void ExampleSpline_Start_mA17AF5D470B3BCA08101E4A82E8EB7A0BAABEAB6 (void);
// 0x00000065 System.Void ExampleSpline::Update()
extern void ExampleSpline_Update_m356EAE7A488D899AAA63B3F9CC96E19743AE7EC6 (void);
// 0x00000066 System.Void ExampleSpline::OnDrawGizmos()
extern void ExampleSpline_OnDrawGizmos_m1479029D4155936B69C362144D347542E05CC0DF (void);
// 0x00000067 System.Void ExampleSpline::.ctor()
extern void ExampleSpline__ctor_m9FCDF08DBF16C46F63F1AF63EF143E3DAC295EE1 (void);
// 0x00000068 System.Void PathSpline2d::Start()
extern void PathSpline2d_Start_m1EBB4A3F12FFD7F33EC3D10C0955B5D421DE5B5D (void);
// 0x00000069 System.Void PathSpline2d::OnDrawGizmos()
extern void PathSpline2d_OnDrawGizmos_mF49ABC37A9C2C3B83BB5BF1419067E2FD8AB1B77 (void);
// 0x0000006A System.Void PathSpline2d::.ctor()
extern void PathSpline2d__ctor_mD3756639777CCF9D251F157BCF32647C9F0AB7C6 (void);
// 0x0000006B System.Void PathSplineEndless::Start()
extern void PathSplineEndless_Start_mAB94E60786A238B143854F91E30BB877D5730800 (void);
// 0x0000006C System.Void PathSplineEndless::Update()
extern void PathSplineEndless_Update_m5C2C0D01E2D3AF4FFD2AAF23756C62733DB1509A (void);
// 0x0000006D UnityEngine.GameObject PathSplineEndless::objectQueue(UnityEngine.GameObject[],System.Int32&)
extern void PathSplineEndless_objectQueue_m4F43CF1A70A9897A13A3B385DCE841DA5141C524 (void);
// 0x0000006E System.Void PathSplineEndless::addRandomTrackPoint()
extern void PathSplineEndless_addRandomTrackPoint_m3A914064336F71FAC30C4919147216682994A961 (void);
// 0x0000006F System.Void PathSplineEndless::refreshSpline()
extern void PathSplineEndless_refreshSpline_m8B64174788E0276D3C61E2F66C85EF7378EA30C2 (void);
// 0x00000070 System.Void PathSplineEndless::playSwish()
extern void PathSplineEndless_playSwish_m968F1F1AED0F8421A2834347C318076A3CE747BD (void);
// 0x00000071 System.Void PathSplineEndless::.ctor()
extern void PathSplineEndless__ctor_m411B70A0AC412E9518BBBDC687C25CD1B388E6C9 (void);
// 0x00000072 System.Void PathSplineEndless::<Start>b__17_0(System.Single)
extern void PathSplineEndless_U3CStartU3Eb__17_0_m89EDA38D298348E747F008DED4C1CF7D4F815D60 (void);
// 0x00000073 System.Void PathSplinePerformance::Start()
extern void PathSplinePerformance_Start_m6A0FB9A0FCA5D3B1AFDF6C03C8B8CD3DCFC57040 (void);
// 0x00000074 System.Void PathSplinePerformance::Update()
extern void PathSplinePerformance_Update_m5416A63FFB450EABE065F6C5C3312B7C048EF5A7 (void);
// 0x00000075 System.Void PathSplinePerformance::OnDrawGizmos()
extern void PathSplinePerformance_OnDrawGizmos_m490615FEB9C7E53A19C0DA922E9A7077A1A66274 (void);
// 0x00000076 System.Void PathSplinePerformance::playSwish()
extern void PathSplinePerformance_playSwish_mDCC9C70227FFF9F7969D9D8330E5B485B954DC7A (void);
// 0x00000077 System.Void PathSplinePerformance::.ctor()
extern void PathSplinePerformance__ctor_m3918B4846029379DBF03985747088A8F4E734CBB (void);
// 0x00000078 System.Void PathSplines::OnEnable()
extern void PathSplines_OnEnable_mBAC54412DA75F41A2961B5D2A8D33971CA42EF78 (void);
// 0x00000079 System.Void PathSplines::Start()
extern void PathSplines_Start_m7E196F5CDC4C203D9187A30B90FC02414FBDF186 (void);
// 0x0000007A System.Void PathSplines::Update()
extern void PathSplines_Update_m2D049CD05DCD8B298477D7A6F6EEF97F16A140EA (void);
// 0x0000007B System.Void PathSplines::OnDrawGizmos()
extern void PathSplines_OnDrawGizmos_m2099220E2C8911102EA2BE1EFC5153CDE8FF6283 (void);
// 0x0000007C System.Void PathSplines::.ctor()
extern void PathSplines__ctor_m4BAB7527375E54EAD3B636B8AB1F5AEBF88AB565 (void);
// 0x0000007D System.Void PathSplines::<Start>b__4_0()
extern void PathSplines_U3CStartU3Eb__4_0_mC304E252D7336E181BBEF0F27B79311DEDF4BB0B (void);
// 0x0000007E System.Void PathSplineTrack::Start()
extern void PathSplineTrack_Start_m63CB9A3917A58E8791AA4DED6921EB32280FFF7F (void);
// 0x0000007F System.Void PathSplineTrack::Update()
extern void PathSplineTrack_Update_m00439912F9ADA883049E7B01A7453419BDB6AE79 (void);
// 0x00000080 System.Void PathSplineTrack::OnDrawGizmos()
extern void PathSplineTrack_OnDrawGizmos_mD7F0DCDB7C1B0CB6ED0F1A4A20E2389A14127D5F (void);
// 0x00000081 System.Void PathSplineTrack::playSwish()
extern void PathSplineTrack_playSwish_mEADD52F2DF6153507245FCEF25CCE5AAC216627A (void);
// 0x00000082 System.Void PathSplineTrack::.ctor()
extern void PathSplineTrack__ctor_m7F933342B4D6E1B74306EF85FC59C7F70E5EF982 (void);
// 0x00000083 System.Void TestingZLegacy::Awake()
extern void TestingZLegacy_Awake_m6ACDE967AF5970766B8EFFCC196B677E07DB0C10 (void);
// 0x00000084 System.Void TestingZLegacy::Start()
extern void TestingZLegacy_Start_mE43112422475CBDC67D4CE4680D0B64C38D23BA6 (void);
// 0x00000085 System.Void TestingZLegacy::pauseNow()
extern void TestingZLegacy_pauseNow_mCC617C16BCE35E2D901183CFAEF9F51D7805DA61 (void);
// 0x00000086 System.Void TestingZLegacy::OnGUI()
extern void TestingZLegacy_OnGUI_mAF981918615B59787EED9DC6EB2F232D6A4F6E5F (void);
// 0x00000087 System.Void TestingZLegacy::endlessCallback()
extern void TestingZLegacy_endlessCallback_m96FFBCC19A52A227D879CA4C9A4D784078F0FB65 (void);
// 0x00000088 System.Void TestingZLegacy::cycleThroughExamples()
extern void TestingZLegacy_cycleThroughExamples_mE124EAAF3FE6EDE035F8D620E0FF877916FFCA2A (void);
// 0x00000089 System.Void TestingZLegacy::updateValue3Example()
extern void TestingZLegacy_updateValue3Example_m690CC88D964055BB090736A4D7D080E043E15D82 (void);
// 0x0000008A System.Void TestingZLegacy::updateValue3ExampleUpdate(UnityEngine.Vector3)
extern void TestingZLegacy_updateValue3ExampleUpdate_m28A23A4EFF68505C400DC9F5BBBD6ADA3ACAEE62 (void);
// 0x0000008B System.Void TestingZLegacy::updateValue3ExampleCallback(UnityEngine.Vector3)
extern void TestingZLegacy_updateValue3ExampleCallback_m25E92C1FD9658ABBED9080D237D95E6BCFC57E12 (void);
// 0x0000008C System.Void TestingZLegacy::loopTestClamp()
extern void TestingZLegacy_loopTestClamp_m1EC272930E9852F0D980AC98F1633B4F2841C450 (void);
// 0x0000008D System.Void TestingZLegacy::loopTestPingPong()
extern void TestingZLegacy_loopTestPingPong_mC5B0F54C34F86BD154F6A0BB43C88FC7913344B9 (void);
// 0x0000008E System.Void TestingZLegacy::colorExample()
extern void TestingZLegacy_colorExample_m9C06FD132681F8FF64F1498BFC2811EFD82E4D28 (void);
// 0x0000008F System.Void TestingZLegacy::moveOnACurveExample()
extern void TestingZLegacy_moveOnACurveExample_m89E0D3F9175DBDEAB39A915BCCD5AF06FCD2357F (void);
// 0x00000090 System.Void TestingZLegacy::customTweenExample()
extern void TestingZLegacy_customTweenExample_mF418B80DCF007D21FBD0663BB9F002CF18EAEB0A (void);
// 0x00000091 System.Void TestingZLegacy::moveExample()
extern void TestingZLegacy_moveExample_m5A0E16D835C2A10B7042F64D1B6CD0C1320B1F78 (void);
// 0x00000092 System.Void TestingZLegacy::rotateExample()
extern void TestingZLegacy_rotateExample_m61CF7E2DC245EBF480DCCD0369ABA0F332658917 (void);
// 0x00000093 System.Void TestingZLegacy::rotateOnUpdate(System.Single)
extern void TestingZLegacy_rotateOnUpdate_mE9D7799638F85C32FB7715059ACBDF657C89B7CE (void);
// 0x00000094 System.Void TestingZLegacy::rotateFinished(System.Object)
extern void TestingZLegacy_rotateFinished_mE2E799232C3119A15368CBABBF594C57799D5683 (void);
// 0x00000095 System.Void TestingZLegacy::scaleExample()
extern void TestingZLegacy_scaleExample_mD080FABF7510CFB95F324A1B4BB1C4277937EFF9 (void);
// 0x00000096 System.Void TestingZLegacy::updateValueExample()
extern void TestingZLegacy_updateValueExample_m9DBE93DA0EC50F7E6764A27DE1E87F12ACF8DA2A (void);
// 0x00000097 System.Void TestingZLegacy::updateValueExampleCallback(System.Single,System.Object)
extern void TestingZLegacy_updateValueExampleCallback_mEB667645443DDFD352588C65ACBBE1C500E97C4C (void);
// 0x00000098 System.Void TestingZLegacy::delayedCallExample()
extern void TestingZLegacy_delayedCallExample_m51E456D34EDF2598C8EF549CDDB2376F607C3CC1 (void);
// 0x00000099 System.Void TestingZLegacy::delayedCallExampleCallback()
extern void TestingZLegacy_delayedCallExampleCallback_m15CE90D43A85804204B22169D73FDE3EA46F609A (void);
// 0x0000009A System.Void TestingZLegacy::alphaExample()
extern void TestingZLegacy_alphaExample_mA4ED4E57F148B4EF6E2B1B608BAEE0EEF5AE7143 (void);
// 0x0000009B System.Void TestingZLegacy::moveLocalExample()
extern void TestingZLegacy_moveLocalExample_mE6E3C5CD62456482BADA24B0CA93C0962125B80F (void);
// 0x0000009C System.Void TestingZLegacy::rotateAroundExample()
extern void TestingZLegacy_rotateAroundExample_mB3F00FEDCDD63C7BFD293C187BA847BF15F7ED3C (void);
// 0x0000009D System.Void TestingZLegacy::loopPause()
extern void TestingZLegacy_loopPause_mC83ACD076E19354DC2CFF3D3E409DA2901CE229E (void);
// 0x0000009E System.Void TestingZLegacy::loopResume()
extern void TestingZLegacy_loopResume_mBA3E805FE386DFC40F4F795223744619E9048A7E (void);
// 0x0000009F System.Void TestingZLegacy::punchTest()
extern void TestingZLegacy_punchTest_m1266ADD2035D0E9971ADA805B0A1636D35ABF2B2 (void);
// 0x000000A0 System.Void TestingZLegacy::.ctor()
extern void TestingZLegacy__ctor_m1CA1AEE9AA84B7551AFA5A034BF4E648AE662D9E (void);
// 0x000000A1 System.Void TestingZLegacy/NextFunc::.ctor(System.Object,System.IntPtr)
extern void NextFunc__ctor_mD13871807D65A0BB2C5E0F567E5FB021AF248AED (void);
// 0x000000A2 System.Void TestingZLegacy/NextFunc::Invoke()
extern void NextFunc_Invoke_m8D7A63BD3F35D7D22BC9DAF5241C637889044719 (void);
// 0x000000A3 System.IAsyncResult TestingZLegacy/NextFunc::BeginInvoke(System.AsyncCallback,System.Object)
extern void NextFunc_BeginInvoke_mB38D1030FE53CEF432C2E6D1569F0EBAD0F1B5F0 (void);
// 0x000000A4 System.Void TestingZLegacy/NextFunc::EndInvoke(System.IAsyncResult)
extern void NextFunc_EndInvoke_mFE2435A3542567B6D23B6E63B50BF9EC6D55609B (void);
// 0x000000A5 System.Void TestingZLegacy/<>c::.cctor()
extern void U3CU3Ec__cctor_mBD80E302F3A69C0C61FDB9987E223DF4A13097D9 (void);
// 0x000000A6 System.Void TestingZLegacy/<>c::.ctor()
extern void U3CU3Ec__ctor_mD7A097986D81133EA1341850CD4003072DC490BB (void);
// 0x000000A7 System.Void TestingZLegacy/<>c::<cycleThroughExamples>b__20_0(System.Single)
extern void U3CU3Ec_U3CcycleThroughExamplesU3Eb__20_0_m99E554155125675C01E3E61098DA676F0B69E649 (void);
// 0x000000A8 System.Void TestingZLegacyExt::Awake()
extern void TestingZLegacyExt_Awake_m7D6F79923E270BE07F7B475DBFE0A7CA94534FCB (void);
// 0x000000A9 System.Void TestingZLegacyExt::Start()
extern void TestingZLegacyExt_Start_m86BD1A532532B1F08B6ECF7457358F6CB1410E5E (void);
// 0x000000AA System.Void TestingZLegacyExt::pauseNow()
extern void TestingZLegacyExt_pauseNow_m0A9E3639505AB9DDE82956D7FB34C37758982180 (void);
// 0x000000AB System.Void TestingZLegacyExt::OnGUI()
extern void TestingZLegacyExt_OnGUI_m25777A6897DF3B16F25546C5CAFBF80FB60923AA (void);
// 0x000000AC System.Void TestingZLegacyExt::endlessCallback()
extern void TestingZLegacyExt_endlessCallback_m73A1A18DA9DD7642231DE5BBBF35F45DB785A077 (void);
// 0x000000AD System.Void TestingZLegacyExt::cycleThroughExamples()
extern void TestingZLegacyExt_cycleThroughExamples_m15FB09793AAA4E36F905F6D81C254E0B763F391D (void);
// 0x000000AE System.Void TestingZLegacyExt::updateValue3Example()
extern void TestingZLegacyExt_updateValue3Example_m15F0FA3A4491DBB842544D0A878B2C533640B4AE (void);
// 0x000000AF System.Void TestingZLegacyExt::updateValue3ExampleUpdate(UnityEngine.Vector3)
extern void TestingZLegacyExt_updateValue3ExampleUpdate_m46BCB3BF2A85D38DE7CE46720DF912FDA7DB84FE (void);
// 0x000000B0 System.Void TestingZLegacyExt::updateValue3ExampleCallback(UnityEngine.Vector3)
extern void TestingZLegacyExt_updateValue3ExampleCallback_m7B2BA08D3878E1672F358452FA11C48F8D686B76 (void);
// 0x000000B1 System.Void TestingZLegacyExt::loopTestClamp()
extern void TestingZLegacyExt_loopTestClamp_mA4A7563CB2448E2D1EC1394494E21C79399BAC06 (void);
// 0x000000B2 System.Void TestingZLegacyExt::loopTestPingPong()
extern void TestingZLegacyExt_loopTestPingPong_m6523FCB9E3A1AC5035882C67F6EF5B8A00AAC5D3 (void);
// 0x000000B3 System.Void TestingZLegacyExt::colorExample()
extern void TestingZLegacyExt_colorExample_mCF5A6A5FAA5CC4EC32FFD99062DA09D220B776A2 (void);
// 0x000000B4 System.Void TestingZLegacyExt::moveOnACurveExample()
extern void TestingZLegacyExt_moveOnACurveExample_m8E7EA9EADCB22EB3DC2DDC7FD27BD05050C2FB44 (void);
// 0x000000B5 System.Void TestingZLegacyExt::customTweenExample()
extern void TestingZLegacyExt_customTweenExample_mE32AD842F2ACF6BB96A100E29B784B942004386F (void);
// 0x000000B6 System.Void TestingZLegacyExt::moveExample()
extern void TestingZLegacyExt_moveExample_mB59B97379A841F1AB17E8E9CE8BC7AFF0DB9F3C8 (void);
// 0x000000B7 System.Void TestingZLegacyExt::rotateExample()
extern void TestingZLegacyExt_rotateExample_m15D1742A434CC70072A4C96DFEDB623B1BD8C02F (void);
// 0x000000B8 System.Void TestingZLegacyExt::rotateOnUpdate(System.Single)
extern void TestingZLegacyExt_rotateOnUpdate_m854E95A564B027980BE0FFF08EF1A20CA3EF5EC7 (void);
// 0x000000B9 System.Void TestingZLegacyExt::rotateFinished(System.Object)
extern void TestingZLegacyExt_rotateFinished_m78E88F2F51C8018BBA472122955C89DCD3C13B53 (void);
// 0x000000BA System.Void TestingZLegacyExt::scaleExample()
extern void TestingZLegacyExt_scaleExample_mDDF64026F75968C35562CEE5C1B2F84874C76C1A (void);
// 0x000000BB System.Void TestingZLegacyExt::updateValueExample()
extern void TestingZLegacyExt_updateValueExample_mD778D12570BC222BB68613D27BD4B5B0DCD9E76C (void);
// 0x000000BC System.Void TestingZLegacyExt::updateValueExampleCallback(System.Single,System.Object)
extern void TestingZLegacyExt_updateValueExampleCallback_m273FBEFFA8289DCB71813A6C4FF5C66ABD2C5003 (void);
// 0x000000BD System.Void TestingZLegacyExt::delayedCallExample()
extern void TestingZLegacyExt_delayedCallExample_m92DECA48CA80DF5FF46B873EF87AC2A564AA6452 (void);
// 0x000000BE System.Void TestingZLegacyExt::delayedCallExampleCallback()
extern void TestingZLegacyExt_delayedCallExampleCallback_m2F2F9A40BA43F410DABE187FEA3861FCF719058B (void);
// 0x000000BF System.Void TestingZLegacyExt::alphaExample()
extern void TestingZLegacyExt_alphaExample_m0875576982CCE840BF262C71817239AB7EA6C8C2 (void);
// 0x000000C0 System.Void TestingZLegacyExt::moveLocalExample()
extern void TestingZLegacyExt_moveLocalExample_m81967DF8243173690E01BCD0C9DEDF141C080813 (void);
// 0x000000C1 System.Void TestingZLegacyExt::rotateAroundExample()
extern void TestingZLegacyExt_rotateAroundExample_mA9188C5A54C644D2B78F8956DAB3B245B975BCA1 (void);
// 0x000000C2 System.Void TestingZLegacyExt::loopPause()
extern void TestingZLegacyExt_loopPause_m04999A727B7B5D4D7C04BA4D24C78FD7C35FCB42 (void);
// 0x000000C3 System.Void TestingZLegacyExt::loopResume()
extern void TestingZLegacyExt_loopResume_mAD5DEA4AB4F83437FE25A8C8BBE141FB885CB6CF (void);
// 0x000000C4 System.Void TestingZLegacyExt::punchTest()
extern void TestingZLegacyExt_punchTest_mEE9802D38D39AB2BE2BE50BD88F901A16383B87B (void);
// 0x000000C5 System.Void TestingZLegacyExt::.ctor()
extern void TestingZLegacyExt__ctor_m5E300E444E4496DA32F5F4A1F8B48051126D8B61 (void);
// 0x000000C6 System.Void TestingZLegacyExt/NextFunc::.ctor(System.Object,System.IntPtr)
extern void NextFunc__ctor_m0008349A4886ECE27F576CFE82C9A888798F60BA (void);
// 0x000000C7 System.Void TestingZLegacyExt/NextFunc::Invoke()
extern void NextFunc_Invoke_mFB6931473FA9F6AD02BE8B752A4D051ECB660ECF (void);
// 0x000000C8 System.IAsyncResult TestingZLegacyExt/NextFunc::BeginInvoke(System.AsyncCallback,System.Object)
extern void NextFunc_BeginInvoke_mD91FEF6AFFC5695F493E6950FAAA8A6B72DB37CD (void);
// 0x000000C9 System.Void TestingZLegacyExt/NextFunc::EndInvoke(System.IAsyncResult)
extern void NextFunc_EndInvoke_m96DB2010CA80A82BDCD35903E0CE02CE8AC78296 (void);
// 0x000000CA System.Void TestingZLegacyExt/<>c::.cctor()
extern void U3CU3Ec__cctor_mD11D91D44BCDECB51A3CF966B686782B4F5330A7 (void);
// 0x000000CB System.Void TestingZLegacyExt/<>c::.ctor()
extern void U3CU3Ec__ctor_mC490B74A87268C68E2428BE981C75A67BE4460A7 (void);
// 0x000000CC System.Void TestingZLegacyExt/<>c::<cycleThroughExamples>b__20_0(System.Single)
extern void U3CU3Ec_U3CcycleThroughExamplesU3Eb__20_0_m232F589CF9029278662FD1D5BB31DB7E6A2973F5 (void);
// 0x000000CD System.Void LeanAudioStream::.ctor(System.Single[])
extern void LeanAudioStream__ctor_m767420411C73925D7260141B6AC141C332340F57 (void);
// 0x000000CE System.Void LeanAudioStream::OnAudioRead(System.Single[])
extern void LeanAudioStream_OnAudioRead_mB0B822C29AF57CD64964495BC47B2D552C9A61EE (void);
// 0x000000CF System.Void LeanAudioStream::OnAudioSetPosition(System.Int32)
extern void LeanAudioStream_OnAudioSetPosition_m84D20E5E2E90A883DA74466C176BD12EF300CFD7 (void);
// 0x000000D0 LeanAudioOptions LeanAudio::options()
extern void LeanAudio_options_m24F4466E9FF88143F014E1341ECAA03A0BD1DE16 (void);
// 0x000000D1 LeanAudioStream LeanAudio::createAudioStream(UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,LeanAudioOptions)
extern void LeanAudio_createAudioStream_mF0B2E4AE61E77E5E4F5BB229BB17EA0010EED167 (void);
// 0x000000D2 UnityEngine.AudioClip LeanAudio::createAudio(UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,LeanAudioOptions)
extern void LeanAudio_createAudio_m741D84D297AF8413E9A6890FA81CD7F288A303C7 (void);
// 0x000000D3 System.Int32 LeanAudio::createAudioWave(UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,LeanAudioOptions)
extern void LeanAudio_createAudioWave_m4368517E2416AF360A06777597C9A3142BA5FC00 (void);
// 0x000000D4 UnityEngine.AudioClip LeanAudio::createAudioFromWave(System.Int32,LeanAudioOptions)
extern void LeanAudio_createAudioFromWave_m1D0C1A2142307BF2378F25C76C42F61265D5ED21 (void);
// 0x000000D5 System.Void LeanAudio::OnAudioSetPosition(System.Int32)
extern void LeanAudio_OnAudioSetPosition_m7FBC95FBFCA93F4A878761356FFD1B4FB716331C (void);
// 0x000000D6 UnityEngine.AudioClip LeanAudio::generateAudioFromCurve(UnityEngine.AnimationCurve,System.Int32)
extern void LeanAudio_generateAudioFromCurve_mD5AE758FCAB5F326FECD85B37A313678B9D8929B (void);
// 0x000000D7 UnityEngine.AudioSource LeanAudio::play(UnityEngine.AudioClip,System.Single)
extern void LeanAudio_play_m96190E0C8E00011FC6A98EFEBDFAEE2E3C06AB9E (void);
// 0x000000D8 UnityEngine.AudioSource LeanAudio::play(UnityEngine.AudioClip)
extern void LeanAudio_play_m7F543AE840832C2D4FC88AA184C605A892F1C030 (void);
// 0x000000D9 UnityEngine.AudioSource LeanAudio::play(UnityEngine.AudioClip,UnityEngine.Vector3)
extern void LeanAudio_play_m6CD5A5DC70FD339BDD5BEDC9243FACA5C3186451 (void);
// 0x000000DA UnityEngine.AudioSource LeanAudio::play(UnityEngine.AudioClip,UnityEngine.Vector3,System.Single)
extern void LeanAudio_play_mBC8A61F39D8ECDCA09F458FB1028945AC35DB0D2 (void);
// 0x000000DB UnityEngine.AudioSource LeanAudio::playClipAt(UnityEngine.AudioClip,UnityEngine.Vector3)
extern void LeanAudio_playClipAt_mC86BC94850F21DBC4B9C831CC666FEBC86A2C510 (void);
// 0x000000DC System.Void LeanAudio::printOutAudioClip(UnityEngine.AudioClip,UnityEngine.AnimationCurve&,System.Single)
extern void LeanAudio_printOutAudioClip_m22003A70913081D802CBF76538C92F929CD8D857 (void);
// 0x000000DD System.Void LeanAudio::.ctor()
extern void LeanAudio__ctor_m492D6C2E9108D90D229F0811C3B185DFD891907A (void);
// 0x000000DE System.Void LeanAudio::.cctor()
extern void LeanAudio__cctor_m31067C511A043987DBFED412C6486A1D4D2CBF53 (void);
// 0x000000DF System.Void LeanAudioOptions::.ctor()
extern void LeanAudioOptions__ctor_m29A189A1499462B631EBD724FF961389527A8EA2 (void);
// 0x000000E0 LeanAudioOptions LeanAudioOptions::setFrequency(System.Int32)
extern void LeanAudioOptions_setFrequency_m85E50359A5904A370955AFF52AC3D88CC01EBBC5 (void);
// 0x000000E1 LeanAudioOptions LeanAudioOptions::setVibrato(UnityEngine.Vector3[])
extern void LeanAudioOptions_setVibrato_mF64FB8E4DD576D90776F76B01B753247D99B49DB (void);
// 0x000000E2 LeanAudioOptions LeanAudioOptions::setWaveSine()
extern void LeanAudioOptions_setWaveSine_m76F10085FDCE754A60760BB1542537F9DA46034F (void);
// 0x000000E3 LeanAudioOptions LeanAudioOptions::setWaveSquare()
extern void LeanAudioOptions_setWaveSquare_mF8F08EACD82E37E15137F5F1C785DD0D671AF42E (void);
// 0x000000E4 LeanAudioOptions LeanAudioOptions::setWaveSawtooth()
extern void LeanAudioOptions_setWaveSawtooth_m27624D2920970C41AD3CE47DE408FE9C7EEA530E (void);
// 0x000000E5 LeanAudioOptions LeanAudioOptions::setWaveNoise()
extern void LeanAudioOptions_setWaveNoise_m55FF493A787F18D8692DF85866D055354AE1A2B0 (void);
// 0x000000E6 LeanAudioOptions LeanAudioOptions::setWaveStyle(LeanAudioOptions/LeanAudioWaveStyle)
extern void LeanAudioOptions_setWaveStyle_mEB0CA095133249C1947E51FA9FBE9657F7A784CF (void);
// 0x000000E7 LeanAudioOptions LeanAudioOptions::setWaveNoiseScale(System.Single)
extern void LeanAudioOptions_setWaveNoiseScale_m68B9C76F8DD866150B060285CF705D4ECB5C533C (void);
// 0x000000E8 LeanAudioOptions LeanAudioOptions::setWaveNoiseInfluence(System.Single)
extern void LeanAudioOptions_setWaveNoiseInfluence_m7CBBEC51092269C15133C3B53D1828ACC874AF56 (void);
// 0x000000E9 System.Single LeanSmooth::damp(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single)
extern void LeanSmooth_damp_mAFCFAA57FEE313048C9AF8C5F16FC2F5E723D0FF (void);
// 0x000000EA UnityEngine.Vector3 LeanSmooth::damp(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&,System.Single,System.Single,System.Single)
extern void LeanSmooth_damp_m7CCE1AB623BF22468A59B97CC2D161682476168E (void);
// 0x000000EB UnityEngine.Color LeanSmooth::damp(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color&,System.Single,System.Single,System.Single)
extern void LeanSmooth_damp_m9AA68F6F1D20FF61F338BA122A374CB0F10D6EFE (void);
// 0x000000EC System.Single LeanSmooth::spring(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_spring_m314389AD151BF197F11CB50DFEC3F05396E189AD (void);
// 0x000000ED UnityEngine.Vector3 LeanSmooth::spring(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_spring_mAC93690E733A39072CB37D5BFF1239414F98E1EF (void);
// 0x000000EE UnityEngine.Color LeanSmooth::spring(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color&,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_spring_mC13A607AA221D50EB1AB72CE0C5D314F2748E946 (void);
// 0x000000EF System.Single LeanSmooth::linear(System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_linear_mCAA760AD54BCCFD8A3C67631437C528F84806A11 (void);
// 0x000000F0 UnityEngine.Vector3 LeanSmooth::linear(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanSmooth_linear_mE9F99D1FD99A7D58A0F81A3A39EA4D994D55BB6D (void);
// 0x000000F1 UnityEngine.Color LeanSmooth::linear(UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanSmooth_linear_mD4AAD42ECD1AFD102F576A52116C57CDEEFB73E7 (void);
// 0x000000F2 System.Single LeanSmooth::bounceOut(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_bounceOut_mD1B8F20CB910CD8609D9F5988472CCC83B98DE6A (void);
// 0x000000F3 UnityEngine.Vector3 LeanSmooth::bounceOut(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_bounceOut_m39C1F55CD36843F594D1C58D067696AD294C4812 (void);
// 0x000000F4 UnityEngine.Color LeanSmooth::bounceOut(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color&,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_bounceOut_mF3E43703A80AB9D705575A9D803AD2E8AF3722F1 (void);
// 0x000000F5 System.Void LeanSmooth::.ctor()
extern void LeanSmooth__ctor_mA939A643B1463547170CECB7FCC6AF013FF0DCAA (void);
// 0x000000F6 System.Void LeanTester::Start()
extern void LeanTester_Start_m9EEED491D1FBBF5A677178E6477E7B68491F1517 (void);
// 0x000000F7 System.Collections.IEnumerator LeanTester::timeoutCheck()
extern void LeanTester_timeoutCheck_m59067A5600F0D2434CE9B7EDAC062829C85D5C00 (void);
// 0x000000F8 System.Void LeanTester::.ctor()
extern void LeanTester__ctor_mBB9C14D1DDB7AA3F591D6B0040EEA74F032EB1F7 (void);
// 0x000000F9 System.Void LeanTester/<timeoutCheck>d__2::.ctor(System.Int32)
extern void U3CtimeoutCheckU3Ed__2__ctor_mACFA1EA79EDBEA235C7474D1BA3E71804A23FEB8 (void);
// 0x000000FA System.Void LeanTester/<timeoutCheck>d__2::System.IDisposable.Dispose()
extern void U3CtimeoutCheckU3Ed__2_System_IDisposable_Dispose_m0AF241B0BA496E508C41A00A92ACCB507F764F3B (void);
// 0x000000FB System.Boolean LeanTester/<timeoutCheck>d__2::MoveNext()
extern void U3CtimeoutCheckU3Ed__2_MoveNext_mF2666B91C53937A9B6CFBCDB7E111B6E74685643 (void);
// 0x000000FC System.Object LeanTester/<timeoutCheck>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtimeoutCheckU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF1EF5FF8E038CD04BB438F282E437032A52DF973 (void);
// 0x000000FD System.Void LeanTester/<timeoutCheck>d__2::System.Collections.IEnumerator.Reset()
extern void U3CtimeoutCheckU3Ed__2_System_Collections_IEnumerator_Reset_m377EF3F9C3E2B3A09D749A0A347B92595F0D756D (void);
// 0x000000FE System.Object LeanTester/<timeoutCheck>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CtimeoutCheckU3Ed__2_System_Collections_IEnumerator_get_Current_m275B7C4BFE741FFBA1D7CE3E422460E5BD15EFF6 (void);
// 0x000000FF System.Void LeanTest::debug(System.String,System.Boolean,System.String)
extern void LeanTest_debug_m185769356E43C51F1A057B12DC7F0E6970CA05D2 (void);
// 0x00000100 System.Void LeanTest::expect(System.Boolean,System.String,System.String)
extern void LeanTest_expect_m697408A02255410088277BB6C41F73FFB789458B (void);
// 0x00000101 System.String LeanTest::padRight(System.Int32)
extern void LeanTest_padRight_m6DEC7C60D840B9D4F35BDFD3A27411551F182CF2 (void);
// 0x00000102 System.Single LeanTest::printOutLength(System.String)
extern void LeanTest_printOutLength_mE439776B28BB9737553D43E95254DD2A79676EFF (void);
// 0x00000103 System.String LeanTest::formatBC(System.String,System.String)
extern void LeanTest_formatBC_mCE1B1751EF0B35A008C32A43AEBACA76B2FF0272 (void);
// 0x00000104 System.String LeanTest::formatB(System.String)
extern void LeanTest_formatB_mDF309FD724B541A97D931A28FE32B9F3245DA966 (void);
// 0x00000105 System.String LeanTest::formatC(System.String,System.String)
extern void LeanTest_formatC_m10095A4C3AEB8A45468A8251101CE25BE1643F34 (void);
// 0x00000106 System.Void LeanTest::overview()
extern void LeanTest_overview_mFBDD0E748CC3DF470B668293185D2637F2ACF17F (void);
// 0x00000107 System.Void LeanTest::.ctor()
extern void LeanTest__ctor_m2534B2F8BA29F28EF5D26482735FE6D93B229F6E (void);
// 0x00000108 System.Void LeanTest::.cctor()
extern void LeanTest__cctor_mE75EB5D6AF852A6D0B480FCC70D1F9958AFE0943 (void);
// 0x00000109 System.Void LeanTween::init()
extern void LeanTween_init_m2257D056F7462641D1316804B27DB2EF03C54B0B (void);
// 0x0000010A System.Int32 LeanTween::get_maxSearch()
extern void LeanTween_get_maxSearch_m76F5A3551B333FEAD6918DC0795238CEA51E8764 (void);
// 0x0000010B System.Int32 LeanTween::get_maxSimulataneousTweens()
extern void LeanTween_get_maxSimulataneousTweens_m38608BBD6E41F3B66FF348DE652F2EA6F1BEBF45 (void);
// 0x0000010C System.Int32 LeanTween::get_tweensRunning()
extern void LeanTween_get_tweensRunning_mDD39747E471B1CCDF83F6F79B5CFA224E47D3842 (void);
// 0x0000010D System.Void LeanTween::init(System.Int32)
extern void LeanTween_init_m4CE7872C3DBD46FF7252C60DA96FD99FE9A071C8 (void);
// 0x0000010E System.Void LeanTween::init(System.Int32,System.Int32)
extern void LeanTween_init_m641164BCF025E634EFBD06151E55753939DB6F13 (void);
// 0x0000010F System.Void LeanTween::reset()
extern void LeanTween_reset_mC778C2B33DA945C2D7FC099D6B0EDE24A98A78AD (void);
// 0x00000110 System.Void LeanTween::Update()
extern void LeanTween_Update_m4E96DD5D854DBD4CDF76759B63786DDD8739BF08 (void);
// 0x00000111 System.Void LeanTween::onLevelWasLoaded54(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void LeanTween_onLevelWasLoaded54_mE6FF28F42BD62CC30963F9D80C2B26FFF0B3C5B0 (void);
// 0x00000112 System.Void LeanTween::internalOnLevelWasLoaded(System.Int32)
extern void LeanTween_internalOnLevelWasLoaded_m14ED1FA1C1214624DC4A780E737D2FFE90A3B0AB (void);
// 0x00000113 System.Void LeanTween::update()
extern void LeanTween_update_m2BF953CB1B4904B57BC6A43058FDA4BE52B57688 (void);
// 0x00000114 System.Void LeanTween::removeTween(System.Int32,System.Int32)
extern void LeanTween_removeTween_mD1216980A45C032C76FBD7D66BD3ECD81F992D84 (void);
// 0x00000115 System.Void LeanTween::removeTween(System.Int32)
extern void LeanTween_removeTween_mEF159BEAAC6DC0EEAA016966749C762D3EE88A33 (void);
// 0x00000116 UnityEngine.Vector3[] LeanTween::add(UnityEngine.Vector3[],UnityEngine.Vector3)
extern void LeanTween_add_mF9434A6D817FB5B901ED97889690158C6E99AD4E (void);
// 0x00000117 System.Single LeanTween::closestRot(System.Single,System.Single)
extern void LeanTween_closestRot_m45089460836416CF978CED05F04748378298675C (void);
// 0x00000118 System.Void LeanTween::cancelAll()
extern void LeanTween_cancelAll_m017FC44A7C2778837326A7518192A881D4F39FD2 (void);
// 0x00000119 System.Void LeanTween::cancelAll(System.Boolean)
extern void LeanTween_cancelAll_mC78FDFE4A1045C23D35C9447D3D71FFBDA2837CE (void);
// 0x0000011A System.Void LeanTween::cancel(UnityEngine.GameObject)
extern void LeanTween_cancel_m8F49EAB6908B4BE0715C0D4160C51516DCDD2823 (void);
// 0x0000011B System.Void LeanTween::cancel(UnityEngine.GameObject,System.Boolean)
extern void LeanTween_cancel_m28B082D5434EBF590F33D552278E4969614E35E6 (void);
// 0x0000011C System.Void LeanTween::cancel(UnityEngine.RectTransform)
extern void LeanTween_cancel_mE6AF5A1A673FCA3C9EAFBAE4C15EBE5ED42B7710 (void);
// 0x0000011D System.Void LeanTween::cancel(UnityEngine.GameObject,System.Int32,System.Boolean)
extern void LeanTween_cancel_mF3E2B961A0AB5C23CB2EB64B557AC718BF931F9C (void);
// 0x0000011E System.Void LeanTween::cancel(LTRect,System.Int32)
extern void LeanTween_cancel_mE5CB54E734F2E1DD02E9979D64E163400677AA7B (void);
// 0x0000011F System.Void LeanTween::cancel(System.Int32)
extern void LeanTween_cancel_m8F235169322AEA85E23C774745EED07BD83D344E (void);
// 0x00000120 System.Void LeanTween::cancel(System.Int32,System.Boolean)
extern void LeanTween_cancel_m54BD97AD0C8C50A9EE5645108B28F7E25104EA01 (void);
// 0x00000121 LTDescr LeanTween::descr(System.Int32)
extern void LeanTween_descr_m0686304AC8834008456087DDB221577D87246C90 (void);
// 0x00000122 LTDescr LeanTween::description(System.Int32)
extern void LeanTween_description_mCF024DD0005701DAC6CFDDFCAC0FCBF66C613B38 (void);
// 0x00000123 LTDescr[] LeanTween::descriptions(UnityEngine.GameObject)
extern void LeanTween_descriptions_m2ADDBCE0DFC78640128B7224DD86FE6C3938D76C (void);
// 0x00000124 System.Void LeanTween::pause(UnityEngine.GameObject,System.Int32)
extern void LeanTween_pause_mAF75D7E636CB0D02F8D515FF76C23D99B8B28FB1 (void);
// 0x00000125 System.Void LeanTween::pause(System.Int32)
extern void LeanTween_pause_mEE3D7E0190BB1879C5E4E40A34E22146B867B2B0 (void);
// 0x00000126 System.Void LeanTween::pause(UnityEngine.GameObject)
extern void LeanTween_pause_m28FA361A74060F3CF6CFF07EA470DEB5871ABFFF (void);
// 0x00000127 System.Void LeanTween::pauseAll()
extern void LeanTween_pauseAll_mBC633AE2F834B3E89A97848EBE21C2CF24E04214 (void);
// 0x00000128 System.Void LeanTween::resumeAll()
extern void LeanTween_resumeAll_m933FBC788EED5D67979F160369D98BEF44CCB435 (void);
// 0x00000129 System.Void LeanTween::resume(UnityEngine.GameObject,System.Int32)
extern void LeanTween_resume_m1940BE978DB7EC3606D92C181FFE10A4570043E2 (void);
// 0x0000012A System.Void LeanTween::resume(System.Int32)
extern void LeanTween_resume_mB671FD12433254652D7A6F1F2E8B5A924EE8665C (void);
// 0x0000012B System.Void LeanTween::resume(UnityEngine.GameObject)
extern void LeanTween_resume_mB910424A52CA71E88F376EBC8BD7DAC8BA638F3A (void);
// 0x0000012C System.Boolean LeanTween::isPaused(UnityEngine.GameObject)
extern void LeanTween_isPaused_m36EF7ACA58D5CF19D261ECC2740D3BABE292AE17 (void);
// 0x0000012D System.Boolean LeanTween::isPaused(UnityEngine.RectTransform)
extern void LeanTween_isPaused_m8433AB793191572FC80B287A05951CCA46D60415 (void);
// 0x0000012E System.Boolean LeanTween::isPaused(System.Int32)
extern void LeanTween_isPaused_mCBFAAB19270CC1E95412B9DB3F5FCE12F56B64C2 (void);
// 0x0000012F System.Boolean LeanTween::isTweening(UnityEngine.GameObject)
extern void LeanTween_isTweening_m7D2702558144653FEF2A2994BFFDB95BB547D76E (void);
// 0x00000130 System.Boolean LeanTween::isTweening(UnityEngine.RectTransform)
extern void LeanTween_isTweening_m0DD0D935F7AEC656A1397DD8DFFADA8509ACCBD1 (void);
// 0x00000131 System.Boolean LeanTween::isTweening(System.Int32)
extern void LeanTween_isTweening_m8A9E15F4297CE4D50C54540646F91DEE0767071C (void);
// 0x00000132 System.Boolean LeanTween::isTweening(LTRect)
extern void LeanTween_isTweening_mD1EC7E088742B5EB1A698E9C82DDC9F4AF2F831A (void);
// 0x00000133 System.Void LeanTween::drawBezierPath(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Transform)
extern void LeanTween_drawBezierPath_m140E21617AD80CA0F665DA5A74E8AB641A5C3A41 (void);
// 0x00000134 System.Object LeanTween::logError(System.String)
extern void LeanTween_logError_m941ECF9F4DA921ACE1845B97FE0C814F9E26B1CA (void);
// 0x00000135 LTDescr LeanTween::options(LTDescr)
extern void LeanTween_options_mC569B03E642CE27936B5ADF511ACE67C40E3EFD6 (void);
// 0x00000136 LTDescr LeanTween::options()
extern void LeanTween_options_m859EFA1A4C3BE0E937966B8F5D902F798B60B2C1 (void);
// 0x00000137 UnityEngine.GameObject LeanTween::get_tweenEmpty()
extern void LeanTween_get_tweenEmpty_mB1509CF6AE8FAB5591F914CBC1CC1DD5C7447F08 (void);
// 0x00000138 LTDescr LeanTween::pushNewTween(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,LTDescr)
extern void LeanTween_pushNewTween_m4C8A3AFE33EDDBCEF0FAFB6D9DE68BE6D930014D (void);
// 0x00000139 LTDescr LeanTween::play(UnityEngine.RectTransform,UnityEngine.Sprite[])
extern void LeanTween_play_m8F84314D3491001A540FA92F7A4F0B1A66ABC010 (void);
// 0x0000013A LTSeq LeanTween::sequence(System.Boolean)
extern void LeanTween_sequence_m8FE5B82BEF69D9AF66D54E849587B4CF933E34A5 (void);
// 0x0000013B LTDescr LeanTween::alpha(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_alpha_m9D8A4A0F46CBF56AA0008446BCE391A124BDE913 (void);
// 0x0000013C LTDescr LeanTween::alpha(LTRect,System.Single,System.Single)
extern void LeanTween_alpha_mCE5E7D21CF2E2687DD1CFB80A9EE40E1E2EBCC0C (void);
// 0x0000013D LTDescr LeanTween::textAlpha(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_textAlpha_m31154DC1D5392385E4318CE77677AC4E29CF99B0 (void);
// 0x0000013E LTDescr LeanTween::alphaText(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_alphaText_mFD7A2FD730E1BA7638F985A93CE91B5EA652C126 (void);
// 0x0000013F LTDescr LeanTween::alphaCanvas(UnityEngine.CanvasGroup,System.Single,System.Single)
extern void LeanTween_alphaCanvas_m8BCDB0DB0CAE28F75A7DDF79FA6D62FC1B3AD513 (void);
// 0x00000140 LTDescr LeanTween::alphaVertex(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_alphaVertex_m94B9F32105B2C406F41E8D014FD6E5E235EFA506 (void);
// 0x00000141 LTDescr LeanTween::color(UnityEngine.GameObject,UnityEngine.Color,System.Single)
extern void LeanTween_color_mB591490EB11BF6AAB3D9C823844CD51A54CB54AF (void);
// 0x00000142 LTDescr LeanTween::textColor(UnityEngine.RectTransform,UnityEngine.Color,System.Single)
extern void LeanTween_textColor_m4BCEDC9A9BAB880E6C4440833CF36C2ED3101343 (void);
// 0x00000143 LTDescr LeanTween::colorText(UnityEngine.RectTransform,UnityEngine.Color,System.Single)
extern void LeanTween_colorText_m34F5C1169E22FE097A19CC6B85377C738D73B937 (void);
// 0x00000144 LTDescr LeanTween::delayedCall(System.Single,System.Action)
extern void LeanTween_delayedCall_mF74ECC6AC349466F55E56C4625DB67892553A59A (void);
// 0x00000145 LTDescr LeanTween::delayedCall(System.Single,System.Action`1<System.Object>)
extern void LeanTween_delayedCall_mF0264F6DCD013BFA7483D1399C04DA37502DC729 (void);
// 0x00000146 LTDescr LeanTween::delayedCall(UnityEngine.GameObject,System.Single,System.Action)
extern void LeanTween_delayedCall_m537234980306375FFF55F428FE0F5AF5F35AD1CB (void);
// 0x00000147 LTDescr LeanTween::delayedCall(UnityEngine.GameObject,System.Single,System.Action`1<System.Object>)
extern void LeanTween_delayedCall_m15A55B8039CD7A86856CE10FF15AC9F99D8D8EE4 (void);
// 0x00000148 LTDescr LeanTween::destroyAfter(LTRect,System.Single)
extern void LeanTween_destroyAfter_m8EBFD0632BEE8F6EB0280D36C85CE24414C0FF15 (void);
// 0x00000149 LTDescr LeanTween::move(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTween_move_m78D08B26807623C822B81CFA60DAC960DDBAC377 (void);
// 0x0000014A LTDescr LeanTween::move(UnityEngine.GameObject,UnityEngine.Vector2,System.Single)
extern void LeanTween_move_m1E331CB112535FB9EA90E55461A2D77D5260B530 (void);
// 0x0000014B LTDescr LeanTween::move(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTween_move_m44271CB95A4B41D5EB76C4D83B419135A6EF33EC (void);
// 0x0000014C LTDescr LeanTween::move(UnityEngine.GameObject,LTBezierPath,System.Single)
extern void LeanTween_move_mFEDFA180C09AB9A50941E5F63CD8670356C67ED3 (void);
// 0x0000014D LTDescr LeanTween::move(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTween_move_mAE5F83E88AABED48945E61924B0427C1A9D2D2B7 (void);
// 0x0000014E LTDescr LeanTween::moveSpline(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTween_moveSpline_m5935DB5E37D36935FF3FBDA2C498F927FB7F5570 (void);
// 0x0000014F LTDescr LeanTween::moveSpline(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTween_moveSpline_mB64C328098A521A9E7BB2EEAF0258238DF2DF712 (void);
// 0x00000150 LTDescr LeanTween::moveSplineLocal(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTween_moveSplineLocal_m280D26BA7DE193E5F3EBB523ACEE6E44FF0978B7 (void);
// 0x00000151 LTDescr LeanTween::move(LTRect,UnityEngine.Vector2,System.Single)
extern void LeanTween_move_m5E61C62A0F22777D370EB7CF2E6C0AC2EF3EB8B5 (void);
// 0x00000152 LTDescr LeanTween::moveMargin(LTRect,UnityEngine.Vector2,System.Single)
extern void LeanTween_moveMargin_mCD8BA91FB1CB5762858AB694C374525B40570658 (void);
// 0x00000153 LTDescr LeanTween::moveX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveX_m3345DDA9D080FF581C38E0B92B7B2A0389A61DB3 (void);
// 0x00000154 LTDescr LeanTween::moveY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveY_mF3562A335C9D8A19192A1D05BBBB062D5696E01F (void);
// 0x00000155 LTDescr LeanTween::moveZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveZ_m1D2828EEFBA366465AE916017B04BD730DB02918 (void);
// 0x00000156 LTDescr LeanTween::moveLocal(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTween_moveLocal_m5EA4899950A5A6E5244A472D5B0095A456A3E828 (void);
// 0x00000157 LTDescr LeanTween::moveLocal(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTween_moveLocal_m5EFEFD16EBF5E4EFC333418953F72DA504E191A0 (void);
// 0x00000158 LTDescr LeanTween::moveLocalX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveLocalX_m6CE239F4E86EE5439B2D86FB0163306EC152DAD4 (void);
// 0x00000159 LTDescr LeanTween::moveLocalY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveLocalY_m2EBE679F58F63B2D3BDA557474CD0C99B6AB8C81 (void);
// 0x0000015A LTDescr LeanTween::moveLocalZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveLocalZ_mCCAE8DEB09113FABEE3D66031E1B25C2B8BD318B (void);
// 0x0000015B LTDescr LeanTween::moveLocal(UnityEngine.GameObject,LTBezierPath,System.Single)
extern void LeanTween_moveLocal_m4D6E9C2DFFB9D9BF77A70A417CEDEB22D7B0689C (void);
// 0x0000015C LTDescr LeanTween::moveLocal(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTween_moveLocal_m3AB20706D4DAFD80EB42795B594331820B1C41E2 (void);
// 0x0000015D LTDescr LeanTween::move(UnityEngine.GameObject,UnityEngine.Transform,System.Single)
extern void LeanTween_move_mAF8F6C1C95F4A9E38C99E9471889EF3D6F26B21B (void);
// 0x0000015E LTDescr LeanTween::rotate(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTween_rotate_mFE6F0AFD90CF08BC42B97AB676D30F5D6B685BB1 (void);
// 0x0000015F LTDescr LeanTween::rotate(LTRect,System.Single,System.Single)
extern void LeanTween_rotate_mCDB520E4B0ACB36B9EE004C8CF8B3C0A5EC0D9E2 (void);
// 0x00000160 LTDescr LeanTween::rotateLocal(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTween_rotateLocal_m5B5E5981164221594D9D4FF15C84D7B9FB81C067 (void);
// 0x00000161 LTDescr LeanTween::rotateX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_rotateX_mCBA79F277E3512C992772E15E9298BEAFAA046D0 (void);
// 0x00000162 LTDescr LeanTween::rotateY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_rotateY_m14E1525768E32FDF7A2378B1C8AF023D1C0B1C24 (void);
// 0x00000163 LTDescr LeanTween::rotateZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_rotateZ_mA0AD8A6E7771AB656243CD3669EBDB0E38CA2408 (void);
// 0x00000164 LTDescr LeanTween::rotateAround(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTween_rotateAround_m588F9185E90CC14CAB0B0EB6C3D575CF7E7C285E (void);
// 0x00000165 LTDescr LeanTween::rotateAroundLocal(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTween_rotateAroundLocal_mC869325E94AD6A0B95314D081F29B976CA91A81B (void);
// 0x00000166 LTDescr LeanTween::scale(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTween_scale_m684CBC818ED1F1ED8C50D1BD0F49495CACC0067C (void);
// 0x00000167 LTDescr LeanTween::scale(LTRect,UnityEngine.Vector2,System.Single)
extern void LeanTween_scale_m16AF41D6C5EB894246FDE21B3B9B90EADE1949AC (void);
// 0x00000168 LTDescr LeanTween::scaleX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_scaleX_m4DCF8A388D1FE2AF9C40F67DDB77B958DE1F0220 (void);
// 0x00000169 LTDescr LeanTween::scaleY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_scaleY_m011130E1548F777DC187AEF2A88291CDE924851B (void);
// 0x0000016A LTDescr LeanTween::scaleZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_scaleZ_m2CF069E5E4EC0E4BC040DBA5C759E1A401A67481 (void);
// 0x0000016B LTDescr LeanTween::value(UnityEngine.GameObject,System.Single,System.Single,System.Single)
extern void LeanTween_value_m16722DEDC681F28C188D0F4557F0BF9B6A1488AF (void);
// 0x0000016C LTDescr LeanTween::value(System.Single,System.Single,System.Single)
extern void LeanTween_value_m85D0A108DCD42E6A1576B1CC53D2F9D00B0E47FC (void);
// 0x0000016D LTDescr LeanTween::value(UnityEngine.GameObject,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void LeanTween_value_m6B91F8AA266FFF0C593FF90E7A6BE3D1960B84E9 (void);
// 0x0000016E LTDescr LeanTween::value(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LeanTween_value_m45BC8CC34DB8AE45C89048BAAAA016A4C6F0D526 (void);
// 0x0000016F LTDescr LeanTween::value(UnityEngine.GameObject,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanTween_value_m91EB5E56B4A910CDE7D3F1CD5EFA1E352FEC04D8 (void);
// 0x00000170 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`1<System.Single>,System.Single,System.Single,System.Single)
extern void LeanTween_value_mAC251E42C465A03544154ADD2059E8808E208F4A (void);
// 0x00000171 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`2<System.Single,System.Single>,System.Single,System.Single,System.Single)
extern void LeanTween_value_mDA02C25B8315D819999BA319C0E183E3534BC0D9 (void);
// 0x00000172 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`1<UnityEngine.Color>,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanTween_value_mEA929F405968830C7B82A604B40F5810DA0274B8 (void);
// 0x00000173 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`2<UnityEngine.Color,System.Object>,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanTween_value_m3F5B71A925784CD187198B3AB14CDB08351AE715 (void);
// 0x00000174 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`1<UnityEngine.Vector2>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void LeanTween_value_m214501A86FB206A4A18BE7C373F67B1E3BF90E17 (void);
// 0x00000175 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`1<UnityEngine.Vector3>,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LeanTween_value_mEE3F62AA3FED7E7FFAF54C8CB689045550F31E2B (void);
// 0x00000176 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`2<System.Single,System.Object>,System.Single,System.Single,System.Single)
extern void LeanTween_value_m062FD40F3682DD46D70A1FD5740CE32C421BE536 (void);
// 0x00000177 LTDescr LeanTween::delayedSound(UnityEngine.AudioClip,UnityEngine.Vector3,System.Single)
extern void LeanTween_delayedSound_mEE95C5C431F6AE155BC4B5CE22679353E5BC5BCA (void);
// 0x00000178 LTDescr LeanTween::delayedSound(UnityEngine.GameObject,UnityEngine.AudioClip,UnityEngine.Vector3,System.Single)
extern void LeanTween_delayedSound_mD975E34F6E1D2545468F716845C850F049D1C430 (void);
// 0x00000179 LTDescr LeanTween::move(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTween_move_m255964EF3EF82F6F371AFCAF934CDBE555E31CA4 (void);
// 0x0000017A LTDescr LeanTween::moveX(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_moveX_m968EB608281BDCD184F6B71F22A36B1BD0B32FF5 (void);
// 0x0000017B LTDescr LeanTween::moveY(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_moveY_m8B0C0BD32AECB7A9F1FE4287A1303F8A5BAA225A (void);
// 0x0000017C LTDescr LeanTween::moveZ(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_moveZ_m2E0856F50D20B11039FB477A7F28124713884DC3 (void);
// 0x0000017D LTDescr LeanTween::rotate(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_rotate_m2D1DFCC15465567BB121704213FAA70FD5C336B6 (void);
// 0x0000017E LTDescr LeanTween::rotate(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTween_rotate_m76084F742248BE37F7A6B458BB34CA78DCFC36F5 (void);
// 0x0000017F LTDescr LeanTween::rotateAround(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTween_rotateAround_m3577D6688BDC54193F15C71894E6E55A69006630 (void);
// 0x00000180 LTDescr LeanTween::rotateAroundLocal(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTween_rotateAroundLocal_mD02923DFFD7FAD7DCBF034734D058E7C99EB8D44 (void);
// 0x00000181 LTDescr LeanTween::scale(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTween_scale_mC4B678FE9E438AD23893A27F09E3C007F2EBEABA (void);
// 0x00000182 LTDescr LeanTween::size(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single)
extern void LeanTween_size_m28011A80D7FC887385A015C3F499A3F33C339E08 (void);
// 0x00000183 LTDescr LeanTween::alpha(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_alpha_mC673E74E2397289FEC358CEFC8E691FF25680886 (void);
// 0x00000184 LTDescr LeanTween::color(UnityEngine.RectTransform,UnityEngine.Color,System.Single)
extern void LeanTween_color_m173A290E75DEDA181F8F8BDEDF62C1B12550830B (void);
// 0x00000185 System.Single LeanTween::tweenOnCurve(LTDescr,System.Single)
extern void LeanTween_tweenOnCurve_mCF1C2DBF18A825484F5213591A74D0406108BE78 (void);
// 0x00000186 UnityEngine.Vector3 LeanTween::tweenOnCurveVector(LTDescr,System.Single)
extern void LeanTween_tweenOnCurveVector_m2F3889ED1C5209DCB8DF39AAAE310848E3AEB882 (void);
// 0x00000187 System.Single LeanTween::easeOutQuadOpt(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutQuadOpt_mA9D54EA2DFA885DCA918FF829C7EAFED0567F0C1 (void);
// 0x00000188 System.Single LeanTween::easeInQuadOpt(System.Single,System.Single,System.Single)
extern void LeanTween_easeInQuadOpt_mA07BD22C4B17A4FE3B44BC927D18C6E14036811C (void);
// 0x00000189 System.Single LeanTween::easeInOutQuadOpt(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutQuadOpt_m5CB73FF539B12D06CD85E6DC09AF281E3FEC2D86 (void);
// 0x0000018A UnityEngine.Vector3 LeanTween::easeInOutQuadOpt(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LeanTween_easeInOutQuadOpt_mE09DD740EAEC7F818C4AC10178F11B6587DDAFF0 (void);
// 0x0000018B System.Single LeanTween::linear(System.Single,System.Single,System.Single)
extern void LeanTween_linear_mC13897A14BF6CAD2E64BD36644CF8D9AF8EE2F0C (void);
// 0x0000018C System.Single LeanTween::clerp(System.Single,System.Single,System.Single)
extern void LeanTween_clerp_mF38CC191CF1F93FBA5FA125C1BBD362A8F492574 (void);
// 0x0000018D System.Single LeanTween::spring(System.Single,System.Single,System.Single)
extern void LeanTween_spring_mE01745BEC87C32BDF7EA6A91D0B321BADF5884AB (void);
// 0x0000018E System.Single LeanTween::easeInQuad(System.Single,System.Single,System.Single)
extern void LeanTween_easeInQuad_mFADDBAF1404502B07D54D73A5E3E6988D8FA3D24 (void);
// 0x0000018F System.Single LeanTween::easeOutQuad(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutQuad_mAC0C6A6D659FCDE5636A2AFC5D622B99B61808C7 (void);
// 0x00000190 System.Single LeanTween::easeInOutQuad(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutQuad_m71AA1E657CE443D018036A98715420BF5925E7E6 (void);
// 0x00000191 System.Single LeanTween::easeInOutQuadOpt2(System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutQuadOpt2_m292D5F817A9125E0415C3526F931AA81432A3B09 (void);
// 0x00000192 System.Single LeanTween::easeInCubic(System.Single,System.Single,System.Single)
extern void LeanTween_easeInCubic_m3D273E2955A78F68A0F2A9032C2A67E0BAEB6F87 (void);
// 0x00000193 System.Single LeanTween::easeOutCubic(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutCubic_m2DB944476129350B6D0700C846E3063346F07090 (void);
// 0x00000194 System.Single LeanTween::easeInOutCubic(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutCubic_m74507BCA5B56F0F295BA3C93D744D332B4B5CC53 (void);
// 0x00000195 System.Single LeanTween::easeInQuart(System.Single,System.Single,System.Single)
extern void LeanTween_easeInQuart_m48DE80F5CC0478F91AD6C3F63879747A0C75D1D9 (void);
// 0x00000196 System.Single LeanTween::easeOutQuart(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutQuart_m609DCDE1826DB79CB2B9985CD60D1B239A984772 (void);
// 0x00000197 System.Single LeanTween::easeInOutQuart(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutQuart_m0F5D0D7285A20C6D860CF36F25D5D99D59900B33 (void);
// 0x00000198 System.Single LeanTween::easeInQuint(System.Single,System.Single,System.Single)
extern void LeanTween_easeInQuint_mDB55F438F15637CA989A645BE894AAF9A004B647 (void);
// 0x00000199 System.Single LeanTween::easeOutQuint(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutQuint_mBB803F45CF6EE94AC581337373184BC955468FB2 (void);
// 0x0000019A System.Single LeanTween::easeInOutQuint(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutQuint_m63E5EFF41668EB890D1A86E424189FCEF9F7D4E2 (void);
// 0x0000019B System.Single LeanTween::easeInSine(System.Single,System.Single,System.Single)
extern void LeanTween_easeInSine_m04E033CDA7DEDAD91402A67441771684E9DF9585 (void);
// 0x0000019C System.Single LeanTween::easeOutSine(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutSine_mAD08688F562F7B020B432B9F176BFC4E7246E9CA (void);
// 0x0000019D System.Single LeanTween::easeInOutSine(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutSine_m19A665CD11C2430CE4D6B2EE7E49D5E4D69B255B (void);
// 0x0000019E System.Single LeanTween::easeInExpo(System.Single,System.Single,System.Single)
extern void LeanTween_easeInExpo_m5849B8DA222D18020D5DF8CF7CB21EFF6CAF918A (void);
// 0x0000019F System.Single LeanTween::easeOutExpo(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutExpo_m8047C00ED545EBB9A49539302273CBD72B99D0DB (void);
// 0x000001A0 System.Single LeanTween::easeInOutExpo(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutExpo_mC75B771143EC08818C36CD0F1F7A3E5C94ED27DE (void);
// 0x000001A1 System.Single LeanTween::easeInCirc(System.Single,System.Single,System.Single)
extern void LeanTween_easeInCirc_m1B31C81F16B5E9297D21C701D372DBD548319883 (void);
// 0x000001A2 System.Single LeanTween::easeOutCirc(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutCirc_m68F5077C75EA1933F562B96FA0EAF3A114D87EE6 (void);
// 0x000001A3 System.Single LeanTween::easeInOutCirc(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutCirc_m76125DC5F83F40B9931F8114B87A44962EADC567 (void);
// 0x000001A4 System.Single LeanTween::easeInBounce(System.Single,System.Single,System.Single)
extern void LeanTween_easeInBounce_mE3DF8975A300367183C2C926938E6405B33112A5 (void);
// 0x000001A5 System.Single LeanTween::easeOutBounce(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutBounce_m27698CDAABF99F8EDC7D6F6D6586433A9D12970E (void);
// 0x000001A6 System.Single LeanTween::easeInOutBounce(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutBounce_m9450C71BE4DC36B9E7C7337E89D1BB131E5E0BFF (void);
// 0x000001A7 System.Single LeanTween::easeInBack(System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeInBack_m0D8EFBAB1984C81F9A26AC9936DF26444E8CFDCD (void);
// 0x000001A8 System.Single LeanTween::easeOutBack(System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeOutBack_m10B1A82299EE98AEF7C601D14A4D74F613849520 (void);
// 0x000001A9 System.Single LeanTween::easeInOutBack(System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutBack_mA896015E78E55F1FBB1F29D8AA0359A74AC1E284 (void);
// 0x000001AA System.Single LeanTween::easeInElastic(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeInElastic_m0A27227BD0603697DA973D517017937FF956C483 (void);
// 0x000001AB System.Single LeanTween::easeOutElastic(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeOutElastic_mC63F462458A6CBE208E739E5F92B4D21A16E2ED4 (void);
// 0x000001AC System.Single LeanTween::easeInOutElastic(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutElastic_m62A56706E9907A81A5459B47709F75C1947E6473 (void);
// 0x000001AD LTDescr LeanTween::followDamp(UnityEngine.Transform,UnityEngine.Transform,LeanProp,System.Single,System.Single)
extern void LeanTween_followDamp_m5C9F35D1479612D8A617DA5EF9698BBA34BCFB5B (void);
// 0x000001AE LTDescr LeanTween::followSpring(UnityEngine.Transform,UnityEngine.Transform,LeanProp,System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_followSpring_m0897DA70DDD37F467E99B8E095157FCD30E02519 (void);
// 0x000001AF LTDescr LeanTween::followBounceOut(UnityEngine.Transform,UnityEngine.Transform,LeanProp,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_followBounceOut_m65AA5DB3E225B699DEF3111EF3FC5F33043D5F08 (void);
// 0x000001B0 LTDescr LeanTween::followLinear(UnityEngine.Transform,UnityEngine.Transform,LeanProp,System.Single)
extern void LeanTween_followLinear_mE9D8D28E7216EB21260AEF49DC344EB5075E36E0 (void);
// 0x000001B1 System.Void LeanTween::addListener(System.Int32,System.Action`1<LTEvent>)
extern void LeanTween_addListener_m3F4A9293AEBEC7AB6896D092CCD808317C3DCD97 (void);
// 0x000001B2 System.Void LeanTween::addListener(UnityEngine.GameObject,System.Int32,System.Action`1<LTEvent>)
extern void LeanTween_addListener_mD93323361477B8F8671D3E0C2261A56967E4BBED (void);
// 0x000001B3 System.Boolean LeanTween::removeListener(System.Int32,System.Action`1<LTEvent>)
extern void LeanTween_removeListener_mB3A2221839B0C781D1597674FC5E0674D8C189FA (void);
// 0x000001B4 System.Boolean LeanTween::removeListener(System.Int32)
extern void LeanTween_removeListener_mC6FD437AF58BC94743A0A978FAD970886CEF0CAE (void);
// 0x000001B5 System.Boolean LeanTween::removeListener(UnityEngine.GameObject,System.Int32,System.Action`1<LTEvent>)
extern void LeanTween_removeListener_m2DECCE672B558063CCED79464E5401FFC922DCE4 (void);
// 0x000001B6 System.Void LeanTween::dispatchEvent(System.Int32)
extern void LeanTween_dispatchEvent_m1B94CB1AE76D30A0629498DC3CF920C16C3C5B80 (void);
// 0x000001B7 System.Void LeanTween::dispatchEvent(System.Int32,System.Object)
extern void LeanTween_dispatchEvent_mA17A21F65DC5DFEA2312C5F1446BCD2A13D03C98 (void);
// 0x000001B8 System.Void LeanTween::.ctor()
extern void LeanTween__ctor_m0E530009EEDFEFE39A618092205A560791154327 (void);
// 0x000001B9 System.Void LeanTween::.cctor()
extern void LeanTween__cctor_m7C31F6E8304BED5D2568407206013A918B566DB8 (void);
// 0x000001BA System.Void LeanTween/<>c__DisplayClass193_0::.ctor()
extern void U3CU3Ec__DisplayClass193_0__ctor_m42DC73265951A655C0448FF60C880316D9DE1A43 (void);
// 0x000001BB System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__0()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__0_m191266906DD2277CDC3B46FF109CF35D23BBDAC7 (void);
// 0x000001BC System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__1()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__1_m704758A390EF812C25A3A65988859C02AFBED1C4 (void);
// 0x000001BD System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__2()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__2_mAA06748BE5AB364E8F927A9EE39224B64FD24398 (void);
// 0x000001BE System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__3()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__3_mD56CFC8DF59426863F6189980E747D8413AA5CA0 (void);
// 0x000001BF System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__4()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__4_m6D340E392A6FE3E0389103E0DDB77C9ED361D9ED (void);
// 0x000001C0 System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__5()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__5_m0220EC49C37178D39C26609650CE24AAB28621FE (void);
// 0x000001C1 System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__6()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__6_m2DC42525AE6AA25C9692CA049C80F6958918DB42 (void);
// 0x000001C2 System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__7()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__7_mE7F490B90125F2A8B3C5B91FF517537313A0FCE0 (void);
// 0x000001C3 System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__8()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__8_m007DC11D196C3911171092A96EFF8588C9CBF947 (void);
// 0x000001C4 System.Void LeanTween/<>c__DisplayClass193_0::<followDamp>b__9()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__9_m26FE2854DC255FCB17A99976AF3B1C9674AF3DA5 (void);
// 0x000001C5 System.Void LeanTween/<>c__DisplayClass194_0::.ctor()
extern void U3CU3Ec__DisplayClass194_0__ctor_m7BE723A2115ACC48F87D807B5AD61D097D7B80CA (void);
// 0x000001C6 System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__0()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__0_m96E779A4A9A701F3B635CFB3EBCC96458BD3FDC3 (void);
// 0x000001C7 System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__1()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__1_mACD73E450FC2E3CE551EDDAC93A2F52A45C5054C (void);
// 0x000001C8 System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__2()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__2_m56BDFF78D7668526967A93EA96C4F427DC600683 (void);
// 0x000001C9 System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__3()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__3_m8F19785EA57FC703DDADF403EE7321F94343C995 (void);
// 0x000001CA System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__4()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__4_mD7590F09F10CE901B14B9E7C2D59281C17BF6D45 (void);
// 0x000001CB System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__5()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__5_m0173F460A2260824B38994CCB06B17F86A43BC00 (void);
// 0x000001CC System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__6()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__6_mDF693219BF56597185AC7A86A83B41CCAE50995D (void);
// 0x000001CD System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__7()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__7_m02E8CAD9D7320D317F1291024AEC66A7BA1DF8BC (void);
// 0x000001CE System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__8()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__8_m2815FB0A57623C0A7694BEE29292D36748AF2FEF (void);
// 0x000001CF System.Void LeanTween/<>c__DisplayClass194_0::<followSpring>b__9()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__9_m2249E4B791535178AF9B4005460FC55DC089E8CA (void);
// 0x000001D0 System.Void LeanTween/<>c__DisplayClass195_0::.ctor()
extern void U3CU3Ec__DisplayClass195_0__ctor_mBBB552F5EA7C92143790A2E28ECAAFA48EC7B4D7 (void);
// 0x000001D1 System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__0()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__0_m85BF6FBF1E6DDC848ED2747320A820E6A16F69A9 (void);
// 0x000001D2 System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__1()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__1_m8C32BD1F9A6489144955B3C032D8EFB4884F2ED6 (void);
// 0x000001D3 System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__2()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__2_m3ADBC5765839ADC33E591B0E3F86D3BCEDD966D3 (void);
// 0x000001D4 System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__3()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__3_m7F6DABA8926F943AB02A6C527D916B95331FD3C9 (void);
// 0x000001D5 System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__4()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__4_mB8C14DF64A83794EB497687CD62EB8246F2F3039 (void);
// 0x000001D6 System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__5()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__5_mA48C1D48F29E8F11F15E727577E658F0CAC0DD46 (void);
// 0x000001D7 System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__6()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__6_m6915716A4F822A502028FEABC63C125E057402CD (void);
// 0x000001D8 System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__7()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__7_mDDD6AF7E2634F4081FD3849FF6C18B87A3235250 (void);
// 0x000001D9 System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__8()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__8_m90947880CD99C1E7247F57A1AC77692D6FFBFE69 (void);
// 0x000001DA System.Void LeanTween/<>c__DisplayClass195_0::<followBounceOut>b__9()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__9_mB5FEE58DA881C5915A000A3B5988C3D58B578E29 (void);
// 0x000001DB System.Void LeanTween/<>c__DisplayClass196_0::.ctor()
extern void U3CU3Ec__DisplayClass196_0__ctor_m53A3A91F675B0FEBE70D8A183F4AEE80BB562132 (void);
// 0x000001DC System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__0()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__0_mFA5A42D63EE280D45A87226343CAED5E72B4259F (void);
// 0x000001DD System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__1()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__1_mA72FB34A781A70DE472941BE6213DAD4056A4156 (void);
// 0x000001DE System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__2()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__2_m3FFAD56268BA1B8BA721A870C39B2EB3FED3AFFF (void);
// 0x000001DF System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__3()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__3_m95959A86F6834CCBCE7080772F0C215459296987 (void);
// 0x000001E0 System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__4()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__4_m17D9CC99AA674FE28A2596BAB8D7AF7EA0AC74F5 (void);
// 0x000001E1 System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__5()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__5_m8CAABA8DEEB326B0DFBA45714E4DE40FA69D2FB9 (void);
// 0x000001E2 System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__6()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__6_mDF3FFFFEFDD9AFFCA4883B15B5BA1C2C568B5238 (void);
// 0x000001E3 System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__7()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__7_mF835E156C518889CC37B7B18196CB66817995693 (void);
// 0x000001E4 System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__8()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__8_m20A928A64BB76244A147659349B4952FB2AEF119 (void);
// 0x000001E5 System.Void LeanTween/<>c__DisplayClass196_0::<followLinear>b__9()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__9_m485F0716E3A701BA24C7BE51462D4081218137BC (void);
// 0x000001E6 UnityEngine.Vector3[] LTUtility::reverse(UnityEngine.Vector3[])
extern void LTUtility_reverse_m792C6EFC380420D2FD31BF47B331CBB50B650824 (void);
// 0x000001E7 System.Void LTUtility::.ctor()
extern void LTUtility__ctor_m480117B4F53FC8EF08F8FA640618FDFEB2DA5730 (void);
// 0x000001E8 System.Void LTBezier::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LTBezier__ctor_mDE430564C41DA6E441FF61F9075DA00688964D8D (void);
// 0x000001E9 System.Single LTBezier::map(System.Single)
extern void LTBezier_map_mC8981CF95BB7EE3FACDC51DC93EE0A638EB7FCD4 (void);
// 0x000001EA UnityEngine.Vector3 LTBezier::bezierPoint(System.Single)
extern void LTBezier_bezierPoint_mB8ECD0DB38F58EEFB1FCEFA03F96C4F6A04E6B9B (void);
// 0x000001EB UnityEngine.Vector3 LTBezier::point(System.Single)
extern void LTBezier_point_m11756123BEE5DB1D4A3D97B8920CB6434DE229FC (void);
// 0x000001EC System.Void LTBezierPath::.ctor()
extern void LTBezierPath__ctor_mC14B0C13FB3A48E608456696A8CA14DB6A6B803A (void);
// 0x000001ED System.Void LTBezierPath::.ctor(UnityEngine.Vector3[])
extern void LTBezierPath__ctor_m761CE6701D4AF322CDF34898E4A75EE3996EEF32 (void);
// 0x000001EE System.Void LTBezierPath::setPoints(UnityEngine.Vector3[])
extern void LTBezierPath_setPoints_m0326ECE7DAAF6A7FF84203D7C4DF88207765D100 (void);
// 0x000001EF System.Single LTBezierPath::get_distance()
extern void LTBezierPath_get_distance_mCC5AFBA165F1DE82DD616DCCD6A1DF46D6DC2268 (void);
// 0x000001F0 UnityEngine.Vector3 LTBezierPath::point(System.Single)
extern void LTBezierPath_point_mE9E25CE7B83E1F6A9509A12EB82792D0F2AD08CF (void);
// 0x000001F1 System.Void LTBezierPath::place2d(UnityEngine.Transform,System.Single)
extern void LTBezierPath_place2d_m86914A18D26070FDB3601E3F4B0A9F337C71D57D (void);
// 0x000001F2 System.Void LTBezierPath::placeLocal2d(UnityEngine.Transform,System.Single)
extern void LTBezierPath_placeLocal2d_m3B554AF9CDDCAD494F07CFEAD024F82501474089 (void);
// 0x000001F3 System.Void LTBezierPath::place(UnityEngine.Transform,System.Single)
extern void LTBezierPath_place_m92A56AFF394BB4A2B4997C09DC87CF712D67FB62 (void);
// 0x000001F4 System.Void LTBezierPath::place(UnityEngine.Transform,System.Single,UnityEngine.Vector3)
extern void LTBezierPath_place_m089484993924A7E12BCFE5418BD9DA5AB802E664 (void);
// 0x000001F5 System.Void LTBezierPath::placeLocal(UnityEngine.Transform,System.Single)
extern void LTBezierPath_placeLocal_m84ECB572C31B7DC9420B9E22745AF8E553EA8EBE (void);
// 0x000001F6 System.Void LTBezierPath::placeLocal(UnityEngine.Transform,System.Single,UnityEngine.Vector3)
extern void LTBezierPath_placeLocal_mD55D6F5C237A9A3E3CF585A38697083AD608A6C9 (void);
// 0x000001F7 System.Void LTBezierPath::gizmoDraw(System.Single)
extern void LTBezierPath_gizmoDraw_mF83D68461DAAD27D153D7C368B9A17CE33FFBA2C (void);
// 0x000001F8 System.Single LTBezierPath::ratioAtPoint(UnityEngine.Vector3,System.Single)
extern void LTBezierPath_ratioAtPoint_m143BC2A593074BDC7858B6861351A0D044EC38C3 (void);
// 0x000001F9 System.Void LTSpline::.ctor(UnityEngine.Vector3[])
extern void LTSpline__ctor_m9551907DED1FDB0E2E0530EC1773BEB94364C936 (void);
// 0x000001FA System.Void LTSpline::.ctor(UnityEngine.Vector3[],System.Boolean)
extern void LTSpline__ctor_m2F73C5AFFBC6626390D7B2FA507B202DD52225E2 (void);
// 0x000001FB System.Void LTSpline::init(UnityEngine.Vector3[],System.Boolean)
extern void LTSpline_init_m25191659D43D582146D1C29714BD5B7B01D6B5F8 (void);
// 0x000001FC UnityEngine.Vector3 LTSpline::map(System.Single)
extern void LTSpline_map_mDCB7147639A49699ED42F125EF6A5D27CA9E1485 (void);
// 0x000001FD UnityEngine.Vector3 LTSpline::interp(System.Single)
extern void LTSpline_interp_m75A500E14151D59BC9F7240B4AB73190AAB1E598 (void);
// 0x000001FE System.Single LTSpline::ratioAtPoint(UnityEngine.Vector3)
extern void LTSpline_ratioAtPoint_mC3196D78FBCD206F34E51D528EEB6D9DE5093B76 (void);
// 0x000001FF UnityEngine.Vector3 LTSpline::point(System.Single)
extern void LTSpline_point_m84BC62A0BE5F6F90E7AE7632BF88AD5BB332E75D (void);
// 0x00000200 System.Void LTSpline::place2d(UnityEngine.Transform,System.Single)
extern void LTSpline_place2d_mF79DEE3F7C5F54541047D267A2E88C681E9A0C52 (void);
// 0x00000201 System.Void LTSpline::placeLocal2d(UnityEngine.Transform,System.Single)
extern void LTSpline_placeLocal2d_m6BA2FF841EADD05FA6CFBAC808B8E7D6641D154B (void);
// 0x00000202 System.Void LTSpline::place(UnityEngine.Transform,System.Single)
extern void LTSpline_place_mEC9C1AB39B6FAFC9D8F41CEE63391BCB2A577DD9 (void);
// 0x00000203 System.Void LTSpline::place(UnityEngine.Transform,System.Single,UnityEngine.Vector3)
extern void LTSpline_place_m2F73FDFFE8520633664738EA4D3E769154EC645E (void);
// 0x00000204 System.Void LTSpline::placeLocal(UnityEngine.Transform,System.Single)
extern void LTSpline_placeLocal_m861F5C7F91F4A15BD04D1F2C66E196219AC2B10F (void);
// 0x00000205 System.Void LTSpline::placeLocal(UnityEngine.Transform,System.Single,UnityEngine.Vector3)
extern void LTSpline_placeLocal_m8A2D1C6DA0C7BC6E9B61E6577998C6397000F388 (void);
// 0x00000206 System.Void LTSpline::gizmoDraw(System.Single)
extern void LTSpline_gizmoDraw_mAADED3A63F49D610C035F71DC725D214895189B9 (void);
// 0x00000207 System.Void LTSpline::drawGizmo(UnityEngine.Color)
extern void LTSpline_drawGizmo_mE14FCC2AE443D4F66CB45A64754557476051945A (void);
// 0x00000208 System.Void LTSpline::drawGizmo(UnityEngine.Transform[],UnityEngine.Color)
extern void LTSpline_drawGizmo_mFC2AB1F7539EB9239D2AC471B276CC5A380A6A5C (void);
// 0x00000209 System.Void LTSpline::drawLine(UnityEngine.Transform[],System.Single,UnityEngine.Color)
extern void LTSpline_drawLine_m4CEECA0A218EA8067F860F48FAC4E6CDD27EB7E7 (void);
// 0x0000020A System.Void LTSpline::drawLinesGLLines(UnityEngine.Material,UnityEngine.Color,System.Single)
extern void LTSpline_drawLinesGLLines_m248D051DAF686041049E3994F3F3FCA9434DB823 (void);
// 0x0000020B UnityEngine.Vector3[] LTSpline::generateVectors()
extern void LTSpline_generateVectors_mECF369B072F107D79E9AD06DD4D7BBBF5E5716D3 (void);
// 0x0000020C System.Void LTSpline::.cctor()
extern void LTSpline__cctor_mEB46040AD009E27CB5CF44A9F39148191983681E (void);
// 0x0000020D System.Void LTRect::.ctor()
extern void LTRect__ctor_m99E599C0ECB67B47DEE7F91377ACB404E81FDD9C (void);
// 0x0000020E System.Void LTRect::.ctor(UnityEngine.Rect)
extern void LTRect__ctor_mA9DDB568C78410BF943CEC80A10FCC9FE8E7E0CB (void);
// 0x0000020F System.Void LTRect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void LTRect__ctor_m3894896A04C8DE5CF36FEB67B77A9D51471AA5EE (void);
// 0x00000210 System.Void LTRect::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LTRect__ctor_m3FE04D973AF8662CF993C5C7A99A8190AF660DFA (void);
// 0x00000211 System.Void LTRect::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LTRect__ctor_m1FE9274A3C18C4E81E0E68C91DDC727712CC0005 (void);
// 0x00000212 System.Boolean LTRect::get_hasInitiliazed()
extern void LTRect_get_hasInitiliazed_mB71593427DCCF0404DEB52065AABFF1762158555 (void);
// 0x00000213 System.Int32 LTRect::get_id()
extern void LTRect_get_id_mD85F4AFF11D30E3CFB7F39142F98EB798EBC2950 (void);
// 0x00000214 System.Void LTRect::setId(System.Int32,System.Int32)
extern void LTRect_setId_mFE916182B6874208861CD518BEE9F0C89EF7EF8D (void);
// 0x00000215 System.Void LTRect::reset()
extern void LTRect_reset_m79849D25F50652D6BCE8CA8F3F329EE9511852DA (void);
// 0x00000216 System.Void LTRect::resetForRotation()
extern void LTRect_resetForRotation_mADBFF61280065301AADF66876334779462287788 (void);
// 0x00000217 System.Single LTRect::get_x()
extern void LTRect_get_x_m515C6A230B6B72CB215A27535B967428BD6DC3A7 (void);
// 0x00000218 System.Void LTRect::set_x(System.Single)
extern void LTRect_set_x_m2F416C9557DD5B8F51C00D0710A634CD1B25FEEE (void);
// 0x00000219 System.Single LTRect::get_y()
extern void LTRect_get_y_mA1414A3C465D24CCA90A9C73D6EEC35F7411C957 (void);
// 0x0000021A System.Void LTRect::set_y(System.Single)
extern void LTRect_set_y_m40A2235B9E00614E61213F394430C86962E9CA42 (void);
// 0x0000021B System.Single LTRect::get_width()
extern void LTRect_get_width_m04A3F854E9585909A7948905CFFCC4C3AA726B60 (void);
// 0x0000021C System.Void LTRect::set_width(System.Single)
extern void LTRect_set_width_mC8D40CA7ECCEA4B3E2E3B0C0DFC573CD6A461888 (void);
// 0x0000021D System.Single LTRect::get_height()
extern void LTRect_get_height_mB4F9445DF5CF2F82979B009EEF48A4F22634CE44 (void);
// 0x0000021E System.Void LTRect::set_height(System.Single)
extern void LTRect_set_height_m52D3E7ACF7372560DD7412E524171FBF34B2FBE2 (void);
// 0x0000021F UnityEngine.Rect LTRect::get_rect()
extern void LTRect_get_rect_m775DA6B41DFC9720EDBAA6ECD285058C4BE41505 (void);
// 0x00000220 System.Void LTRect::set_rect(UnityEngine.Rect)
extern void LTRect_set_rect_m09C60F3DDB77909404D570E60060AEDD73BFD969 (void);
// 0x00000221 LTRect LTRect::setStyle(UnityEngine.GUIStyle)
extern void LTRect_setStyle_m5E5B4BC5ACD18F9ACC62171F440460CBB4B549F1 (void);
// 0x00000222 LTRect LTRect::setFontScaleToFit(System.Boolean)
extern void LTRect_setFontScaleToFit_mA9F65B0883725878887560F549C2F4820445CF3B (void);
// 0x00000223 LTRect LTRect::setColor(UnityEngine.Color)
extern void LTRect_setColor_mD5BD0C65451DDD14F2927F5BFA617DB93B9E2CC6 (void);
// 0x00000224 LTRect LTRect::setAlpha(System.Single)
extern void LTRect_setAlpha_m5F785B6E9D8C6DFD7CC751417A86959041F5D845 (void);
// 0x00000225 LTRect LTRect::setLabel(System.String)
extern void LTRect_setLabel_m87C389A819F76DBCEABE8968C2C357F43EEAC6DC (void);
// 0x00000226 LTRect LTRect::setUseSimpleScale(System.Boolean,UnityEngine.Rect)
extern void LTRect_setUseSimpleScale_m9C2401689E18E8B93BC6C9A95E781226E765E81E (void);
// 0x00000227 LTRect LTRect::setUseSimpleScale(System.Boolean)
extern void LTRect_setUseSimpleScale_mF4509500DEBCBD33A6CBFE6F4B7C2BC4D75D845C (void);
// 0x00000228 LTRect LTRect::setSizeByHeight(System.Boolean)
extern void LTRect_setSizeByHeight_m002DF767AC9C9ACCF561B65C2809C32FE07D8759 (void);
// 0x00000229 System.String LTRect::ToString()
extern void LTRect_ToString_m1034234D679BFDD3FAEB6E69E031652F13022AA8 (void);
// 0x0000022A System.Void LTEvent::.ctor(System.Int32,System.Object)
extern void LTEvent__ctor_m6ABB897986AE37C647A526BA35CD577196C80053 (void);
// 0x0000022B System.Void LTGUI::init()
extern void LTGUI_init_m90D0DAAF20EA870C10FC394D34AE654788D6E9E3 (void);
// 0x0000022C System.Void LTGUI::initRectCheck()
extern void LTGUI_initRectCheck_m01927ECCEAA6B39214CAEC2DA11C59C7D7A417DC (void);
// 0x0000022D System.Void LTGUI::reset()
extern void LTGUI_reset_m4AF33FEE1C4FFF585B61E00329512D26D9348405 (void);
// 0x0000022E System.Void LTGUI::update(System.Int32)
extern void LTGUI_update_m346A6915B0DA4CAE2E17FE892D39122992599AE0 (void);
// 0x0000022F System.Boolean LTGUI::checkOnScreen(UnityEngine.Rect)
extern void LTGUI_checkOnScreen_m018B856681A029B796023FFC001610DB7C87497A (void);
// 0x00000230 System.Void LTGUI::destroy(System.Int32)
extern void LTGUI_destroy_mF6818BEF1B1A6510F9F38B20B4D4ACA1E4BAECE4 (void);
// 0x00000231 System.Void LTGUI::destroyAll(System.Int32)
extern void LTGUI_destroyAll_m6C70120370B727440C087BC73ED1F62914431B5E (void);
// 0x00000232 LTRect LTGUI::label(UnityEngine.Rect,System.String,System.Int32)
extern void LTGUI_label_m568FA2530F19BB9D082825B7406336A90CA7BF19 (void);
// 0x00000233 LTRect LTGUI::label(LTRect,System.String,System.Int32)
extern void LTGUI_label_mF0A3E4EA3BF86BFFAFF265B1E848418ADF91B4CA (void);
// 0x00000234 LTRect LTGUI::texture(UnityEngine.Rect,UnityEngine.Texture,System.Int32)
extern void LTGUI_texture_m18FCD865307F279E7E6A0CF29D3E68DA0125F09E (void);
// 0x00000235 LTRect LTGUI::texture(LTRect,UnityEngine.Texture,System.Int32)
extern void LTGUI_texture_mD8320410FA4A576CAC639511DAD9271A8C2D32EA (void);
// 0x00000236 LTRect LTGUI::element(LTRect,System.Int32)
extern void LTGUI_element_mB7414EFDA599B3627466CD84A505842398B3BDAD (void);
// 0x00000237 System.Boolean LTGUI::hasNoOverlap(UnityEngine.Rect,System.Int32)
extern void LTGUI_hasNoOverlap_m8F6CBA73181394C020334257C98D15D73222A21B (void);
// 0x00000238 System.Boolean LTGUI::pressedWithinRect(UnityEngine.Rect)
extern void LTGUI_pressedWithinRect_m6955BEF35822977E4C488F640E982C05624D2C87 (void);
// 0x00000239 System.Boolean LTGUI::checkWithinRect(UnityEngine.Vector2,UnityEngine.Rect)
extern void LTGUI_checkWithinRect_m7BD673AE0509E6E36A939C2A15547FB1B35425DE (void);
// 0x0000023A UnityEngine.Vector2 LTGUI::firstTouch()
extern void LTGUI_firstTouch_m59A232DAB90147A8D67F2636CF126E3868B749A3 (void);
// 0x0000023B System.Void LTGUI::.ctor()
extern void LTGUI__ctor_m3DF0E374662C139E92CE25BD00208671EA425500 (void);
// 0x0000023C System.Void LTGUI::.cctor()
extern void LTGUI__cctor_m4A1C16EF1B32B267816AE59E983E024BDF672F06 (void);
// 0x0000023D LTDescr LeanTweenExt::LeanAlpha(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanAlpha_m622BA3E2E95AA0C3ED769D14693452C71B464202 (void);
// 0x0000023E LTDescr LeanTweenExt::LeanAlphaVertex(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanAlphaVertex_m276A64D30C7DBC2962D952F39164E74266A80805 (void);
// 0x0000023F LTDescr LeanTweenExt::LeanAlpha(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTweenExt_LeanAlpha_m5D4F86AB57DEAB1788098157DD3858155B41A026 (void);
// 0x00000240 LTDescr LeanTweenExt::LeanAlpha(UnityEngine.CanvasGroup,System.Single,System.Single)
extern void LeanTweenExt_LeanAlpha_m80C6BB78F56CBCBAF562EA721D66F568AEB253E5 (void);
// 0x00000241 LTDescr LeanTweenExt::LeanAlphaText(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTweenExt_LeanAlphaText_m2E879D95FD0A499D43742BC7E19C70FC00DB3C2F (void);
// 0x00000242 System.Void LeanTweenExt::LeanCancel(UnityEngine.GameObject)
extern void LeanTweenExt_LeanCancel_mF59992A82C61655FB9C3C02CAA26A659B9DE3646 (void);
// 0x00000243 System.Void LeanTweenExt::LeanCancel(UnityEngine.GameObject,System.Boolean)
extern void LeanTweenExt_LeanCancel_mD017D8DDC025544954D2E68BC260FC83C9469661 (void);
// 0x00000244 System.Void LeanTweenExt::LeanCancel(UnityEngine.GameObject,System.Int32,System.Boolean)
extern void LeanTweenExt_LeanCancel_mC35E34FD1DF38362DA3970677A1A3745F02873E5 (void);
// 0x00000245 System.Void LeanTweenExt::LeanCancel(UnityEngine.RectTransform)
extern void LeanTweenExt_LeanCancel_m17794FCEC25F233AC350724707F017F476BEA6B1 (void);
// 0x00000246 LTDescr LeanTweenExt::LeanColor(UnityEngine.GameObject,UnityEngine.Color,System.Single)
extern void LeanTweenExt_LeanColor_m444318E458A875BB4CB2A0DD294D992CD93EBEFD (void);
// 0x00000247 LTDescr LeanTweenExt::LeanColorText(UnityEngine.RectTransform,UnityEngine.Color,System.Single)
extern void LeanTweenExt_LeanColorText_m400922DC92DE54BE07F329E4949145CF60571731 (void);
// 0x00000248 LTDescr LeanTweenExt::LeanDelayedCall(UnityEngine.GameObject,System.Single,System.Action)
extern void LeanTweenExt_LeanDelayedCall_m2377F6B8E51CB5CDEA727E0ECB5719E78E1E6DC8 (void);
// 0x00000249 LTDescr LeanTweenExt::LeanDelayedCall(UnityEngine.GameObject,System.Single,System.Action`1<System.Object>)
extern void LeanTweenExt_LeanDelayedCall_m813456BD5A6C3BEF150219E1FB7FFECF3CFDB8FB (void);
// 0x0000024A System.Boolean LeanTweenExt::LeanIsPaused(UnityEngine.GameObject)
extern void LeanTweenExt_LeanIsPaused_m004A9E480599A969A094BBF5EAB8298D25F72877 (void);
// 0x0000024B System.Boolean LeanTweenExt::LeanIsPaused(UnityEngine.RectTransform)
extern void LeanTweenExt_LeanIsPaused_mAB912B24C71D148315C8E71232F9B54C2FADB5A4 (void);
// 0x0000024C System.Boolean LeanTweenExt::LeanIsTweening(UnityEngine.GameObject)
extern void LeanTweenExt_LeanIsTweening_mECC74A5F933AEEEC71D8EBE7E977639DDD124F59 (void);
// 0x0000024D LTDescr LeanTweenExt::LeanMove(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanMove_m054FBB59F420198B550A6069D9F9E6ABAE6AB43A (void);
// 0x0000024E LTDescr LeanTweenExt::LeanMove(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanMove_m615AA30F73F0ADF408BCE5B3EB2E7B0107F613DF (void);
// 0x0000024F LTDescr LeanTweenExt::LeanMove(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanMove_mE09C2744CD6F66260CCB2AE85EEC189D1739A95C (void);
// 0x00000250 LTDescr LeanTweenExt::LeanMove(UnityEngine.GameObject,UnityEngine.Vector2,System.Single)
extern void LeanTweenExt_LeanMove_m213A2BEDDDE4818C47346D971A3D99CCE02F1E51 (void);
// 0x00000251 LTDescr LeanTweenExt::LeanMove(UnityEngine.Transform,UnityEngine.Vector2,System.Single)
extern void LeanTweenExt_LeanMove_m0A44978D179428622357E68D3AE31FCD5597B014 (void);
// 0x00000252 LTDescr LeanTweenExt::LeanMove(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMove_m47C512A7C68F548F0C20D61A9EBC21444A89F47F (void);
// 0x00000253 LTDescr LeanTweenExt::LeanMove(UnityEngine.GameObject,LTBezierPath,System.Single)
extern void LeanTweenExt_LeanMove_m28CCB67D4CD2B89B5A3F25E9D70842C949AD0990 (void);
// 0x00000254 LTDescr LeanTweenExt::LeanMove(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTweenExt_LeanMove_mB1E50A4734E3C3CC223FEB98A394DBDBA9614850 (void);
// 0x00000255 LTDescr LeanTweenExt::LeanMove(UnityEngine.Transform,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMove_m0BF63EB3A8D4B614817EA402EB1CC4B5F2C83328 (void);
// 0x00000256 LTDescr LeanTweenExt::LeanMove(UnityEngine.Transform,LTBezierPath,System.Single)
extern void LeanTweenExt_LeanMove_m184D419AE52BA8FFCD237883D4118434DA2F40BB (void);
// 0x00000257 LTDescr LeanTweenExt::LeanMove(UnityEngine.Transform,LTSpline,System.Single)
extern void LeanTweenExt_LeanMove_m31A80E7BD7568CEFF3D7B4073D2BA176A9ADF3DF (void);
// 0x00000258 LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanMoveLocal_m15178FB562F6F85C35B682AF97FCA004BDA1946F (void);
// 0x00000259 LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.GameObject,LTBezierPath,System.Single)
extern void LeanTweenExt_LeanMoveLocal_m91600FA81E8D5DB2D9C69E4F7C5FC09CC40D2DC1 (void);
// 0x0000025A LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTweenExt_LeanMoveLocal_m98D7F1A2052D0F25DEE7B58DE411EE62A8B27CF2 (void);
// 0x0000025B LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanMoveLocal_m920B50D3FBD4DC072118E879AFC151D682AE64B0 (void);
// 0x0000025C LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.Transform,LTBezierPath,System.Single)
extern void LeanTweenExt_LeanMoveLocal_mE4CF4E64AC16B510FB904060DDC2AE8BB231AA51 (void);
// 0x0000025D LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.Transform,LTSpline,System.Single)
extern void LeanTweenExt_LeanMoveLocal_mA49B57CD94C59A10CA8AF9D53F71C48286EAFFD0 (void);
// 0x0000025E LTDescr LeanTweenExt::LeanMoveLocalX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalX_m8847296A8B3401D2D054027ABB659B296FF73760 (void);
// 0x0000025F LTDescr LeanTweenExt::LeanMoveLocalY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalY_m86889CA4B214309EEAC239C3F175937E24310FD6 (void);
// 0x00000260 LTDescr LeanTweenExt::LeanMoveLocalZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalZ_m248064CFFDF7E42AC612D7F72C1E0F2DA37DCFB4 (void);
// 0x00000261 LTDescr LeanTweenExt::LeanMoveLocalX(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalX_mEAB28137FE65A74C1F4963ADCFF044CD5DB7CF69 (void);
// 0x00000262 LTDescr LeanTweenExt::LeanMoveLocalY(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalY_m727F1ED96B0D8FBF32EBB69787F82561886A07FA (void);
// 0x00000263 LTDescr LeanTweenExt::LeanMoveLocalZ(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalZ_m731E4A396200E9C165B47A327CB59CCDB9817707 (void);
// 0x00000264 LTDescr LeanTweenExt::LeanMoveSpline(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMoveSpline_m0E5CBB6704835E27EDA9B25D2915E8D2360E11A4 (void);
// 0x00000265 LTDescr LeanTweenExt::LeanMoveSpline(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTweenExt_LeanMoveSpline_mF640B6FFF8B58004A5C0A4475FE36E2C5BBAC607 (void);
// 0x00000266 LTDescr LeanTweenExt::LeanMoveSpline(UnityEngine.Transform,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMoveSpline_m3EE259E4A0061A1497D195E7910EAE9CD0B984C5 (void);
// 0x00000267 LTDescr LeanTweenExt::LeanMoveSpline(UnityEngine.Transform,LTSpline,System.Single)
extern void LeanTweenExt_LeanMoveSpline_m59EF921EB07DAFBB715DAB0BBCFAC8003E67BD6C (void);
// 0x00000268 LTDescr LeanTweenExt::LeanMoveSplineLocal(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMoveSplineLocal_m667397DE03EFD1DBE0CE5F6CC2287807C3A56761 (void);
// 0x00000269 LTDescr LeanTweenExt::LeanMoveSplineLocal(UnityEngine.Transform,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMoveSplineLocal_mB8706C86995411A7D6408A04AB6B3545922C192D (void);
// 0x0000026A LTDescr LeanTweenExt::LeanMoveX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveX_m95E776B4AFF3B3472159C21ACCA7A074FD6250E1 (void);
// 0x0000026B LTDescr LeanTweenExt::LeanMoveX(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveX_m5EA136BF257503853FF984AB882BADCEDD22F864 (void);
// 0x0000026C LTDescr LeanTweenExt::LeanMoveX(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveX_m2E387E7F079BC81CCBE68B4D705C31E581639352 (void);
// 0x0000026D LTDescr LeanTweenExt::LeanMoveY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveY_mD3811BDA209686DCA906AB52B2CDD29F337FD6F4 (void);
// 0x0000026E LTDescr LeanTweenExt::LeanMoveY(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveY_m65DEFE466DE737FAB4B1DEF0BD8DC8ED22D03433 (void);
// 0x0000026F LTDescr LeanTweenExt::LeanMoveY(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveY_m846E64F6EED274DD02405DF113895FD366BA0FBB (void);
// 0x00000270 LTDescr LeanTweenExt::LeanMoveZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveZ_mC2E89CF6F065D18A1F456935AF7EFA51091F0D9B (void);
// 0x00000271 LTDescr LeanTweenExt::LeanMoveZ(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveZ_m85D8863F40BBF5A99203396C72B421E022FAA417 (void);
// 0x00000272 LTDescr LeanTweenExt::LeanMoveZ(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveZ_mD47791E76E05328EAE363F9A3D0B8DAE6713902A (void);
// 0x00000273 System.Void LeanTweenExt::LeanPause(UnityEngine.GameObject)
extern void LeanTweenExt_LeanPause_m3B5948BE9F86AAB0C837383D547DF2B291B8EA43 (void);
// 0x00000274 LTDescr LeanTweenExt::LeanPlay(UnityEngine.RectTransform,UnityEngine.Sprite[])
extern void LeanTweenExt_LeanPlay_m6DCF6A71031221B880B66660EFF17720C6F1A42D (void);
// 0x00000275 System.Void LeanTweenExt::LeanResume(UnityEngine.GameObject)
extern void LeanTweenExt_LeanResume_m0C9B09411CA1C367B2E2F1E74ECC4860BB400BF4 (void);
// 0x00000276 LTDescr LeanTweenExt::LeanRotate(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanRotate_m9A3EA584BE26EADB024F0E5EC57B0862F3B0BEAE (void);
// 0x00000277 LTDescr LeanTweenExt::LeanRotate(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanRotate_m46EDCCD0F931B4488BF8EDE72F0B1BA6BD65261F (void);
// 0x00000278 LTDescr LeanTweenExt::LeanRotate(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanRotate_mF34054132FFA550762A3C04473FEF3FB54931CA0 (void);
// 0x00000279 LTDescr LeanTweenExt::LeanRotateAround(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAround_m97EA89B0C3FDE7B29B2A5F2FE3BE10F27B0C9993 (void);
// 0x0000027A LTDescr LeanTweenExt::LeanRotateAround(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAround_m9922AD8260E8767A2D33963F2C9BA18ADDF22D61 (void);
// 0x0000027B LTDescr LeanTweenExt::LeanRotateAround(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAround_m7F47B45F2074B30B22D75DC117BCF0A1351710E3 (void);
// 0x0000027C LTDescr LeanTweenExt::LeanRotateAroundLocal(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAroundLocal_m43F940634C80C6D459B9DACF22F06E1E42F29116 (void);
// 0x0000027D LTDescr LeanTweenExt::LeanRotateAroundLocal(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAroundLocal_m81397BB2C883AC5B3D62716FC3C93F43AE4EE5EB (void);
// 0x0000027E LTDescr LeanTweenExt::LeanRotateAroundLocal(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAroundLocal_mE1E669F2BCAAEA47AB4785D6BBFAE413CC74F053 (void);
// 0x0000027F LTDescr LeanTweenExt::LeanRotateX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateX_m0C6142BF35F74AC9A912509713E53E9F1B3F1A20 (void);
// 0x00000280 LTDescr LeanTweenExt::LeanRotateX(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateX_mD09A3A174615A5479F81DFF31192673DADBF3324 (void);
// 0x00000281 LTDescr LeanTweenExt::LeanRotateY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateY_mE334362113CE7DA83AB29EFA1AAB8BFEB8C326E0 (void);
// 0x00000282 LTDescr LeanTweenExt::LeanRotateY(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateY_m823DD2E4B248E09CA4E2CDDFD2D791DF8527FCD2 (void);
// 0x00000283 LTDescr LeanTweenExt::LeanRotateZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateZ_m587B987933373ED9B32F6D319F158899A25AF9C3 (void);
// 0x00000284 LTDescr LeanTweenExt::LeanRotateZ(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateZ_m25AA9563CCFC5754ABE0CDE84FE8E05F09B0357F (void);
// 0x00000285 LTDescr LeanTweenExt::LeanScale(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanScale_m883D32607662D32DAB92DAF865CF110680BF18B3 (void);
// 0x00000286 LTDescr LeanTweenExt::LeanScale(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanScale_m572C2CB624F08B589CA3F7CEDA18D0521D396DAF (void);
// 0x00000287 LTDescr LeanTweenExt::LeanScale(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanScale_mFDA8A475A889665712B39717572167439241C6D5 (void);
// 0x00000288 LTDescr LeanTweenExt::LeanScaleX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleX_m9AC6DED15F097E50134DF4213F86799F3A3F370B (void);
// 0x00000289 LTDescr LeanTweenExt::LeanScaleX(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleX_m469A49E471B4D706CF5859D368CD3F13DD4FBA67 (void);
// 0x0000028A LTDescr LeanTweenExt::LeanScaleY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleY_mEF7B8B2B411B70E312A061227CC9EF7496F1A720 (void);
// 0x0000028B LTDescr LeanTweenExt::LeanScaleY(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleY_mC152F2D04B0CB5770AF0157B8BA217DDC0881D61 (void);
// 0x0000028C LTDescr LeanTweenExt::LeanScaleZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleZ_m23CAE903347B1DFEA59FFFB127419620967227F1 (void);
// 0x0000028D LTDescr LeanTweenExt::LeanScaleZ(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleZ_m2C148ADC5EA94A54CC00F349B1DF331079B13B16 (void);
// 0x0000028E LTDescr LeanTweenExt::LeanSize(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single)
extern void LeanTweenExt_LeanSize_m410361AD3B78FE2A69D65C21570490F82E660241 (void);
// 0x0000028F LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanTweenExt_LeanValue_m1FB1FFD9157FA40B5854744EA15111178AEDE1C1 (void);
// 0x00000290 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Single,System.Single,System.Single)
extern void LeanTweenExt_LeanValue_mE9099E0F64A010AD2BEA7030C5FEFEDBE78A1571 (void);
// 0x00000291 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void LeanTweenExt_LeanValue_mF09B8E5D72CBADA7E7335A1CCE8397B6BEE0B286 (void);
// 0x00000292 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanValue_m165CCF09A13FA398105E06E6B441D0C5BEF33E20 (void);
// 0x00000293 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`1<System.Single>,System.Single,System.Single,System.Single)
extern void LeanTweenExt_LeanValue_m2D0676A4B078FE10C1819ECF5520374CA2E39E8D (void);
// 0x00000294 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`2<System.Single,System.Single>,System.Single,System.Single,System.Single)
extern void LeanTweenExt_LeanValue_m626C6C95ABDA16428DAD1399B0862363589AC9F9 (void);
// 0x00000295 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`2<System.Single,System.Object>,System.Single,System.Single,System.Single)
extern void LeanTweenExt_LeanValue_mBB6A5387301F308B4C42B4DF5B70C17AE29F5533 (void);
// 0x00000296 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`1<UnityEngine.Color>,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanTweenExt_LeanValue_m93C212023658F9EF6A27683DEBAA852EE4670411 (void);
// 0x00000297 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`1<UnityEngine.Vector2>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void LeanTweenExt_LeanValue_m5B60256ABE5F8B7E28F389AAE1696EB656633398 (void);
// 0x00000298 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`1<UnityEngine.Vector3>,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanValue_mBA982D980197033944CF17D7C7F236A6CED885DF (void);
// 0x00000299 System.Void LeanTweenExt::LeanSetPosX(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetPosX_m82201BB5C9781FA41FB69DDA05471D693E59DF2E (void);
// 0x0000029A System.Void LeanTweenExt::LeanSetPosY(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetPosY_mD9DAE6B462055110382822D16B5551854CB68DD7 (void);
// 0x0000029B System.Void LeanTweenExt::LeanSetPosZ(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetPosZ_m1E17DB053DD69876E163757D053A85B172CAC027 (void);
// 0x0000029C System.Void LeanTweenExt::LeanSetLocalPosX(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetLocalPosX_m2325658730387BE391CB8FE674FD3FD2D8D3ECDE (void);
// 0x0000029D System.Void LeanTweenExt::LeanSetLocalPosY(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetLocalPosY_mF14BA20EFA312158AB0FD456EB8762457DB03AE5 (void);
// 0x0000029E System.Void LeanTweenExt::LeanSetLocalPosZ(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetLocalPosZ_m685180D2C04DF7E3417E9EECA9BEBE60F0850790 (void);
// 0x0000029F UnityEngine.Color LeanTweenExt::LeanColor(UnityEngine.Transform)
extern void LeanTweenExt_LeanColor_m2027EB778950A5E71F3DCF398F8A6A77AE32EF35 (void);
// 0x000002A0 UnityEngine.Vector3 LTDescr::get_from()
extern void LTDescr_get_from_m5C46DBE96932E1F21CB71C1174B3294CF8F80E4A (void);
// 0x000002A1 System.Void LTDescr::set_from(UnityEngine.Vector3)
extern void LTDescr_set_from_mCEA64BFED572C9327D1186A8DB46FFB56836E4F5 (void);
// 0x000002A2 UnityEngine.Vector3 LTDescr::get_to()
extern void LTDescr_get_to_m3887E20BF193FF607A48502338A46C90948F5FA5 (void);
// 0x000002A3 System.Void LTDescr::set_to(UnityEngine.Vector3)
extern void LTDescr_set_to_m12DADAD8999E97D8D46F4219632BFAB4FCA41796 (void);
// 0x000002A4 LTDescr/ActionMethodDelegate LTDescr::get_easeInternal()
extern void LTDescr_get_easeInternal_m263F859E9225DF85EFD75C079CD5073AE647B284 (void);
// 0x000002A5 System.Void LTDescr::set_easeInternal(LTDescr/ActionMethodDelegate)
extern void LTDescr_set_easeInternal_m40A3FEDCAA0EE7EB86E804641367E50E34C0CDB9 (void);
// 0x000002A6 LTDescr/ActionMethodDelegate LTDescr::get_initInternal()
extern void LTDescr_get_initInternal_m3E9E4486FBFCFFDB8A3A91EB00DBEBA29916C943 (void);
// 0x000002A7 System.Void LTDescr::set_initInternal(LTDescr/ActionMethodDelegate)
extern void LTDescr_set_initInternal_m21E7CD474330836550F6298EF7F0F2902BC3EE60 (void);
// 0x000002A8 UnityEngine.Transform LTDescr::get_toTrans()
extern void LTDescr_get_toTrans_m03763D518E68ED5106B72B7DEBBD29765285CB78 (void);
// 0x000002A9 System.String LTDescr::ToString()
extern void LTDescr_ToString_m8D2047DA16E09587E6709505E2328386ED0BADA7 (void);
// 0x000002AA System.Void LTDescr::.ctor()
extern void LTDescr__ctor_mD2905F6F8358B3AD8C5784EEA14DE3C05769538B (void);
// 0x000002AB LTDescr LTDescr::cancel(UnityEngine.GameObject)
extern void LTDescr_cancel_mC12FD3EE1E42AFCC7AA536A6D344BE3AEA86AE70 (void);
// 0x000002AC System.Int32 LTDescr::get_uniqueId()
extern void LTDescr_get_uniqueId_mCCC4A89D6863332459B9771B4E636F951B2AAD3B (void);
// 0x000002AD System.Int32 LTDescr::get_id()
extern void LTDescr_get_id_m71271377DB5ECF8CF10782531395CCB0B5478E09 (void);
// 0x000002AE LTDescrOptional LTDescr::get_optional()
extern void LTDescr_get_optional_m0475EBA98D33B8F970E159516CC6BBEC331BE807 (void);
// 0x000002AF System.Void LTDescr::set_optional(LTDescrOptional)
extern void LTDescr_set_optional_m17425AF517082967050C04F2C3B3D2D574DDB2B0 (void);
// 0x000002B0 System.Void LTDescr::reset()
extern void LTDescr_reset_mDC3B5AEC9CB77A12D954B5BB283B062A950BC8DE (void);
// 0x000002B1 LTDescr LTDescr::setFollow()
extern void LTDescr_setFollow_m984BCBD0F7B73FB1E6ABAF2C1B4CA1EE506546DC (void);
// 0x000002B2 LTDescr LTDescr::setMoveX()
extern void LTDescr_setMoveX_m76CE7ACEF44A88ED9BC3E747549EE2CFCB2013A2 (void);
// 0x000002B3 LTDescr LTDescr::setMoveY()
extern void LTDescr_setMoveY_m09BC6472FAD45D9AB081F3D63C49C09E1369205D (void);
// 0x000002B4 LTDescr LTDescr::setMoveZ()
extern void LTDescr_setMoveZ_m421A6F5AFA9E4627D93946E2797FE1C4DB561DF4 (void);
// 0x000002B5 LTDescr LTDescr::setMoveLocalX()
extern void LTDescr_setMoveLocalX_m75617B5FB504CAF9FF78274042314A7A20388ED6 (void);
// 0x000002B6 LTDescr LTDescr::setMoveLocalY()
extern void LTDescr_setMoveLocalY_m22DCFF89FDE31EBEB557B9964E55DFC264110243 (void);
// 0x000002B7 LTDescr LTDescr::setMoveLocalZ()
extern void LTDescr_setMoveLocalZ_m5B79B96D94FF6B39A2CFEFF240E3C88D6388F6A2 (void);
// 0x000002B8 System.Void LTDescr::initFromInternal()
extern void LTDescr_initFromInternal_m639FAC099FE64CC165535CCE0719472B44AC7F22 (void);
// 0x000002B9 LTDescr LTDescr::setOffset(UnityEngine.Vector3)
extern void LTDescr_setOffset_mB2DAAF3FEB7717194BBA1C04741D4EF68C8F952D (void);
// 0x000002BA LTDescr LTDescr::setMoveCurved()
extern void LTDescr_setMoveCurved_m0DBB6E2C2BAA1806B5027FFA8DC167215CC9425E (void);
// 0x000002BB LTDescr LTDescr::setMoveCurvedLocal()
extern void LTDescr_setMoveCurvedLocal_m81E717B0084EB49305FF083C0CADA94DCD974319 (void);
// 0x000002BC LTDescr LTDescr::setMoveSpline()
extern void LTDescr_setMoveSpline_m0927D09871DEF890A033AC185FD26BCDBE1911C7 (void);
// 0x000002BD LTDescr LTDescr::setMoveSplineLocal()
extern void LTDescr_setMoveSplineLocal_mC0E62F0B0E9597DAAE5027FAB2F649EAF2E13034 (void);
// 0x000002BE LTDescr LTDescr::setScaleX()
extern void LTDescr_setScaleX_m7CFF1D6BE843E9721DB183EB147279B195953B7B (void);
// 0x000002BF LTDescr LTDescr::setScaleY()
extern void LTDescr_setScaleY_mBC9B04832806ACB660D3E326621711B15CF7F791 (void);
// 0x000002C0 LTDescr LTDescr::setScaleZ()
extern void LTDescr_setScaleZ_m0401AF8B4C25CB3AA2CDCBF4C2DBD75166897C0D (void);
// 0x000002C1 LTDescr LTDescr::setRotateX()
extern void LTDescr_setRotateX_m26F91A22B505D763B444E6523057008EAD6FD7B3 (void);
// 0x000002C2 LTDescr LTDescr::setRotateY()
extern void LTDescr_setRotateY_m52016439FDA153BB871679C34D65946434862E66 (void);
// 0x000002C3 LTDescr LTDescr::setRotateZ()
extern void LTDescr_setRotateZ_mBC6D82441A461E415CB255195BD66FBC4AF132D9 (void);
// 0x000002C4 LTDescr LTDescr::setRotateAround()
extern void LTDescr_setRotateAround_m346F3BDC7A38E146F5EF8FACC1DBA07BD3684918 (void);
// 0x000002C5 LTDescr LTDescr::setRotateAroundLocal()
extern void LTDescr_setRotateAroundLocal_mBE18B57E33C1118A01FBDB41A99DE69746C9C865 (void);
// 0x000002C6 LTDescr LTDescr::setAlpha()
extern void LTDescr_setAlpha_m291AFD44926FB68DD36823B9D142AA276D072962 (void);
// 0x000002C7 LTDescr LTDescr::setTextAlpha()
extern void LTDescr_setTextAlpha_m569C24FD4701EFE34149E4803E189518B383B0B1 (void);
// 0x000002C8 LTDescr LTDescr::setAlphaVertex()
extern void LTDescr_setAlphaVertex_mEF508AADA9444AAB3946ED1C2A3506094A541E4A (void);
// 0x000002C9 LTDescr LTDescr::setColor()
extern void LTDescr_setColor_mA47DB19E9808DD9AAA142AB9CC3D33C4A0EFAB04 (void);
// 0x000002CA LTDescr LTDescr::setCallbackColor()
extern void LTDescr_setCallbackColor_m9EA1B5111A4241C55BC10C0705147A33BA07AC33 (void);
// 0x000002CB LTDescr LTDescr::setTextColor()
extern void LTDescr_setTextColor_m3A20272C52CF7141B376DCFFB58C4BA642DD4A83 (void);
// 0x000002CC LTDescr LTDescr::setCanvasAlpha()
extern void LTDescr_setCanvasAlpha_mD05551595E5C4D79C3242EEEEA99AD01274C0BF1 (void);
// 0x000002CD LTDescr LTDescr::setCanvasGroupAlpha()
extern void LTDescr_setCanvasGroupAlpha_mF556BF3EE226BAC974CC36C638CECD86BD29E2B3 (void);
// 0x000002CE LTDescr LTDescr::setCanvasColor()
extern void LTDescr_setCanvasColor_m52C8833AC9DCC36676E9795E24F31D7955CE17C2 (void);
// 0x000002CF LTDescr LTDescr::setCanvasMoveX()
extern void LTDescr_setCanvasMoveX_m7B0F43CC9D9B78A933E1B5EE3CF4A9944129B11F (void);
// 0x000002D0 LTDescr LTDescr::setCanvasMoveY()
extern void LTDescr_setCanvasMoveY_m5A9AAC03AB65F7B5E14CA663EE591F9FF8FDE5A9 (void);
// 0x000002D1 LTDescr LTDescr::setCanvasMoveZ()
extern void LTDescr_setCanvasMoveZ_mEBDF9362DB395299B0E6C4EF45A8E5DF8FCA5CE0 (void);
// 0x000002D2 System.Void LTDescr::initCanvasRotateAround()
extern void LTDescr_initCanvasRotateAround_mDF5155DE268AB0DB414A892643342B04C3C80430 (void);
// 0x000002D3 LTDescr LTDescr::setCanvasRotateAround()
extern void LTDescr_setCanvasRotateAround_mD875CE01D408277380BA7E1A90855FAB176930E5 (void);
// 0x000002D4 LTDescr LTDescr::setCanvasRotateAroundLocal()
extern void LTDescr_setCanvasRotateAroundLocal_m0FB16DFF151E8479B394929E477812B908E71811 (void);
// 0x000002D5 LTDescr LTDescr::setCanvasPlaySprite()
extern void LTDescr_setCanvasPlaySprite_m5507D16A5CA3D57966B06FE22CF280B3635D272D (void);
// 0x000002D6 LTDescr LTDescr::setCanvasMove()
extern void LTDescr_setCanvasMove_mBA28E1B08C4CDD0493F4AEE9032DB47D8DE96A57 (void);
// 0x000002D7 LTDescr LTDescr::setCanvasScale()
extern void LTDescr_setCanvasScale_m25D2078C0E8BC068AB18C945177B424FD8BDF35D (void);
// 0x000002D8 LTDescr LTDescr::setCanvasSizeDelta()
extern void LTDescr_setCanvasSizeDelta_mD3C9CBB1C8F686F2E253C22DB1A2C841C39D00F3 (void);
// 0x000002D9 System.Void LTDescr::callback()
extern void LTDescr_callback_mCC3CF88649A2DB3AADEA6D010D9E1D6F8F0B50A1 (void);
// 0x000002DA LTDescr LTDescr::setCallback()
extern void LTDescr_setCallback_mD0C03116D293C89C840A0EB7D7276987E5704AE4 (void);
// 0x000002DB LTDescr LTDescr::setValue3()
extern void LTDescr_setValue3_mDCA24E0028F363246355199865E0E9C795919C87 (void);
// 0x000002DC LTDescr LTDescr::setMove()
extern void LTDescr_setMove_m0A39B796C492B5F74EAC7A88B614DABEEC83E31D (void);
// 0x000002DD LTDescr LTDescr::setMoveLocal()
extern void LTDescr_setMoveLocal_m7979A95A8C3E83116630A1972484ACB778E042B0 (void);
// 0x000002DE LTDescr LTDescr::setMoveToTransform()
extern void LTDescr_setMoveToTransform_m44267D9670BA0AA3F282AF4654C65465EBE562CD (void);
// 0x000002DF LTDescr LTDescr::setRotate()
extern void LTDescr_setRotate_m481E57B448F3773EB7E8FEDB4D54061FC454EE79 (void);
// 0x000002E0 LTDescr LTDescr::setRotateLocal()
extern void LTDescr_setRotateLocal_m2C363D0AC041E31D2D2ED4143CFFBA71E6625EB8 (void);
// 0x000002E1 LTDescr LTDescr::setScale()
extern void LTDescr_setScale_m0E480B54D7D9C2B8268023050E9231052B87BCA9 (void);
// 0x000002E2 LTDescr LTDescr::setGUIMove()
extern void LTDescr_setGUIMove_mCEF7E5A5AABE334A23DE8CDBE26EDD4658B06CA7 (void);
// 0x000002E3 LTDescr LTDescr::setGUIMoveMargin()
extern void LTDescr_setGUIMoveMargin_mFA7CF549318E4EA41AEF0A72DE52926C88AE3D28 (void);
// 0x000002E4 LTDescr LTDescr::setGUIScale()
extern void LTDescr_setGUIScale_m428EBDD7206CF4353C456CDD2554ACAD5989457A (void);
// 0x000002E5 LTDescr LTDescr::setGUIAlpha()
extern void LTDescr_setGUIAlpha_m316B0C88A1C8D63CA862FEF28A6AC263D043B4A5 (void);
// 0x000002E6 LTDescr LTDescr::setGUIRotate()
extern void LTDescr_setGUIRotate_m5C0C9A675F084647D909DA88099F8A056CC18DD0 (void);
// 0x000002E7 LTDescr LTDescr::setDelayedSound()
extern void LTDescr_setDelayedSound_m1D0040CD3D5090096914684D47D8076A21A0D732 (void);
// 0x000002E8 LTDescr LTDescr::setTarget(UnityEngine.Transform)
extern void LTDescr_setTarget_m5F34436BE6EF6A6E6804AA39FD773562BF873647 (void);
// 0x000002E9 System.Void LTDescr::init()
extern void LTDescr_init_m9C699F46C58C3BEE7F19E14E4B737FBC34685B60 (void);
// 0x000002EA System.Void LTDescr::initSpeed()
extern void LTDescr_initSpeed_m4F4E1F0B62028C76C78B000403BDDFBABCB30754 (void);
// 0x000002EB LTDescr LTDescr::updateNow()
extern void LTDescr_updateNow_m7A17C67327BF1CA89C94257396A0B64F916FD330 (void);
// 0x000002EC System.Boolean LTDescr::updateInternal()
extern void LTDescr_updateInternal_m6F69EAADDCA9F3FC16D96351A97EB4D0DA935A76 (void);
// 0x000002ED System.Void LTDescr::callOnCompletes()
extern void LTDescr_callOnCompletes_m5F22E85C54CBAEA43EB1E2972B309DE885B6BC72 (void);
// 0x000002EE LTDescr LTDescr::setFromColor(UnityEngine.Color)
extern void LTDescr_setFromColor_m93EB8D7B171E28CC66A866DFE42C4637D17BF9B7 (void);
// 0x000002EF System.Void LTDescr::alphaRecursive(UnityEngine.Transform,System.Single,System.Boolean)
extern void LTDescr_alphaRecursive_m9862620727DE002BA5334449EE51E47FB9C35BDE (void);
// 0x000002F0 System.Void LTDescr::colorRecursive(UnityEngine.Transform,UnityEngine.Color,System.Boolean)
extern void LTDescr_colorRecursive_m66AF77EFA7536C295518504A00B20968E3F773AD (void);
// 0x000002F1 System.Void LTDescr::alphaRecursive(UnityEngine.RectTransform,System.Single,System.Int32)
extern void LTDescr_alphaRecursive_mDE10D7A23D1710400E453C787924FB83F89EFDDD (void);
// 0x000002F2 System.Void LTDescr::alphaRecursiveSprite(UnityEngine.Transform,System.Single)
extern void LTDescr_alphaRecursiveSprite_mD677C77EFB7426B437BF89A60BE8D29B6E3ED014 (void);
// 0x000002F3 System.Void LTDescr::colorRecursiveSprite(UnityEngine.Transform,UnityEngine.Color)
extern void LTDescr_colorRecursiveSprite_m2BFB41154D3FBB7D70FF5F2BBB8C3B0F4FC02699 (void);
// 0x000002F4 System.Void LTDescr::colorRecursive(UnityEngine.RectTransform,UnityEngine.Color)
extern void LTDescr_colorRecursive_mDFBBC244E1A546B872218F43CE4D3E230C07DC35 (void);
// 0x000002F5 System.Void LTDescr::textAlphaChildrenRecursive(UnityEngine.Transform,System.Single,System.Boolean)
extern void LTDescr_textAlphaChildrenRecursive_m30329E963CF1363F6841959DD91EED9FF7EA320B (void);
// 0x000002F6 System.Void LTDescr::textAlphaRecursive(UnityEngine.Transform,System.Single,System.Boolean)
extern void LTDescr_textAlphaRecursive_m12B0431A56E4A23B20528CD399BFA2CE80658E3F (void);
// 0x000002F7 System.Void LTDescr::textColorRecursive(UnityEngine.Transform,UnityEngine.Color)
extern void LTDescr_textColorRecursive_mFA16970EB10C436367105DAEF64A22B7C584CCF0 (void);
// 0x000002F8 UnityEngine.Color LTDescr::tweenColor(LTDescr,System.Single)
extern void LTDescr_tweenColor_m3020E4C1CC76332688E53BF95D72BD1B3CB461A9 (void);
// 0x000002F9 LTDescr LTDescr::pause()
extern void LTDescr_pause_m831C5988C87832DF30D6566086DD174849A8712A (void);
// 0x000002FA LTDescr LTDescr::resume()
extern void LTDescr_resume_mBE4F94BDD8338970F8DA5167345E87C4B58B30F8 (void);
// 0x000002FB LTDescr LTDescr::setAxis(UnityEngine.Vector3)
extern void LTDescr_setAxis_m786C5D48517CA736F5B704D876E4E69234E087E3 (void);
// 0x000002FC LTDescr LTDescr::setDelay(System.Single)
extern void LTDescr_setDelay_m55BD6D8AB740123B2EE42BA1721C7E6E29504110 (void);
// 0x000002FD LTDescr LTDescr::setEase(LeanTweenType)
extern void LTDescr_setEase_mDE953D5A1E2D1234C5CFD2F0CDB6F32B787ACD0C (void);
// 0x000002FE LTDescr LTDescr::setEaseLinear()
extern void LTDescr_setEaseLinear_m02CEA15BDCDC76F3FFE4AF8C8C7D4C4BE696A2E0 (void);
// 0x000002FF LTDescr LTDescr::setEaseSpring()
extern void LTDescr_setEaseSpring_mB49258E88824A2EF22CA9CFA751754D59C00646D (void);
// 0x00000300 LTDescr LTDescr::setEaseInQuad()
extern void LTDescr_setEaseInQuad_m1BD7E1556B02807399E80DB203B9553AA766B35E (void);
// 0x00000301 LTDescr LTDescr::setEaseOutQuad()
extern void LTDescr_setEaseOutQuad_m9FD9511C826BB2309FB7ADA82620AC454C207960 (void);
// 0x00000302 LTDescr LTDescr::setEaseInOutQuad()
extern void LTDescr_setEaseInOutQuad_mAD680A08BE87DD7F92202EEEA941DBD71A78B92A (void);
// 0x00000303 LTDescr LTDescr::setEaseInCubic()
extern void LTDescr_setEaseInCubic_mDB7AC25B20EDCDA9371911EFEA0D073548C13681 (void);
// 0x00000304 LTDescr LTDescr::setEaseOutCubic()
extern void LTDescr_setEaseOutCubic_m405E1A83E795CC30CA245DA33133F21D9FCD03A9 (void);
// 0x00000305 LTDescr LTDescr::setEaseInOutCubic()
extern void LTDescr_setEaseInOutCubic_mD69B9E20BD56C26709938CE025470378595D804A (void);
// 0x00000306 LTDescr LTDescr::setEaseInQuart()
extern void LTDescr_setEaseInQuart_m8B52F62F717ED83CB7CD00E869E548FC5C60E0FD (void);
// 0x00000307 LTDescr LTDescr::setEaseOutQuart()
extern void LTDescr_setEaseOutQuart_m926A771E75DCEF5025BA544D0D4814D802658967 (void);
// 0x00000308 LTDescr LTDescr::setEaseInOutQuart()
extern void LTDescr_setEaseInOutQuart_m36299EB57FE5C7B772B07760AD79A4758A407EAB (void);
// 0x00000309 LTDescr LTDescr::setEaseInQuint()
extern void LTDescr_setEaseInQuint_mC37282FD3528ABDE0B9ABBECD0EE4158EB78BB34 (void);
// 0x0000030A LTDescr LTDescr::setEaseOutQuint()
extern void LTDescr_setEaseOutQuint_mB5DF94AAAA9986D805ABD7B8705957F00C9FA298 (void);
// 0x0000030B LTDescr LTDescr::setEaseInOutQuint()
extern void LTDescr_setEaseInOutQuint_mD107E1CB8DA7D291E2507DE864FE3BA7690D3667 (void);
// 0x0000030C LTDescr LTDescr::setEaseInSine()
extern void LTDescr_setEaseInSine_m558572F5D792761C77311F93E823AA522ABFA821 (void);
// 0x0000030D LTDescr LTDescr::setEaseOutSine()
extern void LTDescr_setEaseOutSine_m04078632ABF4A13E48FFC566DADAE650E3531D8B (void);
// 0x0000030E LTDescr LTDescr::setEaseInOutSine()
extern void LTDescr_setEaseInOutSine_mCBF1B47223AB0E40EC462E99311B5BAC92CAF0F4 (void);
// 0x0000030F LTDescr LTDescr::setEaseInExpo()
extern void LTDescr_setEaseInExpo_m031D49646619C11E4CD9124E33ACDE7455963D61 (void);
// 0x00000310 LTDescr LTDescr::setEaseOutExpo()
extern void LTDescr_setEaseOutExpo_mD8C3A31B2E8E473F34744C5BD4855A28385777F5 (void);
// 0x00000311 LTDescr LTDescr::setEaseInOutExpo()
extern void LTDescr_setEaseInOutExpo_mC6247CF20D6C40011D6A31C5AFB4E93C2E27D83B (void);
// 0x00000312 LTDescr LTDescr::setEaseInCirc()
extern void LTDescr_setEaseInCirc_m64F3ED13ADA1CF100ADAD67A01A5C57FC073ECFF (void);
// 0x00000313 LTDescr LTDescr::setEaseOutCirc()
extern void LTDescr_setEaseOutCirc_m3FA5A19F0A8F63DE7F2FA3AE57A1E0DF371451DF (void);
// 0x00000314 LTDescr LTDescr::setEaseInOutCirc()
extern void LTDescr_setEaseInOutCirc_m82DC1F56BD70CAAA7CFC0786FC31565D4B7D1F8C (void);
// 0x00000315 LTDescr LTDescr::setEaseInBounce()
extern void LTDescr_setEaseInBounce_m0A8189AD4174381A97F215E75C5D40485395A549 (void);
// 0x00000316 LTDescr LTDescr::setEaseOutBounce()
extern void LTDescr_setEaseOutBounce_m3E4A3DBF4712B6F566D5FFC5B8AFD96DB06FA374 (void);
// 0x00000317 LTDescr LTDescr::setEaseInOutBounce()
extern void LTDescr_setEaseInOutBounce_mEC4B3DC43EE8829DB40045AE072895AB782B9FDC (void);
// 0x00000318 LTDescr LTDescr::setEaseInBack()
extern void LTDescr_setEaseInBack_m7A4BBFA0DA727FA51678D7B4561F588CFB1454E6 (void);
// 0x00000319 LTDescr LTDescr::setEaseOutBack()
extern void LTDescr_setEaseOutBack_m53F3499FE76CAEA5F462EC96113A265E15438418 (void);
// 0x0000031A LTDescr LTDescr::setEaseInOutBack()
extern void LTDescr_setEaseInOutBack_m1970B0DA7214B401D9894AD2BDC75A96016FDBBE (void);
// 0x0000031B LTDescr LTDescr::setEaseInElastic()
extern void LTDescr_setEaseInElastic_m9D356852FCC3BEB9002EC375047192B257CDE9A6 (void);
// 0x0000031C LTDescr LTDescr::setEaseOutElastic()
extern void LTDescr_setEaseOutElastic_m97DCC808E07D990199DD09AE961EF7CFEB401698 (void);
// 0x0000031D LTDescr LTDescr::setEaseInOutElastic()
extern void LTDescr_setEaseInOutElastic_m1CC9921334D9A64FF1F4C2E5285F3E5712C6D401 (void);
// 0x0000031E LTDescr LTDescr::setEasePunch()
extern void LTDescr_setEasePunch_m1DF3D2CFEE5E2CB6EEAF5F485DF326F8B2A08B66 (void);
// 0x0000031F LTDescr LTDescr::setEaseShake()
extern void LTDescr_setEaseShake_mAC2E54A2A115410297235E85E9567AD5D96CC86E (void);
// 0x00000320 UnityEngine.Vector3 LTDescr::tweenOnCurve()
extern void LTDescr_tweenOnCurve_mAAE98016598CDD3C0E19EC9016CDAAC27290FE40 (void);
// 0x00000321 UnityEngine.Vector3 LTDescr::easeInOutQuad()
extern void LTDescr_easeInOutQuad_mA1B3167A272546E8F4AD51BA4456D911E7A97CAA (void);
// 0x00000322 UnityEngine.Vector3 LTDescr::easeInQuad()
extern void LTDescr_easeInQuad_m8B8A3D80C2483095A2E9319E0E560A16A5D45E03 (void);
// 0x00000323 UnityEngine.Vector3 LTDescr::easeOutQuad()
extern void LTDescr_easeOutQuad_mA14EA059114AEE2C71D891DA1D723554387FB0FC (void);
// 0x00000324 UnityEngine.Vector3 LTDescr::easeLinear()
extern void LTDescr_easeLinear_mAFF573D79A9494A37F171FDCFB1A843E4A099BBD (void);
// 0x00000325 UnityEngine.Vector3 LTDescr::easeSpring()
extern void LTDescr_easeSpring_mBCC692E63F1CA607883E42FD5EF44799798CF544 (void);
// 0x00000326 UnityEngine.Vector3 LTDescr::easeInCubic()
extern void LTDescr_easeInCubic_mA1F3409DFC4FAB01F0E1D7D5C13AF3EF533A592C (void);
// 0x00000327 UnityEngine.Vector3 LTDescr::easeOutCubic()
extern void LTDescr_easeOutCubic_mD3D32B7F8A774C06F28C78EFD8B35F82ABE0B2BD (void);
// 0x00000328 UnityEngine.Vector3 LTDescr::easeInOutCubic()
extern void LTDescr_easeInOutCubic_m10079E2EE4532DD46450DE324ACA7091D4082449 (void);
// 0x00000329 UnityEngine.Vector3 LTDescr::easeInQuart()
extern void LTDescr_easeInQuart_m7F3F07BAF2296FE3EF14D9315A45B101B4A88C48 (void);
// 0x0000032A UnityEngine.Vector3 LTDescr::easeOutQuart()
extern void LTDescr_easeOutQuart_m19C687BA558F3962E3D94A9224DFB21AEB8DB6B4 (void);
// 0x0000032B UnityEngine.Vector3 LTDescr::easeInOutQuart()
extern void LTDescr_easeInOutQuart_m4A83DAC8CFD32AC6A3F5BE9847FAB5B397567EA0 (void);
// 0x0000032C UnityEngine.Vector3 LTDescr::easeInQuint()
extern void LTDescr_easeInQuint_m8DA64234B559AEBF6F300EF6FB4A168247F54DD3 (void);
// 0x0000032D UnityEngine.Vector3 LTDescr::easeOutQuint()
extern void LTDescr_easeOutQuint_m72712C773DCFA24B83AF2013806A795B3874B4E2 (void);
// 0x0000032E UnityEngine.Vector3 LTDescr::easeInOutQuint()
extern void LTDescr_easeInOutQuint_m860D58335F979A03E4BDA78BA9D37BF3F9CAE3C0 (void);
// 0x0000032F UnityEngine.Vector3 LTDescr::easeInSine()
extern void LTDescr_easeInSine_mCF9996B096BEB3C47C523DB7DC3DDE5EEEB362CF (void);
// 0x00000330 UnityEngine.Vector3 LTDescr::easeOutSine()
extern void LTDescr_easeOutSine_m56F54AD9F205357269FB50FC3DB13551BBDE7AA8 (void);
// 0x00000331 UnityEngine.Vector3 LTDescr::easeInOutSine()
extern void LTDescr_easeInOutSine_m4ACFEF2C6BDF120DAA6C76E563B6AE6CECD5380F (void);
// 0x00000332 UnityEngine.Vector3 LTDescr::easeInExpo()
extern void LTDescr_easeInExpo_m07C8C2CF2B118A52CBDC361310B8D8C3DF366A52 (void);
// 0x00000333 UnityEngine.Vector3 LTDescr::easeOutExpo()
extern void LTDescr_easeOutExpo_mA866024A98FD2A2CC8808917B983BE6D3BF4156A (void);
// 0x00000334 UnityEngine.Vector3 LTDescr::easeInOutExpo()
extern void LTDescr_easeInOutExpo_mBEC5D63CA1D9C4E0F6EB2C0935E6D2550E8FBBC9 (void);
// 0x00000335 UnityEngine.Vector3 LTDescr::easeInCirc()
extern void LTDescr_easeInCirc_m5DB7FCD1C107646BCBF7BACAE93191864E5A7BDA (void);
// 0x00000336 UnityEngine.Vector3 LTDescr::easeOutCirc()
extern void LTDescr_easeOutCirc_mEA8558C7773468D1860A2BDC636FA5EC2C8791D8 (void);
// 0x00000337 UnityEngine.Vector3 LTDescr::easeInOutCirc()
extern void LTDescr_easeInOutCirc_m49B59C6FB90730D195CFE5910C1A1596F18385C3 (void);
// 0x00000338 UnityEngine.Vector3 LTDescr::easeInBounce()
extern void LTDescr_easeInBounce_mA53E96538907F54DF3E668026D7F62D8A346B325 (void);
// 0x00000339 UnityEngine.Vector3 LTDescr::easeOutBounce()
extern void LTDescr_easeOutBounce_mB2732EE22499FB0ED83AFEF85FD4896E629A19C8 (void);
// 0x0000033A UnityEngine.Vector3 LTDescr::easeInOutBounce()
extern void LTDescr_easeInOutBounce_m5F2B5E4F09C66FA720583B13EE7DEBF46E4B4250 (void);
// 0x0000033B UnityEngine.Vector3 LTDescr::easeInBack()
extern void LTDescr_easeInBack_m1AD66A3F3E0513ACF1D06C01909DB989D263ACFC (void);
// 0x0000033C UnityEngine.Vector3 LTDescr::easeOutBack()
extern void LTDescr_easeOutBack_m971AF92F07A84D5E01B10F1CED6F43F02217CA0B (void);
// 0x0000033D UnityEngine.Vector3 LTDescr::easeInOutBack()
extern void LTDescr_easeInOutBack_m9AEAAB6088AA50FAE1F839D39D70B79250467DC1 (void);
// 0x0000033E UnityEngine.Vector3 LTDescr::easeInElastic()
extern void LTDescr_easeInElastic_mA980A8C764DFBC3A2DCE4CC566A3AE5D8DBB4F15 (void);
// 0x0000033F UnityEngine.Vector3 LTDescr::easeOutElastic()
extern void LTDescr_easeOutElastic_m4E6536A1BEA97546DDB61146FD6A78BBF31E1207 (void);
// 0x00000340 UnityEngine.Vector3 LTDescr::easeInOutElastic()
extern void LTDescr_easeInOutElastic_mA4E464B18D106514E4092266A28D8EA4138858CA (void);
// 0x00000341 LTDescr LTDescr::setOvershoot(System.Single)
extern void LTDescr_setOvershoot_m47B052637792C78A0D5195306CCA86CB9E7A96A8 (void);
// 0x00000342 LTDescr LTDescr::setPeriod(System.Single)
extern void LTDescr_setPeriod_mC03D996C9712715E08646FDBD5421719CFB8F3DE (void);
// 0x00000343 LTDescr LTDescr::setScale(System.Single)
extern void LTDescr_setScale_m20ABE114A3769DF22F968A3BAE3B969D7405C84D (void);
// 0x00000344 LTDescr LTDescr::setEase(UnityEngine.AnimationCurve)
extern void LTDescr_setEase_m85FF6788C6DD67CB57F667E80E456C7CE86C9E3F (void);
// 0x00000345 LTDescr LTDescr::setTo(UnityEngine.Vector3)
extern void LTDescr_setTo_mC614978FF31785D8135380F7BF1FEF8ADD2F365D (void);
// 0x00000346 LTDescr LTDescr::setTo(UnityEngine.Transform)
extern void LTDescr_setTo_mFE5077089EAAEED3A0B4E00827BF4867FA5E4E76 (void);
// 0x00000347 LTDescr LTDescr::setFrom(UnityEngine.Vector3)
extern void LTDescr_setFrom_mBFD923D83E18FB53AAFB9293C1F13175DCE15D34 (void);
// 0x00000348 LTDescr LTDescr::setFrom(System.Single)
extern void LTDescr_setFrom_m82D22FCE616A6826A3007BCF148DA437F16BC0F8 (void);
// 0x00000349 LTDescr LTDescr::setDiff(UnityEngine.Vector3)
extern void LTDescr_setDiff_mAF9E8982F70181B987022FA76170A8DD7C61FA5E (void);
// 0x0000034A LTDescr LTDescr::setHasInitialized(System.Boolean)
extern void LTDescr_setHasInitialized_mC54A9AAD341DE33BC04734061E6EFE42E89402F6 (void);
// 0x0000034B LTDescr LTDescr::setId(System.UInt32,System.UInt32)
extern void LTDescr_setId_mB492FBFB9D2FE6ED7CDD7EEA9F3A547A4BC596D0 (void);
// 0x0000034C LTDescr LTDescr::setPassed(System.Single)
extern void LTDescr_setPassed_mC3016CADF8995A54ABF2D5D95908EE63D3298E65 (void);
// 0x0000034D LTDescr LTDescr::setTime(System.Single)
extern void LTDescr_setTime_m0DE4EDC9DA5F9D6B5255BCA73A198E18F0A320A4 (void);
// 0x0000034E LTDescr LTDescr::setSpeed(System.Single)
extern void LTDescr_setSpeed_mF8F0031D23893E09F0E258367FEA33FB175CC269 (void);
// 0x0000034F LTDescr LTDescr::setRepeat(System.Int32)
extern void LTDescr_setRepeat_m7D66B000EF60DCC943D848C1BF53BD7C7BEF8313 (void);
// 0x00000350 LTDescr LTDescr::setLoopType(LeanTweenType)
extern void LTDescr_setLoopType_mF2FC7F646DDB9AD46FE9FB3972C4B2EAA5634C77 (void);
// 0x00000351 LTDescr LTDescr::setUseEstimatedTime(System.Boolean)
extern void LTDescr_setUseEstimatedTime_m1882A739CA3F9CE2F6215C61E92D4D5108F352EE (void);
// 0x00000352 LTDescr LTDescr::setIgnoreTimeScale(System.Boolean)
extern void LTDescr_setIgnoreTimeScale_mDD2DF875144DFBA017697139ECD83164182DD6C9 (void);
// 0x00000353 LTDescr LTDescr::setUseFrames(System.Boolean)
extern void LTDescr_setUseFrames_m356FC10CEEBD2F74F93F14EDB27B58A87C67901E (void);
// 0x00000354 LTDescr LTDescr::setUseManualTime(System.Boolean)
extern void LTDescr_setUseManualTime_mAC4545BA99BA9C7117151D155C4664F6D7E933D2 (void);
// 0x00000355 LTDescr LTDescr::setLoopCount(System.Int32)
extern void LTDescr_setLoopCount_m658F924D41197D6DDDEAC18C37849D2341C1C993 (void);
// 0x00000356 LTDescr LTDescr::setLoopOnce()
extern void LTDescr_setLoopOnce_mBF4016C6E09D73B6C4210585FEC20D250BBEBE60 (void);
// 0x00000357 LTDescr LTDescr::setLoopClamp()
extern void LTDescr_setLoopClamp_m596650592727CC3F3C5513F54432D93CEA45666E (void);
// 0x00000358 LTDescr LTDescr::setLoopClamp(System.Int32)
extern void LTDescr_setLoopClamp_mEA5F7A4F29DFEC798D1DBF7CACC4EA95E5681733 (void);
// 0x00000359 LTDescr LTDescr::setLoopPingPong()
extern void LTDescr_setLoopPingPong_mB14D4AD3878CB744ADBB3F0B933E1C50CACEDC18 (void);
// 0x0000035A LTDescr LTDescr::setLoopPingPong(System.Int32)
extern void LTDescr_setLoopPingPong_mDBEA094A372AACBB02EFFA679DBF2C551928140F (void);
// 0x0000035B LTDescr LTDescr::setOnComplete(System.Action)
extern void LTDescr_setOnComplete_mBD0B6BAC2B05C7AE12E93F478BC6F0F41A33C44F (void);
// 0x0000035C LTDescr LTDescr::setOnComplete(System.Action`1<System.Object>)
extern void LTDescr_setOnComplete_m61B62A9BB1DE6DC69EB97F111139AB379A2AB33C (void);
// 0x0000035D LTDescr LTDescr::setOnComplete(System.Action`1<System.Object>,System.Object)
extern void LTDescr_setOnComplete_mD8043AC13DAB80A85715B3B5D2E273754A93157D (void);
// 0x0000035E LTDescr LTDescr::setOnCompleteParam(System.Object)
extern void LTDescr_setOnCompleteParam_m0DF2C3997947B6F72DFD4AA1BAEC841B8B84DD63 (void);
// 0x0000035F LTDescr LTDescr::setOnUpdate(System.Action`1<System.Single>)
extern void LTDescr_setOnUpdate_m55FBE2275AB200DCFB3793FA745A238491E7072B (void);
// 0x00000360 LTDescr LTDescr::setOnUpdateRatio(System.Action`2<System.Single,System.Single>)
extern void LTDescr_setOnUpdateRatio_m52F3EB3754460E5DA2EC48CDD69FAEF7953CC1EB (void);
// 0x00000361 LTDescr LTDescr::setOnUpdateObject(System.Action`2<System.Single,System.Object>)
extern void LTDescr_setOnUpdateObject_m5B410690D035323DF02535DBFDD06EFE602E4F45 (void);
// 0x00000362 LTDescr LTDescr::setOnUpdateVector2(System.Action`1<UnityEngine.Vector2>)
extern void LTDescr_setOnUpdateVector2_m0FB697AF241FBDB5958D2FA52C5BF964FDC9F3A8 (void);
// 0x00000363 LTDescr LTDescr::setOnUpdateVector3(System.Action`1<UnityEngine.Vector3>)
extern void LTDescr_setOnUpdateVector3_mFFF939DCA282CC77307788DDE78DAC3FE8801920 (void);
// 0x00000364 LTDescr LTDescr::setOnUpdateColor(System.Action`1<UnityEngine.Color>)
extern void LTDescr_setOnUpdateColor_m162951739730174C9B12F55091940A39981C1BF7 (void);
// 0x00000365 LTDescr LTDescr::setOnUpdateColor(System.Action`2<UnityEngine.Color,System.Object>)
extern void LTDescr_setOnUpdateColor_m3CE8B6B35EA308221FF7739A6DDFFE8B308DAAAB (void);
// 0x00000366 LTDescr LTDescr::setOnUpdate(System.Action`1<UnityEngine.Color>)
extern void LTDescr_setOnUpdate_mB7154EAF06F1366C24E3E8EABFDF53169A1EC1B5 (void);
// 0x00000367 LTDescr LTDescr::setOnUpdate(System.Action`2<UnityEngine.Color,System.Object>)
extern void LTDescr_setOnUpdate_m487CA9638173DAF55C9DF3B81F7D97232222D88B (void);
// 0x00000368 LTDescr LTDescr::setOnUpdate(System.Action`2<System.Single,System.Object>,System.Object)
extern void LTDescr_setOnUpdate_m8A27C956F125846A8713234EDB95FCA045C3BF50 (void);
// 0x00000369 LTDescr LTDescr::setOnUpdate(System.Action`2<UnityEngine.Vector3,System.Object>,System.Object)
extern void LTDescr_setOnUpdate_mF6CF9481ADCA4D3270B5E21634B8D924D1FD7DD2 (void);
// 0x0000036A LTDescr LTDescr::setOnUpdate(System.Action`1<UnityEngine.Vector2>,System.Object)
extern void LTDescr_setOnUpdate_m5404E20426F6927BD37BD1EA3B0BBBF1642CE37F (void);
// 0x0000036B LTDescr LTDescr::setOnUpdate(System.Action`1<UnityEngine.Vector3>,System.Object)
extern void LTDescr_setOnUpdate_m5356685ADFD38D45881211740E990617C0F2A80D (void);
// 0x0000036C LTDescr LTDescr::setOnUpdateParam(System.Object)
extern void LTDescr_setOnUpdateParam_mE61939C5A8C86D2603FD2F9ED73ACF3CF1FB1D69 (void);
// 0x0000036D LTDescr LTDescr::setOrientToPath(System.Boolean)
extern void LTDescr_setOrientToPath_mECE38AB1BC1313F39FD2FE3D582AB18A849826EE (void);
// 0x0000036E LTDescr LTDescr::setOrientToPath2d(System.Boolean)
extern void LTDescr_setOrientToPath2d_m08D9F50871013C10D1961AFF0A7759A8D92B1294 (void);
// 0x0000036F LTDescr LTDescr::setRect(LTRect)
extern void LTDescr_setRect_m1E626946F828ADB3D7E7A70D4E43B1CA7B2009F3 (void);
// 0x00000370 LTDescr LTDescr::setRect(UnityEngine.Rect)
extern void LTDescr_setRect_m98165DCDF51B68BCB9CAEC7EAC5C364D2E26758A (void);
// 0x00000371 LTDescr LTDescr::setPath(LTBezierPath)
extern void LTDescr_setPath_m9BFB77CBAE51ED839078FD49C7611ED8323AC154 (void);
// 0x00000372 LTDescr LTDescr::setPoint(UnityEngine.Vector3)
extern void LTDescr_setPoint_mB0744AAA4A452543F68A3126C05CD8487BEF7F6E (void);
// 0x00000373 LTDescr LTDescr::setDestroyOnComplete(System.Boolean)
extern void LTDescr_setDestroyOnComplete_m697253D565C9D2CDA484A82E1005660595CEDA06 (void);
// 0x00000374 LTDescr LTDescr::setAudio(System.Object)
extern void LTDescr_setAudio_m86759A48D1B7D26395A5B28AB5450E84AE46AECE (void);
// 0x00000375 LTDescr LTDescr::setOnCompleteOnRepeat(System.Boolean)
extern void LTDescr_setOnCompleteOnRepeat_m28F4EE66B0CA50AC6FA5A9391EEAD0B62E85A3C1 (void);
// 0x00000376 LTDescr LTDescr::setOnCompleteOnStart(System.Boolean)
extern void LTDescr_setOnCompleteOnStart_m2FB448E21905E33DCE1F35EBBD8E39097AC35938 (void);
// 0x00000377 LTDescr LTDescr::setRect(UnityEngine.RectTransform)
extern void LTDescr_setRect_m0CDE3901FF03E6216DD7E6DEC6E65E7DEAF39AB6 (void);
// 0x00000378 LTDescr LTDescr::setSprites(UnityEngine.Sprite[])
extern void LTDescr_setSprites_mC83C58D4DC03CDB0E8529A2DCDE37270089B0A41 (void);
// 0x00000379 LTDescr LTDescr::setFrameRate(System.Single)
extern void LTDescr_setFrameRate_m0A569EDD28DDFD74BD71E0E5CF4730F178220AF9 (void);
// 0x0000037A LTDescr LTDescr::setOnStart(System.Action)
extern void LTDescr_setOnStart_m37383DCD917312D49E2291F3A47F8F9361155A1A (void);
// 0x0000037B LTDescr LTDescr::setDirection(System.Single)
extern void LTDescr_setDirection_m3B76149226B433591EC98766C8B14D63BDB7F193 (void);
// 0x0000037C LTDescr LTDescr::setRecursive(System.Boolean)
extern void LTDescr_setRecursive_mC2895C6EEB59EF79044495FA8BB6ACE914CAB402 (void);
// 0x0000037D System.Void LTDescr::<setMoveX>b__73_0()
extern void LTDescr_U3CsetMoveXU3Eb__73_0_m39FBF744669510650F08E0F2B4E87CDEECA7440A (void);
// 0x0000037E System.Void LTDescr::<setMoveX>b__73_1()
extern void LTDescr_U3CsetMoveXU3Eb__73_1_mB4AD58106C3D205E5D4763F3F1CCA63B2F99EB78 (void);
// 0x0000037F System.Void LTDescr::<setMoveY>b__74_0()
extern void LTDescr_U3CsetMoveYU3Eb__74_0_mA6309FBB2AF9AEEB859024E69AC8ABD6D102AD87 (void);
// 0x00000380 System.Void LTDescr::<setMoveY>b__74_1()
extern void LTDescr_U3CsetMoveYU3Eb__74_1_m7A2A40165003571F84F057D3F39BD77A23B2F0E9 (void);
// 0x00000381 System.Void LTDescr::<setMoveZ>b__75_0()
extern void LTDescr_U3CsetMoveZU3Eb__75_0_mFE473C92996CA9EBA69397DA460F4B16EDE7D03C (void);
// 0x00000382 System.Void LTDescr::<setMoveZ>b__75_1()
extern void LTDescr_U3CsetMoveZU3Eb__75_1_mF78C868A90B38D04ADAD2AD93D536B1791B3C671 (void);
// 0x00000383 System.Void LTDescr::<setMoveLocalX>b__76_0()
extern void LTDescr_U3CsetMoveLocalXU3Eb__76_0_m8A895E545E84F266EA0E0963C68274D9545D17BE (void);
// 0x00000384 System.Void LTDescr::<setMoveLocalX>b__76_1()
extern void LTDescr_U3CsetMoveLocalXU3Eb__76_1_m17F56B25DDE0E4E6D6CC186BF9A9A617957C2FD6 (void);
// 0x00000385 System.Void LTDescr::<setMoveLocalY>b__77_0()
extern void LTDescr_U3CsetMoveLocalYU3Eb__77_0_m8D00D189D0ADCF6C778EFF39DCA1EA55C03E34F4 (void);
// 0x00000386 System.Void LTDescr::<setMoveLocalY>b__77_1()
extern void LTDescr_U3CsetMoveLocalYU3Eb__77_1_m67ADD5EAA303DB53DA4B896F2080F3E6975C76A6 (void);
// 0x00000387 System.Void LTDescr::<setMoveLocalZ>b__78_0()
extern void LTDescr_U3CsetMoveLocalZU3Eb__78_0_mFD9EBD1D9EF08CB93FDCCE06D88DC940852C03DF (void);
// 0x00000388 System.Void LTDescr::<setMoveLocalZ>b__78_1()
extern void LTDescr_U3CsetMoveLocalZU3Eb__78_1_mF3D05D5EA84618939D8F8C0F9F06EE5D6294D77D (void);
// 0x00000389 System.Void LTDescr::<setMoveCurved>b__81_0()
extern void LTDescr_U3CsetMoveCurvedU3Eb__81_0_m8BD948AD31BE03BCEF9D75091AECC3DF8B4A50E3 (void);
// 0x0000038A System.Void LTDescr::<setMoveCurvedLocal>b__82_0()
extern void LTDescr_U3CsetMoveCurvedLocalU3Eb__82_0_mE35CF92421F9DBB5687F1F1608AFBE9F86C80378 (void);
// 0x0000038B System.Void LTDescr::<setMoveSpline>b__83_0()
extern void LTDescr_U3CsetMoveSplineU3Eb__83_0_mD9ACDB850FD978554B5465139A4E1EDD844CA1BB (void);
// 0x0000038C System.Void LTDescr::<setMoveSplineLocal>b__84_0()
extern void LTDescr_U3CsetMoveSplineLocalU3Eb__84_0_m3219BB023C406080046DE37EBCF0CEDB8E323D1A (void);
// 0x0000038D System.Void LTDescr::<setScaleX>b__85_0()
extern void LTDescr_U3CsetScaleXU3Eb__85_0_mDF33F215E49CCD02AE02FDD79827006CB41DF18F (void);
// 0x0000038E System.Void LTDescr::<setScaleX>b__85_1()
extern void LTDescr_U3CsetScaleXU3Eb__85_1_m332A3ADAFDC200BF9AC11279593B5968DC537623 (void);
// 0x0000038F System.Void LTDescr::<setScaleY>b__86_0()
extern void LTDescr_U3CsetScaleYU3Eb__86_0_m6A34E540DCDF955D835FCBD7FEF708257E07FE31 (void);
// 0x00000390 System.Void LTDescr::<setScaleY>b__86_1()
extern void LTDescr_U3CsetScaleYU3Eb__86_1_mE1BCEE0DADE70D6CA3873140143141D9B51DBDB5 (void);
// 0x00000391 System.Void LTDescr::<setScaleZ>b__87_0()
extern void LTDescr_U3CsetScaleZU3Eb__87_0_mAEDB3F2903B250EC9885F64B147EDBDD4881E812 (void);
// 0x00000392 System.Void LTDescr::<setScaleZ>b__87_1()
extern void LTDescr_U3CsetScaleZU3Eb__87_1_mEFAE5A48C329D33CAF5F7003D48DD0B9C581AB85 (void);
// 0x00000393 System.Void LTDescr::<setRotateX>b__88_0()
extern void LTDescr_U3CsetRotateXU3Eb__88_0_mD0976C92A94804F53718DEE26B131989A067EF7A (void);
// 0x00000394 System.Void LTDescr::<setRotateX>b__88_1()
extern void LTDescr_U3CsetRotateXU3Eb__88_1_m7C171EAF890568C630E3BE1EC70C97A0C55F0984 (void);
// 0x00000395 System.Void LTDescr::<setRotateY>b__89_0()
extern void LTDescr_U3CsetRotateYU3Eb__89_0_m1B2E4D04C35A72D3957FEB294B306BF741228F1B (void);
// 0x00000396 System.Void LTDescr::<setRotateY>b__89_1()
extern void LTDescr_U3CsetRotateYU3Eb__89_1_m67AC1C0BCE4902B58FA999FC9475BF42D391F167 (void);
// 0x00000397 System.Void LTDescr::<setRotateZ>b__90_0()
extern void LTDescr_U3CsetRotateZU3Eb__90_0_m92CFF50F82E8606EA58D1FF72D2FBD7C9A0C7290 (void);
// 0x00000398 System.Void LTDescr::<setRotateZ>b__90_1()
extern void LTDescr_U3CsetRotateZU3Eb__90_1_mA55CC8D92F2601E04F21F45337134FBE437A20B6 (void);
// 0x00000399 System.Void LTDescr::<setRotateAround>b__91_0()
extern void LTDescr_U3CsetRotateAroundU3Eb__91_0_m90372B54B5D631FC5E1FF67FCE64F69A1E0E7E63 (void);
// 0x0000039A System.Void LTDescr::<setRotateAround>b__91_1()
extern void LTDescr_U3CsetRotateAroundU3Eb__91_1_m21A791E3630653D459153347755624070DC38965 (void);
// 0x0000039B System.Void LTDescr::<setRotateAroundLocal>b__92_0()
extern void LTDescr_U3CsetRotateAroundLocalU3Eb__92_0_m49A28FAA34B36D7AA09A322D63974997CB31BF43 (void);
// 0x0000039C System.Void LTDescr::<setRotateAroundLocal>b__92_1()
extern void LTDescr_U3CsetRotateAroundLocalU3Eb__92_1_mCB4C19C5A763BBF640E5BE6785D5AB03859AD606 (void);
// 0x0000039D System.Void LTDescr::<setAlpha>b__93_0()
extern void LTDescr_U3CsetAlphaU3Eb__93_0_mADB8DE8A613762F896CC7754539BFFCA3A23B4E9 (void);
// 0x0000039E System.Void LTDescr::<setAlpha>b__93_2()
extern void LTDescr_U3CsetAlphaU3Eb__93_2_m5318309F91340E1C143196D9ADA659C53B3C918C (void);
// 0x0000039F System.Void LTDescr::<setAlpha>b__93_1()
extern void LTDescr_U3CsetAlphaU3Eb__93_1_m44E2198D69A4F502CE6F5B1532AFD8F600D84717 (void);
// 0x000003A0 System.Void LTDescr::<setTextAlpha>b__94_0()
extern void LTDescr_U3CsetTextAlphaU3Eb__94_0_mE551CB2CBB6114CA72656A033CAC42EAB2EFCC8B (void);
// 0x000003A1 System.Void LTDescr::<setTextAlpha>b__94_1()
extern void LTDescr_U3CsetTextAlphaU3Eb__94_1_m809E895E8E9ED4049E44ACAEEFE222EBA2E6A259 (void);
// 0x000003A2 System.Void LTDescr::<setAlphaVertex>b__95_0()
extern void LTDescr_U3CsetAlphaVertexU3Eb__95_0_m53AB148A8D470C29DACB58274436B9535F5CAC37 (void);
// 0x000003A3 System.Void LTDescr::<setAlphaVertex>b__95_1()
extern void LTDescr_U3CsetAlphaVertexU3Eb__95_1_m197F71CB84DF1265DDDEC92AB60CFAEC2B2E8BE9 (void);
// 0x000003A4 System.Void LTDescr::<setColor>b__96_0()
extern void LTDescr_U3CsetColorU3Eb__96_0_mF63A06E57C3B0A1B5B8FE3E7EF131BD862457B19 (void);
// 0x000003A5 System.Void LTDescr::<setColor>b__96_1()
extern void LTDescr_U3CsetColorU3Eb__96_1_m8EA4C07F32943F895BE0CB6909C63263827CEBF8 (void);
// 0x000003A6 System.Void LTDescr::<setCallbackColor>b__97_0()
extern void LTDescr_U3CsetCallbackColorU3Eb__97_0_mAD825635FB3FD1DE688BD692F394C91BD701659F (void);
// 0x000003A7 System.Void LTDescr::<setCallbackColor>b__97_1()
extern void LTDescr_U3CsetCallbackColorU3Eb__97_1_m3635B1AEC7852BD361B57DAC85568D84BB050C38 (void);
// 0x000003A8 System.Void LTDescr::<setTextColor>b__98_0()
extern void LTDescr_U3CsetTextColorU3Eb__98_0_m1AC4C496FB90FA9C994A7D8772EE75A96D8C7499 (void);
// 0x000003A9 System.Void LTDescr::<setTextColor>b__98_1()
extern void LTDescr_U3CsetTextColorU3Eb__98_1_mF8C5941A01CC6AB44A3C1A9E4E0D42DE0019B1B5 (void);
// 0x000003AA System.Void LTDescr::<setCanvasAlpha>b__99_0()
extern void LTDescr_U3CsetCanvasAlphaU3Eb__99_0_m03D22099CB93845FEE24F439C52C194CF424307B (void);
// 0x000003AB System.Void LTDescr::<setCanvasAlpha>b__99_1()
extern void LTDescr_U3CsetCanvasAlphaU3Eb__99_1_mC255CF9153C3E078BCC1E35C16E786C8662EBFD8 (void);
// 0x000003AC System.Void LTDescr::<setCanvasGroupAlpha>b__100_0()
extern void LTDescr_U3CsetCanvasGroupAlphaU3Eb__100_0_mCD1535F45FF98C078658D56A058E4D44D7CD47F2 (void);
// 0x000003AD System.Void LTDescr::<setCanvasGroupAlpha>b__100_1()
extern void LTDescr_U3CsetCanvasGroupAlphaU3Eb__100_1_m6EBBBBA42BCC9B435DB26932FCCD7DF904E3D31E (void);
// 0x000003AE System.Void LTDescr::<setCanvasColor>b__101_0()
extern void LTDescr_U3CsetCanvasColorU3Eb__101_0_mC7AB0F758514D92910F073FB38A41C395E6764E3 (void);
// 0x000003AF System.Void LTDescr::<setCanvasColor>b__101_1()
extern void LTDescr_U3CsetCanvasColorU3Eb__101_1_m0DC35761F636C62A4B9A9D9B7C8EEFD9B30F3CA4 (void);
// 0x000003B0 System.Void LTDescr::<setCanvasMoveX>b__102_0()
extern void LTDescr_U3CsetCanvasMoveXU3Eb__102_0_m0E72A6C3F16FC694B6D54907490ECA5D6D359776 (void);
// 0x000003B1 System.Void LTDescr::<setCanvasMoveX>b__102_1()
extern void LTDescr_U3CsetCanvasMoveXU3Eb__102_1_m7FF531D0062E0803860A0CD456E6DD6AE30BD00D (void);
// 0x000003B2 System.Void LTDescr::<setCanvasMoveY>b__103_0()
extern void LTDescr_U3CsetCanvasMoveYU3Eb__103_0_m4730D57FCDEA041D3F4111E45E34C15149C4BD4C (void);
// 0x000003B3 System.Void LTDescr::<setCanvasMoveY>b__103_1()
extern void LTDescr_U3CsetCanvasMoveYU3Eb__103_1_m9CF415CA444CEBB9B5B6C01B337E1C4598B74A32 (void);
// 0x000003B4 System.Void LTDescr::<setCanvasMoveZ>b__104_0()
extern void LTDescr_U3CsetCanvasMoveZU3Eb__104_0_mDCD6897BB64C355669F8EE9CFD8F2B386F830CA2 (void);
// 0x000003B5 System.Void LTDescr::<setCanvasMoveZ>b__104_1()
extern void LTDescr_U3CsetCanvasMoveZU3Eb__104_1_m5B12F7E220DA96B6F05025A4BE1AFABDEEAFF853 (void);
// 0x000003B6 System.Void LTDescr::<setCanvasRotateAround>b__106_0()
extern void LTDescr_U3CsetCanvasRotateAroundU3Eb__106_0_m7254F615E75865226968EA098B90FE904EE5B38B (void);
// 0x000003B7 System.Void LTDescr::<setCanvasRotateAroundLocal>b__107_0()
extern void LTDescr_U3CsetCanvasRotateAroundLocalU3Eb__107_0_m2C7BE76A27280304C47F1787199EA2790E10D6D6 (void);
// 0x000003B8 System.Void LTDescr::<setCanvasPlaySprite>b__108_0()
extern void LTDescr_U3CsetCanvasPlaySpriteU3Eb__108_0_m60117A734E7707143FD04D2FBEE6B75618B258DF (void);
// 0x000003B9 System.Void LTDescr::<setCanvasPlaySprite>b__108_1()
extern void LTDescr_U3CsetCanvasPlaySpriteU3Eb__108_1_mAA1E46E79560810DA46AE8EA0BFA91EDAE3D2DA5 (void);
// 0x000003BA System.Void LTDescr::<setCanvasMove>b__109_0()
extern void LTDescr_U3CsetCanvasMoveU3Eb__109_0_mABE1F4262412CA7E93AA4B71881FAE5B817FCF8D (void);
// 0x000003BB System.Void LTDescr::<setCanvasMove>b__109_1()
extern void LTDescr_U3CsetCanvasMoveU3Eb__109_1_mD190E0829649FEA47BAB95DED30B6EAB95C03ECF (void);
// 0x000003BC System.Void LTDescr::<setCanvasScale>b__110_0()
extern void LTDescr_U3CsetCanvasScaleU3Eb__110_0_mF68FF782BFAD2E93A65661953E683EADC9A52DA0 (void);
// 0x000003BD System.Void LTDescr::<setCanvasScale>b__110_1()
extern void LTDescr_U3CsetCanvasScaleU3Eb__110_1_mCF2CCB10E46FC5F00061B020C857E7A35EF14E0D (void);
// 0x000003BE System.Void LTDescr::<setCanvasSizeDelta>b__111_0()
extern void LTDescr_U3CsetCanvasSizeDeltaU3Eb__111_0_m021163E83008DE7874346B03AE065037CDC34D3A (void);
// 0x000003BF System.Void LTDescr::<setCanvasSizeDelta>b__111_1()
extern void LTDescr_U3CsetCanvasSizeDeltaU3Eb__111_1_m9AA7511D4E1BF277C282F3E30388E82D0A9E1C0B (void);
// 0x000003C0 System.Void LTDescr::<setMove>b__115_0()
extern void LTDescr_U3CsetMoveU3Eb__115_0_m5DDD8D43FEBB5099535893334FD76E1F0DAFD21B (void);
// 0x000003C1 System.Void LTDescr::<setMove>b__115_1()
extern void LTDescr_U3CsetMoveU3Eb__115_1_m6B66D8C95AE011E8C641FA4AEF0245809F334399 (void);
// 0x000003C2 System.Void LTDescr::<setMoveLocal>b__116_0()
extern void LTDescr_U3CsetMoveLocalU3Eb__116_0_m7FE28EDDA5832CD6FF6BB2370DA6059DF23B2A1F (void);
// 0x000003C3 System.Void LTDescr::<setMoveLocal>b__116_1()
extern void LTDescr_U3CsetMoveLocalU3Eb__116_1_m8D5A11FA82E103327A9ABD0CA5BB397952CD260C (void);
// 0x000003C4 System.Void LTDescr::<setMoveToTransform>b__117_0()
extern void LTDescr_U3CsetMoveToTransformU3Eb__117_0_mCE51DD0DF5028FDE555344C8FF602CD98716C33B (void);
// 0x000003C5 System.Void LTDescr::<setMoveToTransform>b__117_1()
extern void LTDescr_U3CsetMoveToTransformU3Eb__117_1_mACD7BEE7BA65E5DCB5B234EB3A31D9D8C3ED7FA8 (void);
// 0x000003C6 System.Void LTDescr::<setRotate>b__118_0()
extern void LTDescr_U3CsetRotateU3Eb__118_0_m3ECAD3ACBA191CD5E6FBB67BB3E3AAEDA454B4D3 (void);
// 0x000003C7 System.Void LTDescr::<setRotate>b__118_1()
extern void LTDescr_U3CsetRotateU3Eb__118_1_mEB2249D09D0E69826AE6036FD448AF92D77B47D7 (void);
// 0x000003C8 System.Void LTDescr::<setRotateLocal>b__119_0()
extern void LTDescr_U3CsetRotateLocalU3Eb__119_0_m2AA0D5D9B5334CEC12D6E8670332C4BEEB5CD7DB (void);
// 0x000003C9 System.Void LTDescr::<setRotateLocal>b__119_1()
extern void LTDescr_U3CsetRotateLocalU3Eb__119_1_m64E3FC9E52EE3EB30AA9260408C3D02F24A34064 (void);
// 0x000003CA System.Void LTDescr::<setScale>b__120_0()
extern void LTDescr_U3CsetScaleU3Eb__120_0_m4528B485CAF7246E73A7E602504675D4B11A44B7 (void);
// 0x000003CB System.Void LTDescr::<setScale>b__120_1()
extern void LTDescr_U3CsetScaleU3Eb__120_1_m4A1D3D78C772991EC422F7D2D29EF4D069E39C78 (void);
// 0x000003CC System.Void LTDescr::<setGUIMove>b__121_0()
extern void LTDescr_U3CsetGUIMoveU3Eb__121_0_m4AD272AF144CD468B048F733B93D1DFC08527804 (void);
// 0x000003CD System.Void LTDescr::<setGUIMove>b__121_1()
extern void LTDescr_U3CsetGUIMoveU3Eb__121_1_mDBDABB8CB0F1D7CC05BF861826BD23B1A5CF68B3 (void);
// 0x000003CE System.Void LTDescr::<setGUIMoveMargin>b__122_0()
extern void LTDescr_U3CsetGUIMoveMarginU3Eb__122_0_m92AE989BFBF68D262DDFD12EB9DBBE05BDCE7AB1 (void);
// 0x000003CF System.Void LTDescr::<setGUIMoveMargin>b__122_1()
extern void LTDescr_U3CsetGUIMoveMarginU3Eb__122_1_mA60DFD7F27C11D0B55761F7A0936D8056ED09382 (void);
// 0x000003D0 System.Void LTDescr::<setGUIScale>b__123_0()
extern void LTDescr_U3CsetGUIScaleU3Eb__123_0_m5A0FC254BDC374AACB5FED5922852DD294CBA77C (void);
// 0x000003D1 System.Void LTDescr::<setGUIScale>b__123_1()
extern void LTDescr_U3CsetGUIScaleU3Eb__123_1_m01CD6A1BE962B34E940B420723AE967C9AF4B248 (void);
// 0x000003D2 System.Void LTDescr::<setGUIAlpha>b__124_0()
extern void LTDescr_U3CsetGUIAlphaU3Eb__124_0_m584C608C8B326C3B0401CAA8C696F8ABCF95DD38 (void);
// 0x000003D3 System.Void LTDescr::<setGUIAlpha>b__124_1()
extern void LTDescr_U3CsetGUIAlphaU3Eb__124_1_mF0CAD09EF53473D82DF70172C27DA602272E3361 (void);
// 0x000003D4 System.Void LTDescr::<setGUIRotate>b__125_0()
extern void LTDescr_U3CsetGUIRotateU3Eb__125_0_m8F9A74633240B6083A3C1DB7EA24F762D40589C9 (void);
// 0x000003D5 System.Void LTDescr::<setGUIRotate>b__125_1()
extern void LTDescr_U3CsetGUIRotateU3Eb__125_1_m43CF134D7443CAA354F69A7031CE9F2574CE4DA5 (void);
// 0x000003D6 System.Void LTDescr::<setDelayedSound>b__126_0()
extern void LTDescr_U3CsetDelayedSoundU3Eb__126_0_mEF9FE367DCCC3DDE76DA47246D17F221C0F4D93F (void);
// 0x000003D7 System.Void LTDescr/EaseTypeDelegate::.ctor(System.Object,System.IntPtr)
extern void EaseTypeDelegate__ctor_mCD18183DC7DD02010D779AB3969F9D2B6BDAEA28 (void);
// 0x000003D8 UnityEngine.Vector3 LTDescr/EaseTypeDelegate::Invoke()
extern void EaseTypeDelegate_Invoke_mC562BC4CD7B5786B26A9D7F780BC43F1B2F601AB (void);
// 0x000003D9 System.IAsyncResult LTDescr/EaseTypeDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void EaseTypeDelegate_BeginInvoke_m8B1407D35ABAD4BA8A49AA4CE44736F90B5D834F (void);
// 0x000003DA UnityEngine.Vector3 LTDescr/EaseTypeDelegate::EndInvoke(System.IAsyncResult)
extern void EaseTypeDelegate_EndInvoke_m275A58EADF116EBDD21996509E57703CCC10C75A (void);
// 0x000003DB System.Void LTDescr/ActionMethodDelegate::.ctor(System.Object,System.IntPtr)
extern void ActionMethodDelegate__ctor_m86D0105115F69AEC95F9DB9542A3CB52854649E6 (void);
// 0x000003DC System.Void LTDescr/ActionMethodDelegate::Invoke()
extern void ActionMethodDelegate_Invoke_m4C86E95A515A25BE5ADB85D4889AAEDAA157173A (void);
// 0x000003DD System.IAsyncResult LTDescr/ActionMethodDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void ActionMethodDelegate_BeginInvoke_m1B3C88E85A1CF2B1CE645AC9594565046D26BE0B (void);
// 0x000003DE System.Void LTDescr/ActionMethodDelegate::EndInvoke(System.IAsyncResult)
extern void ActionMethodDelegate_EndInvoke_m7B0A147CA95FB6A16463DFA4C30DE37053C5D72F (void);
// 0x000003DF System.Void LTDescr/<>c::.cctor()
extern void U3CU3Ec__cctor_mACE0E27AEF258161C83D9EA95E78DBD6B559A5EB (void);
// 0x000003E0 System.Void LTDescr/<>c::.ctor()
extern void U3CU3Ec__ctor_mB934C02FF6DB889EA42A5581DCA0B5CEDBCC5903 (void);
// 0x000003E1 System.Void LTDescr/<>c::<setCallback>b__113_0()
extern void U3CU3Ec_U3CsetCallbackU3Eb__113_0_m7FC51BAA4B25A66AAAE19427E427E31C01E7EF0D (void);
// 0x000003E2 System.Void LTDescr/<>c::<setValue3>b__114_0()
extern void U3CU3Ec_U3CsetValue3U3Eb__114_0_mF93F5C6F0B095D8B1EE239BE2A475A16E713A012 (void);
// 0x000003E3 UnityEngine.Transform LTDescrOptional::get_toTrans()
extern void LTDescrOptional_get_toTrans_mB4912C34FDE4BB3CE94B86FDE3D7D8C0DE951773 (void);
// 0x000003E4 System.Void LTDescrOptional::set_toTrans(UnityEngine.Transform)
extern void LTDescrOptional_set_toTrans_m4C747BFF6573BF8C2484AF01C3FCCC65E8A54FB9 (void);
// 0x000003E5 UnityEngine.Vector3 LTDescrOptional::get_point()
extern void LTDescrOptional_get_point_mB79A8D97B381FE3122B1549DBB3527F9691A1156 (void);
// 0x000003E6 System.Void LTDescrOptional::set_point(UnityEngine.Vector3)
extern void LTDescrOptional_set_point_mD7AE42D3184ED0D66545DC3A98B5CB5759DF8627 (void);
// 0x000003E7 UnityEngine.Vector3 LTDescrOptional::get_axis()
extern void LTDescrOptional_get_axis_m546D8ADD4003D9B69F7EC4A243008064AC42DB20 (void);
// 0x000003E8 System.Void LTDescrOptional::set_axis(UnityEngine.Vector3)
extern void LTDescrOptional_set_axis_m17A070B698AFE6D066C10EEF1A86DDEABC0A13B0 (void);
// 0x000003E9 System.Single LTDescrOptional::get_lastVal()
extern void LTDescrOptional_get_lastVal_mD84B4C7F0D87E853D6EB9786261068DA74031DF8 (void);
// 0x000003EA System.Void LTDescrOptional::set_lastVal(System.Single)
extern void LTDescrOptional_set_lastVal_m1B2DC6C4CE540BA60BD467F4394CEF18409B820C (void);
// 0x000003EB UnityEngine.Quaternion LTDescrOptional::get_origRotation()
extern void LTDescrOptional_get_origRotation_m1B98DB0AACC7AF940C43DC29DF7A61FE6E49736E (void);
// 0x000003EC System.Void LTDescrOptional::set_origRotation(UnityEngine.Quaternion)
extern void LTDescrOptional_set_origRotation_m6E9E885A6259D7DE7FD1D398CABD7F7CB1751DE4 (void);
// 0x000003ED LTBezierPath LTDescrOptional::get_path()
extern void LTDescrOptional_get_path_m20A38F266BEEDD6370F8F54B2FA58C7262808AC5 (void);
// 0x000003EE System.Void LTDescrOptional::set_path(LTBezierPath)
extern void LTDescrOptional_set_path_m10D4A506CACD00E0951E6E5E41003DE99C72DA64 (void);
// 0x000003EF LTSpline LTDescrOptional::get_spline()
extern void LTDescrOptional_get_spline_m32BE3A98427C2A4FBE40E1BAEDEC2246ABD3D4F2 (void);
// 0x000003F0 System.Void LTDescrOptional::set_spline(LTSpline)
extern void LTDescrOptional_set_spline_m5FF9921610A1F8C38E004CF9129F185BC7FDD708 (void);
// 0x000003F1 LTRect LTDescrOptional::get_ltRect()
extern void LTDescrOptional_get_ltRect_mDBE34D781428B8DD07A8F25AC32B9B0932B62BE7 (void);
// 0x000003F2 System.Void LTDescrOptional::set_ltRect(LTRect)
extern void LTDescrOptional_set_ltRect_m273BF61CF793266842167003657D7083E62B655A (void);
// 0x000003F3 System.Action`1<System.Single> LTDescrOptional::get_onUpdateFloat()
extern void LTDescrOptional_get_onUpdateFloat_m8573CE5DFEFBA6E350AF056485B265FF59E8072B (void);
// 0x000003F4 System.Void LTDescrOptional::set_onUpdateFloat(System.Action`1<System.Single>)
extern void LTDescrOptional_set_onUpdateFloat_mC6B9DD9721D66C62B4F196BFB0AC6EAFFD98A10F (void);
// 0x000003F5 System.Action`2<System.Single,System.Single> LTDescrOptional::get_onUpdateFloatRatio()
extern void LTDescrOptional_get_onUpdateFloatRatio_m6757E471D3ADDB9202062289E6EFAD3B61B2C7F4 (void);
// 0x000003F6 System.Void LTDescrOptional::set_onUpdateFloatRatio(System.Action`2<System.Single,System.Single>)
extern void LTDescrOptional_set_onUpdateFloatRatio_m0FBA91E49D4D592E2CAA88BF2963B2B5737ACEF7 (void);
// 0x000003F7 System.Action`2<System.Single,System.Object> LTDescrOptional::get_onUpdateFloatObject()
extern void LTDescrOptional_get_onUpdateFloatObject_m8A282C0573D32731C92CA8A4E352382CE73E4839 (void);
// 0x000003F8 System.Void LTDescrOptional::set_onUpdateFloatObject(System.Action`2<System.Single,System.Object>)
extern void LTDescrOptional_set_onUpdateFloatObject_mF14622BEBB5CCF990BC0DC9622CE2EE037F364C5 (void);
// 0x000003F9 System.Action`1<UnityEngine.Vector2> LTDescrOptional::get_onUpdateVector2()
extern void LTDescrOptional_get_onUpdateVector2_m2692F9A7698099312B1956EF89ADC677E156325A (void);
// 0x000003FA System.Void LTDescrOptional::set_onUpdateVector2(System.Action`1<UnityEngine.Vector2>)
extern void LTDescrOptional_set_onUpdateVector2_mC9D4DD0709714E0D052A2A6E475099B146375331 (void);
// 0x000003FB System.Action`1<UnityEngine.Vector3> LTDescrOptional::get_onUpdateVector3()
extern void LTDescrOptional_get_onUpdateVector3_m1A96786092458EFAEAB22467DC6AA01F2F34247A (void);
// 0x000003FC System.Void LTDescrOptional::set_onUpdateVector3(System.Action`1<UnityEngine.Vector3>)
extern void LTDescrOptional_set_onUpdateVector3_m91295D4F19AFF3080F6B44A071118CCDB52F288B (void);
// 0x000003FD System.Action`2<UnityEngine.Vector3,System.Object> LTDescrOptional::get_onUpdateVector3Object()
extern void LTDescrOptional_get_onUpdateVector3Object_m2CAC540C86A54F7744AE39443FB4054066E3DE02 (void);
// 0x000003FE System.Void LTDescrOptional::set_onUpdateVector3Object(System.Action`2<UnityEngine.Vector3,System.Object>)
extern void LTDescrOptional_set_onUpdateVector3Object_m2ECE95CB5D7DFE5E9DEDB9F3B03BEF39D4000B77 (void);
// 0x000003FF System.Action`1<UnityEngine.Color> LTDescrOptional::get_onUpdateColor()
extern void LTDescrOptional_get_onUpdateColor_m78FBDE4D9B8856EBE7EBF42709CFF81E127B9A43 (void);
// 0x00000400 System.Void LTDescrOptional::set_onUpdateColor(System.Action`1<UnityEngine.Color>)
extern void LTDescrOptional_set_onUpdateColor_m7D77082D84B3831810A825A0628CE556012AAA2B (void);
// 0x00000401 System.Action`2<UnityEngine.Color,System.Object> LTDescrOptional::get_onUpdateColorObject()
extern void LTDescrOptional_get_onUpdateColorObject_m45EF15CE7E1F771A04B6E9374323759A7A021C62 (void);
// 0x00000402 System.Void LTDescrOptional::set_onUpdateColorObject(System.Action`2<UnityEngine.Color,System.Object>)
extern void LTDescrOptional_set_onUpdateColorObject_m25F9B89775D1630387BEDF4DC8B2F88F8E4842A0 (void);
// 0x00000403 System.Action LTDescrOptional::get_onComplete()
extern void LTDescrOptional_get_onComplete_m89C63A691247B03AF894749D3F32987E7994CFA8 (void);
// 0x00000404 System.Void LTDescrOptional::set_onComplete(System.Action)
extern void LTDescrOptional_set_onComplete_m71A16E08537D67C75C706DD0EB88B7954A0CFCAF (void);
// 0x00000405 System.Action`1<System.Object> LTDescrOptional::get_onCompleteObject()
extern void LTDescrOptional_get_onCompleteObject_m50BB3E137E79340957CEE096A24A3635ACD91DEA (void);
// 0x00000406 System.Void LTDescrOptional::set_onCompleteObject(System.Action`1<System.Object>)
extern void LTDescrOptional_set_onCompleteObject_m70FA4651F7E2CA76267B09C3EFBEA8AFE62F95E3 (void);
// 0x00000407 System.Object LTDescrOptional::get_onCompleteParam()
extern void LTDescrOptional_get_onCompleteParam_mD546B9E0E31E70CEF0514DFB486D49E4BBE9D120 (void);
// 0x00000408 System.Void LTDescrOptional::set_onCompleteParam(System.Object)
extern void LTDescrOptional_set_onCompleteParam_mE4F5C590CFA4FF8204FE0C4722BF7EEBC1F4D7C7 (void);
// 0x00000409 System.Object LTDescrOptional::get_onUpdateParam()
extern void LTDescrOptional_get_onUpdateParam_m8568043E9421D14172B62712FF0519738F0E8D88 (void);
// 0x0000040A System.Void LTDescrOptional::set_onUpdateParam(System.Object)
extern void LTDescrOptional_set_onUpdateParam_m415D9E73546338141E97EBEB8587C8A6F00C18E6 (void);
// 0x0000040B System.Action LTDescrOptional::get_onStart()
extern void LTDescrOptional_get_onStart_m67196E7D6B56A68A3D7A5BAC7201CB5D79575EEF (void);
// 0x0000040C System.Void LTDescrOptional::set_onStart(System.Action)
extern void LTDescrOptional_set_onStart_mFC780FBC4B2363070A9395BAA7693ACBFA3C75F8 (void);
// 0x0000040D System.Void LTDescrOptional::reset()
extern void LTDescrOptional_reset_mBC39AAD50AF833763A2547D2BB295CCE04F8CDBB (void);
// 0x0000040E System.Void LTDescrOptional::callOnUpdate(System.Single,System.Single)
extern void LTDescrOptional_callOnUpdate_mCD2B766E5358969C288B360A409B741962E90A9E (void);
// 0x0000040F System.Void LTDescrOptional::.ctor()
extern void LTDescrOptional__ctor_m31327367F73AF26F1F0513529F56B4D4527F1CD7 (void);
// 0x00000410 System.Int32 LTSeq::get_id()
extern void LTSeq_get_id_m7BB843F84403ED4D5D7816EAB92F64D64AA16528 (void);
// 0x00000411 System.Void LTSeq::reset()
extern void LTSeq_reset_m81F08444C11BCB0DA11DA2655C967A31B0F48E43 (void);
// 0x00000412 System.Void LTSeq::init(System.UInt32,System.UInt32)
extern void LTSeq_init_m7666AE5720766D32C80F45C08B15F29BDF926FCA (void);
// 0x00000413 LTSeq LTSeq::addOn()
extern void LTSeq_addOn_m92322FBE7CE53DC475ECF67BD47ED0A0FB37471A (void);
// 0x00000414 System.Single LTSeq::addPreviousDelays()
extern void LTSeq_addPreviousDelays_mA44FF52A17DF8B8F70385938E809B922AE96D914 (void);
// 0x00000415 LTSeq LTSeq::append(System.Single)
extern void LTSeq_append_m9CCA2E2A2FEC8BB783E6973C5259009DE1F2CA93 (void);
// 0x00000416 LTSeq LTSeq::append(System.Action)
extern void LTSeq_append_m918D5B42DF7E78F1D632BDFB559566153BC03151 (void);
// 0x00000417 LTSeq LTSeq::append(System.Action`1<System.Object>,System.Object)
extern void LTSeq_append_m771276B4D19FF6E029D920552CA35785C06166DC (void);
// 0x00000418 LTSeq LTSeq::append(UnityEngine.GameObject,System.Action)
extern void LTSeq_append_m7B1CF4D2CEE5FEC4FD64BE97123E3466981969C9 (void);
// 0x00000419 LTSeq LTSeq::append(UnityEngine.GameObject,System.Action`1<System.Object>,System.Object)
extern void LTSeq_append_m38B101D631E233A3EA115D8102FECE3A9F8501D0 (void);
// 0x0000041A LTSeq LTSeq::append(LTDescr)
extern void LTSeq_append_m16B953CD94139BC1C75791A5D461C744EA4ECA64 (void);
// 0x0000041B LTSeq LTSeq::insert(LTDescr)
extern void LTSeq_insert_m557E13DE79F5FDF0DCC9746AA15307FED3DA9F0A (void);
// 0x0000041C LTSeq LTSeq::setScale(System.Single)
extern void LTSeq_setScale_m01A80A5EF1549289CCFE8F7E7176A5B669781699 (void);
// 0x0000041D System.Void LTSeq::setScaleRecursive(LTSeq,System.Single,System.Int32)
extern void LTSeq_setScaleRecursive_mA815906D527D73895B508205E6DF519C26560C7F (void);
// 0x0000041E LTSeq LTSeq::reverse()
extern void LTSeq_reverse_m626178075D66F45F66D451A95BCFFDBCAD95E04C (void);
// 0x0000041F System.Void LTSeq::.ctor()
extern void LTSeq__ctor_m0C853ED35E39038B9153A5E212A11D5366E3EB01 (void);
// 0x00000420 System.Boolean CSAlert::get_active()
extern void CSAlert_get_active_mC5B8764A86575F47018AC1E0D19FB794D7C7C63B (void);
// 0x00000421 System.Void CSAlert::set_active(System.Boolean)
extern void CSAlert_set_active_mF170DD19A7A2A402D2CE725381844C53CF9D25BF (void);
// 0x00000422 System.Void CSAlert::Awake()
extern void CSAlert_Awake_m3D0BE042AD4D8C019D4E3BED6AB0D0AED741B9FC (void);
// 0x00000423 System.Void CSAlert::Appear(System.Action)
extern void CSAlert_Appear_m6EBB93449DD91DD2194BBD91E10D43DEEC49B7B0 (void);
// 0x00000424 System.Void CSAlert::ScaleActionCompleted()
extern void CSAlert_ScaleActionCompleted_mA52B4130600CBA1FF43D872EDB1A8B1018A0B8BF (void);
// 0x00000425 System.Void CSAlert::Disappear(System.Action)
extern void CSAlert_Disappear_mC54683355BB67451B479B2A12C3284382686FCAE (void);
// 0x00000426 System.Void CSAlert::Init()
extern void CSAlert_Init_m73B7799D7D8A49E18A01BD3C31E8816B945D4BEF (void);
// 0x00000427 System.Void CSAlert::HideBoards()
extern void CSAlert_HideBoards_m1F7620E49D0A33A6DB31C82800127561F256F53E (void);
// 0x00000428 LTDescr CSAlert::ScaleAction(System.Action)
extern void CSAlert_ScaleAction_mF1B81F23A4F3A13225B2C13032E9D41BF9BE93E9 (void);
// 0x00000429 LTDescr CSAlert::MoveAction(UnityEngine.GameObject,UnityEngine.Vector3,System.Action)
extern void CSAlert_MoveAction_mFDB0B6DC6074DA7B3F94F7B7C729F1878396D01B (void);
// 0x0000042A System.Void CSAlert::AddPaticle()
extern void CSAlert_AddPaticle_m3E0BE7CF647C2682EC45D2C4A187721DA13A3FC4 (void);
// 0x0000042B System.Void CSAlert::DestroyPaticle()
extern void CSAlert_DestroyPaticle_m20D0B2E007EB8FC26E3500EB7D57FA8CDE2B0BAD (void);
// 0x0000042C System.Void CSAlert::CanvasStatus(System.Boolean)
extern void CSAlert_CanvasStatus_m80BD4FE178D2A374142BEA746DB7FDC86BEE8EBA (void);
// 0x0000042D System.Void CSAlert::OnCollect()
extern void CSAlert_OnCollect_mF9713B011E292BBD094EE068952B10BA27B9CF1A (void);
// 0x0000042E System.Void CSAlert::OnShare()
extern void CSAlert_OnShare_m2400699168BAA44090B6CD7F76620476D6792F5C (void);
// 0x0000042F System.Void CSAlert::.ctor()
extern void CSAlert__ctor_mF41167DB7EF732F9D3F75A1ABE6D3D12FE3476E6 (void);
// 0x00000430 System.Void CSAlert::<ScaleAction>b__24_0(UnityEngine.Vector2)
extern void CSAlert_U3CScaleActionU3Eb__24_0_mA4C2DF12B01ACF01082ACC7E4FC8616245A366CF (void);
// 0x00000431 System.Void CSAlert/<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_m84FF5523AD0F6EAC7675B6BBA7BA76B54C41E90A (void);
// 0x00000432 System.Void CSAlert/<>c__DisplayClass19_0::<Appear>b__0()
extern void U3CU3Ec__DisplayClass19_0_U3CAppearU3Eb__0_m9196E668F8639F11C3BA037FB4ABB9C30B46BEFF (void);
// 0x00000433 System.Void CSAlert/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_m503CBBCF9D0E9A9E8884917C2886FF0D95FE5902 (void);
// 0x00000434 System.Void CSAlert/<>c__DisplayClass21_0::<Disappear>b__0()
extern void U3CU3Ec__DisplayClass21_0_U3CDisappearU3Eb__0_mDB7FC6B50A194FA78E79B6DB3D9A430F2D01D18D (void);
// 0x00000435 System.Void CSAlertRewardAnim::ScaleActionCompleted()
extern void CSAlertRewardAnim_ScaleActionCompleted_mAFE3E629690CC832C3E437824699AB1DEB31EA7D (void);
// 0x00000436 System.Void CSAlertRewardAnim::AppearWithReward(System.Single,System.Action)
extern void CSAlertRewardAnim_AppearWithReward_m7724C1C8FD8C4C5DEA03CADF699E112FD477B005 (void);
// 0x00000437 System.Void CSAlertRewardAnim::Appear(System.Action)
extern void CSAlertRewardAnim_Appear_mD5FDEB03F2BE6C0E88C8CBF4CADE4A9001F4CCD6 (void);
// 0x00000438 System.Void CSAlertRewardAnim::AddCoins()
extern void CSAlertRewardAnim_AddCoins_mDCAE20031B643FFF787CDE4552B1800CEC1B0967 (void);
// 0x00000439 System.Void CSAlertRewardAnim::AnimateRewardCoins(System.Single,System.Single)
extern void CSAlertRewardAnim_AnimateRewardCoins_m5F716A2D71A4011289F38CEA44E34F67A172B682 (void);
// 0x0000043A System.Void CSAlertRewardAnim::OnCollect()
extern void CSAlertRewardAnim_OnCollect_mDCBCEFAFB0718FAA987CA28A3C48F8B3594082C6 (void);
// 0x0000043B System.Void CSAlertRewardAnim::OnDouble()
extern void CSAlertRewardAnim_OnDouble_m9766562DC0F2C3992912071D3C5081EC633A0830 (void);
// 0x0000043C System.Void CSAlertRewardAnim::EnableShareButton(System.Boolean)
extern void CSAlertRewardAnim_EnableShareButton_mF5E0A82A129C63B0B8DEAD829F247184B1ACE663 (void);
// 0x0000043D System.Void CSAlertRewardAnim::.ctor()
extern void CSAlertRewardAnim__ctor_mD5B36902E2CAD2D68FDAD6C5600F5F4E5BBAD5B8 (void);
// 0x0000043E System.Void CSAlertRewardAnim::<OnDouble>b__9_0()
extern void CSAlertRewardAnim_U3COnDoubleU3Eb__9_0_mD85A3A3EE277E75F3E423E7AFF1666D3A782BE21 (void);
// 0x0000043F System.Void CSLevelUpAlert::Awake()
extern void CSLevelUpAlert_Awake_mE2D7C42AF0F5BB54FF63250619A46E6098134C37 (void);
// 0x00000440 System.Void CSLevelUpAlert::ScaleActionCompleted()
extern void CSLevelUpAlert_ScaleActionCompleted_m86690BFE0504B42336B82495C39A3F209F053CC0 (void);
// 0x00000441 System.Single CSLevelUpAlert::Reward()
extern void CSLevelUpAlert_Reward_m27380A04198AC929F7A9218355194B301A91C246 (void);
// 0x00000442 System.Void CSLevelUpAlert::Appear(System.Int32,System.Action)
extern void CSLevelUpAlert_Appear_mDE4AAAE772DCF1558287D404E6DF1634017B5208 (void);
// 0x00000443 LTDescr CSLevelUpAlert::MoveAction(UnityEngine.RectTransform,UnityEngine.Vector3,System.Action)
extern void CSLevelUpAlert_MoveAction_m46C694E0583E7F7CD9AE5CA11309EA08632C7FA1 (void);
// 0x00000444 System.Void CSLevelUpAlert::StarAction(UnityEngine.RectTransform)
extern void CSLevelUpAlert_StarAction_mC27C975150C66A6503F3B0289CB861121A05A654 (void);
// 0x00000445 System.Collections.Generic.Dictionary`2<System.Int32,StarData> CSLevelUpAlert::DefineStarInit(UnityEngine.RectTransform)
extern void CSLevelUpAlert_DefineStarInit_m43B8F9B5941286AEA0702FD06F4008F6BB2976ED (void);
// 0x00000446 System.Void CSLevelUpAlert::SetStar(UnityEngine.RectTransform,StarData,System.Boolean)
extern void CSLevelUpAlert_SetStar_mCB25A66A961A0E69F96F1DD101AFCF4C02D8FAEE (void);
// 0x00000447 System.Void CSLevelUpAlert::.ctor()
extern void CSLevelUpAlert__ctor_mE8E08597FD9B88042F3CDDEFA0746F8C8FCA5A0E (void);
// 0x00000448 System.Void StarData::.ctor(UnityEngine.RectTransform)
extern void StarData__ctor_m42ABA685AF38D2705BD9AE61B36F9E1E6CFA3A85 (void);
// 0x00000449 System.Boolean CSCSBlock::get_animate()
extern void CSCSBlock_get_animate_mE11CB4AF5AFB8E8C182B9FB9B55C409EBCEFFE81 (void);
// 0x0000044A System.Void CSCSBlock::set_animate(System.Boolean)
extern void CSCSBlock_set_animate_mED7B5B9EE6615D91D3D93DAB8A7388AEB18A0832 (void);
// 0x0000044B System.Void CSCSBlock::Awake()
extern void CSCSBlock_Awake_m18C45DEFD106F4724DBC6211BF7AC3CFA30A317E (void);
// 0x0000044C System.Void CSCSBlock::Start()
extern void CSCSBlock_Start_m0F5EC8A80DC1A22E5AE63410A72A2A3457FDB0EA (void);
// 0x0000044D System.Void CSCSBlock::OnDestroy()
extern void CSCSBlock_OnDestroy_mAC77DA074AD711FC9E13DD08747DACFFADFDC034 (void);
// 0x0000044E System.Void CSCSBlock::.ctor()
extern void CSCSBlock__ctor_mA00C3B606F7E924FDF91C4062B8D60713733654A (void);
// 0x0000044F System.Boolean CSCSBottomPanel::get_showGamble()
extern void CSCSBottomPanel_get_showGamble_m1DDE5B2D3A58976B4AA0D0F7DBA3FCAD1200B803 (void);
// 0x00000450 System.Void CSCSBottomPanel::set_showGamble(System.Boolean)
extern void CSCSBottomPanel_set_showGamble_mB00DDB493564B384DDADBAC13A17717877612941 (void);
// 0x00000451 System.Void CSCSBottomPanel::Loaded()
extern void CSCSBottomPanel_Loaded_m00637C935356B7E018F0069A593D9CF2CC21935F (void);
// 0x00000452 System.Void CSCSBottomPanel::OnChangeMultipiler(System.Int32)
extern void CSCSBottomPanel_OnChangeMultipiler_m4A05F28CBE0D88DF41BC12B5BB3E68B75C7D33FA (void);
// 0x00000453 System.Void CSCSBottomPanel::OnBetMax()
extern void CSCSBottomPanel_OnBetMax_mC0C2D1D52559DC00FE548D7DDDA9809C8D09080E (void);
// 0x00000454 System.Void CSCSBottomPanel::SetShowGamble(System.Boolean,System.Boolean)
extern void CSCSBottomPanel_SetShowGamble_m022745B1403AFEF8C2CAB5E6E7948DA8F51049D1 (void);
// 0x00000455 LTDescr CSCSBottomPanel::Move(System.Single)
extern void CSCSBottomPanel_Move_m706287C6EA69746DFD409ECE6289D41B39C17E54 (void);
// 0x00000456 System.Void CSCSBottomPanel::OnGamble()
extern void CSCSBottomPanel_OnGamble_m0790A4F8A3E9884017240F9732971A3B9064CBFA (void);
// 0x00000457 System.Void CSCSBottomPanel::WinAmount(System.Single)
extern void CSCSBottomPanel_WinAmount_m0E0F72E242FDD0E5F33BB7A5521EBF7FC3345595 (void);
// 0x00000458 System.Void CSCSBottomPanel::.ctor()
extern void CSCSBottomPanel__ctor_m522B4E43966C2CD6F184AE9AA854BA5F53D2D039 (void);
// 0x00000459 System.Void CSCSBottomPanel::<OnGamble>b__13_0()
extern void CSCSBottomPanel_U3COnGambleU3Eb__13_0_m32361AC3C885FD8D212D6FD9DAE0B945BA080693 (void);
// 0x0000045A System.Void CSCSFreeGameInterface::Awake()
extern void CSCSFreeGameInterface_Awake_mDD61C2161D12D02540034635D2934C65570BF09B (void);
// 0x0000045B System.Void CSCSFreeGameInterface::OnEnable()
extern void CSCSFreeGameInterface_OnEnable_m8AE23571E411DC7526A6EEAC59E002B5D05F1372 (void);
// 0x0000045C System.Void CSCSFreeGameInterface::OnDisable()
extern void CSCSFreeGameInterface_OnDisable_m1182BE8C024A77678207654A1A6C8E9DC9A26263 (void);
// 0x0000045D System.Void CSCSFreeGameInterface::FreeGameValueChanged(System.Boolean)
extern void CSCSFreeGameInterface_FreeGameValueChanged_mE5C3A102A1A3C03023D3A49863EB97CC3A9DEC50 (void);
// 0x0000045E System.Void CSCSFreeGameInterface::.ctor()
extern void CSCSFreeGameInterface__ctor_mD02813A50D44391965280EE077804445E4EFAA62 (void);
// 0x0000045F System.Int32 CSCSFreeGamePanel::get_totalBet()
extern void CSCSFreeGamePanel_get_totalBet_m376135DAF9232F195BFA74892F9D01B0ADD6B397 (void);
// 0x00000460 System.Void CSCSFreeGamePanel::set_totalBet(System.Int32)
extern void CSCSFreeGamePanel_set_totalBet_mBF4E4F5D1DA2BB72D26764CDC9A2DF1E726A1442 (void);
// 0x00000461 System.Boolean CSCSFreeGamePanel::get_muliplierEnable()
extern void CSCSFreeGamePanel_get_muliplierEnable_mE520FA5A871AB4B16D9E499E6AEB955D3E16517D (void);
// 0x00000462 System.Void CSCSFreeGamePanel::set_muliplierEnable(System.Boolean)
extern void CSCSFreeGamePanel_set_muliplierEnable_mFE826957CC779B2CBFDE41C75354CB44096FF1FA (void);
// 0x00000463 System.Int32 CSCSFreeGamePanel::get_multiplier()
extern void CSCSFreeGamePanel_get_multiplier_mD526D4CEDD4A7A6B68E1454F27340FDC84ECAEAE (void);
// 0x00000464 System.Void CSCSFreeGamePanel::set_multiplier(System.Int32)
extern void CSCSFreeGamePanel_set_multiplier_m50711E46A410F546FDFD3CDC6CE041D44C932934 (void);
// 0x00000465 System.Void CSCSFreeGamePanel::SetWin(System.Single)
extern void CSCSFreeGamePanel_SetWin_m1B2F94F62720B4E8F3CC8F1EDEA674B4F8B2B95D (void);
// 0x00000466 System.Void CSCSFreeGamePanel::.ctor()
extern void CSCSFreeGamePanel__ctor_m4AA51D30054BC503E52483A7A9F0B17234481A35 (void);
// 0x00000467 System.Void CSCSGamble::.ctor()
extern void CSCSGamble__ctor_m1622FC3EB223643EC52E482C22854F1406F63E12 (void);
// 0x00000468 System.Void CSCSLoading::Start()
extern void CSCSLoading_Start_mFC1947A449DFA2FC71B6C12CF5CF65928B8AA704 (void);
// 0x00000469 System.Void CSCSLoading::OnDestroy()
extern void CSCSLoading_OnDestroy_m991735735857AFF39A905E9933D6D7B940DE4F21 (void);
// 0x0000046A System.Void CSCSLoading::Load(System.String)
extern void CSCSLoading_Load_mE103BFDAD5876834A2F05B6C47D087B7D693FAF5 (void);
// 0x0000046B System.Void CSCSLoading::Load()
extern void CSCSLoading_Load_m50CFFB7D3A49EF65D3B9A1FE9BB42C8203CEEA34 (void);
// 0x0000046C System.Void CSCSLoading::.ctor()
extern void CSCSLoading__ctor_m7667374BDA6DF683F0B4AB8355E0000E7341B69E (void);
// 0x0000046D System.Void CSCSLoading::<Load>b__4_0(System.Single)
extern void CSCSLoading_U3CLoadU3Eb__4_0_m617FB510FCE2A1DDBC6A0476B096A28EF57FC512 (void);
// 0x0000046E System.Void CSCSLoading/<>c::.cctor()
extern void U3CU3Ec__cctor_mB0F5C4244AD9E79B9C2CC693780AC3BE1F53DB34 (void);
// 0x0000046F System.Void CSCSLoading/<>c::.ctor()
extern void U3CU3Ec__ctor_m18443F007E70E5A805A177B368210B7856AFA8D2 (void);
// 0x00000470 System.Void CSCSLoading/<>c::<Load>b__4_1(System.Single,System.Boolean)
extern void U3CU3Ec_U3CLoadU3Eb__4_1_m725BCD9908A2B3B00A528C7284C47B840ECF7B28 (void);
// 0x00000471 System.Void CSCSBonusGameSettings::.ctor(System.Int32,System.Single,System.Int32)
extern void CSCSBonusGameSettings__ctor_m058A3519669A5F5179EC9C3C2148D50331189E9B (void);
// 0x00000472 CSCSBonusGameSettings CSCSBonusGameSettings::get_Zero()
extern void CSCSBonusGameSettings_get_Zero_m41F85776A908212EC522EBE58304DC06D909D9AB (void);
// 0x00000473 System.Void CSCSReels::UpdateFreeGamePanel(System.Boolean)
extern void CSCSReels_UpdateFreeGamePanel_m5907BD3C5FE11812A78409CF33C3F52D23095933 (void);
// 0x00000474 System.Void CSCSReels::BonusGame(System.Int32)
extern void CSCSReels_BonusGame_m45BC2CDFA4983BD80BF7B479AB0F4E34C3DDD2BA (void);
// 0x00000475 CSCSBonusGameSettings CSCSReels::SettingsForScatter(System.Int32)
extern void CSCSReels_SettingsForScatter_m8465A51ACB4B083FA4AC7C20B77C61856E934FD8 (void);
// 0x00000476 System.Void CSCSReels::.ctor()
extern void CSCSReels__ctor_mB1B974FF937077687D7414D5F24FB1312398E609 (void);
// 0x00000477 System.Void CSCSSymbol::.ctor()
extern void CSCSSymbol__ctor_m23DC1FACAB532B78CE260CB2038E02D389CC2248 (void);
// 0x00000478 System.Void CSCSTopPanel::OnInfo()
extern void CSCSTopPanel_OnInfo_m1D621BDC03E96E9673C007A9A11E3850597BC712 (void);
// 0x00000479 System.Void CSCSTopPanel::.ctor()
extern void CSCSTopPanel__ctor_m6E51EFB9F3A136091C33B48C84DA724DE8D6805B (void);
// 0x0000047A System.Void CSCSWinAlert::Appear(CSCSBonusGameSettings,System.Action)
extern void CSCSWinAlert_Appear_m5DDCE0CF9D75F7F3111F30028C7448B8990AC0CE (void);
// 0x0000047B System.Void CSCSWinAlert::OnCollect()
extern void CSCSWinAlert_OnCollect_mE546A680981C1221C1FA2E5E347BF9BDBBAEAC54 (void);
// 0x0000047C System.Void CSCSWinAlert::Reset()
extern void CSCSWinAlert_Reset_m720D44828311C2F7A6425D2A500FCD224C6EDBB5 (void);
// 0x0000047D System.Void CSCSWinAlert::.ctor()
extern void CSCSWinAlert__ctor_m4C28BD9731A575EC89243A1627792AE5F421B9A8 (void);
// 0x0000047E System.Boolean CSGameStore::get_active()
extern void CSGameStore_get_active_m25A2CCF8D4668E15F295D189CDBB52609302B87B (void);
// 0x0000047F System.Void CSGameStore::set_active(System.Boolean)
extern void CSGameStore_set_active_mD2613BE891FA8517E515A81CDD3DAA54A15E5238 (void);
// 0x00000480 System.Single CSGameStore::get_coins()
extern void CSGameStore_get_coins_m05421ADD315755571EB7ACE98A95463B441A97D4 (void);
// 0x00000481 System.Void CSGameStore::set_coins(System.Single)
extern void CSGameStore_set_coins_m22BA8CF0B64C3EC78B61CD4D5F8846156AAB62BF (void);
// 0x00000482 System.Void CSGameStore::Awake()
extern void CSGameStore_Awake_m5CAF5E0FD48FCA0D38C194C12FC254BFD910F263 (void);
// 0x00000483 System.Void CSGameStore::OnEnable()
extern void CSGameStore_OnEnable_mA4D4B8B9AC5E0E0C07A01CDFBB8040E6A6EE4B93 (void);
// 0x00000484 System.Void CSGameStore::OnDisable()
extern void CSGameStore_OnDisable_mBFA8E2C97F8246A3A1788E890D189F175A739325 (void);
// 0x00000485 System.Void CSGameStore::Appear()
extern void CSGameStore_Appear_m8AB09B9DF7F942304AA98AB85E6AE45A1370D0CC (void);
// 0x00000486 System.Void CSGameStore::Disappear(System.Action)
extern void CSGameStore_Disappear_m663BE11F6701792A338C731D41006FECB2E8BF86 (void);
// 0x00000487 LTDescr CSGameStore::Scale(LeanTweenType)
extern void CSGameStore_Scale_m6BA966E2852A086CC5178B15EFE7F54E6ECC413F (void);
// 0x00000488 LTDescr CSGameStore::AlphaBackground(System.Single)
extern void CSGameStore_AlphaBackground_mFDC97250FB5D0777FA388669320A9850FA1240CB (void);
// 0x00000489 LTDescr CSGameStore::AlphaBoard(System.Single)
extern void CSGameStore_AlphaBoard_m3B7C7556D29657F54DD43ECB479AF66492086E47 (void);
// 0x0000048A LTDescr CSGameStore::Alpha(UnityEngine.UI.Image,System.Single,System.Single)
extern void CSGameStore_Alpha_m6C93D0D91C196ED304A583472F9936A106891D01 (void);
// 0x0000048B System.Void CSGameStore::CanvasStatus(System.Boolean)
extern void CSGameStore_CanvasStatus_m80FC9192303E65844319AF70B4CAEE4A4DC0CEE3 (void);
// 0x0000048C System.Void CSGameStore::OnClose()
extern void CSGameStore_OnClose_m9F28895C1CB6CF147BEC46776D48BB02EAFA07BD (void);
// 0x0000048D System.Void CSGameStore::OnPack(CSStoreCoinPack)
extern void CSGameStore_OnPack_m44105379E72C41AD896CB641312625DEC1B44F4B (void);
// 0x0000048E System.Void CSGameStore::CoinPanelValueChanged(CSBankCoinPanel)
extern void CSGameStore_CoinPanelValueChanged_m50B9C35A2020D6EF628CF7EE04CF04978987B592 (void);
// 0x0000048F System.Void CSGameStore::.ctor()
extern void CSGameStore__ctor_m2663CB2ABDC19E1AFEE9C6BE5A21084D6483970E (void);
// 0x00000490 System.Void CSGameStore::<OnClose>b__27_0()
extern void CSGameStore_U3COnCloseU3Eb__27_0_m18128A21422ED9D26C0A86BA927FF7E35F3C66AF (void);
// 0x00000491 System.Void CSGameStore/<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_mB5F439256C70796DF24FBF1ABAAE1BCE25941CBA (void);
// 0x00000492 System.Void CSGameStore/<>c__DisplayClass25_0::<Alpha>b__0(System.Single)
extern void U3CU3Ec__DisplayClass25_0_U3CAlphaU3Eb__0_m5A03C7FD017BF27255D5AD70E18D555B78301799 (void);
// 0x00000493 System.Int32 CSLFBGAlertBoard::get_multiplier()
extern void CSLFBGAlertBoard_get_multiplier_m193A730CFBF905D5CCF153FA4DFD5F6D0249BFE5 (void);
// 0x00000494 System.Void CSLFBGAlertBoard::set_multiplier(System.Int32)
extern void CSLFBGAlertBoard_set_multiplier_mE386FBEDD873CA316BE0E3705632AF342CFCCE10 (void);
// 0x00000495 System.Int32 CSLFBGAlertBoard::get_freeSpins()
extern void CSLFBGAlertBoard_get_freeSpins_mB28202E401B8226FE1680CC2F95044DA96C3D23E (void);
// 0x00000496 System.Void CSLFBGAlertBoard::set_freeSpins(System.Int32)
extern void CSLFBGAlertBoard_set_freeSpins_m43A33D80D5A2C8DDBE069A1F261CEEA8711267FA (void);
// 0x00000497 System.Void CSLFBGAlertBoard::.ctor()
extern void CSLFBGAlertBoard__ctor_m2F63EA1D6B26EAD48B0B3E61F6C5AB94AC3B83DC (void);
// 0x00000498 System.Single CSLFProgressBar::get_value()
extern void CSLFProgressBar_get_value_m30FC80B76971F1BAA70887F105E813FC81B486BC (void);
// 0x00000499 System.Void CSLFProgressBar::set_value(System.Single)
extern void CSLFProgressBar_set_value_m80BA16BF22727FB190B2544763CD3709F3654277 (void);
// 0x0000049A System.Void CSLFProgressBar::SetValue(System.Single)
extern void CSLFProgressBar_SetValue_m35BF9490D754C8A3ED962265C4223263472BF354 (void);
// 0x0000049B System.Void CSLFProgressBar::.ctor()
extern void CSLFProgressBar__ctor_mC6236F1AEFB32492E389DB39252D6A5329196FCA (void);
// 0x0000049C System.String CSStoreCoinPack::get_productId()
extern void CSStoreCoinPack_get_productId_m989AFADCF66D2158BBC653FF1B0AEC54B2CB5B95 (void);
// 0x0000049D System.Void CSStoreCoinPack::Start()
extern void CSStoreCoinPack_Start_m3DC68D8FDBF30F23C8F8CC635666A209E711B2BC (void);
// 0x0000049E System.Int32 CSStoreCoinPack::Total()
extern void CSStoreCoinPack_Total_m1B253B034C359095DB46C575D844B3606C4CD59B (void);
// 0x0000049F System.Void CSStoreCoinPack::SetPrice(System.String)
extern void CSStoreCoinPack_SetPrice_m0C6C5EB9265655BF8CF6C0267F494DC590D6C1B5 (void);
// 0x000004A0 System.Void CSStoreCoinPack::.ctor()
extern void CSStoreCoinPack__ctor_mB868845EAE6CFFB50D3AE92679627E8944235888 (void);
// 0x000004A1 System.Single CSBankCoinPanel::get_bank()
extern void CSBankCoinPanel_get_bank_m24152567A2ECB0A1691EE87BBBADFB1C012BE927 (void);
// 0x000004A2 System.Void CSBankCoinPanel::set_bank(System.Single)
extern void CSBankCoinPanel_set_bank_m41E22A92A9EE3312657C7E28C9E5DF5BAB10E31D (void);
// 0x000004A3 System.Void CSBankCoinPanel::OnDestroy()
extern void CSBankCoinPanel_OnDestroy_mF9428626D71EE3F4A75E589DF3943DB8B11A248C (void);
// 0x000004A4 System.Void CSBankCoinPanel::Awake()
extern void CSBankCoinPanel_Awake_m270A65148C4664AECAE4235670A37A5BBFC187CB (void);
// 0x000004A5 System.Void CSBankCoinPanel::OnEnable()
extern void CSBankCoinPanel_OnEnable_m3FC2F0867CAA19DBEB50A7AE0F70A2732ABB7E54 (void);
// 0x000004A6 System.Void CSBankCoinPanel::OnDisable()
extern void CSBankCoinPanel_OnDisable_m1E41C177AD6E4A2FE8F68061845B3C1DDE20B88A (void);
// 0x000004A7 System.String CSBankCoinPanel::Format(System.Single)
extern void CSBankCoinPanel_Format_mEE83D02FF2E63F6BDCE0B766E8CB011C55C3FCA2 (void);
// 0x000004A8 System.Void CSBankCoinPanel::Add(System.Single,System.Boolean)
extern void CSBankCoinPanel_Add_mE45C4DB12F4A5069D2E48ED0ADAF075ACD0279EE (void);
// 0x000004A9 System.Void CSBankCoinPanel::Add(System.Single,UnityEngine.RectTransform)
extern void CSBankCoinPanel_Add_mC3890EFFB70AB1CE7BC7341E93CF86663C2C5EBE (void);
// 0x000004AA System.Void CSBankCoinPanel::AddWithAnimation(System.Single,System.Single)
extern void CSBankCoinPanel_AddWithAnimation_m8FD74B72F1C55AF73BCE2A3DDAA1F772E36A8AB2 (void);
// 0x000004AB System.Single CSBankCoinPanel::AddParticle(UnityEngine.RectTransform)
extern void CSBankCoinPanel_AddParticle_m7142EEA0E90D1522959D54A260313B347517F56A (void);
// 0x000004AC LTDescr CSBankCoinPanel::LabelAction(UnityEngine.UI.Text,System.Single,System.Single,System.Single)
extern void CSBankCoinPanel_LabelAction_m532287B54B3AE5C8A35A89DD3F54ABB674CE9523 (void);
// 0x000004AD System.Void CSBankCoinPanel::ValueChanged()
extern void CSBankCoinPanel_ValueChanged_m20B0F3448065713991C98237D3563E24145B25A2 (void);
// 0x000004AE System.Void CSBankCoinPanel::HandleSuccessPurchase(UnityEngine.Purchasing.Product,CSIAPProduct)
extern void CSBankCoinPanel_HandleSuccessPurchase_m07A9C46703B740C37EC331E08C2B5C8A787FF415 (void);
// 0x000004AF System.Void CSBankCoinPanel::.ctor()
extern void CSBankCoinPanel__ctor_m052C20441B1B81048ACD82549A5472BBB63A001F (void);
// 0x000004B0 System.Void CSBankCoinPanel/<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_mA2F4BDCBF8F56ABB7627CA5B38298F78A326363E (void);
// 0x000004B1 System.Void CSBankCoinPanel/<>c__DisplayClass18_0::<LabelAction>b__0(System.Single)
extern void U3CU3Ec__DisplayClass18_0_U3CLabelActionU3Eb__0_m11FED61E17A2F93FAAF0BD802238843E936B50DB (void);
// 0x000004B2 System.Int32 CSBottomPanel::get_totalBet()
extern void CSBottomPanel_get_totalBet_m093E370E8262F272C9A1B2F6553F82B579071D65 (void);
// 0x000004B3 System.Void CSBottomPanel::set_totalBet(System.Int32)
extern void CSBottomPanel_set_totalBet_mD4167C838C6D539B6186A33B2F7087644A37C5F6 (void);
// 0x000004B4 System.Single CSBottomPanel::get_win()
extern void CSBottomPanel_get_win_m5DD4483D02E2B1E09A570CCC7BA3B4F3A2C7FBC8 (void);
// 0x000004B5 System.Void CSBottomPanel::set_win(System.Single)
extern void CSBottomPanel_set_win_m80D0D6427B05D7BD14FB6400F21C9AFEDED5683C (void);
// 0x000004B6 System.Boolean CSBottomPanel::get_enableSpin()
extern void CSBottomPanel_get_enableSpin_m8B4ECFE9105EA904DB3608F717AE17F2021A59B8 (void);
// 0x000004B7 System.Void CSBottomPanel::set_enableSpin(System.Boolean)
extern void CSBottomPanel_set_enableSpin_mC42DC57E958B425D2578EA23C0386D9A18E62E03 (void);
// 0x000004B8 System.Boolean CSBottomPanel::get_CanSpin()
extern void CSBottomPanel_get_CanSpin_m8F1F80239EE36062E0E2164D4764CD299B96ABC5 (void);
// 0x000004B9 System.Void CSBottomPanel::Awake()
extern void CSBottomPanel_Awake_mB36710219902D7239E0D5D0282F4B750FF1122CC (void);
// 0x000004BA System.Void CSBottomPanel::Start()
extern void CSBottomPanel_Start_m28B168C349C6F0C4400ADBDDE8B05FD55CF09BF7 (void);
// 0x000004BB System.Void CSBottomPanel::Loaded()
extern void CSBottomPanel_Loaded_mF5255318B41F972F8FCF59915572CE21CF853FAB (void);
// 0x000004BC System.Void CSBottomPanel::OnChangeMultipiler(System.Int32)
extern void CSBottomPanel_OnChangeMultipiler_m6C1CE69BD9DD498280B52CAB9C51E5B7028EB16C (void);
// 0x000004BD System.Void CSBottomPanel::OnBetMax()
extern void CSBottomPanel_OnBetMax_mD68B57DBB8AD08ED5CBF0A346AD870690220817F (void);
// 0x000004BE System.Void CSBottomPanel::UpdateBet(System.Int32)
extern void CSBottomPanel_UpdateBet_mA8AEFA77EC682F4DB7ED346114721C2996DA0A11 (void);
// 0x000004BF System.Void CSBottomPanel::WinAmount(System.Single)
extern void CSBottomPanel_WinAmount_mF55918594F71D8A98185BCB33AF89E2DAF4B56EA (void);
// 0x000004C0 System.Void CSBottomPanel::AddXP()
extern void CSBottomPanel_AddXP_m74BD09613BF3FDE16B670C3CACA29C936A097BDF (void);
// 0x000004C1 System.Void CSBottomPanel::SetEnable(System.Boolean)
extern void CSBottomPanel_SetEnable_mA161F25F8F7C5A7729573BC0E9F478E4B67D1BAD (void);
// 0x000004C2 System.Void CSBottomPanel::BetLabelAnimate(System.Single,System.Single)
extern void CSBottomPanel_BetLabelAnimate_mDE5EC216CAD5ACB4519773330BFE434C5C142C72 (void);
// 0x000004C3 System.Void CSBottomPanel::.ctor()
extern void CSBottomPanel__ctor_m2898CECCECC096FFC830070E52847AA949B87D4B (void);
// 0x000004C4 System.Void CSBottomPanel::<BetLabelAnimate>b__33_0(System.Single)
extern void CSBottomPanel_U3CBetLabelAnimateU3Eb__33_0_m940DA0547B5573DBEA603D11CC18C3545799AE01 (void);
// 0x000004C5 System.Int32 CSCardValue::get_rankIndex()
extern void CSCardValue_get_rankIndex_m174802D6FCD42BE5D4EF8CB18F1526F2F050F1AF (void);
// 0x000004C6 System.Int32 CSCardValue::get_suitIndex()
extern void CSCardValue_get_suitIndex_m781E7F614976BADE99E6284DABCA108E380ACFB4 (void);
// 0x000004C7 CSCardValue CSCardValue::get_random()
extern void CSCardValue_get_random_m7947AAC2AC29752532FB01C351BF50BF9119F1E3 (void);
// 0x000004C8 System.Void CSCardValue::.ctor(CSSuit,CSRank)
extern void CSCardValue__ctor_m8E30CBB7352115D6A1CD8DF7E4C2DB28AFB5F381 (void);
// 0x000004C9 System.Boolean CSCardValue::op_Equality(CSCardValue,CSCardValue)
extern void CSCardValue_op_Equality_mBF0F4DAEC09EB6F5F173C03FE896B3C5F5D86FD8 (void);
// 0x000004CA System.Boolean CSCardValue::op_Inequality(CSCardValue,CSCardValue)
extern void CSCardValue_op_Inequality_m718147A0B83EBF4D2A5BE2FF44580FD43CE02EDF (void);
// 0x000004CB System.Boolean CSCardValue::Equals(System.Object)
extern void CSCardValue_Equals_m76AD0CA57CDB069290DE8B8D56217FCBB3F9DF06 (void);
// 0x000004CC System.Int32 CSCardValue::GetHashCode()
extern void CSCardValue_GetHashCode_m38131925AA54B1CD1AF9ECE3514E68C48576D5C6 (void);
// 0x000004CD System.String CSCardValue::ToString()
extern void CSCardValue_ToString_m6E389700BF8EE2F6B660F1216EDE3DD029719E7C (void);
// 0x000004CE System.Void CSCard::add_selectedEvent(System.Action`1<CSCard>)
extern void CSCard_add_selectedEvent_mE3210C6427006E5C8822E2B58F579B4372E9094A (void);
// 0x000004CF System.Void CSCard::remove_selectedEvent(System.Action`1<CSCard>)
extern void CSCard_remove_selectedEvent_mA3D0A8F0DE543EE4722DF3DC06908D4D9A26E558 (void);
// 0x000004D0 CSCardData CSCard::get_data()
extern void CSCard_get_data_mB1D794CECF31FFA7C826B7D5D7B025E38B9DF566 (void);
// 0x000004D1 System.Boolean CSCard::get_flip()
extern void CSCard_get_flip_m19E1496D8E459E5C33C999EB0A9FD4386A4B328C (void);
// 0x000004D2 System.Void CSCard::set_flip(System.Boolean)
extern void CSCard_set_flip_m402C531B62EE0248B0F6AAED7448C21C160425E2 (void);
// 0x000004D3 System.Boolean CSCard::get_interactible()
extern void CSCard_get_interactible_m5F23DAD84861A366292E55D4C02473F465B990B1 (void);
// 0x000004D4 System.Void CSCard::set_interactible(System.Boolean)
extern void CSCard_set_interactible_m58EA3366729B183CCCA4D60058BFE1196EE73E17 (void);
// 0x000004D5 CSCardValue CSCard::get_cardValue()
extern void CSCard_get_cardValue_m0BC50B6CCB4D7D37F6CFCD28A1A5312F9B95C1D0 (void);
// 0x000004D6 System.Void CSCard::set_cardValue(CSCardValue)
extern void CSCard_set_cardValue_m479074ACAB45EA9ED6B680D1E32017C73CFEACE7 (void);
// 0x000004D7 System.Void CSCard::Awake()
extern void CSCard_Awake_mA4F152E79E56949FD70B6EB566618351F297176B (void);
// 0x000004D8 System.Void CSCard::OnDestroy()
extern void CSCard_OnDestroy_m7D45513512F201325B4ACB3464E95E9A16C055C7 (void);
// 0x000004D9 System.Void CSCard::LoadWitValue(CSCardValue,System.Boolean,System.Action`1<CSCard>)
extern void CSCard_LoadWitValue_mF99DDFA4EBD78295F6F4AE944B7264FBB7397169 (void);
// 0x000004DA System.Void CSCard::UnsubscribeEvent()
extern void CSCard_UnsubscribeEvent_mBA16E1D9E5F0F972EDCCC9A3BB894C8DE06BCA7C (void);
// 0x000004DB System.Void CSCard::SetFlip(System.Boolean,System.Boolean)
extern void CSCard_SetFlip_m2741DD3D1ACDBE9EC0869C2FB086D937E59511F4 (void);
// 0x000004DC System.Void CSCard::Flip()
extern void CSCard_Flip_mCB639228B0E3A626F50F3AFD91E982FEA6DF28F6 (void);
// 0x000004DD CSCardData CSCard::DataForValue(CSCardValue)
extern void CSCard_DataForValue_mBFAB94DF5014FB59293DDAEC56EC0AC327A16D93 (void);
// 0x000004DE System.Void CSCard::OnClick()
extern void CSCard_OnClick_m29DBE87D132A8B6A6FBC3AD3DFFE7F9749C61F70 (void);
// 0x000004DF System.Void CSCard::.ctor()
extern void CSCard__ctor_mCD214CC7FCF5C0D34116A943C28939FCCCC1A9CE (void);
// 0x000004E0 System.Void CSCardData::.ctor()
extern void CSCardData__ctor_mED7773B7F26A003EB9ECB8BFCE63F6E85B8E991C (void);
// 0x000004E1 System.Single CSExperiencePanel::get_xp()
extern void CSExperiencePanel_get_xp_mD425CE6C47F4836AD046F9A315A92BBD4CB9576B (void);
// 0x000004E2 System.Void CSExperiencePanel::set_xp(System.Single)
extern void CSExperiencePanel_set_xp_m073074467BE1F4B5995938FA26AD2A54A8BDF41B (void);
// 0x000004E3 System.Int32 CSExperiencePanel::get_level()
extern void CSExperiencePanel_get_level_m913EF158F992EBA1F3FC13A88FBE8DA1596DD434 (void);
// 0x000004E4 System.Void CSExperiencePanel::set_level(System.Int32)
extern void CSExperiencePanel_set_level_mBC773657A11690624153B556E75AF589C61433BA (void);
// 0x000004E5 System.Void CSExperiencePanel::Awake()
extern void CSExperiencePanel_Awake_m8F3BE4F23B57EF5632E2526524BDFBDC0393025D (void);
// 0x000004E6 System.Void CSExperiencePanel::AddValue(System.Single,UnityEngine.RectTransform)
extern void CSExperiencePanel_AddValue_m7EC3DC5BF89F8D399FC8AC1511B425A3B03904FD (void);
// 0x000004E7 System.Void CSExperiencePanel::AnimateProgressWith(System.Single,System.Int32,System.Single,System.Single)
extern void CSExperiencePanel_AnimateProgressWith_m2DA1693DB1D5914FC67F0716E3327CBCDDF5477F (void);
// 0x000004E8 System.Void CSExperiencePanel::TrueValues(System.Single)
extern void CSExperiencePanel_TrueValues_m4C4863830B9056DA9D9CAF1D291A1F8EFBAA64FE (void);
// 0x000004E9 System.Void CSExperiencePanel::UpdateBar(System.Single)
extern void CSExperiencePanel_UpdateBar_m5FD4059533E51A4321753545AC7133874CCF8F60 (void);
// 0x000004EA System.Single CSExperiencePanel::MaxForLevel(System.Int32)
extern void CSExperiencePanel_MaxForLevel_m2F9A5EA43F6B356460712925E18B0E1C8CBAA00E (void);
// 0x000004EB System.Single CSExperiencePanel::AddParticle(UnityEngine.RectTransform)
extern void CSExperiencePanel_AddParticle_mF5786BBC0DBAFD349425B901EA14B20C9A81DCD8 (void);
// 0x000004EC System.Void CSExperiencePanel::Alert()
extern void CSExperiencePanel_Alert_mC883BA4BDF127E5754BA509443CE210C72536173 (void);
// 0x000004ED System.Void CSExperiencePanel::.ctor()
extern void CSExperiencePanel__ctor_mCD9DC37A8312F2F8334F3D4B06F2D64551472224 (void);
// 0x000004EE System.Void CSExperiencePanel/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_m4C79D65771BF1D650104128A21491051D25DB9F8 (void);
// 0x000004EF System.Void CSExperiencePanel/<>c__DisplayClass21_0::<AnimateProgressWith>b__0(System.Single)
extern void U3CU3Ec__DisplayClass21_0_U3CAnimateProgressWithU3Eb__0_m1E45498D3E59582D06901EBC23D67EE9B7708E56 (void);
// 0x000004F0 System.Void CSExperiencePanel/<>c__DisplayClass21_0::<AnimateProgressWith>b__1()
extern void U3CU3Ec__DisplayClass21_0_U3CAnimateProgressWithU3Eb__1_m52390DDB9EB5BAF79F66543DCB0B987E427CE338 (void);
// 0x000004F1 System.Void CSFindMatchManager::Awake()
extern void CSFindMatchManager_Awake_mCD552CD24E5411B32494071396365B46E25EF59C (void);
// 0x000004F2 System.Collections.Generic.List`1<CSPayline> CSFindMatchManager::GetPayLines(CSLine[],System.Func`2<CSCell,CSSymbol>,System.Boolean)
extern void CSFindMatchManager_GetPayLines_m340041209B635BB4522C7A8EB04B7026ADFD0CD5 (void);
// 0x000004F3 CSScatterPlayLine CSFindMatchManager::CheckScatter(System.Func`2<CSCell,CSSymbol>)
extern void CSFindMatchManager_CheckScatter_m4034889EA47DC1041CBC56001F78FD02B5A3086F (void);
// 0x000004F4 System.Single CSFindMatchManager::WinAmountForPaylines(System.Collections.Generic.List`1<CSPayline>,System.Single)
extern void CSFindMatchManager_WinAmountForPaylines_mFBE1CF162CF7DB3F743C7F56F860847FE29C107D (void);
// 0x000004F5 System.Void CSFindMatchManager::.ctor()
extern void CSFindMatchManager__ctor_mD40C29B78B0CC8ABA3FD6D0709815B776A254F81 (void);
// 0x000004F6 System.Void CSFindMatchManager::.cctor()
extern void CSFindMatchManager__cctor_m842EF22F7A29C0F49E2C23A9FD560568854883E1 (void);
// 0x000004F7 System.Int32 CSListNavigation`1::get_idx()
// 0x000004F8 System.Void CSListNavigation`1::set_idx(System.Int32)
// 0x000004F9 System.Void CSListNavigation`1::.ctor(System.Collections.Generic.List`1<T>)
// 0x000004FA System.Int32 CSListNavigation`1::CSInfinScrollValue(System.Int32,System.Int32,System.Int32)
// 0x000004FB T CSListNavigation`1::get_Next()
// 0x000004FC T CSListNavigation`1::get_Previous()
// 0x000004FD T CSListNavigation`1::get_Current()
// 0x000004FE System.Int32 CSPayline::get_count()
extern void CSPayline_get_count_m20BF3B27EEE60C5D44A244C7D6C6872A1758FEA6 (void);
// 0x000004FF System.Void CSPayline::.ctor(CSLine)
extern void CSPayline__ctor_m13DDF3E47BE7ECAE209C7D4756607A6E4C41F4D4 (void);
// 0x00000500 System.Boolean CSPayline::AddSymbol(CSSymbol)
extern void CSPayline_AddSymbol_m824E7CB42F9B21A23A947019395790221332B6EE (void);
// 0x00000501 System.Boolean CSPayline::isWin()
extern void CSPayline_isWin_m89A4E707AF784D572FC062830CE5E2895DBC0EF6 (void);
// 0x00000502 CSRule CSPayline::GetRule()
extern void CSPayline_GetRule_mD3986A63337B92305C6F8473795AF4F21F573CD9 (void);
// 0x00000503 System.Void CSScatterPlayLine::.ctor()
extern void CSScatterPlayLine__ctor_m83CC8AC66063EF4DECFAFD8A929B345234829272 (void);
// 0x00000504 System.Boolean CSScatterPlayLine::AddSymbol(CSSymbol)
extern void CSScatterPlayLine_AddSymbol_m6EDE072B70EF7FF273A3BCF9CF149067D92D6673 (void);
// 0x00000505 System.Boolean CSFreeGamePanel::get_freeSpinEnable()
extern void CSFreeGamePanel_get_freeSpinEnable_m7AFCC7BDC1E3DE823AA3F8F79E11F0ACED510690 (void);
// 0x00000506 System.Void CSFreeGamePanel::set_freeSpinEnable(System.Boolean)
extern void CSFreeGamePanel_set_freeSpinEnable_m9F67FB760DFA56F835786D49024CA36F3AA1C701 (void);
// 0x00000507 System.Int32 CSFreeGamePanel::get_freeSpins()
extern void CSFreeGamePanel_get_freeSpins_m07D2B45C7322CEDADBAD4C4D2B354FA60E8CC152 (void);
// 0x00000508 System.Void CSFreeGamePanel::set_freeSpins(System.Int32)
extern void CSFreeGamePanel_set_freeSpins_mDCE177E387BE519D66D45BE4C6C1F95A99C032F3 (void);
// 0x00000509 System.Single CSFreeGamePanel::get_win()
extern void CSFreeGamePanel_get_win_m345B7CF9A37F34BF3CEFF839BB848B8988DA1FBB (void);
// 0x0000050A System.Void CSFreeGamePanel::set_win(System.Single)
extern void CSFreeGamePanel_set_win_m08FB17A566E3FE6D3CAD798A7D42DD8812C8681F (void);
// 0x0000050B System.Void CSFreeGamePanel::SetWin(System.Single)
extern void CSFreeGamePanel_SetWin_m9A78EF73492E927AFC16F1912614C212B03376C7 (void);
// 0x0000050C System.Void CSFreeGamePanel::.ctor()
extern void CSFreeGamePanel__ctor_mBD3DDA64503D55A4D12E32E7DD238981A081AE88 (void);
// 0x0000050D System.Void CSGame::Start()
extern void CSGame_Start_mEB2ECCB2319A8132CE5BA3785485C39CFF9373F1 (void);
// 0x0000050E System.Void CSGame::Update()
extern void CSGame_Update_mFAC8186AB5DCFDA9E8D4521A13E7378A22CFFDF0 (void);
// 0x0000050F System.Void CSGame::.ctor()
extern void CSGame__ctor_mCD5EE8778561B48F4AA37F11B2B73CE6D6A708E6 (void);
// 0x00000510 System.Void CSGameManager::Awake()
extern void CSGameManager_Awake_m33132A5E5525E00D5997C2FF15AEA48A16722A91 (void);
// 0x00000511 System.Void CSGameManager::Loaded()
extern void CSGameManager_Loaded_m59DA71BE129523EEFA6B9DA31D72333DD04D4DDD (void);
// 0x00000512 System.Void CSGameManager::Rate()
extern void CSGameManager_Rate_mA33F5F3AFE88BC27E76AE9B6650CDA1D0386A854 (void);
// 0x00000513 System.Void CSGameManager::.ctor()
extern void CSGameManager__ctor_mEFB17626D6DEB11399BD785F6E4EE0DB4D9F32F2 (void);
// 0x00000514 System.Void CSGameManager::.cctor()
extern void CSGameManager__cctor_m64897BF8218413B4BB9E45B29D8D4F982CB1EA31 (void);
// 0x00000515 System.Boolean CSGameSettings::get_music()
extern void CSGameSettings_get_music_m6C5958C2DD63042B981B00FB52299190549099E7 (void);
// 0x00000516 System.Void CSGameSettings::set_music(System.Boolean)
extern void CSGameSettings_set_music_m2F7B556AD5CAB49A260FA8A3DFB007420AE6B3D1 (void);
// 0x00000517 System.Boolean CSGameSettings::get_sound()
extern void CSGameSettings_get_sound_mCB37E0C027ECBF65463A865080DE5C9E8D90D8C9 (void);
// 0x00000518 System.Void CSGameSettings::set_sound(System.Boolean)
extern void CSGameSettings_set_sound_mA084AE76541095FE9D3A03CE1D13349B37ED12DA (void);
// 0x00000519 System.Single CSGameSettings::get_highscore()
extern void CSGameSettings_get_highscore_mE91AC0C4695BA7C8FCBDF9495E1759AD15F5A2F7 (void);
// 0x0000051A System.Void CSGameSettings::set_highscore(System.Single)
extern void CSGameSettings_set_highscore_mD3BA9081771F760E4494F6A4060E6E7A7817DDEF (void);
// 0x0000051B System.UInt32 CSGameSettings::get_playcount()
extern void CSGameSettings_get_playcount_mF3B57D21CC8A61F98CD2FA2AAA2B2D34D69FED94 (void);
// 0x0000051C System.Void CSGameSettings::set_playcount(System.UInt32)
extern void CSGameSettings_set_playcount_mF0CD183759A6FD40D88CFA579487EAE2278342DE (void);
// 0x0000051D System.Boolean CSGameSettings::get_ads()
extern void CSGameSettings_get_ads_mDE0051A3BE321D385D913D37F3A12B2D1B2612F0 (void);
// 0x0000051E System.Void CSGameSettings::set_ads(System.Boolean)
extern void CSGameSettings_set_ads_m4BFBA87AC1050FB37A966E88E5A3467D9E876C39 (void);
// 0x0000051F System.Single CSGameSettings::get_coins()
extern void CSGameSettings_get_coins_mBA84A4FC5F8197967405115B24FEF320FEE81CAD (void);
// 0x00000520 System.Void CSGameSettings::set_coins(System.Single)
extern void CSGameSettings_set_coins_mCD85255057007DA7B16C12BFF239C596C48E601A (void);
// 0x00000521 System.Single CSGameSettings::get_xp()
extern void CSGameSettings_get_xp_m4DB0F998D7A286F50A98368851F3B39240A5B215 (void);
// 0x00000522 System.Void CSGameSettings::set_xp(System.Single)
extern void CSGameSettings_set_xp_m89478E55C35EF73E9C6ADB05FA32E76A03D467FF (void);
// 0x00000523 System.Int32 CSGameSettings::get_level()
extern void CSGameSettings_get_level_mAB49D065925159FEA6E2BF77D520C9566837E950 (void);
// 0x00000524 System.Void CSGameSettings::set_level(System.Int32)
extern void CSGameSettings_set_level_m443765A0ECDD02AAE9E67578CBCF021AEFD59D46 (void);
// 0x00000525 System.String CSGameSettings::get_selectedTheme()
extern void CSGameSettings_get_selectedTheme_mF83E3620F37C51B458C0B329C0D076F4A9E6E975 (void);
// 0x00000526 System.Void CSGameSettings::set_selectedTheme(System.String)
extern void CSGameSettings_set_selectedTheme_m90C50EEC8D04163AAD116443CCD4635F12AB0F33 (void);
// 0x00000527 System.Int32 CSGameSettings::get_classicSevenBetStep()
extern void CSGameSettings_get_classicSevenBetStep_m7A8633D0E0E555873FF38D74E1B3CAF875B34063 (void);
// 0x00000528 System.Void CSGameSettings::set_classicSevenBetStep(System.Int32)
extern void CSGameSettings_set_classicSevenBetStep_mA593E6C180F21DA0FCF21EFCB3505BD3AD05858C (void);
// 0x00000529 System.Single CSGameSettings::get_volume()
extern void CSGameSettings_get_volume_m27B40F2ED0AA71A816787438A0B5E429BD982DD9 (void);
// 0x0000052A System.Void CSGameSettings::set_volume(System.Single)
extern void CSGameSettings_set_volume_mE6CFD8AE3887D2DA71FE010DBF48AB3C2625EB93 (void);
// 0x0000052B System.Void CSGameSettings::Awake()
extern void CSGameSettings_Awake_m33C590D01F3CF8A1C8CF87B176CD3E8F31CD9FDE (void);
// 0x0000052C System.String CSGameSettings::FilePath()
extern void CSGameSettings_FilePath_mFF476BF74007CDF25CDEEFF849DDE2A4E792D83A (void);
// 0x0000052D System.Void CSGameSettings::Save()
extern void CSGameSettings_Save_m0C1AF49D3D9F0D62458D21EDCBE6B49844BA5981 (void);
// 0x0000052E System.Void CSGameSettings::Load()
extern void CSGameSettings_Load_m91A3C4146B7EB2B89F78160F99E2D4CCF44AD09F (void);
// 0x0000052F System.Void CSGameSettings::Reset()
extern void CSGameSettings_Reset_mCC529A51E25783047D98EAA9FAA26B627C0CF6C9 (void);
// 0x00000530 System.Void CSGameSettings::Delete()
extern void CSGameSettings_Delete_m639DBA0150BEB12F32AF847D9DF00E1E94136618 (void);
// 0x00000531 System.IO.FileStream CSGameSettings::GetFileStream()
extern void CSGameSettings_GetFileStream_m59DEB3B3E96936C29F98A78386C21C208C6D7090 (void);
// 0x00000532 System.Void CSGameSettings::AddTimer(CSTimer)
extern void CSGameSettings_AddTimer_m4F9DEC3794AF6C345D0234BDCED42AF8B25E3900 (void);
// 0x00000533 System.Void CSGameSettings::RemoveTimer(CSTimer)
extern void CSGameSettings_RemoveTimer_m7A1EF2575A9D4DB465B77B1B00C83F4098ADF22B (void);
// 0x00000534 System.Void CSGameSettings::RemoveTimer(System.String)
extern void CSGameSettings_RemoveTimer_m136FBC88B9E05E3998C17FEDFE25031953B31887 (void);
// 0x00000535 System.Boolean CSGameSettings::AvalibleDailyReward()
extern void CSGameSettings_AvalibleDailyReward_m75AD23B5F6AC677FFFDC0EECC85EA6F63EAA39F3 (void);
// 0x00000536 System.Void CSGameSettings::DailyRewardCollected()
extern void CSGameSettings_DailyRewardCollected_mEB621C1286A50AE3D8435222B6154F43D5826A0C (void);
// 0x00000537 System.Int32 CSGameSettings::RewardDay(System.Int32)
extern void CSGameSettings_RewardDay_m097532924309FCBAC85F038AD6569DAA5924C141 (void);
// 0x00000538 System.Void CSGameSettings::.ctor()
extern void CSGameSettings__ctor_m73EC3C87A262AA1B72CFA808DDCEE8FFECAD01A4 (void);
// 0x00000539 System.Void CSGameSettings::.cctor()
extern void CSGameSettings__cctor_m042AF71218B0970319CC341D9DDDA608B2DFC8CA (void);
// 0x0000053A System.Int32 CSGameData::get_TotalInstallDays()
extern void CSGameData_get_TotalInstallDays_mAAEACE7101EBCF8BAB72BBBDE35CE6030641DB4A (void);
// 0x0000053B System.Void CSGameData::.ctor()
extern void CSGameData__ctor_m1FE4D0D28AD568D686691C37DC2BCED4DE5D07D5 (void);
// 0x0000053C System.Void CSIAPManager::Awake()
extern void CSIAPManager_Awake_m31D196FB9E0AA929DC5B1F52CCD8F705F02F4ADF (void);
// 0x0000053D System.Void CSIAPManager::Loaded()
extern void CSIAPManager_Loaded_m796788F3CCBA46C3668F4862FAEAC307D839CFF8 (void);
// 0x0000053E System.Void CSIAPManager::InitializePurchasing(System.Collections.Generic.List`1<CSIAPProduct>)
extern void CSIAPManager_InitializePurchasing_mBF324D0C9B6EBFAEF22791285D7F8883F1E21B63 (void);
// 0x0000053F System.Boolean CSIAPManager::IsInitialized()
extern void CSIAPManager_IsInitialized_m7A65CC1EA47DB11065E5E574055347D4916C3525 (void);
// 0x00000540 UnityEngine.Purchasing.Product CSIAPManager::ProductForId(System.String)
extern void CSIAPManager_ProductForId_m82CC2B62AE28B58A4AAD62F0199E30ADCCDC9023 (void);
// 0x00000541 System.String CSIAPManager::PriceForProductId(System.String)
extern void CSIAPManager_PriceForProductId_m25E30B70CFFEDBEB7486B6AEBF187EA7761980FD (void);
// 0x00000542 System.String CSIAPManager::TitleForProductId(System.String)
extern void CSIAPManager_TitleForProductId_m707B5468D8B11F9A4C1C01A3A3E1E41D25ACADD3 (void);
// 0x00000543 System.String CSIAPManager::DescriptionForProductId(System.String)
extern void CSIAPManager_DescriptionForProductId_m8B8F8BFA85E7DE833347006BEA8B0F225ECFBD30 (void);
// 0x00000544 System.Void CSIAPManager::Purchase(System.String)
extern void CSIAPManager_Purchase_m32891282F3B2B6DDE88C07CBD201557DA3ADFAE5 (void);
// 0x00000545 System.Void CSIAPManager::RestorePurchases()
extern void CSIAPManager_RestorePurchases_m0867EB885E6FD3B613543493B0BB504140BAA464 (void);
// 0x00000546 System.Void CSIAPManager::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern void CSIAPManager_OnInitialized_mD318EBE6089F47973CAC686BF9C7B369816F90EA (void);
// 0x00000547 System.Void CSIAPManager::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern void CSIAPManager_OnInitializeFailed_mBB958AD161E4CE7B3241BD4BDCDDAED9B29084A8 (void);
// 0x00000548 UnityEngine.Purchasing.PurchaseProcessingResult CSIAPManager::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern void CSIAPManager_ProcessPurchase_mB5541CD4E3424E546A02CDE99AFE54407B7F2E63 (void);
// 0x00000549 System.Void CSIAPManager::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern void CSIAPManager_OnPurchaseFailed_m8A44AB877756B820A572E474631ECA81AAF6A190 (void);
// 0x0000054A CSIAPProduct CSIAPManager::ProductDataForId(System.String)
extern void CSIAPManager_ProductDataForId_m15805DEAFF797925297B266EA1B424179F3A69C0 (void);
// 0x0000054B System.Void CSIAPManager::.ctor()
extern void CSIAPManager__ctor_m48BA8C87496613CCAF5CD7FB705891FB015EA6DE (void);
// 0x0000054C System.Void CSIAPManager::.cctor()
extern void CSIAPManager__cctor_mB32C84838320161BAE52FC75FF9387861D951D6B (void);
// 0x0000054D System.Void CSIAPManager/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_mC60FC8983D5E34C4C2933209B500DE6695AE4D51 (void);
// 0x0000054E System.Void CSIAPManager/<>c__DisplayClass8_0::<InitializePurchasing>b__0(CSIAPProduct)
extern void U3CU3Ec__DisplayClass8_0_U3CInitializePurchasingU3Eb__0_mD1EF2034DA8EC9223ECA4403199B2C70028AB727 (void);
// 0x0000054F System.Void CSIAPManager/<>c::.cctor()
extern void U3CU3Ec__cctor_m5AB778FF883DA1B6C11583CE21E545DC1E9FD987 (void);
// 0x00000550 System.Void CSIAPManager/<>c::.ctor()
extern void U3CU3Ec__ctor_mA16CFC7CE83E88EF91B47ECD6C1F29E99A9D594C (void);
// 0x00000551 System.Void CSIAPManager/<>c::<RestorePurchases>b__15_0(System.Boolean)
extern void U3CU3Ec_U3CRestorePurchasesU3Eb__15_0_mDEB9F112DEDBCF6BD2337D223D8291E3247E7768 (void);
// 0x00000552 System.Void CSIAPProduct::.ctor()
extern void CSIAPProduct__ctor_mF89195342849DC27E3493282AD39A1999697666B (void);
// 0x00000553 System.Boolean CSInfo::get_active()
extern void CSInfo_get_active_mEF6E3DBB93C4AE596FC5E26DCFB6093FB38FBF77 (void);
// 0x00000554 System.Void CSInfo::set_active(System.Boolean)
extern void CSInfo_set_active_m19C2C36C880B55B3DEFD95977384A07D09DFB15D (void);
// 0x00000555 System.Void CSInfo::Awake()
extern void CSInfo_Awake_mE565412CB0DDB62749494DA62660326B533BE086 (void);
// 0x00000556 System.Void CSInfo::Appear(System.Single)
extern void CSInfo_Appear_mA2AEBB36B38C4501483D35DE3BE3CB249B68936B (void);
// 0x00000557 System.Void CSInfo::Disappear(System.Action)
extern void CSInfo_Disappear_mDE5FEE83D7E5DA843FF44ECC26C839B68E9214B7 (void);
// 0x00000558 LTDescr CSInfo::Scale(LeanTweenType)
extern void CSInfo_Scale_m4DC8079B904A477DF0182F2FAE9FB18FF166EBF0 (void);
// 0x00000559 LTDescr CSInfo::AlphaBackground(System.Single)
extern void CSInfo_AlphaBackground_m31AE7E6D7ED65F883465D8810DD21F459783FA40 (void);
// 0x0000055A LTDescr CSInfo::AlphaBoard(System.Single)
extern void CSInfo_AlphaBoard_m17FB003BB487FC0A6D2779C21B888C55B7A8B74D (void);
// 0x0000055B LTDescr CSInfo::Alpha(UnityEngine.UI.Image,System.Single,System.Single)
extern void CSInfo_Alpha_mC9A376A328D57B6074621EB71AF16EF90D40E888 (void);
// 0x0000055C System.Void CSInfo::CanvasStatus(System.Boolean)
extern void CSInfo_CanvasStatus_mC11C572DD6A86A13ADCAC6F74A3AAE7DE62A816C (void);
// 0x0000055D System.Void CSInfo::OnClose()
extern void CSInfo_OnClose_m4253B10FFF1FE1779916ADCFAEC6EE7199DFB880 (void);
// 0x0000055E System.Void CSInfo::.ctor()
extern void CSInfo__ctor_m93621DA1F5239B84A0814F72F9C1BE5781C52B60 (void);
// 0x0000055F System.Void CSInfo::<OnClose>b__19_0()
extern void CSInfo_U3COnCloseU3Eb__19_0_m824E1B514D6FC36FF3BC04C6C91781B6A7045E74 (void);
// 0x00000560 System.Void CSInfo/<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_m1284C7DAEE32533DCC25BB53E947155FBB83FF5A (void);
// 0x00000561 System.Void CSInfo/<>c__DisplayClass17_0::<Alpha>b__0(System.Single)
extern void U3CU3Ec__DisplayClass17_0_U3CAlphaU3Eb__0_mACB94DFB42FE881112FEE6352E48FAFAF267C83A (void);
// 0x00000562 System.Int32 CSLine::get_count()
extern void CSLine_get_count_m47FFD50F292A840B462119D912A9DC4FFFD87C94 (void);
// 0x00000563 CSCell CSLine::get_Item(System.Int32)
extern void CSLine_get_Item_mA2228DF5D06778B4D64A9FA3A7824E51FB7481DB (void);
// 0x00000564 System.Void CSLine::set_Item(System.Int32,CSCell)
extern void CSLine_set_Item_m1ED001A1329F3C2F20BA86DC4860362B87B8D6CF (void);
// 0x00000565 System.Void CSLine::.ctor()
extern void CSLine__ctor_mC368540ED8D5A4A58FB22688A02C04D9E3D37D29 (void);
// 0x00000566 System.Void CSMenuToggle::Awake()
extern void CSMenuToggle_Awake_m5D6C945391FBB82FEB8A1E8D215EFAA2A5A4821A (void);
// 0x00000567 System.Void CSMenuToggle::Update()
extern void CSMenuToggle_Update_m3FBB4185BD7F04DFCBC7B355AE449F5285B9BC1C (void);
// 0x00000568 System.Void CSMenuToggle::OnMiniSettings(System.Boolean)
extern void CSMenuToggle_OnMiniSettings_m982320A7D75AD7C50007CDD54B75BEC1F077E598 (void);
// 0x00000569 System.Boolean CSMenuToggle::PointerOverGameObject()
extern void CSMenuToggle_PointerOverGameObject_mEF4B1070B0F1FB7DDC8BB314008985AE2D5D0A75 (void);
// 0x0000056A System.Void CSMenuToggle::.ctor()
extern void CSMenuToggle__ctor_m9B906E8FFC2E616856EC0850B6E47F9FCD1237B4 (void);
// 0x0000056B CSSymbol CSReel::get_Item(System.Int32)
extern void CSReel_get_Item_m0D1A0D0371F47EA4911433B30ED0969B983CF8F4 (void);
// 0x0000056C System.Void CSReel::set_Item(System.Int32,CSSymbol)
extern void CSReel_set_Item_m247E1BC8E14A003ADDF3CC9BE101176D77C25A44 (void);
// 0x0000056D System.Void CSReel::Awake()
extern void CSReel_Awake_mFB1253946A129BA1E85D00CABEEEF53FC36849A7 (void);
// 0x0000056E System.Void CSReel::StartWithSize(UnityEngine.Vector2,System.Int32)
extern void CSReel_StartWithSize_m9A06EE5AD7BEFD6455479D66869C4AA64EC3EB22 (void);
// 0x0000056F System.Void CSReel::CreateSymbols(System.Int32)
extern void CSReel_CreateSymbols_mE8744AB99EAD2C8A63D4178FFD21167AB1E466EF (void);
// 0x00000570 CSSymbol CSReel::CreateSymbol(CSCell)
extern void CSReel_CreateSymbol_m0D43606DFC6531A9463434DAECCB456FD06D80DE (void);
// 0x00000571 UnityEngine.Vector3 CSReel::PositionForRow(System.Int32)
extern void CSReel_PositionForRow_m7B68B78BF88CB91EF6C2454CE2F533DE192CAF48 (void);
// 0x00000572 LTDescr CSReel::Animate(System.Action)
extern void CSReel_Animate_m77B4A6EEA709C814292157334829ABFC5CB51EE7 (void);
// 0x00000573 LTDescr CSReel::Parallax(System.Int32,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void CSReel_Parallax_m31BB619C3A783B07EB873E0CF3ED6B1477704C6C (void);
// 0x00000574 System.Void CSReel::.ctor()
extern void CSReel__ctor_m04D8EB0E3A28A27B46C1B978AA38B4169320BC77 (void);
// 0x00000575 System.Void CSReel/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m1FD5FC4F2E3014CF1B47272CED5C6D48EEAB133B (void);
// 0x00000576 System.Void CSReel/<>c__DisplayClass16_0::<Animate>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CAnimateU3Eb__0_m8634D0AA32D727C7AF99E760CFA8556468BDF231 (void);
// 0x00000577 System.Void CSReel/<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_m6A97AD9A19EBC8D1B40468BBF6F8B11F6EDCE04D (void);
// 0x00000578 System.Void CSReel/<>c__DisplayClass17_0::<Parallax>b__0(System.Single)
extern void U3CU3Ec__DisplayClass17_0_U3CParallaxU3Eb__0_m333130C50BE07CF57A2F131CD80FC1AFB78F02C3 (void);
// 0x00000579 System.Void CSReelAutoSpin::Awake()
extern void CSReelAutoSpin_Awake_mC338C06F7398CEEBFB625470B7DBD4B1052CC05F (void);
// 0x0000057A System.Void CSReelAutoSpin::Update()
extern void CSReelAutoSpin_Update_m6C23AB750886015F44FE13D33541EB4FFFE704CD (void);
// 0x0000057B System.Void CSReelAutoSpin::SwapSpinSprite(CSBottomPanel)
extern void CSReelAutoSpin_SwapSpinSprite_m4497828B0F201F4FEBCAF234091FE806AA62A328 (void);
// 0x0000057C System.Void CSReelAutoSpin::OnSpinDown()
extern void CSReelAutoSpin_OnSpinDown_m2BEECFE8570B00A33004632CCEB42EE3638622DF (void);
// 0x0000057D System.Void CSReelAutoSpin::OnSpinUp()
extern void CSReelAutoSpin_OnSpinUp_m53732E3FAA1A9CB35CF723DC808461FAAAB3EA7D (void);
// 0x0000057E System.Void CSReelAutoSpin::.ctor()
extern void CSReelAutoSpin__ctor_m8906225DF8D40446D5203F42A1F2AA7EB291CF32 (void);
// 0x0000057F System.Void CSReelRandom::.ctor(CSSymbolPercent[],CS2DBoolArray,System.Int32)
extern void CSReelRandom__ctor_mD48F72AD4073AFC6A710BBA6C2A3B308CF33EE3C (void);
// 0x00000580 CSSymbolType CSReelRandom::SmartRandomSymbol()
extern void CSReelRandom_SmartRandomSymbol_mF89F81DB10EDA68BEC909A42535EA945658E56C9 (void);
// 0x00000581 CSSymbolType CSReelRandom::RandomSymbol()
extern void CSReelRandom_RandomSymbol_m5E317A413D5E1AB62D395DF1F73212BBB05491F6 (void);
// 0x00000582 CSSymbolType CSReelRandom::TrueRandom()
extern void CSReelRandom_TrueRandom_mBDBFE494DA223D6C726B0516D97955490F4B0933 (void);
// 0x00000583 System.Void CSReelRandom::Test(System.Int32)
extern void CSReelRandom_Test_mFD8ABDD30354D485838846458767D678729680B9 (void);
// 0x00000584 System.Void IFreeGame::FreeGameValueChanged(System.Boolean)
// 0x00000585 System.Void CSReels::add_FreeGameValueChangedEvent(System.Action`1<System.Boolean>)
extern void CSReels_add_FreeGameValueChangedEvent_mCFB5F737B6F73CF588EA258B3462DEEAD518A16B (void);
// 0x00000586 System.Void CSReels::remove_FreeGameValueChangedEvent(System.Action`1<System.Boolean>)
extern void CSReels_remove_FreeGameValueChangedEvent_mDD1891856F71DCC2723A893C39E664B464F0EF24 (void);
// 0x00000587 System.Boolean CSReels::get_autoSpin()
extern void CSReels_get_autoSpin_mFC08FD8915EC98C2FE04A06892E430A7BD634520 (void);
// 0x00000588 System.Boolean CSReels::get_enable()
extern void CSReels_get_enable_m51C65BAC81136296489766D34662FEFD39EDFD1F (void);
// 0x00000589 System.Void CSReels::set_enable(System.Boolean)
extern void CSReels_set_enable_m84C300DA05F031D2AA0C6BC34848C2915FEF4978 (void);
// 0x0000058A System.Boolean CSReels::get_freeGame()
extern void CSReels_get_freeGame_m181D4F3845C0514B0EA7FEBF371BB7D175AA67FA (void);
// 0x0000058B System.Void CSReels::set_freeGame(System.Boolean)
extern void CSReels_set_freeGame_m4FF923704A53BCDFDFA8DDAA12D3ADF36C8498EE (void);
// 0x0000058C System.Void CSReels::Start()
extern void CSReels_Start_m0E1CFC714303D26D441FCF7C04BA2492442DAACF (void);
// 0x0000058D UnityEngine.Vector2 CSReels::GetTileSize()
extern void CSReels_GetTileSize_m5D896F8C6E5EFD58E82048055FE34CCFD25AFB67 (void);
// 0x0000058E System.Void CSReels::CreateReels()
extern void CSReels_CreateReels_m33363E74E22F4635B83A848CCC00A502351A269D (void);
// 0x0000058F CSReel CSReels::CreateReel(System.Int32)
extern void CSReels_CreateReel_m3C4AFF38E15FE67B6E00B12594BDA26E707ADFEF (void);
// 0x00000590 UnityEngine.Vector3 CSReels::PositionForColumn(System.Int32)
extern void CSReels_PositionForColumn_mF3D91DF92A3E7795BC15072602598FCC8CC4F9B7 (void);
// 0x00000591 System.Void CSReels::AddXPParticle()
extern void CSReels_AddXPParticle_m37863FC5069FD00027782445EB5DEDA432BEF13D (void);
// 0x00000592 System.Void CSReels::Spin()
extern void CSReels_Spin_m4E8663F295A7C321ED9474161B4C8D3D9AB7EF24 (void);
// 0x00000593 System.Void CSReels::FreeGameAutoSpin()
extern void CSReels_FreeGameAutoSpin_mEC3D230823A6AC6DA56E98AD34146C7FF6AA9848 (void);
// 0x00000594 System.Void CSReels::AutoSpin()
extern void CSReels_AutoSpin_m6F058B4ED405E429AF5E9E9B8D688196BB55793D (void);
// 0x00000595 System.Void CSReels::SetAutoSpin(System.Boolean)
extern void CSReels_SetAutoSpin_m3060282E4F3109987C1FA53AC56C0BCDB73D4600 (void);
// 0x00000596 System.Void CSReels::Animate(System.Action)
extern void CSReels_Animate_m5303C4047E76092BE546EA057D3DD722BAC55811 (void);
// 0x00000597 CSSymbol CSReels::CellToSymbol(CSCell)
extern void CSReels_CellToSymbol_m906DDA4FED856F3057859261814788099079F589 (void);
// 0x00000598 System.Void CSReels::FindPayLines()
extern void CSReels_FindPayLines_m9642F7F8B1A88D3037942886B20B152E709E18C7 (void);
// 0x00000599 System.Void CSReels::CalculateResult(System.Collections.Generic.List`1<CSPayline>)
extern void CSReels_CalculateResult_m1ABF13C6825CC4E185C243535AB9AA015C778D61 (void);
// 0x0000059A System.Void CSReels::BonusGame(System.Int32)
extern void CSReels_BonusGame_mFCC3783D3B8F2E01BF0DF9B983490681AD0230B7 (void);
// 0x0000059B System.Void CSReels::StartFreeGame()
extern void CSReels_StartFreeGame_m782F30E54A08F07B02C0F6E1D974E90D0619C530 (void);
// 0x0000059C System.Void CSReels::EndFreeGame()
extern void CSReels_EndFreeGame_m98C41CAFD68F96337A3F7E9074341E58B48451D8 (void);
// 0x0000059D System.Boolean CSReels::SetPage(System.Int32)
extern void CSReels_SetPage_mEC2697A55E100AC06CE29EA5CD0613F5D5955253 (void);
// 0x0000059E System.Void CSReels::UpdateFreeGamePanel(System.Boolean)
extern void CSReels_UpdateFreeGamePanel_mB3C94EF313BE68ED2FE025F21B6A1595EF7E6993 (void);
// 0x0000059F System.Void CSReels::ShowAlert(System.Single)
extern void CSReels_ShowAlert_m4390CAC2BA0581DC95A76A5CEB63592A9BF0E37C (void);
// 0x000005A0 System.Void CSReels::.ctor()
extern void CSReels__ctor_mB9D1FB29F68C534A19417ABF3301E1302FBD4C25 (void);
// 0x000005A1 System.Void CSReels::<Spin>b__31_0()
extern void CSReels_U3CSpinU3Eb__31_0_m761371C52E6624AA1C6F6A838D7D1733B7C9E96A (void);
// 0x000005A2 System.Void CSReelsAnimation::Awake()
extern void CSReelsAnimation_Awake_mC3DA79574CBB115813B6DFA0B956F22DB16042A8 (void);
// 0x000005A3 System.Void CSReelsAnimation::StartAnimatePlayLines(System.Collections.Generic.List`1<CSPayline>,System.Action)
extern void CSReelsAnimation_StartAnimatePlayLines_mF729BFCDCE8585782048D090E12D1C0E195D554F (void);
// 0x000005A4 System.Void CSReelsAnimation::StopAnimatePlayLines()
extern void CSReelsAnimation_StopAnimatePlayLines_m7A9D717D5ACC6ACDD3CBF6CEAAF9EA891701DD11 (void);
// 0x000005A5 System.Collections.IEnumerator CSReelsAnimation::AnimatePaylines(CSListNavigation`1<CSPayline>)
extern void CSReelsAnimation_AnimatePaylines_m26125ABF07A5731D264B3284B1166602D438D0D4 (void);
// 0x000005A6 System.Collections.IEnumerator CSReelsAnimation::AnimatePaylinesFreeGame(CSListNavigation`1<CSPayline>,System.Action)
extern void CSReelsAnimation_AnimatePaylinesFreeGame_m0D54CD7660FA8D4615026E2270390BEAA074FE86 (void);
// 0x000005A7 System.Void CSReelsAnimation::StartPayline(CSPayline,System.Single)
extern void CSReelsAnimation_StartPayline_m491F7BF956D438E7F03BA0D40D7A5B24421519D8 (void);
// 0x000005A8 System.Void CSReelsAnimation::ScatterPayline(CSScatterPlayLine)
extern void CSReelsAnimation_ScatterPayline_mCB70373DAC59DC2A1D5FBA3A9F8589A5601D41DA (void);
// 0x000005A9 System.Void CSReelsAnimation::StopPayline(CSPayline)
extern void CSReelsAnimation_StopPayline_m3FEF904BB259E71E56E35494418F38120DA339CC (void);
// 0x000005AA System.Void CSReelsAnimation::DestroyParticlePayline(CSPayline)
extern void CSReelsAnimation_DestroyParticlePayline_m791BD7145894F9F173152C3CCA36C3F71E0C5552 (void);
// 0x000005AB System.Void CSReelsAnimation::SetLineForPayline(CSPayline)
extern void CSReelsAnimation_SetLineForPayline_mB599E6F9AC87AFED3122EB795F3CC7F90D4EF594 (void);
// 0x000005AC System.Void CSReelsAnimation::.ctor()
extern void CSReelsAnimation__ctor_mFA06155A76A37769840250A4233D25CE20A13E64 (void);
// 0x000005AD System.Void CSReelsAnimation/<AnimatePaylines>d__7::.ctor(System.Int32)
extern void U3CAnimatePaylinesU3Ed__7__ctor_mD4D6AAB9746CCF351CE765334F0983A6DF5BF854 (void);
// 0x000005AE System.Void CSReelsAnimation/<AnimatePaylines>d__7::System.IDisposable.Dispose()
extern void U3CAnimatePaylinesU3Ed__7_System_IDisposable_Dispose_m6DE035D0BD06401C6712CBB42741F13B54AD248E (void);
// 0x000005AF System.Boolean CSReelsAnimation/<AnimatePaylines>d__7::MoveNext()
extern void U3CAnimatePaylinesU3Ed__7_MoveNext_mB452328A8D02BA925D034E0C296BBE0DB194FD5B (void);
// 0x000005B0 System.Object CSReelsAnimation/<AnimatePaylines>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePaylinesU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC798924ACE7A70C9EA55F6486E219B70EF63FDEB (void);
// 0x000005B1 System.Void CSReelsAnimation/<AnimatePaylines>d__7::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePaylinesU3Ed__7_System_Collections_IEnumerator_Reset_m36AB284FD72B087EB4D40A2EE181A17C948EBD01 (void);
// 0x000005B2 System.Object CSReelsAnimation/<AnimatePaylines>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePaylinesU3Ed__7_System_Collections_IEnumerator_get_Current_m158CB134A2F12231CED360FF8CE52E22A07B1846 (void);
// 0x000005B3 System.Void CSReelsAnimation/<AnimatePaylinesFreeGame>d__8::.ctor(System.Int32)
extern void U3CAnimatePaylinesFreeGameU3Ed__8__ctor_m88E61765CD759489B684B3F2C8B5F1D8F2CDBAD7 (void);
// 0x000005B4 System.Void CSReelsAnimation/<AnimatePaylinesFreeGame>d__8::System.IDisposable.Dispose()
extern void U3CAnimatePaylinesFreeGameU3Ed__8_System_IDisposable_Dispose_m93957906D15DE26BA2D822B5044001DE07DAFCC9 (void);
// 0x000005B5 System.Boolean CSReelsAnimation/<AnimatePaylinesFreeGame>d__8::MoveNext()
extern void U3CAnimatePaylinesFreeGameU3Ed__8_MoveNext_m824326586127F2201B5C048056A0DD837F7A4ABE (void);
// 0x000005B6 System.Object CSReelsAnimation/<AnimatePaylinesFreeGame>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePaylinesFreeGameU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE6B67BCCE381B0183DDD621352CF8F186DB759CB (void);
// 0x000005B7 System.Void CSReelsAnimation/<AnimatePaylinesFreeGame>d__8::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePaylinesFreeGameU3Ed__8_System_Collections_IEnumerator_Reset_mE07F517D9960C222F6C98F6229BFF29B930B85B9 (void);
// 0x000005B8 System.Object CSReelsAnimation/<AnimatePaylinesFreeGame>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePaylinesFreeGameU3Ed__8_System_Collections_IEnumerator_get_Current_m0C8203DBCE7A2EC30EF4AA8BA0E14A5700DCB3AD (void);
// 0x000005B9 System.Void CSSceneManager::Awake()
extern void CSSceneManager_Awake_mD63FDA1B12F3998783686CAFE01AA408BD70ACFE (void);
// 0x000005BA System.Void CSSceneManager::Loaded()
extern void CSSceneManager_Loaded_m1BF6A700B039B28CF5798C8502B57EC5F68024E5 (void);
// 0x000005BB System.Void CSSceneManager::Update()
extern void CSSceneManager_Update_mC6572B2656E36583CA4EE2FCC74BFDA30F56521B (void);
// 0x000005BC System.Void CSSceneManager::LoadScene(System.Int32,System.Action`2<System.Single,System.Boolean>)
extern void CSSceneManager_LoadScene_mB153168EAD4A264A28914015A5C0A236A39A9C0E (void);
// 0x000005BD System.Void CSSceneManager::LoadScene(System.String,System.Action`2<System.Single,System.Boolean>)
extern void CSSceneManager_LoadScene_m4AE753B59E34E057C54246143C03BF1AA04327FC (void);
// 0x000005BE System.Void CSSceneManager::StartLoad()
extern void CSSceneManager_StartLoad_m0066D323914782FB3870D7D36ED1423D528C2B3E (void);
// 0x000005BF System.Void CSSceneManager::EndLoad()
extern void CSSceneManager_EndLoad_m24CB4105A6EECF663211AEA40413765F420F2862 (void);
// 0x000005C0 System.Void CSSceneManager::Tick()
extern void CSSceneManager_Tick_m84EABBDA7CE71B07A45AC361FA05B79751AC6391 (void);
// 0x000005C1 System.Void CSSceneManager::Next()
extern void CSSceneManager_Next_m29C0A4DC19F647592ED8FB013AEDB6B0BC2F934C (void);
// 0x000005C2 System.Boolean CSSceneManager::IsComplete()
extern void CSSceneManager_IsComplete_mAC2B00306D8CF34693FFA2F38D49302E7AF744C4 (void);
// 0x000005C3 System.Void CSSceneManager::PurgeAssets()
extern void CSSceneManager_PurgeAssets_m546FD4A4A3E7583B8E90D2FC685E230724319E5D (void);
// 0x000005C4 System.Void CSSceneManager::LoadScene()
extern void CSSceneManager_LoadScene_mEA320D80A38557D53F618E061A1101B311CF963A (void);
// 0x000005C5 System.Void CSSceneManager::.ctor()
extern void CSSceneManager__ctor_m77AA654B2383E15B5604D1DBFA43664D66E7E022 (void);
// 0x000005C6 System.Void CSSceneManager::.cctor()
extern void CSSceneManager__cctor_mC55CBBF663350981E38314B22A2A21953E7B489E (void);
// 0x000005C7 System.Boolean CSSettingsPanel::get_active()
extern void CSSettingsPanel_get_active_mC6D503D2292366888196B874DA3F73380D952FD7 (void);
// 0x000005C8 System.Void CSSettingsPanel::set_active(System.Boolean)
extern void CSSettingsPanel_set_active_mA4E263AF6C1E61CA7478001986446BA79461AB3E (void);
// 0x000005C9 System.Void CSSettingsPanel::Awake()
extern void CSSettingsPanel_Awake_mA69AAD3C71A60B04AF2319E9E3E8AB81FF6BECF7 (void);
// 0x000005CA System.Void CSSettingsPanel::UpdateValues()
extern void CSSettingsPanel_UpdateValues_m24AFF078B5B1D2EF71FAC3AF00E75EEB8C527F75 (void);
// 0x000005CB System.Void CSSettingsPanel::Appear()
extern void CSSettingsPanel_Appear_m78136F79593127DB2C96FC090C23784A79906E9D (void);
// 0x000005CC System.Void CSSettingsPanel::Disappear(System.Action)
extern void CSSettingsPanel_Disappear_mACEDCB9E89F17C2E85CAC5939E27B0A5CF168A41 (void);
// 0x000005CD LTDescr CSSettingsPanel::Scale(LeanTweenType)
extern void CSSettingsPanel_Scale_m0C7DAD710FDAC8B70A5494ACF811391DA150E7D0 (void);
// 0x000005CE LTDescr CSSettingsPanel::AlphaBackground(System.Single)
extern void CSSettingsPanel_AlphaBackground_m6B164BE8C49B2ECACF63C499CF7D6A424A9563D1 (void);
// 0x000005CF LTDescr CSSettingsPanel::AlphaBoard(System.Single)
extern void CSSettingsPanel_AlphaBoard_m110077FD02F682DEB4BAE0B592E3EF706B38637E (void);
// 0x000005D0 LTDescr CSSettingsPanel::Alpha(UnityEngine.UI.Image,System.Single,System.Single)
extern void CSSettingsPanel_Alpha_mF853E384AA72BC9F80EBED8F0190A1D064351E3C (void);
// 0x000005D1 System.Void CSSettingsPanel::CanvasStatus(System.Boolean)
extern void CSSettingsPanel_CanvasStatus_m09A0B85AE9688BAA8B56ECCE1187A08590A8696B (void);
// 0x000005D2 System.Void CSSettingsPanel::CanvasStatus(UnityEngine.CanvasGroup,System.Boolean)
extern void CSSettingsPanel_CanvasStatus_mB406334CA76477583E14B6C8F9B320F2FB0EC76A (void);
// 0x000005D3 System.Void CSSettingsPanel::OnClose()
extern void CSSettingsPanel_OnClose_mEDF698F880EAC347D8207FA621859FD2DABC1F5D (void);
// 0x000005D4 System.Void CSSettingsPanel::OnSound(System.Boolean)
extern void CSSettingsPanel_OnSound_mA10BA9DC46A3497D1AA38229A917A7D04E20EC30 (void);
// 0x000005D5 System.Void CSSettingsPanel::OnMusic(System.Boolean)
extern void CSSettingsPanel_OnMusic_mDA51021F3CE9DBCECEB05A1BFEF265BC6EC099AF (void);
// 0x000005D6 System.Void CSSettingsPanel::OnVolume(System.Single)
extern void CSSettingsPanel_OnVolume_m3C7CE227280A139884BCA960272F8DA239286C68 (void);
// 0x000005D7 System.Void CSSettingsPanel::OnFacebookLogin()
extern void CSSettingsPanel_OnFacebookLogin_m8DAD32232DB8830867D1487002BA79DF57D0E074 (void);
// 0x000005D8 System.Void CSSettingsPanel::OnFacebookLogout()
extern void CSSettingsPanel_OnFacebookLogout_mE932317FF72A0232F22DDF4FA4F5483C3F6C918E (void);
// 0x000005D9 System.Void CSSettingsPanel::OnFacebookInvite()
extern void CSSettingsPanel_OnFacebookInvite_mE88741C22AB61FC197E6DFBF06CD47B7D3505BA0 (void);
// 0x000005DA System.Void CSSettingsPanel::OnRestore()
extern void CSSettingsPanel_OnRestore_m9E02740609B27003E6E41EB4F9168A783135EB30 (void);
// 0x000005DB System.Void CSSettingsPanel::.ctor()
extern void CSSettingsPanel__ctor_mC92C0A9E969606B36390440C13E4C8622BCC5742 (void);
// 0x000005DC System.Void CSSettingsPanel::<OnClose>b__24_0()
extern void CSSettingsPanel_U3COnCloseU3Eb__24_0_m7621188436DABC1F806C4339EE010D2A840AB82F (void);
// 0x000005DD System.Void CSSettingsPanel/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_mC2B2A7E6E31611F46D069C4A622C66AD918802EB (void);
// 0x000005DE System.Void CSSettingsPanel/<>c__DisplayClass21_0::<Alpha>b__0(System.Single)
extern void U3CU3Ec__DisplayClass21_0_U3CAlphaU3Eb__0_mBE7D2E5CD70369B05D9BB040C7CE2135E88C1F0B (void);
// 0x000005DF System.Boolean CSSettingsPanelSmall::get_show()
extern void CSSettingsPanelSmall_get_show_m117388B432AA40519D0D5DB44759A85DAF4444BB (void);
// 0x000005E0 System.Void CSSettingsPanelSmall::set_show(System.Boolean)
extern void CSSettingsPanelSmall_set_show_m5DF84B3EB6E3D7A7A6A169CAAF896B97F6B097FC (void);
// 0x000005E1 System.Void CSSettingsPanelSmall::Start()
extern void CSSettingsPanelSmall_Start_m533A99B464A3C89A2FF592A6ADF070F820048126 (void);
// 0x000005E2 System.Void CSSettingsPanelSmall::Appear()
extern void CSSettingsPanelSmall_Appear_m34D84F9619FB7643C4D93FF8076465ADA7A700FB (void);
// 0x000005E3 System.Void CSSettingsPanelSmall::Disappear()
extern void CSSettingsPanelSmall_Disappear_m39C59A60544C0252A215F8B444DD78C5303C76BC (void);
// 0x000005E4 LTDescr CSSettingsPanelSmall::Scale(System.Single)
extern void CSSettingsPanelSmall_Scale_m32B0B1F9E51D50AD5E4AB6BB8416362B5FF7CB05 (void);
// 0x000005E5 System.Void CSSettingsPanelSmall::OnSound(UnityEngine.UI.Toggle)
extern void CSSettingsPanelSmall_OnSound_m8A6F40554A7A8B109D053926B031AC832B2F6DCF (void);
// 0x000005E6 System.Void CSSettingsPanelSmall::OnSettings()
extern void CSSettingsPanelSmall_OnSettings_m9FDE9FE6A4F95DA004F6DC88E1B7DD9E4404B4E8 (void);
// 0x000005E7 System.Void CSSettingsPanelSmall::OnContactUs()
extern void CSSettingsPanelSmall_OnContactUs_m909BE30313FF5E537253E50087256F30AEE0A518 (void);
// 0x000005E8 System.Void CSSettingsPanelSmall::SendEmail(System.String,System.String,System.String)
extern void CSSettingsPanelSmall_SendEmail_m04E02EFAA11F0A440A4DD5ACBC065D793130D929 (void);
// 0x000005E9 System.String CSSettingsPanelSmall::MyEscapeURL(System.String)
extern void CSSettingsPanelSmall_MyEscapeURL_m784FEFA564FC6FB98B84157866D13ED8641FAF0E (void);
// 0x000005EA System.Void CSSettingsPanelSmall::.ctor()
extern void CSSettingsPanelSmall__ctor_m37258BCFA3953FAC783E41A1FB3FAD33D97D1B32 (void);
// 0x000005EB System.Boolean CSSoundManager::get_sound()
extern void CSSoundManager_get_sound_mB995027223DF822CCFD4B3861440EA01B2C0DDBB (void);
// 0x000005EC System.Void CSSoundManager::set_sound(System.Boolean)
extern void CSSoundManager_set_sound_m88953F084D0B81C3CF383068568F02A295814232 (void);
// 0x000005ED System.Boolean CSSoundManager::get_music()
extern void CSSoundManager_get_music_m0E162D07ABCD21B1B26157E1CD60CE45EED1C8DF (void);
// 0x000005EE System.Void CSSoundManager::set_music(System.Boolean)
extern void CSSoundManager_set_music_m4BBA4CC772B3B54F8DA97BD40B295BFDF9BE543E (void);
// 0x000005EF System.Single CSSoundManager::get_volume()
extern void CSSoundManager_get_volume_m362FB602FB48EA29F517361E5FC3024EDC7BAF4D (void);
// 0x000005F0 System.Void CSSoundManager::set_volume(System.Single)
extern void CSSoundManager_set_volume_m323706F07D247B4C2BE31E3CB41875A529FB3F63 (void);
// 0x000005F1 System.Void CSSoundManager::Awake()
extern void CSSoundManager_Awake_mD71AAC755338C2ED179EDDF9AF045AB528A70D25 (void);
// 0x000005F2 System.Void CSSoundManager::Loaded()
extern void CSSoundManager_Loaded_mF55BBE6ADFFC5E7305E6D542E68C703A0287DE57 (void);
// 0x000005F3 System.Void CSSoundManager::PlayMusic(System.String)
extern void CSSoundManager_PlayMusic_m1BD270F017822C1C924587A0FF340584E95F853C (void);
// 0x000005F4 System.Void CSSoundManager::StopMusic()
extern void CSSoundManager_StopMusic_mF48719008E630496968421E3E433485D830121A2 (void);
// 0x000005F5 System.Void CSSoundManager::Tap()
extern void CSSoundManager_Tap_mC3E135530A7FD889CCDFA05F9BD46CC97C344E21 (void);
// 0x000005F6 System.Void CSSoundManager::Stop(System.String)
extern void CSSoundManager_Stop_mFDCAC1ED3AD6E581D9B347BF11117222C705888F (void);
// 0x000005F7 System.Void CSSoundManager::Pause(System.String)
extern void CSSoundManager_Pause_mF6F12C681EB0AFD512952D21C2995611B7247C96 (void);
// 0x000005F8 System.Void CSSoundManager::Play(System.String)
extern void CSSoundManager_Play_mD7EB328AF23761DDC3755D2A18DCCD671169D4C8 (void);
// 0x000005F9 System.Void CSSoundManager::ClipForName(System.String,System.Action`1<Sound>)
extern void CSSoundManager_ClipForName_m3D55C697982B70405F3D80BFF940AA3B04E134DA (void);
// 0x000005FA System.Void CSSoundManager::PauseAll(System.Boolean)
extern void CSSoundManager_PauseAll_m8756405D5302BE75B6726A615610EF3FB6B431AD (void);
// 0x000005FB System.Void CSSoundManager::.ctor()
extern void CSSoundManager__ctor_m2A5DE3D303EDDAE8EF9AB44969492ED5975FDAD9 (void);
// 0x000005FC System.Void CSSoundManager::.cctor()
extern void CSSoundManager__cctor_m944B86F0A3F756E4CFF73443E01B13BC67665445 (void);
// 0x000005FD System.Void CSSoundManager/<>c::.cctor()
extern void U3CU3Ec__cctor_mADE652E646DE533C33432DF1955971A682F55EE9 (void);
// 0x000005FE System.Void CSSoundManager/<>c::.ctor()
extern void U3CU3Ec__ctor_mBF2B1AE3B0AF8D90D1EBAFF1EFE1B758B8EABE7A (void);
// 0x000005FF System.Void CSSoundManager/<>c::<Stop>b__17_0(Sound)
extern void U3CU3Ec_U3CStopU3Eb__17_0_m5DACA5391D1584D17B885D552457CDE863989EBC (void);
// 0x00000600 System.Void CSSoundManager/<>c::<Pause>b__18_0(Sound)
extern void U3CU3Ec_U3CPauseU3Eb__18_0_mA6F024CB1BFBD89CE5303C7985782524E969261E (void);
// 0x00000601 System.Void CSSoundManager/<>c::<Play>b__19_0(Sound)
extern void U3CU3Ec_U3CPlayU3Eb__19_0_m25F59A0AD2168B506AD29D4C92B276E68EB577C7 (void);
// 0x00000602 System.Void CSSoundManager/<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m47D33B355A061ACCF35916340B3EDCBE12A66EBB (void);
// 0x00000603 System.Boolean CSSoundManager/<>c__DisplayClass20_0::<ClipForName>b__0(Sound)
extern void U3CU3Ec__DisplayClass20_0_U3CClipForNameU3Eb__0_m4B2DC76C45324B2CFBDD5D4A6E0F95BB49F120F8 (void);
// 0x00000604 System.Void CSSoundManager/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_m7B70CC6F807B9F0D45292DAA28E54498A27A829C (void);
// 0x00000605 System.Void CSSoundManager/<>c__DisplayClass21_0::<PauseAll>b__0(Sound)
extern void U3CU3Ec__DisplayClass21_0_U3CPauseAllU3Eb__0_mC3F5C79E99C2176BDB0AE38DF368D26867713F75 (void);
// 0x00000606 System.String Sound::get_name()
extern void Sound_get_name_m2F0E4AF357AE40536505E771E8F12D93A2E84526 (void);
// 0x00000607 System.Boolean Sound::get_isPlaying()
extern void Sound_get_isPlaying_m6FF0E3A5B9A511447A07224022B7596710AC1BDE (void);
// 0x00000608 System.Void Sound::AddSource(UnityEngine.AudioSource)
extern void Sound_AddSource_m866B99890ADFE25B135BC676E3B8B606CA4CFA99 (void);
// 0x00000609 System.Void Sound::Play()
extern void Sound_Play_mB064EA168DA913655CAF4BADB5942BC1BD4245F6 (void);
// 0x0000060A System.Void Sound::Stop()
extern void Sound_Stop_m418EBF80EA2F6D87147FDFD3D311B3ECA7F92425 (void);
// 0x0000060B System.Void Sound::Pause()
extern void Sound_Pause_m2B7DFA4F0583FF3BD6C0172742FEB47EF7075966 (void);
// 0x0000060C System.Void Sound::UnPause()
extern void Sound_UnPause_m3615D722E9E7432BF5F883EE140608F2BFD90ED2 (void);
// 0x0000060D System.Void Sound::.ctor()
extern void Sound__ctor_mEA0B0D2FBD514F91C21900B0BB8679CD78843FCD (void);
// 0x0000060E System.Void CSSymbol::Awake()
extern void CSSymbol_Awake_m8D57C09E36B923B03FD6899D8E6BBCFE965C6475 (void);
// 0x0000060F System.Void CSSymbol::StartWith(CSSymbolType,CSCell,UnityEngine.Vector3)
extern void CSSymbol_StartWith_m6D92BFA31A7BABA569E25E614C0BB06246EF1D39 (void);
// 0x00000610 System.Void CSSymbol::SetType(CSSymbolType)
extern void CSSymbol_SetType_m07241F5CEEAAABA2ACBD0A96CAB1B305D7676B6C (void);
// 0x00000611 System.Void CSSymbol::AddParticle(CSPayline,System.Single)
extern void CSSymbol_AddParticle_m682E057E762123681161AD6F318906A7E15C3967 (void);
// 0x00000612 System.Void CSSymbol::LifeTimeParticles(UnityEngine.ParticleSystem[],System.Single)
extern void CSSymbol_LifeTimeParticles_mBE10311CFD3B37E9C8D25F5CCECB1A025CB313A8 (void);
// 0x00000613 System.Void CSSymbol::DestroyParticle()
extern void CSSymbol_DestroyParticle_mDEA6B7AD4E99D000F92108D7089A814346934921 (void);
// 0x00000614 System.Void CSSymbol::StartAnimation(CSPayline,System.Single)
extern void CSSymbol_StartAnimation_mF0487B31477AF67FBA5F87F751AFA1E9D2138F86 (void);
// 0x00000615 System.Void CSSymbol::StopAnimation()
extern void CSSymbol_StopAnimation_m03C94FDECE64AB4153BF06864B0B90BD0EE302B3 (void);
// 0x00000616 System.Collections.Generic.Dictionary`2<CSSymbolType,CSSymbolData> CSSymbol::CreateDictionaryData(CSSymbolData[])
extern void CSSymbol_CreateDictionaryData_m43B588846CAACE4BFCE6A3979F0DC404E96EC412 (void);
// 0x00000617 CSRule CSSymbol::Rule()
extern void CSSymbol_Rule_m2BB931AB077BDF6DF9D4C4A0079BD27754C1668E (void);
// 0x00000618 System.Void CSSymbol::.ctor()
extern void CSSymbol__ctor_m4764AE04F96E860C4678344CEFBF667617C46D39 (void);
// 0x00000619 System.String CSSymbolPercent::ToString()
extern void CSSymbolPercent_ToString_mDB52EEF0AB867320264AF736B487BE6B117CF240 (void);
// 0x0000061A System.Void CSSymbolData::.ctor()
extern void CSSymbolData__ctor_m4BDF8AE33151A2469E95DF8C56453067E2A60FE7 (void);
// 0x0000061B System.Void CSAnimationData::.ctor()
extern void CSAnimationData__ctor_m35A1535591D52F4B10B77993FE193D51B898339F (void);
// 0x0000061C System.Single CSRule::Win(System.Int32)
extern void CSRule_Win_m868E86E25771BDF0E19D2D7ED5AC66E1EBD6328C (void);
// 0x0000061D System.Void CSRule::.ctor()
extern void CSRule__ctor_m3D0E3BB8C7ABB2A62AAF28FAA0BF44EB7C76C753 (void);
// 0x0000061E System.Void CSTopPanel::OnBuyCoins()
extern void CSTopPanel_OnBuyCoins_m86B786C2A1DAC38C6B58B38F48318E9982D99763 (void);
// 0x0000061F System.Void CSTopPanel::AddXPValue(System.Single)
extern void CSTopPanel_AddXPValue_m7ECD2D6BA40F80FC2A9CFC70B153335D28D76367 (void);
// 0x00000620 System.Void CSTopPanel::AddCoins(System.Single)
extern void CSTopPanel_AddCoins_mCADD5560B958C9FFBFA64BF7D84898C2CBCEB46B (void);
// 0x00000621 System.Void CSTopPanel::OnLobby(System.String)
extern void CSTopPanel_OnLobby_m2D5E0BA304B1D114731C2379F96BCAC4B0FE277C (void);
// 0x00000622 System.Void CSTopPanel::.ctor()
extern void CSTopPanel__ctor_m0FF8BBE2CF3DEBD21ABDC858E8A2DE622CCA6E9E (void);
// 0x00000623 System.Void CSAdMobManager::Awake()
extern void CSAdMobManager_Awake_mFBCE1821A1184700BE8CE2E9E806414F7B81E0A5 (void);
// 0x00000624 System.Void CSAdMobManager::OnDestroy()
extern void CSAdMobManager_OnDestroy_m95C726DAF85A0DA1ED7AB37632220C0FCB7CE932 (void);
// 0x00000625 System.Void CSAdMobManager::Loaded()
extern void CSAdMobManager_Loaded_m981B6EEFB2EAC3427843A109351DEDC9D4923C2A (void);
// 0x00000626 System.Void CSAdMobManager::InitAdmob()
extern void CSAdMobManager_InitAdmob_mC16CC92005A4E2A3C83A06C6030E27AD65FF57AB (void);
// 0x00000627 System.Void CSAdMobManager::RequestRewardBasedVideo()
extern void CSAdMobManager_RequestRewardBasedVideo_mF3A7CE7A53719F5EC1ECAEC57929E21DCD76EF9D (void);
// 0x00000628 System.Boolean CSAdMobManager::ShowVideoRewardAd(System.Action)
extern void CSAdMobManager_ShowVideoRewardAd_mA05529A6127DC68B96A18662D1BB7909BE64A814 (void);
// 0x00000629 System.Boolean CSAdMobManager::ShowInterstitialAd(System.Action)
extern void CSAdMobManager_ShowInterstitialAd_m98EC45DDCBA3E083E173EAF2BE8A532C33505944 (void);
// 0x0000062A System.Void CSAdMobManager::HideInterstitialAd()
extern void CSAdMobManager_HideInterstitialAd_mD4F7845277CC2525A84A0E81E9C0677B11E23708 (void);
// 0x0000062B System.Void CSAdMobManager::RequestInterstitialAd(GoogleMobileAds.Api.InterstitialAd)
extern void CSAdMobManager_RequestInterstitialAd_m38CD9C61DDE47579644829519F1791C5C0505695 (void);
// 0x0000062C GoogleMobileAds.Api.AdRequest CSAdMobManager::CreateAdRequest()
extern void CSAdMobManager_CreateAdRequest_m4FE3B1E7BA5D8B122CD8F1AE70C82EB598F4A443 (void);
// 0x0000062D GoogleMobileAds.Api.InterstitialAd CSAdMobManager::CreateInterstitialAd()
extern void CSAdMobManager_CreateInterstitialAd_mBCFD61376452AE0072E6679DD7F149705915CF8B (void);
// 0x0000062E System.Void CSAdMobManager::ShowAds(System.Action)
extern void CSAdMobManager_ShowAds_m95DE70B0EBE13CF3AF87DBD7F1178DEA3B5037D4 (void);
// 0x0000062F System.Void CSAdMobManager::HandleInterstitialAdFailedToLoad(System.Object,GoogleMobileAds.Api.AdFailedToLoadEventArgs)
extern void CSAdMobManager_HandleInterstitialAdFailedToLoad_mFCD43C930A7355F48DCB0589FBC014407F6EBD51 (void);
// 0x00000630 System.Void CSAdMobManager::HandleInterstitialAdOnAdOpened(System.Object,System.EventArgs)
extern void CSAdMobManager_HandleInterstitialAdOnAdOpened_mE785E07BCD01FC918FB8334804840F1521599EED (void);
// 0x00000631 System.Void CSAdMobManager::HandleInterstitialAdOnAdClosed(System.Object,System.EventArgs)
extern void CSAdMobManager_HandleInterstitialAdOnAdClosed_mF8CEE254DCEAFD74EFA89909EC64DA33BF32866C (void);
// 0x00000632 System.Void CSAdMobManager::HandleRewardBasedVideoOpened(System.Object,System.EventArgs)
extern void CSAdMobManager_HandleRewardBasedVideoOpened_m3AD185DC943BA3E205FB5F39E8044B4A0D754A6B (void);
// 0x00000633 System.Void CSAdMobManager::HandleRewardBasedVideoClosed(System.Object,System.EventArgs)
extern void CSAdMobManager_HandleRewardBasedVideoClosed_m992A4797EA0325DDC3C4A19E3F69504A428DBE83 (void);
// 0x00000634 System.Void CSAdMobManager::HandleRewardBasedVideoRewarded(System.Object,GoogleMobileAds.Api.Reward)
extern void CSAdMobManager_HandleRewardBasedVideoRewarded_m71B2C156AB064BD84F2DCFF9D98E869004957AA9 (void);
// 0x00000635 System.Void CSAdMobManager::HandleRewardBasedVideoFailedToLoad(System.Object,GoogleMobileAds.Api.AdFailedToLoadEventArgs)
extern void CSAdMobManager_HandleRewardBasedVideoFailedToLoad_mF461F2F4F1E77E8C09703A392BF25B9FD1102016 (void);
// 0x00000636 System.Void CSAdMobManager::HandleRewardBasedVideoLoaded(System.Object,System.EventArgs)
extern void CSAdMobManager_HandleRewardBasedVideoLoaded_mABEE476D7B01DEE88105340BBA8EF8C7491FFE5C (void);
// 0x00000637 System.Void CSAdMobManager::.ctor()
extern void CSAdMobManager__ctor_m117F74D642EDD49DC962705BCC8369CDFAB57088 (void);
// 0x00000638 System.Void CSAdMobManager::.cctor()
extern void CSAdMobManager__cctor_m04A3D4B4D98B4610F98D1AD454199D7771299A72 (void);
// 0x00000639 System.Void CSAdMobManager/<>c::.cctor()
extern void U3CU3Ec__cctor_mF15F33F15785960B93F441527C0BE442CB53D431 (void);
// 0x0000063A System.Void CSAdMobManager/<>c::.ctor()
extern void U3CU3Ec__ctor_mC89E1B48E435EC43F555C1898A126E015DDB8B01 (void);
// 0x0000063B System.Void CSAdMobManager/<>c::<InitAdmob>b__15_0(GoogleMobileAds.Api.InitializationStatus)
extern void U3CU3Ec_U3CInitAdmobU3Eb__15_0_mF572069890B616E86C5FCA2C611C03775DD1108C (void);
// 0x0000063C System.Void CSButtonTap::Start()
extern void CSButtonTap_Start_m6560AA1157484D251E6829DC88477ED80E7B46AA (void);
// 0x0000063D System.Void CSButtonTap::.ctor()
extern void CSButtonTap__ctor_m55A4A9529B4D9F0C6C77ADE388FE7667045594E6 (void);
// 0x0000063E System.Void CSButtonTap/<>c::.cctor()
extern void U3CU3Ec__cctor_m5680B75BCAE1191EF5EB0039237F0317518E0534 (void);
// 0x0000063F System.Void CSButtonTap/<>c::.ctor()
extern void U3CU3Ec__ctor_m3E7D504EE5CF24ACAB7CC455A707D904DC0FD050 (void);
// 0x00000640 System.Void CSButtonTap/<>c::<Start>b__0_0()
extern void U3CU3Ec_U3CStartU3Eb__0_0_m44A3520B00EAE861C349F81C5D1066EBDE3A8D08 (void);
// 0x00000641 System.Void CSCell::.ctor(System.Int32,System.Int32)
extern void CSCell__ctor_m0C8EC709B1E45D68FF687CE791819FBDD81E45A7 (void);
// 0x00000642 CSCell CSCell::Zero()
extern void CSCell_Zero_mF90C7682C3F29217DEE2C4977386867340D1B654 (void);
// 0x00000643 CSCell CSCell::One()
extern void CSCell_One_m3764016EBD2A77635755CFD14F23F6382F14F065 (void);
// 0x00000644 CSCell CSCell::Up()
extern void CSCell_Up_m7E874F6B1D339642B96D1F68364A4F8669C1AE11 (void);
// 0x00000645 CSCell CSCell::Down()
extern void CSCell_Down_mD11942FCA842CEB18CBDC3F326E0331D0A5DEBC4 (void);
// 0x00000646 CSCell CSCell::Right()
extern void CSCell_Right_mDE9D4DA4D386B835211CB60C01FDE091333757F9 (void);
// 0x00000647 CSCell CSCell::Left()
extern void CSCell_Left_mD0E32BA21BDA968D41FF5EBDC55414C27788F194 (void);
// 0x00000648 CSCell CSCell::NextColumn()
extern void CSCell_NextColumn_m6CBB9ACE6D997D24DD47373EDCCECA6FEB0D8186 (void);
// 0x00000649 CSCell CSCell::NextRow()
extern void CSCell_NextRow_mE1400800E58C6AD112B2ADCD1F147C0C163C42C2 (void);
// 0x0000064A CSCell CSCell::PreviousColumn()
extern void CSCell_PreviousColumn_m86AA491343B74822BDF89BF80286B5C0B3DFA692 (void);
// 0x0000064B CSCell CSCell::PreviousRow()
extern void CSCell_PreviousRow_m3BFE6BD09252F9C2F836AFDC35C9D59F2BB0BE33 (void);
// 0x0000064C CSCell CSCell::op_Addition(CSCell,CSCell)
extern void CSCell_op_Addition_m89F2809AE808E5E4F8E1624325DF26CBAE1D2117 (void);
// 0x0000064D CSCell CSCell::op_Subtraction(CSCell,CSCell)
extern void CSCell_op_Subtraction_m0B98E50B26332713B57BD6C80AC162FD6A294906 (void);
// 0x0000064E UnityEngine.Vector3 CSCell::op_Multiply(UnityEngine.Vector3,CSCell)
extern void CSCell_op_Multiply_m64AF49C81366F48E3DE4FE446E9F7B3569651A66 (void);
// 0x0000064F System.Boolean CSCell::op_Equality(CSCell,CSCell)
extern void CSCell_op_Equality_m0C228A03F980DA8D5AC925D74436A43B6DF094AD (void);
// 0x00000650 System.Boolean CSCell::op_Inequality(CSCell,CSCell)
extern void CSCell_op_Inequality_m5E144DD354080D8730A966249CB73BB129DA2B5E (void);
// 0x00000651 System.Boolean CSCell::Equals(System.Object)
extern void CSCell_Equals_mED606063B54378D2B4B319B74D56F869A91AFECB (void);
// 0x00000652 System.Int32 CSCell::GetHashCode()
extern void CSCell_GetHashCode_m68B4FB99837F4EABF397896223202DE95CAAEF46 (void);
// 0x00000653 System.String CSCell::ToString()
extern void CSCell_ToString_m9F794641C70313A531B220B091BF0019978C62DE (void);
// 0x00000654 System.Boolean CS2DBoolArray::get_Item(System.Int32,System.Int32)
extern void CS2DBoolArray_get_Item_m9B9776F8A650CF281486EDAF048BF6A8968647B5 (void);
// 0x00000655 System.Void CS2DBoolArray::set_Item(System.Int32,System.Int32,System.Boolean)
extern void CS2DBoolArray_set_Item_mA5B49B82391C45C0BCE298D67213C119CC664077 (void);
// 0x00000656 System.Void CS2DBoolArray::.ctor()
extern void CS2DBoolArray__ctor_m9C9DC3904567576B15C4165F0739065305E7B8DF (void);
// 0x00000657 System.Boolean CS2DBoolArray/L2D::get_Item(System.Int32)
extern void L2D_get_Item_m17258F42C1AB187829FA85D33B930995C272CB8C (void);
// 0x00000658 System.Void CS2DBoolArray/L2D::set_Item(System.Int32,System.Boolean)
extern void L2D_set_Item_m4D9FEDEA05402830C287E075742C20E4F947645A (void);
// 0x00000659 System.Void CSEnumFlagAttribute::.ctor()
extern void CSEnumFlagAttribute__ctor_m6B052910A97CB4F2E5C202F04BF1EBA84AC12321 (void);
// 0x0000065A System.Void CSEnumFlagAttribute::.ctor(System.String)
extern void CSEnumFlagAttribute__ctor_m5259FDA7456298860A418138E8984D5B9ABDA886 (void);
// 0x0000065B System.Void CSFPSDisplay::Update()
extern void CSFPSDisplay_Update_m28D6110B4AC2145B8FA9FAA5ABD4A840A1F5A673 (void);
// 0x0000065C System.Void CSFPSDisplay::OnGUI()
extern void CSFPSDisplay_OnGUI_m00C5DB2132975CF8580A7318157CC36E68BEBA04 (void);
// 0x0000065D System.Void CSFPSDisplay::.ctor()
extern void CSFPSDisplay__ctor_mDEBCC3653113EC8133F0055DA7EC8F2B3F01D62C (void);
// 0x0000065E System.Void CSGameScaler::Awake()
extern void CSGameScaler_Awake_m8EEBE36373DCA1D12502E3B624A63863124B2E39 (void);
// 0x0000065F System.Void CSGameScaler::Scale()
extern void CSGameScaler_Scale_mE7FCBEEB557787A31B437D1411456578D1D92484 (void);
// 0x00000660 System.Void CSGameScaler::.ctor()
extern void CSGameScaler__ctor_m77C71FF328157A236355638F4CDFCBA73E401410 (void);
// 0x00000661 UnityEngine.Material CSGlowAnimation::get_Material()
extern void CSGlowAnimation_get_Material_m6B6384AABA52F2B8853E00C27AE6724F950E6FF8 (void);
// 0x00000662 System.Boolean CSGlowAnimation::get_enable()
extern void CSGlowAnimation_get_enable_m16B2E71A3DB921FF22134EAF6DE0007D08B83AAB (void);
// 0x00000663 System.Void CSGlowAnimation::set_enable(System.Boolean)
extern void CSGlowAnimation_set_enable_m8EC8028D16D1F6B1D36B16BE3198AA064A2BFA98 (void);
// 0x00000664 System.Void CSGlowAnimation::Start()
extern void CSGlowAnimation_Start_mAEC28FFCA7C6315829BA1508C88C5D3A825145C5 (void);
// 0x00000665 System.Void CSGlowAnimation::OnDestroy()
extern void CSGlowAnimation_OnDestroy_m5AE40B49B1ED5B523DEB3535813342F8D86A4F42 (void);
// 0x00000666 System.Void CSGlowAnimation::Action(System.Boolean)
extern void CSGlowAnimation_Action_mF4186990FBE21753B8BAE303BB134D2F06F2FD92 (void);
// 0x00000667 System.Void CSGlowAnimation::Start(System.Boolean)
extern void CSGlowAnimation_Start_m045372FFF11A0D162F0DEDDDA1137333A6BE26C3 (void);
// 0x00000668 System.Void CSGlowAnimation::SetShineLocation(System.Single)
extern void CSGlowAnimation_SetShineLocation_mCB1D71F46B1561B85A6C1BA7723E1FF7F477B28D (void);
// 0x00000669 UnityEngine.Material CSGlowAnimation::GetMaterial()
extern void CSGlowAnimation_GetMaterial_m118B558F3148A0C56403E2757F5E499A4A00B23B (void);
// 0x0000066A System.Void CSGlowAnimation::Stop(System.Boolean)
extern void CSGlowAnimation_Stop_m5E09C2D911CBE011609EF21D2B0FC813ABC91224 (void);
// 0x0000066B System.Void CSGlowAnimation::SetMaterial()
extern void CSGlowAnimation_SetMaterial_m66B85D1FE992876FDE75E80705DAA6317A844C15 (void);
// 0x0000066C System.Void CSGlowAnimation::.ctor()
extern void CSGlowAnimation__ctor_m1531F50F2E50E53D5885D8449F294B8C2AC4EC01 (void);
// 0x0000066D System.Void CSGlowAnimation::<Start>b__14_0()
extern void CSGlowAnimation_U3CStartU3Eb__14_0_m2B6F45F34F11E7F1C3AD45944E5F7ACFB768864A (void);
// 0x0000066E System.Void CSGlowAnimation::<Start>b__17_0(System.Single)
extern void CSGlowAnimation_U3CStartU3Eb__17_0_m6F8A4CBDC691DBF8775798137980DA997C5567D7 (void);
// 0x0000066F CSCardData CSCardList2D::get_Item(System.Int32,System.Int32)
extern void CSCardList2D_get_Item_mDE93E85DFC4AAC3B61A90B6721188B535085718F (void);
// 0x00000670 System.Void CSCardList2D::set_Item(System.Int32,System.Int32,CSCardData)
extern void CSCardList2D_set_Item_m5B12731E0E1AEE9CF3FAAFFB4818BB12CFCA1D50 (void);
// 0x00000671 System.Void CSCardList2D::.ctor()
extern void CSCardList2D__ctor_mA9712CA94278D594048CF84077E12C810BAB25A1 (void);
// 0x00000672 CSCardData CSCardList2D/List2D::get_Item(System.Int32)
extern void List2D_get_Item_mB2DF0149C9C9045D8DF5B2A6796335BF11D30B16 (void);
// 0x00000673 System.Void CSCardList2D/List2D::set_Item(System.Int32,CSCardData)
extern void List2D_set_Item_m9566EFC162F85575933F9BD9127E2B2BA6C1C0CE (void);
// 0x00000674 System.Void CSSliderGroup::Start()
extern void CSSliderGroup_Start_m5ECB7BEF38E8A1665D8FFE73C045228514EDD722 (void);
// 0x00000675 System.Void CSSliderGroup::OnDestroy()
extern void CSSliderGroup_OnDestroy_m6C9624B348FC45223B716486FA5B17183C787657 (void);
// 0x00000676 System.Void CSSliderGroup::onValueChanged(UnityEngine.UI.Slider)
extern void CSSliderGroup_onValueChanged_m404AEC9C63450E627E4536AA643267024583B5BC (void);
// 0x00000677 System.Void CSSliderGroup::.ctor()
extern void CSSliderGroup__ctor_mE1E2BE6B360F855596FC01DC1C68A57783EA2E3F (void);
// 0x00000678 System.Void CSSliderGroup/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m3C7EB1AC7CC54057F31AEE3B2F13EF87A2F2637D (void);
// 0x00000679 System.Void CSSliderGroup/<>c__DisplayClass2_0::<Start>b__0(System.Single)
extern void U3CU3Ec__DisplayClass2_0_U3CStartU3Eb__0_mCDB453C141F5C53D3A3DBC131324B0C8EBF13D5A (void);
// 0x0000067A System.Void CSSliderGroup/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m861E98EFE78B4BD3BB5FDE5CA1CAC9C8C9D611A2 (void);
// 0x0000067B System.Void CSSliderGroup/<>c__DisplayClass3_0::<OnDestroy>b__0(System.Single)
extern void U3CU3Ec__DisplayClass3_0_U3COnDestroyU3Eb__0_m74B61B841EEF753187CAAA0F07A131012E944C3F (void);
// 0x0000067C System.Void TimerDelegate::.ctor(System.Object,System.IntPtr)
extern void TimerDelegate__ctor_mAC91CA2E750EEAF31798F5B718C8D11A320CF77C (void);
// 0x0000067D System.Void TimerDelegate::Invoke(CSTimer,System.Double)
extern void TimerDelegate_Invoke_m501F053679BE400307B360BBF0A9D8341D1AA496 (void);
// 0x0000067E System.IAsyncResult TimerDelegate::BeginInvoke(CSTimer,System.Double,System.AsyncCallback,System.Object)
extern void TimerDelegate_BeginInvoke_mAD21B78B7322C5FD7C0DC461A262F8D9CF5EDCCE (void);
// 0x0000067F System.Void TimerDelegate::EndInvoke(System.IAsyncResult)
extern void TimerDelegate_EndInvoke_m339B5435C09519708773BD0D696D2FAF063BE0B0 (void);
// 0x00000680 System.Void ICSTimer::TimerTick(CSTimer,System.Double)
// 0x00000681 System.Void ICSTimer::TimerStop(CSTimer,System.Double)
// 0x00000682 System.Void CSTimer::add_timerTickEvent(TimerDelegate)
extern void CSTimer_add_timerTickEvent_m4C53AE1A27DF33DCF4B9042A739D5B52954B12EC (void);
// 0x00000683 System.Void CSTimer::remove_timerTickEvent(TimerDelegate)
extern void CSTimer_remove_timerTickEvent_m86940855EF727EEA2557AAEA6896FFAC5037AF94 (void);
// 0x00000684 System.Void CSTimer::add_timerStopEvent(TimerDelegate)
extern void CSTimer_add_timerStopEvent_m685240BFBA6110FC031EC045C8E7C4DEEEB6F47C (void);
// 0x00000685 System.Void CSTimer::remove_timerStopEvent(TimerDelegate)
extern void CSTimer_remove_timerStopEvent_mD79D54D5A8D3430F6FA97410C63A97077E6D7732 (void);
// 0x00000686 System.Boolean CSTimer::get_running()
extern void CSTimer_get_running_m39D048B1383BC9A478ABF9CF6D687DFC9CFDC303 (void);
// 0x00000687 System.Void CSTimer::set_running(System.Boolean)
extern void CSTimer_set_running_mCE2A21EA49A1A2B1CA76A201EB1288DDD4D3D89F (void);
// 0x00000688 System.Void CSTimer::.ctor(System.DateTime,System.TimeSpan,System.String)
extern void CSTimer__ctor_m8BFECAAE4D3FB335E450309C2ADF700305B015AF (void);
// 0x00000689 System.Void CSTimer::.ctor(CSTimerData)
extern void CSTimer__ctor_m6D4B46768AB7DA8D50F5F877BAA53DFF9557123C (void);
// 0x0000068A System.Void CSTimer::.ctor(System.DateTime,System.Int32,System.String)
extern void CSTimer__ctor_m4E47B724F200621E0CF3BFC13A516A9F88BE48F4 (void);
// 0x0000068B System.Void CSTimer::OnDestroy()
extern void CSTimer_OnDestroy_mE7044D88B8D4B316126D3D50DA6AFDB9FCFF6A1C (void);
// 0x0000068C System.Void CSTimer::Tick()
extern void CSTimer_Tick_mD0BD23CF3BB947F921874006A670EC0EA3C3D99B (void);
// 0x0000068D System.Void CSTimer::Add(System.TimeSpan)
extern void CSTimer_Add_mB6F085C72C9EA37E70F0788A456FB4E408E73907 (void);
// 0x0000068E System.Void CSTimer::AddMinutes(System.Int32)
extern void CSTimer_AddMinutes_m6957558CFDCCF678890A85F7BBDF1A78E498F4E5 (void);
// 0x0000068F System.Void CSTimer::AddHours(System.Int32)
extern void CSTimer_AddHours_m0DF55A76695C9A146079AFC4B9B9A18747A8DB33 (void);
// 0x00000690 System.Void CSTimer::AddDays(System.Int32)
extern void CSTimer_AddDays_m13EC79056F5276E1944C43BE16FDDB47F8A72BE8 (void);
// 0x00000691 System.Void CSTimer::Subtract(System.TimeSpan)
extern void CSTimer_Subtract_m076F275E7C35ABBEE0FCD4470DDE82D3197A8D34 (void);
// 0x00000692 System.Void CSTimer::SubtractMinutes(System.Int32)
extern void CSTimer_SubtractMinutes_mE4D7498A2BA4C7575165D70BCC6688998899F0E2 (void);
// 0x00000693 System.Void CSTimer::SubtractHours(System.Int32)
extern void CSTimer_SubtractHours_m45BD8B5F8C66C1B2256E4A83F95536BC6B40142A (void);
// 0x00000694 System.Void CSTimer::SubtractDays(System.Int32)
extern void CSTimer_SubtractDays_mF55C8846828792F059DA1FF32F3C66052F313D40 (void);
// 0x00000695 System.TimeSpan CSTimer::DeltaNow()
extern void CSTimer_DeltaNow_mB70E1B4DEDE83D63B33E20936CBF7082300B1A17 (void);
// 0x00000696 System.TimeSpan CSTimer::Delta(System.DateTime)
extern void CSTimer_Delta_m4198B938B3B37C0B01EF49108A9F613FF7CF2175 (void);
// 0x00000697 System.TimeSpan CSTimer::Delta()
extern void CSTimer_Delta_mFB0C804DC639E26C5A395AFB5E93F15F975D0FA2 (void);
// 0x00000698 System.Int32 CSTimer::DeltaDays()
extern void CSTimer_DeltaDays_m25DF60B59CD441A1DBE4DC471D0B4B9094A3E73B (void);
// 0x00000699 System.Int32 CSTimer::DeltaHours()
extern void CSTimer_DeltaHours_m6653AFB1022EF65E853D172FE79AF00E0B2E659A (void);
// 0x0000069A System.Int32 CSTimer::DeltaMinutes()
extern void CSTimer_DeltaMinutes_m366A156168E0D1A83989FADEFC50C431490AA360 (void);
// 0x0000069B System.Int32 CSTimer::DeltaSeconds()
extern void CSTimer_DeltaSeconds_mED3F7CFC8A9979928C9347DAC454BF87D6CF90F1 (void);
// 0x0000069C System.Double CSTimer::TotalSeconds()
extern void CSTimer_TotalSeconds_m43306ED0006DA990BBC4B7E36A90C1E1D6EF86D5 (void);
// 0x0000069D System.Double CSTimer::UnsignedTotalSeconds()
extern void CSTimer_UnsignedTotalSeconds_m711FB0C79F365B1576263FC19D2109C69A1860F7 (void);
// 0x0000069E System.Double CSTimer::TotalMinutes()
extern void CSTimer_TotalMinutes_m40B48493ED7F80315406445824C8BD00789FD2F8 (void);
// 0x0000069F System.Boolean CSTimer::isTimeUp()
extern void CSTimer_isTimeUp_mFD15A14EC7507DB8CEC3EB6F4D7E4F7C3900EA2F (void);
// 0x000006A0 System.Void CSTimer::Subscribe(ICSTimer)
extern void CSTimer_Subscribe_mB02CCEED830F909D3AAF05FE89F38B5A977CBA54 (void);
// 0x000006A1 System.Void CSTimer::Unsubscribe(ICSTimer)
extern void CSTimer_Unsubscribe_mC5B1D46A2713EFEB72D951FC7EBC380ECC54A93B (void);
// 0x000006A2 System.Void CSTimer::UnsubscribeAll()
extern void CSTimer_UnsubscribeAll_m0242E17404EF2E6A6561B871DAF68320D06C5BF7 (void);
// 0x000006A3 System.DateTime CSTime::Now()
extern void CSTime_Now_mDF557C8208DBD172B15364241E842F9873BB0195 (void);
// 0x000006A4 System.Void CSTime::.ctor()
extern void CSTime__ctor_m80F677EC60F7B5F61FB9CC2DD0F57853F1BDA122 (void);
// 0x000006A5 System.Void CSTimerData::.ctor(CSTimer)
extern void CSTimerData__ctor_m52E7865B4D72DFDC44C03FE80861CF33BD41F654 (void);
// 0x000006A6 System.Void ICSUpdateable::Tick()
// 0x000006A7 System.Void CSTimerManager::add_TimerCreatedEvent(System.Action`2<CSTimer,System.String>)
extern void CSTimerManager_add_TimerCreatedEvent_mC170EB57ED91700AFDD85FA2540DFEF9157F69CD (void);
// 0x000006A8 System.Void CSTimerManager::remove_TimerCreatedEvent(System.Action`2<CSTimer,System.String>)
extern void CSTimerManager_remove_TimerCreatedEvent_m173BBDC52674515908021890019CE251FF168484 (void);
// 0x000006A9 System.Void CSTimerManager::Awake()
extern void CSTimerManager_Awake_m4783BA5B482F90FA9B73DEADCE30E0D823A3CF44 (void);
// 0x000006AA System.Void CSTimerManager::Loaded()
extern void CSTimerManager_Loaded_m2D1039D77EE569348E8638A64E676E6AE0E48A8E (void);
// 0x000006AB System.Void CSTimerManager::Update()
extern void CSTimerManager_Update_m65D6C8A69E0905616DAAE10127B342876F4B3A55 (void);
// 0x000006AC System.Collections.Generic.Dictionary`2<System.String,CSTimer> CSTimerManager::LoadTimers()
extern void CSTimerManager_LoadTimers_m2BCF4C33D453F27ABD7B52543E118C46400C0939 (void);
// 0x000006AD System.Void CSTimerManager::AddTimers()
extern void CSTimerManager_AddTimers_m451A8A74081A84E95FB8E8630C08FC5A8B4DE4C6 (void);
// 0x000006AE System.Void CSTimerManager::DestroyTimers()
extern void CSTimerManager_DestroyTimers_m4F9FD66A0CB0FA86D9F2B2913F423349D39D80A5 (void);
// 0x000006AF CSTimer CSTimerManager::CreateTimerHour(System.Int32,System.String)
extern void CSTimerManager_CreateTimerHour_mFFB0927C579EEDFBD4D0F75885E544D45C75E3CF (void);
// 0x000006B0 CSTimer CSTimerManager::CreateTimerMinutes(System.Double,System.String)
extern void CSTimerManager_CreateTimerMinutes_m93C5FF0FDFE863D129A0823C52257CE6DE167239 (void);
// 0x000006B1 CSTimer CSTimerManager::TimerForKey(System.String)
extern void CSTimerManager_TimerForKey_m1BFF886EBBC56E254AAC7220C10AD00E4E44607E (void);
// 0x000006B2 System.Void CSTimerManager::DestroyTimerForKey(System.String)
extern void CSTimerManager_DestroyTimerForKey_mAF073C786DF6428E53C071D7ABB82751FF162245 (void);
// 0x000006B3 System.Void CSTimerManager::DestroyTimer(CSTimer)
extern void CSTimerManager_DestroyTimer_mBFB468F26E81812FC9B13BAB9D1F0AAD0E6F6BC5 (void);
// 0x000006B4 System.Void CSTimerManager::AddTimer(CSTimer)
extern void CSTimerManager_AddTimer_m5E33F24A4BD6F5BFD743C34ECF852432F2BC61F4 (void);
// 0x000006B5 System.Boolean CSTimerManager::SubsricbeToTimer(ICSTimer,System.String)
extern void CSTimerManager_SubsricbeToTimer_m3D17ADA853BF9CD27F10EA8060841670494AE725 (void);
// 0x000006B6 System.Void CSTimerManager::UnsubsricbeFromTimer(ICSTimer,System.String)
extern void CSTimerManager_UnsubsricbeFromTimer_m621A90B83495566C58ECEF00E93B3022B15BF514 (void);
// 0x000006B7 CSTimer CSTimerManager::CreatePresentTimer(System.Double)
extern void CSTimerManager_CreatePresentTimer_mE466A966C7C374A05F3702C8F3CFB603041D769D (void);
// 0x000006B8 System.Void CSTimerManager::DestroyPresentTimer()
extern void CSTimerManager_DestroyPresentTimer_mB07B01E1BBF5CE88A476270DFF98B528A1AEBE11 (void);
// 0x000006B9 CSTimer CSTimerManager::CreateWheelTimer(System.Int32)
extern void CSTimerManager_CreateWheelTimer_m5949428B2B6BB4271CD5E7D4982376ED3E955E34 (void);
// 0x000006BA System.Void CSTimerManager::DestroyWheelTimer()
extern void CSTimerManager_DestroyWheelTimer_m95F628D33CDA04D2829BE3EDA80FC43BA3267DF1 (void);
// 0x000006BB CSTimer CSTimerManager::GetPresentTimer()
extern void CSTimerManager_GetPresentTimer_m15A9D5F4D55B91FEF292FF505581D1B9380F91DB (void);
// 0x000006BC CSTimer CSTimerManager::GetWheelTimer()
extern void CSTimerManager_GetWheelTimer_m0A14D6024AE11FB4DFA77A3A66C1FB38C04B28D6 (void);
// 0x000006BD System.Boolean CSTimerManager::SubsricbeToPresentTimer(ICSTimer)
extern void CSTimerManager_SubsricbeToPresentTimer_mC5F38353E392D963BDF8B4402C20F4493CAE5473 (void);
// 0x000006BE System.Boolean CSTimerManager::SubsricbeToWheelTimer(ICSTimer)
extern void CSTimerManager_SubsricbeToWheelTimer_mF9670E9892A37023E34137D9674EB71BE17EBE84 (void);
// 0x000006BF System.Void CSTimerManager::UnsubsricbeFromPresentTimer(ICSTimer)
extern void CSTimerManager_UnsubsricbeFromPresentTimer_m78DE47DF6416A61BE2CBF948ED8F29765CFADA08 (void);
// 0x000006C0 System.Void CSTimerManager::UnsubsricbeFromWheelTimer(ICSTimer)
extern void CSTimerManager_UnsubsricbeFromWheelTimer_m3B598D2CA7CAD5D7A6ED2099F092BA25B7D5AC80 (void);
// 0x000006C1 System.Void CSTimerManager::.ctor()
extern void CSTimerManager__ctor_m0E7561D7F860DCA0158732C97C732D26B68FBC6C (void);
// 0x000006C2 System.Void CSTimerManager::.cctor()
extern void CSTimerManager__cctor_mC08971E445F5FC0EBA57D60413C7426F25BC5E5C (void);
// 0x000006C3 System.Void CSToggleGroupHelper::Start()
extern void CSToggleGroupHelper_Start_mA3B10B55B8C77B916EB23AD1F710720C238616F6 (void);
// 0x000006C4 System.Void CSToggleGroupHelper::ToggleChanged(UnityEngine.UI.Toggle,System.Boolean)
extern void CSToggleGroupHelper_ToggleChanged_m6B9A5880CB5F2BB3201C01C41051690B860EE358 (void);
// 0x000006C5 System.Void CSToggleGroupHelper::.ctor()
extern void CSToggleGroupHelper__ctor_m6C1387B79C5BB92AD1CE07FFF5CC64C000D2F206 (void);
// 0x000006C6 System.Void CSToggleGroupHelper/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mE5495258D918BEAC8815C1F401E4B657C6A80EF7 (void);
// 0x000006C7 System.Void CSToggleGroupHelper/<>c__DisplayClass3_0::<Start>b__0(System.Boolean)
extern void U3CU3Ec__DisplayClass3_0_U3CStartU3Eb__0_mF2B1395E21F5FEFB668E57CF1CABBC8B05C83D41 (void);
// 0x000006C8 System.Void CSToggleTap::Start()
extern void CSToggleTap_Start_m4194B9168E38AF9B9173516323882E0A6B9CEE88 (void);
// 0x000006C9 System.Void CSToggleTap::.ctor()
extern void CSToggleTap__ctor_m33B9E1D1F8EF53F016F15081420557725C52ABE2 (void);
// 0x000006CA System.Void CSToggleTap/<>c::.cctor()
extern void U3CU3Ec__cctor_m759F11235A281874BCE6B1E05448E85619739437 (void);
// 0x000006CB System.Void CSToggleTap/<>c::.ctor()
extern void U3CU3Ec__ctor_m47F8944E7A5120A93B26946FE1377E7B277AE68F (void);
// 0x000006CC System.Void CSToggleTap/<>c::<Start>b__0_0(System.Boolean)
extern void U3CU3Ec_U3CStartU3Eb__0_0_m38C76ADBC4CBAB0933E9511DD676499597F16202 (void);
// 0x000006CD System.String CSUtilities::FormatNumber(System.Single)
extern void CSUtilities_FormatNumber_m65E38533E89A40AFEA2E2B8319EEE2B906DFB014 (void);
// 0x000006CE T CSUtilities::RandomSymbolValue()
// 0x000006CF LTDescr CSUtilities::AnimateWithFrames(UnityEngine.UI.Image,CSAnimationData,System.Single,System.Boolean)
extern void CSUtilities_AnimateWithFrames_m3A6536CE2BE2AC8AA5ECB1FFDD63F22783E27411 (void);
// 0x000006D0 System.Single CSUtilities::CalculateDuration(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void CSUtilities_CalculateDuration_mA125160DBFCA9DBD31730CF5759FF7E360E42234 (void);
// 0x000006D1 LTDescr CSUtilities::LabelAction(TMPro.TextMeshProUGUI,System.Single,System.Single,System.Boolean,System.Boolean)
extern void CSUtilities_LabelAction_m9536631C599D75A773CFBB9699500D48E3494835 (void);
// 0x000006D2 LTDescr CSUtilities::LabelAction(TMPro.TextMeshProUGUI,System.Single,System.Single,System.Single,System.Boolean,System.Boolean)
extern void CSUtilities_LabelAction_m03FB3607C8041434A112CF6B3F7C510AAA097F6E (void);
// 0x000006D3 System.String CSUtilities::TimeFormat(System.Single,CSTimeProperty)
extern void CSUtilities_TimeFormat_mA4C4899CB77F8574B6F7F36C13F2CF06FF199561 (void);
// 0x000006D4 System.Void CSUtilities::.ctor()
extern void CSUtilities__ctor_m4387E659D9046E628E44FB25F18A151050257F7E (void);
// 0x000006D5 System.Void CSUtilities::.cctor()
extern void CSUtilities__cctor_mE1B92C1D5C90846E539E045C68A0686BD92E90B1 (void);
// 0x000006D6 System.Void CSUtilities/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m61B5ED513EB032DCBBCA33450900EBC70FBC7719 (void);
// 0x000006D7 System.Void CSUtilities/<>c__DisplayClass5_0::<AnimateWithFrames>b__0(System.Single)
extern void U3CU3Ec__DisplayClass5_0_U3CAnimateWithFramesU3Eb__0_m25A151A93527F3B4B992D95E8D9E70C07DDCB353 (void);
// 0x000006D8 System.Void CSUtilities/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m244CA7CF796DDCDDA350001552965A4F683D1FC8 (void);
// 0x000006D9 System.Void CSUtilities/<>c__DisplayClass8_0::<LabelAction>b__0(System.Single)
extern void U3CU3Ec__DisplayClass8_0_U3CLabelActionU3Eb__0_m6519239416D28607D0D674A261549D65F7C0375F (void);
// 0x000006DA System.Void HorizontalScrollSnap::Start()
extern void HorizontalScrollSnap_Start_m80023AEE9DDCBC718CAB66E84BEB4648D4AD7895 (void);
// 0x000006DB System.Void HorizontalScrollSnap::Update()
extern void HorizontalScrollSnap_Update_mDCC17A71E7C4823E99E8ED4DD27A81B1D60C7A29 (void);
// 0x000006DC System.Boolean HorizontalScrollSnap::IsRectMovingSlowerThanThreshold(System.Single)
extern void HorizontalScrollSnap_IsRectMovingSlowerThanThreshold_mE7A843809FE30021E4045A9FD7A28B0FBCAD9A85 (void);
// 0x000006DD System.Void HorizontalScrollSnap::DistributePages()
extern void HorizontalScrollSnap_DistributePages_m28864A2E25760776B5BEF1C8843EF045BB9B666B (void);
// 0x000006DE System.Void HorizontalScrollSnap::AddChild(UnityEngine.GameObject)
extern void HorizontalScrollSnap_AddChild_mAC70C81104E4D1986A8B97D1059D84C9CA197F85 (void);
// 0x000006DF System.Void HorizontalScrollSnap::AddChild(UnityEngine.GameObject,System.Boolean)
extern void HorizontalScrollSnap_AddChild_m644E8EE86D58D542B2E4B308B976A669C20BC12D (void);
// 0x000006E0 System.Void HorizontalScrollSnap::RemoveChild(System.Int32,UnityEngine.GameObject&)
extern void HorizontalScrollSnap_RemoveChild_mDD35AFA7560623F1C4AA2E908F26A9EA8B0ACB2F (void);
// 0x000006E1 System.Void HorizontalScrollSnap::RemoveAllChildren(UnityEngine.GameObject[]&)
extern void HorizontalScrollSnap_RemoveAllChildren_m3B04527F4B659C90A0CC409205968FED1A83627C (void);
// 0x000006E2 System.Void HorizontalScrollSnap::SetScrollContainerPosition()
extern void HorizontalScrollSnap_SetScrollContainerPosition_mBEF5A23EA14AADD69865258AA90FE3A0BFDDD4B2 (void);
// 0x000006E3 System.Void HorizontalScrollSnap::UpdateLayout()
extern void HorizontalScrollSnap_UpdateLayout_mBC3AC45BE51B12A4F35E50FECB5AB77D8B56658A (void);
// 0x000006E4 System.Void HorizontalScrollSnap::OnRectTransformDimensionsChange()
extern void HorizontalScrollSnap_OnRectTransformDimensionsChange_m6A744DB8BAA1DE928FCCD3389DEA1FC451A2DBA3 (void);
// 0x000006E5 System.Void HorizontalScrollSnap::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void HorizontalScrollSnap_OnEndDrag_m6BB57030D8645A6323F91212B9DB6B21D6A74754 (void);
// 0x000006E6 System.Void HorizontalScrollSnap::.ctor()
extern void HorizontalScrollSnap__ctor_m34A5926EF17B61BA53FAC30DAB9CE1B5CCFF26BE (void);
// 0x000006E7 System.Int32 ScrollSnapBase::get_CurrentPage()
extern void ScrollSnapBase_get_CurrentPage_m9B3DE8B11183EE8212AF1318906C744D711A4562 (void);
// 0x000006E8 System.Void ScrollSnapBase::set_CurrentPage(System.Int32)
extern void ScrollSnapBase_set_CurrentPage_mE3B8B097A054EDD9129187B8210DC96368B779FC (void);
// 0x000006E9 ScrollSnapBase/SelectionChangeStartEvent ScrollSnapBase::get_OnSelectionChangeStartEvent()
extern void ScrollSnapBase_get_OnSelectionChangeStartEvent_mC2F99AF2296D3FC0D1E786470B9BD8947F21827C (void);
// 0x000006EA System.Void ScrollSnapBase::set_OnSelectionChangeStartEvent(ScrollSnapBase/SelectionChangeStartEvent)
extern void ScrollSnapBase_set_OnSelectionChangeStartEvent_m7334D49D8D3C1991138848E4C960F188F730CC09 (void);
// 0x000006EB ScrollSnapBase/SelectionPageChangedEvent ScrollSnapBase::get_OnSelectionPageChangedEvent()
extern void ScrollSnapBase_get_OnSelectionPageChangedEvent_m525D2766053E986DD79F3A32A5CC0DEBCCF5F956 (void);
// 0x000006EC System.Void ScrollSnapBase::set_OnSelectionPageChangedEvent(ScrollSnapBase/SelectionPageChangedEvent)
extern void ScrollSnapBase_set_OnSelectionPageChangedEvent_m61532F77155F678EA1EE86FCB36AE63F6CB9A5AA (void);
// 0x000006ED ScrollSnapBase/SelectionChangeEndEvent ScrollSnapBase::get_OnSelectionChangeEndEvent()
extern void ScrollSnapBase_get_OnSelectionChangeEndEvent_m22028CF7165330B5E1DD0BB393D30B7DBB290CC1 (void);
// 0x000006EE System.Void ScrollSnapBase::set_OnSelectionChangeEndEvent(ScrollSnapBase/SelectionChangeEndEvent)
extern void ScrollSnapBase_set_OnSelectionChangeEndEvent_mAD7AF338B88F446F4313B93119884462558B688C (void);
// 0x000006EF System.Void ScrollSnapBase::Awake()
extern void ScrollSnapBase_Awake_mA83FA22D2B1BE1FA483B0FD5700058EF7AE05CA5 (void);
// 0x000006F0 System.Void ScrollSnapBase::InitialiseChildObjectsFromScene()
extern void ScrollSnapBase_InitialiseChildObjectsFromScene_m98A082721B861E645E6581A83EA599CE2DC45BF4 (void);
// 0x000006F1 System.Void ScrollSnapBase::InitialiseChildObjectsFromArray()
extern void ScrollSnapBase_InitialiseChildObjectsFromArray_mD31EC067271250D3E458461DCEC8447FEC556FCB (void);
// 0x000006F2 System.Void ScrollSnapBase::UpdateVisible()
extern void ScrollSnapBase_UpdateVisible_mCD0E1732CBBCACD6EE0DF8EE7465E26132AD75C8 (void);
// 0x000006F3 System.Void ScrollSnapBase::NextScreen()
extern void ScrollSnapBase_NextScreen_mFDBCB916020C6531491AD59F5A6F11B1C4DF545C (void);
// 0x000006F4 System.Void ScrollSnapBase::PreviousScreen()
extern void ScrollSnapBase_PreviousScreen_m15A2B1D9E4BFB8C608CD188BA0BE7518499A6E2E (void);
// 0x000006F5 System.Void ScrollSnapBase::GoToScreen(System.Int32)
extern void ScrollSnapBase_GoToScreen_mC7F3B882B8B0DCC0C91CF05EA3663B8B05DA52F7 (void);
// 0x000006F6 System.Int32 ScrollSnapBase::GetPageforPosition(UnityEngine.Vector3)
extern void ScrollSnapBase_GetPageforPosition_mC1F3151D884790B82A944B8BE5023D006F1DCF6F (void);
// 0x000006F7 System.Boolean ScrollSnapBase::IsRectSettledOnaPage(UnityEngine.Vector3)
extern void ScrollSnapBase_IsRectSettledOnaPage_m37D4CB53D73B909D94D0714111347CCA500FE567 (void);
// 0x000006F8 System.Void ScrollSnapBase::GetPositionforPage(System.Int32,UnityEngine.Vector3&)
extern void ScrollSnapBase_GetPositionforPage_mEE8992489F81C0F77397B50CADD6B3BC866C0B5F (void);
// 0x000006F9 System.Void ScrollSnapBase::ScrollToClosestElement()
extern void ScrollSnapBase_ScrollToClosestElement_mB8F27BF8A30152817988C0552C13EE52FE23E6F1 (void);
// 0x000006FA System.Void ScrollSnapBase::ChangeBulletsInfo(System.Int32)
extern void ScrollSnapBase_ChangeBulletsInfo_mCA6A55BEF6BD9AC1514B6ED84B8D43F57D3521DE (void);
// 0x000006FB System.Void ScrollSnapBase::OnValidate()
extern void ScrollSnapBase_OnValidate_mFA23F26882A4D11AAE56594C680AFB1429690739 (void);
// 0x000006FC System.Void ScrollSnapBase::StartScreenChange()
extern void ScrollSnapBase_StartScreenChange_mA3AC6E65D454803EB3C4CE1334B9F7716EA72C25 (void);
// 0x000006FD System.Void ScrollSnapBase::ScreenChange()
extern void ScrollSnapBase_ScreenChange_mE184FC874FA10E59C9A64D0BD2342F0E550C0F76 (void);
// 0x000006FE System.Void ScrollSnapBase::EndScreenChange()
extern void ScrollSnapBase_EndScreenChange_mEBB5CBB9157C82C13BB511AA541A140322C7BC07 (void);
// 0x000006FF System.Void ScrollSnapBase::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void ScrollSnapBase_OnBeginDrag_mF9AB1D5314BEFC675392F8BAAA0E9BAC0CD70EFC (void);
// 0x00000700 System.Void ScrollSnapBase::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void ScrollSnapBase_OnDrag_mE399D592D4D44A6417B458440A1033D31FF0F02D (void);
// 0x00000701 System.Void ScrollSnapBase::.ctor()
extern void ScrollSnapBase__ctor_mF107F70E03A9D45A6A4A0690BCB167450E782751 (void);
// 0x00000702 System.Void ScrollSnapBase::<Awake>b__51_0()
extern void ScrollSnapBase_U3CAwakeU3Eb__51_0_mA8343B4EB63458F2C2B9261B675535D8DFD1B9B9 (void);
// 0x00000703 System.Void ScrollSnapBase::<Awake>b__51_1()
extern void ScrollSnapBase_U3CAwakeU3Eb__51_1_mABFF0A12D4D0ADBDEA9A7AC45E26DAB17EFFD8D0 (void);
// 0x00000704 System.Void ScrollSnapBase/SelectionChangeStartEvent::.ctor()
extern void SelectionChangeStartEvent__ctor_m92D2ED42EE1ACFDC97CC2D36080D59DEC9F06237 (void);
// 0x00000705 System.Void ScrollSnapBase/SelectionPageChangedEvent::.ctor()
extern void SelectionPageChangedEvent__ctor_m32BEB6EE4F4AC30C2F8A9F6B787EA62174FCC1F5 (void);
// 0x00000706 System.Void ScrollSnapBase/SelectionChangeEndEvent::.ctor()
extern void SelectionChangeEndEvent__ctor_m82148ECD356BCC1188582E8E685B0C84F0314F44 (void);
// 0x00000707 System.Single CSZLGamble::get_bet()
extern void CSZLGamble_get_bet_m22EBC18F79A5F80DB19AF67E41E334C564B575B9 (void);
// 0x00000708 System.Void CSZLGamble::set_bet(System.Single)
extern void CSZLGamble_set_bet_m7CA94F11501E6F82964D14189905ECDE0B4E36A9 (void);
// 0x00000709 System.Single CSZLGamble::get_bank()
extern void CSZLGamble_get_bank_m4D2AC7F1842ADCFA07470E837AED479622BB7FFF (void);
// 0x0000070A System.Void CSZLGamble::set_bank(System.Single)
extern void CSZLGamble_set_bank_mC3295A159C610CAD8FB48CA7DDB49BE29205EB13 (void);
// 0x0000070B System.Boolean CSZLGamble::get_interactable()
extern void CSZLGamble_get_interactable_m877FBB2EFC2FCBA1C4527839D3D4C9E6A3E60DD8 (void);
// 0x0000070C System.Void CSZLGamble::set_interactable(System.Boolean)
extern void CSZLGamble_set_interactable_m1EA4915FE55F68E65F0D2AB14FDD6FC6A5145B3A (void);
// 0x0000070D System.Boolean CSZLGamble::get_enable()
extern void CSZLGamble_get_enable_m43487C216A45C23C87BFE5F08CDFDE119EA3811F (void);
// 0x0000070E System.Void CSZLGamble::set_enable(System.Boolean)
extern void CSZLGamble_set_enable_mE8528FC815D7F3B1076FF2E784835BC97379235E (void);
// 0x0000070F System.Void CSZLGamble::Appear(System.Single,System.Action)
extern void CSZLGamble_Appear_mA852224A295112A226237703350A44FC0384C7AB (void);
// 0x00000710 System.Void CSZLGamble::Disappear(System.Action)
extern void CSZLGamble_Disappear_mFADEE9D6BDA71A327DC1BBBFF02E78AB8C13989F (void);
// 0x00000711 System.Void CSZLGamble::CreateContent()
extern void CSZLGamble_CreateContent_m2989F00F337B1694529762F793EEA5A7697E8B09 (void);
// 0x00000712 System.Void CSZLGamble::DestroyContent()
extern void CSZLGamble_DestroyContent_mC3091332F5012A500378BBE6C3F82D36D4282504 (void);
// 0x00000713 System.Void CSZLGamble::BackgroundAlpha(System.Single)
extern void CSZLGamble_BackgroundAlpha_mE4F068BA07239661A99E90ECA48CF76A7A9DA1DB (void);
// 0x00000714 System.Void CSZLGamble::CardSelected(System.Boolean,CSCard,CSCard)
extern void CSZLGamble_CardSelected_mD7E7D5948D54BB19AD583244619757DA3106835E (void);
// 0x00000715 System.Void CSZLGamble::ContentResult(System.Boolean,CSZLGambleContent)
extern void CSZLGamble_ContentResult_m312C441D6A227A353FE1A3949A6139C0BE6B4ABD (void);
// 0x00000716 System.Void CSZLGamble::OnCollect()
extern void CSZLGamble_OnCollect_m289701076BA9FAA0D16DCFC5C9E3586692134D24 (void);
// 0x00000717 System.Void CSZLGamble::.ctor()
extern void CSZLGamble__ctor_m7383A9616E0F108BBA8D3E8DAA0CCE6A436D16CB (void);
// 0x00000718 System.Void CSZLGamble/<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_m4695728A81161DCAA2401F0B71042DD08AB5970B (void);
// 0x00000719 System.Void CSZLGamble/<>c__DisplayClass26_0::<Appear>b__0()
extern void U3CU3Ec__DisplayClass26_0_U3CAppearU3Eb__0_m4C053283E1196E7C73800F57F6203F3F5D5E9A45 (void);
// 0x0000071A System.Void CSZLGamble/<>c__DisplayClass27_0::.ctor()
extern void U3CU3Ec__DisplayClass27_0__ctor_m277F097813134417D162400B951DE467A9F9D27B (void);
// 0x0000071B System.Void CSZLGamble/<>c__DisplayClass27_0::<Disappear>b__0()
extern void U3CU3Ec__DisplayClass27_0_U3CDisappearU3Eb__0_mFD542E11BDDE242572AEC4DB68597FA66CB89C17 (void);
// 0x0000071C System.Void CSZLGambleAlert::Appear(System.Single)
extern void CSZLGambleAlert_Appear_m6E383248A258377DC45842A2CF0D8FAD27CBDBEC (void);
// 0x0000071D System.Void CSZLGambleAlert::OnCollect()
extern void CSZLGambleAlert_OnCollect_mEED8476F2F92B421409ACB43B9F7AE1C0D265704 (void);
// 0x0000071E System.Void CSZLGambleAlert::.ctor()
extern void CSZLGambleAlert__ctor_m4F2B55B4AEBB9EEAE99D4B2AF975C99FAFB181AD (void);
// 0x0000071F System.Void CSZLGambleContent::add_ResultEvent(System.Action`2<System.Boolean,CSZLGambleContent>)
extern void CSZLGambleContent_add_ResultEvent_m4B23C6DCA6A110B76DB41457AFF0C313E2472A57 (void);
// 0x00000720 System.Void CSZLGambleContent::remove_ResultEvent(System.Action`2<System.Boolean,CSZLGambleContent>)
extern void CSZLGambleContent_remove_ResultEvent_mB0FD442349C7C4A0629EAF1C3E19CE2A1E235893 (void);
// 0x00000721 System.Void CSZLGambleContent::add_CardSelectedEvent(System.Action`3<System.Boolean,CSCard,CSCard>)
extern void CSZLGambleContent_add_CardSelectedEvent_m973E8FA9EB3B9E792BA462C985B7F829A3C34CAF (void);
// 0x00000722 System.Void CSZLGambleContent::remove_CardSelectedEvent(System.Action`3<System.Boolean,CSCard,CSCard>)
extern void CSZLGambleContent_remove_CardSelectedEvent_m43030534131D742C28DD2FA9C9B095C51E878181 (void);
// 0x00000723 System.Boolean CSZLGambleContent::get_enable()
extern void CSZLGambleContent_get_enable_m038E01B9DFF6613F066453FBFAA5332252CE85DB (void);
// 0x00000724 System.Void CSZLGambleContent::set_enable(System.Boolean)
extern void CSZLGambleContent_set_enable_mE025802673DB38F12F81862B9255F39468E01535 (void);
// 0x00000725 System.Void CSZLGambleContent::Awake()
extern void CSZLGambleContent_Awake_mA42A8CEF7C3B348529E5286272D630F3FCAA5D2C (void);
// 0x00000726 UnityEngine.Transform[] CSZLGambleContent::CreateCards(System.Int32)
extern void CSZLGambleContent_CreateCards_m8344DBE19F2CAEEFA7EA05341DFA30FCE73F7162 (void);
// 0x00000727 System.Void CSZLGambleContent::MoveCards(UnityEngine.Transform[],System.Single,System.Action)
extern void CSZLGambleContent_MoveCards_mFC49FC4B6B7CA5143608AAC148E4AD3E784D2E9E (void);
// 0x00000728 System.Void CSZLGambleContent::Clean()
extern void CSZLGambleContent_Clean_m5D8DE0E69FB82A42020010A85074F1A75BB928FE (void);
// 0x00000729 UnityEngine.Transform CSZLGambleContent::CardAtIdx(System.Int32,System.Int32)
extern void CSZLGambleContent_CardAtIdx_m73E285D2CBC2F390F45A3BA9CC095E99C049BE9E (void);
// 0x0000072A UnityEngine.Vector2 CSZLGambleContent::PositionForIdx(System.Int32,System.Int32,System.Single)
extern void CSZLGambleContent_PositionForIdx_m3F1F8FC26930F83205969C5759FA4680C38F1852 (void);
// 0x0000072B System.Collections.Generic.List`1<CSCardValue> CSZLGambleContent::CreateCardDeck()
extern void CSZLGambleContent_CreateCardDeck_mB3242135F8B7A5284EC17BFC5C789B58BC5517F7 (void);
// 0x0000072C CSCardValue CSZLGambleContent::RandomCardValue()
extern void CSZLGambleContent_RandomCardValue_mBC92E7948350EB6B18F799C3C8D753EB41797D3A (void);
// 0x0000072D CSCardValue CSZLGambleContent::FirstCardValue(CSRank,CSRank)
extern void CSZLGambleContent_FirstCardValue_mADCA20A4C2FF34D568E4D4300293B3C59E472750 (void);
// 0x0000072E System.Void CSZLGambleContent::CardSelected(CSCard)
extern void CSZLGambleContent_CardSelected_m790D4303995B0A253B44958128E2CA2C61B7F0F8 (void);
// 0x0000072F System.Void CSZLGambleContent::ResultAnimation(CSCard,CSCard)
extern void CSZLGambleContent_ResultAnimation_mA17DDD2D2A2053EFDB407170E15D1EEC628E03FB (void);
// 0x00000730 System.Boolean CSZLGambleContent::IsWin(CSCard,CSCard)
extern void CSZLGambleContent_IsWin_m5EAD012141FFACCE54A6C7C79598F6A335EAB61B (void);
// 0x00000731 System.Void CSZLGambleContent::.ctor()
extern void CSZLGambleContent__ctor_m0AE3BD259A5A7939B6D36F0529BEAF766D909E7F (void);
// 0x00000732 System.Void CSZLGambleContent::<Awake>b__15_0()
extern void CSZLGambleContent_U3CAwakeU3Eb__15_0_m7889AAAF61BABB9CDBDE6455352FDF28FDFB175F (void);
// 0x00000733 System.Void CSZLGambleContent/<>c::.cctor()
extern void U3CU3Ec__cctor_mFDAD6EF5DA3F3E4EB3D500F6D289547DA69A14DC (void);
// 0x00000734 System.Void CSZLGambleContent/<>c::.ctor()
extern void U3CU3Ec__ctor_m5CD2DD63DE8E91DDFB81D216D1A87333C4F8CB5B (void);
// 0x00000735 System.Void CSZLGambleContent/<>c::<MoveCards>b__17_0()
extern void U3CU3Ec_U3CMoveCardsU3Eb__17_0_mA6B0835478334161AC447C8B9F73C2473EC48BBE (void);
// 0x00000736 System.Void CSZLGambleContent/<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_mF8D1ECE0C585D63C5B28605CC2E7C2F0E941D482 (void);
// 0x00000737 System.Void CSZLGambleContent/<>c__DisplayClass24_0::<CardSelected>b__0()
extern void U3CU3Ec__DisplayClass24_0_U3CCardSelectedU3Eb__0_mA75456B6FEAC6ADF8AF3C6BB1A9760E2223BBE9C (void);
// 0x00000738 System.Void CSZLGambleContent/<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_m9F066F31A5D4DCB79EFE8842EA1E12833054CFCB (void);
// 0x00000739 System.Void CSZLGambleContent/<>c__DisplayClass25_0::<ResultAnimation>b__0()
extern void U3CU3Ec__DisplayClass25_0_U3CResultAnimationU3Eb__0_mA30C4CFA945BB78BC492400478F4079C20CBBC34 (void);
// 0x0000073A System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2 (void);
// 0x0000073B System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B (void);
// 0x0000073C System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371 (void);
// 0x0000073D System.Void ChatController::.ctor()
extern void ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026 (void);
// 0x0000073E System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93 (void);
// 0x0000073F System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE (void);
// 0x00000740 System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC (void);
// 0x00000741 System.Void EnvMapAnimator/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23 (void);
// 0x00000742 System.Void EnvMapAnimator/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B (void);
// 0x00000743 System.Boolean EnvMapAnimator/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE (void);
// 0x00000744 System.Object EnvMapAnimator/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E (void);
// 0x00000745 System.Void EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB (void);
// 0x00000746 System.Object EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF (void);
// 0x00000747 System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226 (void);
// 0x00000748 System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E (void);
// 0x00000749 System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1 (void);
// 0x0000074A System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3 (void);
// 0x0000074B TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385 (void);
// 0x0000074C System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler/CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4 (void);
// 0x0000074D TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B (void);
// 0x0000074E System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler/SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788 (void);
// 0x0000074F TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A (void);
// 0x00000750 System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler/WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076 (void);
// 0x00000751 TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D (void);
// 0x00000752 System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler/LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA (void);
// 0x00000753 TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB (void);
// 0x00000754 System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler/LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5 (void);
// 0x00000755 System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70 (void);
// 0x00000756 System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11 (void);
// 0x00000757 System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC (void);
// 0x00000758 System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44 (void);
// 0x00000759 System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC (void);
// 0x0000075A System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53 (void);
// 0x0000075B System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66 (void);
// 0x0000075C System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43 (void);
// 0x0000075D System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77 (void);
// 0x0000075E System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8 (void);
// 0x0000075F System.Void TMPro.TMP_TextEventHandler/CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7 (void);
// 0x00000760 System.Void TMPro.TMP_TextEventHandler/SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2 (void);
// 0x00000761 System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C (void);
// 0x00000762 System.Void TMPro.TMP_TextEventHandler/LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018 (void);
// 0x00000763 System.Void TMPro.TMP_TextEventHandler/LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0 (void);
// 0x00000764 System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C (void);
// 0x00000765 System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F (void);
// 0x00000766 System.Void TMPro.Examples.Benchmark01/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB (void);
// 0x00000767 System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73 (void);
// 0x00000768 System.Boolean TMPro.Examples.Benchmark01/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C (void);
// 0x00000769 System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591 (void);
// 0x0000076A System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B (void);
// 0x0000076B System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E (void);
// 0x0000076C System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4 (void);
// 0x0000076D System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376 (void);
// 0x0000076E System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651 (void);
// 0x0000076F System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561 (void);
// 0x00000770 System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1 (void);
// 0x00000771 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD (void);
// 0x00000772 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA (void);
// 0x00000773 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2 (void);
// 0x00000774 System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046 (void);
// 0x00000775 System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5 (void);
// 0x00000776 System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77 (void);
// 0x00000777 System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516 (void);
// 0x00000778 System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE (void);
// 0x00000779 System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9 (void);
// 0x0000077A System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C (void);
// 0x0000077B System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E (void);
// 0x0000077C System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610 (void);
// 0x0000077D System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373 (void);
// 0x0000077E System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3 (void);
// 0x0000077F System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124 (void);
// 0x00000780 System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA (void);
// 0x00000781 System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3 (void);
// 0x00000782 System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC (void);
// 0x00000783 System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8 (void);
// 0x00000784 System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2 (void);
// 0x00000785 System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469 (void);
// 0x00000786 System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64 (void);
// 0x00000787 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A (void);
// 0x00000788 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B (void);
// 0x00000789 System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF (void);
// 0x0000078A System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390 (void);
// 0x0000078B System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B (void);
// 0x0000078C System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362 (void);
// 0x0000078D System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F (void);
// 0x0000078E System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5 (void);
// 0x0000078F System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697 (void);
// 0x00000790 System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB (void);
// 0x00000791 System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F (void);
// 0x00000792 UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3 (void);
// 0x00000793 System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3 (void);
// 0x00000794 System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86 (void);
// 0x00000795 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F (void);
// 0x00000796 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF (void);
// 0x00000797 System.Boolean TMPro.Examples.SkewTextExample/<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF (void);
// 0x00000798 System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1 (void);
// 0x00000799 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F (void);
// 0x0000079A System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A (void);
// 0x0000079B System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE (void);
// 0x0000079C System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7 (void);
// 0x0000079D System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF (void);
// 0x0000079E System.Void TMPro.Examples.TeleType/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629 (void);
// 0x0000079F System.Void TMPro.Examples.TeleType/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9 (void);
// 0x000007A0 System.Boolean TMPro.Examples.TeleType/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715 (void);
// 0x000007A1 System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B (void);
// 0x000007A2 System.Void TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8 (void);
// 0x000007A3 System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261 (void);
// 0x000007A4 System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0 (void);
// 0x000007A5 System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA (void);
// 0x000007A6 System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50 (void);
// 0x000007A7 System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712 (void);
// 0x000007A8 System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8 (void);
// 0x000007A9 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534 (void);
// 0x000007AA System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4 (void);
// 0x000007AB System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848 (void);
// 0x000007AC System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D (void);
// 0x000007AD System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C (void);
// 0x000007AE System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3 (void);
// 0x000007AF System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83 (void);
// 0x000007B0 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691 (void);
// 0x000007B1 System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F (void);
// 0x000007B2 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0 (void);
// 0x000007B3 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8 (void);
// 0x000007B4 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210 (void);
// 0x000007B5 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB (void);
// 0x000007B6 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F (void);
// 0x000007B7 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27 (void);
// 0x000007B8 System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046 (void);
// 0x000007B9 System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1 (void);
// 0x000007BA System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7 (void);
// 0x000007BB System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF (void);
// 0x000007BC System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E (void);
// 0x000007BD System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12__ctor_mA27BBC06A409A9D9FB03E0D2C74677486B80D168 (void);
// 0x000007BE System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_IDisposable_Dispose_m510FB73335FCBCBEC6AC61A4F2A0217722BF27FC (void);
// 0x000007BF System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_MoveNext_m8DFB6850F5F9B8DF05173D4CB9C76B997EC87B57 (void);
// 0x000007C0 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCD7B5218C81C0872AB501CD54626F1284A4F73BF (void);
// 0x000007C1 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_Reset_mD2D32B94CA8D5A6D502BA38BDE1969C856368F73 (void);
// 0x000007C2 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_get_Current_m59B2488B0E3C72CE97659E8D19BB13C6E0A44E90 (void);
// 0x000007C3 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__13__ctor_m359EAC129649F98C759B341846A6DC07F95230D2 (void);
// 0x000007C4 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_IDisposable_Dispose_m36DE8722B670A7DDD9E70107024590B20A4E0B18 (void);
// 0x000007C5 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_MoveNext_mB7D320C272AAA835590B8460DA89DEBC0A243815 (void);
// 0x000007C6 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m891591B80D28B1DF2CF9EB78C19DA672A81231A2 (void);
// 0x000007C7 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_Reset_m5EF21CA1E0C64E67D18D9355B5E064C93452F5B2 (void);
// 0x000007C8 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_get_Current_m4850A72D887C4DA3F53CA1A1A2BEAD5C5C6605A1 (void);
// 0x000007C9 System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB (void);
// 0x000007CA System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA (void);
// 0x000007CB System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1 (void);
// 0x000007CC System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC (void);
// 0x000007CD System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB (void);
// 0x000007CE System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF (void);
// 0x000007CF System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4 (void);
// 0x000007D0 System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB (void);
// 0x000007D1 System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12 (void);
// 0x000007D2 System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3 (void);
// 0x000007D3 System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720 (void);
// 0x000007D4 System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21 (void);
// 0x000007D5 System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1 (void);
// 0x000007D6 System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C (void);
// 0x000007D7 System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF (void);
// 0x000007D8 System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A (void);
// 0x000007D9 System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D (void);
// 0x000007DA System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0 (void);
// 0x000007DB System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B (void);
// 0x000007DC System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363 (void);
// 0x000007DD System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C (void);
// 0x000007DE System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B (void);
// 0x000007DF System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE (void);
// 0x000007E0 System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943 (void);
// 0x000007E1 System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD (void);
// 0x000007E2 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0 (void);
// 0x000007E3 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66 (void);
// 0x000007E4 System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF (void);
// 0x000007E5 System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A (void);
// 0x000007E6 System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB (void);
// 0x000007E7 System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781 (void);
// 0x000007E8 System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910 (void);
// 0x000007E9 System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691 (void);
// 0x000007EA System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D (void);
// 0x000007EB System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F (void);
// 0x000007EC System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F (void);
// 0x000007ED System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC (void);
// 0x000007EE System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2 (void);
// 0x000007EF System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33 (void);
// 0x000007F0 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E (void);
// 0x000007F1 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944 (void);
// 0x000007F2 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749 (void);
// 0x000007F3 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647 (void);
// 0x000007F4 System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84 (void);
// 0x000007F5 System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC (void);
// 0x000007F6 System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66 (void);
// 0x000007F7 System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42 (void);
// 0x000007F8 System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9 (void);
// 0x000007F9 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F (void);
// 0x000007FA System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007 (void);
// 0x000007FB System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6 (void);
// 0x000007FC System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51 (void);
// 0x000007FD System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932 (void);
// 0x000007FE System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52 (void);
// 0x000007FF System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F (void);
// 0x00000800 System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825 (void);
// 0x00000801 System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846 (void);
// 0x00000802 System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97 (void);
// 0x00000803 System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549 (void);
// 0x00000804 System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16 (void);
// 0x00000805 System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE (void);
// 0x00000806 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8 (void);
// 0x00000807 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099 (void);
// 0x00000808 System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B (void);
// 0x00000809 System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479 (void);
// 0x0000080A System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D (void);
// 0x0000080B System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF (void);
// 0x0000080C System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2 (void);
// 0x0000080D System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA (void);
// 0x0000080E System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2 (void);
// 0x0000080F System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE (void);
// 0x00000810 System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E (void);
// 0x00000811 System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599 (void);
// 0x00000812 System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62 (void);
// 0x00000813 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2 (void);
// 0x00000814 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81 (void);
// 0x00000815 System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F (void);
// 0x00000816 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE (void);
// 0x00000817 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA (void);
// 0x00000818 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E (void);
// 0x00000819 System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5 (void);
// 0x0000081A System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971 (void);
// 0x0000081B System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4 (void);
// 0x0000081C System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1 (void);
// 0x0000081D System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5 (void);
// 0x0000081E System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87 (void);
// 0x0000081F System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC (void);
// 0x00000820 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760 (void);
// 0x00000821 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC (void);
// 0x00000822 System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E (void);
// 0x00000823 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D (void);
// 0x00000824 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB (void);
// 0x00000825 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD (void);
// 0x00000826 System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C (void);
// 0x00000827 System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC (void);
// 0x00000828 System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884 (void);
// 0x00000829 System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C (void);
// 0x0000082A System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F (void);
// 0x0000082B System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED (void);
// 0x0000082C System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2 (void);
// 0x0000082D System.Void TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348 (void);
// 0x0000082E System.Int32 TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107 (void);
// 0x0000082F System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2 (void);
// 0x00000830 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F (void);
// 0x00000831 System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66 (void);
// 0x00000832 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9 (void);
// 0x00000833 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C (void);
// 0x00000834 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F (void);
// 0x00000835 System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0 (void);
// 0x00000836 System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4 (void);
// 0x00000837 UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB (void);
// 0x00000838 System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2 (void);
// 0x00000839 System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099 (void);
// 0x0000083A System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49 (void);
// 0x0000083B System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60 (void);
// 0x0000083C System.Boolean TMPro.Examples.WarpTextExample/<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84 (void);
// 0x0000083D System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1 (void);
// 0x0000083E System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535 (void);
// 0x0000083F System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25 (void);
// 0x00000840 System.Void DentedPixel.LeanDummy::.ctor()
extern void LeanDummy__ctor_mD0BA3A71589D10708BAF2C24160EC0305D014DBE (void);
// 0x00000841 System.Void DentedPixel.LTExamples.PathBezier::OnEnable()
extern void PathBezier_OnEnable_m5E057D877378F11B064309894D1AE8BB524F4771 (void);
// 0x00000842 System.Void DentedPixel.LTExamples.PathBezier::Start()
extern void PathBezier_Start_m17B69DE04B368B76932DC991342B633A7F0A9334 (void);
// 0x00000843 System.Void DentedPixel.LTExamples.PathBezier::Update()
extern void PathBezier_Update_m3827BD355D4006FA740F2B9F1EDD8C426CB43A91 (void);
// 0x00000844 System.Void DentedPixel.LTExamples.PathBezier::OnDrawGizmos()
extern void PathBezier_OnDrawGizmos_mBC48C0867056FB925C45FF97187F8AB4C4D6F13C (void);
// 0x00000845 System.Void DentedPixel.LTExamples.PathBezier::.ctor()
extern void PathBezier__ctor_m64C0684464ED26D96A5AE82DEA44C6ADF0F2085A (void);
// 0x00000846 System.Void DentedPixel.LTExamples.TestingUnitTests::Awake()
extern void TestingUnitTests_Awake_m93E88F38DA88C64CC12ABAE80900B948E5BC30E1 (void);
// 0x00000847 System.Void DentedPixel.LTExamples.TestingUnitTests::Start()
extern void TestingUnitTests_Start_m8C9EF37C5BC5461665D44564D0FE7FB3A6C69A66 (void);
// 0x00000848 UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cubeNamed(System.String)
extern void TestingUnitTests_cubeNamed_m3948700DD2B6420F25DFC461AB2A2A5357E792D6 (void);
// 0x00000849 System.Collections.IEnumerator DentedPixel.LTExamples.TestingUnitTests::timeBasedTesting()
extern void TestingUnitTests_timeBasedTesting_mBD8B52CEA7286C9F4FAAA419C773B78255E55F4D (void);
// 0x0000084A System.Collections.IEnumerator DentedPixel.LTExamples.TestingUnitTests::lotsOfCancels()
extern void TestingUnitTests_lotsOfCancels_mD21D0D0D0B8BE316E58C412A19ADE6658DAA5134 (void);
// 0x0000084B System.Collections.IEnumerator DentedPixel.LTExamples.TestingUnitTests::pauseTimeNow()
extern void TestingUnitTests_pauseTimeNow_m3293FFC3DDEC01D507093F2D2B8E34B40FC0DD7F (void);
// 0x0000084C System.Void DentedPixel.LTExamples.TestingUnitTests::rotateRepeatFinished()
extern void TestingUnitTests_rotateRepeatFinished_m3ED0D0A9DF982711F7D8D670535F1136CB7E3BA0 (void);
// 0x0000084D System.Void DentedPixel.LTExamples.TestingUnitTests::rotateRepeatAllFinished()
extern void TestingUnitTests_rotateRepeatAllFinished_mA343A62B2E6F1D84DAD61BF7DAD6DDC4F24A5077 (void);
// 0x0000084E System.Void DentedPixel.LTExamples.TestingUnitTests::eventGameObjectCalled(LTEvent)
extern void TestingUnitTests_eventGameObjectCalled_mAFA0C3AF3614A013DC2BB8B1F82E6FB121FE56AF (void);
// 0x0000084F System.Void DentedPixel.LTExamples.TestingUnitTests::eventGeneralCalled(LTEvent)
extern void TestingUnitTests_eventGeneralCalled_m9054A75A4B6E1A3BE3E393F88168FAEA8B34D65E (void);
// 0x00000850 System.Void DentedPixel.LTExamples.TestingUnitTests::.ctor()
extern void TestingUnitTests__ctor_m7108B4718BC8D59EC59A4EF627657842243EBB27 (void);
// 0x00000851 System.Void DentedPixel.LTExamples.TestingUnitTests::<lotsOfCancels>b__25_0()
extern void TestingUnitTests_U3ClotsOfCancelsU3Eb__25_0_m79612DDE2FFAD2A0986AA0D91A768BA96269D4F6 (void);
// 0x00000852 System.Void DentedPixel.LTExamples.TestingUnitTests::<pauseTimeNow>b__26_1()
extern void TestingUnitTests_U3CpauseTimeNowU3Eb__26_1_mB0C630E660D38F819734F11E2998CDE43590D503 (void);
// 0x00000853 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m85E81FE7FB95A8484DB589E78AB8F1B684592DDF (void);
// 0x00000854 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__0()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__0_m0E29967BE8C8986E397C440673E2793E2569089B (void);
// 0x00000855 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__1()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__1_m887D60815BE91C503B1B557775A0633F1B60E86E (void);
// 0x00000856 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__21()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__21_mD56FCF9AA4FAE66DF497E1779D9334E757A222E5 (void);
// 0x00000857 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__2()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__2_m25A626FC5B5176333408538B200230F7A012FCF1 (void);
// 0x00000858 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__4()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__4_mC5E35367A4E3D78731DCB8D56BC172E263F40648 (void);
// 0x00000859 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__5()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__5_m31888B2277D8EEE835811D9FF81FA68312AA086B (void);
// 0x0000085A System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__6()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__6_mE7251F18767B392C7011C9893101E7498D8B0C29 (void);
// 0x0000085B System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__8()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__8_mCAE5BA94A4F65FFE4C9F531C9AA0249D6F7B9218 (void);
// 0x0000085C System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__9()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__9_m4AA88BFFB46D1E0E6212B98A76E7A0F6AA5C0039 (void);
// 0x0000085D System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__10()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__10_m561C045E5C1DC104420555CF0BA62F1DF6B671E8 (void);
// 0x0000085E System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__11()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__11_m81593E9FC9183EC0E37356C733A02C3266B87796 (void);
// 0x0000085F System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__13(System.Object)
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__13_mF54BD21F02ED31763F697C67DB121D4646CC7522 (void);
// 0x00000860 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__14()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__14_m5A91E5886E31BA74327DDC19A47BB2E14DB7E810 (void);
// 0x00000861 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__15()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__15_mF1644C8D1D721BD4651678F2F71ACCA470CA60D9 (void);
// 0x00000862 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__16()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__16_m347E0C3ED3E8390844A251CE7CDAE009166D0358 (void);
// 0x00000863 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__17()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__17_m5404D4E64F471E4BB3E95B2DBF8C78AEDCD6C4FC (void);
// 0x00000864 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__19(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__19_mFCD1C9E7E22F2F6710CB151C14E7C6094B6F0EF2 (void);
// 0x00000865 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_0::<Start>b__20()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__20_m07D0BA90D12217971B1F53FF20AA9B02391A4877 (void);
// 0x00000866 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_1::.ctor()
extern void U3CU3Ec__DisplayClass22_1__ctor_m42740028F7DBAC0910C2B0D8FE48AE424104B01F (void);
// 0x00000867 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_1::<Start>b__23()
extern void U3CU3Ec__DisplayClass22_1_U3CStartU3Eb__23_m71B69B2DE70762D5765B748E1B062290289D9195 (void);
// 0x00000868 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_2::.ctor()
extern void U3CU3Ec__DisplayClass22_2__ctor_m92BFBC8093B249E40CB64FABE4D548B594B0A5A0 (void);
// 0x00000869 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass22_2::<Start>b__24(System.Object)
extern void U3CU3Ec__DisplayClass22_2_U3CStartU3Eb__24_mF27E3CE748DB0EEE7037C4DD812DD75BAE86B46A (void);
// 0x0000086A System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::.cctor()
extern void U3CU3Ec__cctor_m6F153F2D37E38349116C0879DD2F597DEC855ADD (void);
// 0x0000086B System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::.ctor()
extern void U3CU3Ec__ctor_mF4403EE165173DC522103616E6645018625DE513 (void);
// 0x0000086C System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::<Start>b__22_3()
extern void U3CU3Ec_U3CStartU3Eb__22_3_mB3B2B3F460A87856365CD811630D58EA7A704357 (void);
// 0x0000086D System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::<Start>b__22_22()
extern void U3CU3Ec_U3CStartU3Eb__22_22_mCFEF8C7D30A25EAB5E114C737BF1D2A30D599CA9 (void);
// 0x0000086E System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::<Start>b__22_7()
extern void U3CU3Ec_U3CStartU3Eb__22_7_mAC888F5211B48BEDE3B76735AF1835BC125992CE (void);
// 0x0000086F System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::<Start>b__22_12(System.Single)
extern void U3CU3Ec_U3CStartU3Eb__22_12_m56B9B268A61C3FB036CCE2D844B45C27C0B201AD (void);
// 0x00000870 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::<Start>b__22_18()
extern void U3CU3Ec_U3CStartU3Eb__22_18_m210C935C0307C41651403480E5EF0856246C8C3D (void);
// 0x00000871 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c::<pauseTimeNow>b__26_0()
extern void U3CU3Ec_U3CpauseTimeNowU3Eb__26_0_mE763C76C7D6AEEAAB8A0C84892C1D42F508144EB (void);
// 0x00000872 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_m8818C4F306B80B7FEE062793CBEBEAB352E53F22 (void);
// 0x00000873 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__0()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__0_mB82E5FEE09CC712C778F6A007B14D3D455256ECB (void);
// 0x00000874 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__1()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__1_m8D5BED91D3EE4A8C0C0DF77D2B62F903C3E20D0C (void);
// 0x00000875 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__2(System.Single)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__2_mB677486A14C58435757062820302D43BCC02D144 (void);
// 0x00000876 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__3()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__3_m3987A57ECD0F50AB39FEF707C42D9710AA3CFB2D (void);
// 0x00000877 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__4()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__4_m87D8E7758DA9C355EE513F9775D9F4B4C387BB56 (void);
// 0x00000878 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__5()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__5_m2B18F0F9B914A675CEEA356DCFF478B4F5611D8F (void);
// 0x00000879 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__6(System.Single)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__6_mE7EA3413E1F81C42AAFE03C96A7C6C430EAFAF81 (void);
// 0x0000087A System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__7()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__7_mF6A7BD6551F5A9F146C1B47E86AFF650A2B0005D (void);
// 0x0000087B System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__13()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__13_mFFA8F8E7E5F79A608E1C2216AA8D1B734A758093 (void);
// 0x0000087C System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__14(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__14_m0D4E422B37F43F7D3ACECDE81ADD5A18D4C97EE8 (void);
// 0x0000087D System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__15(System.Object)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__15_mE41B0CD588F01B0A76360EBD332CFAF425E20931 (void);
// 0x0000087E System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__16()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__16_mEA9C490B5AEA717E999FFF3DD2CB25B23966277E (void);
// 0x0000087F System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__8()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__8_m63E7CF9513E4624B5B101B1C46941DC34982AC33 (void);
// 0x00000880 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__9(System.Single)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__9_m7B1419838E647715BC8200AD6D56DAF1A8F04F81 (void);
// 0x00000881 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__10()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__10_m91860D812373862DCCD33D1D479A52867ED3CC02 (void);
// 0x00000882 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__11(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__11_m9D776D70F7B40CB7DB069BE5D095C281D59F2713 (void);
// 0x00000883 System.Void DentedPixel.LTExamples.TestingUnitTests/<>c__DisplayClass24_0::<timeBasedTesting>b__12()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__12_mB40AAAC5766C52276E4F9A375DB379DBE62012FA (void);
// 0x00000884 System.Void DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::.ctor(System.Int32)
extern void U3CtimeBasedTestingU3Ed__24__ctor_mBB473D0F9FCF74C17EE67A02377DBAC113B0ECE6 (void);
// 0x00000885 System.Void DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::System.IDisposable.Dispose()
extern void U3CtimeBasedTestingU3Ed__24_System_IDisposable_Dispose_m2240ABE280E91D3110F4956F4DDAE8A6F211340A (void);
// 0x00000886 System.Boolean DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::MoveNext()
extern void U3CtimeBasedTestingU3Ed__24_MoveNext_mF87066714174A943FE18B6A562B291DFA766B72E (void);
// 0x00000887 System.Object DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtimeBasedTestingU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mACB1A3175D122506B71BCDB9EBF4AA9F0383E74B (void);
// 0x00000888 System.Void DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::System.Collections.IEnumerator.Reset()
extern void U3CtimeBasedTestingU3Ed__24_System_Collections_IEnumerator_Reset_m96D9D8DFA5B24A08E71FA9EA4595288F06E9837B (void);
// 0x00000889 System.Object DentedPixel.LTExamples.TestingUnitTests/<timeBasedTesting>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CtimeBasedTestingU3Ed__24_System_Collections_IEnumerator_get_Current_mA059C8602E0A9BCCC1849108A55B19E09E4818CA (void);
// 0x0000088A System.Void DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::.ctor(System.Int32)
extern void U3ClotsOfCancelsU3Ed__25__ctor_m9B88818966D8A321B3F23FB53C461C28A354F1C0 (void);
// 0x0000088B System.Void DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::System.IDisposable.Dispose()
extern void U3ClotsOfCancelsU3Ed__25_System_IDisposable_Dispose_m40E5F2D2626AB1E624CA39E3740CDFBE5B4F1B30 (void);
// 0x0000088C System.Boolean DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::MoveNext()
extern void U3ClotsOfCancelsU3Ed__25_MoveNext_m9AA75163A6CE98F58622048E0971E438D1E2AFEE (void);
// 0x0000088D System.Object DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3ClotsOfCancelsU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2F111DF2BFDD6B3974DFA490B271DC9FCCBFC1DB (void);
// 0x0000088E System.Void DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::System.Collections.IEnumerator.Reset()
extern void U3ClotsOfCancelsU3Ed__25_System_Collections_IEnumerator_Reset_m440F860762B28408E3B9AB6D72144B5E6A1C6F8C (void);
// 0x0000088F System.Object DentedPixel.LTExamples.TestingUnitTests/<lotsOfCancels>d__25::System.Collections.IEnumerator.get_Current()
extern void U3ClotsOfCancelsU3Ed__25_System_Collections_IEnumerator_get_Current_mA7D1CE85A7D50D60D73C5914F5EC60B5E33A344E (void);
// 0x00000890 System.Void DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::.ctor(System.Int32)
extern void U3CpauseTimeNowU3Ed__26__ctor_mD1AE96EC04B7EB3C54830BBCA910ACEC6A857D84 (void);
// 0x00000891 System.Void DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::System.IDisposable.Dispose()
extern void U3CpauseTimeNowU3Ed__26_System_IDisposable_Dispose_mE8F0437ED1B062C967A1FFCA460708BD42BC5142 (void);
// 0x00000892 System.Boolean DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::MoveNext()
extern void U3CpauseTimeNowU3Ed__26_MoveNext_m0750DC2D5E587C420188232EDCAD54FEE7ABF659 (void);
// 0x00000893 System.Object DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CpauseTimeNowU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD12A76EAE3CD0B1B9C938030B3E4D2FAA6456011 (void);
// 0x00000894 System.Void DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::System.Collections.IEnumerator.Reset()
extern void U3CpauseTimeNowU3Ed__26_System_Collections_IEnumerator_Reset_m88957DE3F231D3C4F8E64C6DADA45876FABB7FD0 (void);
// 0x00000895 System.Object DentedPixel.LTExamples.TestingUnitTests/<pauseTimeNow>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CpauseTimeNowU3Ed__26_System_Collections_IEnumerator_get_Current_m04EA51DB175A4043B51D9B3CFDD410D924B422A0 (void);
static Il2CppMethodPointer s_methodPointers[2197] = 
{
	OldGUIExamplesCS_Start_mC33821BD3ADFD74B0B1BDD411FE35F29173AAE16,
	OldGUIExamplesCS_catMoved_m0AFE2D767EEFFE2B23F700545EDCADBA4FDE4495,
	OldGUIExamplesCS_OnGUI_mFA5EA2B56824C4712729521C4545139AB167C4AA,
	OldGUIExamplesCS__ctor_m805206E5EF03EF6452AC2A3979FD378F1CB86998,
	TestingPunch_Start_m51A034719AB8587292861C585C70970EDEBE8877,
	TestingPunch_Update_m953417D5B76254AF9BF4E2BA996801A997F9D6D6,
	TestingPunch_tweenStatically_m0E13525CEE807E5FA77B94242B182868B1D129A0,
	TestingPunch_enterMiniGameStart_m017AD4178C7F47FB535AFCA0B39AB97A58F0D9BE,
	TestingPunch_updateColor_mB0688DDA58E8C0CBA76525C4B31BD18EB3A2E4FC,
	TestingPunch_delayedMethod_mC7F5E34BF17C44BC3B311081B8ED1897F1CC08C3,
	TestingPunch_destroyOnComp_mB95F605847D2D64B1F4BF239600FAF788917BF44,
	TestingPunch_curveToString_mA840114D184FEA8169EE6B379C4513E282E09511,
	TestingPunch__ctor_m8AA41432E6E7079F6B683958FECD329615A21169,
	TestingPunch_U3CUpdateU3Eb__4_0_m573E0207272AF5B03F77EBE674FC9B7C0B49791D,
	TestingPunch_U3CUpdateU3Eb__4_3_m4CC34D2970098316C32EBE67B368AB199A20A2E1,
	TestingPunch_U3CUpdateU3Eb__4_7_m2E2AD49C20FF476ED389803BE00C0C87495D0DFC,
	U3CU3Ec__DisplayClass4_0__ctor_m6EF45FEFD275B14E2DFC1358F9AF89B57EF810AC,
	U3CU3Ec__DisplayClass4_0_U3CUpdateU3Eb__2_m59E4E1443D5262BE2A23D2C28AB670EA4071ABF4,
	U3CU3Ec__DisplayClass4_1__ctor_mB4665A6E7864E352E8943BE5EFEDF0E53D1685AC,
	U3CU3Ec__DisplayClass4_1_U3CUpdateU3Eb__6_m954F24C94FA12A5815D1FED30C2506F75E8628F2,
	U3CU3Ec__DisplayClass4_2__ctor_m1584CFD1510446CA5FCCF444CC4C3A8416419C58,
	U3CU3Ec__DisplayClass4_2_U3CUpdateU3Eb__8_m06A77409E2EEAED1E858D86BDDD2CC58DE2CE83A,
	U3CU3Ec__cctor_m66A08467936DA18B932008F1E6DC72EEB39F16A1,
	U3CU3Ec__ctor_mF00B82FD7B30077FBDE19AD680A208A4C3BA70F9,
	U3CU3Ec_U3CUpdateU3Eb__4_1_mC4766FA978E4EE87A4519997A0A18B9FF21F128A,
	U3CU3Ec_U3CUpdateU3Eb__4_4_mB29A35054C7AD0CC10D5E31667EDF5249A05D507,
	U3CU3Ec_U3CUpdateU3Eb__4_5_mDC2E23D6DCE6159A01A4E5965CA5654E521A6203,
	U3CU3Ec_U3CtweenStaticallyU3Eb__5_0_m412A7565AC15AED058D5BD67A3BFF97E6450F355,
	TestingRigidbodyCS_Start_m995B801673B71100B5483E039BFCF522BA624548,
	TestingRigidbodyCS_Update_m8DD8457212175A81186F50921B999350FA9DEC0F,
	TestingRigidbodyCS__ctor_mDE99A4FEAFB79D731E586C9AF3656849987B741B,
	Following_Start_m5CD86C24B70F949375792EFAEF58F657F06DBA18,
	Following_Update_mEB56FD4D3CE1029AB038A8DA8CE6C6704A9541DC,
	Following_moveArrow_mF5878CA6A071D36230DD6FA4209FFA765BAC640F,
	Following__ctor_mA110CFAE2A03042B9F1FE53338D377A005CBAB13,
	GeneralAdvancedTechniques_Start_m132CD028BB491EE7C59B5F5F1D1AB47EB7DAE306,
	GeneralAdvancedTechniques__ctor_mB828FC801FA5649B544E8FF31A6E1FF8916563D8,
	GeneralAdvancedTechniques_U3CStartU3Eb__10_0_mBBEFB6358D20EC1EFC4D6B51139D0DEEDFF86AD2,
	GeneralBasic_Start_m159ABB4F96835B58FEB058E24A576462A119E871,
	GeneralBasic_advancedExamples_mB7CE50C2F062F66FD7D0E1BD403DFDACE1D099AF,
	GeneralBasic__ctor_mF2DBA4586E298C9F216C4680C14837E6B5E095AD,
	GeneralBasic_U3CadvancedExamplesU3Eb__2_0_m706BD8DE22C36E9A7EF6326EF1B7EAB6C780BF3D,
	U3CU3Ec__DisplayClass2_0__ctor_mAE48CD4D687E71BADBB3C871A2194F0803D1EDC0,
	U3CU3Ec__DisplayClass2_0_U3CadvancedExamplesU3Eb__1_m9425EEE45EC19E72004ADE4D0B203F5448C21B27,
	U3CU3Ec__DisplayClass2_0_U3CadvancedExamplesU3Eb__2_mA4BA962A162AB1425F97630A3FB0FD395D77444C,
	GeneralBasics2d_Start_mD2ED7FA046D4AD2C347896AFF54984E3BDC73876,
	GeneralBasics2d_createSpriteDude_m5D7A1E41EEAADFE587F9AB375AA4C3B7617DF7B3,
	GeneralBasics2d_advancedExamples_m7A48E45BB9836372021B76A523A2594A1E66F1EA,
	GeneralBasics2d__ctor_m5BBA8B80514CB8E8098C55AF472FADC2BA164C2A,
	GeneralBasics2d_U3CadvancedExamplesU3Eb__4_0_mFB34B228D53D66D2DA60CBE36C863AAC135AAE41,
	U3CU3Ec__DisplayClass4_0__ctor_mF77915960E9F4C762CB8473CB99645EB20C2BB06,
	U3CU3Ec__DisplayClass4_0_U3CadvancedExamplesU3Eb__1_m34824CEE9AF028A0C15E9EF50B69F24B63AD82C2,
	U3CU3Ec__DisplayClass4_0_U3CadvancedExamplesU3Eb__2_m9486B659998B862E7D8D7F60D70F7E60023F33C1,
	GeneralCameraShake_Start_mF7CB74F9B674B375BDCB29EC25FF2B568431E543,
	GeneralCameraShake_bigGuyJump_m502DE5D59ED9FEAD8F13549A9A2105E86D1F90E3,
	GeneralCameraShake__ctor_m6B09C9BCA8DDFD913791AD123731FFC86E274FAD,
	U3CU3Ec__DisplayClass4_0__ctor_m3C8CF484C54E2EE225CC0D8A11A5BAEB2686C557,
	U3CU3Ec__DisplayClass4_0_U3CbigGuyJumpU3Eb__0_mA78BEF951754FC194ABA798EF9E551E30CD1DF91,
	U3CU3Ec__DisplayClass4_0_U3CbigGuyJumpU3Eb__1_m4A2B4F54066C9921E28893C3580BB18C47C8E99F,
	U3CU3Ec__DisplayClass4_1__ctor_mE5D0BDBB5AC685B299E3E124147549F48D5360FD,
	U3CU3Ec__DisplayClass4_1_U3CbigGuyJumpU3Eb__2_mE50BF2533D5A8E4215CA056E6C5A7B707C31615B,
	GeneralEasingTypes_Start_m9918519E1FE8506C66C09A06FC0973544C9B6CFE,
	GeneralEasingTypes_demoEaseTypes_mFAB0A2AA4E562BA44E4D99BB36EFC8BCC92CBD4E,
	GeneralEasingTypes_resetLines_mF6F826BE8342FEBEBB97FE69AEDCD749AD38E290,
	GeneralEasingTypes__ctor_m826931FD7A0F6840EC3920EF742AE00023053424,
	U3CU3Ec__DisplayClass4_0__ctor_m67D836D2CA426B728355DCE885351EF9BA44FB17,
	U3CU3Ec__DisplayClass4_0_U3CdemoEaseTypesU3Eb__0_mDA8F2DD5BE6F9B20420A94430FE2C634CC07EED3,
	GeneralEventsListeners_Awake_m929CA13301F3FA1A5C1AA230681377F30E0E78C4,
	GeneralEventsListeners_Start_m1C63BB1DBC231496398533A9D365C0B2EAD4BE80,
	GeneralEventsListeners_jumpUp_m2CBD7F9072D9EB312F608BFC683E63D3DFF1DB28,
	GeneralEventsListeners_changeColor_mF0F3863B481DE533D2DA0CCF3424BD1B231268BE,
	GeneralEventsListeners_OnCollisionEnter_m296C0AD01860A6C98C1E293DD67D880BB0590E91,
	GeneralEventsListeners_OnCollisionStay_m290268F1B803DC1E306F52423DA7A365442C7C31,
	GeneralEventsListeners_FixedUpdate_m903B40B7AC9A96613D21A365E8BC005E87AA8439,
	GeneralEventsListeners_OnMouseDown_m6B7190177327186B2D363437F5B96595367C7454,
	GeneralEventsListeners__ctor_m7B76E509C73273AA56871D0D0E9772F6FC2A3056,
	GeneralEventsListeners_U3CchangeColorU3Eb__8_0_m8D567E7710ED42E4ED9DF3074F1F9E4AF19AF1A1,
	GeneralSequencer_Start_m8B0E62A3511525F37EF49CF5BA670A8FDC005FBC,
	GeneralSequencer__ctor_mFD32ABD60398858ED0D676B843D61702B7FB1F75,
	GeneralSequencer_U3CStartU3Eb__4_0_m01F09B25E4652D0AD12402145A95897D0E65C36F,
	GeneralSimpleUI_Start_mE89FC53D2284D832DC21032BAB637D0C4075AC6D,
	GeneralSimpleUI__ctor_mC03751F3B550ACC5742FCB2C9B381E1EA841C085,
	GeneralSimpleUI_U3CStartU3Eb__1_0_mCC061524D85ACD8BAF6FE7D8CE1F2277735B3FC8,
	GeneralSimpleUI_U3CStartU3Eb__1_2_m32632A4DB6420C70DED2CA0F5D16A52ABED4D202,
	GeneralSimpleUI_U3CStartU3Eb__1_3_m81A54F1B0397D24C3ABC0367E5F3A96CD8938D25,
	U3CU3Ec__cctor_m2F58606DC92BEEAFADDAB84BDE6B467660090F3A,
	U3CU3Ec__ctor_mB27080DC10CB152A62D2B7D7F2BFFA689E952C73,
	U3CU3Ec_U3CStartU3Eb__1_1_m3121B206ADF49779458AA5961E16BC6F6DFC8277,
	GeneralUISpace_Start_m5EE834954E03550B87B33D931A593651703C2223,
	GeneralUISpace__ctor_m007D52C81E8F67F6C5FDBD433EBDD1F82281CE87,
	U3CU3Ec__DisplayClass15_0__ctor_mBE556FCA9CC905E8AFDA137DCA924101E479875C,
	U3CU3Ec__DisplayClass15_0_U3CStartU3Eb__0_m6F18D230FF8599CCA7A14221973A08E0447954B1,
	LogoCinematic_Awake_mC405113467B061C77307CFD90E064F5A651B3D22,
	LogoCinematic_Start_m3E319B3DEC812349F8C37171CA5D4081276F7826,
	LogoCinematic_playBoom_m787C8F6D4E17645300701C39C99D4E37893FEFAF,
	LogoCinematic__ctor_m4106D7C04CED9DBA0EA597C3A8961A01C01B88AD,
	PathBezier2d_Start_m6F6F8E4B60B7D179AE36BEE9EC0A264ED03D6849,
	PathBezier2d_OnDrawGizmos_m69311F42A05B822B9FCBD8CC4F0F4C4804AC9354,
	PathBezier2d__ctor_m28D0B7A13B4DA0EDA3A009A105016F4A7E143055,
	ExampleSpline_Start_mA17AF5D470B3BCA08101E4A82E8EB7A0BAABEAB6,
	ExampleSpline_Update_m356EAE7A488D899AAA63B3F9CC96E19743AE7EC6,
	ExampleSpline_OnDrawGizmos_m1479029D4155936B69C362144D347542E05CC0DF,
	ExampleSpline__ctor_m9FCDF08DBF16C46F63F1AF63EF143E3DAC295EE1,
	PathSpline2d_Start_m1EBB4A3F12FFD7F33EC3D10C0955B5D421DE5B5D,
	PathSpline2d_OnDrawGizmos_mF49ABC37A9C2C3B83BB5BF1419067E2FD8AB1B77,
	PathSpline2d__ctor_mD3756639777CCF9D251F157BCF32647C9F0AB7C6,
	PathSplineEndless_Start_mAB94E60786A238B143854F91E30BB877D5730800,
	PathSplineEndless_Update_m5C2C0D01E2D3AF4FFD2AAF23756C62733DB1509A,
	PathSplineEndless_objectQueue_m4F43CF1A70A9897A13A3B385DCE841DA5141C524,
	PathSplineEndless_addRandomTrackPoint_m3A914064336F71FAC30C4919147216682994A961,
	PathSplineEndless_refreshSpline_m8B64174788E0276D3C61E2F66C85EF7378EA30C2,
	PathSplineEndless_playSwish_m968F1F1AED0F8421A2834347C318076A3CE747BD,
	PathSplineEndless__ctor_m411B70A0AC412E9518BBBDC687C25CD1B388E6C9,
	PathSplineEndless_U3CStartU3Eb__17_0_m89EDA38D298348E747F008DED4C1CF7D4F815D60,
	PathSplinePerformance_Start_m6A0FB9A0FCA5D3B1AFDF6C03C8B8CD3DCFC57040,
	PathSplinePerformance_Update_m5416A63FFB450EABE065F6C5C3312B7C048EF5A7,
	PathSplinePerformance_OnDrawGizmos_m490615FEB9C7E53A19C0DA922E9A7077A1A66274,
	PathSplinePerformance_playSwish_mDCC9C70227FFF9F7969D9D8330E5B485B954DC7A,
	PathSplinePerformance__ctor_m3918B4846029379DBF03985747088A8F4E734CBB,
	PathSplines_OnEnable_mBAC54412DA75F41A2961B5D2A8D33971CA42EF78,
	PathSplines_Start_m7E196F5CDC4C203D9187A30B90FC02414FBDF186,
	PathSplines_Update_m2D049CD05DCD8B298477D7A6F6EEF97F16A140EA,
	PathSplines_OnDrawGizmos_m2099220E2C8911102EA2BE1EFC5153CDE8FF6283,
	PathSplines__ctor_m4BAB7527375E54EAD3B636B8AB1F5AEBF88AB565,
	PathSplines_U3CStartU3Eb__4_0_mC304E252D7336E181BBEF0F27B79311DEDF4BB0B,
	PathSplineTrack_Start_m63CB9A3917A58E8791AA4DED6921EB32280FFF7F,
	PathSplineTrack_Update_m00439912F9ADA883049E7B01A7453419BDB6AE79,
	PathSplineTrack_OnDrawGizmos_mD7F0DCDB7C1B0CB6ED0F1A4A20E2389A14127D5F,
	PathSplineTrack_playSwish_mEADD52F2DF6153507245FCEF25CCE5AAC216627A,
	PathSplineTrack__ctor_m7F933342B4D6E1B74306EF85FC59C7F70E5EF982,
	TestingZLegacy_Awake_m6ACDE967AF5970766B8EFFCC196B677E07DB0C10,
	TestingZLegacy_Start_mE43112422475CBDC67D4CE4680D0B64C38D23BA6,
	TestingZLegacy_pauseNow_mCC617C16BCE35E2D901183CFAEF9F51D7805DA61,
	TestingZLegacy_OnGUI_mAF981918615B59787EED9DC6EB2F232D6A4F6E5F,
	TestingZLegacy_endlessCallback_m96FFBCC19A52A227D879CA4C9A4D784078F0FB65,
	TestingZLegacy_cycleThroughExamples_mE124EAAF3FE6EDE035F8D620E0FF877916FFCA2A,
	TestingZLegacy_updateValue3Example_m690CC88D964055BB090736A4D7D080E043E15D82,
	TestingZLegacy_updateValue3ExampleUpdate_m28A23A4EFF68505C400DC9F5BBBD6ADA3ACAEE62,
	TestingZLegacy_updateValue3ExampleCallback_m25E92C1FD9658ABBED9080D237D95E6BCFC57E12,
	TestingZLegacy_loopTestClamp_m1EC272930E9852F0D980AC98F1633B4F2841C450,
	TestingZLegacy_loopTestPingPong_mC5B0F54C34F86BD154F6A0BB43C88FC7913344B9,
	TestingZLegacy_colorExample_m9C06FD132681F8FF64F1498BFC2811EFD82E4D28,
	TestingZLegacy_moveOnACurveExample_m89E0D3F9175DBDEAB39A915BCCD5AF06FCD2357F,
	TestingZLegacy_customTweenExample_mF418B80DCF007D21FBD0663BB9F002CF18EAEB0A,
	TestingZLegacy_moveExample_m5A0E16D835C2A10B7042F64D1B6CD0C1320B1F78,
	TestingZLegacy_rotateExample_m61CF7E2DC245EBF480DCCD0369ABA0F332658917,
	TestingZLegacy_rotateOnUpdate_mE9D7799638F85C32FB7715059ACBDF657C89B7CE,
	TestingZLegacy_rotateFinished_mE2E799232C3119A15368CBABBF594C57799D5683,
	TestingZLegacy_scaleExample_mD080FABF7510CFB95F324A1B4BB1C4277937EFF9,
	TestingZLegacy_updateValueExample_m9DBE93DA0EC50F7E6764A27DE1E87F12ACF8DA2A,
	TestingZLegacy_updateValueExampleCallback_mEB667645443DDFD352588C65ACBBE1C500E97C4C,
	TestingZLegacy_delayedCallExample_m51E456D34EDF2598C8EF549CDDB2376F607C3CC1,
	TestingZLegacy_delayedCallExampleCallback_m15CE90D43A85804204B22169D73FDE3EA46F609A,
	TestingZLegacy_alphaExample_mA4ED4E57F148B4EF6E2B1B608BAEE0EEF5AE7143,
	TestingZLegacy_moveLocalExample_mE6E3C5CD62456482BADA24B0CA93C0962125B80F,
	TestingZLegacy_rotateAroundExample_mB3F00FEDCDD63C7BFD293C187BA847BF15F7ED3C,
	TestingZLegacy_loopPause_mC83ACD076E19354DC2CFF3D3E409DA2901CE229E,
	TestingZLegacy_loopResume_mBA3E805FE386DFC40F4F795223744619E9048A7E,
	TestingZLegacy_punchTest_m1266ADD2035D0E9971ADA805B0A1636D35ABF2B2,
	TestingZLegacy__ctor_m1CA1AEE9AA84B7551AFA5A034BF4E648AE662D9E,
	NextFunc__ctor_mD13871807D65A0BB2C5E0F567E5FB021AF248AED,
	NextFunc_Invoke_m8D7A63BD3F35D7D22BC9DAF5241C637889044719,
	NextFunc_BeginInvoke_mB38D1030FE53CEF432C2E6D1569F0EBAD0F1B5F0,
	NextFunc_EndInvoke_mFE2435A3542567B6D23B6E63B50BF9EC6D55609B,
	U3CU3Ec__cctor_mBD80E302F3A69C0C61FDB9987E223DF4A13097D9,
	U3CU3Ec__ctor_mD7A097986D81133EA1341850CD4003072DC490BB,
	U3CU3Ec_U3CcycleThroughExamplesU3Eb__20_0_m99E554155125675C01E3E61098DA676F0B69E649,
	TestingZLegacyExt_Awake_m7D6F79923E270BE07F7B475DBFE0A7CA94534FCB,
	TestingZLegacyExt_Start_m86BD1A532532B1F08B6ECF7457358F6CB1410E5E,
	TestingZLegacyExt_pauseNow_m0A9E3639505AB9DDE82956D7FB34C37758982180,
	TestingZLegacyExt_OnGUI_m25777A6897DF3B16F25546C5CAFBF80FB60923AA,
	TestingZLegacyExt_endlessCallback_m73A1A18DA9DD7642231DE5BBBF35F45DB785A077,
	TestingZLegacyExt_cycleThroughExamples_m15FB09793AAA4E36F905F6D81C254E0B763F391D,
	TestingZLegacyExt_updateValue3Example_m15F0FA3A4491DBB842544D0A878B2C533640B4AE,
	TestingZLegacyExt_updateValue3ExampleUpdate_m46BCB3BF2A85D38DE7CE46720DF912FDA7DB84FE,
	TestingZLegacyExt_updateValue3ExampleCallback_m7B2BA08D3878E1672F358452FA11C48F8D686B76,
	TestingZLegacyExt_loopTestClamp_mA4A7563CB2448E2D1EC1394494E21C79399BAC06,
	TestingZLegacyExt_loopTestPingPong_m6523FCB9E3A1AC5035882C67F6EF5B8A00AAC5D3,
	TestingZLegacyExt_colorExample_mCF5A6A5FAA5CC4EC32FFD99062DA09D220B776A2,
	TestingZLegacyExt_moveOnACurveExample_m8E7EA9EADCB22EB3DC2DDC7FD27BD05050C2FB44,
	TestingZLegacyExt_customTweenExample_mE32AD842F2ACF6BB96A100E29B784B942004386F,
	TestingZLegacyExt_moveExample_mB59B97379A841F1AB17E8E9CE8BC7AFF0DB9F3C8,
	TestingZLegacyExt_rotateExample_m15D1742A434CC70072A4C96DFEDB623B1BD8C02F,
	TestingZLegacyExt_rotateOnUpdate_m854E95A564B027980BE0FFF08EF1A20CA3EF5EC7,
	TestingZLegacyExt_rotateFinished_m78E88F2F51C8018BBA472122955C89DCD3C13B53,
	TestingZLegacyExt_scaleExample_mDDF64026F75968C35562CEE5C1B2F84874C76C1A,
	TestingZLegacyExt_updateValueExample_mD778D12570BC222BB68613D27BD4B5B0DCD9E76C,
	TestingZLegacyExt_updateValueExampleCallback_m273FBEFFA8289DCB71813A6C4FF5C66ABD2C5003,
	TestingZLegacyExt_delayedCallExample_m92DECA48CA80DF5FF46B873EF87AC2A564AA6452,
	TestingZLegacyExt_delayedCallExampleCallback_m2F2F9A40BA43F410DABE187FEA3861FCF719058B,
	TestingZLegacyExt_alphaExample_m0875576982CCE840BF262C71817239AB7EA6C8C2,
	TestingZLegacyExt_moveLocalExample_m81967DF8243173690E01BCD0C9DEDF141C080813,
	TestingZLegacyExt_rotateAroundExample_mA9188C5A54C644D2B78F8956DAB3B245B975BCA1,
	TestingZLegacyExt_loopPause_m04999A727B7B5D4D7C04BA4D24C78FD7C35FCB42,
	TestingZLegacyExt_loopResume_mAD5DEA4AB4F83437FE25A8C8BBE141FB885CB6CF,
	TestingZLegacyExt_punchTest_mEE9802D38D39AB2BE2BE50BD88F901A16383B87B,
	TestingZLegacyExt__ctor_m5E300E444E4496DA32F5F4A1F8B48051126D8B61,
	NextFunc__ctor_m0008349A4886ECE27F576CFE82C9A888798F60BA,
	NextFunc_Invoke_mFB6931473FA9F6AD02BE8B752A4D051ECB660ECF,
	NextFunc_BeginInvoke_mD91FEF6AFFC5695F493E6950FAAA8A6B72DB37CD,
	NextFunc_EndInvoke_m96DB2010CA80A82BDCD35903E0CE02CE8AC78296,
	U3CU3Ec__cctor_mD11D91D44BCDECB51A3CF966B686782B4F5330A7,
	U3CU3Ec__ctor_mC490B74A87268C68E2428BE981C75A67BE4460A7,
	U3CU3Ec_U3CcycleThroughExamplesU3Eb__20_0_m232F589CF9029278662FD1D5BB31DB7E6A2973F5,
	LeanAudioStream__ctor_m767420411C73925D7260141B6AC141C332340F57,
	LeanAudioStream_OnAudioRead_mB0B822C29AF57CD64964495BC47B2D552C9A61EE,
	LeanAudioStream_OnAudioSetPosition_m84D20E5E2E90A883DA74466C176BD12EF300CFD7,
	LeanAudio_options_m24F4466E9FF88143F014E1341ECAA03A0BD1DE16,
	LeanAudio_createAudioStream_mF0B2E4AE61E77E5E4F5BB229BB17EA0010EED167,
	LeanAudio_createAudio_m741D84D297AF8413E9A6890FA81CD7F288A303C7,
	LeanAudio_createAudioWave_m4368517E2416AF360A06777597C9A3142BA5FC00,
	LeanAudio_createAudioFromWave_m1D0C1A2142307BF2378F25C76C42F61265D5ED21,
	LeanAudio_OnAudioSetPosition_m7FBC95FBFCA93F4A878761356FFD1B4FB716331C,
	LeanAudio_generateAudioFromCurve_mD5AE758FCAB5F326FECD85B37A313678B9D8929B,
	LeanAudio_play_m96190E0C8E00011FC6A98EFEBDFAEE2E3C06AB9E,
	LeanAudio_play_m7F543AE840832C2D4FC88AA184C605A892F1C030,
	LeanAudio_play_m6CD5A5DC70FD339BDD5BEDC9243FACA5C3186451,
	LeanAudio_play_mBC8A61F39D8ECDCA09F458FB1028945AC35DB0D2,
	LeanAudio_playClipAt_mC86BC94850F21DBC4B9C831CC666FEBC86A2C510,
	LeanAudio_printOutAudioClip_m22003A70913081D802CBF76538C92F929CD8D857,
	LeanAudio__ctor_m492D6C2E9108D90D229F0811C3B185DFD891907A,
	LeanAudio__cctor_m31067C511A043987DBFED412C6486A1D4D2CBF53,
	LeanAudioOptions__ctor_m29A189A1499462B631EBD724FF961389527A8EA2,
	LeanAudioOptions_setFrequency_m85E50359A5904A370955AFF52AC3D88CC01EBBC5,
	LeanAudioOptions_setVibrato_mF64FB8E4DD576D90776F76B01B753247D99B49DB,
	LeanAudioOptions_setWaveSine_m76F10085FDCE754A60760BB1542537F9DA46034F,
	LeanAudioOptions_setWaveSquare_mF8F08EACD82E37E15137F5F1C785DD0D671AF42E,
	LeanAudioOptions_setWaveSawtooth_m27624D2920970C41AD3CE47DE408FE9C7EEA530E,
	LeanAudioOptions_setWaveNoise_m55FF493A787F18D8692DF85866D055354AE1A2B0,
	LeanAudioOptions_setWaveStyle_mEB0CA095133249C1947E51FA9FBE9657F7A784CF,
	LeanAudioOptions_setWaveNoiseScale_m68B9C76F8DD866150B060285CF705D4ECB5C533C,
	LeanAudioOptions_setWaveNoiseInfluence_m7CBBEC51092269C15133C3B53D1828ACC874AF56,
	LeanSmooth_damp_mAFCFAA57FEE313048C9AF8C5F16FC2F5E723D0FF,
	LeanSmooth_damp_m7CCE1AB623BF22468A59B97CC2D161682476168E,
	LeanSmooth_damp_m9AA68F6F1D20FF61F338BA122A374CB0F10D6EFE,
	LeanSmooth_spring_m314389AD151BF197F11CB50DFEC3F05396E189AD,
	LeanSmooth_spring_mAC93690E733A39072CB37D5BFF1239414F98E1EF,
	LeanSmooth_spring_mC13A607AA221D50EB1AB72CE0C5D314F2748E946,
	LeanSmooth_linear_mCAA760AD54BCCFD8A3C67631437C528F84806A11,
	LeanSmooth_linear_mE9F99D1FD99A7D58A0F81A3A39EA4D994D55BB6D,
	LeanSmooth_linear_mD4AAD42ECD1AFD102F576A52116C57CDEEFB73E7,
	LeanSmooth_bounceOut_mD1B8F20CB910CD8609D9F5988472CCC83B98DE6A,
	LeanSmooth_bounceOut_m39C1F55CD36843F594D1C58D067696AD294C4812,
	LeanSmooth_bounceOut_mF3E43703A80AB9D705575A9D803AD2E8AF3722F1,
	LeanSmooth__ctor_mA939A643B1463547170CECB7FCC6AF013FF0DCAA,
	LeanTester_Start_m9EEED491D1FBBF5A677178E6477E7B68491F1517,
	LeanTester_timeoutCheck_m59067A5600F0D2434CE9B7EDAC062829C85D5C00,
	LeanTester__ctor_mBB9C14D1DDB7AA3F591D6B0040EEA74F032EB1F7,
	U3CtimeoutCheckU3Ed__2__ctor_mACFA1EA79EDBEA235C7474D1BA3E71804A23FEB8,
	U3CtimeoutCheckU3Ed__2_System_IDisposable_Dispose_m0AF241B0BA496E508C41A00A92ACCB507F764F3B,
	U3CtimeoutCheckU3Ed__2_MoveNext_mF2666B91C53937A9B6CFBCDB7E111B6E74685643,
	U3CtimeoutCheckU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF1EF5FF8E038CD04BB438F282E437032A52DF973,
	U3CtimeoutCheckU3Ed__2_System_Collections_IEnumerator_Reset_m377EF3F9C3E2B3A09D749A0A347B92595F0D756D,
	U3CtimeoutCheckU3Ed__2_System_Collections_IEnumerator_get_Current_m275B7C4BFE741FFBA1D7CE3E422460E5BD15EFF6,
	LeanTest_debug_m185769356E43C51F1A057B12DC7F0E6970CA05D2,
	LeanTest_expect_m697408A02255410088277BB6C41F73FFB789458B,
	LeanTest_padRight_m6DEC7C60D840B9D4F35BDFD3A27411551F182CF2,
	LeanTest_printOutLength_mE439776B28BB9737553D43E95254DD2A79676EFF,
	LeanTest_formatBC_mCE1B1751EF0B35A008C32A43AEBACA76B2FF0272,
	LeanTest_formatB_mDF309FD724B541A97D931A28FE32B9F3245DA966,
	LeanTest_formatC_m10095A4C3AEB8A45468A8251101CE25BE1643F34,
	LeanTest_overview_mFBDD0E748CC3DF470B668293185D2637F2ACF17F,
	LeanTest__ctor_m2534B2F8BA29F28EF5D26482735FE6D93B229F6E,
	LeanTest__cctor_mE75EB5D6AF852A6D0B480FCC70D1F9958AFE0943,
	LeanTween_init_m2257D056F7462641D1316804B27DB2EF03C54B0B,
	LeanTween_get_maxSearch_m76F5A3551B333FEAD6918DC0795238CEA51E8764,
	LeanTween_get_maxSimulataneousTweens_m38608BBD6E41F3B66FF348DE652F2EA6F1BEBF45,
	LeanTween_get_tweensRunning_mDD39747E471B1CCDF83F6F79B5CFA224E47D3842,
	LeanTween_init_m4CE7872C3DBD46FF7252C60DA96FD99FE9A071C8,
	LeanTween_init_m641164BCF025E634EFBD06151E55753939DB6F13,
	LeanTween_reset_mC778C2B33DA945C2D7FC099D6B0EDE24A98A78AD,
	LeanTween_Update_m4E96DD5D854DBD4CDF76759B63786DDD8739BF08,
	LeanTween_onLevelWasLoaded54_mE6FF28F42BD62CC30963F9D80C2B26FFF0B3C5B0,
	LeanTween_internalOnLevelWasLoaded_m14ED1FA1C1214624DC4A780E737D2FFE90A3B0AB,
	LeanTween_update_m2BF953CB1B4904B57BC6A43058FDA4BE52B57688,
	LeanTween_removeTween_mD1216980A45C032C76FBD7D66BD3ECD81F992D84,
	LeanTween_removeTween_mEF159BEAAC6DC0EEAA016966749C762D3EE88A33,
	LeanTween_add_mF9434A6D817FB5B901ED97889690158C6E99AD4E,
	LeanTween_closestRot_m45089460836416CF978CED05F04748378298675C,
	LeanTween_cancelAll_m017FC44A7C2778837326A7518192A881D4F39FD2,
	LeanTween_cancelAll_mC78FDFE4A1045C23D35C9447D3D71FFBDA2837CE,
	LeanTween_cancel_m8F49EAB6908B4BE0715C0D4160C51516DCDD2823,
	LeanTween_cancel_m28B082D5434EBF590F33D552278E4969614E35E6,
	LeanTween_cancel_mE6AF5A1A673FCA3C9EAFBAE4C15EBE5ED42B7710,
	LeanTween_cancel_mF3E2B961A0AB5C23CB2EB64B557AC718BF931F9C,
	LeanTween_cancel_mE5CB54E734F2E1DD02E9979D64E163400677AA7B,
	LeanTween_cancel_m8F235169322AEA85E23C774745EED07BD83D344E,
	LeanTween_cancel_m54BD97AD0C8C50A9EE5645108B28F7E25104EA01,
	LeanTween_descr_m0686304AC8834008456087DDB221577D87246C90,
	LeanTween_description_mCF024DD0005701DAC6CFDDFCAC0FCBF66C613B38,
	LeanTween_descriptions_m2ADDBCE0DFC78640128B7224DD86FE6C3938D76C,
	LeanTween_pause_mAF75D7E636CB0D02F8D515FF76C23D99B8B28FB1,
	LeanTween_pause_mEE3D7E0190BB1879C5E4E40A34E22146B867B2B0,
	LeanTween_pause_m28FA361A74060F3CF6CFF07EA470DEB5871ABFFF,
	LeanTween_pauseAll_mBC633AE2F834B3E89A97848EBE21C2CF24E04214,
	LeanTween_resumeAll_m933FBC788EED5D67979F160369D98BEF44CCB435,
	LeanTween_resume_m1940BE978DB7EC3606D92C181FFE10A4570043E2,
	LeanTween_resume_mB671FD12433254652D7A6F1F2E8B5A924EE8665C,
	LeanTween_resume_mB910424A52CA71E88F376EBC8BD7DAC8BA638F3A,
	LeanTween_isPaused_m36EF7ACA58D5CF19D261ECC2740D3BABE292AE17,
	LeanTween_isPaused_m8433AB793191572FC80B287A05951CCA46D60415,
	LeanTween_isPaused_mCBFAAB19270CC1E95412B9DB3F5FCE12F56B64C2,
	LeanTween_isTweening_m7D2702558144653FEF2A2994BFFDB95BB547D76E,
	LeanTween_isTweening_m0DD0D935F7AEC656A1397DD8DFFADA8509ACCBD1,
	LeanTween_isTweening_m8A9E15F4297CE4D50C54540646F91DEE0767071C,
	LeanTween_isTweening_mD1EC7E088742B5EB1A698E9C82DDC9F4AF2F831A,
	LeanTween_drawBezierPath_m140E21617AD80CA0F665DA5A74E8AB641A5C3A41,
	LeanTween_logError_m941ECF9F4DA921ACE1845B97FE0C814F9E26B1CA,
	LeanTween_options_mC569B03E642CE27936B5ADF511ACE67C40E3EFD6,
	LeanTween_options_m859EFA1A4C3BE0E937966B8F5D902F798B60B2C1,
	LeanTween_get_tweenEmpty_mB1509CF6AE8FAB5591F914CBC1CC1DD5C7447F08,
	LeanTween_pushNewTween_m4C8A3AFE33EDDBCEF0FAFB6D9DE68BE6D930014D,
	LeanTween_play_m8F84314D3491001A540FA92F7A4F0B1A66ABC010,
	LeanTween_sequence_m8FE5B82BEF69D9AF66D54E849587B4CF933E34A5,
	LeanTween_alpha_m9D8A4A0F46CBF56AA0008446BCE391A124BDE913,
	LeanTween_alpha_mCE5E7D21CF2E2687DD1CFB80A9EE40E1E2EBCC0C,
	LeanTween_textAlpha_m31154DC1D5392385E4318CE77677AC4E29CF99B0,
	LeanTween_alphaText_mFD7A2FD730E1BA7638F985A93CE91B5EA652C126,
	LeanTween_alphaCanvas_m8BCDB0DB0CAE28F75A7DDF79FA6D62FC1B3AD513,
	LeanTween_alphaVertex_m94B9F32105B2C406F41E8D014FD6E5E235EFA506,
	LeanTween_color_mB591490EB11BF6AAB3D9C823844CD51A54CB54AF,
	LeanTween_textColor_m4BCEDC9A9BAB880E6C4440833CF36C2ED3101343,
	LeanTween_colorText_m34F5C1169E22FE097A19CC6B85377C738D73B937,
	LeanTween_delayedCall_mF74ECC6AC349466F55E56C4625DB67892553A59A,
	LeanTween_delayedCall_mF0264F6DCD013BFA7483D1399C04DA37502DC729,
	LeanTween_delayedCall_m537234980306375FFF55F428FE0F5AF5F35AD1CB,
	LeanTween_delayedCall_m15A55B8039CD7A86856CE10FF15AC9F99D8D8EE4,
	LeanTween_destroyAfter_m8EBFD0632BEE8F6EB0280D36C85CE24414C0FF15,
	LeanTween_move_m78D08B26807623C822B81CFA60DAC960DDBAC377,
	LeanTween_move_m1E331CB112535FB9EA90E55461A2D77D5260B530,
	LeanTween_move_m44271CB95A4B41D5EB76C4D83B419135A6EF33EC,
	LeanTween_move_mFEDFA180C09AB9A50941E5F63CD8670356C67ED3,
	LeanTween_move_mAE5F83E88AABED48945E61924B0427C1A9D2D2B7,
	LeanTween_moveSpline_m5935DB5E37D36935FF3FBDA2C498F927FB7F5570,
	LeanTween_moveSpline_mB64C328098A521A9E7BB2EEAF0258238DF2DF712,
	LeanTween_moveSplineLocal_m280D26BA7DE193E5F3EBB523ACEE6E44FF0978B7,
	LeanTween_move_m5E61C62A0F22777D370EB7CF2E6C0AC2EF3EB8B5,
	LeanTween_moveMargin_mCD8BA91FB1CB5762858AB694C374525B40570658,
	LeanTween_moveX_m3345DDA9D080FF581C38E0B92B7B2A0389A61DB3,
	LeanTween_moveY_mF3562A335C9D8A19192A1D05BBBB062D5696E01F,
	LeanTween_moveZ_m1D2828EEFBA366465AE916017B04BD730DB02918,
	LeanTween_moveLocal_m5EA4899950A5A6E5244A472D5B0095A456A3E828,
	LeanTween_moveLocal_m5EFEFD16EBF5E4EFC333418953F72DA504E191A0,
	LeanTween_moveLocalX_m6CE239F4E86EE5439B2D86FB0163306EC152DAD4,
	LeanTween_moveLocalY_m2EBE679F58F63B2D3BDA557474CD0C99B6AB8C81,
	LeanTween_moveLocalZ_mCCAE8DEB09113FABEE3D66031E1B25C2B8BD318B,
	LeanTween_moveLocal_m4D6E9C2DFFB9D9BF77A70A417CEDEB22D7B0689C,
	LeanTween_moveLocal_m3AB20706D4DAFD80EB42795B594331820B1C41E2,
	LeanTween_move_mAF8F6C1C95F4A9E38C99E9471889EF3D6F26B21B,
	LeanTween_rotate_mFE6F0AFD90CF08BC42B97AB676D30F5D6B685BB1,
	LeanTween_rotate_mCDB520E4B0ACB36B9EE004C8CF8B3C0A5EC0D9E2,
	LeanTween_rotateLocal_m5B5E5981164221594D9D4FF15C84D7B9FB81C067,
	LeanTween_rotateX_mCBA79F277E3512C992772E15E9298BEAFAA046D0,
	LeanTween_rotateY_m14E1525768E32FDF7A2378B1C8AF023D1C0B1C24,
	LeanTween_rotateZ_mA0AD8A6E7771AB656243CD3669EBDB0E38CA2408,
	LeanTween_rotateAround_m588F9185E90CC14CAB0B0EB6C3D575CF7E7C285E,
	LeanTween_rotateAroundLocal_mC869325E94AD6A0B95314D081F29B976CA91A81B,
	LeanTween_scale_m684CBC818ED1F1ED8C50D1BD0F49495CACC0067C,
	LeanTween_scale_m16AF41D6C5EB894246FDE21B3B9B90EADE1949AC,
	LeanTween_scaleX_m4DCF8A388D1FE2AF9C40F67DDB77B958DE1F0220,
	LeanTween_scaleY_m011130E1548F777DC187AEF2A88291CDE924851B,
	LeanTween_scaleZ_m2CF069E5E4EC0E4BC040DBA5C759E1A401A67481,
	LeanTween_value_m16722DEDC681F28C188D0F4557F0BF9B6A1488AF,
	LeanTween_value_m85D0A108DCD42E6A1576B1CC53D2F9D00B0E47FC,
	LeanTween_value_m6B91F8AA266FFF0C593FF90E7A6BE3D1960B84E9,
	LeanTween_value_m45BC8CC34DB8AE45C89048BAAAA016A4C6F0D526,
	LeanTween_value_m91EB5E56B4A910CDE7D3F1CD5EFA1E352FEC04D8,
	LeanTween_value_mAC251E42C465A03544154ADD2059E8808E208F4A,
	LeanTween_value_mDA02C25B8315D819999BA319C0E183E3534BC0D9,
	LeanTween_value_mEA929F405968830C7B82A604B40F5810DA0274B8,
	LeanTween_value_m3F5B71A925784CD187198B3AB14CDB08351AE715,
	LeanTween_value_m214501A86FB206A4A18BE7C373F67B1E3BF90E17,
	LeanTween_value_mEE3F62AA3FED7E7FFAF54C8CB689045550F31E2B,
	LeanTween_value_m062FD40F3682DD46D70A1FD5740CE32C421BE536,
	LeanTween_delayedSound_mEE95C5C431F6AE155BC4B5CE22679353E5BC5BCA,
	LeanTween_delayedSound_mD975E34F6E1D2545468F716845C850F049D1C430,
	LeanTween_move_m255964EF3EF82F6F371AFCAF934CDBE555E31CA4,
	LeanTween_moveX_m968EB608281BDCD184F6B71F22A36B1BD0B32FF5,
	LeanTween_moveY_m8B0C0BD32AECB7A9F1FE4287A1303F8A5BAA225A,
	LeanTween_moveZ_m2E0856F50D20B11039FB477A7F28124713884DC3,
	LeanTween_rotate_m2D1DFCC15465567BB121704213FAA70FD5C336B6,
	LeanTween_rotate_m76084F742248BE37F7A6B458BB34CA78DCFC36F5,
	LeanTween_rotateAround_m3577D6688BDC54193F15C71894E6E55A69006630,
	LeanTween_rotateAroundLocal_mD02923DFFD7FAD7DCBF034734D058E7C99EB8D44,
	LeanTween_scale_mC4B678FE9E438AD23893A27F09E3C007F2EBEABA,
	LeanTween_size_m28011A80D7FC887385A015C3F499A3F33C339E08,
	LeanTween_alpha_mC673E74E2397289FEC358CEFC8E691FF25680886,
	LeanTween_color_m173A290E75DEDA181F8F8BDEDF62C1B12550830B,
	LeanTween_tweenOnCurve_mCF1C2DBF18A825484F5213591A74D0406108BE78,
	LeanTween_tweenOnCurveVector_m2F3889ED1C5209DCB8DF39AAAE310848E3AEB882,
	LeanTween_easeOutQuadOpt_mA9D54EA2DFA885DCA918FF829C7EAFED0567F0C1,
	LeanTween_easeInQuadOpt_mA07BD22C4B17A4FE3B44BC927D18C6E14036811C,
	LeanTween_easeInOutQuadOpt_m5CB73FF539B12D06CD85E6DC09AF281E3FEC2D86,
	LeanTween_easeInOutQuadOpt_mE09DD740EAEC7F818C4AC10178F11B6587DDAFF0,
	LeanTween_linear_mC13897A14BF6CAD2E64BD36644CF8D9AF8EE2F0C,
	LeanTween_clerp_mF38CC191CF1F93FBA5FA125C1BBD362A8F492574,
	LeanTween_spring_mE01745BEC87C32BDF7EA6A91D0B321BADF5884AB,
	LeanTween_easeInQuad_mFADDBAF1404502B07D54D73A5E3E6988D8FA3D24,
	LeanTween_easeOutQuad_mAC0C6A6D659FCDE5636A2AFC5D622B99B61808C7,
	LeanTween_easeInOutQuad_m71AA1E657CE443D018036A98715420BF5925E7E6,
	LeanTween_easeInOutQuadOpt2_m292D5F817A9125E0415C3526F931AA81432A3B09,
	LeanTween_easeInCubic_m3D273E2955A78F68A0F2A9032C2A67E0BAEB6F87,
	LeanTween_easeOutCubic_m2DB944476129350B6D0700C846E3063346F07090,
	LeanTween_easeInOutCubic_m74507BCA5B56F0F295BA3C93D744D332B4B5CC53,
	LeanTween_easeInQuart_m48DE80F5CC0478F91AD6C3F63879747A0C75D1D9,
	LeanTween_easeOutQuart_m609DCDE1826DB79CB2B9985CD60D1B239A984772,
	LeanTween_easeInOutQuart_m0F5D0D7285A20C6D860CF36F25D5D99D59900B33,
	LeanTween_easeInQuint_mDB55F438F15637CA989A645BE894AAF9A004B647,
	LeanTween_easeOutQuint_mBB803F45CF6EE94AC581337373184BC955468FB2,
	LeanTween_easeInOutQuint_m63E5EFF41668EB890D1A86E424189FCEF9F7D4E2,
	LeanTween_easeInSine_m04E033CDA7DEDAD91402A67441771684E9DF9585,
	LeanTween_easeOutSine_mAD08688F562F7B020B432B9F176BFC4E7246E9CA,
	LeanTween_easeInOutSine_m19A665CD11C2430CE4D6B2EE7E49D5E4D69B255B,
	LeanTween_easeInExpo_m5849B8DA222D18020D5DF8CF7CB21EFF6CAF918A,
	LeanTween_easeOutExpo_m8047C00ED545EBB9A49539302273CBD72B99D0DB,
	LeanTween_easeInOutExpo_mC75B771143EC08818C36CD0F1F7A3E5C94ED27DE,
	LeanTween_easeInCirc_m1B31C81F16B5E9297D21C701D372DBD548319883,
	LeanTween_easeOutCirc_m68F5077C75EA1933F562B96FA0EAF3A114D87EE6,
	LeanTween_easeInOutCirc_m76125DC5F83F40B9931F8114B87A44962EADC567,
	LeanTween_easeInBounce_mE3DF8975A300367183C2C926938E6405B33112A5,
	LeanTween_easeOutBounce_m27698CDAABF99F8EDC7D6F6D6586433A9D12970E,
	LeanTween_easeInOutBounce_m9450C71BE4DC36B9E7C7337E89D1BB131E5E0BFF,
	LeanTween_easeInBack_m0D8EFBAB1984C81F9A26AC9936DF26444E8CFDCD,
	LeanTween_easeOutBack_m10B1A82299EE98AEF7C601D14A4D74F613849520,
	LeanTween_easeInOutBack_mA896015E78E55F1FBB1F29D8AA0359A74AC1E284,
	LeanTween_easeInElastic_m0A27227BD0603697DA973D517017937FF956C483,
	LeanTween_easeOutElastic_mC63F462458A6CBE208E739E5F92B4D21A16E2ED4,
	LeanTween_easeInOutElastic_m62A56706E9907A81A5459B47709F75C1947E6473,
	LeanTween_followDamp_m5C9F35D1479612D8A617DA5EF9698BBA34BCFB5B,
	LeanTween_followSpring_m0897DA70DDD37F467E99B8E095157FCD30E02519,
	LeanTween_followBounceOut_m65AA5DB3E225B699DEF3111EF3FC5F33043D5F08,
	LeanTween_followLinear_mE9D8D28E7216EB21260AEF49DC344EB5075E36E0,
	LeanTween_addListener_m3F4A9293AEBEC7AB6896D092CCD808317C3DCD97,
	LeanTween_addListener_mD93323361477B8F8671D3E0C2261A56967E4BBED,
	LeanTween_removeListener_mB3A2221839B0C781D1597674FC5E0674D8C189FA,
	LeanTween_removeListener_mC6FD437AF58BC94743A0A978FAD970886CEF0CAE,
	LeanTween_removeListener_m2DECCE672B558063CCED79464E5401FFC922DCE4,
	LeanTween_dispatchEvent_m1B94CB1AE76D30A0629498DC3CF920C16C3C5B80,
	LeanTween_dispatchEvent_mA17A21F65DC5DFEA2312C5F1446BCD2A13D03C98,
	LeanTween__ctor_m0E530009EEDFEFE39A618092205A560791154327,
	LeanTween__cctor_m7C31F6E8304BED5D2568407206013A918B566DB8,
	U3CU3Ec__DisplayClass193_0__ctor_m42DC73265951A655C0448FF60C880316D9DE1A43,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__0_m191266906DD2277CDC3B46FF109CF35D23BBDAC7,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__1_m704758A390EF812C25A3A65988859C02AFBED1C4,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__2_mAA06748BE5AB364E8F927A9EE39224B64FD24398,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__3_mD56CFC8DF59426863F6189980E747D8413AA5CA0,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__4_m6D340E392A6FE3E0389103E0DDB77C9ED361D9ED,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__5_m0220EC49C37178D39C26609650CE24AAB28621FE,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__6_m2DC42525AE6AA25C9692CA049C80F6958918DB42,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__7_mE7F490B90125F2A8B3C5B91FF517537313A0FCE0,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__8_m007DC11D196C3911171092A96EFF8588C9CBF947,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__9_m26FE2854DC255FCB17A99976AF3B1C9674AF3DA5,
	U3CU3Ec__DisplayClass194_0__ctor_m7BE723A2115ACC48F87D807B5AD61D097D7B80CA,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__0_m96E779A4A9A701F3B635CFB3EBCC96458BD3FDC3,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__1_mACD73E450FC2E3CE551EDDAC93A2F52A45C5054C,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__2_m56BDFF78D7668526967A93EA96C4F427DC600683,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__3_m8F19785EA57FC703DDADF403EE7321F94343C995,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__4_mD7590F09F10CE901B14B9E7C2D59281C17BF6D45,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__5_m0173F460A2260824B38994CCB06B17F86A43BC00,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__6_mDF693219BF56597185AC7A86A83B41CCAE50995D,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__7_m02E8CAD9D7320D317F1291024AEC66A7BA1DF8BC,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__8_m2815FB0A57623C0A7694BEE29292D36748AF2FEF,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__9_m2249E4B791535178AF9B4005460FC55DC089E8CA,
	U3CU3Ec__DisplayClass195_0__ctor_mBBB552F5EA7C92143790A2E28ECAAFA48EC7B4D7,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__0_m85BF6FBF1E6DDC848ED2747320A820E6A16F69A9,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__1_m8C32BD1F9A6489144955B3C032D8EFB4884F2ED6,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__2_m3ADBC5765839ADC33E591B0E3F86D3BCEDD966D3,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__3_m7F6DABA8926F943AB02A6C527D916B95331FD3C9,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__4_mB8C14DF64A83794EB497687CD62EB8246F2F3039,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__5_mA48C1D48F29E8F11F15E727577E658F0CAC0DD46,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__6_m6915716A4F822A502028FEABC63C125E057402CD,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__7_mDDD6AF7E2634F4081FD3849FF6C18B87A3235250,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__8_m90947880CD99C1E7247F57A1AC77692D6FFBFE69,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__9_mB5FEE58DA881C5915A000A3B5988C3D58B578E29,
	U3CU3Ec__DisplayClass196_0__ctor_m53A3A91F675B0FEBE70D8A183F4AEE80BB562132,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__0_mFA5A42D63EE280D45A87226343CAED5E72B4259F,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__1_mA72FB34A781A70DE472941BE6213DAD4056A4156,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__2_m3FFAD56268BA1B8BA721A870C39B2EB3FED3AFFF,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__3_m95959A86F6834CCBCE7080772F0C215459296987,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__4_m17D9CC99AA674FE28A2596BAB8D7AF7EA0AC74F5,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__5_m8CAABA8DEEB326B0DFBA45714E4DE40FA69D2FB9,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__6_mDF3FFFFEFDD9AFFCA4883B15B5BA1C2C568B5238,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__7_mF835E156C518889CC37B7B18196CB66817995693,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__8_m20A928A64BB76244A147659349B4952FB2AEF119,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__9_m485F0716E3A701BA24C7BE51462D4081218137BC,
	LTUtility_reverse_m792C6EFC380420D2FD31BF47B331CBB50B650824,
	LTUtility__ctor_m480117B4F53FC8EF08F8FA640618FDFEB2DA5730,
	LTBezier__ctor_mDE430564C41DA6E441FF61F9075DA00688964D8D,
	LTBezier_map_mC8981CF95BB7EE3FACDC51DC93EE0A638EB7FCD4,
	LTBezier_bezierPoint_mB8ECD0DB38F58EEFB1FCEFA03F96C4F6A04E6B9B,
	LTBezier_point_m11756123BEE5DB1D4A3D97B8920CB6434DE229FC,
	LTBezierPath__ctor_mC14B0C13FB3A48E608456696A8CA14DB6A6B803A,
	LTBezierPath__ctor_m761CE6701D4AF322CDF34898E4A75EE3996EEF32,
	LTBezierPath_setPoints_m0326ECE7DAAF6A7FF84203D7C4DF88207765D100,
	LTBezierPath_get_distance_mCC5AFBA165F1DE82DD616DCCD6A1DF46D6DC2268,
	LTBezierPath_point_mE9E25CE7B83E1F6A9509A12EB82792D0F2AD08CF,
	LTBezierPath_place2d_m86914A18D26070FDB3601E3F4B0A9F337C71D57D,
	LTBezierPath_placeLocal2d_m3B554AF9CDDCAD494F07CFEAD024F82501474089,
	LTBezierPath_place_m92A56AFF394BB4A2B4997C09DC87CF712D67FB62,
	LTBezierPath_place_m089484993924A7E12BCFE5418BD9DA5AB802E664,
	LTBezierPath_placeLocal_m84ECB572C31B7DC9420B9E22745AF8E553EA8EBE,
	LTBezierPath_placeLocal_mD55D6F5C237A9A3E3CF585A38697083AD608A6C9,
	LTBezierPath_gizmoDraw_mF83D68461DAAD27D153D7C368B9A17CE33FFBA2C,
	LTBezierPath_ratioAtPoint_m143BC2A593074BDC7858B6861351A0D044EC38C3,
	LTSpline__ctor_m9551907DED1FDB0E2E0530EC1773BEB94364C936,
	LTSpline__ctor_m2F73C5AFFBC6626390D7B2FA507B202DD52225E2,
	LTSpline_init_m25191659D43D582146D1C29714BD5B7B01D6B5F8,
	LTSpline_map_mDCB7147639A49699ED42F125EF6A5D27CA9E1485,
	LTSpline_interp_m75A500E14151D59BC9F7240B4AB73190AAB1E598,
	LTSpline_ratioAtPoint_mC3196D78FBCD206F34E51D528EEB6D9DE5093B76,
	LTSpline_point_m84BC62A0BE5F6F90E7AE7632BF88AD5BB332E75D,
	LTSpline_place2d_mF79DEE3F7C5F54541047D267A2E88C681E9A0C52,
	LTSpline_placeLocal2d_m6BA2FF841EADD05FA6CFBAC808B8E7D6641D154B,
	LTSpline_place_mEC9C1AB39B6FAFC9D8F41CEE63391BCB2A577DD9,
	LTSpline_place_m2F73FDFFE8520633664738EA4D3E769154EC645E,
	LTSpline_placeLocal_m861F5C7F91F4A15BD04D1F2C66E196219AC2B10F,
	LTSpline_placeLocal_m8A2D1C6DA0C7BC6E9B61E6577998C6397000F388,
	LTSpline_gizmoDraw_mAADED3A63F49D610C035F71DC725D214895189B9,
	LTSpline_drawGizmo_mE14FCC2AE443D4F66CB45A64754557476051945A,
	LTSpline_drawGizmo_mFC2AB1F7539EB9239D2AC471B276CC5A380A6A5C,
	LTSpline_drawLine_m4CEECA0A218EA8067F860F48FAC4E6CDD27EB7E7,
	LTSpline_drawLinesGLLines_m248D051DAF686041049E3994F3F3FCA9434DB823,
	LTSpline_generateVectors_mECF369B072F107D79E9AD06DD4D7BBBF5E5716D3,
	LTSpline__cctor_mEB46040AD009E27CB5CF44A9F39148191983681E,
	LTRect__ctor_m99E599C0ECB67B47DEE7F91377ACB404E81FDD9C,
	LTRect__ctor_mA9DDB568C78410BF943CEC80A10FCC9FE8E7E0CB,
	LTRect__ctor_m3894896A04C8DE5CF36FEB67B77A9D51471AA5EE,
	LTRect__ctor_m3FE04D973AF8662CF993C5C7A99A8190AF660DFA,
	LTRect__ctor_m1FE9274A3C18C4E81E0E68C91DDC727712CC0005,
	LTRect_get_hasInitiliazed_mB71593427DCCF0404DEB52065AABFF1762158555,
	LTRect_get_id_mD85F4AFF11D30E3CFB7F39142F98EB798EBC2950,
	LTRect_setId_mFE916182B6874208861CD518BEE9F0C89EF7EF8D,
	LTRect_reset_m79849D25F50652D6BCE8CA8F3F329EE9511852DA,
	LTRect_resetForRotation_mADBFF61280065301AADF66876334779462287788,
	LTRect_get_x_m515C6A230B6B72CB215A27535B967428BD6DC3A7,
	LTRect_set_x_m2F416C9557DD5B8F51C00D0710A634CD1B25FEEE,
	LTRect_get_y_mA1414A3C465D24CCA90A9C73D6EEC35F7411C957,
	LTRect_set_y_m40A2235B9E00614E61213F394430C86962E9CA42,
	LTRect_get_width_m04A3F854E9585909A7948905CFFCC4C3AA726B60,
	LTRect_set_width_mC8D40CA7ECCEA4B3E2E3B0C0DFC573CD6A461888,
	LTRect_get_height_mB4F9445DF5CF2F82979B009EEF48A4F22634CE44,
	LTRect_set_height_m52D3E7ACF7372560DD7412E524171FBF34B2FBE2,
	LTRect_get_rect_m775DA6B41DFC9720EDBAA6ECD285058C4BE41505,
	LTRect_set_rect_m09C60F3DDB77909404D570E60060AEDD73BFD969,
	LTRect_setStyle_m5E5B4BC5ACD18F9ACC62171F440460CBB4B549F1,
	LTRect_setFontScaleToFit_mA9F65B0883725878887560F549C2F4820445CF3B,
	LTRect_setColor_mD5BD0C65451DDD14F2927F5BFA617DB93B9E2CC6,
	LTRect_setAlpha_m5F785B6E9D8C6DFD7CC751417A86959041F5D845,
	LTRect_setLabel_m87C389A819F76DBCEABE8968C2C357F43EEAC6DC,
	LTRect_setUseSimpleScale_m9C2401689E18E8B93BC6C9A95E781226E765E81E,
	LTRect_setUseSimpleScale_mF4509500DEBCBD33A6CBFE6F4B7C2BC4D75D845C,
	LTRect_setSizeByHeight_m002DF767AC9C9ACCF561B65C2809C32FE07D8759,
	LTRect_ToString_m1034234D679BFDD3FAEB6E69E031652F13022AA8,
	LTEvent__ctor_m6ABB897986AE37C647A526BA35CD577196C80053,
	LTGUI_init_m90D0DAAF20EA870C10FC394D34AE654788D6E9E3,
	LTGUI_initRectCheck_m01927ECCEAA6B39214CAEC2DA11C59C7D7A417DC,
	LTGUI_reset_m4AF33FEE1C4FFF585B61E00329512D26D9348405,
	LTGUI_update_m346A6915B0DA4CAE2E17FE892D39122992599AE0,
	LTGUI_checkOnScreen_m018B856681A029B796023FFC001610DB7C87497A,
	LTGUI_destroy_mF6818BEF1B1A6510F9F38B20B4D4ACA1E4BAECE4,
	LTGUI_destroyAll_m6C70120370B727440C087BC73ED1F62914431B5E,
	LTGUI_label_m568FA2530F19BB9D082825B7406336A90CA7BF19,
	LTGUI_label_mF0A3E4EA3BF86BFFAFF265B1E848418ADF91B4CA,
	LTGUI_texture_m18FCD865307F279E7E6A0CF29D3E68DA0125F09E,
	LTGUI_texture_mD8320410FA4A576CAC639511DAD9271A8C2D32EA,
	LTGUI_element_mB7414EFDA599B3627466CD84A505842398B3BDAD,
	LTGUI_hasNoOverlap_m8F6CBA73181394C020334257C98D15D73222A21B,
	LTGUI_pressedWithinRect_m6955BEF35822977E4C488F640E982C05624D2C87,
	LTGUI_checkWithinRect_m7BD673AE0509E6E36A939C2A15547FB1B35425DE,
	LTGUI_firstTouch_m59A232DAB90147A8D67F2636CF126E3868B749A3,
	LTGUI__ctor_m3DF0E374662C139E92CE25BD00208671EA425500,
	LTGUI__cctor_m4A1C16EF1B32B267816AE59E983E024BDF672F06,
	LeanTweenExt_LeanAlpha_m622BA3E2E95AA0C3ED769D14693452C71B464202,
	LeanTweenExt_LeanAlphaVertex_m276A64D30C7DBC2962D952F39164E74266A80805,
	LeanTweenExt_LeanAlpha_m5D4F86AB57DEAB1788098157DD3858155B41A026,
	LeanTweenExt_LeanAlpha_m80C6BB78F56CBCBAF562EA721D66F568AEB253E5,
	LeanTweenExt_LeanAlphaText_m2E879D95FD0A499D43742BC7E19C70FC00DB3C2F,
	LeanTweenExt_LeanCancel_mF59992A82C61655FB9C3C02CAA26A659B9DE3646,
	LeanTweenExt_LeanCancel_mD017D8DDC025544954D2E68BC260FC83C9469661,
	LeanTweenExt_LeanCancel_mC35E34FD1DF38362DA3970677A1A3745F02873E5,
	LeanTweenExt_LeanCancel_m17794FCEC25F233AC350724707F017F476BEA6B1,
	LeanTweenExt_LeanColor_m444318E458A875BB4CB2A0DD294D992CD93EBEFD,
	LeanTweenExt_LeanColorText_m400922DC92DE54BE07F329E4949145CF60571731,
	LeanTweenExt_LeanDelayedCall_m2377F6B8E51CB5CDEA727E0ECB5719E78E1E6DC8,
	LeanTweenExt_LeanDelayedCall_m813456BD5A6C3BEF150219E1FB7FFECF3CFDB8FB,
	LeanTweenExt_LeanIsPaused_m004A9E480599A969A094BBF5EAB8298D25F72877,
	LeanTweenExt_LeanIsPaused_mAB912B24C71D148315C8E71232F9B54C2FADB5A4,
	LeanTweenExt_LeanIsTweening_mECC74A5F933AEEEC71D8EBE7E977639DDD124F59,
	LeanTweenExt_LeanMove_m054FBB59F420198B550A6069D9F9E6ABAE6AB43A,
	LeanTweenExt_LeanMove_m615AA30F73F0ADF408BCE5B3EB2E7B0107F613DF,
	LeanTweenExt_LeanMove_mE09C2744CD6F66260CCB2AE85EEC189D1739A95C,
	LeanTweenExt_LeanMove_m213A2BEDDDE4818C47346D971A3D99CCE02F1E51,
	LeanTweenExt_LeanMove_m0A44978D179428622357E68D3AE31FCD5597B014,
	LeanTweenExt_LeanMove_m47C512A7C68F548F0C20D61A9EBC21444A89F47F,
	LeanTweenExt_LeanMove_m28CCB67D4CD2B89B5A3F25E9D70842C949AD0990,
	LeanTweenExt_LeanMove_mB1E50A4734E3C3CC223FEB98A394DBDBA9614850,
	LeanTweenExt_LeanMove_m0BF63EB3A8D4B614817EA402EB1CC4B5F2C83328,
	LeanTweenExt_LeanMove_m184D419AE52BA8FFCD237883D4118434DA2F40BB,
	LeanTweenExt_LeanMove_m31A80E7BD7568CEFF3D7B4073D2BA176A9ADF3DF,
	LeanTweenExt_LeanMoveLocal_m15178FB562F6F85C35B682AF97FCA004BDA1946F,
	LeanTweenExt_LeanMoveLocal_m91600FA81E8D5DB2D9C69E4F7C5FC09CC40D2DC1,
	LeanTweenExt_LeanMoveLocal_m98D7F1A2052D0F25DEE7B58DE411EE62A8B27CF2,
	LeanTweenExt_LeanMoveLocal_m920B50D3FBD4DC072118E879AFC151D682AE64B0,
	LeanTweenExt_LeanMoveLocal_mE4CF4E64AC16B510FB904060DDC2AE8BB231AA51,
	LeanTweenExt_LeanMoveLocal_mA49B57CD94C59A10CA8AF9D53F71C48286EAFFD0,
	LeanTweenExt_LeanMoveLocalX_m8847296A8B3401D2D054027ABB659B296FF73760,
	LeanTweenExt_LeanMoveLocalY_m86889CA4B214309EEAC239C3F175937E24310FD6,
	LeanTweenExt_LeanMoveLocalZ_m248064CFFDF7E42AC612D7F72C1E0F2DA37DCFB4,
	LeanTweenExt_LeanMoveLocalX_mEAB28137FE65A74C1F4963ADCFF044CD5DB7CF69,
	LeanTweenExt_LeanMoveLocalY_m727F1ED96B0D8FBF32EBB69787F82561886A07FA,
	LeanTweenExt_LeanMoveLocalZ_m731E4A396200E9C165B47A327CB59CCDB9817707,
	LeanTweenExt_LeanMoveSpline_m0E5CBB6704835E27EDA9B25D2915E8D2360E11A4,
	LeanTweenExt_LeanMoveSpline_mF640B6FFF8B58004A5C0A4475FE36E2C5BBAC607,
	LeanTweenExt_LeanMoveSpline_m3EE259E4A0061A1497D195E7910EAE9CD0B984C5,
	LeanTweenExt_LeanMoveSpline_m59EF921EB07DAFBB715DAB0BBCFAC8003E67BD6C,
	LeanTweenExt_LeanMoveSplineLocal_m667397DE03EFD1DBE0CE5F6CC2287807C3A56761,
	LeanTweenExt_LeanMoveSplineLocal_mB8706C86995411A7D6408A04AB6B3545922C192D,
	LeanTweenExt_LeanMoveX_m95E776B4AFF3B3472159C21ACCA7A074FD6250E1,
	LeanTweenExt_LeanMoveX_m5EA136BF257503853FF984AB882BADCEDD22F864,
	LeanTweenExt_LeanMoveX_m2E387E7F079BC81CCBE68B4D705C31E581639352,
	LeanTweenExt_LeanMoveY_mD3811BDA209686DCA906AB52B2CDD29F337FD6F4,
	LeanTweenExt_LeanMoveY_m65DEFE466DE737FAB4B1DEF0BD8DC8ED22D03433,
	LeanTweenExt_LeanMoveY_m846E64F6EED274DD02405DF113895FD366BA0FBB,
	LeanTweenExt_LeanMoveZ_mC2E89CF6F065D18A1F456935AF7EFA51091F0D9B,
	LeanTweenExt_LeanMoveZ_m85D8863F40BBF5A99203396C72B421E022FAA417,
	LeanTweenExt_LeanMoveZ_mD47791E76E05328EAE363F9A3D0B8DAE6713902A,
	LeanTweenExt_LeanPause_m3B5948BE9F86AAB0C837383D547DF2B291B8EA43,
	LeanTweenExt_LeanPlay_m6DCF6A71031221B880B66660EFF17720C6F1A42D,
	LeanTweenExt_LeanResume_m0C9B09411CA1C367B2E2F1E74ECC4860BB400BF4,
	LeanTweenExt_LeanRotate_m9A3EA584BE26EADB024F0E5EC57B0862F3B0BEAE,
	LeanTweenExt_LeanRotate_m46EDCCD0F931B4488BF8EDE72F0B1BA6BD65261F,
	LeanTweenExt_LeanRotate_mF34054132FFA550762A3C04473FEF3FB54931CA0,
	LeanTweenExt_LeanRotateAround_m97EA89B0C3FDE7B29B2A5F2FE3BE10F27B0C9993,
	LeanTweenExt_LeanRotateAround_m9922AD8260E8767A2D33963F2C9BA18ADDF22D61,
	LeanTweenExt_LeanRotateAround_m7F47B45F2074B30B22D75DC117BCF0A1351710E3,
	LeanTweenExt_LeanRotateAroundLocal_m43F940634C80C6D459B9DACF22F06E1E42F29116,
	LeanTweenExt_LeanRotateAroundLocal_m81397BB2C883AC5B3D62716FC3C93F43AE4EE5EB,
	LeanTweenExt_LeanRotateAroundLocal_mE1E669F2BCAAEA47AB4785D6BBFAE413CC74F053,
	LeanTweenExt_LeanRotateX_m0C6142BF35F74AC9A912509713E53E9F1B3F1A20,
	LeanTweenExt_LeanRotateX_mD09A3A174615A5479F81DFF31192673DADBF3324,
	LeanTweenExt_LeanRotateY_mE334362113CE7DA83AB29EFA1AAB8BFEB8C326E0,
	LeanTweenExt_LeanRotateY_m823DD2E4B248E09CA4E2CDDFD2D791DF8527FCD2,
	LeanTweenExt_LeanRotateZ_m587B987933373ED9B32F6D319F158899A25AF9C3,
	LeanTweenExt_LeanRotateZ_m25AA9563CCFC5754ABE0CDE84FE8E05F09B0357F,
	LeanTweenExt_LeanScale_m883D32607662D32DAB92DAF865CF110680BF18B3,
	LeanTweenExt_LeanScale_m572C2CB624F08B589CA3F7CEDA18D0521D396DAF,
	LeanTweenExt_LeanScale_mFDA8A475A889665712B39717572167439241C6D5,
	LeanTweenExt_LeanScaleX_m9AC6DED15F097E50134DF4213F86799F3A3F370B,
	LeanTweenExt_LeanScaleX_m469A49E471B4D706CF5859D368CD3F13DD4FBA67,
	LeanTweenExt_LeanScaleY_mEF7B8B2B411B70E312A061227CC9EF7496F1A720,
	LeanTweenExt_LeanScaleY_mC152F2D04B0CB5770AF0157B8BA217DDC0881D61,
	LeanTweenExt_LeanScaleZ_m23CAE903347B1DFEA59FFFB127419620967227F1,
	LeanTweenExt_LeanScaleZ_m2C148ADC5EA94A54CC00F349B1DF331079B13B16,
	LeanTweenExt_LeanSize_m410361AD3B78FE2A69D65C21570490F82E660241,
	LeanTweenExt_LeanValue_m1FB1FFD9157FA40B5854744EA15111178AEDE1C1,
	LeanTweenExt_LeanValue_mE9099E0F64A010AD2BEA7030C5FEFEDBE78A1571,
	LeanTweenExt_LeanValue_mF09B8E5D72CBADA7E7335A1CCE8397B6BEE0B286,
	LeanTweenExt_LeanValue_m165CCF09A13FA398105E06E6B441D0C5BEF33E20,
	LeanTweenExt_LeanValue_m2D0676A4B078FE10C1819ECF5520374CA2E39E8D,
	LeanTweenExt_LeanValue_m626C6C95ABDA16428DAD1399B0862363589AC9F9,
	LeanTweenExt_LeanValue_mBB6A5387301F308B4C42B4DF5B70C17AE29F5533,
	LeanTweenExt_LeanValue_m93C212023658F9EF6A27683DEBAA852EE4670411,
	LeanTweenExt_LeanValue_m5B60256ABE5F8B7E28F389AAE1696EB656633398,
	LeanTweenExt_LeanValue_mBA982D980197033944CF17D7C7F236A6CED885DF,
	LeanTweenExt_LeanSetPosX_m82201BB5C9781FA41FB69DDA05471D693E59DF2E,
	LeanTweenExt_LeanSetPosY_mD9DAE6B462055110382822D16B5551854CB68DD7,
	LeanTweenExt_LeanSetPosZ_m1E17DB053DD69876E163757D053A85B172CAC027,
	LeanTweenExt_LeanSetLocalPosX_m2325658730387BE391CB8FE674FD3FD2D8D3ECDE,
	LeanTweenExt_LeanSetLocalPosY_mF14BA20EFA312158AB0FD456EB8762457DB03AE5,
	LeanTweenExt_LeanSetLocalPosZ_m685180D2C04DF7E3417E9EECA9BEBE60F0850790,
	LeanTweenExt_LeanColor_m2027EB778950A5E71F3DCF398F8A6A77AE32EF35,
	LTDescr_get_from_m5C46DBE96932E1F21CB71C1174B3294CF8F80E4A,
	LTDescr_set_from_mCEA64BFED572C9327D1186A8DB46FFB56836E4F5,
	LTDescr_get_to_m3887E20BF193FF607A48502338A46C90948F5FA5,
	LTDescr_set_to_m12DADAD8999E97D8D46F4219632BFAB4FCA41796,
	LTDescr_get_easeInternal_m263F859E9225DF85EFD75C079CD5073AE647B284,
	LTDescr_set_easeInternal_m40A3FEDCAA0EE7EB86E804641367E50E34C0CDB9,
	LTDescr_get_initInternal_m3E9E4486FBFCFFDB8A3A91EB00DBEBA29916C943,
	LTDescr_set_initInternal_m21E7CD474330836550F6298EF7F0F2902BC3EE60,
	LTDescr_get_toTrans_m03763D518E68ED5106B72B7DEBBD29765285CB78,
	LTDescr_ToString_m8D2047DA16E09587E6709505E2328386ED0BADA7,
	LTDescr__ctor_mD2905F6F8358B3AD8C5784EEA14DE3C05769538B,
	LTDescr_cancel_mC12FD3EE1E42AFCC7AA536A6D344BE3AEA86AE70,
	LTDescr_get_uniqueId_mCCC4A89D6863332459B9771B4E636F951B2AAD3B,
	LTDescr_get_id_m71271377DB5ECF8CF10782531395CCB0B5478E09,
	LTDescr_get_optional_m0475EBA98D33B8F970E159516CC6BBEC331BE807,
	LTDescr_set_optional_m17425AF517082967050C04F2C3B3D2D574DDB2B0,
	LTDescr_reset_mDC3B5AEC9CB77A12D954B5BB283B062A950BC8DE,
	LTDescr_setFollow_m984BCBD0F7B73FB1E6ABAF2C1B4CA1EE506546DC,
	LTDescr_setMoveX_m76CE7ACEF44A88ED9BC3E747549EE2CFCB2013A2,
	LTDescr_setMoveY_m09BC6472FAD45D9AB081F3D63C49C09E1369205D,
	LTDescr_setMoveZ_m421A6F5AFA9E4627D93946E2797FE1C4DB561DF4,
	LTDescr_setMoveLocalX_m75617B5FB504CAF9FF78274042314A7A20388ED6,
	LTDescr_setMoveLocalY_m22DCFF89FDE31EBEB557B9964E55DFC264110243,
	LTDescr_setMoveLocalZ_m5B79B96D94FF6B39A2CFEFF240E3C88D6388F6A2,
	LTDescr_initFromInternal_m639FAC099FE64CC165535CCE0719472B44AC7F22,
	LTDescr_setOffset_mB2DAAF3FEB7717194BBA1C04741D4EF68C8F952D,
	LTDescr_setMoveCurved_m0DBB6E2C2BAA1806B5027FFA8DC167215CC9425E,
	LTDescr_setMoveCurvedLocal_m81E717B0084EB49305FF083C0CADA94DCD974319,
	LTDescr_setMoveSpline_m0927D09871DEF890A033AC185FD26BCDBE1911C7,
	LTDescr_setMoveSplineLocal_mC0E62F0B0E9597DAAE5027FAB2F649EAF2E13034,
	LTDescr_setScaleX_m7CFF1D6BE843E9721DB183EB147279B195953B7B,
	LTDescr_setScaleY_mBC9B04832806ACB660D3E326621711B15CF7F791,
	LTDescr_setScaleZ_m0401AF8B4C25CB3AA2CDCBF4C2DBD75166897C0D,
	LTDescr_setRotateX_m26F91A22B505D763B444E6523057008EAD6FD7B3,
	LTDescr_setRotateY_m52016439FDA153BB871679C34D65946434862E66,
	LTDescr_setRotateZ_mBC6D82441A461E415CB255195BD66FBC4AF132D9,
	LTDescr_setRotateAround_m346F3BDC7A38E146F5EF8FACC1DBA07BD3684918,
	LTDescr_setRotateAroundLocal_mBE18B57E33C1118A01FBDB41A99DE69746C9C865,
	LTDescr_setAlpha_m291AFD44926FB68DD36823B9D142AA276D072962,
	LTDescr_setTextAlpha_m569C24FD4701EFE34149E4803E189518B383B0B1,
	LTDescr_setAlphaVertex_mEF508AADA9444AAB3946ED1C2A3506094A541E4A,
	LTDescr_setColor_mA47DB19E9808DD9AAA142AB9CC3D33C4A0EFAB04,
	LTDescr_setCallbackColor_m9EA1B5111A4241C55BC10C0705147A33BA07AC33,
	LTDescr_setTextColor_m3A20272C52CF7141B376DCFFB58C4BA642DD4A83,
	LTDescr_setCanvasAlpha_mD05551595E5C4D79C3242EEEEA99AD01274C0BF1,
	LTDescr_setCanvasGroupAlpha_mF556BF3EE226BAC974CC36C638CECD86BD29E2B3,
	LTDescr_setCanvasColor_m52C8833AC9DCC36676E9795E24F31D7955CE17C2,
	LTDescr_setCanvasMoveX_m7B0F43CC9D9B78A933E1B5EE3CF4A9944129B11F,
	LTDescr_setCanvasMoveY_m5A9AAC03AB65F7B5E14CA663EE591F9FF8FDE5A9,
	LTDescr_setCanvasMoveZ_mEBDF9362DB395299B0E6C4EF45A8E5DF8FCA5CE0,
	LTDescr_initCanvasRotateAround_mDF5155DE268AB0DB414A892643342B04C3C80430,
	LTDescr_setCanvasRotateAround_mD875CE01D408277380BA7E1A90855FAB176930E5,
	LTDescr_setCanvasRotateAroundLocal_m0FB16DFF151E8479B394929E477812B908E71811,
	LTDescr_setCanvasPlaySprite_m5507D16A5CA3D57966B06FE22CF280B3635D272D,
	LTDescr_setCanvasMove_mBA28E1B08C4CDD0493F4AEE9032DB47D8DE96A57,
	LTDescr_setCanvasScale_m25D2078C0E8BC068AB18C945177B424FD8BDF35D,
	LTDescr_setCanvasSizeDelta_mD3C9CBB1C8F686F2E253C22DB1A2C841C39D00F3,
	LTDescr_callback_mCC3CF88649A2DB3AADEA6D010D9E1D6F8F0B50A1,
	LTDescr_setCallback_mD0C03116D293C89C840A0EB7D7276987E5704AE4,
	LTDescr_setValue3_mDCA24E0028F363246355199865E0E9C795919C87,
	LTDescr_setMove_m0A39B796C492B5F74EAC7A88B614DABEEC83E31D,
	LTDescr_setMoveLocal_m7979A95A8C3E83116630A1972484ACB778E042B0,
	LTDescr_setMoveToTransform_m44267D9670BA0AA3F282AF4654C65465EBE562CD,
	LTDescr_setRotate_m481E57B448F3773EB7E8FEDB4D54061FC454EE79,
	LTDescr_setRotateLocal_m2C363D0AC041E31D2D2ED4143CFFBA71E6625EB8,
	LTDescr_setScale_m0E480B54D7D9C2B8268023050E9231052B87BCA9,
	LTDescr_setGUIMove_mCEF7E5A5AABE334A23DE8CDBE26EDD4658B06CA7,
	LTDescr_setGUIMoveMargin_mFA7CF549318E4EA41AEF0A72DE52926C88AE3D28,
	LTDescr_setGUIScale_m428EBDD7206CF4353C456CDD2554ACAD5989457A,
	LTDescr_setGUIAlpha_m316B0C88A1C8D63CA862FEF28A6AC263D043B4A5,
	LTDescr_setGUIRotate_m5C0C9A675F084647D909DA88099F8A056CC18DD0,
	LTDescr_setDelayedSound_m1D0040CD3D5090096914684D47D8076A21A0D732,
	LTDescr_setTarget_m5F34436BE6EF6A6E6804AA39FD773562BF873647,
	LTDescr_init_m9C699F46C58C3BEE7F19E14E4B737FBC34685B60,
	LTDescr_initSpeed_m4F4E1F0B62028C76C78B000403BDDFBABCB30754,
	LTDescr_updateNow_m7A17C67327BF1CA89C94257396A0B64F916FD330,
	LTDescr_updateInternal_m6F69EAADDCA9F3FC16D96351A97EB4D0DA935A76,
	LTDescr_callOnCompletes_m5F22E85C54CBAEA43EB1E2972B309DE885B6BC72,
	LTDescr_setFromColor_m93EB8D7B171E28CC66A866DFE42C4637D17BF9B7,
	LTDescr_alphaRecursive_m9862620727DE002BA5334449EE51E47FB9C35BDE,
	LTDescr_colorRecursive_m66AF77EFA7536C295518504A00B20968E3F773AD,
	LTDescr_alphaRecursive_mDE10D7A23D1710400E453C787924FB83F89EFDDD,
	LTDescr_alphaRecursiveSprite_mD677C77EFB7426B437BF89A60BE8D29B6E3ED014,
	LTDescr_colorRecursiveSprite_m2BFB41154D3FBB7D70FF5F2BBB8C3B0F4FC02699,
	LTDescr_colorRecursive_mDFBBC244E1A546B872218F43CE4D3E230C07DC35,
	LTDescr_textAlphaChildrenRecursive_m30329E963CF1363F6841959DD91EED9FF7EA320B,
	LTDescr_textAlphaRecursive_m12B0431A56E4A23B20528CD399BFA2CE80658E3F,
	LTDescr_textColorRecursive_mFA16970EB10C436367105DAEF64A22B7C584CCF0,
	LTDescr_tweenColor_m3020E4C1CC76332688E53BF95D72BD1B3CB461A9,
	LTDescr_pause_m831C5988C87832DF30D6566086DD174849A8712A,
	LTDescr_resume_mBE4F94BDD8338970F8DA5167345E87C4B58B30F8,
	LTDescr_setAxis_m786C5D48517CA736F5B704D876E4E69234E087E3,
	LTDescr_setDelay_m55BD6D8AB740123B2EE42BA1721C7E6E29504110,
	LTDescr_setEase_mDE953D5A1E2D1234C5CFD2F0CDB6F32B787ACD0C,
	LTDescr_setEaseLinear_m02CEA15BDCDC76F3FFE4AF8C8C7D4C4BE696A2E0,
	LTDescr_setEaseSpring_mB49258E88824A2EF22CA9CFA751754D59C00646D,
	LTDescr_setEaseInQuad_m1BD7E1556B02807399E80DB203B9553AA766B35E,
	LTDescr_setEaseOutQuad_m9FD9511C826BB2309FB7ADA82620AC454C207960,
	LTDescr_setEaseInOutQuad_mAD680A08BE87DD7F92202EEEA941DBD71A78B92A,
	LTDescr_setEaseInCubic_mDB7AC25B20EDCDA9371911EFEA0D073548C13681,
	LTDescr_setEaseOutCubic_m405E1A83E795CC30CA245DA33133F21D9FCD03A9,
	LTDescr_setEaseInOutCubic_mD69B9E20BD56C26709938CE025470378595D804A,
	LTDescr_setEaseInQuart_m8B52F62F717ED83CB7CD00E869E548FC5C60E0FD,
	LTDescr_setEaseOutQuart_m926A771E75DCEF5025BA544D0D4814D802658967,
	LTDescr_setEaseInOutQuart_m36299EB57FE5C7B772B07760AD79A4758A407EAB,
	LTDescr_setEaseInQuint_mC37282FD3528ABDE0B9ABBECD0EE4158EB78BB34,
	LTDescr_setEaseOutQuint_mB5DF94AAAA9986D805ABD7B8705957F00C9FA298,
	LTDescr_setEaseInOutQuint_mD107E1CB8DA7D291E2507DE864FE3BA7690D3667,
	LTDescr_setEaseInSine_m558572F5D792761C77311F93E823AA522ABFA821,
	LTDescr_setEaseOutSine_m04078632ABF4A13E48FFC566DADAE650E3531D8B,
	LTDescr_setEaseInOutSine_mCBF1B47223AB0E40EC462E99311B5BAC92CAF0F4,
	LTDescr_setEaseInExpo_m031D49646619C11E4CD9124E33ACDE7455963D61,
	LTDescr_setEaseOutExpo_mD8C3A31B2E8E473F34744C5BD4855A28385777F5,
	LTDescr_setEaseInOutExpo_mC6247CF20D6C40011D6A31C5AFB4E93C2E27D83B,
	LTDescr_setEaseInCirc_m64F3ED13ADA1CF100ADAD67A01A5C57FC073ECFF,
	LTDescr_setEaseOutCirc_m3FA5A19F0A8F63DE7F2FA3AE57A1E0DF371451DF,
	LTDescr_setEaseInOutCirc_m82DC1F56BD70CAAA7CFC0786FC31565D4B7D1F8C,
	LTDescr_setEaseInBounce_m0A8189AD4174381A97F215E75C5D40485395A549,
	LTDescr_setEaseOutBounce_m3E4A3DBF4712B6F566D5FFC5B8AFD96DB06FA374,
	LTDescr_setEaseInOutBounce_mEC4B3DC43EE8829DB40045AE072895AB782B9FDC,
	LTDescr_setEaseInBack_m7A4BBFA0DA727FA51678D7B4561F588CFB1454E6,
	LTDescr_setEaseOutBack_m53F3499FE76CAEA5F462EC96113A265E15438418,
	LTDescr_setEaseInOutBack_m1970B0DA7214B401D9894AD2BDC75A96016FDBBE,
	LTDescr_setEaseInElastic_m9D356852FCC3BEB9002EC375047192B257CDE9A6,
	LTDescr_setEaseOutElastic_m97DCC808E07D990199DD09AE961EF7CFEB401698,
	LTDescr_setEaseInOutElastic_m1CC9921334D9A64FF1F4C2E5285F3E5712C6D401,
	LTDescr_setEasePunch_m1DF3D2CFEE5E2CB6EEAF5F485DF326F8B2A08B66,
	LTDescr_setEaseShake_mAC2E54A2A115410297235E85E9567AD5D96CC86E,
	LTDescr_tweenOnCurve_mAAE98016598CDD3C0E19EC9016CDAAC27290FE40,
	LTDescr_easeInOutQuad_mA1B3167A272546E8F4AD51BA4456D911E7A97CAA,
	LTDescr_easeInQuad_m8B8A3D80C2483095A2E9319E0E560A16A5D45E03,
	LTDescr_easeOutQuad_mA14EA059114AEE2C71D891DA1D723554387FB0FC,
	LTDescr_easeLinear_mAFF573D79A9494A37F171FDCFB1A843E4A099BBD,
	LTDescr_easeSpring_mBCC692E63F1CA607883E42FD5EF44799798CF544,
	LTDescr_easeInCubic_mA1F3409DFC4FAB01F0E1D7D5C13AF3EF533A592C,
	LTDescr_easeOutCubic_mD3D32B7F8A774C06F28C78EFD8B35F82ABE0B2BD,
	LTDescr_easeInOutCubic_m10079E2EE4532DD46450DE324ACA7091D4082449,
	LTDescr_easeInQuart_m7F3F07BAF2296FE3EF14D9315A45B101B4A88C48,
	LTDescr_easeOutQuart_m19C687BA558F3962E3D94A9224DFB21AEB8DB6B4,
	LTDescr_easeInOutQuart_m4A83DAC8CFD32AC6A3F5BE9847FAB5B397567EA0,
	LTDescr_easeInQuint_m8DA64234B559AEBF6F300EF6FB4A168247F54DD3,
	LTDescr_easeOutQuint_m72712C773DCFA24B83AF2013806A795B3874B4E2,
	LTDescr_easeInOutQuint_m860D58335F979A03E4BDA78BA9D37BF3F9CAE3C0,
	LTDescr_easeInSine_mCF9996B096BEB3C47C523DB7DC3DDE5EEEB362CF,
	LTDescr_easeOutSine_m56F54AD9F205357269FB50FC3DB13551BBDE7AA8,
	LTDescr_easeInOutSine_m4ACFEF2C6BDF120DAA6C76E563B6AE6CECD5380F,
	LTDescr_easeInExpo_m07C8C2CF2B118A52CBDC361310B8D8C3DF366A52,
	LTDescr_easeOutExpo_mA866024A98FD2A2CC8808917B983BE6D3BF4156A,
	LTDescr_easeInOutExpo_mBEC5D63CA1D9C4E0F6EB2C0935E6D2550E8FBBC9,
	LTDescr_easeInCirc_m5DB7FCD1C107646BCBF7BACAE93191864E5A7BDA,
	LTDescr_easeOutCirc_mEA8558C7773468D1860A2BDC636FA5EC2C8791D8,
	LTDescr_easeInOutCirc_m49B59C6FB90730D195CFE5910C1A1596F18385C3,
	LTDescr_easeInBounce_mA53E96538907F54DF3E668026D7F62D8A346B325,
	LTDescr_easeOutBounce_mB2732EE22499FB0ED83AFEF85FD4896E629A19C8,
	LTDescr_easeInOutBounce_m5F2B5E4F09C66FA720583B13EE7DEBF46E4B4250,
	LTDescr_easeInBack_m1AD66A3F3E0513ACF1D06C01909DB989D263ACFC,
	LTDescr_easeOutBack_m971AF92F07A84D5E01B10F1CED6F43F02217CA0B,
	LTDescr_easeInOutBack_m9AEAAB6088AA50FAE1F839D39D70B79250467DC1,
	LTDescr_easeInElastic_mA980A8C764DFBC3A2DCE4CC566A3AE5D8DBB4F15,
	LTDescr_easeOutElastic_m4E6536A1BEA97546DDB61146FD6A78BBF31E1207,
	LTDescr_easeInOutElastic_mA4E464B18D106514E4092266A28D8EA4138858CA,
	LTDescr_setOvershoot_m47B052637792C78A0D5195306CCA86CB9E7A96A8,
	LTDescr_setPeriod_mC03D996C9712715E08646FDBD5421719CFB8F3DE,
	LTDescr_setScale_m20ABE114A3769DF22F968A3BAE3B969D7405C84D,
	LTDescr_setEase_m85FF6788C6DD67CB57F667E80E456C7CE86C9E3F,
	LTDescr_setTo_mC614978FF31785D8135380F7BF1FEF8ADD2F365D,
	LTDescr_setTo_mFE5077089EAAEED3A0B4E00827BF4867FA5E4E76,
	LTDescr_setFrom_mBFD923D83E18FB53AAFB9293C1F13175DCE15D34,
	LTDescr_setFrom_m82D22FCE616A6826A3007BCF148DA437F16BC0F8,
	LTDescr_setDiff_mAF9E8982F70181B987022FA76170A8DD7C61FA5E,
	LTDescr_setHasInitialized_mC54A9AAD341DE33BC04734061E6EFE42E89402F6,
	LTDescr_setId_mB492FBFB9D2FE6ED7CDD7EEA9F3A547A4BC596D0,
	LTDescr_setPassed_mC3016CADF8995A54ABF2D5D95908EE63D3298E65,
	LTDescr_setTime_m0DE4EDC9DA5F9D6B5255BCA73A198E18F0A320A4,
	LTDescr_setSpeed_mF8F0031D23893E09F0E258367FEA33FB175CC269,
	LTDescr_setRepeat_m7D66B000EF60DCC943D848C1BF53BD7C7BEF8313,
	LTDescr_setLoopType_mF2FC7F646DDB9AD46FE9FB3972C4B2EAA5634C77,
	LTDescr_setUseEstimatedTime_m1882A739CA3F9CE2F6215C61E92D4D5108F352EE,
	LTDescr_setIgnoreTimeScale_mDD2DF875144DFBA017697139ECD83164182DD6C9,
	LTDescr_setUseFrames_m356FC10CEEBD2F74F93F14EDB27B58A87C67901E,
	LTDescr_setUseManualTime_mAC4545BA99BA9C7117151D155C4664F6D7E933D2,
	LTDescr_setLoopCount_m658F924D41197D6DDDEAC18C37849D2341C1C993,
	LTDescr_setLoopOnce_mBF4016C6E09D73B6C4210585FEC20D250BBEBE60,
	LTDescr_setLoopClamp_m596650592727CC3F3C5513F54432D93CEA45666E,
	LTDescr_setLoopClamp_mEA5F7A4F29DFEC798D1DBF7CACC4EA95E5681733,
	LTDescr_setLoopPingPong_mB14D4AD3878CB744ADBB3F0B933E1C50CACEDC18,
	LTDescr_setLoopPingPong_mDBEA094A372AACBB02EFFA679DBF2C551928140F,
	LTDescr_setOnComplete_mBD0B6BAC2B05C7AE12E93F478BC6F0F41A33C44F,
	LTDescr_setOnComplete_m61B62A9BB1DE6DC69EB97F111139AB379A2AB33C,
	LTDescr_setOnComplete_mD8043AC13DAB80A85715B3B5D2E273754A93157D,
	LTDescr_setOnCompleteParam_m0DF2C3997947B6F72DFD4AA1BAEC841B8B84DD63,
	LTDescr_setOnUpdate_m55FBE2275AB200DCFB3793FA745A238491E7072B,
	LTDescr_setOnUpdateRatio_m52F3EB3754460E5DA2EC48CDD69FAEF7953CC1EB,
	LTDescr_setOnUpdateObject_m5B410690D035323DF02535DBFDD06EFE602E4F45,
	LTDescr_setOnUpdateVector2_m0FB697AF241FBDB5958D2FA52C5BF964FDC9F3A8,
	LTDescr_setOnUpdateVector3_mFFF939DCA282CC77307788DDE78DAC3FE8801920,
	LTDescr_setOnUpdateColor_m162951739730174C9B12F55091940A39981C1BF7,
	LTDescr_setOnUpdateColor_m3CE8B6B35EA308221FF7739A6DDFFE8B308DAAAB,
	LTDescr_setOnUpdate_mB7154EAF06F1366C24E3E8EABFDF53169A1EC1B5,
	LTDescr_setOnUpdate_m487CA9638173DAF55C9DF3B81F7D97232222D88B,
	LTDescr_setOnUpdate_m8A27C956F125846A8713234EDB95FCA045C3BF50,
	LTDescr_setOnUpdate_mF6CF9481ADCA4D3270B5E21634B8D924D1FD7DD2,
	LTDescr_setOnUpdate_m5404E20426F6927BD37BD1EA3B0BBBF1642CE37F,
	LTDescr_setOnUpdate_m5356685ADFD38D45881211740E990617C0F2A80D,
	LTDescr_setOnUpdateParam_mE61939C5A8C86D2603FD2F9ED73ACF3CF1FB1D69,
	LTDescr_setOrientToPath_mECE38AB1BC1313F39FD2FE3D582AB18A849826EE,
	LTDescr_setOrientToPath2d_m08D9F50871013C10D1961AFF0A7759A8D92B1294,
	LTDescr_setRect_m1E626946F828ADB3D7E7A70D4E43B1CA7B2009F3,
	LTDescr_setRect_m98165DCDF51B68BCB9CAEC7EAC5C364D2E26758A,
	LTDescr_setPath_m9BFB77CBAE51ED839078FD49C7611ED8323AC154,
	LTDescr_setPoint_mB0744AAA4A452543F68A3126C05CD8487BEF7F6E,
	LTDescr_setDestroyOnComplete_m697253D565C9D2CDA484A82E1005660595CEDA06,
	LTDescr_setAudio_m86759A48D1B7D26395A5B28AB5450E84AE46AECE,
	LTDescr_setOnCompleteOnRepeat_m28F4EE66B0CA50AC6FA5A9391EEAD0B62E85A3C1,
	LTDescr_setOnCompleteOnStart_m2FB448E21905E33DCE1F35EBBD8E39097AC35938,
	LTDescr_setRect_m0CDE3901FF03E6216DD7E6DEC6E65E7DEAF39AB6,
	LTDescr_setSprites_mC83C58D4DC03CDB0E8529A2DCDE37270089B0A41,
	LTDescr_setFrameRate_m0A569EDD28DDFD74BD71E0E5CF4730F178220AF9,
	LTDescr_setOnStart_m37383DCD917312D49E2291F3A47F8F9361155A1A,
	LTDescr_setDirection_m3B76149226B433591EC98766C8B14D63BDB7F193,
	LTDescr_setRecursive_mC2895C6EEB59EF79044495FA8BB6ACE914CAB402,
	LTDescr_U3CsetMoveXU3Eb__73_0_m39FBF744669510650F08E0F2B4E87CDEECA7440A,
	LTDescr_U3CsetMoveXU3Eb__73_1_mB4AD58106C3D205E5D4763F3F1CCA63B2F99EB78,
	LTDescr_U3CsetMoveYU3Eb__74_0_mA6309FBB2AF9AEEB859024E69AC8ABD6D102AD87,
	LTDescr_U3CsetMoveYU3Eb__74_1_m7A2A40165003571F84F057D3F39BD77A23B2F0E9,
	LTDescr_U3CsetMoveZU3Eb__75_0_mFE473C92996CA9EBA69397DA460F4B16EDE7D03C,
	LTDescr_U3CsetMoveZU3Eb__75_1_mF78C868A90B38D04ADAD2AD93D536B1791B3C671,
	LTDescr_U3CsetMoveLocalXU3Eb__76_0_m8A895E545E84F266EA0E0963C68274D9545D17BE,
	LTDescr_U3CsetMoveLocalXU3Eb__76_1_m17F56B25DDE0E4E6D6CC186BF9A9A617957C2FD6,
	LTDescr_U3CsetMoveLocalYU3Eb__77_0_m8D00D189D0ADCF6C778EFF39DCA1EA55C03E34F4,
	LTDescr_U3CsetMoveLocalYU3Eb__77_1_m67ADD5EAA303DB53DA4B896F2080F3E6975C76A6,
	LTDescr_U3CsetMoveLocalZU3Eb__78_0_mFD9EBD1D9EF08CB93FDCCE06D88DC940852C03DF,
	LTDescr_U3CsetMoveLocalZU3Eb__78_1_mF3D05D5EA84618939D8F8C0F9F06EE5D6294D77D,
	LTDescr_U3CsetMoveCurvedU3Eb__81_0_m8BD948AD31BE03BCEF9D75091AECC3DF8B4A50E3,
	LTDescr_U3CsetMoveCurvedLocalU3Eb__82_0_mE35CF92421F9DBB5687F1F1608AFBE9F86C80378,
	LTDescr_U3CsetMoveSplineU3Eb__83_0_mD9ACDB850FD978554B5465139A4E1EDD844CA1BB,
	LTDescr_U3CsetMoveSplineLocalU3Eb__84_0_m3219BB023C406080046DE37EBCF0CEDB8E323D1A,
	LTDescr_U3CsetScaleXU3Eb__85_0_mDF33F215E49CCD02AE02FDD79827006CB41DF18F,
	LTDescr_U3CsetScaleXU3Eb__85_1_m332A3ADAFDC200BF9AC11279593B5968DC537623,
	LTDescr_U3CsetScaleYU3Eb__86_0_m6A34E540DCDF955D835FCBD7FEF708257E07FE31,
	LTDescr_U3CsetScaleYU3Eb__86_1_mE1BCEE0DADE70D6CA3873140143141D9B51DBDB5,
	LTDescr_U3CsetScaleZU3Eb__87_0_mAEDB3F2903B250EC9885F64B147EDBDD4881E812,
	LTDescr_U3CsetScaleZU3Eb__87_1_mEFAE5A48C329D33CAF5F7003D48DD0B9C581AB85,
	LTDescr_U3CsetRotateXU3Eb__88_0_mD0976C92A94804F53718DEE26B131989A067EF7A,
	LTDescr_U3CsetRotateXU3Eb__88_1_m7C171EAF890568C630E3BE1EC70C97A0C55F0984,
	LTDescr_U3CsetRotateYU3Eb__89_0_m1B2E4D04C35A72D3957FEB294B306BF741228F1B,
	LTDescr_U3CsetRotateYU3Eb__89_1_m67AC1C0BCE4902B58FA999FC9475BF42D391F167,
	LTDescr_U3CsetRotateZU3Eb__90_0_m92CFF50F82E8606EA58D1FF72D2FBD7C9A0C7290,
	LTDescr_U3CsetRotateZU3Eb__90_1_mA55CC8D92F2601E04F21F45337134FBE437A20B6,
	LTDescr_U3CsetRotateAroundU3Eb__91_0_m90372B54B5D631FC5E1FF67FCE64F69A1E0E7E63,
	LTDescr_U3CsetRotateAroundU3Eb__91_1_m21A791E3630653D459153347755624070DC38965,
	LTDescr_U3CsetRotateAroundLocalU3Eb__92_0_m49A28FAA34B36D7AA09A322D63974997CB31BF43,
	LTDescr_U3CsetRotateAroundLocalU3Eb__92_1_mCB4C19C5A763BBF640E5BE6785D5AB03859AD606,
	LTDescr_U3CsetAlphaU3Eb__93_0_mADB8DE8A613762F896CC7754539BFFCA3A23B4E9,
	LTDescr_U3CsetAlphaU3Eb__93_2_m5318309F91340E1C143196D9ADA659C53B3C918C,
	LTDescr_U3CsetAlphaU3Eb__93_1_m44E2198D69A4F502CE6F5B1532AFD8F600D84717,
	LTDescr_U3CsetTextAlphaU3Eb__94_0_mE551CB2CBB6114CA72656A033CAC42EAB2EFCC8B,
	LTDescr_U3CsetTextAlphaU3Eb__94_1_m809E895E8E9ED4049E44ACAEEFE222EBA2E6A259,
	LTDescr_U3CsetAlphaVertexU3Eb__95_0_m53AB148A8D470C29DACB58274436B9535F5CAC37,
	LTDescr_U3CsetAlphaVertexU3Eb__95_1_m197F71CB84DF1265DDDEC92AB60CFAEC2B2E8BE9,
	LTDescr_U3CsetColorU3Eb__96_0_mF63A06E57C3B0A1B5B8FE3E7EF131BD862457B19,
	LTDescr_U3CsetColorU3Eb__96_1_m8EA4C07F32943F895BE0CB6909C63263827CEBF8,
	LTDescr_U3CsetCallbackColorU3Eb__97_0_mAD825635FB3FD1DE688BD692F394C91BD701659F,
	LTDescr_U3CsetCallbackColorU3Eb__97_1_m3635B1AEC7852BD361B57DAC85568D84BB050C38,
	LTDescr_U3CsetTextColorU3Eb__98_0_m1AC4C496FB90FA9C994A7D8772EE75A96D8C7499,
	LTDescr_U3CsetTextColorU3Eb__98_1_mF8C5941A01CC6AB44A3C1A9E4E0D42DE0019B1B5,
	LTDescr_U3CsetCanvasAlphaU3Eb__99_0_m03D22099CB93845FEE24F439C52C194CF424307B,
	LTDescr_U3CsetCanvasAlphaU3Eb__99_1_mC255CF9153C3E078BCC1E35C16E786C8662EBFD8,
	LTDescr_U3CsetCanvasGroupAlphaU3Eb__100_0_mCD1535F45FF98C078658D56A058E4D44D7CD47F2,
	LTDescr_U3CsetCanvasGroupAlphaU3Eb__100_1_m6EBBBBA42BCC9B435DB26932FCCD7DF904E3D31E,
	LTDescr_U3CsetCanvasColorU3Eb__101_0_mC7AB0F758514D92910F073FB38A41C395E6764E3,
	LTDescr_U3CsetCanvasColorU3Eb__101_1_m0DC35761F636C62A4B9A9D9B7C8EEFD9B30F3CA4,
	LTDescr_U3CsetCanvasMoveXU3Eb__102_0_m0E72A6C3F16FC694B6D54907490ECA5D6D359776,
	LTDescr_U3CsetCanvasMoveXU3Eb__102_1_m7FF531D0062E0803860A0CD456E6DD6AE30BD00D,
	LTDescr_U3CsetCanvasMoveYU3Eb__103_0_m4730D57FCDEA041D3F4111E45E34C15149C4BD4C,
	LTDescr_U3CsetCanvasMoveYU3Eb__103_1_m9CF415CA444CEBB9B5B6C01B337E1C4598B74A32,
	LTDescr_U3CsetCanvasMoveZU3Eb__104_0_mDCD6897BB64C355669F8EE9CFD8F2B386F830CA2,
	LTDescr_U3CsetCanvasMoveZU3Eb__104_1_m5B12F7E220DA96B6F05025A4BE1AFABDEEAFF853,
	LTDescr_U3CsetCanvasRotateAroundU3Eb__106_0_m7254F615E75865226968EA098B90FE904EE5B38B,
	LTDescr_U3CsetCanvasRotateAroundLocalU3Eb__107_0_m2C7BE76A27280304C47F1787199EA2790E10D6D6,
	LTDescr_U3CsetCanvasPlaySpriteU3Eb__108_0_m60117A734E7707143FD04D2FBEE6B75618B258DF,
	LTDescr_U3CsetCanvasPlaySpriteU3Eb__108_1_mAA1E46E79560810DA46AE8EA0BFA91EDAE3D2DA5,
	LTDescr_U3CsetCanvasMoveU3Eb__109_0_mABE1F4262412CA7E93AA4B71881FAE5B817FCF8D,
	LTDescr_U3CsetCanvasMoveU3Eb__109_1_mD190E0829649FEA47BAB95DED30B6EAB95C03ECF,
	LTDescr_U3CsetCanvasScaleU3Eb__110_0_mF68FF782BFAD2E93A65661953E683EADC9A52DA0,
	LTDescr_U3CsetCanvasScaleU3Eb__110_1_mCF2CCB10E46FC5F00061B020C857E7A35EF14E0D,
	LTDescr_U3CsetCanvasSizeDeltaU3Eb__111_0_m021163E83008DE7874346B03AE065037CDC34D3A,
	LTDescr_U3CsetCanvasSizeDeltaU3Eb__111_1_m9AA7511D4E1BF277C282F3E30388E82D0A9E1C0B,
	LTDescr_U3CsetMoveU3Eb__115_0_m5DDD8D43FEBB5099535893334FD76E1F0DAFD21B,
	LTDescr_U3CsetMoveU3Eb__115_1_m6B66D8C95AE011E8C641FA4AEF0245809F334399,
	LTDescr_U3CsetMoveLocalU3Eb__116_0_m7FE28EDDA5832CD6FF6BB2370DA6059DF23B2A1F,
	LTDescr_U3CsetMoveLocalU3Eb__116_1_m8D5A11FA82E103327A9ABD0CA5BB397952CD260C,
	LTDescr_U3CsetMoveToTransformU3Eb__117_0_mCE51DD0DF5028FDE555344C8FF602CD98716C33B,
	LTDescr_U3CsetMoveToTransformU3Eb__117_1_mACD7BEE7BA65E5DCB5B234EB3A31D9D8C3ED7FA8,
	LTDescr_U3CsetRotateU3Eb__118_0_m3ECAD3ACBA191CD5E6FBB67BB3E3AAEDA454B4D3,
	LTDescr_U3CsetRotateU3Eb__118_1_mEB2249D09D0E69826AE6036FD448AF92D77B47D7,
	LTDescr_U3CsetRotateLocalU3Eb__119_0_m2AA0D5D9B5334CEC12D6E8670332C4BEEB5CD7DB,
	LTDescr_U3CsetRotateLocalU3Eb__119_1_m64E3FC9E52EE3EB30AA9260408C3D02F24A34064,
	LTDescr_U3CsetScaleU3Eb__120_0_m4528B485CAF7246E73A7E602504675D4B11A44B7,
	LTDescr_U3CsetScaleU3Eb__120_1_m4A1D3D78C772991EC422F7D2D29EF4D069E39C78,
	LTDescr_U3CsetGUIMoveU3Eb__121_0_m4AD272AF144CD468B048F733B93D1DFC08527804,
	LTDescr_U3CsetGUIMoveU3Eb__121_1_mDBDABB8CB0F1D7CC05BF861826BD23B1A5CF68B3,
	LTDescr_U3CsetGUIMoveMarginU3Eb__122_0_m92AE989BFBF68D262DDFD12EB9DBBE05BDCE7AB1,
	LTDescr_U3CsetGUIMoveMarginU3Eb__122_1_mA60DFD7F27C11D0B55761F7A0936D8056ED09382,
	LTDescr_U3CsetGUIScaleU3Eb__123_0_m5A0FC254BDC374AACB5FED5922852DD294CBA77C,
	LTDescr_U3CsetGUIScaleU3Eb__123_1_m01CD6A1BE962B34E940B420723AE967C9AF4B248,
	LTDescr_U3CsetGUIAlphaU3Eb__124_0_m584C608C8B326C3B0401CAA8C696F8ABCF95DD38,
	LTDescr_U3CsetGUIAlphaU3Eb__124_1_mF0CAD09EF53473D82DF70172C27DA602272E3361,
	LTDescr_U3CsetGUIRotateU3Eb__125_0_m8F9A74633240B6083A3C1DB7EA24F762D40589C9,
	LTDescr_U3CsetGUIRotateU3Eb__125_1_m43CF134D7443CAA354F69A7031CE9F2574CE4DA5,
	LTDescr_U3CsetDelayedSoundU3Eb__126_0_mEF9FE367DCCC3DDE76DA47246D17F221C0F4D93F,
	EaseTypeDelegate__ctor_mCD18183DC7DD02010D779AB3969F9D2B6BDAEA28,
	EaseTypeDelegate_Invoke_mC562BC4CD7B5786B26A9D7F780BC43F1B2F601AB,
	EaseTypeDelegate_BeginInvoke_m8B1407D35ABAD4BA8A49AA4CE44736F90B5D834F,
	EaseTypeDelegate_EndInvoke_m275A58EADF116EBDD21996509E57703CCC10C75A,
	ActionMethodDelegate__ctor_m86D0105115F69AEC95F9DB9542A3CB52854649E6,
	ActionMethodDelegate_Invoke_m4C86E95A515A25BE5ADB85D4889AAEDAA157173A,
	ActionMethodDelegate_BeginInvoke_m1B3C88E85A1CF2B1CE645AC9594565046D26BE0B,
	ActionMethodDelegate_EndInvoke_m7B0A147CA95FB6A16463DFA4C30DE37053C5D72F,
	U3CU3Ec__cctor_mACE0E27AEF258161C83D9EA95E78DBD6B559A5EB,
	U3CU3Ec__ctor_mB934C02FF6DB889EA42A5581DCA0B5CEDBCC5903,
	U3CU3Ec_U3CsetCallbackU3Eb__113_0_m7FC51BAA4B25A66AAAE19427E427E31C01E7EF0D,
	U3CU3Ec_U3CsetValue3U3Eb__114_0_mF93F5C6F0B095D8B1EE239BE2A475A16E713A012,
	LTDescrOptional_get_toTrans_mB4912C34FDE4BB3CE94B86FDE3D7D8C0DE951773,
	LTDescrOptional_set_toTrans_m4C747BFF6573BF8C2484AF01C3FCCC65E8A54FB9,
	LTDescrOptional_get_point_mB79A8D97B381FE3122B1549DBB3527F9691A1156,
	LTDescrOptional_set_point_mD7AE42D3184ED0D66545DC3A98B5CB5759DF8627,
	LTDescrOptional_get_axis_m546D8ADD4003D9B69F7EC4A243008064AC42DB20,
	LTDescrOptional_set_axis_m17A070B698AFE6D066C10EEF1A86DDEABC0A13B0,
	LTDescrOptional_get_lastVal_mD84B4C7F0D87E853D6EB9786261068DA74031DF8,
	LTDescrOptional_set_lastVal_m1B2DC6C4CE540BA60BD467F4394CEF18409B820C,
	LTDescrOptional_get_origRotation_m1B98DB0AACC7AF940C43DC29DF7A61FE6E49736E,
	LTDescrOptional_set_origRotation_m6E9E885A6259D7DE7FD1D398CABD7F7CB1751DE4,
	LTDescrOptional_get_path_m20A38F266BEEDD6370F8F54B2FA58C7262808AC5,
	LTDescrOptional_set_path_m10D4A506CACD00E0951E6E5E41003DE99C72DA64,
	LTDescrOptional_get_spline_m32BE3A98427C2A4FBE40E1BAEDEC2246ABD3D4F2,
	LTDescrOptional_set_spline_m5FF9921610A1F8C38E004CF9129F185BC7FDD708,
	LTDescrOptional_get_ltRect_mDBE34D781428B8DD07A8F25AC32B9B0932B62BE7,
	LTDescrOptional_set_ltRect_m273BF61CF793266842167003657D7083E62B655A,
	LTDescrOptional_get_onUpdateFloat_m8573CE5DFEFBA6E350AF056485B265FF59E8072B,
	LTDescrOptional_set_onUpdateFloat_mC6B9DD9721D66C62B4F196BFB0AC6EAFFD98A10F,
	LTDescrOptional_get_onUpdateFloatRatio_m6757E471D3ADDB9202062289E6EFAD3B61B2C7F4,
	LTDescrOptional_set_onUpdateFloatRatio_m0FBA91E49D4D592E2CAA88BF2963B2B5737ACEF7,
	LTDescrOptional_get_onUpdateFloatObject_m8A282C0573D32731C92CA8A4E352382CE73E4839,
	LTDescrOptional_set_onUpdateFloatObject_mF14622BEBB5CCF990BC0DC9622CE2EE037F364C5,
	LTDescrOptional_get_onUpdateVector2_m2692F9A7698099312B1956EF89ADC677E156325A,
	LTDescrOptional_set_onUpdateVector2_mC9D4DD0709714E0D052A2A6E475099B146375331,
	LTDescrOptional_get_onUpdateVector3_m1A96786092458EFAEAB22467DC6AA01F2F34247A,
	LTDescrOptional_set_onUpdateVector3_m91295D4F19AFF3080F6B44A071118CCDB52F288B,
	LTDescrOptional_get_onUpdateVector3Object_m2CAC540C86A54F7744AE39443FB4054066E3DE02,
	LTDescrOptional_set_onUpdateVector3Object_m2ECE95CB5D7DFE5E9DEDB9F3B03BEF39D4000B77,
	LTDescrOptional_get_onUpdateColor_m78FBDE4D9B8856EBE7EBF42709CFF81E127B9A43,
	LTDescrOptional_set_onUpdateColor_m7D77082D84B3831810A825A0628CE556012AAA2B,
	LTDescrOptional_get_onUpdateColorObject_m45EF15CE7E1F771A04B6E9374323759A7A021C62,
	LTDescrOptional_set_onUpdateColorObject_m25F9B89775D1630387BEDF4DC8B2F88F8E4842A0,
	LTDescrOptional_get_onComplete_m89C63A691247B03AF894749D3F32987E7994CFA8,
	LTDescrOptional_set_onComplete_m71A16E08537D67C75C706DD0EB88B7954A0CFCAF,
	LTDescrOptional_get_onCompleteObject_m50BB3E137E79340957CEE096A24A3635ACD91DEA,
	LTDescrOptional_set_onCompleteObject_m70FA4651F7E2CA76267B09C3EFBEA8AFE62F95E3,
	LTDescrOptional_get_onCompleteParam_mD546B9E0E31E70CEF0514DFB486D49E4BBE9D120,
	LTDescrOptional_set_onCompleteParam_mE4F5C590CFA4FF8204FE0C4722BF7EEBC1F4D7C7,
	LTDescrOptional_get_onUpdateParam_m8568043E9421D14172B62712FF0519738F0E8D88,
	LTDescrOptional_set_onUpdateParam_m415D9E73546338141E97EBEB8587C8A6F00C18E6,
	LTDescrOptional_get_onStart_m67196E7D6B56A68A3D7A5BAC7201CB5D79575EEF,
	LTDescrOptional_set_onStart_mFC780FBC4B2363070A9395BAA7693ACBFA3C75F8,
	LTDescrOptional_reset_mBC39AAD50AF833763A2547D2BB295CCE04F8CDBB,
	LTDescrOptional_callOnUpdate_mCD2B766E5358969C288B360A409B741962E90A9E,
	LTDescrOptional__ctor_m31327367F73AF26F1F0513529F56B4D4527F1CD7,
	LTSeq_get_id_m7BB843F84403ED4D5D7816EAB92F64D64AA16528,
	LTSeq_reset_m81F08444C11BCB0DA11DA2655C967A31B0F48E43,
	LTSeq_init_m7666AE5720766D32C80F45C08B15F29BDF926FCA,
	LTSeq_addOn_m92322FBE7CE53DC475ECF67BD47ED0A0FB37471A,
	LTSeq_addPreviousDelays_mA44FF52A17DF8B8F70385938E809B922AE96D914,
	LTSeq_append_m9CCA2E2A2FEC8BB783E6973C5259009DE1F2CA93,
	LTSeq_append_m918D5B42DF7E78F1D632BDFB559566153BC03151,
	LTSeq_append_m771276B4D19FF6E029D920552CA35785C06166DC,
	LTSeq_append_m7B1CF4D2CEE5FEC4FD64BE97123E3466981969C9,
	LTSeq_append_m38B101D631E233A3EA115D8102FECE3A9F8501D0,
	LTSeq_append_m16B953CD94139BC1C75791A5D461C744EA4ECA64,
	LTSeq_insert_m557E13DE79F5FDF0DCC9746AA15307FED3DA9F0A,
	LTSeq_setScale_m01A80A5EF1549289CCFE8F7E7176A5B669781699,
	LTSeq_setScaleRecursive_mA815906D527D73895B508205E6DF519C26560C7F,
	LTSeq_reverse_m626178075D66F45F66D451A95BCFFDBCAD95E04C,
	LTSeq__ctor_m0C853ED35E39038B9153A5E212A11D5366E3EB01,
	CSAlert_get_active_mC5B8764A86575F47018AC1E0D19FB794D7C7C63B,
	CSAlert_set_active_mF170DD19A7A2A402D2CE725381844C53CF9D25BF,
	CSAlert_Awake_m3D0BE042AD4D8C019D4E3BED6AB0D0AED741B9FC,
	CSAlert_Appear_m6EBB93449DD91DD2194BBD91E10D43DEEC49B7B0,
	CSAlert_ScaleActionCompleted_mA52B4130600CBA1FF43D872EDB1A8B1018A0B8BF,
	CSAlert_Disappear_mC54683355BB67451B479B2A12C3284382686FCAE,
	CSAlert_Init_m73B7799D7D8A49E18A01BD3C31E8816B945D4BEF,
	CSAlert_HideBoards_m1F7620E49D0A33A6DB31C82800127561F256F53E,
	CSAlert_ScaleAction_mF1B81F23A4F3A13225B2C13032E9D41BF9BE93E9,
	CSAlert_MoveAction_mFDB0B6DC6074DA7B3F94F7B7C729F1878396D01B,
	CSAlert_AddPaticle_m3E0BE7CF647C2682EC45D2C4A187721DA13A3FC4,
	CSAlert_DestroyPaticle_m20D0B2E007EB8FC26E3500EB7D57FA8CDE2B0BAD,
	CSAlert_CanvasStatus_m80BD4FE178D2A374142BEA746DB7FDC86BEE8EBA,
	CSAlert_OnCollect_mF9713B011E292BBD094EE068952B10BA27B9CF1A,
	CSAlert_OnShare_m2400699168BAA44090B6CD7F76620476D6792F5C,
	CSAlert__ctor_mF41167DB7EF732F9D3F75A1ABE6D3D12FE3476E6,
	CSAlert_U3CScaleActionU3Eb__24_0_mA4C2DF12B01ACF01082ACC7E4FC8616245A366CF,
	U3CU3Ec__DisplayClass19_0__ctor_m84FF5523AD0F6EAC7675B6BBA7BA76B54C41E90A,
	U3CU3Ec__DisplayClass19_0_U3CAppearU3Eb__0_m9196E668F8639F11C3BA037FB4ABB9C30B46BEFF,
	U3CU3Ec__DisplayClass21_0__ctor_m503CBBCF9D0E9A9E8884917C2886FF0D95FE5902,
	U3CU3Ec__DisplayClass21_0_U3CDisappearU3Eb__0_mDB7FC6B50A194FA78E79B6DB3D9A430F2D01D18D,
	CSAlertRewardAnim_ScaleActionCompleted_mAFE3E629690CC832C3E437824699AB1DEB31EA7D,
	CSAlertRewardAnim_AppearWithReward_m7724C1C8FD8C4C5DEA03CADF699E112FD477B005,
	CSAlertRewardAnim_Appear_mD5FDEB03F2BE6C0E88C8CBF4CADE4A9001F4CCD6,
	CSAlertRewardAnim_AddCoins_mDCAE20031B643FFF787CDE4552B1800CEC1B0967,
	CSAlertRewardAnim_AnimateRewardCoins_m5F716A2D71A4011289F38CEA44E34F67A172B682,
	CSAlertRewardAnim_OnCollect_mDCBCEFAFB0718FAA987CA28A3C48F8B3594082C6,
	CSAlertRewardAnim_OnDouble_m9766562DC0F2C3992912071D3C5081EC633A0830,
	CSAlertRewardAnim_EnableShareButton_mF5E0A82A129C63B0B8DEAD829F247184B1ACE663,
	CSAlertRewardAnim__ctor_mD5B36902E2CAD2D68FDAD6C5600F5F4E5BBAD5B8,
	CSAlertRewardAnim_U3COnDoubleU3Eb__9_0_mD85A3A3EE277E75F3E423E7AFF1666D3A782BE21,
	CSLevelUpAlert_Awake_mE2D7C42AF0F5BB54FF63250619A46E6098134C37,
	CSLevelUpAlert_ScaleActionCompleted_m86690BFE0504B42336B82495C39A3F209F053CC0,
	CSLevelUpAlert_Reward_m27380A04198AC929F7A9218355194B301A91C246,
	CSLevelUpAlert_Appear_mDE4AAAE772DCF1558287D404E6DF1634017B5208,
	CSLevelUpAlert_MoveAction_m46C694E0583E7F7CD9AE5CA11309EA08632C7FA1,
	CSLevelUpAlert_StarAction_mC27C975150C66A6503F3B0289CB861121A05A654,
	CSLevelUpAlert_DefineStarInit_m43B8F9B5941286AEA0702FD06F4008F6BB2976ED,
	CSLevelUpAlert_SetStar_mCB25A66A961A0E69F96F1DD101AFCF4C02D8FAEE,
	CSLevelUpAlert__ctor_mE8E08597FD9B88042F3CDDEFA0746F8C8FCA5A0E,
	StarData__ctor_m42ABA685AF38D2705BD9AE61B36F9E1E6CFA3A85,
	CSCSBlock_get_animate_mE11CB4AF5AFB8E8C182B9FB9B55C409EBCEFFE81,
	CSCSBlock_set_animate_mED7B5B9EE6615D91D3D93DAB8A7388AEB18A0832,
	CSCSBlock_Awake_m18C45DEFD106F4724DBC6211BF7AC3CFA30A317E,
	CSCSBlock_Start_m0F5EC8A80DC1A22E5AE63410A72A2A3457FDB0EA,
	CSCSBlock_OnDestroy_mAC77DA074AD711FC9E13DD08747DACFFADFDC034,
	CSCSBlock__ctor_mA00C3B606F7E924FDF91C4062B8D60713733654A,
	CSCSBottomPanel_get_showGamble_m1DDE5B2D3A58976B4AA0D0F7DBA3FCAD1200B803,
	CSCSBottomPanel_set_showGamble_mB00DDB493564B384DDADBAC13A17717877612941,
	CSCSBottomPanel_Loaded_m00637C935356B7E018F0069A593D9CF2CC21935F,
	CSCSBottomPanel_OnChangeMultipiler_m4A05F28CBE0D88DF41BC12B5BB3E68B75C7D33FA,
	CSCSBottomPanel_OnBetMax_mC0C2D1D52559DC00FE548D7DDDA9809C8D09080E,
	CSCSBottomPanel_SetShowGamble_m022745B1403AFEF8C2CAB5E6E7948DA8F51049D1,
	CSCSBottomPanel_Move_m706287C6EA69746DFD409ECE6289D41B39C17E54,
	CSCSBottomPanel_OnGamble_m0790A4F8A3E9884017240F9732971A3B9064CBFA,
	CSCSBottomPanel_WinAmount_m0E0F72E242FDD0E5F33BB7A5521EBF7FC3345595,
	CSCSBottomPanel__ctor_m522B4E43966C2CD6F184AE9AA854BA5F53D2D039,
	CSCSBottomPanel_U3COnGambleU3Eb__13_0_m32361AC3C885FD8D212D6FD9DAE0B945BA080693,
	CSCSFreeGameInterface_Awake_mDD61C2161D12D02540034635D2934C65570BF09B,
	CSCSFreeGameInterface_OnEnable_m8AE23571E411DC7526A6EEAC59E002B5D05F1372,
	CSCSFreeGameInterface_OnDisable_m1182BE8C024A77678207654A1A6C8E9DC9A26263,
	CSCSFreeGameInterface_FreeGameValueChanged_mE5C3A102A1A3C03023D3A49863EB97CC3A9DEC50,
	CSCSFreeGameInterface__ctor_mD02813A50D44391965280EE077804445E4EFAA62,
	CSCSFreeGamePanel_get_totalBet_m376135DAF9232F195BFA74892F9D01B0ADD6B397,
	CSCSFreeGamePanel_set_totalBet_mBF4E4F5D1DA2BB72D26764CDC9A2DF1E726A1442,
	CSCSFreeGamePanel_get_muliplierEnable_mE520FA5A871AB4B16D9E499E6AEB955D3E16517D,
	CSCSFreeGamePanel_set_muliplierEnable_mFE826957CC779B2CBFDE41C75354CB44096FF1FA,
	CSCSFreeGamePanel_get_multiplier_mD526D4CEDD4A7A6B68E1454F27340FDC84ECAEAE,
	CSCSFreeGamePanel_set_multiplier_m50711E46A410F546FDFD3CDC6CE041D44C932934,
	CSCSFreeGamePanel_SetWin_m1B2F94F62720B4E8F3CC8F1EDEA674B4F8B2B95D,
	CSCSFreeGamePanel__ctor_m4AA51D30054BC503E52483A7A9F0B17234481A35,
	CSCSGamble__ctor_m1622FC3EB223643EC52E482C22854F1406F63E12,
	CSCSLoading_Start_mFC1947A449DFA2FC71B6C12CF5CF65928B8AA704,
	CSCSLoading_OnDestroy_m991735735857AFF39A905E9933D6D7B940DE4F21,
	CSCSLoading_Load_mE103BFDAD5876834A2F05B6C47D087B7D693FAF5,
	CSCSLoading_Load_m50CFFB7D3A49EF65D3B9A1FE9BB42C8203CEEA34,
	CSCSLoading__ctor_m7667374BDA6DF683F0B4AB8355E0000E7341B69E,
	CSCSLoading_U3CLoadU3Eb__4_0_m617FB510FCE2A1DDBC6A0476B096A28EF57FC512,
	U3CU3Ec__cctor_mB0F5C4244AD9E79B9C2CC693780AC3BE1F53DB34,
	U3CU3Ec__ctor_m18443F007E70E5A805A177B368210B7856AFA8D2,
	U3CU3Ec_U3CLoadU3Eb__4_1_m725BCD9908A2B3B00A528C7284C47B840ECF7B28,
	CSCSBonusGameSettings__ctor_m058A3519669A5F5179EC9C3C2148D50331189E9B,
	CSCSBonusGameSettings_get_Zero_m41F85776A908212EC522EBE58304DC06D909D9AB,
	CSCSReels_UpdateFreeGamePanel_m5907BD3C5FE11812A78409CF33C3F52D23095933,
	CSCSReels_BonusGame_m45BC2CDFA4983BD80BF7B479AB0F4E34C3DDD2BA,
	CSCSReels_SettingsForScatter_m8465A51ACB4B083FA4AC7C20B77C61856E934FD8,
	CSCSReels__ctor_mB1B974FF937077687D7414D5F24FB1312398E609,
	CSCSSymbol__ctor_m23DC1FACAB532B78CE260CB2038E02D389CC2248,
	CSCSTopPanel_OnInfo_m1D621BDC03E96E9673C007A9A11E3850597BC712,
	CSCSTopPanel__ctor_m6E51EFB9F3A136091C33B48C84DA724DE8D6805B,
	CSCSWinAlert_Appear_m5DDCE0CF9D75F7F3111F30028C7448B8990AC0CE,
	CSCSWinAlert_OnCollect_mE546A680981C1221C1FA2E5E347BF9BDBBAEAC54,
	CSCSWinAlert_Reset_m720D44828311C2F7A6425D2A500FCD224C6EDBB5,
	CSCSWinAlert__ctor_m4C28BD9731A575EC89243A1627792AE5F421B9A8,
	CSGameStore_get_active_m25A2CCF8D4668E15F295D189CDBB52609302B87B,
	CSGameStore_set_active_mD2613BE891FA8517E515A81CDD3DAA54A15E5238,
	CSGameStore_get_coins_m05421ADD315755571EB7ACE98A95463B441A97D4,
	CSGameStore_set_coins_m22BA8CF0B64C3EC78B61CD4D5F8846156AAB62BF,
	CSGameStore_Awake_m5CAF5E0FD48FCA0D38C194C12FC254BFD910F263,
	CSGameStore_OnEnable_mA4D4B8B9AC5E0E0C07A01CDFBB8040E6A6EE4B93,
	CSGameStore_OnDisable_mBFA8E2C97F8246A3A1788E890D189F175A739325,
	CSGameStore_Appear_m8AB09B9DF7F942304AA98AB85E6AE45A1370D0CC,
	CSGameStore_Disappear_m663BE11F6701792A338C731D41006FECB2E8BF86,
	CSGameStore_Scale_m6BA966E2852A086CC5178B15EFE7F54E6ECC413F,
	CSGameStore_AlphaBackground_mFDC97250FB5D0777FA388669320A9850FA1240CB,
	CSGameStore_AlphaBoard_m3B7C7556D29657F54DD43ECB479AF66492086E47,
	CSGameStore_Alpha_m6C93D0D91C196ED304A583472F9936A106891D01,
	CSGameStore_CanvasStatus_m80FC9192303E65844319AF70B4CAEE4A4DC0CEE3,
	CSGameStore_OnClose_m9F28895C1CB6CF147BEC46776D48BB02EAFA07BD,
	CSGameStore_OnPack_m44105379E72C41AD896CB641312625DEC1B44F4B,
	CSGameStore_CoinPanelValueChanged_m50B9C35A2020D6EF628CF7EE04CF04978987B592,
	CSGameStore__ctor_m2663CB2ABDC19E1AFEE9C6BE5A21084D6483970E,
	CSGameStore_U3COnCloseU3Eb__27_0_m18128A21422ED9D26C0A86BA927FF7E35F3C66AF,
	U3CU3Ec__DisplayClass25_0__ctor_mB5F439256C70796DF24FBF1ABAAE1BCE25941CBA,
	U3CU3Ec__DisplayClass25_0_U3CAlphaU3Eb__0_m5A03C7FD017BF27255D5AD70E18D555B78301799,
	CSLFBGAlertBoard_get_multiplier_m193A730CFBF905D5CCF153FA4DFD5F6D0249BFE5,
	CSLFBGAlertBoard_set_multiplier_mE386FBEDD873CA316BE0E3705632AF342CFCCE10,
	CSLFBGAlertBoard_get_freeSpins_mB28202E401B8226FE1680CC2F95044DA96C3D23E,
	CSLFBGAlertBoard_set_freeSpins_m43A33D80D5A2C8DDBE069A1F261CEEA8711267FA,
	CSLFBGAlertBoard__ctor_m2F63EA1D6B26EAD48B0B3E61F6C5AB94AC3B83DC,
	CSLFProgressBar_get_value_m30FC80B76971F1BAA70887F105E813FC81B486BC,
	CSLFProgressBar_set_value_m80BA16BF22727FB190B2544763CD3709F3654277,
	CSLFProgressBar_SetValue_m35BF9490D754C8A3ED962265C4223263472BF354,
	CSLFProgressBar__ctor_mC6236F1AEFB32492E389DB39252D6A5329196FCA,
	CSStoreCoinPack_get_productId_m989AFADCF66D2158BBC653FF1B0AEC54B2CB5B95,
	CSStoreCoinPack_Start_m3DC68D8FDBF30F23C8F8CC635666A209E711B2BC,
	CSStoreCoinPack_Total_m1B253B034C359095DB46C575D844B3606C4CD59B,
	CSStoreCoinPack_SetPrice_m0C6C5EB9265655BF8CF6C0267F494DC590D6C1B5,
	CSStoreCoinPack__ctor_mB868845EAE6CFFB50D3AE92679627E8944235888,
	CSBankCoinPanel_get_bank_m24152567A2ECB0A1691EE87BBBADFB1C012BE927,
	CSBankCoinPanel_set_bank_m41E22A92A9EE3312657C7E28C9E5DF5BAB10E31D,
	CSBankCoinPanel_OnDestroy_mF9428626D71EE3F4A75E589DF3943DB8B11A248C,
	CSBankCoinPanel_Awake_m270A65148C4664AECAE4235670A37A5BBFC187CB,
	CSBankCoinPanel_OnEnable_m3FC2F0867CAA19DBEB50A7AE0F70A2732ABB7E54,
	CSBankCoinPanel_OnDisable_m1E41C177AD6E4A2FE8F68061845B3C1DDE20B88A,
	CSBankCoinPanel_Format_mEE83D02FF2E63F6BDCE0B766E8CB011C55C3FCA2,
	CSBankCoinPanel_Add_mE45C4DB12F4A5069D2E48ED0ADAF075ACD0279EE,
	CSBankCoinPanel_Add_mC3890EFFB70AB1CE7BC7341E93CF86663C2C5EBE,
	CSBankCoinPanel_AddWithAnimation_m8FD74B72F1C55AF73BCE2A3DDAA1F772E36A8AB2,
	CSBankCoinPanel_AddParticle_m7142EEA0E90D1522959D54A260313B347517F56A,
	CSBankCoinPanel_LabelAction_m532287B54B3AE5C8A35A89DD3F54ABB674CE9523,
	CSBankCoinPanel_ValueChanged_m20B0F3448065713991C98237D3563E24145B25A2,
	CSBankCoinPanel_HandleSuccessPurchase_m07A9C46703B740C37EC331E08C2B5C8A787FF415,
	CSBankCoinPanel__ctor_m052C20441B1B81048ACD82549A5472BBB63A001F,
	U3CU3Ec__DisplayClass18_0__ctor_mA2F4BDCBF8F56ABB7627CA5B38298F78A326363E,
	U3CU3Ec__DisplayClass18_0_U3CLabelActionU3Eb__0_m11FED61E17A2F93FAAF0BD802238843E936B50DB,
	CSBottomPanel_get_totalBet_m093E370E8262F272C9A1B2F6553F82B579071D65,
	CSBottomPanel_set_totalBet_mD4167C838C6D539B6186A33B2F7087644A37C5F6,
	CSBottomPanel_get_win_m5DD4483D02E2B1E09A570CCC7BA3B4F3A2C7FBC8,
	CSBottomPanel_set_win_m80D0D6427B05D7BD14FB6400F21C9AFEDED5683C,
	CSBottomPanel_get_enableSpin_m8B4ECFE9105EA904DB3608F717AE17F2021A59B8,
	CSBottomPanel_set_enableSpin_mC42DC57E958B425D2578EA23C0386D9A18E62E03,
	CSBottomPanel_get_CanSpin_m8F1F80239EE36062E0E2164D4764CD299B96ABC5,
	CSBottomPanel_Awake_mB36710219902D7239E0D5D0282F4B750FF1122CC,
	CSBottomPanel_Start_m28B168C349C6F0C4400ADBDDE8B05FD55CF09BF7,
	CSBottomPanel_Loaded_mF5255318B41F972F8FCF59915572CE21CF853FAB,
	CSBottomPanel_OnChangeMultipiler_m6C1CE69BD9DD498280B52CAB9C51E5B7028EB16C,
	CSBottomPanel_OnBetMax_mD68B57DBB8AD08ED5CBF0A346AD870690220817F,
	CSBottomPanel_UpdateBet_mA8AEFA77EC682F4DB7ED346114721C2996DA0A11,
	CSBottomPanel_WinAmount_mF55918594F71D8A98185BCB33AF89E2DAF4B56EA,
	CSBottomPanel_AddXP_m74BD09613BF3FDE16B670C3CACA29C936A097BDF,
	CSBottomPanel_SetEnable_mA161F25F8F7C5A7729573BC0E9F478E4B67D1BAD,
	CSBottomPanel_BetLabelAnimate_mDE5EC216CAD5ACB4519773330BFE434C5C142C72,
	CSBottomPanel__ctor_m2898CECCECC096FFC830070E52847AA949B87D4B,
	CSBottomPanel_U3CBetLabelAnimateU3Eb__33_0_m940DA0547B5573DBEA603D11CC18C3545799AE01,
	CSCardValue_get_rankIndex_m174802D6FCD42BE5D4EF8CB18F1526F2F050F1AF,
	CSCardValue_get_suitIndex_m781E7F614976BADE99E6284DABCA108E380ACFB4,
	CSCardValue_get_random_m7947AAC2AC29752532FB01C351BF50BF9119F1E3,
	CSCardValue__ctor_m8E30CBB7352115D6A1CD8DF7E4C2DB28AFB5F381,
	CSCardValue_op_Equality_mBF0F4DAEC09EB6F5F173C03FE896B3C5F5D86FD8,
	CSCardValue_op_Inequality_m718147A0B83EBF4D2A5BE2FF44580FD43CE02EDF,
	CSCardValue_Equals_m76AD0CA57CDB069290DE8B8D56217FCBB3F9DF06,
	CSCardValue_GetHashCode_m38131925AA54B1CD1AF9ECE3514E68C48576D5C6,
	CSCardValue_ToString_m6E389700BF8EE2F6B660F1216EDE3DD029719E7C,
	CSCard_add_selectedEvent_mE3210C6427006E5C8822E2B58F579B4372E9094A,
	CSCard_remove_selectedEvent_mA3D0A8F0DE543EE4722DF3DC06908D4D9A26E558,
	CSCard_get_data_mB1D794CECF31FFA7C826B7D5D7B025E38B9DF566,
	CSCard_get_flip_m19E1496D8E459E5C33C999EB0A9FD4386A4B328C,
	CSCard_set_flip_m402C531B62EE0248B0F6AAED7448C21C160425E2,
	CSCard_get_interactible_m5F23DAD84861A366292E55D4C02473F465B990B1,
	CSCard_set_interactible_m58EA3366729B183CCCA4D60058BFE1196EE73E17,
	CSCard_get_cardValue_m0BC50B6CCB4D7D37F6CFCD28A1A5312F9B95C1D0,
	CSCard_set_cardValue_m479074ACAB45EA9ED6B680D1E32017C73CFEACE7,
	CSCard_Awake_mA4F152E79E56949FD70B6EB566618351F297176B,
	CSCard_OnDestroy_m7D45513512F201325B4ACB3464E95E9A16C055C7,
	CSCard_LoadWitValue_mF99DDFA4EBD78295F6F4AE944B7264FBB7397169,
	CSCard_UnsubscribeEvent_mBA16E1D9E5F0F972EDCCC9A3BB894C8DE06BCA7C,
	CSCard_SetFlip_m2741DD3D1ACDBE9EC0869C2FB086D937E59511F4,
	CSCard_Flip_mCB639228B0E3A626F50F3AFD91E982FEA6DF28F6,
	CSCard_DataForValue_mBFAB94DF5014FB59293DDAEC56EC0AC327A16D93,
	CSCard_OnClick_m29DBE87D132A8B6A6FBC3AD3DFFE7F9749C61F70,
	CSCard__ctor_mCD214CC7FCF5C0D34116A943C28939FCCCC1A9CE,
	CSCardData__ctor_mED7773B7F26A003EB9ECB8BFCE63F6E85B8E991C,
	CSExperiencePanel_get_xp_mD425CE6C47F4836AD046F9A315A92BBD4CB9576B,
	CSExperiencePanel_set_xp_m073074467BE1F4B5995938FA26AD2A54A8BDF41B,
	CSExperiencePanel_get_level_m913EF158F992EBA1F3FC13A88FBE8DA1596DD434,
	CSExperiencePanel_set_level_mBC773657A11690624153B556E75AF589C61433BA,
	CSExperiencePanel_Awake_m8F3BE4F23B57EF5632E2526524BDFBDC0393025D,
	CSExperiencePanel_AddValue_m7EC3DC5BF89F8D399FC8AC1511B425A3B03904FD,
	CSExperiencePanel_AnimateProgressWith_m2DA1693DB1D5914FC67F0716E3327CBCDDF5477F,
	CSExperiencePanel_TrueValues_m4C4863830B9056DA9D9CAF1D291A1F8EFBAA64FE,
	CSExperiencePanel_UpdateBar_m5FD4059533E51A4321753545AC7133874CCF8F60,
	CSExperiencePanel_MaxForLevel_m2F9A5EA43F6B356460712925E18B0E1C8CBAA00E,
	CSExperiencePanel_AddParticle_mF5786BBC0DBAFD349425B901EA14B20C9A81DCD8,
	CSExperiencePanel_Alert_mC883BA4BDF127E5754BA509443CE210C72536173,
	CSExperiencePanel__ctor_mCD9DC37A8312F2F8334F3D4B06F2D64551472224,
	U3CU3Ec__DisplayClass21_0__ctor_m4C79D65771BF1D650104128A21491051D25DB9F8,
	U3CU3Ec__DisplayClass21_0_U3CAnimateProgressWithU3Eb__0_m1E45498D3E59582D06901EBC23D67EE9B7708E56,
	U3CU3Ec__DisplayClass21_0_U3CAnimateProgressWithU3Eb__1_m52390DDB9EB5BAF79F66543DCB0B987E427CE338,
	CSFindMatchManager_Awake_mCD552CD24E5411B32494071396365B46E25EF59C,
	CSFindMatchManager_GetPayLines_m340041209B635BB4522C7A8EB04B7026ADFD0CD5,
	CSFindMatchManager_CheckScatter_m4034889EA47DC1041CBC56001F78FD02B5A3086F,
	CSFindMatchManager_WinAmountForPaylines_mFBE1CF162CF7DB3F743C7F56F860847FE29C107D,
	CSFindMatchManager__ctor_mD40C29B78B0CC8ABA3FD6D0709815B776A254F81,
	CSFindMatchManager__cctor_m842EF22F7A29C0F49E2C23A9FD560568854883E1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	CSPayline_get_count_m20BF3B27EEE60C5D44A244C7D6C6872A1758FEA6,
	CSPayline__ctor_m13DDF3E47BE7ECAE209C7D4756607A6E4C41F4D4,
	CSPayline_AddSymbol_m824E7CB42F9B21A23A947019395790221332B6EE,
	CSPayline_isWin_m89A4E707AF784D572FC062830CE5E2895DBC0EF6,
	CSPayline_GetRule_mD3986A63337B92305C6F8473795AF4F21F573CD9,
	CSScatterPlayLine__ctor_m83CC8AC66063EF4DECFAFD8A929B345234829272,
	CSScatterPlayLine_AddSymbol_m6EDE072B70EF7FF273A3BCF9CF149067D92D6673,
	CSFreeGamePanel_get_freeSpinEnable_m7AFCC7BDC1E3DE823AA3F8F79E11F0ACED510690,
	CSFreeGamePanel_set_freeSpinEnable_m9F67FB760DFA56F835786D49024CA36F3AA1C701,
	CSFreeGamePanel_get_freeSpins_m07D2B45C7322CEDADBAD4C4D2B354FA60E8CC152,
	CSFreeGamePanel_set_freeSpins_mDCE177E387BE519D66D45BE4C6C1F95A99C032F3,
	CSFreeGamePanel_get_win_m345B7CF9A37F34BF3CEFF839BB848B8988DA1FBB,
	CSFreeGamePanel_set_win_m08FB17A566E3FE6D3CAD798A7D42DD8812C8681F,
	CSFreeGamePanel_SetWin_m9A78EF73492E927AFC16F1912614C212B03376C7,
	CSFreeGamePanel__ctor_mBD3DDA64503D55A4D12E32E7DD238981A081AE88,
	CSGame_Start_mEB2ECCB2319A8132CE5BA3785485C39CFF9373F1,
	CSGame_Update_mFAC8186AB5DCFDA9E8D4521A13E7378A22CFFDF0,
	CSGame__ctor_mCD5EE8778561B48F4AA37F11B2B73CE6D6A708E6,
	CSGameManager_Awake_m33132A5E5525E00D5997C2FF15AEA48A16722A91,
	CSGameManager_Loaded_m59DA71BE129523EEFA6B9DA31D72333DD04D4DDD,
	CSGameManager_Rate_mA33F5F3AFE88BC27E76AE9B6650CDA1D0386A854,
	CSGameManager__ctor_mEFB17626D6DEB11399BD785F6E4EE0DB4D9F32F2,
	CSGameManager__cctor_m64897BF8218413B4BB9E45B29D8D4F982CB1EA31,
	CSGameSettings_get_music_m6C5958C2DD63042B981B00FB52299190549099E7,
	CSGameSettings_set_music_m2F7B556AD5CAB49A260FA8A3DFB007420AE6B3D1,
	CSGameSettings_get_sound_mCB37E0C027ECBF65463A865080DE5C9E8D90D8C9,
	CSGameSettings_set_sound_mA084AE76541095FE9D3A03CE1D13349B37ED12DA,
	CSGameSettings_get_highscore_mE91AC0C4695BA7C8FCBDF9495E1759AD15F5A2F7,
	CSGameSettings_set_highscore_mD3BA9081771F760E4494F6A4060E6E7A7817DDEF,
	CSGameSettings_get_playcount_mF3B57D21CC8A61F98CD2FA2AAA2B2D34D69FED94,
	CSGameSettings_set_playcount_mF0CD183759A6FD40D88CFA579487EAE2278342DE,
	CSGameSettings_get_ads_mDE0051A3BE321D385D913D37F3A12B2D1B2612F0,
	CSGameSettings_set_ads_m4BFBA87AC1050FB37A966E88E5A3467D9E876C39,
	CSGameSettings_get_coins_mBA84A4FC5F8197967405115B24FEF320FEE81CAD,
	CSGameSettings_set_coins_mCD85255057007DA7B16C12BFF239C596C48E601A,
	CSGameSettings_get_xp_m4DB0F998D7A286F50A98368851F3B39240A5B215,
	CSGameSettings_set_xp_m89478E55C35EF73E9C6ADB05FA32E76A03D467FF,
	CSGameSettings_get_level_mAB49D065925159FEA6E2BF77D520C9566837E950,
	CSGameSettings_set_level_m443765A0ECDD02AAE9E67578CBCF021AEFD59D46,
	CSGameSettings_get_selectedTheme_mF83E3620F37C51B458C0B329C0D076F4A9E6E975,
	CSGameSettings_set_selectedTheme_m90C50EEC8D04163AAD116443CCD4635F12AB0F33,
	CSGameSettings_get_classicSevenBetStep_m7A8633D0E0E555873FF38D74E1B3CAF875B34063,
	CSGameSettings_set_classicSevenBetStep_mA593E6C180F21DA0FCF21EFCB3505BD3AD05858C,
	CSGameSettings_get_volume_m27B40F2ED0AA71A816787438A0B5E429BD982DD9,
	CSGameSettings_set_volume_mE6CFD8AE3887D2DA71FE010DBF48AB3C2625EB93,
	CSGameSettings_Awake_m33C590D01F3CF8A1C8CF87B176CD3E8F31CD9FDE,
	CSGameSettings_FilePath_mFF476BF74007CDF25CDEEFF849DDE2A4E792D83A,
	CSGameSettings_Save_m0C1AF49D3D9F0D62458D21EDCBE6B49844BA5981,
	CSGameSettings_Load_m91A3C4146B7EB2B89F78160F99E2D4CCF44AD09F,
	CSGameSettings_Reset_mCC529A51E25783047D98EAA9FAA26B627C0CF6C9,
	CSGameSettings_Delete_m639DBA0150BEB12F32AF847D9DF00E1E94136618,
	CSGameSettings_GetFileStream_m59DEB3B3E96936C29F98A78386C21C208C6D7090,
	CSGameSettings_AddTimer_m4F9DEC3794AF6C345D0234BDCED42AF8B25E3900,
	CSGameSettings_RemoveTimer_m7A1EF2575A9D4DB465B77B1B00C83F4098ADF22B,
	CSGameSettings_RemoveTimer_m136FBC88B9E05E3998C17FEDFE25031953B31887,
	CSGameSettings_AvalibleDailyReward_m75AD23B5F6AC677FFFDC0EECC85EA6F63EAA39F3,
	CSGameSettings_DailyRewardCollected_mEB621C1286A50AE3D8435222B6154F43D5826A0C,
	CSGameSettings_RewardDay_m097532924309FCBAC85F038AD6569DAA5924C141,
	CSGameSettings__ctor_m73EC3C87A262AA1B72CFA808DDCEE8FFECAD01A4,
	CSGameSettings__cctor_m042AF71218B0970319CC341D9DDDA608B2DFC8CA,
	CSGameData_get_TotalInstallDays_mAAEACE7101EBCF8BAB72BBBDE35CE6030641DB4A,
	CSGameData__ctor_m1FE4D0D28AD568D686691C37DC2BCED4DE5D07D5,
	CSIAPManager_Awake_m31D196FB9E0AA929DC5B1F52CCD8F705F02F4ADF,
	CSIAPManager_Loaded_m796788F3CCBA46C3668F4862FAEAC307D839CFF8,
	CSIAPManager_InitializePurchasing_mBF324D0C9B6EBFAEF22791285D7F8883F1E21B63,
	CSIAPManager_IsInitialized_m7A65CC1EA47DB11065E5E574055347D4916C3525,
	CSIAPManager_ProductForId_m82CC2B62AE28B58A4AAD62F0199E30ADCCDC9023,
	CSIAPManager_PriceForProductId_m25E30B70CFFEDBEB7486B6AEBF187EA7761980FD,
	CSIAPManager_TitleForProductId_m707B5468D8B11F9A4C1C01A3A3E1E41D25ACADD3,
	CSIAPManager_DescriptionForProductId_m8B8F8BFA85E7DE833347006BEA8B0F225ECFBD30,
	CSIAPManager_Purchase_m32891282F3B2B6DDE88C07CBD201557DA3ADFAE5,
	CSIAPManager_RestorePurchases_m0867EB885E6FD3B613543493B0BB504140BAA464,
	CSIAPManager_OnInitialized_mD318EBE6089F47973CAC686BF9C7B369816F90EA,
	CSIAPManager_OnInitializeFailed_mBB958AD161E4CE7B3241BD4BDCDDAED9B29084A8,
	CSIAPManager_ProcessPurchase_mB5541CD4E3424E546A02CDE99AFE54407B7F2E63,
	CSIAPManager_OnPurchaseFailed_m8A44AB877756B820A572E474631ECA81AAF6A190,
	CSIAPManager_ProductDataForId_m15805DEAFF797925297B266EA1B424179F3A69C0,
	CSIAPManager__ctor_m48BA8C87496613CCAF5CD7FB705891FB015EA6DE,
	CSIAPManager__cctor_mB32C84838320161BAE52FC75FF9387861D951D6B,
	U3CU3Ec__DisplayClass8_0__ctor_mC60FC8983D5E34C4C2933209B500DE6695AE4D51,
	U3CU3Ec__DisplayClass8_0_U3CInitializePurchasingU3Eb__0_mD1EF2034DA8EC9223ECA4403199B2C70028AB727,
	U3CU3Ec__cctor_m5AB778FF883DA1B6C11583CE21E545DC1E9FD987,
	U3CU3Ec__ctor_mA16CFC7CE83E88EF91B47ECD6C1F29E99A9D594C,
	U3CU3Ec_U3CRestorePurchasesU3Eb__15_0_mDEB9F112DEDBCF6BD2337D223D8291E3247E7768,
	CSIAPProduct__ctor_mF89195342849DC27E3493282AD39A1999697666B,
	CSInfo_get_active_mEF6E3DBB93C4AE596FC5E26DCFB6093FB38FBF77,
	CSInfo_set_active_m19C2C36C880B55B3DEFD95977384A07D09DFB15D,
	CSInfo_Awake_mE565412CB0DDB62749494DA62660326B533BE086,
	CSInfo_Appear_mA2AEBB36B38C4501483D35DE3BE3CB249B68936B,
	CSInfo_Disappear_mDE5FEE83D7E5DA843FF44ECC26C839B68E9214B7,
	CSInfo_Scale_m4DC8079B904A477DF0182F2FAE9FB18FF166EBF0,
	CSInfo_AlphaBackground_m31AE7E6D7ED65F883465D8810DD21F459783FA40,
	CSInfo_AlphaBoard_m17FB003BB487FC0A6D2779C21B888C55B7A8B74D,
	CSInfo_Alpha_mC9A376A328D57B6074621EB71AF16EF90D40E888,
	CSInfo_CanvasStatus_mC11C572DD6A86A13ADCAC6F74A3AAE7DE62A816C,
	CSInfo_OnClose_m4253B10FFF1FE1779916ADCFAEC6EE7199DFB880,
	CSInfo__ctor_m93621DA1F5239B84A0814F72F9C1BE5781C52B60,
	CSInfo_U3COnCloseU3Eb__19_0_m824E1B514D6FC36FF3BC04C6C91781B6A7045E74,
	U3CU3Ec__DisplayClass17_0__ctor_m1284C7DAEE32533DCC25BB53E947155FBB83FF5A,
	U3CU3Ec__DisplayClass17_0_U3CAlphaU3Eb__0_mACB94DFB42FE881112FEE6352E48FAFAF267C83A,
	CSLine_get_count_m47FFD50F292A840B462119D912A9DC4FFFD87C94,
	CSLine_get_Item_mA2228DF5D06778B4D64A9FA3A7824E51FB7481DB,
	CSLine_set_Item_m1ED001A1329F3C2F20BA86DC4860362B87B8D6CF,
	CSLine__ctor_mC368540ED8D5A4A58FB22688A02C04D9E3D37D29,
	CSMenuToggle_Awake_m5D6C945391FBB82FEB8A1E8D215EFAA2A5A4821A,
	CSMenuToggle_Update_m3FBB4185BD7F04DFCBC7B355AE449F5285B9BC1C,
	CSMenuToggle_OnMiniSettings_m982320A7D75AD7C50007CDD54B75BEC1F077E598,
	CSMenuToggle_PointerOverGameObject_mEF4B1070B0F1FB7DDC8BB314008985AE2D5D0A75,
	CSMenuToggle__ctor_m9B906E8FFC2E616856EC0850B6E47F9FCD1237B4,
	CSReel_get_Item_m0D1A0D0371F47EA4911433B30ED0969B983CF8F4,
	CSReel_set_Item_m247E1BC8E14A003ADDF3CC9BE101176D77C25A44,
	CSReel_Awake_mFB1253946A129BA1E85D00CABEEEF53FC36849A7,
	CSReel_StartWithSize_m9A06EE5AD7BEFD6455479D66869C4AA64EC3EB22,
	CSReel_CreateSymbols_mE8744AB99EAD2C8A63D4178FFD21167AB1E466EF,
	CSReel_CreateSymbol_m0D43606DFC6531A9463434DAECCB456FD06D80DE,
	CSReel_PositionForRow_m7B68B78BF88CB91EF6C2454CE2F533DE192CAF48,
	CSReel_Animate_m77B4A6EEA709C814292157334829ABFC5CB51EE7,
	CSReel_Parallax_m31BB619C3A783B07EB873E0CF3ED6B1477704C6C,
	CSReel__ctor_m04D8EB0E3A28A27B46C1B978AA38B4169320BC77,
	U3CU3Ec__DisplayClass16_0__ctor_m1FD5FC4F2E3014CF1B47272CED5C6D48EEAB133B,
	U3CU3Ec__DisplayClass16_0_U3CAnimateU3Eb__0_m8634D0AA32D727C7AF99E760CFA8556468BDF231,
	U3CU3Ec__DisplayClass17_0__ctor_m6A97AD9A19EBC8D1B40468BBF6F8B11F6EDCE04D,
	U3CU3Ec__DisplayClass17_0_U3CParallaxU3Eb__0_m333130C50BE07CF57A2F131CD80FC1AFB78F02C3,
	CSReelAutoSpin_Awake_mC338C06F7398CEEBFB625470B7DBD4B1052CC05F,
	CSReelAutoSpin_Update_m6C23AB750886015F44FE13D33541EB4FFFE704CD,
	CSReelAutoSpin_SwapSpinSprite_m4497828B0F201F4FEBCAF234091FE806AA62A328,
	CSReelAutoSpin_OnSpinDown_m2BEECFE8570B00A33004632CCEB42EE3638622DF,
	CSReelAutoSpin_OnSpinUp_m53732E3FAA1A9CB35CF723DC808461FAAAB3EA7D,
	CSReelAutoSpin__ctor_m8906225DF8D40446D5203F42A1F2AA7EB291CF32,
	CSReelRandom__ctor_mD48F72AD4073AFC6A710BBA6C2A3B308CF33EE3C,
	CSReelRandom_SmartRandomSymbol_mF89F81DB10EDA68BEC909A42535EA945658E56C9,
	CSReelRandom_RandomSymbol_m5E317A413D5E1AB62D395DF1F73212BBB05491F6,
	CSReelRandom_TrueRandom_mBDBFE494DA223D6C726B0516D97955490F4B0933,
	CSReelRandom_Test_mFD8ABDD30354D485838846458767D678729680B9,
	NULL,
	CSReels_add_FreeGameValueChangedEvent_mCFB5F737B6F73CF588EA258B3462DEEAD518A16B,
	CSReels_remove_FreeGameValueChangedEvent_mDD1891856F71DCC2723A893C39E664B464F0EF24,
	CSReels_get_autoSpin_mFC08FD8915EC98C2FE04A06892E430A7BD634520,
	CSReels_get_enable_m51C65BAC81136296489766D34662FEFD39EDFD1F,
	CSReels_set_enable_m84C300DA05F031D2AA0C6BC34848C2915FEF4978,
	CSReels_get_freeGame_m181D4F3845C0514B0EA7FEBF371BB7D175AA67FA,
	CSReels_set_freeGame_m4FF923704A53BCDFDFA8DDAA12D3ADF36C8498EE,
	CSReels_Start_m0E1CFC714303D26D441FCF7C04BA2492442DAACF,
	CSReels_GetTileSize_m5D896F8C6E5EFD58E82048055FE34CCFD25AFB67,
	CSReels_CreateReels_m33363E74E22F4635B83A848CCC00A502351A269D,
	CSReels_CreateReel_m3C4AFF38E15FE67B6E00B12594BDA26E707ADFEF,
	CSReels_PositionForColumn_mF3D91DF92A3E7795BC15072602598FCC8CC4F9B7,
	CSReels_AddXPParticle_m37863FC5069FD00027782445EB5DEDA432BEF13D,
	CSReels_Spin_m4E8663F295A7C321ED9474161B4C8D3D9AB7EF24,
	CSReels_FreeGameAutoSpin_mEC3D230823A6AC6DA56E98AD34146C7FF6AA9848,
	CSReels_AutoSpin_m6F058B4ED405E429AF5E9E9B8D688196BB55793D,
	CSReels_SetAutoSpin_m3060282E4F3109987C1FA53AC56C0BCDB73D4600,
	CSReels_Animate_m5303C4047E76092BE546EA057D3DD722BAC55811,
	CSReels_CellToSymbol_m906DDA4FED856F3057859261814788099079F589,
	CSReels_FindPayLines_m9642F7F8B1A88D3037942886B20B152E709E18C7,
	CSReels_CalculateResult_m1ABF13C6825CC4E185C243535AB9AA015C778D61,
	CSReels_BonusGame_mFCC3783D3B8F2E01BF0DF9B983490681AD0230B7,
	CSReels_StartFreeGame_m782F30E54A08F07B02C0F6E1D974E90D0619C530,
	CSReels_EndFreeGame_m98C41CAFD68F96337A3F7E9074341E58B48451D8,
	CSReels_SetPage_mEC2697A55E100AC06CE29EA5CD0613F5D5955253,
	CSReels_UpdateFreeGamePanel_mB3C94EF313BE68ED2FE025F21B6A1595EF7E6993,
	CSReels_ShowAlert_m4390CAC2BA0581DC95A76A5CEB63592A9BF0E37C,
	CSReels__ctor_mB9D1FB29F68C534A19417ABF3301E1302FBD4C25,
	CSReels_U3CSpinU3Eb__31_0_m761371C52E6624AA1C6F6A838D7D1733B7C9E96A,
	CSReelsAnimation_Awake_mC3DA79574CBB115813B6DFA0B956F22DB16042A8,
	CSReelsAnimation_StartAnimatePlayLines_mF729BFCDCE8585782048D090E12D1C0E195D554F,
	CSReelsAnimation_StopAnimatePlayLines_m7A9D717D5ACC6ACDD3CBF6CEAAF9EA891701DD11,
	CSReelsAnimation_AnimatePaylines_m26125ABF07A5731D264B3284B1166602D438D0D4,
	CSReelsAnimation_AnimatePaylinesFreeGame_m0D54CD7660FA8D4615026E2270390BEAA074FE86,
	CSReelsAnimation_StartPayline_m491F7BF956D438E7F03BA0D40D7A5B24421519D8,
	CSReelsAnimation_ScatterPayline_mCB70373DAC59DC2A1D5FBA3A9F8589A5601D41DA,
	CSReelsAnimation_StopPayline_m3FEF904BB259E71E56E35494418F38120DA339CC,
	CSReelsAnimation_DestroyParticlePayline_m791BD7145894F9F173152C3CCA36C3F71E0C5552,
	CSReelsAnimation_SetLineForPayline_mB599E6F9AC87AFED3122EB795F3CC7F90D4EF594,
	CSReelsAnimation__ctor_mFA06155A76A37769840250A4233D25CE20A13E64,
	U3CAnimatePaylinesU3Ed__7__ctor_mD4D6AAB9746CCF351CE765334F0983A6DF5BF854,
	U3CAnimatePaylinesU3Ed__7_System_IDisposable_Dispose_m6DE035D0BD06401C6712CBB42741F13B54AD248E,
	U3CAnimatePaylinesU3Ed__7_MoveNext_mB452328A8D02BA925D034E0C296BBE0DB194FD5B,
	U3CAnimatePaylinesU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC798924ACE7A70C9EA55F6486E219B70EF63FDEB,
	U3CAnimatePaylinesU3Ed__7_System_Collections_IEnumerator_Reset_m36AB284FD72B087EB4D40A2EE181A17C948EBD01,
	U3CAnimatePaylinesU3Ed__7_System_Collections_IEnumerator_get_Current_m158CB134A2F12231CED360FF8CE52E22A07B1846,
	U3CAnimatePaylinesFreeGameU3Ed__8__ctor_m88E61765CD759489B684B3F2C8B5F1D8F2CDBAD7,
	U3CAnimatePaylinesFreeGameU3Ed__8_System_IDisposable_Dispose_m93957906D15DE26BA2D822B5044001DE07DAFCC9,
	U3CAnimatePaylinesFreeGameU3Ed__8_MoveNext_m824326586127F2201B5C048056A0DD837F7A4ABE,
	U3CAnimatePaylinesFreeGameU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE6B67BCCE381B0183DDD621352CF8F186DB759CB,
	U3CAnimatePaylinesFreeGameU3Ed__8_System_Collections_IEnumerator_Reset_mE07F517D9960C222F6C98F6229BFF29B930B85B9,
	U3CAnimatePaylinesFreeGameU3Ed__8_System_Collections_IEnumerator_get_Current_m0C8203DBCE7A2EC30EF4AA8BA0E14A5700DCB3AD,
	CSSceneManager_Awake_mD63FDA1B12F3998783686CAFE01AA408BD70ACFE,
	CSSceneManager_Loaded_m1BF6A700B039B28CF5798C8502B57EC5F68024E5,
	CSSceneManager_Update_mC6572B2656E36583CA4EE2FCC74BFDA30F56521B,
	CSSceneManager_LoadScene_mB153168EAD4A264A28914015A5C0A236A39A9C0E,
	CSSceneManager_LoadScene_m4AE753B59E34E057C54246143C03BF1AA04327FC,
	CSSceneManager_StartLoad_m0066D323914782FB3870D7D36ED1423D528C2B3E,
	CSSceneManager_EndLoad_m24CB4105A6EECF663211AEA40413765F420F2862,
	CSSceneManager_Tick_m84EABBDA7CE71B07A45AC361FA05B79751AC6391,
	CSSceneManager_Next_m29C0A4DC19F647592ED8FB013AEDB6B0BC2F934C,
	CSSceneManager_IsComplete_mAC2B00306D8CF34693FFA2F38D49302E7AF744C4,
	CSSceneManager_PurgeAssets_m546FD4A4A3E7583B8E90D2FC685E230724319E5D,
	CSSceneManager_LoadScene_mEA320D80A38557D53F618E061A1101B311CF963A,
	CSSceneManager__ctor_m77AA654B2383E15B5604D1DBFA43664D66E7E022,
	CSSceneManager__cctor_mC55CBBF663350981E38314B22A2A21953E7B489E,
	CSSettingsPanel_get_active_mC6D503D2292366888196B874DA3F73380D952FD7,
	CSSettingsPanel_set_active_mA4E263AF6C1E61CA7478001986446BA79461AB3E,
	CSSettingsPanel_Awake_mA69AAD3C71A60B04AF2319E9E3E8AB81FF6BECF7,
	CSSettingsPanel_UpdateValues_m24AFF078B5B1D2EF71FAC3AF00E75EEB8C527F75,
	CSSettingsPanel_Appear_m78136F79593127DB2C96FC090C23784A79906E9D,
	CSSettingsPanel_Disappear_mACEDCB9E89F17C2E85CAC5939E27B0A5CF168A41,
	CSSettingsPanel_Scale_m0C7DAD710FDAC8B70A5494ACF811391DA150E7D0,
	CSSettingsPanel_AlphaBackground_m6B164BE8C49B2ECACF63C499CF7D6A424A9563D1,
	CSSettingsPanel_AlphaBoard_m110077FD02F682DEB4BAE0B592E3EF706B38637E,
	CSSettingsPanel_Alpha_mF853E384AA72BC9F80EBED8F0190A1D064351E3C,
	CSSettingsPanel_CanvasStatus_m09A0B85AE9688BAA8B56ECCE1187A08590A8696B,
	CSSettingsPanel_CanvasStatus_mB406334CA76477583E14B6C8F9B320F2FB0EC76A,
	CSSettingsPanel_OnClose_mEDF698F880EAC347D8207FA621859FD2DABC1F5D,
	CSSettingsPanel_OnSound_mA10BA9DC46A3497D1AA38229A917A7D04E20EC30,
	CSSettingsPanel_OnMusic_mDA51021F3CE9DBCECEB05A1BFEF265BC6EC099AF,
	CSSettingsPanel_OnVolume_m3C7CE227280A139884BCA960272F8DA239286C68,
	CSSettingsPanel_OnFacebookLogin_m8DAD32232DB8830867D1487002BA79DF57D0E074,
	CSSettingsPanel_OnFacebookLogout_mE932317FF72A0232F22DDF4FA4F5483C3F6C918E,
	CSSettingsPanel_OnFacebookInvite_mE88741C22AB61FC197E6DFBF06CD47B7D3505BA0,
	CSSettingsPanel_OnRestore_m9E02740609B27003E6E41EB4F9168A783135EB30,
	CSSettingsPanel__ctor_mC92C0A9E969606B36390440C13E4C8622BCC5742,
	CSSettingsPanel_U3COnCloseU3Eb__24_0_m7621188436DABC1F806C4339EE010D2A840AB82F,
	U3CU3Ec__DisplayClass21_0__ctor_mC2B2A7E6E31611F46D069C4A622C66AD918802EB,
	U3CU3Ec__DisplayClass21_0_U3CAlphaU3Eb__0_mBE7D2E5CD70369B05D9BB040C7CE2135E88C1F0B,
	CSSettingsPanelSmall_get_show_m117388B432AA40519D0D5DB44759A85DAF4444BB,
	CSSettingsPanelSmall_set_show_m5DF84B3EB6E3D7A7A6A169CAAF896B97F6B097FC,
	CSSettingsPanelSmall_Start_m533A99B464A3C89A2FF592A6ADF070F820048126,
	CSSettingsPanelSmall_Appear_m34D84F9619FB7643C4D93FF8076465ADA7A700FB,
	CSSettingsPanelSmall_Disappear_m39C59A60544C0252A215F8B444DD78C5303C76BC,
	CSSettingsPanelSmall_Scale_m32B0B1F9E51D50AD5E4AB6BB8416362B5FF7CB05,
	CSSettingsPanelSmall_OnSound_m8A6F40554A7A8B109D053926B031AC832B2F6DCF,
	CSSettingsPanelSmall_OnSettings_m9FDE9FE6A4F95DA004F6DC88E1B7DD9E4404B4E8,
	CSSettingsPanelSmall_OnContactUs_m909BE30313FF5E537253E50087256F30AEE0A518,
	CSSettingsPanelSmall_SendEmail_m04E02EFAA11F0A440A4DD5ACBC065D793130D929,
	CSSettingsPanelSmall_MyEscapeURL_m784FEFA564FC6FB98B84157866D13ED8641FAF0E,
	CSSettingsPanelSmall__ctor_m37258BCFA3953FAC783E41A1FB3FAD33D97D1B32,
	CSSoundManager_get_sound_mB995027223DF822CCFD4B3861440EA01B2C0DDBB,
	CSSoundManager_set_sound_m88953F084D0B81C3CF383068568F02A295814232,
	CSSoundManager_get_music_m0E162D07ABCD21B1B26157E1CD60CE45EED1C8DF,
	CSSoundManager_set_music_m4BBA4CC772B3B54F8DA97BD40B295BFDF9BE543E,
	CSSoundManager_get_volume_m362FB602FB48EA29F517361E5FC3024EDC7BAF4D,
	CSSoundManager_set_volume_m323706F07D247B4C2BE31E3CB41875A529FB3F63,
	CSSoundManager_Awake_mD71AAC755338C2ED179EDDF9AF045AB528A70D25,
	CSSoundManager_Loaded_mF55BBE6ADFFC5E7305E6D542E68C703A0287DE57,
	CSSoundManager_PlayMusic_m1BD270F017822C1C924587A0FF340584E95F853C,
	CSSoundManager_StopMusic_mF48719008E630496968421E3E433485D830121A2,
	CSSoundManager_Tap_mC3E135530A7FD889CCDFA05F9BD46CC97C344E21,
	CSSoundManager_Stop_mFDCAC1ED3AD6E581D9B347BF11117222C705888F,
	CSSoundManager_Pause_mF6F12C681EB0AFD512952D21C2995611B7247C96,
	CSSoundManager_Play_mD7EB328AF23761DDC3755D2A18DCCD671169D4C8,
	CSSoundManager_ClipForName_m3D55C697982B70405F3D80BFF940AA3B04E134DA,
	CSSoundManager_PauseAll_m8756405D5302BE75B6726A615610EF3FB6B431AD,
	CSSoundManager__ctor_m2A5DE3D303EDDAE8EF9AB44969492ED5975FDAD9,
	CSSoundManager__cctor_m944B86F0A3F756E4CFF73443E01B13BC67665445,
	U3CU3Ec__cctor_mADE652E646DE533C33432DF1955971A682F55EE9,
	U3CU3Ec__ctor_mBF2B1AE3B0AF8D90D1EBAFF1EFE1B758B8EABE7A,
	U3CU3Ec_U3CStopU3Eb__17_0_m5DACA5391D1584D17B885D552457CDE863989EBC,
	U3CU3Ec_U3CPauseU3Eb__18_0_mA6F024CB1BFBD89CE5303C7985782524E969261E,
	U3CU3Ec_U3CPlayU3Eb__19_0_m25F59A0AD2168B506AD29D4C92B276E68EB577C7,
	U3CU3Ec__DisplayClass20_0__ctor_m47D33B355A061ACCF35916340B3EDCBE12A66EBB,
	U3CU3Ec__DisplayClass20_0_U3CClipForNameU3Eb__0_m4B2DC76C45324B2CFBDD5D4A6E0F95BB49F120F8,
	U3CU3Ec__DisplayClass21_0__ctor_m7B70CC6F807B9F0D45292DAA28E54498A27A829C,
	U3CU3Ec__DisplayClass21_0_U3CPauseAllU3Eb__0_mC3F5C79E99C2176BDB0AE38DF368D26867713F75,
	Sound_get_name_m2F0E4AF357AE40536505E771E8F12D93A2E84526,
	Sound_get_isPlaying_m6FF0E3A5B9A511447A07224022B7596710AC1BDE,
	Sound_AddSource_m866B99890ADFE25B135BC676E3B8B606CA4CFA99,
	Sound_Play_mB064EA168DA913655CAF4BADB5942BC1BD4245F6,
	Sound_Stop_m418EBF80EA2F6D87147FDFD3D311B3ECA7F92425,
	Sound_Pause_m2B7DFA4F0583FF3BD6C0172742FEB47EF7075966,
	Sound_UnPause_m3615D722E9E7432BF5F883EE140608F2BFD90ED2,
	Sound__ctor_mEA0B0D2FBD514F91C21900B0BB8679CD78843FCD,
	CSSymbol_Awake_m8D57C09E36B923B03FD6899D8E6BBCFE965C6475,
	CSSymbol_StartWith_m6D92BFA31A7BABA569E25E614C0BB06246EF1D39,
	CSSymbol_SetType_m07241F5CEEAAABA2ACBD0A96CAB1B305D7676B6C,
	CSSymbol_AddParticle_m682E057E762123681161AD6F318906A7E15C3967,
	CSSymbol_LifeTimeParticles_mBE10311CFD3B37E9C8D25F5CCECB1A025CB313A8,
	CSSymbol_DestroyParticle_mDEA6B7AD4E99D000F92108D7089A814346934921,
	CSSymbol_StartAnimation_mF0487B31477AF67FBA5F87F751AFA1E9D2138F86,
	CSSymbol_StopAnimation_m03C94FDECE64AB4153BF06864B0B90BD0EE302B3,
	CSSymbol_CreateDictionaryData_m43B588846CAACE4BFCE6A3979F0DC404E96EC412,
	CSSymbol_Rule_m2BB931AB077BDF6DF9D4C4A0079BD27754C1668E,
	CSSymbol__ctor_m4764AE04F96E860C4678344CEFBF667617C46D39,
	CSSymbolPercent_ToString_mDB52EEF0AB867320264AF736B487BE6B117CF240,
	CSSymbolData__ctor_m4BDF8AE33151A2469E95DF8C56453067E2A60FE7,
	CSAnimationData__ctor_m35A1535591D52F4B10B77993FE193D51B898339F,
	CSRule_Win_m868E86E25771BDF0E19D2D7ED5AC66E1EBD6328C,
	CSRule__ctor_m3D0E3BB8C7ABB2A62AAF28FAA0BF44EB7C76C753,
	CSTopPanel_OnBuyCoins_m86B786C2A1DAC38C6B58B38F48318E9982D99763,
	CSTopPanel_AddXPValue_m7ECD2D6BA40F80FC2A9CFC70B153335D28D76367,
	CSTopPanel_AddCoins_mCADD5560B958C9FFBFA64BF7D84898C2CBCEB46B,
	CSTopPanel_OnLobby_m2D5E0BA304B1D114731C2379F96BCAC4B0FE277C,
	CSTopPanel__ctor_m0FF8BBE2CF3DEBD21ABDC858E8A2DE622CCA6E9E,
	CSAdMobManager_Awake_mFBCE1821A1184700BE8CE2E9E806414F7B81E0A5,
	CSAdMobManager_OnDestroy_m95C726DAF85A0DA1ED7AB37632220C0FCB7CE932,
	CSAdMobManager_Loaded_m981B6EEFB2EAC3427843A109351DEDC9D4923C2A,
	CSAdMobManager_InitAdmob_mC16CC92005A4E2A3C83A06C6030E27AD65FF57AB,
	CSAdMobManager_RequestRewardBasedVideo_mF3A7CE7A53719F5EC1ECAEC57929E21DCD76EF9D,
	CSAdMobManager_ShowVideoRewardAd_mA05529A6127DC68B96A18662D1BB7909BE64A814,
	CSAdMobManager_ShowInterstitialAd_m98EC45DDCBA3E083E173EAF2BE8A532C33505944,
	CSAdMobManager_HideInterstitialAd_mD4F7845277CC2525A84A0E81E9C0677B11E23708,
	CSAdMobManager_RequestInterstitialAd_m38CD9C61DDE47579644829519F1791C5C0505695,
	CSAdMobManager_CreateAdRequest_m4FE3B1E7BA5D8B122CD8F1AE70C82EB598F4A443,
	CSAdMobManager_CreateInterstitialAd_mBCFD61376452AE0072E6679DD7F149705915CF8B,
	CSAdMobManager_ShowAds_m95DE70B0EBE13CF3AF87DBD7F1178DEA3B5037D4,
	CSAdMobManager_HandleInterstitialAdFailedToLoad_mFCD43C930A7355F48DCB0589FBC014407F6EBD51,
	CSAdMobManager_HandleInterstitialAdOnAdOpened_mE785E07BCD01FC918FB8334804840F1521599EED,
	CSAdMobManager_HandleInterstitialAdOnAdClosed_mF8CEE254DCEAFD74EFA89909EC64DA33BF32866C,
	CSAdMobManager_HandleRewardBasedVideoOpened_m3AD185DC943BA3E205FB5F39E8044B4A0D754A6B,
	CSAdMobManager_HandleRewardBasedVideoClosed_m992A4797EA0325DDC3C4A19E3F69504A428DBE83,
	CSAdMobManager_HandleRewardBasedVideoRewarded_m71B2C156AB064BD84F2DCFF9D98E869004957AA9,
	CSAdMobManager_HandleRewardBasedVideoFailedToLoad_mF461F2F4F1E77E8C09703A392BF25B9FD1102016,
	CSAdMobManager_HandleRewardBasedVideoLoaded_mABEE476D7B01DEE88105340BBA8EF8C7491FFE5C,
	CSAdMobManager__ctor_m117F74D642EDD49DC962705BCC8369CDFAB57088,
	CSAdMobManager__cctor_m04A3D4B4D98B4610F98D1AD454199D7771299A72,
	U3CU3Ec__cctor_mF15F33F15785960B93F441527C0BE442CB53D431,
	U3CU3Ec__ctor_mC89E1B48E435EC43F555C1898A126E015DDB8B01,
	U3CU3Ec_U3CInitAdmobU3Eb__15_0_mF572069890B616E86C5FCA2C611C03775DD1108C,
	CSButtonTap_Start_m6560AA1157484D251E6829DC88477ED80E7B46AA,
	CSButtonTap__ctor_m55A4A9529B4D9F0C6C77ADE388FE7667045594E6,
	U3CU3Ec__cctor_m5680B75BCAE1191EF5EB0039237F0317518E0534,
	U3CU3Ec__ctor_m3E7D504EE5CF24ACAB7CC455A707D904DC0FD050,
	U3CU3Ec_U3CStartU3Eb__0_0_m44A3520B00EAE861C349F81C5D1066EBDE3A8D08,
	CSCell__ctor_m0C8EC709B1E45D68FF687CE791819FBDD81E45A7,
	CSCell_Zero_mF90C7682C3F29217DEE2C4977386867340D1B654,
	CSCell_One_m3764016EBD2A77635755CFD14F23F6382F14F065,
	CSCell_Up_m7E874F6B1D339642B96D1F68364A4F8669C1AE11,
	CSCell_Down_mD11942FCA842CEB18CBDC3F326E0331D0A5DEBC4,
	CSCell_Right_mDE9D4DA4D386B835211CB60C01FDE091333757F9,
	CSCell_Left_mD0E32BA21BDA968D41FF5EBDC55414C27788F194,
	CSCell_NextColumn_m6CBB9ACE6D997D24DD47373EDCCECA6FEB0D8186,
	CSCell_NextRow_mE1400800E58C6AD112B2ADCD1F147C0C163C42C2,
	CSCell_PreviousColumn_m86AA491343B74822BDF89BF80286B5C0B3DFA692,
	CSCell_PreviousRow_m3BFE6BD09252F9C2F836AFDC35C9D59F2BB0BE33,
	CSCell_op_Addition_m89F2809AE808E5E4F8E1624325DF26CBAE1D2117,
	CSCell_op_Subtraction_m0B98E50B26332713B57BD6C80AC162FD6A294906,
	CSCell_op_Multiply_m64AF49C81366F48E3DE4FE446E9F7B3569651A66,
	CSCell_op_Equality_m0C228A03F980DA8D5AC925D74436A43B6DF094AD,
	CSCell_op_Inequality_m5E144DD354080D8730A966249CB73BB129DA2B5E,
	CSCell_Equals_mED606063B54378D2B4B319B74D56F869A91AFECB,
	CSCell_GetHashCode_m68B4FB99837F4EABF397896223202DE95CAAEF46,
	CSCell_ToString_m9F794641C70313A531B220B091BF0019978C62DE,
	CS2DBoolArray_get_Item_m9B9776F8A650CF281486EDAF048BF6A8968647B5,
	CS2DBoolArray_set_Item_mA5B49B82391C45C0BCE298D67213C119CC664077,
	CS2DBoolArray__ctor_m9C9DC3904567576B15C4165F0739065305E7B8DF,
	L2D_get_Item_m17258F42C1AB187829FA85D33B930995C272CB8C,
	L2D_set_Item_m4D9FEDEA05402830C287E075742C20E4F947645A,
	CSEnumFlagAttribute__ctor_m6B052910A97CB4F2E5C202F04BF1EBA84AC12321,
	CSEnumFlagAttribute__ctor_m5259FDA7456298860A418138E8984D5B9ABDA886,
	CSFPSDisplay_Update_m28D6110B4AC2145B8FA9FAA5ABD4A840A1F5A673,
	CSFPSDisplay_OnGUI_m00C5DB2132975CF8580A7318157CC36E68BEBA04,
	CSFPSDisplay__ctor_mDEBCC3653113EC8133F0055DA7EC8F2B3F01D62C,
	CSGameScaler_Awake_m8EEBE36373DCA1D12502E3B624A63863124B2E39,
	CSGameScaler_Scale_mE7FCBEEB557787A31B437D1411456578D1D92484,
	CSGameScaler__ctor_m77C71FF328157A236355638F4CDFCBA73E401410,
	CSGlowAnimation_get_Material_m6B6384AABA52F2B8853E00C27AE6724F950E6FF8,
	CSGlowAnimation_get_enable_m16B2E71A3DB921FF22134EAF6DE0007D08B83AAB,
	CSGlowAnimation_set_enable_m8EC8028D16D1F6B1D36B16BE3198AA064A2BFA98,
	CSGlowAnimation_Start_mAEC28FFCA7C6315829BA1508C88C5D3A825145C5,
	CSGlowAnimation_OnDestroy_m5AE40B49B1ED5B523DEB3535813342F8D86A4F42,
	CSGlowAnimation_Action_mF4186990FBE21753B8BAE303BB134D2F06F2FD92,
	CSGlowAnimation_Start_m045372FFF11A0D162F0DEDDDA1137333A6BE26C3,
	CSGlowAnimation_SetShineLocation_mCB1D71F46B1561B85A6C1BA7723E1FF7F477B28D,
	CSGlowAnimation_GetMaterial_m118B558F3148A0C56403E2757F5E499A4A00B23B,
	CSGlowAnimation_Stop_m5E09C2D911CBE011609EF21D2B0FC813ABC91224,
	CSGlowAnimation_SetMaterial_m66B85D1FE992876FDE75E80705DAA6317A844C15,
	CSGlowAnimation__ctor_m1531F50F2E50E53D5885D8449F294B8C2AC4EC01,
	CSGlowAnimation_U3CStartU3Eb__14_0_m2B6F45F34F11E7F1C3AD45944E5F7ACFB768864A,
	CSGlowAnimation_U3CStartU3Eb__17_0_m6F8A4CBDC691DBF8775798137980DA997C5567D7,
	CSCardList2D_get_Item_mDE93E85DFC4AAC3B61A90B6721188B535085718F,
	CSCardList2D_set_Item_m5B12731E0E1AEE9CF3FAAFFB4818BB12CFCA1D50,
	CSCardList2D__ctor_mA9712CA94278D594048CF84077E12C810BAB25A1,
	List2D_get_Item_mB2DF0149C9C9045D8DF5B2A6796335BF11D30B16,
	List2D_set_Item_m9566EFC162F85575933F9BD9127E2B2BA6C1C0CE,
	CSSliderGroup_Start_m5ECB7BEF38E8A1665D8FFE73C045228514EDD722,
	CSSliderGroup_OnDestroy_m6C9624B348FC45223B716486FA5B17183C787657,
	CSSliderGroup_onValueChanged_m404AEC9C63450E627E4536AA643267024583B5BC,
	CSSliderGroup__ctor_mE1E2BE6B360F855596FC01DC1C68A57783EA2E3F,
	U3CU3Ec__DisplayClass2_0__ctor_m3C7EB1AC7CC54057F31AEE3B2F13EF87A2F2637D,
	U3CU3Ec__DisplayClass2_0_U3CStartU3Eb__0_mCDB453C141F5C53D3A3DBC131324B0C8EBF13D5A,
	U3CU3Ec__DisplayClass3_0__ctor_m861E98EFE78B4BD3BB5FDE5CA1CAC9C8C9D611A2,
	U3CU3Ec__DisplayClass3_0_U3COnDestroyU3Eb__0_m74B61B841EEF753187CAAA0F07A131012E944C3F,
	TimerDelegate__ctor_mAC91CA2E750EEAF31798F5B718C8D11A320CF77C,
	TimerDelegate_Invoke_m501F053679BE400307B360BBF0A9D8341D1AA496,
	TimerDelegate_BeginInvoke_mAD21B78B7322C5FD7C0DC461A262F8D9CF5EDCCE,
	TimerDelegate_EndInvoke_m339B5435C09519708773BD0D696D2FAF063BE0B0,
	NULL,
	NULL,
	CSTimer_add_timerTickEvent_m4C53AE1A27DF33DCF4B9042A739D5B52954B12EC,
	CSTimer_remove_timerTickEvent_m86940855EF727EEA2557AAEA6896FFAC5037AF94,
	CSTimer_add_timerStopEvent_m685240BFBA6110FC031EC045C8E7C4DEEEB6F47C,
	CSTimer_remove_timerStopEvent_mD79D54D5A8D3430F6FA97410C63A97077E6D7732,
	CSTimer_get_running_m39D048B1383BC9A478ABF9CF6D687DFC9CFDC303,
	CSTimer_set_running_mCE2A21EA49A1A2B1CA76A201EB1288DDD4D3D89F,
	CSTimer__ctor_m8BFECAAE4D3FB335E450309C2ADF700305B015AF,
	CSTimer__ctor_m6D4B46768AB7DA8D50F5F877BAA53DFF9557123C,
	CSTimer__ctor_m4E47B724F200621E0CF3BFC13A516A9F88BE48F4,
	CSTimer_OnDestroy_mE7044D88B8D4B316126D3D50DA6AFDB9FCFF6A1C,
	CSTimer_Tick_mD0BD23CF3BB947F921874006A670EC0EA3C3D99B,
	CSTimer_Add_mB6F085C72C9EA37E70F0788A456FB4E408E73907,
	CSTimer_AddMinutes_m6957558CFDCCF678890A85F7BBDF1A78E498F4E5,
	CSTimer_AddHours_m0DF55A76695C9A146079AFC4B9B9A18747A8DB33,
	CSTimer_AddDays_m13EC79056F5276E1944C43BE16FDDB47F8A72BE8,
	CSTimer_Subtract_m076F275E7C35ABBEE0FCD4470DDE82D3197A8D34,
	CSTimer_SubtractMinutes_mE4D7498A2BA4C7575165D70BCC6688998899F0E2,
	CSTimer_SubtractHours_m45BD8B5F8C66C1B2256E4A83F95536BC6B40142A,
	CSTimer_SubtractDays_mF55C8846828792F059DA1FF32F3C66052F313D40,
	CSTimer_DeltaNow_mB70E1B4DEDE83D63B33E20936CBF7082300B1A17,
	CSTimer_Delta_m4198B938B3B37C0B01EF49108A9F613FF7CF2175,
	CSTimer_Delta_mFB0C804DC639E26C5A395AFB5E93F15F975D0FA2,
	CSTimer_DeltaDays_m25DF60B59CD441A1DBE4DC471D0B4B9094A3E73B,
	CSTimer_DeltaHours_m6653AFB1022EF65E853D172FE79AF00E0B2E659A,
	CSTimer_DeltaMinutes_m366A156168E0D1A83989FADEFC50C431490AA360,
	CSTimer_DeltaSeconds_mED3F7CFC8A9979928C9347DAC454BF87D6CF90F1,
	CSTimer_TotalSeconds_m43306ED0006DA990BBC4B7E36A90C1E1D6EF86D5,
	CSTimer_UnsignedTotalSeconds_m711FB0C79F365B1576263FC19D2109C69A1860F7,
	CSTimer_TotalMinutes_m40B48493ED7F80315406445824C8BD00789FD2F8,
	CSTimer_isTimeUp_mFD15A14EC7507DB8CEC3EB6F4D7E4F7C3900EA2F,
	CSTimer_Subscribe_mB02CCEED830F909D3AAF05FE89F38B5A977CBA54,
	CSTimer_Unsubscribe_mC5B1D46A2713EFEB72D951FC7EBC380ECC54A93B,
	CSTimer_UnsubscribeAll_m0242E17404EF2E6A6561B871DAF68320D06C5BF7,
	CSTime_Now_mDF557C8208DBD172B15364241E842F9873BB0195,
	CSTime__ctor_m80F677EC60F7B5F61FB9CC2DD0F57853F1BDA122,
	CSTimerData__ctor_m52E7865B4D72DFDC44C03FE80861CF33BD41F654,
	NULL,
	CSTimerManager_add_TimerCreatedEvent_mC170EB57ED91700AFDD85FA2540DFEF9157F69CD,
	CSTimerManager_remove_TimerCreatedEvent_m173BBDC52674515908021890019CE251FF168484,
	CSTimerManager_Awake_m4783BA5B482F90FA9B73DEADCE30E0D823A3CF44,
	CSTimerManager_Loaded_m2D1039D77EE569348E8638A64E676E6AE0E48A8E,
	CSTimerManager_Update_m65D6C8A69E0905616DAAE10127B342876F4B3A55,
	CSTimerManager_LoadTimers_m2BCF4C33D453F27ABD7B52543E118C46400C0939,
	CSTimerManager_AddTimers_m451A8A74081A84E95FB8E8630C08FC5A8B4DE4C6,
	CSTimerManager_DestroyTimers_m4F9FD66A0CB0FA86D9F2B2913F423349D39D80A5,
	CSTimerManager_CreateTimerHour_mFFB0927C579EEDFBD4D0F75885E544D45C75E3CF,
	CSTimerManager_CreateTimerMinutes_m93C5FF0FDFE863D129A0823C52257CE6DE167239,
	CSTimerManager_TimerForKey_m1BFF886EBBC56E254AAC7220C10AD00E4E44607E,
	CSTimerManager_DestroyTimerForKey_mAF073C786DF6428E53C071D7ABB82751FF162245,
	CSTimerManager_DestroyTimer_mBFB468F26E81812FC9B13BAB9D1F0AAD0E6F6BC5,
	CSTimerManager_AddTimer_m5E33F24A4BD6F5BFD743C34ECF852432F2BC61F4,
	CSTimerManager_SubsricbeToTimer_m3D17ADA853BF9CD27F10EA8060841670494AE725,
	CSTimerManager_UnsubsricbeFromTimer_m621A90B83495566C58ECEF00E93B3022B15BF514,
	CSTimerManager_CreatePresentTimer_mE466A966C7C374A05F3702C8F3CFB603041D769D,
	CSTimerManager_DestroyPresentTimer_mB07B01E1BBF5CE88A476270DFF98B528A1AEBE11,
	CSTimerManager_CreateWheelTimer_m5949428B2B6BB4271CD5E7D4982376ED3E955E34,
	CSTimerManager_DestroyWheelTimer_m95F628D33CDA04D2829BE3EDA80FC43BA3267DF1,
	CSTimerManager_GetPresentTimer_m15A9D5F4D55B91FEF292FF505581D1B9380F91DB,
	CSTimerManager_GetWheelTimer_m0A14D6024AE11FB4DFA77A3A66C1FB38C04B28D6,
	CSTimerManager_SubsricbeToPresentTimer_mC5F38353E392D963BDF8B4402C20F4493CAE5473,
	CSTimerManager_SubsricbeToWheelTimer_mF9670E9892A37023E34137D9674EB71BE17EBE84,
	CSTimerManager_UnsubsricbeFromPresentTimer_m78DE47DF6416A61BE2CBF948ED8F29765CFADA08,
	CSTimerManager_UnsubsricbeFromWheelTimer_m3B598D2CA7CAD5D7A6ED2099F092BA25B7D5AC80,
	CSTimerManager__ctor_m0E7561D7F860DCA0158732C97C732D26B68FBC6C,
	CSTimerManager__cctor_mC08971E445F5FC0EBA57D60413C7426F25BC5E5C,
	CSToggleGroupHelper_Start_mA3B10B55B8C77B916EB23AD1F710720C238616F6,
	CSToggleGroupHelper_ToggleChanged_m6B9A5880CB5F2BB3201C01C41051690B860EE358,
	CSToggleGroupHelper__ctor_m6C1387B79C5BB92AD1CE07FFF5CC64C000D2F206,
	U3CU3Ec__DisplayClass3_0__ctor_mE5495258D918BEAC8815C1F401E4B657C6A80EF7,
	U3CU3Ec__DisplayClass3_0_U3CStartU3Eb__0_mF2B1395E21F5FEFB668E57CF1CABBC8B05C83D41,
	CSToggleTap_Start_m4194B9168E38AF9B9173516323882E0A6B9CEE88,
	CSToggleTap__ctor_m33B9E1D1F8EF53F016F15081420557725C52ABE2,
	U3CU3Ec__cctor_m759F11235A281874BCE6B1E05448E85619739437,
	U3CU3Ec__ctor_m47F8944E7A5120A93B26946FE1377E7B277AE68F,
	U3CU3Ec_U3CStartU3Eb__0_0_m38C76ADBC4CBAB0933E9511DD676499597F16202,
	CSUtilities_FormatNumber_m65E38533E89A40AFEA2E2B8319EEE2B906DFB014,
	NULL,
	CSUtilities_AnimateWithFrames_m3A6536CE2BE2AC8AA5ECB1FFDD63F22783E27411,
	CSUtilities_CalculateDuration_mA125160DBFCA9DBD31730CF5759FF7E360E42234,
	CSUtilities_LabelAction_m9536631C599D75A773CFBB9699500D48E3494835,
	CSUtilities_LabelAction_m03FB3607C8041434A112CF6B3F7C510AAA097F6E,
	CSUtilities_TimeFormat_mA4C4899CB77F8574B6F7F36C13F2CF06FF199561,
	CSUtilities__ctor_m4387E659D9046E628E44FB25F18A151050257F7E,
	CSUtilities__cctor_mE1B92C1D5C90846E539E045C68A0686BD92E90B1,
	U3CU3Ec__DisplayClass5_0__ctor_m61B5ED513EB032DCBBCA33450900EBC70FBC7719,
	U3CU3Ec__DisplayClass5_0_U3CAnimateWithFramesU3Eb__0_m25A151A93527F3B4B992D95E8D9E70C07DDCB353,
	U3CU3Ec__DisplayClass8_0__ctor_m244CA7CF796DDCDDA350001552965A4F683D1FC8,
	U3CU3Ec__DisplayClass8_0_U3CLabelActionU3Eb__0_m6519239416D28607D0D674A261549D65F7C0375F,
	HorizontalScrollSnap_Start_m80023AEE9DDCBC718CAB66E84BEB4648D4AD7895,
	HorizontalScrollSnap_Update_mDCC17A71E7C4823E99E8ED4DD27A81B1D60C7A29,
	HorizontalScrollSnap_IsRectMovingSlowerThanThreshold_mE7A843809FE30021E4045A9FD7A28B0FBCAD9A85,
	HorizontalScrollSnap_DistributePages_m28864A2E25760776B5BEF1C8843EF045BB9B666B,
	HorizontalScrollSnap_AddChild_mAC70C81104E4D1986A8B97D1059D84C9CA197F85,
	HorizontalScrollSnap_AddChild_m644E8EE86D58D542B2E4B308B976A669C20BC12D,
	HorizontalScrollSnap_RemoveChild_mDD35AFA7560623F1C4AA2E908F26A9EA8B0ACB2F,
	HorizontalScrollSnap_RemoveAllChildren_m3B04527F4B659C90A0CC409205968FED1A83627C,
	HorizontalScrollSnap_SetScrollContainerPosition_mBEF5A23EA14AADD69865258AA90FE3A0BFDDD4B2,
	HorizontalScrollSnap_UpdateLayout_mBC3AC45BE51B12A4F35E50FECB5AB77D8B56658A,
	HorizontalScrollSnap_OnRectTransformDimensionsChange_m6A744DB8BAA1DE928FCCD3389DEA1FC451A2DBA3,
	HorizontalScrollSnap_OnEndDrag_m6BB57030D8645A6323F91212B9DB6B21D6A74754,
	HorizontalScrollSnap__ctor_m34A5926EF17B61BA53FAC30DAB9CE1B5CCFF26BE,
	ScrollSnapBase_get_CurrentPage_m9B3DE8B11183EE8212AF1318906C744D711A4562,
	ScrollSnapBase_set_CurrentPage_mE3B8B097A054EDD9129187B8210DC96368B779FC,
	ScrollSnapBase_get_OnSelectionChangeStartEvent_mC2F99AF2296D3FC0D1E786470B9BD8947F21827C,
	ScrollSnapBase_set_OnSelectionChangeStartEvent_m7334D49D8D3C1991138848E4C960F188F730CC09,
	ScrollSnapBase_get_OnSelectionPageChangedEvent_m525D2766053E986DD79F3A32A5CC0DEBCCF5F956,
	ScrollSnapBase_set_OnSelectionPageChangedEvent_m61532F77155F678EA1EE86FCB36AE63F6CB9A5AA,
	ScrollSnapBase_get_OnSelectionChangeEndEvent_m22028CF7165330B5E1DD0BB393D30B7DBB290CC1,
	ScrollSnapBase_set_OnSelectionChangeEndEvent_mAD7AF338B88F446F4313B93119884462558B688C,
	ScrollSnapBase_Awake_mA83FA22D2B1BE1FA483B0FD5700058EF7AE05CA5,
	ScrollSnapBase_InitialiseChildObjectsFromScene_m98A082721B861E645E6581A83EA599CE2DC45BF4,
	ScrollSnapBase_InitialiseChildObjectsFromArray_mD31EC067271250D3E458461DCEC8447FEC556FCB,
	ScrollSnapBase_UpdateVisible_mCD0E1732CBBCACD6EE0DF8EE7465E26132AD75C8,
	ScrollSnapBase_NextScreen_mFDBCB916020C6531491AD59F5A6F11B1C4DF545C,
	ScrollSnapBase_PreviousScreen_m15A2B1D9E4BFB8C608CD188BA0BE7518499A6E2E,
	ScrollSnapBase_GoToScreen_mC7F3B882B8B0DCC0C91CF05EA3663B8B05DA52F7,
	ScrollSnapBase_GetPageforPosition_mC1F3151D884790B82A944B8BE5023D006F1DCF6F,
	ScrollSnapBase_IsRectSettledOnaPage_m37D4CB53D73B909D94D0714111347CCA500FE567,
	ScrollSnapBase_GetPositionforPage_mEE8992489F81C0F77397B50CADD6B3BC866C0B5F,
	ScrollSnapBase_ScrollToClosestElement_mB8F27BF8A30152817988C0552C13EE52FE23E6F1,
	ScrollSnapBase_ChangeBulletsInfo_mCA6A55BEF6BD9AC1514B6ED84B8D43F57D3521DE,
	ScrollSnapBase_OnValidate_mFA23F26882A4D11AAE56594C680AFB1429690739,
	ScrollSnapBase_StartScreenChange_mA3AC6E65D454803EB3C4CE1334B9F7716EA72C25,
	ScrollSnapBase_ScreenChange_mE184FC874FA10E59C9A64D0BD2342F0E550C0F76,
	ScrollSnapBase_EndScreenChange_mEBB5CBB9157C82C13BB511AA541A140322C7BC07,
	ScrollSnapBase_OnBeginDrag_mF9AB1D5314BEFC675392F8BAAA0E9BAC0CD70EFC,
	ScrollSnapBase_OnDrag_mE399D592D4D44A6417B458440A1033D31FF0F02D,
	ScrollSnapBase__ctor_mF107F70E03A9D45A6A4A0690BCB167450E782751,
	ScrollSnapBase_U3CAwakeU3Eb__51_0_mA8343B4EB63458F2C2B9261B675535D8DFD1B9B9,
	ScrollSnapBase_U3CAwakeU3Eb__51_1_mABFF0A12D4D0ADBDEA9A7AC45E26DAB17EFFD8D0,
	SelectionChangeStartEvent__ctor_m92D2ED42EE1ACFDC97CC2D36080D59DEC9F06237,
	SelectionPageChangedEvent__ctor_m32BEB6EE4F4AC30C2F8A9F6B787EA62174FCC1F5,
	SelectionChangeEndEvent__ctor_m82148ECD356BCC1188582E8E685B0C84F0314F44,
	CSZLGamble_get_bet_m22EBC18F79A5F80DB19AF67E41E334C564B575B9,
	CSZLGamble_set_bet_m7CA94F11501E6F82964D14189905ECDE0B4E36A9,
	CSZLGamble_get_bank_m4D2AC7F1842ADCFA07470E837AED479622BB7FFF,
	CSZLGamble_set_bank_mC3295A159C610CAD8FB48CA7DDB49BE29205EB13,
	CSZLGamble_get_interactable_m877FBB2EFC2FCBA1C4527839D3D4C9E6A3E60DD8,
	CSZLGamble_set_interactable_m1EA4915FE55F68E65F0D2AB14FDD6FC6A5145B3A,
	CSZLGamble_get_enable_m43487C216A45C23C87BFE5F08CDFDE119EA3811F,
	CSZLGamble_set_enable_mE8528FC815D7F3B1076FF2E784835BC97379235E,
	CSZLGamble_Appear_mA852224A295112A226237703350A44FC0384C7AB,
	CSZLGamble_Disappear_mFADEE9D6BDA71A327DC1BBBFF02E78AB8C13989F,
	CSZLGamble_CreateContent_m2989F00F337B1694529762F793EEA5A7697E8B09,
	CSZLGamble_DestroyContent_mC3091332F5012A500378BBE6C3F82D36D4282504,
	CSZLGamble_BackgroundAlpha_mE4F068BA07239661A99E90ECA48CF76A7A9DA1DB,
	CSZLGamble_CardSelected_mD7E7D5948D54BB19AD583244619757DA3106835E,
	CSZLGamble_ContentResult_m312C441D6A227A353FE1A3949A6139C0BE6B4ABD,
	CSZLGamble_OnCollect_m289701076BA9FAA0D16DCFC5C9E3586692134D24,
	CSZLGamble__ctor_m7383A9616E0F108BBA8D3E8DAA0CCE6A436D16CB,
	U3CU3Ec__DisplayClass26_0__ctor_m4695728A81161DCAA2401F0B71042DD08AB5970B,
	U3CU3Ec__DisplayClass26_0_U3CAppearU3Eb__0_m4C053283E1196E7C73800F57F6203F3F5D5E9A45,
	U3CU3Ec__DisplayClass27_0__ctor_m277F097813134417D162400B951DE467A9F9D27B,
	U3CU3Ec__DisplayClass27_0_U3CDisappearU3Eb__0_mFD542E11BDDE242572AEC4DB68597FA66CB89C17,
	CSZLGambleAlert_Appear_m6E383248A258377DC45842A2CF0D8FAD27CBDBEC,
	CSZLGambleAlert_OnCollect_mEED8476F2F92B421409ACB43B9F7AE1C0D265704,
	CSZLGambleAlert__ctor_m4F2B55B4AEBB9EEAE99D4B2AF975C99FAFB181AD,
	CSZLGambleContent_add_ResultEvent_m4B23C6DCA6A110B76DB41457AFF0C313E2472A57,
	CSZLGambleContent_remove_ResultEvent_mB0FD442349C7C4A0629EAF1C3E19CE2A1E235893,
	CSZLGambleContent_add_CardSelectedEvent_m973E8FA9EB3B9E792BA462C985B7F829A3C34CAF,
	CSZLGambleContent_remove_CardSelectedEvent_m43030534131D742C28DD2FA9C9B095C51E878181,
	CSZLGambleContent_get_enable_m038E01B9DFF6613F066453FBFAA5332252CE85DB,
	CSZLGambleContent_set_enable_mE025802673DB38F12F81862B9255F39468E01535,
	CSZLGambleContent_Awake_mA42A8CEF7C3B348529E5286272D630F3FCAA5D2C,
	CSZLGambleContent_CreateCards_m8344DBE19F2CAEEFA7EA05341DFA30FCE73F7162,
	CSZLGambleContent_MoveCards_mFC49FC4B6B7CA5143608AAC148E4AD3E784D2E9E,
	CSZLGambleContent_Clean_m5D8DE0E69FB82A42020010A85074F1A75BB928FE,
	CSZLGambleContent_CardAtIdx_m73E285D2CBC2F390F45A3BA9CC095E99C049BE9E,
	CSZLGambleContent_PositionForIdx_m3F1F8FC26930F83205969C5759FA4680C38F1852,
	CSZLGambleContent_CreateCardDeck_mB3242135F8B7A5284EC17BFC5C789B58BC5517F7,
	CSZLGambleContent_RandomCardValue_mBC92E7948350EB6B18F799C3C8D753EB41797D3A,
	CSZLGambleContent_FirstCardValue_mADCA20A4C2FF34D568E4D4300293B3C59E472750,
	CSZLGambleContent_CardSelected_m790D4303995B0A253B44958128E2CA2C61B7F0F8,
	CSZLGambleContent_ResultAnimation_mA17DDD2D2A2053EFDB407170E15D1EEC628E03FB,
	CSZLGambleContent_IsWin_m5EAD012141FFACCE54A6C7C79598F6A335EAB61B,
	CSZLGambleContent__ctor_m0AE3BD259A5A7939B6D36F0529BEAF766D909E7F,
	CSZLGambleContent_U3CAwakeU3Eb__15_0_m7889AAAF61BABB9CDBDE6455352FDF28FDFB175F,
	U3CU3Ec__cctor_mFDAD6EF5DA3F3E4EB3D500F6D289547DA69A14DC,
	U3CU3Ec__ctor_m5CD2DD63DE8E91DDFB81D216D1A87333C4F8CB5B,
	U3CU3Ec_U3CMoveCardsU3Eb__17_0_mA6B0835478334161AC447C8B9F73C2473EC48BBE,
	U3CU3Ec__DisplayClass24_0__ctor_mF8D1ECE0C585D63C5B28605CC2E7C2F0E941D482,
	U3CU3Ec__DisplayClass24_0_U3CCardSelectedU3Eb__0_mA75456B6FEAC6ADF8AF3C6BB1A9760E2223BBE9C,
	U3CU3Ec__DisplayClass25_0__ctor_m9F066F31A5D4DCB79EFE8842EA1E12833054CFCB,
	U3CU3Ec__DisplayClass25_0_U3CResultAnimationU3Eb__0_mA30C4CFA945BB78BC492400478F4079C20CBBC34,
	ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2,
	ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B,
	ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371,
	ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026,
	EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93,
	EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE,
	EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC,
	U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B,
	U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF,
	TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226,
	TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E,
	TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1,
	TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3,
	TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385,
	TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4,
	TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B,
	TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788,
	TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A,
	TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076,
	TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D,
	TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA,
	TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB,
	TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5,
	TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70,
	TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11,
	TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC,
	TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44,
	TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC,
	TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53,
	TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66,
	TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43,
	TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77,
	TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8,
	CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7,
	SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2,
	WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C,
	LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018,
	LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0,
	Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C,
	Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F,
	U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73,
	U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E,
	Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4,
	Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376,
	U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561,
	U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2,
	Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046,
	Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5,
	Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77,
	Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516,
	Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE,
	Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9,
	Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C,
	CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E,
	CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610,
	CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373,
	CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3,
	CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124,
	ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA,
	ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3,
	ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC,
	ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8,
	ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2,
	ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469,
	ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64,
	U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B,
	U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362,
	SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F,
	SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5,
	SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697,
	SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB,
	SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F,
	SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3,
	SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3,
	SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86,
	U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF,
	U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A,
	TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE,
	TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7,
	TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF,
	U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9,
	U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261,
	TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0,
	TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA,
	TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50,
	TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712,
	TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8,
	TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534,
	TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4,
	TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848,
	U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C,
	U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F,
	U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8,
	U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27,
	TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046,
	TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF,
	TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E,
	U3CDisplayTextMeshProFloatingTextU3Ed__12__ctor_mA27BBC06A409A9D9FB03E0D2C74677486B80D168,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_IDisposable_Dispose_m510FB73335FCBCBEC6AC61A4F2A0217722BF27FC,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_MoveNext_m8DFB6850F5F9B8DF05173D4CB9C76B997EC87B57,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCD7B5218C81C0872AB501CD54626F1284A4F73BF,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_Reset_mD2D32B94CA8D5A6D502BA38BDE1969C856368F73,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_get_Current_m59B2488B0E3C72CE97659E8D19BB13C6E0A44E90,
	U3CDisplayTextMeshFloatingTextU3Ed__13__ctor_m359EAC129649F98C759B341846A6DC07F95230D2,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_IDisposable_Dispose_m36DE8722B670A7DDD9E70107024590B20A4E0B18,
	U3CDisplayTextMeshFloatingTextU3Ed__13_MoveNext_mB7D320C272AAA835590B8460DA89DEBC0A243815,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m891591B80D28B1DF2CF9EB78C19DA672A81231A2,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_Reset_m5EF21CA1E0C64E67D18D9355B5E064C93452F5B2,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_get_Current_m4850A72D887C4DA3F53CA1A1A2BEAD5C5C6605A1,
	TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB,
	TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA,
	TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1,
	TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB,
	TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF,
	TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4,
	TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB,
	TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12,
	TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3,
	TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720,
	TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21,
	TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1,
	TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C,
	TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF,
	TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A,
	TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D,
	TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0,
	TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B,
	TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363,
	TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C,
	TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B,
	TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE,
	TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943,
	TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD,
	TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0,
	TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66,
	TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF,
	TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A,
	TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB,
	TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910,
	TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691,
	TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D,
	TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F,
	TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F,
	TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2,
	TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33,
	TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E,
	TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944,
	TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647,
	TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84,
	VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC,
	VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66,
	VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42,
	VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9,
	U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52,
	VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F,
	VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825,
	VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846,
	VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97,
	VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549,
	VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16,
	VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE,
	U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF,
	VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2,
	VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA,
	VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2,
	VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE,
	VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E,
	VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599,
	VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62,
	U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E,
	VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5,
	VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971,
	VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4,
	VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1,
	VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5,
	VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87,
	VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC,
	U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD,
	VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C,
	VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC,
	VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884,
	VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C,
	VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F,
	VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED,
	VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2,
	U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107,
	U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F,
	WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0,
	WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4,
	WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB,
	WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2,
	WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099,
	U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60,
	U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25,
	LeanDummy__ctor_mD0BA3A71589D10708BAF2C24160EC0305D014DBE,
	PathBezier_OnEnable_m5E057D877378F11B064309894D1AE8BB524F4771,
	PathBezier_Start_m17B69DE04B368B76932DC991342B633A7F0A9334,
	PathBezier_Update_m3827BD355D4006FA740F2B9F1EDD8C426CB43A91,
	PathBezier_OnDrawGizmos_mBC48C0867056FB925C45FF97187F8AB4C4D6F13C,
	PathBezier__ctor_m64C0684464ED26D96A5AE82DEA44C6ADF0F2085A,
	TestingUnitTests_Awake_m93E88F38DA88C64CC12ABAE80900B948E5BC30E1,
	TestingUnitTests_Start_m8C9EF37C5BC5461665D44564D0FE7FB3A6C69A66,
	TestingUnitTests_cubeNamed_m3948700DD2B6420F25DFC461AB2A2A5357E792D6,
	TestingUnitTests_timeBasedTesting_mBD8B52CEA7286C9F4FAAA419C773B78255E55F4D,
	TestingUnitTests_lotsOfCancels_mD21D0D0D0B8BE316E58C412A19ADE6658DAA5134,
	TestingUnitTests_pauseTimeNow_m3293FFC3DDEC01D507093F2D2B8E34B40FC0DD7F,
	TestingUnitTests_rotateRepeatFinished_m3ED0D0A9DF982711F7D8D670535F1136CB7E3BA0,
	TestingUnitTests_rotateRepeatAllFinished_mA343A62B2E6F1D84DAD61BF7DAD6DDC4F24A5077,
	TestingUnitTests_eventGameObjectCalled_mAFA0C3AF3614A013DC2BB8B1F82E6FB121FE56AF,
	TestingUnitTests_eventGeneralCalled_m9054A75A4B6E1A3BE3E393F88168FAEA8B34D65E,
	TestingUnitTests__ctor_m7108B4718BC8D59EC59A4EF627657842243EBB27,
	TestingUnitTests_U3ClotsOfCancelsU3Eb__25_0_m79612DDE2FFAD2A0986AA0D91A768BA96269D4F6,
	TestingUnitTests_U3CpauseTimeNowU3Eb__26_1_mB0C630E660D38F819734F11E2998CDE43590D503,
	U3CU3Ec__DisplayClass22_0__ctor_m85E81FE7FB95A8484DB589E78AB8F1B684592DDF,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__0_m0E29967BE8C8986E397C440673E2793E2569089B,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__1_m887D60815BE91C503B1B557775A0633F1B60E86E,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__21_mD56FCF9AA4FAE66DF497E1779D9334E757A222E5,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__2_m25A626FC5B5176333408538B200230F7A012FCF1,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__4_mC5E35367A4E3D78731DCB8D56BC172E263F40648,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__5_m31888B2277D8EEE835811D9FF81FA68312AA086B,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__6_mE7251F18767B392C7011C9893101E7498D8B0C29,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__8_mCAE5BA94A4F65FFE4C9F531C9AA0249D6F7B9218,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__9_m4AA88BFFB46D1E0E6212B98A76E7A0F6AA5C0039,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__10_m561C045E5C1DC104420555CF0BA62F1DF6B671E8,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__11_m81593E9FC9183EC0E37356C733A02C3266B87796,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__13_mF54BD21F02ED31763F697C67DB121D4646CC7522,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__14_m5A91E5886E31BA74327DDC19A47BB2E14DB7E810,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__15_mF1644C8D1D721BD4651678F2F71ACCA470CA60D9,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__16_m347E0C3ED3E8390844A251CE7CDAE009166D0358,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__17_m5404D4E64F471E4BB3E95B2DBF8C78AEDCD6C4FC,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__19_mFCD1C9E7E22F2F6710CB151C14E7C6094B6F0EF2,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__20_m07D0BA90D12217971B1F53FF20AA9B02391A4877,
	U3CU3Ec__DisplayClass22_1__ctor_m42740028F7DBAC0910C2B0D8FE48AE424104B01F,
	U3CU3Ec__DisplayClass22_1_U3CStartU3Eb__23_m71B69B2DE70762D5765B748E1B062290289D9195,
	U3CU3Ec__DisplayClass22_2__ctor_m92BFBC8093B249E40CB64FABE4D548B594B0A5A0,
	U3CU3Ec__DisplayClass22_2_U3CStartU3Eb__24_mF27E3CE748DB0EEE7037C4DD812DD75BAE86B46A,
	U3CU3Ec__cctor_m6F153F2D37E38349116C0879DD2F597DEC855ADD,
	U3CU3Ec__ctor_mF4403EE165173DC522103616E6645018625DE513,
	U3CU3Ec_U3CStartU3Eb__22_3_mB3B2B3F460A87856365CD811630D58EA7A704357,
	U3CU3Ec_U3CStartU3Eb__22_22_mCFEF8C7D30A25EAB5E114C737BF1D2A30D599CA9,
	U3CU3Ec_U3CStartU3Eb__22_7_mAC888F5211B48BEDE3B76735AF1835BC125992CE,
	U3CU3Ec_U3CStartU3Eb__22_12_m56B9B268A61C3FB036CCE2D844B45C27C0B201AD,
	U3CU3Ec_U3CStartU3Eb__22_18_m210C935C0307C41651403480E5EF0856246C8C3D,
	U3CU3Ec_U3CpauseTimeNowU3Eb__26_0_mE763C76C7D6AEEAAB8A0C84892C1D42F508144EB,
	U3CU3Ec__DisplayClass24_0__ctor_m8818C4F306B80B7FEE062793CBEBEAB352E53F22,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__0_mB82E5FEE09CC712C778F6A007B14D3D455256ECB,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__1_m8D5BED91D3EE4A8C0C0DF77D2B62F903C3E20D0C,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__2_mB677486A14C58435757062820302D43BCC02D144,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__3_m3987A57ECD0F50AB39FEF707C42D9710AA3CFB2D,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__4_m87D8E7758DA9C355EE513F9775D9F4B4C387BB56,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__5_m2B18F0F9B914A675CEEA356DCFF478B4F5611D8F,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__6_mE7EA3413E1F81C42AAFE03C96A7C6C430EAFAF81,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__7_mF6A7BD6551F5A9F146C1B47E86AFF650A2B0005D,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__13_mFFA8F8E7E5F79A608E1C2216AA8D1B734A758093,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__14_m0D4E422B37F43F7D3ACECDE81ADD5A18D4C97EE8,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__15_mE41B0CD588F01B0A76360EBD332CFAF425E20931,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__16_mEA9C490B5AEA717E999FFF3DD2CB25B23966277E,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__8_m63E7CF9513E4624B5B101B1C46941DC34982AC33,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__9_m7B1419838E647715BC8200AD6D56DAF1A8F04F81,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__10_m91860D812373862DCCD33D1D479A52867ED3CC02,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__11_m9D776D70F7B40CB7DB069BE5D095C281D59F2713,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__12_mB40AAAC5766C52276E4F9A375DB379DBE62012FA,
	U3CtimeBasedTestingU3Ed__24__ctor_mBB473D0F9FCF74C17EE67A02377DBAC113B0ECE6,
	U3CtimeBasedTestingU3Ed__24_System_IDisposable_Dispose_m2240ABE280E91D3110F4956F4DDAE8A6F211340A,
	U3CtimeBasedTestingU3Ed__24_MoveNext_mF87066714174A943FE18B6A562B291DFA766B72E,
	U3CtimeBasedTestingU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mACB1A3175D122506B71BCDB9EBF4AA9F0383E74B,
	U3CtimeBasedTestingU3Ed__24_System_Collections_IEnumerator_Reset_m96D9D8DFA5B24A08E71FA9EA4595288F06E9837B,
	U3CtimeBasedTestingU3Ed__24_System_Collections_IEnumerator_get_Current_mA059C8602E0A9BCCC1849108A55B19E09E4818CA,
	U3ClotsOfCancelsU3Ed__25__ctor_m9B88818966D8A321B3F23FB53C461C28A354F1C0,
	U3ClotsOfCancelsU3Ed__25_System_IDisposable_Dispose_m40E5F2D2626AB1E624CA39E3740CDFBE5B4F1B30,
	U3ClotsOfCancelsU3Ed__25_MoveNext_m9AA75163A6CE98F58622048E0971E438D1E2AFEE,
	U3ClotsOfCancelsU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2F111DF2BFDD6B3974DFA490B271DC9FCCBFC1DB,
	U3ClotsOfCancelsU3Ed__25_System_Collections_IEnumerator_Reset_m440F860762B28408E3B9AB6D72144B5E6A1C6F8C,
	U3ClotsOfCancelsU3Ed__25_System_Collections_IEnumerator_get_Current_mA7D1CE85A7D50D60D73C5914F5EC60B5E33A344E,
	U3CpauseTimeNowU3Ed__26__ctor_mD1AE96EC04B7EB3C54830BBCA910ACEC6A857D84,
	U3CpauseTimeNowU3Ed__26_System_IDisposable_Dispose_mE8F0437ED1B062C967A1FFCA460708BD42BC5142,
	U3CpauseTimeNowU3Ed__26_MoveNext_m0750DC2D5E587C420188232EDCAD54FEE7ABF659,
	U3CpauseTimeNowU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD12A76EAE3CD0B1B9C938030B3E4D2FAA6456011,
	U3CpauseTimeNowU3Ed__26_System_Collections_IEnumerator_Reset_m88957DE3F231D3C4F8E64C6DADA45876FABB7FD0,
	U3CpauseTimeNowU3Ed__26_System_Collections_IEnumerator_get_Current_m04EA51DB175A4043B51D9B3CFDD410D924B422A0,
};
extern void CSCSBonusGameSettings__ctor_m058A3519669A5F5179EC9C3C2148D50331189E9B_AdjustorThunk (void);
extern void CSCardValue_get_rankIndex_m174802D6FCD42BE5D4EF8CB18F1526F2F050F1AF_AdjustorThunk (void);
extern void CSCardValue_get_suitIndex_m781E7F614976BADE99E6284DABCA108E380ACFB4_AdjustorThunk (void);
extern void CSCardValue__ctor_m8E30CBB7352115D6A1CD8DF7E4C2DB28AFB5F381_AdjustorThunk (void);
extern void CSCardValue_Equals_m76AD0CA57CDB069290DE8B8D56217FCBB3F9DF06_AdjustorThunk (void);
extern void CSCardValue_GetHashCode_m38131925AA54B1CD1AF9ECE3514E68C48576D5C6_AdjustorThunk (void);
extern void CSCardValue_ToString_m6E389700BF8EE2F6B660F1216EDE3DD029719E7C_AdjustorThunk (void);
extern void CSSymbolPercent_ToString_mDB52EEF0AB867320264AF736B487BE6B117CF240_AdjustorThunk (void);
extern void L2D_get_Item_m17258F42C1AB187829FA85D33B930995C272CB8C_AdjustorThunk (void);
extern void L2D_set_Item_m4D9FEDEA05402830C287E075742C20E4F947645A_AdjustorThunk (void);
extern void List2D_get_Item_mB2DF0149C9C9045D8DF5B2A6796335BF11D30B16_AdjustorThunk (void);
extern void List2D_set_Item_m9566EFC162F85575933F9BD9127E2B2BA6C1C0CE_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[12] = 
{
	{ 0x06000471, CSCSBonusGameSettings__ctor_m058A3519669A5F5179EC9C3C2148D50331189E9B_AdjustorThunk },
	{ 0x060004C5, CSCardValue_get_rankIndex_m174802D6FCD42BE5D4EF8CB18F1526F2F050F1AF_AdjustorThunk },
	{ 0x060004C6, CSCardValue_get_suitIndex_m781E7F614976BADE99E6284DABCA108E380ACFB4_AdjustorThunk },
	{ 0x060004C8, CSCardValue__ctor_m8E30CBB7352115D6A1CD8DF7E4C2DB28AFB5F381_AdjustorThunk },
	{ 0x060004CB, CSCardValue_Equals_m76AD0CA57CDB069290DE8B8D56217FCBB3F9DF06_AdjustorThunk },
	{ 0x060004CC, CSCardValue_GetHashCode_m38131925AA54B1CD1AF9ECE3514E68C48576D5C6_AdjustorThunk },
	{ 0x060004CD, CSCardValue_ToString_m6E389700BF8EE2F6B660F1216EDE3DD029719E7C_AdjustorThunk },
	{ 0x06000619, CSSymbolPercent_ToString_mDB52EEF0AB867320264AF736B487BE6B117CF240_AdjustorThunk },
	{ 0x06000657, L2D_get_Item_m17258F42C1AB187829FA85D33B930995C272CB8C_AdjustorThunk },
	{ 0x06000658, L2D_set_Item_m4D9FEDEA05402830C287E075742C20E4F947645A_AdjustorThunk },
	{ 0x06000672, List2D_get_Item_mB2DF0149C9C9045D8DF5B2A6796335BF11D30B16_AdjustorThunk },
	{ 0x06000673, List2D_set_Item_m9566EFC162F85575933F9BD9127E2B2BA6C1C0CE_AdjustorThunk },
};
static const int32_t s_InvokerIndices[2197] = 
{
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	3111,
	1593,
	1550,
	1593,
	1593,
	1196,
	1958,
	1958,
	1958,
	1639,
	1958,
	1616,
	1958,
	1958,
	1958,
	1639,
	3158,
	1958,
	1958,
	1616,
	1958,
	1616,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1616,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	407,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1616,
	1958,
	1958,
	1958,
	1958,
	1958,
	1616,
	1958,
	1958,
	1593,
	1593,
	1593,
	1593,
	1958,
	1958,
	1958,
	1550,
	1958,
	1958,
	1958,
	1958,
	1958,
	1639,
	1641,
	1550,
	3158,
	1958,
	1616,
	1958,
	1958,
	1958,
	1616,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	613,
	1958,
	1958,
	1958,
	1958,
	1616,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1641,
	1641,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1616,
	1593,
	1958,
	1958,
	908,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	886,
	1958,
	615,
	1593,
	3158,
	1958,
	1616,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1641,
	1641,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1616,
	1593,
	1958,
	1958,
	908,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	886,
	1958,
	615,
	1593,
	3158,
	1958,
	1616,
	1593,
	1593,
	1583,
	3142,
	2512,
	2512,
	2462,
	2745,
	3107,
	2751,
	2758,
	3040,
	2761,
	2525,
	2761,
	2625,
	1958,
	3158,
	1958,
	1193,
	1196,
	1901,
	1901,
	1901,
	1901,
	1193,
	1199,
	1199,
	2099,
	2101,
	2058,
	2025,
	2027,
	2011,
	2383,
	2387,
	2433,
	2004,
	2005,
	2000,
	1958,
	1958,
	1901,
	1958,
	1583,
	1958,
	1924,
	1901,
	1958,
	1901,
	2642,
	2652,
	3037,
	3083,
	2756,
	3040,
	2756,
	3158,
	1958,
	3158,
	3158,
	3136,
	3136,
	3136,
	3107,
	2901,
	3158,
	1958,
	2936,
	3107,
	3158,
	2901,
	3107,
	2761,
	2858,
	3158,
	3113,
	3111,
	2930,
	3111,
	2630,
	2923,
	3107,
	2905,
	3037,
	3037,
	3040,
	2923,
	3107,
	3111,
	3158,
	3158,
	2923,
	3107,
	3111,
	3066,
	3066,
	3063,
	3066,
	3066,
	3063,
	3066,
	2117,
	3040,
	3040,
	3142,
	3142,
	2336,
	2756,
	3045,
	2519,
	2519,
	2519,
	2519,
	2519,
	2519,
	2501,
	2501,
	2501,
	2768,
	2768,
	2518,
	2518,
	2758,
	2525,
	2523,
	2515,
	2515,
	2515,
	2515,
	2515,
	2515,
	2523,
	2523,
	2519,
	2519,
	2519,
	2525,
	2515,
	2519,
	2519,
	2519,
	2515,
	2515,
	2515,
	2525,
	2519,
	2525,
	2519,
	2519,
	2519,
	2337,
	2337,
	2525,
	2523,
	2519,
	2519,
	2519,
	2333,
	2529,
	2335,
	2338,
	2313,
	2181,
	2181,
	2173,
	2173,
	2182,
	2183,
	2181,
	2525,
	2329,
	2525,
	2519,
	2519,
	2519,
	2519,
	2525,
	2337,
	2337,
	2525,
	2523,
	2519,
	2501,
	2856,
	2883,
	2577,
	2577,
	2577,
	2582,
	2577,
	2577,
	2577,
	2577,
	2577,
	2577,
	2383,
	2577,
	2577,
	2577,
	2577,
	2577,
	2577,
	2577,
	2577,
	2577,
	2577,
	2577,
	2577,
	2577,
	2577,
	2577,
	2577,
	2577,
	2577,
	2577,
	2577,
	2577,
	2383,
	2383,
	2383,
	2210,
	2210,
	2210,
	2174,
	2040,
	2017,
	2324,
	2903,
	2629,
	2814,
	3063,
	2550,
	3107,
	2903,
	1958,
	3158,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	3040,
	1958,
	185,
	1436,
	1478,
	1478,
	1958,
	1593,
	1593,
	1927,
	1478,
	891,
	891,
	891,
	519,
	891,
	519,
	1616,
	701,
	1593,
	890,
	890,
	1478,
	1478,
	1439,
	1478,
	891,
	891,
	891,
	519,
	891,
	519,
	1616,
	1550,
	2919,
	2644,
	492,
	1901,
	3158,
	1958,
	1608,
	355,
	183,
	117,
	1924,
	1889,
	798,
	1958,
	1958,
	1927,
	1616,
	1927,
	1616,
	1927,
	1616,
	1927,
	1616,
	1915,
	1608,
	1196,
	1198,
	1188,
	1199,
	1196,
	619,
	1198,
	1198,
	1901,
	808,
	3158,
	3158,
	3158,
	3107,
	3068,
	3107,
	3107,
	2527,
	2511,
	2527,
	2511,
	2751,
	2833,
	3068,
	2843,
	3155,
	1958,
	3158,
	2519,
	2519,
	2519,
	2519,
	2519,
	3111,
	2930,
	2630,
	3111,
	2501,
	2501,
	2518,
	2518,
	3066,
	3066,
	3066,
	2525,
	2525,
	2525,
	2523,
	2523,
	2515,
	2515,
	2515,
	2515,
	2515,
	2515,
	2525,
	2515,
	2515,
	2525,
	2515,
	2515,
	2519,
	2519,
	2519,
	2519,
	2519,
	2519,
	2515,
	2515,
	2515,
	2515,
	2515,
	2515,
	2519,
	2519,
	2519,
	2519,
	2519,
	2519,
	2519,
	2519,
	2519,
	3111,
	2756,
	3111,
	2525,
	2525,
	2525,
	2337,
	2337,
	2337,
	2337,
	2337,
	2337,
	2519,
	2519,
	2519,
	2519,
	2519,
	2519,
	2525,
	2525,
	2525,
	2519,
	2519,
	2519,
	2519,
	2519,
	2519,
	2523,
	2313,
	2333,
	2335,
	2338,
	2181,
	2181,
	2181,
	2173,
	2182,
	2183,
	2931,
	2931,
	2931,
	2931,
	2931,
	2931,
	2957,
	1953,
	1641,
	1953,
	1641,
	1901,
	1593,
	1901,
	1593,
	1901,
	1901,
	1958,
	1196,
	1889,
	1889,
	1901,
	1593,
	1958,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1958,
	1204,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1958,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1958,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1196,
	1958,
	1958,
	1901,
	1924,
	1958,
	1188,
	2646,
	2626,
	2645,
	2931,
	2919,
	2919,
	2646,
	2646,
	2919,
	2669,
	1901,
	1901,
	1204,
	1199,
	1193,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1901,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1953,
	1199,
	1199,
	1199,
	1196,
	1204,
	1196,
	1204,
	1199,
	1204,
	1198,
	608,
	1199,
	1199,
	1199,
	1193,
	1193,
	1198,
	1198,
	1198,
	1198,
	1193,
	1901,
	1901,
	1193,
	1901,
	1193,
	1196,
	1196,
	615,
	1196,
	1196,
	1196,
	1196,
	1196,
	1196,
	1196,
	1196,
	1196,
	1196,
	615,
	615,
	615,
	615,
	1196,
	1198,
	1198,
	1196,
	1197,
	1196,
	1204,
	1198,
	1196,
	1198,
	1198,
	1196,
	1196,
	1199,
	1196,
	1199,
	1198,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	886,
	1953,
	615,
	1477,
	886,
	1958,
	615,
	1593,
	3158,
	1958,
	1958,
	1958,
	1901,
	1593,
	1953,
	1641,
	1953,
	1641,
	1927,
	1616,
	1910,
	1602,
	1901,
	1593,
	1901,
	1593,
	1901,
	1593,
	1901,
	1593,
	1901,
	1593,
	1901,
	1593,
	1901,
	1593,
	1901,
	1593,
	1901,
	1593,
	1901,
	1593,
	1901,
	1593,
	1901,
	1593,
	1901,
	1593,
	1901,
	1593,
	1901,
	1593,
	1901,
	1593,
	1958,
	910,
	1958,
	1889,
	1958,
	798,
	1901,
	1927,
	1199,
	1196,
	615,
	615,
	399,
	1196,
	1196,
	1199,
	516,
	1901,
	1958,
	1924,
	1613,
	1958,
	1593,
	1958,
	1593,
	1958,
	1958,
	1196,
	406,
	1958,
	1958,
	1613,
	1958,
	1958,
	1958,
	1639,
	1958,
	1958,
	1958,
	1958,
	1958,
	908,
	1593,
	1958,
	910,
	1958,
	1958,
	1613,
	1958,
	1958,
	1958,
	1958,
	1927,
	808,
	406,
	1593,
	1196,
	507,
	1958,
	1593,
	1924,
	1613,
	1958,
	1958,
	1958,
	1958,
	1924,
	1613,
	1958,
	1583,
	1958,
	903,
	1199,
	1958,
	1616,
	1958,
	1958,
	1958,
	1958,
	1958,
	1613,
	1958,
	1889,
	1583,
	1924,
	1613,
	1889,
	1583,
	1616,
	1958,
	1958,
	1958,
	1958,
	1593,
	1958,
	1958,
	1616,
	3158,
	1958,
	909,
	482,
	3126,
	1613,
	1583,
	982,
	1958,
	1958,
	1958,
	1958,
	715,
	1958,
	1958,
	1958,
	1924,
	1613,
	1927,
	1616,
	1958,
	1958,
	1958,
	1958,
	1593,
	1193,
	1199,
	1199,
	404,
	1613,
	1958,
	1593,
	1593,
	1958,
	1958,
	1958,
	1616,
	1889,
	1583,
	1889,
	1583,
	1958,
	1927,
	1616,
	1616,
	1958,
	1901,
	1958,
	1889,
	1593,
	1958,
	1927,
	1616,
	1958,
	1958,
	1958,
	1958,
	1199,
	909,
	908,
	910,
	1434,
	256,
	1958,
	888,
	1958,
	1958,
	1616,
	1889,
	1583,
	1927,
	1616,
	1924,
	1613,
	1924,
	1958,
	1958,
	1958,
	1583,
	1958,
	1583,
	1616,
	1958,
	1613,
	910,
	1958,
	1616,
	1889,
	1889,
	3127,
	798,
	2793,
	2793,
	1348,
	1889,
	1901,
	1593,
	1593,
	1901,
	1924,
	1613,
	1924,
	1613,
	1855,
	1545,
	1958,
	1958,
	460,
	1958,
	903,
	1958,
	1187,
	1958,
	1958,
	1958,
	1927,
	1616,
	1889,
	1583,
	1958,
	908,
	353,
	1616,
	1616,
	1433,
	1434,
	1958,
	1958,
	1958,
	1616,
	1958,
	1958,
	401,
	1196,
	697,
	1958,
	3158,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1889,
	1593,
	1348,
	1924,
	1901,
	1958,
	1348,
	1924,
	1613,
	1889,
	1583,
	1927,
	1616,
	1616,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	3158,
	1924,
	1613,
	1924,
	1613,
	1927,
	1616,
	1889,
	1583,
	1924,
	1613,
	1927,
	1616,
	1927,
	1616,
	1889,
	1583,
	1901,
	1593,
	1889,
	1583,
	1927,
	1616,
	1958,
	1901,
	1958,
	1958,
	1958,
	1958,
	1901,
	1593,
	1593,
	1593,
	1924,
	1958,
	1100,
	1958,
	3158,
	1889,
	1958,
	1958,
	1958,
	1593,
	1924,
	1196,
	1196,
	1196,
	1196,
	1593,
	1958,
	888,
	1583,
	1109,
	884,
	1196,
	1958,
	3158,
	1958,
	1593,
	3158,
	1958,
	1613,
	1958,
	1924,
	1613,
	1958,
	1616,
	1593,
	1193,
	1199,
	1199,
	404,
	1613,
	1958,
	1958,
	1958,
	1958,
	1616,
	1889,
	1193,
	808,
	1958,
	1958,
	1958,
	1613,
	1924,
	1958,
	1193,
	808,
	1958,
	914,
	1583,
	1196,
	1476,
	1196,
	132,
	1958,
	1958,
	1958,
	1958,
	1616,
	1958,
	1958,
	1593,
	1958,
	1958,
	1958,
	505,
	1889,
	1889,
	1889,
	1583,
	1613,
	1593,
	1593,
	1924,
	1924,
	1613,
	1924,
	1613,
	1958,
	1951,
	1958,
	1193,
	1476,
	1958,
	1958,
	1958,
	1958,
	1613,
	1593,
	1196,
	1958,
	1593,
	1583,
	1958,
	1958,
	1334,
	1613,
	1616,
	1958,
	1958,
	1958,
	888,
	1958,
	1196,
	615,
	891,
	1593,
	1593,
	1593,
	1593,
	1958,
	1583,
	1958,
	1924,
	1901,
	1958,
	1901,
	1583,
	1958,
	1924,
	1901,
	1958,
	1901,
	1958,
	1958,
	1958,
	808,
	888,
	1958,
	1958,
	1958,
	1958,
	1924,
	1958,
	1958,
	1958,
	3158,
	1924,
	1613,
	1958,
	1958,
	1958,
	1593,
	1193,
	1199,
	1199,
	404,
	1613,
	890,
	1958,
	1613,
	1613,
	1616,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1616,
	1924,
	1613,
	1958,
	1958,
	1958,
	1199,
	1593,
	1958,
	1958,
	506,
	1196,
	1958,
	1924,
	1613,
	1924,
	1613,
	1927,
	1616,
	1958,
	1958,
	1593,
	1958,
	1958,
	1593,
	1593,
	1593,
	888,
	1613,
	1958,
	3158,
	3158,
	1958,
	1593,
	1593,
	1593,
	1958,
	1348,
	1958,
	1593,
	1901,
	1924,
	1593,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	479,
	1583,
	891,
	891,
	1958,
	891,
	1958,
	1196,
	1901,
	1958,
	1901,
	1958,
	1958,
	1433,
	1958,
	1958,
	1616,
	1616,
	1593,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1348,
	1348,
	1958,
	1593,
	1901,
	1901,
	1593,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	1958,
	3158,
	3158,
	1958,
	1593,
	1958,
	1958,
	3158,
	1958,
	1958,
	798,
	3142,
	3142,
	3142,
	3142,
	3142,
	3142,
	1901,
	1901,
	1901,
	1901,
	2756,
	2756,
	2887,
	2825,
	2825,
	1348,
	1889,
	1901,
	648,
	472,
	1958,
	1334,
	820,
	1958,
	1593,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1901,
	1924,
	1613,
	1958,
	1958,
	1613,
	1613,
	1616,
	1901,
	1613,
	1958,
	1958,
	1958,
	1616,
	608,
	471,
	1958,
	1193,
	808,
	1958,
	1958,
	1593,
	1958,
	1958,
	1616,
	1958,
	1616,
	886,
	882,
	241,
	1593,
	882,
	882,
	1593,
	1593,
	1593,
	1593,
	1924,
	1613,
	463,
	1593,
	462,
	1958,
	1958,
	1634,
	1583,
	1583,
	1583,
	1634,
	1583,
	1583,
	1583,
	1946,
	1461,
	1946,
	1889,
	1889,
	1889,
	1889,
	1872,
	1872,
	1872,
	1924,
	1593,
	1593,
	1958,
	3131,
	1958,
	1593,
	1958,
	1593,
	1593,
	1958,
	1958,
	1958,
	1901,
	1958,
	1958,
	609,
	601,
	1196,
	1593,
	1593,
	1593,
	660,
	888,
	1190,
	1958,
	1193,
	1958,
	1901,
	1901,
	1348,
	1348,
	1593,
	1593,
	1958,
	3158,
	1958,
	890,
	1958,
	1958,
	1613,
	1958,
	1958,
	3158,
	1958,
	1613,
	3046,
	-1,
	2328,
	2578,
	2186,
	2085,
	2767,
	1958,
	3158,
	1958,
	1616,
	1958,
	1616,
	1958,
	1958,
	1373,
	1958,
	1593,
	890,
	773,
	1542,
	1958,
	1958,
	1958,
	1593,
	1958,
	1889,
	1583,
	1901,
	1593,
	1901,
	1593,
	1901,
	1593,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1583,
	1145,
	1402,
	773,
	1958,
	1583,
	1958,
	1958,
	1958,
	1958,
	1593,
	1593,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1927,
	1616,
	1927,
	1616,
	1924,
	1613,
	1924,
	1613,
	908,
	1593,
	1958,
	1958,
	1616,
	527,
	902,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1616,
	1958,
	1958,
	1593,
	1593,
	1593,
	1593,
	1924,
	1613,
	1958,
	1193,
	517,
	1958,
	608,
	452,
	1901,
	1855,
	540,
	1593,
	888,
	660,
	1958,
	1958,
	3158,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1593,
	1958,
	1958,
	1901,
	1958,
	1583,
	1958,
	1924,
	1901,
	1958,
	1901,
	363,
	1958,
	363,
	1958,
	1901,
	1593,
	1901,
	1593,
	1901,
	1593,
	1901,
	1593,
	1901,
	1593,
	1958,
	1958,
	1593,
	1593,
	723,
	723,
	496,
	496,
	505,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1901,
	1958,
	1583,
	1958,
	1924,
	1901,
	1958,
	1901,
	1901,
	1958,
	1583,
	1958,
	1924,
	1901,
	1958,
	1901,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1901,
	1958,
	1583,
	1958,
	1924,
	1901,
	1958,
	1901,
	1958,
	1958,
	1958,
	1958,
	1958,
	1196,
	1901,
	1958,
	1583,
	1958,
	1924,
	1901,
	1958,
	1901,
	1958,
	1901,
	1958,
	1583,
	1958,
	1924,
	1901,
	1958,
	1901,
	1958,
	1958,
	1958,
	1958,
	1593,
	1196,
	1196,
	1958,
	1583,
	1958,
	1924,
	1901,
	1958,
	1901,
	1583,
	1958,
	1924,
	1901,
	1958,
	1901,
	1958,
	1958,
	1901,
	1901,
	1958,
	1583,
	1958,
	1924,
	1901,
	1958,
	1901,
	1583,
	1958,
	1924,
	1901,
	1958,
	1901,
	1958,
	1958,
	1958,
	1958,
	1583,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1583,
	1958,
	1958,
	1958,
	723,
	723,
	496,
	496,
	505,
	1958,
	1958,
	1958,
	1958,
	1593,
	1593,
	1958,
	1958,
	1958,
	1958,
	1593,
	1958,
	1593,
	1593,
	1593,
	1593,
	1583,
	1958,
	1958,
	1958,
	1958,
	1583,
	1958,
	1958,
	1958,
	1901,
	1958,
	1583,
	1958,
	1924,
	1901,
	1958,
	1901,
	1958,
	1958,
	1958,
	1958,
	1593,
	1901,
	1958,
	1583,
	1958,
	1924,
	1901,
	1958,
	1901,
	1958,
	1958,
	1958,
	1958,
	1593,
	1901,
	1958,
	1583,
	1958,
	1924,
	1901,
	1958,
	1901,
	1958,
	1958,
	1958,
	1958,
	1593,
	1901,
	1958,
	1583,
	1958,
	1924,
	1901,
	1958,
	1901,
	1958,
	1958,
	1958,
	1958,
	1593,
	1901,
	1958,
	1958,
	560,
	1583,
	1958,
	1924,
	1901,
	1958,
	1901,
	1958,
	1958,
	1196,
	1901,
	1958,
	1583,
	1958,
	1924,
	1901,
	1958,
	1901,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1196,
	1901,
	1901,
	1901,
	1958,
	1958,
	1593,
	1593,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1958,
	1593,
	1958,
	1958,
	1958,
	1958,
	1639,
	1958,
	1958,
	1958,
	1958,
	1593,
	3158,
	1958,
	1958,
	1958,
	1958,
	1616,
	1958,
	1958,
	1958,
	1958,
	1958,
	1616,
	1958,
	1958,
	1958,
	1616,
	1958,
	1958,
	1641,
	1593,
	1958,
	1958,
	1616,
	1958,
	1641,
	1958,
	1583,
	1958,
	1924,
	1901,
	1958,
	1901,
	1583,
	1958,
	1924,
	1901,
	1958,
	1901,
	1583,
	1958,
	1924,
	1901,
	1958,
	1901,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x0200006D, { 0, 9 } },
	{ 0x060006CE, { 9, 2 } },
};
extern const uint32_t g_rgctx_List_1_get_Count_m118A47185833FD69C6C38AD27AFAE818F504D2B8;
extern const uint32_t g_rgctx_CSListNavigation_1_CSInfinScrollValue_m5A95992EEC5019B0A8681FF0AC3A5B6D6CA52127;
extern const uint32_t g_rgctx_CSListNavigation_1_t5EB648FF03A32671AE6F729FE9C2BDCFDCBBF1E9;
extern const uint32_t g_rgctx_List_1_tFCA8FDEEBEE4BB7960BCC1544EAFFB13071BAA32;
extern const uint32_t g_rgctx_List_1__ctor_m05422EEF40CAECDC0D40798B243F823E8F8DAB88;
extern const uint32_t g_rgctx_List_1_get_Item_m04DB9E9D88FCA5AEB1F457EFEF4C0593537607D3;
extern const uint32_t g_rgctx_List_1_Add_m3F877EF237CA8635A610D9CABD706504C0DF537F;
extern const uint32_t g_rgctx_CSListNavigation_1_get_idx_mAB736819EAB246BEE7D20DDBDB485B3A5E8888C9;
extern const uint32_t g_rgctx_CSListNavigation_1_set_idx_m441D425CD194A97FECF90981E6E225F52553EA94;
extern const uint32_t g_rgctx_T_t0C7461B6FD1FB0F7E34C4C2F86036E57E52C4D43;
extern const uint32_t g_rgctx_T_t0C7461B6FD1FB0F7E34C4C2F86036E57E52C4D43;
static const Il2CppRGCTXDefinition s_rgctxValues[11] = 
{
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_get_Count_m118A47185833FD69C6C38AD27AFAE818F504D2B8 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_CSListNavigation_1_CSInfinScrollValue_m5A95992EEC5019B0A8681FF0AC3A5B6D6CA52127 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_CSListNavigation_1_t5EB648FF03A32671AE6F729FE9C2BDCFDCBBF1E9 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_tFCA8FDEEBEE4BB7960BCC1544EAFFB13071BAA32 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1__ctor_m05422EEF40CAECDC0D40798B243F823E8F8DAB88 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_get_Item_m04DB9E9D88FCA5AEB1F457EFEF4C0593537607D3 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_List_1_Add_m3F877EF237CA8635A610D9CABD706504C0DF537F },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_CSListNavigation_1_get_idx_mAB736819EAB246BEE7D20DDBDB485B3A5E8888C9 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_CSListNavigation_1_set_idx_m441D425CD194A97FECF90981E6E225F52553EA94 },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t0C7461B6FD1FB0F7E34C4C2F86036E57E52C4D43 },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t0C7461B6FD1FB0F7E34C4C2F86036E57E52C4D43 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	2197,
	s_methodPointers,
	12,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	11,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
